package com.wiley.core.wiley.classification.impl;

import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;
import com.wiley.core.wiley.classification.WileyClassificationService;


/**
 * Created by Uladzimir_Barouski on 12/29/2016.
 */
public class WileyClassificationServiceImpl implements WileyClassificationService
{
	@Autowired
	private PersistentKeyGenerator cmsPoductAttributeUidGenerator;

	@Autowired
	private ModelService modelService;

	@Override
	public Set<ClassAttributeAssignmentModel> getFilteredAssignmentForProducts(final List<WileyVariantProductModel> products)
	{
		Set<ClassAttributeAssignmentModel> filteredAssignments = new HashSet<>();

		for (ProductModel product : products)
		{

			if (CollectionUtils.isNotEmpty(product.getFeatures()))
			{
				Set<ClassAttributeAssignmentModel> productAssignments = product.getFeatures().stream()
						.filter(feature ->	filterProductFeatures(feature))
						.map(featureModel -> featureModel.getClassificationAttributeAssignment())
						.collect(Collectors.toSet());

				filteredAssignments.addAll(productAssignments);

			}
		}
		return filteredAssignments;
	}

	private boolean filterProductFeatures(final ProductFeatureModel feature)
	{
		ClassAttributeAssignmentModel classAttributeAssignment = feature.getClassificationAttributeAssignment();
		setDefaultDisplayName(classAttributeAssignment);
		return isFeatureVisible(feature);
	}

	private void setDefaultDisplayName(final ClassAttributeAssignmentModel classAttributeAssignment)
	{
		if (StringUtils.isEmpty(classAttributeAssignment.getDisplayName()))
		{
			classAttributeAssignment.setDisplayName(classAttributeAssignment.getClassificationAttribute().getName());
		}
	}

	public boolean isFeatureVisible(final ProductFeatureModel feature)
	{
		ClassAttributeAssignmentModel classAttributeAssignment = feature.getClassificationAttributeAssignment();
		final Object featureValue = feature.getValue();
		return classAttributeAssignment != null
				&& ClassAttributeAssignmentTypeEnum.INCLUDES == classAttributeAssignment.getType()
				&& ClassificationAttributeTypeEnum.BOOLEAN == classAttributeAssignment.getAttributeType()
				&& featureValue != null && Boolean.TRUE.equals(featureValue);
	}

	@Override
	public void mergeAssigmentsWithCMSProductAttributes(final Set<ClassAttributeAssignmentModel> assignments,
			final VariantProductComparisonTableComponentModel variantProductComparisonTableComponentModel)
	{
		Set<CMSProductAttributeModel> cmsProductAttributesForRemove = new HashSet<>();
		Set<ClassAttributeAssignmentModel> existingAssignments = new HashSet<>();
		if (CollectionUtils.isNotEmpty(variantProductComparisonTableComponentModel.getCmsProductAttributes()))
		{
			variantProductComparisonTableComponentModel.getCmsProductAttributes().stream().forEachOrdered(attribute ->
			{
				if (!assignments.contains(attribute.getAssignment()))
				{
					cmsProductAttributesForRemove.add(attribute);
				}
				else
				{
					existingAssignments.add(attribute.getAssignment());
				}
			});

			modelService.removeAll(cmsProductAttributesForRemove);
			assignments.removeAll(existingAssignments);
		}
		for (ClassAttributeAssignmentModel assignment : assignments)
		{
			CMSProductAttributeModel comparisonRowModel = modelService.create(CMSProductAttributeModel.class);
			comparisonRowModel.setAssignment(assignment);
			comparisonRowModel.setComparisonTableComponent(variantProductComparisonTableComponentModel);
			comparisonRowModel.setCatalogVersion(variantProductComparisonTableComponentModel.getCatalogVersion());
			comparisonRowModel.setUid(cmsPoductAttributeUidGenerator.generate().toString());
			modelService.save(comparisonRowModel);
			//save assignment to update display name and tooltip
			modelService.save(assignment);
		}
	}
}
