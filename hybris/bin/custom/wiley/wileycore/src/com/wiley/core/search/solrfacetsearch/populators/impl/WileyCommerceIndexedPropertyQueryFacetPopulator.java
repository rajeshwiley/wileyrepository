package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrValueRangeSetModel;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyCommerceIndexedPropertyQueryFacetPopulator implements Populator<SolrIndexedPropertyModel, IndexedProperty>
{
	private Converter<SolrValueRangeSetModel, ValueRangeSet> valueRangeSetConverter;

	@Override
	public void populate(final SolrIndexedPropertyModel solrIndexedPropertyModel, final IndexedProperty indexedProperty)
			throws ConversionException
	{
		indexedProperty.setQueryFacet(solrIndexedPropertyModel.isQueryFacet());
		indexedProperty.setQueryValueRangeSets(convertRangeSet(solrIndexedPropertyModel));
	}

	private Map<String, ValueRangeSet> convertRangeSet(final SolrIndexedPropertyModel source)
	{
		final List<ValueRangeSet> convertAll = Converters.convertAll(source.getQueryRangeSets(), this.valueRangeSetConverter);
		return convertAll.stream().collect(Collectors.toMap(ValueRangeSet::getQualifier, valueRangeSet -> valueRangeSet));
	}

	@Required
	public void setValueRangeSetConverter(final Converter<SolrValueRangeSetModel, ValueRangeSet> valueRangeSetConverter)
	{
		this.valueRangeSetConverter = valueRangeSetConverter;
	}

}
