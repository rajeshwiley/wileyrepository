package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommercePlaceOrderStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Date;

import javax.annotation.Resource;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class WileyCommercePlaceOrderStrategy extends DefaultCommercePlaceOrderStrategy
{
	@Resource
	private ModelService modelService;

	private static final Logger LOG = LoggerFactory.getLogger(WileyCommercePlaceOrderStrategy.class);

	/**
	 * This is mostly OOTB impl taken from DefaultCommercePlaceOrderStrategy with few changes
	 * - fail if cart about to place is not calculated
	 * - do not recalculate placed order
	 */
	@Override
	public CommerceOrderResult placeOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		final CartModel cartModel = parameter.getCart();
		modelService.refresh(cartModel);

		validateParameterNotNull(cartModel, "Cart model cannot be null");
		final CommerceOrderResult result = new CommerceOrderResult();
		try
		{
			beforePlaceOrder(parameter);
			if (getCalculationService().requiresCalculation(cartModel))
			{
				//CHANGE: do not allow to place not calculated cart as we do not allow any further calculation of order
				throw new InvalidCartException("Unable to place not calculated cart");
			}

			final CustomerModel customer = (CustomerModel) cartModel.getUser();
			validateParameterNotNull(customer, "Customer model cannot be null");

			final OrderModel orderModel = getOrderService().createOrderFromCart(cartModel);
			if (orderModel != null)
			{
				// Reset the Date attribute for use in determining when the order was placed
				orderModel.setDate(new Date());

				// Store the current site and store on the order
				orderModel.setSite(getBaseSiteService().getCurrentBaseSite());
				orderModel.setStore(getBaseStoreService().getCurrentBaseStore());
				orderModel.setLanguage(getCommonI18NService().getCurrentLanguage());

				if (parameter.getSalesApplication() != null)
				{
					orderModel.setSalesApplication(parameter.getSalesApplication());
				}

				// clear the promotionResults that where cloned from cart PromotionService.transferPromotionsToOrder
				// will copy them over bellow.
				orderModel.setAllPromotionResults(Collections.<PromotionResultModel> emptySet());

				getModelService().saveAll(customer, orderModel);

				fillPaymentAddress(cartModel, orderModel);

				getModelService().save(orderModel);
				// Transfer promotions to the order
				getPromotionsService().transferPromotionsToOrder(cartModel, orderModel, false);
				// CHANGE: OOTB does order recalculation and external tax calculation here
				// we do need order to be AS IS placed cart so force setting calculated flag to TRUE without calculation
				// as we placed calculated cart (see CHANGE above)
				orderModel.setCalculated(Boolean.TRUE);
				getModelService().save(orderModel);

				getModelService().refresh(orderModel);
				getModelService().refresh(customer);

				result.setOrder(orderModel);

				this.beforeSubmitOrder(parameter, result);

				getOrderService().submitOrder(orderModel);
			}
			else
			{
				throw new IllegalArgumentException(String.format("Order was not properly created from cart %s",
						cartModel.getCode()));
			}
		}
		finally
		{
			getExternalTaxesService().clearSessionTaxDocument();
		}

		this.afterPlaceOrder(parameter, result);
		return result;
	}

	protected void fillPaymentAddress(final CartModel cartModel, final OrderModel orderModel)
	{
		if (cartModel.getPaymentInfo() != null && cartModel.getPaymentInfo().getBillingAddress() != null)
		{
			final AddressModel billingAddress = cartModel.getPaymentInfo().getBillingAddress();
			orderModel.setPaymentAddress(billingAddress);
			orderModel.getPaymentInfo().setBillingAddress(cloneAddressForOrder(orderModel, billingAddress));
			getModelService().save(orderModel.getPaymentInfo());
		}
	}

	private AddressModel cloneAddressForOrder(final OrderModel orderModel, final AddressModel billingAddress)
	{
		final AddressModel clonedBillingAddress = getModelService().clone(billingAddress);
		clonedBillingAddress.setOwner(orderModel);
		return clonedBillingAddress;
	}
}
