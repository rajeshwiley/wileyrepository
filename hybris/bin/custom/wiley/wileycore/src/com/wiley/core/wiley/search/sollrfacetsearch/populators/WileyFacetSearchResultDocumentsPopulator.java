package com.wiley.core.wiley.search.sollrfacetsearch.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroupCommand;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultDocument;
import de.hybris.platform.solrfacetsearch.search.impl.SearchResultConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResultGroupCommand;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.GroupResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * This is just OOTB FacetSearchResultDocumentsPopulator from Hybris 6.5 - it supports search term highlight
 * structure and replaces original document fields with highlighted versions
 */
public class WileyFacetSearchResultDocumentsPopulator implements Populator<SearchResultConverterData, SolrSearchResult>
{
	@Autowired
	private FieldNameTranslator fieldNameTranslator;

	public void populate(final SearchResultConverterData source, final SolrSearchResult target)
	{
		QueryResponse queryResponse = source.getQueryResponse();
		if (queryResponse != null)
		{
			GroupResponse groupResponse = queryResponse.getGroupResponse();
			FieldNameTranslator.FieldInfosMapping fieldInfosMapping =
					fieldNameTranslator.getFieldInfos(source.getFacetSearchContext());
			Map<String, FieldNameTranslator.FieldInfo> fieldInfos = fieldInfosMapping.getInvertedFieldInfos();
			long numberOfResults = 0L;
			List<Document> documents = new ArrayList<>();
			List<SolrDocument> solrDocuments = new ArrayList<>();
			if (groupResponse != null)
			{
				List<SearchResultGroupCommand> searchResultGroupCommands = new ArrayList<>();
				int groupCommandIndex = 0;

				for (Iterator iterator = groupResponse.getValues().iterator(); iterator.hasNext(); ++groupCommandIndex)
				{
					GroupCommand groupCommand = (GroupCommand) iterator.next();
					if (groupCommandIndex == 0)
					{
						numberOfResults = (long) groupCommand.getNGroups();
					}

					List<SearchResultGroup> searchResultGroups = new ArrayList<>();
					this.populateGroupedResults(documents, solrDocuments, searchResultGroups, fieldInfos, groupCommand,
							groupCommandIndex, queryResponse.getHighlighting());
					String groupCommandName = resolveFieldName(groupCommand.getName(), fieldInfos);
					SolrSearchResultGroupCommand searchResultGroupCommand = new SolrSearchResultGroupCommand();
					searchResultGroupCommand.setName(groupCommandName);
					searchResultGroupCommand.setNumberOfMatches((long) groupCommand.getMatches());
					searchResultGroupCommand.setNumberOfGroups((long) groupCommand.getNGroups());
					searchResultGroupCommand.setGroups(searchResultGroups);
					searchResultGroupCommands.add(searchResultGroupCommand);
				}

				target.getGroupCommands().addAll(searchResultGroupCommands);
			}
			else
			{
				SolrDocumentList results = queryResponse.getResults();

				if (CollectionUtils.isNotEmpty(results))
				{
					for (SolrDocument solrDocument : queryResponse.getResults())
					{
						Document document = this.convertDocument(solrDocument, fieldInfos, queryResponse.getHighlighting());
						documents.add(document);
						solrDocuments.add(solrDocument);
					}
				}

				numberOfResults = results.getNumFound();
			}

			target.setNumberOfResults(numberOfResults);
			target.setDocuments(documents);
			target.setSolrDocuments(solrDocuments);
		}
	}

	private void populateGroupedResults(final List<Document> documents, final List<SolrDocument> solrDocuments,
			final List<SearchResultGroup> searchResultGroups,
			final Map<String, FieldNameTranslator.FieldInfo> fieldInfos,
			final GroupCommand groupCommand, final int groupCommandIndex,
			final Map<String, Map<String, List<String>>> highlighting)
	{

		for (Group group : groupCommand.getValues())
		{
			List<Document> groupDocuments = new ArrayList<>();
			long groupDocumentIndex = 0L;
			SolrDocumentList groupResult = group.getResult();

			for (Iterator iterator = groupResult.iterator(); iterator.hasNext(); ++groupDocumentIndex)
			{
				SolrDocument solrGroupDocument = (SolrDocument) iterator.next();
				Document groupDocument = convertDocument(solrGroupDocument, fieldInfos, highlighting);
				groupDocuments.add(groupDocument);
				if (groupCommandIndex == 0 && groupDocumentIndex == 0L)
				{
					documents.add(groupDocument);
					solrDocuments.add(solrGroupDocument);
				}
			}

			SolrSearchResultGroup searchResultGroup = new SolrSearchResultGroup();
			searchResultGroup.setGroupValue(group.getGroupValue());
			searchResultGroup.setNumberOfResults(groupResult.getNumFound());
			searchResultGroup.setDocuments(groupDocuments);
			searchResultGroups.add(searchResultGroup);
		}

	}

	private String resolveFieldName(final String fieldName, final Map<String, FieldNameTranslator.FieldInfo> fieldInfos)
	{
		FieldNameTranslator.FieldInfo fieldInfo = fieldInfos.get(fieldName);
		return fieldInfo != null ? fieldInfo.getFieldName() : fieldName;
	}

	private Document convertDocument(final SolrDocument solrDocument,
			final Map<String, FieldNameTranslator.FieldInfo> fieldInfos,
			final Map<String, Map<String, List<String>>> highlighting)
	{
		DefaultDocument document = new DefaultDocument();
		Map<String, Object> documentFields = document.getFields();
		documentFields.putAll(solrDocument);
		this.replaceWithHighlightedFields(highlighting, documentFields);

		for (FieldNameTranslator.FieldInfo fieldInfo : fieldInfos.values())
		{
			Object fieldValue = documentFields.get(fieldInfo.getTranslatedFieldName());
			if (fieldValue != null)
			{
				documentFields.put(fieldInfo.getFieldName(), fieldValue);
				documentFields.remove(fieldInfo.getTranslatedFieldName());
			}
		}

		return document;
	}

	private void replaceWithHighlightedFields(final Map<String, Map<String, List<String>>> highlighting,
			final Map<String, Object> documentFields)
	{
		String id = (String) documentFields.get("id");
		if (!MapUtils.isEmpty(highlighting) && id != null)
		{
			Map<String, List<String>> highlightingForDoc = highlighting.get(id);
			if (!MapUtils.isEmpty(highlightingForDoc))
			{
				highlightingForDoc.forEach((key, value) ->
				{
					Object documentField = documentFields.get(key);
					if (documentField != null
							&& CollectionUtils.isNotEmpty(value))
					{
						if (documentField instanceof List)
						{
							//OOTB bug fix. If document field is a list that put highlights also as a list
							documentFields.put(key, new ArrayList<>((List) value));
						}
						else
						{
							documentFields.put(key, ((List) value).get(0));
						}
					}

				});
			}
		}
	}
}