package com.wiley.core.healthcheck;

public enum HealthCheckStatus
{
	OK,
	ERROR;
}