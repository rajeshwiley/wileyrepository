package com.wiley.core.healthcheck.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;
import com.wiley.core.healthcheck.HealthCheckStatus;
import com.wiley.core.healthcheck.WileyHealthCheckRegistry;
import com.wiley.core.healthcheck.WileyHealthCheckService;


public class WileyHealthCheckServiceImpl implements WileyHealthCheckService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyHealthCheckServiceImpl.class);

	private static final String STATUS_RESULT_KEY = "status";
	private static final String GIT_RESULT_KEY = "git";

	private WileyHealthCheckRegistry healthCheckRegistry;
	private Properties buildProperties;

	@Override
	public Map<String, Object> performInstanceHealthCheck()
	{
		Map<String, HealthCheck.Result> healthCheckResults = healthCheckRegistry.runHealthChecks();
		HealthCheckStatus aggregatedStatus = calculateAggregatedStatus(healthCheckResults.values());

		Map<String, Object> result = new LinkedHashMap<>();
		result.put(GIT_RESULT_KEY, buildProperties);
		result.putAll(toObjectMap(healthCheckResults));
		result.put(STATUS_RESULT_KEY, aggregatedStatus);

		return result;
	}

	private <T> Map<String, Object> toObjectMap(final Map<String, T> map)
	{
		Map<String, Object> objectMap = new HashMap<>();

		for (String key : map.keySet())
		{
			objectMap.put(key, map.get(key));
		}

		return objectMap;
	}

	private HealthCheckStatus calculateAggregatedStatus(final Collection<HealthCheck.Result> healthCheckResults)
	{
		HealthCheckStatus aggregatedStatus = HealthCheckStatus.OK;

		for (HealthCheck.Result healthCheckResult : healthCheckResults)
		{
			if (!healthCheckResult.isHealthy())
			{
				aggregatedStatus = HealthCheckStatus.ERROR;
				break;
			}
		}

		return aggregatedStatus;
	}

	@Override
	public Map<String, Object> getBuildDetails()
	{
		return Collections.singletonMap(GIT_RESULT_KEY, buildProperties);
	}

	@PostConstruct
	public void afterPropertiesSet()
	{
		buildProperties = new Properties();

		try
		{
			buildProperties.load(getClass().getResourceAsStream("/git_info.properties"));
		}
		catch (Exception e)
		{
			LOG.warn("Unable to read git build information file: {}", e.getMessage());
		}
	}

	public WileyHealthCheckRegistry getHealthCheckRegistry()
	{
		return healthCheckRegistry;
	}

	public void setHealthCheckRegistry(final WileyHealthCheckRegistry healthCheckRegistry)
	{
		this.healthCheckRegistry = healthCheckRegistry;
	}
}