package com.wiley.core.product.interceptors.freetrial;

import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.StringUtils;


/**
 * Contains common methods for validation {@link com.wiley.core.model.WileyFreeTrialProductModel}
 * and {@link com.wiley.core.model.WileyFreeTrialVariantProductModel}
 */
public abstract class AbstractWileyFreeTrialProductsValidatorInterceptor
{
	/**
	 * Return Boolean.TRUE when all fields are valid.
	 *
	 * @param pModel
	 * @return
	 */
	protected boolean validateCommonFields(final ProductModel pModel)
	{
		final boolean result = validateName(pModel)
				&& validateCode(pModel)
				&& validateIsbn(pModel)
				&& validateEditionFormat(pModel)
				&& validateCatalogVersion(pModel);

		return result;
	}

	protected boolean validateCatalogVersion(final ProductModel pModel)
	{
		return pModel.getCatalogVersion() != null;
	}

	protected boolean validateEditionFormat(final ProductModel pModel)
	{
		return pModel.getEditionFormat() != null;
	}

	protected boolean validateIsbn(final ProductModel pModel)
	{
		return !StringUtils.isBlank(pModel.getIsbn());
	}

	protected boolean validateCode(final ProductModel pModel)
	{
		return !StringUtils.isBlank(pModel.getCode());
	}

	protected boolean validateName(final ProductModel pModel)
	{
		return !StringUtils.isBlank(pModel.getName());
	}
}
