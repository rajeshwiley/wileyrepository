package com.wiley.core.i18n.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.i18n.WileyCountryService;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;


public class WileyCountryServiceImpl implements WileyCountryService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCountryServiceImpl.class);

	private String defaultCountryIsocode;

	@Autowired
	private CountryDao countryDao;

	@Autowired
	private SessionService sessionService;

	@Nullable
	@Override
	public CountryModel findCountryByCode(final String isoCode)
	{
		CountryModel countryModel = null;
		List<CountryModel> countryModels = countryDao.findCountriesByCode(isoCode);
		if (CollectionUtils.isNotEmpty(countryModels))
		{
			countryModel = countryModels.get(0);
		}
		return countryModel;
	}

	@Nonnull
	@Override
	public CountryModel useDefaultCountryAsCurrent()
	{
		CountryModel defaultCountry = findCountryByCode(defaultCountryIsocode);
		ServicesUtil.validateParameterNotNull(defaultCountry,
				String.format("Wrong config, not valid default country isocode [%s]", defaultCountryIsocode));
		sessionService.setAttribute(CURRENT_COUNTRY_SESSION_ATTR, defaultCountry);
		LOG.debug("Setting default  country as current [{}]", defaultCountry);
		return defaultCountry;
	}

	public void setDefaultCountryIsocode(final String defaultCountryIsocode)
	{
		this.defaultCountryIsocode = defaultCountryIsocode;
	}
}
