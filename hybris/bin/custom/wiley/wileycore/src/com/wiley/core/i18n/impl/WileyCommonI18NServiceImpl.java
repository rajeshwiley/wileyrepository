package com.wiley.core.i18n.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.i18n.daos.LanguageDao;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.Locale;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.countrystore.WileyCountryStoreConfigurationDao;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.model.WileyCountryStoreConfigurationModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;


public class WileyCommonI18NServiceImpl implements WileyCommonI18NService
{
	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private I18NService i18NService;

	@Resource
	private LanguageDao languageDao;

	@Resource
	private WileyCountryStoreConfigurationDao wileyCountryStoreConfigurationDao;

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public RegionModel getRegionForShortCode(final CountryModel countryModel, final String shortRegionCode)
	{
		validateParameterNotNull(countryModel, "country mustn't be null");
		validateParameterNotNull(shortRegionCode, "shortRegionCode mustn't be null");
		return commonI18NService.getRegion(countryModel, countryModel.getIsocode() + "-" + shortRegionCode);
	}

	@Override
	public boolean isValidLanguage(final String value)
	{
		validateParameterNotNull(value, "locale must not be null");
		final List<LanguageModel> language = languageDao.findLanguagesByCode(value);
		return !language.isEmpty();
	}

	@Override
	public CurrencyModel getDefaultCurrency(@Nonnull final CountryModel countryModel)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return getDefaultCurrency(countryModel, currentBaseStore);
	}

	@Override
	public CurrencyModel getDefaultCurrency(@Nonnull final CountryModel countryModel,
			@Nonnull final BaseStoreModel baseStoreModel)
	{
		final List<CurrencyModel> currencies = getCurrenciesForBaseStore(countryModel, baseStoreModel);
		return currencies.get(0);
	}

	@Override
	public List<RegionModel> getConfigurableRegionsForCurrentBaseStore(@Nonnull final CountryModel countryModel,
			@Nonnull final BaseStoreModel baseStoreModel)
	{
		validateParameterNotNull(countryModel, "country mustn't be null");
		validateParameterNotNull(baseStoreModel, "baseStore mustn't be null");

		final WileyCountryStoreConfigurationModel configuration = getWileyCountryStoreConfiguration(countryModel, baseStoreModel);
		final List<RegionModel> regions = configuration.getRegions();
		return regions;
	}

	@Override
	public Locale getLocaleByCountryIsoCodeWithCurrentLanguage(final String countryIso)
	{
		final String language = i18NService.getCurrentLocale().getLanguage();
		return StringUtils.isNotEmpty(countryIso) ? new Locale(language, countryIso) : new Locale(language);
	}

	private List<CurrencyModel> getCurrenciesForBaseStore(final CountryModel countryModel,
			final BaseStoreModel baseStoreModel)
	{
		validateParameterNotNull(countryModel, "country mustn't be null");
		validateParameterNotNull(baseStoreModel, "baseStore mustn't be null");

		WileyCountryStoreConfigurationModel configuration = getWileyCountryStoreConfiguration(countryModel, baseStoreModel);
		List<CurrencyModel> configurationCurrencies = configuration.getCurrencies();

		if (isEmpty(configurationCurrencies))
		{
			final String errorMessage = String.format(
					"Currencies not configured for country with isocode: %s, baseStore: %s",
					countryModel.getIsocode(), baseStoreModel.getUid());
			throw new IllegalStateException(errorMessage);
		}

		return configurationCurrencies;
	}

	private WileyCountryStoreConfigurationModel getWileyCountryStoreConfiguration(final CountryModel countryModel,
			final BaseStoreModel baseStoreModel)
	{
		validateParameterNotNull(countryModel, "country mustn't be null");
		validateParameterNotNull(baseStoreModel, "baseStore mustn't be null");

		final List<WileyCountryStoreConfigurationModel> configurations =
				wileyCountryStoreConfigurationDao.findConfigurationByCountryAndBasestore(countryModel, baseStoreModel);

		if (isEmpty(configurations))
		{
			final String errorMessage = String.format(
					"WileyCountryStoreConfiguration not found for country with isocode: %s, baseStore: %s",
					countryModel.getIsocode(), baseStoreModel.getUid());
			throw new IllegalStateException(errorMessage);
		}

		return configurations.get(0);
	}
}
