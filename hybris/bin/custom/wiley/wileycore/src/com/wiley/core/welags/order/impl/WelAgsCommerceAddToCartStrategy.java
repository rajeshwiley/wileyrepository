/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.welags.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.wiley.order.impl.WileyCommerceAddToCartStrategy;

import static java.util.Comparator.comparing;


/**
 * This strategy is a copy-paste version of DefaultCommerceAddToCartStrategy for WEL site.<br/>
 * TODO-REFACT-1808 Needs to be reviewed to match possible changes in newer Hybris version.
 *
 * @author Maksim_Kozich
 */
public class WelAgsCommerceAddToCartStrategy extends WileyCommerceAddToCartStrategy
{
	@Autowired
	private ConfiguratorSettingsService configuratorSettingsService;


	/**
	 * OOTB_CODE
	 * It's based on {@link DefaultCommerceAddToCartStrategy#doAddToCart(CommerceCartParameter)}.
	 * Modified behavior is based on replace cart entry logic.
	 * Changes are placed in {@link #saveCartEntry}
	 *
	 * Adds an item to the cart for pickup in a given location
	 *
	 * Do add to cart.
	 *
	 * @param parameter
	 * 		the parameter
	 * @return the commerce cart modification
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	@Override
	protected CommerceCartModification doAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		CommerceCartModification modification;

		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		final long quantityToAdd = parameter.getQuantity();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

		this.beforeAddToCart(parameter);
		validateAddToCart(parameter);

		if (isProductForCode(parameter).booleanValue())
		{
			// So now work out what the maximum allowed to be added is (note that this may be negative!)
			final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
					deliveryPointOfService);
			final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();
			final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
			final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

			if (actualAllowedQuantityChange > 0)
			{
				// We are allowed to add items to the cart
				final CartEntryModel entryModel = addCartEntry(parameter, actualAllowedQuantityChange);
				saveCartEntry(parameter, cartModel, entryModel);

				final String statusCode = getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
						quantityToAdd, cartLevelAfterQuantityChange);

				modification = createAddToCartResp(parameter, statusCode, entryModel, actualAllowedQuantityChange);
			}
			else
			{
				// Not allowed to add any quantity, or maybe even asked to reduce the quantity
				// Do nothing!
				final String status = getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, maxOrderQuantity);

				modification = createAddToCartResp(parameter, status, createEmptyCartEntry(parameter), 0);

			}
		}
		else
		{
			modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
					createEmptyCartEntry(parameter), 0);
		}

		return modification;
	}

	/**
	 * OOTB_CODE
	 * It's based on {@link DefaultCommerceAddToCartStrategy#addCartEntry(CommerceCartParameter, long)}.
	 * Modified behavior is based on replace cart entry logic.
	 * Changes are placed in {@link #addCartEntry(CommerceCartParameter, long, CartModel, ProductModel, boolean, UnitModel)}
	 *
	 * Adds an item to the cart for pickup in a given location
	 *
	 * @param parameter
	 * 		Cart parameters
	 * @return Cart modification information
	 * @throws de.hybris.platform.commerceservices.order.CommerceCartModificationException
	 */
	@Override
	protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
			throws CommerceCartModificationException
	{
		final ProductModel productModel = parameter.getProduct();
		final boolean forceNewEntry = parameter.isCreateNewEntry()
				|| CollectionUtils.isNotEmpty(configuratorSettingsService.getConfiguratorSettingsForProduct(productModel));

		if (parameter.getUnit() == null)
		{
			parameter.setUnit(getUnit(parameter));
		}

		final CartEntryModel cartEntryModel = addCartEntry(parameter, actualAllowedQuantityChange, parameter.getCart(),
				parameter.getProduct(), forceNewEntry, parameter.getUnit());
		cartEntryModel.setDeliveryPointOfService(parameter.getPointOfService());

		return cartEntryModel;
	}

	private void saveCartEntry(final CommerceCartParameter parameter, final CartModel cartModel,
			final CartEntryModel cartEntryModel)
	{
		if (parameter.isReplaceEntry())
		{   // when cart entry replaced  entries should be saved in reverse order
			cartModel.getEntries().stream()
					.sorted(comparing(AbstractOrderEntryModel::getEntryNumber).reversed())
					.forEach(entry -> getModelService().save(entry));
		}
		else
		{
			getModelService().save(cartEntryModel);
		}
	}

	private CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange,
			final CartModel cartModel, final ProductModel productModel, final boolean forceNewEntry,
			final UnitModel orderableUnit)
	{
		return getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange,
				orderableUnit,
				parameter.isReplaceEntry() ? (int) parameter.getEntryNumber() : APPEND_AS_LAST, !forceNewEntry);
	}

	/**
	 * OOTB_CODE
	 * It's based on {@link DefaultCommerceAddToCartStrategy#addToCart(CommerceCartParameter)}.
	 * The only diff is replace deprecated calculateCart method call.
	 *
	 * Adds an item to the cart for pickup in a given location
	 *
	 * @param parameter
	 * 		Cart parameters
	 * @return Cart modification information
	 * @throws de.hybris.platform.commerceservices.order.CommerceCartModificationException
	 */
	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final CommerceCartModification modification = doAddToCart(parameter);
		getCommerceCartCalculationStrategy().recalculateCart(parameter);
		afterAddToCart(parameter, modification);

		// TODO-REFACT-1808: Came from OOTB-1808
		mergeEntry(modification, parameter);

		return modification;
	}

	@Override
	protected void validateAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
	{
		super.validateAddToCart(parameters);
		if (parameters.getProduct() instanceof WileyGiftCardProductModel)
		{
			throw new CommerceCartModificationException("Gift Card Product cannot be added to cart");
		}
	}

	public ConfiguratorSettingsService getConfiguratorSettingsService()
	{
		return configuratorSettingsService;
	}

	public void setConfiguratorSettingsService(final ConfiguratorSettingsService configuratorSettingsService)
	{
		this.configuratorSettingsService = configuratorSettingsService;
	}
}