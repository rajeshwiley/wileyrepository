package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populator to fill BillTo address
 */
public class BillToWPGHttpRequestPopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	private static final String WPG_CUSTOM_BILLTO_TITLE_NAME = "WPG_CUSTOM_billto_title_name";
	private static final String WPG_CUSTOM_PAYMENT_TYPE = "WPG_CUSTOM_PAYMENT_TYPE";
	private static final String WPG_CUSTOM_BILLTO_FIRST_NAME = "WPG_CUSTOM_billto_first_name";
	private static final String WPG_CUSTOM_BILLTO_LAST_NAME = "WPG_CUSTOM_billto_last_name";
	private static final String WPG_CUSTOM_BILLTO_LINE1 = "WPG_CUSTOM_billto_line1";
	private static final String WPG_CUSTOM_BILLTO_LINE2 = "WPG_CUSTOM_billto_line2";
	private static final String WPG_CUSTOM_BILLTO_CITY = "WPG_CUSTOM_billto_city";
	private static final String WPG_CUSTOM_BILLTO_STATE_NAME = "WPG_CUSTOM_billto_state_name";
	private static final String WPG_CUSTOM_BILLTO_POSTAL_CODE = "WPG_CUSTOM_billto_postalcode";
	private static final String WPG_CUSTOM_BILLTO_COUNTRY_NAME = "WPG_CUSTOM_billto_country_name";

	@Override
	public void populate(final CreateSubscriptionRequest createSubscriptionRequest, final PaymentData paymentData)
			throws ConversionException
	{
		CustomerBillToData billTo = createSubscriptionRequest.getCustomerBillToData();
		addRequestQueryParam(paymentData,
				WPG_CUSTOM_PAYMENT_TYPE, createSubscriptionRequest.getPaymentInfoData().getDisplayName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_TITLE_NAME, billTo.getBillToTitleName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_FIRST_NAME, billTo.getBillToFirstName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_LAST_NAME, billTo.getBillToLastName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_LINE1, billTo.getBillToStreet1());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_LINE2, billTo.getBillToStreet2());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_CITY, billTo.getBillToCity());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_STATE_NAME, billTo.getBillToStateName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_POSTAL_CODE, billTo.getBillToPostalCode());
		addRequestQueryParam(paymentData, WPG_CUSTOM_BILLTO_COUNTRY_NAME, billTo.getBillToCountryName());
	}
}
