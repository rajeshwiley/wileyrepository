package com.wiley.core.wileyb2b.company.impl;

import de.hybris.platform.b2b.company.impl.DefaultB2BCommerceUnitService;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.company.CompanyB2BCommerceService;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalAddressService;

import java.util.Optional;


public class WileyB2BCommerceUnitServiceImpl extends DefaultB2BCommerceUnitService
		implements de.hybris.platform.b2b.company.B2BCommerceUnitService
{

	@Autowired
	private CompanyB2BCommerceService companyB2BCommerceService;


	@Autowired
	private WileyB2BUnitExternalAddressService wileyB2BUnitExternalAddressService;

	@Override
	public void saveAddressEntry(final B2BUnitModel unitModel, final AddressModel addressModel)
	{
		addressModel.setExternalId(wileyB2BUnitExternalAddressService.addShippingAddress(unitModel, addressModel));
		super.saveAddressEntry(unitModel, addressModel);
	}

	@Override
	public void removeAddressEntry(final String unitUid, final String addressId)
	{
		final B2BUnitModel unit = companyB2BCommerceService.getUnitForUid(unitUid);

		Optional<AddressModel> addressModel = unit.getAddresses()
				.stream()
				.filter(address -> StringUtils.equals(address.getPk().getLongValueAsString(), addressId))
				.findFirst();

		if (addressModel.isPresent()) {
			wileyB2BUnitExternalAddressService
					.deleteShippingAddress(unit, addressModel.get().getExternalId());
			super.removeAddressEntry(unitUid, addressId);
		}
	}

	@Override
	public void editAddressEntry(final B2BUnitModel unitModel, final AddressModel addressModel)
	{
		wileyB2BUnitExternalAddressService.updateShippingAddress(unitModel, addressModel);
		super.editAddressEntry(unitModel, addressModel);
	}
}
