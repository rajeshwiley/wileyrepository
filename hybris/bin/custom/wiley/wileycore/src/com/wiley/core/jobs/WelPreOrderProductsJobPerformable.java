package com.wiley.core.jobs;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WelPreOrderProductsCronJobModel;
import com.wiley.core.order.dao.WileyOrderDao;


public class WelPreOrderProductsJobPerformable extends AbstractJobPerformable<WelPreOrderProductsCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(WelPreOrderProductsJobPerformable.class);

	private static final String WEL_ORDER_PROCESS_DEFINITION_NAME = "wel-order-process";

	@Autowired
	private BusinessProcessService businessProcessService;

	@Autowired
	private WileyOrderDao wileyOrderDao;

	@Override
	public PerformResult perform(final WelPreOrderProductsCronJobModel cronJob)
	{
		LOG.info("Starting job for cron job " + cronJob.getCode());
		try
		{
			final List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();
			for (OrderModel order : result)
			{
				order.getOrderProcess().stream().forEach(op -> {
					if (op.getProcessDefinitionName().equals(WEL_ORDER_PROCESS_DEFINITION_NAME)) {
						final BusinessProcessEvent event = BusinessProcessEvent
								.builder(op.getCode() + "_waitForProductActivationOrCancel").build();
						businessProcessService.triggerEvent(event);
					}
				});
			}
			LOG.info("Cron job " + cronJob.getCode() + " finished.");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (Exception e)
		{
			LOG.error("Exception " + cronJob.getCode() + " cron job execution", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

	}
}
