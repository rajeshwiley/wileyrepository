package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;


public class AmountToPayCalculationStrategyImpl implements AmountToPayCalculationStrategy
{
	@Autowired
	private CommonI18NService commonI18NService;

	@Override
	public BigDecimal getOrderAmountToPay(final AbstractOrderModel orderModel)
	{
		Preconditions.checkState(orderModel.getCalculated(), "Order is not calculated");

		CurrencyModel currencyModel = orderModel.getCurrency();
		Double totalPrice = orderModel.getTotalPrice();
		Double totalTax = orderModel.getTotalTax();

		final int digits = currencyModel.getDigits();
		final double totalPriceWithTax = commonI18NService.roundCurrency(totalPrice
				+ totalTax, digits);
		return BigDecimal.valueOf(totalPriceWithTax);
	}
}
