package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductPricesValueResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import de.hybris.platform.solrfacetsearch.provider.RangeNameProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cProductPriceRangesValueResolver extends ProductPricesValueResolver
{
	private RangeNameProvider rangeNameProvider;
	private FieldNameProvider fieldNameProvider;

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final ProductModel model,
			final ValueResolverContext<Object, List<PriceInformation>> resolverContext) throws FieldValueProviderException
	{
		boolean hasPrice = false;

		final List<PriceInformation> priceInformations = getQualifierData(resolverContext);
		if (priceInformations != null)
		{
			final Double priceValue = getPriceValue(indexedProperty, priceInformations);
			if (priceValue != null)
			{
				hasPrice = true;
				Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier
						countryAndCurrencyQualifier =
						(Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier) getQualifier(
								resolverContext);
				final List<String> rangeNames = rangeNameProvider.getRangeNameList(indexedProperty, priceValue,
						countryAndCurrencyQualifier.getCurrency().getIsocode());
				final String qualifier = getFieldQualifier(resolverContext);
				final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty, qualifier);

				for (final String fieldName : fieldNames)
				{
					for (final String rangeName : rangeNames)
					{
						document.addField(fieldName, rangeName);
					}
				}
			}
		}

		if (!hasPrice)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	protected String getFieldQualifier(
			final ValueResolverContext<Object, List<PriceInformation>> resolverContext)
	{
		return resolverContext.getFieldQualifier();
	}

	protected Qualifier getQualifier(
			final ValueResolverContext<Object, List<PriceInformation>> resolverContext)
	{
		return resolverContext
				.getQualifier();
	}

	protected List<PriceInformation> getQualifierData(
			final ValueResolverContext<Object, List<PriceInformation>> resolverContext)
	{
		return resolverContext.getQualifierData();
	}



	@Required
	public void setRangeNameProvider(final RangeNameProvider rangeNameProvider)
	{
		this.rangeNameProvider = rangeNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
