package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer;

import de.hybris.platform.commerceservices.search.solrfacetsearch.indexer.listeners.CommerceSessionInitializationListener;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.media.MediaManager;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;

import org.springframework.beans.factory.annotation.Required;

/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyCommerceSessionInitializationListener extends CommerceSessionInitializationListener
{
	private PriceFactory priceFactory;

	private String solrIndexerUrlStrategyName;

	@Override
	protected void initializeSession(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
	{
		super.initializeSession(facetSearchConfig, indexedType);
		initializePriceFactory();
		initializeMediaUrlStrategy();
	}

	protected void initializeMediaUrlStrategy()
	{
		JaloSession.getCurrentSession().setAttribute(MediaManager.PREFERRED_URL_STRATEGY_ID, solrIndexerUrlStrategyName);
	}

	protected void initializePriceFactory()
	{
		JaloSession.getCurrentSession().setPriceFactory(priceFactory);
	}

	@Required
	public void setPriceFactory(final PriceFactory priceFactory)
	{
		this.priceFactory = priceFactory;
	}

	@Required
	public void setSolrIndexerUrlStrategyName(final String solrIndexerUrlStrategyName)
	{
		this.solrIndexerUrlStrategyName = solrIndexerUrlStrategyName;
	}
}
