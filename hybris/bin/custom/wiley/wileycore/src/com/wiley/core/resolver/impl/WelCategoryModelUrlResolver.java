package com.wiley.core.resolver.impl;

import com.wiley.core.category.WileyCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;
import java.util.Map;

/**
 * Creates a SEO friendly URL for catalog pages.
 */
public class WelCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver
{
	private static final String CATEGORY_PATH = "{category-path}";
	private static final String SUBCATEGORY_PATH = "{subcategory-path}";
	private static final String SLASH = "/";

	private Map<String, String> categoryPageUrlPatterns;

	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Override
	protected String resolveInternal(final CategoryModel category)
	{
		String resolvedUrl;
		String patternToUrl = categoryPageUrlPatterns.get(category.getCatalogVersion().getCatalog().getId());

		if (patternToUrl != null)
		{
			patternToUrl = resolveCategoryPath(category, patternToUrl);
			patternToUrl = resolveSubCategoryPath(category, patternToUrl);

			resolvedUrl = patternToUrl.toLowerCase();
		}
		else
		{
			resolvedUrl = super.resolveInternal(category);
		}

		return resolvedUrl;
	}

	private String resolveCategoryPath(final CategoryModel category, final String url)
	{
		CategoryModel rootCategory = getBaseSupercategory(category);
		String categoryPath = rootCategory.getSeoLabel() != null ? rootCategory.getSeoLabel() : rootCategory.getName();
		return url.replace(CATEGORY_PATH, urlSafe(categoryPath));
	}

	private CategoryModel getBaseSupercategory(final CategoryModel category)
	{
		CategoryModel primaryCategory = null;
		if (category instanceof VariantValueCategoryModel) {
			primaryCategory = wileyCategoryService.
					getPrimaryWileyCategoryForVariantCategory((VariantValueCategoryModel) category);
		}
		if (primaryCategory == null)
		{
			primaryCategory = getCategoryPath(category).get(0);
		}
		return primaryCategory;
	}

	private String resolveSubCategoryPath(final CategoryModel category, final String url)
	{
		String subcategoryPath = StringUtils.EMPTY;
		List<CategoryModel> categoryPath = getCategoryPath(category);
		if (categoryPath.size() > 1)
		{
			CategoryModel lastSubCategory = categoryPath.get(categoryPath.size() - 1);
			subcategoryPath =
					lastSubCategory.getSeoLabel() != null ? lastSubCategory.getSeoLabel() : lastSubCategory.getName();
			subcategoryPath = urlSafe(subcategoryPath) + SLASH;
		}
		return url.replace(SUBCATEGORY_PATH, subcategoryPath);
	}

	@Required
	public void setCategoryPageUrlPatterns(final Map<String, String> categoryPageUrlPatterns)
	{
		this.categoryPageUrlPatterns = categoryPageUrlPatterns;
	}
}