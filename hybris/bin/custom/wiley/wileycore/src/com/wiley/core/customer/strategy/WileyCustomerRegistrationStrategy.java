package com.wiley.core.customer.strategy;

import de.hybris.platform.core.model.user.CustomerModel;


public interface WileyCustomerRegistrationStrategy
{
	/**
	 * Register a customer with given parameters
	 *
	 * @param customerModel The to be registered customer's data
	 * @param password The to be registered customer's password
	 */
	boolean register(CustomerModel customerModel, String password);
}
