package com.wiley.core.integration.edi.strategy.impl;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.integration.edi.strategy.EdiProcessValidatingStrategy;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * Filters export processes for orders that were made and refunded the same reporting period
 */
public class RefundedTheSameDayProcessesValidationStrategy implements EdiProcessValidatingStrategy
{
	private static final Logger LOG = Logger.getLogger(RefundedTheSameDayProcessesValidationStrategy.class);

	//we can export refund or settle
	private static final int EXPORT_PROCESS_PERMITTED_TO_EXPORT = 1;
	//Only full refund is selected for export +  capture = 2 WileyExportProcessModel instances for
	// same order can be selected for export
	private static final int SETTLE_AND_REFUND_FOR_SAME_ORDER = 2;

	private static final Predicate<WileyExportProcessModel> IS_REFUND =
			entry -> entry.getExportType().equals(ExportProcessType.REFUND);
	private static final Predicate<WileyExportProcessModel> IS_SETTLE =
			entry -> entry.getExportType().equals(ExportProcessType.SETTLE);

	@Resource
	private WileyRefundService wileyRefundService;


	@Override
	public Collection<WileyExportProcessModel> getRejectedProcesses(final Collection<WileyExportProcessModel> exportProcesses)
	{
		List<WileyExportProcessModel> rejectedProcesses = new ArrayList<>();

		Map<OrderModel, List<WileyExportProcessModel>> mapOrderToWileyExportProcess =
				exportProcesses
						.stream()
						.collect(Collectors.groupingBy(WileyExportProcessModel::getOrder));

		mapOrderToWileyExportProcess.forEach((orderModel, wileyExportProcessList) -> {
			if (wileyExportProcessList.size() > EXPORT_PROCESS_PERMITTED_TO_EXPORT)
			{
				List<WileyExportProcessModel> refundWileyExportProcesses = wileyExportProcessList.stream().filter(
						IS_REFUND).collect(Collectors.toList());
				Optional<WileyExportProcessModel> settleWileyExportProcessModel = wileyExportProcessList.stream().filter(
						IS_SETTLE)
						.findFirst();
				if (!refundWileyExportProcesses.isEmpty() && settleWileyExportProcessModel.isPresent()
						&& wileyRefundService.isFullyRefundedOrder(orderModel))
				{
					final WileyExportProcessModel settlementProcess = settleWileyExportProcessModel.get();

					LOG.debug(String.format(
							"Filtering SETTLE + REFUND order processes pairs "
									+ "that were made during the same reporting period, order code: [%s]",
							settlementProcess.getOrder().getCode()));
					rejectedProcesses.addAll(refundWileyExportProcesses);
					rejectedProcesses.add(settlementProcess);
				}
				else
				{
					LOG.error(String.format("Could not find Refund and Settle for the order %s,"
									+ " though %s WileyExportProcessModel for this order were found",
							orderModel.getCode(), wileyExportProcessList.size()));
				}
			}
			if (wileyExportProcessList.size() > SETTLE_AND_REFUND_FOR_SAME_ORDER)
			{
				LOG.error(String.format("Found %s instances for  WileyExportProcessModel for order: %s, "
								+ "only %s WileyExportProcessModel were expected",
						wileyExportProcessList.size(), orderModel.getCode(), SETTLE_AND_REFUND_FOR_SAME_ORDER));
			}
		});

		return rejectedProcesses;
	}
}
