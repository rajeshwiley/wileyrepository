package com.wiley.core.integration.edi.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.CreditCardTypeCode;
import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.integration.ebp.enums.WileyShipMethod;
import com.wiley.core.payment.helpers.CreditCardTypeHelper;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.service.PurchaseOrderService;


/**
 * Service class to form view data from order model.
 * Used in populators.
 */
public class WileyPurchaseOrderService implements PurchaseOrderService
{
	private static final Logger LOG = Logger.getLogger(WileyPurchaseOrderService.class);
	public static final String CR = "CR";
	public static final String CC = "CC";

	private WileyShipMethod wileyShipMethod;

	@Resource
	protected PaymentTransactionService paymentTransactionService;

	@Override
	public String buildHybrisAuthorizationCode(final OrderModel orderModel)
	{
		PaymentTransactionEntryModel transactionEntryModel =
				paymentTransactionService.getAcceptedTransactionEntry(orderModel, PaymentTransactionType.AUTHORIZATION);
		if (transactionEntryModel == null)
		{
			LOG.warn("Cant find Authorization transaction for order: " + orderModel.getCode());
			return StringUtils.EMPTY;
		}
		String authCode = null;
		if (orderModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel) {
			authCode = transactionEntryModel.getWpgAuthCode();
		}
		else if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
			authCode = transactionEntryModel.getRequestId();
		}

		if (StringUtils.isBlank(authCode))
		{
			LOG.warn("Authorization code is blank for TransactionEntry: " + transactionEntryModel.getCode()
					+ ", Order: " + orderModel.getCode());
			return StringUtils.EMPTY;
		}
		return authCode;
	}


	@Override
	public String getCreditCardType(final CreditCardPaymentInfoModel paymentInfo)
	{
		final CreditCardTypeCode creditCardTypeCode = CreditCardTypeHelper.getCreditCardTypeCode(paymentInfo.getType());
		return creditCardTypeCode != null ? creditCardTypeCode.name() : StringUtils.EMPTY;
	}

	@Override
	public BigDecimal getSettleOrRefundTransactionAmount(final WileyExportProcessModel exportProcess)
	{
		OrderModel orderModel = exportProcess.getOrder();
		final boolean isOrderRefunded = isOrderRefunded(exportProcess);
		PaymentTransactionEntryModel transactionEntryModel = null;

		if (isOrderRefunded)
		{
			transactionEntryModel = exportProcess.getTransactionEntry();
		}
		else
		{
			transactionEntryModel = paymentTransactionService.getAcceptedTransactionEntry(orderModel,
					PaymentTransactionType.CAPTURE);
		}
		if (transactionEntryModel == null)
		{
			LOG.warn("Can't find settle or refund transaction for order: " + orderModel.getCode());
			return null;
		}
		return transactionEntryModel.getAmount();
	}


	@Override
	public String getCCToken(final OrderModel orderModel)
	{
		if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
			final PaypalPaymentInfoModel paypalPaymentInfoModel = (PaypalPaymentInfoModel) orderModel.getPaymentInfo();
			return paypalPaymentInfoModel.getToken(); //TODO: Add test
		}

		PaymentTransactionEntryModel transactionEntryModel =
				paymentTransactionService.getAcceptedTransactionEntry(orderModel, PaymentTransactionType.AUTHORIZATION);
		if (transactionEntryModel == null)
		{
			LOG.warn("Cant find Authorization transaction for order: " + orderModel.getCode());
			return StringUtils.EMPTY;
		}
		return transactionEntryModel.getRequestToken();
	}

	@Override
	public String getOrderType(final WileyExportProcessModel exportProcess)
	{
		final boolean isOrderRefunded = isOrderRefunded(exportProcess);
		return isOrderRefunded ? CR : CC;
	}

	@Override
	public boolean isOrderRefunded(final WileyExportProcessModel exportProcess)
	{
		return exportProcess.getExportType() == ExportProcessType.REFUND;
	}

	@Override
	public String buildTransactionId(final WileyExportProcessModel exportProcess)
	{
		final PaymentTransactionEntryModel transactionEntry = exportProcess.getTransactionEntry();
		if (transactionEntry != null)
		{
			return transactionEntry.getRequestId();
		}
		else
		{
			LOG.error("Export process [" + exportProcess.getCode() + "] doesn't have transaction entry attached");
		}
		return null;
	}

	public String buildShipMethod(@Nonnull final OrderModel orderModel)
	{
		return shipMethod(orderModel);
	}

	public String buildCreditCardExpiry(final CreditCardPaymentInfoModel paymentInfo)
	{
		String month = paymentInfo.getValidToMonth().length() == 1 ?
				"0" + paymentInfo.getValidToMonth() : paymentInfo.getValidToMonth();
		String year = paymentInfo.getValidToYear();
		if (year.length() > 2)
		{
			year = year.substring(2);
		}
		return month + year;
	}

	private String shipMethod(final OrderModel orderModel)
	{
		if (orderModel.getDeliveryMode() != null)
		{
			String code = orderModel.getDeliveryMode().getCode();
			if (code != null && WileyShipMethod.getByValue(code) != null)
			{
				return WileyShipMethod.getByValue(code).name();
			}
		}
		return WileyShipMethod.DONT.name();
	}
}