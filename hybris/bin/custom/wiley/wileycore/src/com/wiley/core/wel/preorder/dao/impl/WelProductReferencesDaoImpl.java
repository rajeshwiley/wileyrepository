package com.wiley.core.wel.preorder.dao.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.references.daos.impl.DefaultProductReferencesDao;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wiley.core.wel.preorder.dao.WelProductReferencesDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WelProductReferencesDaoImpl extends DefaultProductReferencesDao implements WelProductReferencesDao
{

	@Override
	public List<ProductModel> findSourceReferenceProducts(final ProductModel targetProduct,
			final ProductReferenceTypeEnum targetProductReferenceType)
	{
		validateParameterNotNull(targetProduct, "targetProduct must not be null!");
		validateParameterNotNull(targetProductReferenceType, "targetProductReferenceType must not be null!");

		StringBuilder query = new StringBuilder();
		query.append("SELECT {r.source} ");
		query.append("FROM ");
		query.append("{");
		query.append("ProductReference AS r");
		query.append("} ");
		query.append("WHERE ");
		query.append("{r.referenceType}=?targetProductReferenceType ");
		query.append("AND {r.target}=?targetProduct");

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("targetProductReferenceType", targetProductReferenceType);
		params.put("targetProduct", targetProduct);

		FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		flexibleSearchQuery.addQueryParameters(params);
		final List<ProductModel> result = getFlexibleSearchService().<ProductModel> search(flexibleSearchQuery).getResult();
		return result;
	}
}
