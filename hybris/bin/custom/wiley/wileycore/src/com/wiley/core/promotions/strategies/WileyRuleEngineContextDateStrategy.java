package com.wiley.core.promotions.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Date;


public interface WileyRuleEngineContextDateStrategy
{
	Date getRuleEngineContextDate(AbstractOrderModel order);
}
