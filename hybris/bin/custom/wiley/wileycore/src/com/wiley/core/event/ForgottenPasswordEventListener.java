/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.util.Assert;

import com.wiley.core.event.models.WileyForgottenPasswordEventModel;


/**
 * Listener for "forgotten password" functionality event.
 */
public class ForgottenPasswordEventListener extends AbstractWileySiteEventListener<WileyForgottenPasswordEventModel>
{
	/**
	 * On site event.
	 *
	 * @param event
	 * 		the event
	 */
	@Override
	protected void onSiteEvent(final WileyForgottenPasswordEventModel event)
	{
		final ForgottenPasswordProcessModel forgottenPasswordProcessModel =
				(ForgottenPasswordProcessModel) getBusinessProcessService().createProcess(
						"forgottenPassword-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"forgottenPasswordEmailProcess");
		Assert.hasLength(event.getCustomer().getContactEmail());
		Assert.hasLength(event.getCustomer().getDisplayName());
		forgottenPasswordProcessModel.setSite(event.getSite());
		forgottenPasswordProcessModel.setCustomer(event.getCustomer());
		forgottenPasswordProcessModel.setToken(event.getToken());
		forgottenPasswordProcessModel.setLanguage(event.getLanguage());
		forgottenPasswordProcessModel.setCurrency(event.getCurrency());
		forgottenPasswordProcessModel.setStore(event.getBaseStore());
		forgottenPasswordProcessModel.setRedirectType(event.getRedirectType());
		getModelService().save(forgottenPasswordProcessModel);
		getBusinessProcessService().startProcess(forgottenPasswordProcessModel);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final WileyForgottenPasswordEventModel event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.site", site);
		return SiteChannel.B2C.equals(site.getChannel()) || SiteChannel.B2B.equals(site.getChannel());
	}
}
