package com.wiley.core.wileyb2c.product.access.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.lightningsource.LightningSourceGateway;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductStrategy;


/**
 * Created by Georgii_Gavrysh on 9/8/2016.
 */
public class Wileyb2cLightningSourceDownloadDigitalStrategy implements Wileyb2cDownloadDigitalProductStrategy
{

	@Resource
	private LightningSourceGateway lightningSourceGateway;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private ConfigurationService configurationService;


	@Override
	public boolean apply(@Nonnull final OrderEntryModel orderEntry)
	{
		return WileyDigitalContentTypeEnum.LIGHTNING_SOURCE.equals(orderEntry.getProduct().getDigitalContentType());
	}

	@Override
	public String generateRedirectUrl(@Nonnull final OrderEntryModel orderEntry) throws Exception
	{
		final Optional<CountryModel> countryModelOptional = wileycomI18NService.getCurrentCountry();
		String countryIso = configurationService.getConfiguration().getString("default.country.isocode", "US");
		if (countryModelOptional.isPresent())
		{
			countryIso = countryModelOptional.get().getIsocode();
		}
		return lightningSourceGateway.getLink(countryIso, orderEntry);
	}
}
