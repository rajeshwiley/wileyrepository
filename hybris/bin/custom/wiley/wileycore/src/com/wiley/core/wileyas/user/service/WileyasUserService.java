package com.wiley.core.wileyas.user.service;

import de.hybris.platform.servicelayer.user.UserService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyasUserService extends UserService
{

	/**
	 * Checks is user of speciffic type exists
	 * @param uid - user uid
	 * @param ofType - Type of the user
	 * @return true if user exists
	 */
	boolean isUserExisting(String uid, Class ofType);
}
