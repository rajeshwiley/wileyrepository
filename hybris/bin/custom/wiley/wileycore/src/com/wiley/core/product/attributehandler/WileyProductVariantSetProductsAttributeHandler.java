package com.wiley.core.product.attributehandler;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.product.WileyProductVariantSetService;


/**
 * Dynamic attribute handler for {@link WileyProductVariantSetModel#getProducts()}.
 *
 * @author Aliaksei_Zlobich
 */
public class WileyProductVariantSetProductsAttributeHandler extends
		AbstractDynamicAttributeHandler<Collection<GenericVariantProductModel>, WileyProductVariantSetModel>
{

	@Resource
	private WileyProductVariantSetService wileyProductVariantSetService;

	@Override
	public Collection<GenericVariantProductModel> get(final WileyProductVariantSetModel model)
	{
		return getWileyProductVariantSetService().getSetEntries(model);
	}

	public WileyProductVariantSetService getWileyProductVariantSetService()
	{
		return wileyProductVariantSetService;
	}

	public void setWileyProductVariantSetService(final WileyProductVariantSetService wileyProductVariantSetService)
	{
		this.wileyProductVariantSetService = wileyProductVariantSetService;
	}
}
