package com.wiley.core.servicelayer.impl;

import de.hybris.platform.catalog.CatalogTypeService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import java.util.Collection;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.base.Preconditions;
import com.wiley.core.servicelayer.WileyItemService;
import com.wiley.core.servicelayer.dao.WileyItemDao;


public class WileyItemServiceImpl implements WileyItemService
{
	@Resource
	private WileyItemDao wileyItemDao;

	@Resource
	private CatalogTypeService catalogTypeService;

	@Nonnull
	public Collection<ItemModel> getCatalogAwareItems(@Nonnull final ComposedTypeModel type,
			@Nonnull final Collection<CatalogVersionModel> catalogVersions)
	{
		Preconditions.checkArgument(catalogTypeService.isCatalogVersionAwareType(type),
				"parameter type should be catalog-aware type");
		Preconditions.checkArgument(CollectionUtils.isNotEmpty(catalogVersions),
				"parameter catalogVersions should not be not empty");

		return wileyItemDao.findItems(type, catalogVersions);
	}
}
