package com.wiley.core.healthcheck;

import java.util.Set;

import javax.annotation.PostConstruct;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;


public class WileyHealthCheckRegistry extends HealthCheckRegistry
{
	private Set<HealthCheck> healthChecks;

	@PostConstruct
	public void registerHealthChecks()
	{
		for (HealthCheck healthCheck : healthChecks)
		{
			register(healthCheck.getClass().getName(), healthCheck);
		}
	}

	public Set<HealthCheck> getHealthChecks()
	{
		return healthChecks;
	}

	public void setHealthChecks(final Set<HealthCheck> healthChecks)
	{
		this.healthChecks = healthChecks;
	}
}