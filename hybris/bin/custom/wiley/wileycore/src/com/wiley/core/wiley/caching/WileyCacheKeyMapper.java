package com.wiley.core.wiley.caching;

import de.hybris.platform.regioncache.key.CacheKey;


public interface WileyCacheKeyMapper
{
	CacheKey map(Object key);
}
