package com.wiley.core.wileyb2c.order;

import java.util.Map;

import com.wiley.core.product.data.ExternalInventoryStatus;


/**
 * Created by Mikhail_Asadchy on 7/29/2016.
 */
public interface Wileyb2cExternalInventoryService
{
	/**
	 * Performs real-time inventory check
	 *
	 * @return - map of sapProductCodes and lists of their inventory statuses
	 */
	Map<String, ExternalInventoryStatus> doRealTimeInventoryCheck();
}
