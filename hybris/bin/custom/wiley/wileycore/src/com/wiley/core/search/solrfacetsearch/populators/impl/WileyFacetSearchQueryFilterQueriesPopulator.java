package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.wiley.core.search.client.solrj.WileySolrQuery;


/**
 * OOTB_CODE extracted
 */
public class WileyFacetSearchQueryFilterQueriesPopulator extends WileyAbstractFacetSearchQueryPopulator
{
	@Override
	public void populate(final SearchQueryConverterData source, final WileySolrQuery target)
			throws ConversionException
	{

		SearchQuery searchQuery = source.getSearchQuery();
		List<String> filterQueries = new ArrayList();
		this.addQueryFieldQueries(searchQuery, filterQueries);
		this.addRawQueries(searchQuery, filterQueries);
		Iterator var6 = filterQueries.iterator();

		while (var6.hasNext())
		{
			String filterQuery = (String) var6.next();
			target.addFilterQuery(new String[] { filterQuery });
		}

	}

	protected void addQueryFieldQueries(final SearchQuery searchQuery, final List<String> queries)
	{
		Iterator var4 = searchQuery.getFilterQueries().iterator();

		while (var4.hasNext())
		{
			QueryField filterQuery = (QueryField) var4.next();
			String query = this.convertQueryField(searchQuery, filterQuery);
			queries.add(query);
		}
	}

	protected void addRawQueries(final SearchQuery searchQuery, final List<String> queries)
	{
		Iterator var4 = searchQuery.getFilterRawQueries().iterator();

		while (var4.hasNext())
		{
			RawQuery filterRawQuery = (RawQuery) var4.next();
			String query = this.convertRawQuery(searchQuery, filterRawQuery);
			queries.add(query);
		}
	}
}
