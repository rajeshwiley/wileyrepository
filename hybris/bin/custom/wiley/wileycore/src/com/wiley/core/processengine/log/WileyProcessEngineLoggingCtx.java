package com.wiley.core.processengine.log;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.logging.ProcessEngineLoggingCtx;

import org.apache.commons.lang3.StringUtils;


public class WileyProcessEngineLoggingCtx extends ProcessEngineLoggingCtx
{

	private final int logMessageLength;

	public WileyProcessEngineLoggingCtx(final ProcessTaskModel task, final ModelService modelService, final int logMessageLength)
	{
		super(task, modelService);
		this.logMessageLength = logMessageLength;
	}

	@Override
	protected void saveAsTaskLogModel(final BusinessProcessModel process, final String loggedMessages)
	{
		super.saveAsTaskLogModel(process, StringUtils.left(loggedMessages, logMessageLength));
	}
}
