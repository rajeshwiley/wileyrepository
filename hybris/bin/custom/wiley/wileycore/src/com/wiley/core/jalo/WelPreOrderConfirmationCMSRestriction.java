package com.wiley.core.jalo;


import de.hybris.platform.jalo.SessionContext;


public class WelPreOrderConfirmationCMSRestriction extends GeneratedWelPreOrderConfirmationCMSRestriction
{
	@Override
	public String getDescription(final SessionContext sessionContext)
	{
		return "Pre-order confirmation message is restricted!";
	}
}
