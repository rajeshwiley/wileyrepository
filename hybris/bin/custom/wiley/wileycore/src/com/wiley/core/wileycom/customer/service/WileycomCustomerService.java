package com.wiley.core.wileycom.customer.service;

import de.hybris.platform.core.model.user.CustomerModel;

public interface WileycomCustomerService
{
	/**
	 * Register a customer with given parameters
	 *
	 * @param customerModel
	 *          The to be registered customer's data
	 * @param password
	 *          The to be registered customer's password
	 */
	boolean register(CustomerModel customerModel, String password);
}
