package com.wiley.core.wileyb2c.product.access.impl;

import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.Security;
import java.util.Date;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jets3t.service.CloudFrontService;
import org.jets3t.service.utils.ServiceUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductStrategy;


/**
 * Generating signed url for secured access to digital product from Amazon CloudFront
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cAmazonDownloadDigitalStrategy implements Wileyb2cDownloadDigitalProductStrategy
{
	private String distributionDomain;
	private String privateKeyFilePath;
	private String keyPairId;
	private Long digitalProductAmazonS3LinkExpirationTime;
	private byte[] derPrivateKey;

	@PostConstruct
	public void init() throws IOException
	{
		derPrivateKey = ServiceUtils.readInputStreamToBytes(new FileInputStream(privateKeyFilePath));
		Security.addProvider(new BouncyCastleProvider());
	}

	@Override
	public boolean apply(@Nonnull final OrderEntryModel orderEntry)
	{
		return WileyDigitalContentTypeEnum.WILEY_DOWNLOAD.equals(orderEntry.getProduct().getDigitalContentType());
	}

	@Override
	public String generateRedirectUrl(@Nonnull final OrderEntryModel orderEntry) throws Exception
	{
		ProductModel product = orderEntry.getProduct();
		Assert.notNull(product.getDigitalContentPath(),
				String.format("Digital content path is not defined. Could not access product %s.", product.getCode()));
		Date expirationDate = new Date();
		long expirationTime = expirationDate.getTime();
		expirationTime += digitalProductAmazonS3LinkExpirationTime;
		expirationDate.setTime(expirationTime);
		return CloudFrontService.signUrlCanned("http://" + distributionDomain + "/" + product.getDigitalContentPath(),
				keyPairId, derPrivateKey, expirationDate);
	}

	@Required
	public void setDistributionDomain(final String distributionDomain)
	{
		this.distributionDomain = distributionDomain;
	}

	@Required
	public void setPrivateKeyFilePath(final String privateKeyFilePath)
	{
		this.privateKeyFilePath = privateKeyFilePath;
	}

	@Required
	public void setKeyPairId(final String keyPairId)
	{
		this.keyPairId = keyPairId;
	}

	@Required
	public void setDigitalProductAmazonS3LinkExpirationTime(final Long digitalProductAmazonS3LinkExpirationTime)
	{
		this.digitalProductAmazonS3LinkExpirationTime = digitalProductAmazonS3LinkExpirationTime;
	}
}
