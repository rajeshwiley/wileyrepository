package com.wiley.core.product.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyPurchaseOptionProductModel;

import static com.wiley.core.enums.ProductEditionFormat.PHYSICAL;


public class WileyPurchaseOptionProductCountablePrepareInterceptor
		implements PrepareInterceptor<WileyPurchaseOptionProductModel>
{
	private static final String WILEY_PRODUCT_CATALOG_ID = "wileyProductCatalog";

	@Override
	public void onPrepare(final WileyPurchaseOptionProductModel wileyPurchaseOptionVariantProduct,
			final InterceptorContext interceptorContext)
	{
		final String productCatalogVersionId = wileyPurchaseOptionVariantProduct.getCatalogVersion()
				.getCatalog().getId();

		if (productCatalogVersionId.equals(WILEY_PRODUCT_CATALOG_ID))
		{
			final ProductEditionFormat editionFormat = wileyPurchaseOptionVariantProduct.getEditionFormat();
			wileyPurchaseOptionVariantProduct.setCountable(PHYSICAL.equals(editionFormat));
		}
	}
}
