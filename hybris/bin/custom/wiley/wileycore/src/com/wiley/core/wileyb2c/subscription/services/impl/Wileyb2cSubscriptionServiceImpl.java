package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbB2CSubscriptionGateway;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.order.dao.WileyOrderEntryDao;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.core.subscription.services.impl.WileySubscriptionServiceImpl;
import com.wiley.core.wileyb2c.subscription.dao.Wileyb2cSubscriptionDao;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;

import static de.hybris.platform.core.enums.OrderStatus.CANCELLED;
import static de.hybris.platform.core.enums.OrderStatus.CHECKED_INVALID;
import static de.hybris.platform.core.enums.OrderStatus.FAILED;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.text.MessageFormat.format;
import static org.springframework.util.Assert.notNull;


public class Wileyb2cSubscriptionServiceImpl extends WileySubscriptionServiceImpl implements Wileyb2cSubscriptionService
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cSubscriptionServiceImpl.class);

	@Resource
	private EsbB2CSubscriptionGateway esbB2CSubscriptionGateway;

	@Resource
	private ModelService modelService;

	@Resource
	private Wileyb2cSubscriptionDao wileyb2cSubscriptionDao;

	@Resource
	private WileyOrderEntryDao wileyOrderEntryDao;

	@Override
	public void autorenewSubscription(@Nonnull final WileySubscriptionModel subscription, final boolean requireRenewal)
	{
		try
		{
			final String externalCode = subscription.getExternalCode();
			Assert.notNull(externalCode);
			esbB2CSubscriptionGateway.autorenewSubscription(externalCode, requireRenewal);
			subscription.setRequireRenewal(requireRenewal);
			modelService.save(subscription);
		}
		catch (final ExternalSystemException e)
		{
			final String message = String.format("Autorenewal for subscription %s with external code %s is not updated",
					subscription.getCode(), subscription.getExternalCode());
			LOG.error(message, e);
			throw new SubscriptionUpdateException(message, e);
		}
	}

	@Override
	public void updateDeliveryAddress(@Nonnull final WileySubscriptionModel subscription,
			@Nonnull final AddressModel deliveryAddress) throws SubscriptionUpdateException
	{
		notNull(subscription);
		notNull(deliveryAddress);

		AddressModel previousSubscriptionAddress = subscription.getDeliveryAddress();
		AddressModel subscriptionAddress = modelService.clone(deliveryAddress);
		subscriptionAddress.setOwner(subscription);
		subscription.setDeliveryAddress(subscriptionAddress);

		try
		{
			esbB2CSubscriptionGateway.updateDeliveryAddress(subscription.getExternalCode(), deliveryAddress);
		}
		catch (ExternalSystemException e)
		{
			throw new SubscriptionUpdateException("External subscription delivery address update process failed!", e);
		}

		modelService.saveAll(subscription, subscriptionAddress);

		if (previousSubscriptionAddress != null)
		{
			modelService.remove(previousSubscriptionAddress);
		}
	}

	@Override
	public boolean isCustomerHasSubscription(final WileyProductModel product, final CustomerModel customer)
	{
		validateParameterNotNull(product, "product must not be null!");
		validateParameterNotNull(customer, "customer must not be null!");

		if (isProductSubscription(product))
		{
			final String productCode = product.getCode();
			final String customerUid = customer.getUid();
			final OrderStatus[] orderStatusesToBeExcluded = { CANCELLED, FAILED, CHECKED_INVALID };

			List<WileySubscriptionModel> subscriptions = wileyb2cSubscriptionDao.findSubscriptionsByProductAndCustomer(
					productCode, customerUid);
			List<OrderEntryModel> subscriptionOrderEntries =
					wileyOrderEntryDao.findOrderEntriesByProductAndOwnerExcludingStatuses(
							productCode, customerUid, orderStatusesToBeExcluded);
			return !subscriptions.isEmpty() || !subscriptionOrderEntries.isEmpty();
		}
		else
		{
			throw new IllegalArgumentException(
					format("Product with code {0} has subtype different from SUBSCRIPTION or no subscription terms",
							product.getCode()));
		}
	}

	@Override
	public boolean isValidSubscriptionTermForProduct(final WileyProductModel productModel,
			final SubscriptionTermModel orderEntrySubscriptionTerm)
	{
		Collection<SubscriptionTermModel> productSubscriptionTerms = productModel.getSubscriptionTerms();

		return isProductSubscription(productModel)
				&& productSubscriptionTerms
				.stream()
				.anyMatch(
						productSubscriptionTerm -> StringUtils
								.equals(productSubscriptionTerm.getId(), orderEntrySubscriptionTerm.getId()));
	}
}
