package com.wiley.core.welags.assistedservice.impl;

import de.hybris.platform.assistedserviceservices.exception.AssistedServiceCartBindException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.assistedserviceservices.impl.DefaultAssistedServiceService;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;


public class WelAgsAssistedServiceService extends DefaultAssistedServiceService
{
	@Resource(name = "welAgsExternalTaxesService")
	private ExternalTaxesService externalTaxesService;

	@Override
	public void bindCustomerToCart(final String customerId, final String cartId) throws AssistedServiceException
	{
		super.bindCustomerToCart(customerId, cartId);
		final CartModel cart = getCartService().getSessionCart();
		if (cart == null)
		{
			throw new AssistedServiceCartBindException(String.format("Cart not found for current customer [%s]", customerId));
		}
		externalTaxesService.calculateExternalTaxes(cart);
	}
}
