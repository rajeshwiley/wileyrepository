package com.wiley.core.regCode.service.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.OrderType;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.integration.esb.RegistrationCodeGateway;
import com.wiley.core.regCode.exception.AbstractRegCodeOperationException;
import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.core.regCode.service.Wileyb2cRegCodeService;
import com.wiley.core.cart.WileyCartFactory;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation of {@link Wileyb2cRegCodeService}
 */
public class Wileyb2cRegCodeServiceImpl implements Wileyb2cRegCodeService
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cRegCodeServiceImpl.class);

	@Resource(name = "wileyCartFactory")
	private WileyCartFactory cartFactory;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private CommerceCheckoutService commerceCheckoutService;

	@Autowired
	private CalculationService calculationService;

	private CartValidationStrategy cartValidationStrategy;

	@Autowired
	private RegistrationCodeGateway registrationCodeGateway;

	@Override
	public CartModel validateRegCode(@Nonnull final String regCode)
	{
		validateParameterNotNull(regCode, "RegCode cannot be null.");

		final CartModel cartModel = cartFactory.createCartWithType(OrderType.REGISTRATION_CODE_ACIVATION);
		CommerceCartParameter cartParameter = new CommerceCartParameter();
		cartParameter.setCart(cartModel);
		validateInExternalSystem(cartModel, regCode, cartParameter);
		return cartModel;
	}

	@Override
	public CartModel validateRegCodeForProduct(@Nonnull final String regCode, @Nonnull final String productCode)
	{
		validateParameterNotNullStandardMessage("regCode", regCode);
		validateParameterNotNullStandardMessage("productCode", productCode);


		CartModel cartModel = cartFactory.createCartWithType(OrderType.REGISTRATION_CODE_ACIVATION);
		CommerceCartParameter cartParameter = new CommerceCartParameter();
		cartParameter.setCart(cartModel);
		cartParameter.setProductCodeForRegcode(productCode);
		validateInExternalSystem(cartModel, regCode, cartParameter);
		return cartModel;
	}

	private void validateInExternalSystem(final CartModel cartModel, final String regCode,
			final CommerceCartParameter parameterForCartValidation)
	{
		if (cartModel == null)
		{
			throw new RegCodeValidationException("Cart model for RegCode cannot be created");
		}
		try
		{
			registrationCodeGateway.validateRegCode(regCode, cartModel);
			cartValidationStrategy.validateCart(parameterForCartValidation);

			// SV: PLease beware, that complete recalculation will clear this discount, as there is no promotion behind
			calculationService.calculateTotals(cartModel, true);
		}
		catch (AbstractRegCodeOperationException ex)
		{
			removeCart(cartModel);
			throw ex;
		}
		catch (ExternalSystemNotFoundException ex)
		{
			removeCart(cartModel);
			throw new RegCodeInvalidException("Passed registration code does not exist", ex);
		}
		catch (CalculationException | ExternalSystemException ex)
		{
			removeCart(cartModel);
			throw new RegCodeValidationException("Cannot validate registration code", ex);
		}
	}

	@Override
	public Optional<String> getRegCodeForOrder(final AbstractOrderModel abstractOrderModel)
	{
		return Optional.ofNullable(abstractOrderModel.getRegistrationCode());
	}

	@Required
	public void setCartValidationStrategy(final CartValidationStrategy cartValidationStrategy)
	{
		this.cartValidationStrategy = cartValidationStrategy;
	}

	/** Mask exception when cart is removed not to interrupt successful order placement */
	private void removeCart(final CartModel cartModel)
	{
		try
		{
			modelService.remove(cartModel);
		}
		catch (Exception ex)
		{
			LOG.warn("Error when removing registrationcode cart", ex);
		}
	}

}
