package com.wiley.core.integration.handlers;

import javax.annotation.Nonnull;

import org.springframework.messaging.support.ErrorMessage;
import org.springframework.util.Assert;


/**
 * Class is used to return ErrorMessage back to gateway.
 */
public class CommonGatewayErrorHandler
{

	/**
	 * Method return received ErrorMessage back.
	 *
	 * @param errorMessage
	 * 		error message with exception in payload.
	 */
	@Nonnull
	public ErrorMessage onError(@Nonnull final ErrorMessage errorMessage)
	{
		Assert.notNull(errorMessage);
		Assert.notNull(errorMessage.getPayload());

		return errorMessage;
	}

}
