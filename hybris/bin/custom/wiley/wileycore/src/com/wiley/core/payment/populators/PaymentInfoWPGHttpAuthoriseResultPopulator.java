package com.wiley.core.payment.populators;

import com.wiley.core.payment.enums.WPGCreditCardType;
import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.log4j.Logger;

import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class PaymentInfoWPGHttpAuthoriseResultPopulator
		extends AbstractResultPopulator<Map<String, String>, CreateSubscriptionResult>
{
	private static final int STRING_START = 0;
	private static final int EXPIRY_MONTH_LENGTH = 2;
	private static final int YEARS_TO_ADD = 2000;

	private static final Logger LOG = Logger.getLogger(PaymentInfoWPGHttpAuthoriseResultPopulator.class);

	@Override
	public void populate(final Map<String, String> wpgReponseParameters, final CreateSubscriptionResult createSubscriptionResult)
			throws ConversionException
	{
		if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(createSubscriptionResult.getDecision()))
		{
			validateParameterNotNull(createSubscriptionResult,
					"Parameter [CreateSubscriptionResult] createSubscriptionResult cannot be null");
			final WPGHttpValidateResultData wpgResultInfoData = createSubscriptionResult.getWpgResultInfoData();
			validateParameterNotNull(wpgResultInfoData, "Parameter [wpgResultInfoData] wpgResultInfoData cannot be null");

			final PaymentInfoData paymentInfoData = new PaymentInfoData();

			populateCardType(wpgResultInfoData, paymentInfoData);

			final String cardExpiry = wpgResultInfoData.getCardExpiry();
			validateParameterNotNull(cardExpiry, "Invalid WPG HTTP Validate response. cardExpiry is null");

			final String expiryMonth = cardExpiry.substring(STRING_START, EXPIRY_MONTH_LENGTH);
			paymentInfoData.setCardExpirationMonth(getIntegerForString(expiryMonth));

			final String expiryYear = cardExpiry.substring(EXPIRY_MONTH_LENGTH);
			paymentInfoData.setCardExpirationYear(Integer.valueOf(getIntegerForString(expiryYear).intValue() + YEARS_TO_ADD));

			final String maskedCardNumber = wpgResultInfoData.getMaskedCardNumber();
			validateParameterNotNull(maskedCardNumber, "Invalid WPG HTTP Validate response. maskedCardNumber is null");
			paymentInfoData.setCardAccountNumber(maskedCardNumber);

			createSubscriptionResult.setPaymentInfoData(paymentInfoData);

			final SubscriptionInfoData subscriptionInfoData = new SubscriptionInfoData();

			validateParameterNotNull(wpgResultInfoData.getToken(), "Invalid WPG HTTP Validate response. token is null");
			subscriptionInfoData.setSubscriptionID(wpgResultInfoData.getToken());
			createSubscriptionResult.setSubscriptionInfoData(subscriptionInfoData);
		}
	}

	private void populateCardType(final WPGHttpValidateResultData wpgResultInfoData, final PaymentInfoData paymentInfoData)
	{
		validateParameterNotNull(wpgResultInfoData.getAcquirerID(), "Invalid WPG HTTP Validate response. acquirerID is null");
		final WPGCreditCardType wpgCreditCardType = WPGCreditCardType.get(wpgResultInfoData.getAcquirerID());

		if (wpgCreditCardType == null)
		{
			LOG.error("Cannot find WPGCreditCardType matching acquirer ID: " + wpgResultInfoData.getAcquirerID());
		}
		else
		{
			final String creditCardTypeCode = wpgCreditCardType.toString().toLowerCase();
			paymentInfoData.setCardCardType(CardTypeEnum.valueOf(creditCardTypeCode).getStringValue());
		}
	}
}
