/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.util;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.Registry;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Helper bean for generating the MCC site links for the supported websites
 */
public class MccSiteUrlHelper
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(MccSiteUrlHelper.class);

	/**
	 * Gets all sites and urls.
	 *
	 * @return the all sites and urls
	 */
	// Called from BeanShell by MCC
	public static Map<String, String> getAllSitesAndUrls()
	{
		final MccSiteUrlHelper mccSiteUrlHelper = Registry.getApplicationContext().getBean("mccSiteUrlHelper",
				MccSiteUrlHelper.class);
		return mccSiteUrlHelper.getSitesAndUrls();
	}

	private CMSSiteService cmsSiteService;
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	/**
	 * Gets cms site service.
	 *
	 * @return the cms site service
	 */
	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}

	/**
	 * Sets cms site service.
	 *
	 * @param cmsSiteService
	 * 		the cms site service
	 */
	@Required
	public void setCmsSiteService(final CMSSiteService cmsSiteService)
	{
		this.cmsSiteService = cmsSiteService;
	}

	/**
	 * Gets site base url resolution service.
	 *
	 * @return the site base url resolution service
	 */
	protected SiteBaseUrlResolutionService getSiteBaseUrlResolutionService()
	{
		return siteBaseUrlResolutionService;
	}

	/**
	 * Sets site base url resolution service.
	 *
	 * @param siteBaseUrlResolutionService
	 * 		the site base url resolution service
	 */
	@Required
	public void setSiteBaseUrlResolutionService(final SiteBaseUrlResolutionService siteBaseUrlResolutionService)
	{
		this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
	}

	/**
	 * Gets sites and urls.
	 *
	 * @return the sites and urls
	 */
	public Map<String, String> getSitesAndUrls()
	{
		final Map<String, String> siteToUrl = new TreeMap<String, String>();

		for (final CMSSiteModel cmsSiteModel : getCmsSiteService().getSites())
		{
			final String url = getSiteUrl(cmsSiteModel);
			if (url != null && !url.isEmpty() && SiteChannel.B2C.equals(cmsSiteModel.getChannel()))
			{
				siteToUrl.put(cmsSiteModel.getName(), url);
			}
		}

		return siteToUrl;
	}

	/**
	 * Gets site url.
	 *
	 * @param cmsSiteModel
	 * 		the cms site model
	 * @return the site url
	 */
	protected String getSiteUrl(final CMSSiteModel cmsSiteModel)
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(cmsSiteModel, false, "/");
	}
}
