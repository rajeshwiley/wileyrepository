package com.wiley.core.wileyb2b.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


/**
 * Defines actions specific to B2B AddToCartStrategy
 */
public interface Wileyb2bCommerceAddToCartStrategy
{
	void addAllEntriesToCart(CartModel fromCartModel, CartModel toCartModel,
													List<CommerceCartModification> modifications)
			throws CommerceCartModificationException;
}
