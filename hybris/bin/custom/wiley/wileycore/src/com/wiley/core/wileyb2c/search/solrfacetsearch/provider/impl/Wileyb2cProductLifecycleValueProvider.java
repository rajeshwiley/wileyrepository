package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;

import java.util.Collections;
import java.util.List;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 */
public class Wileyb2cProductLifecycleValueProvider extends AbstractWileycomValueProvider<String>
{

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
	{
		if (model instanceof ProductModel)
		{
			final ProductModel productModel = (ProductModel) model;
			WileyProductLifecycleEnum lifecycleStatus = productModel.getLifecycleStatus();
			if (lifecycleStatus != null)
			{
				return Collections.singletonList(lifecycleStatus.getCode());
			}
		}
		return Collections.emptyList();
	}

}
