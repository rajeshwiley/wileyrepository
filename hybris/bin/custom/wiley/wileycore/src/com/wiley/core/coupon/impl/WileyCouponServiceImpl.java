package com.wiley.core.coupon.impl;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponservices.services.impl.DefaultCouponService;

import com.wiley.core.coupon.WileyCouponService;


/**
 * Created by Sergiy_Mishkovets on 10/11/2017.
 */
public class WileyCouponServiceImpl extends DefaultCouponService implements WileyCouponService
{
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Override
	protected void recalculateOrder(final AbstractOrderModel order)
	{
		if (order instanceof CartModel)
		{
			final CommerceCartParameter parameter = new CommerceCartParameter();
			parameter.setEnableHooks(true);
			parameter.setCart((CartModel) order);
			parameter.setVoucherApplied(true);
			commerceCartCalculationStrategy.recalculateCart(parameter);
		}
		else
		{
			super.recalculateOrder(order);
		}
	}

	@Override
	public void releaseAllCoupons(final AbstractOrderModel order)
	{
		order.getAppliedCouponCodes()
				.stream()
				.forEach(couponCode -> releaseCouponCode(couponCode, order));
	}

	public CommerceCartCalculationStrategy getCommerceCartCalculationStrategy()
	{
		return commerceCartCalculationStrategy;
	}

	public void setCommerceCartCalculationStrategy(
			final CommerceCartCalculationStrategy commerceCartCalculationStrategy)
	{
		this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
	}
}
