package com.wiley.core.externaltax.dto;

public class TaxCalculationRequestEntryDto
{
	private String id;
	private Double amount;
	private String codeType;
	private String code;
	private String productType;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public Double getAmount()
	{
		return amount;
	}

	public void setAmount(final Double amount)
	{
		this.amount = amount;
	}

	public String getCodeType()
	{
		return codeType;
	}

	public void setCodeType(final String codeType)
	{
		this.codeType = codeType;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getProductType()
	{
		return productType;
	}

	public void setProductType(final String productType)
	{
		this.productType = productType;
	}
}
