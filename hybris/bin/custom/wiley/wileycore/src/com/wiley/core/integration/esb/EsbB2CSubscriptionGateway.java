package com.wiley.core.integration.esb;

import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface EsbB2CSubscriptionGateway
{
	String EXTERNAL_SUBSCRIPTION = "header_subscription";
	String SUBSCRIPTION_ID = "subscriptionId";
	String EXTERNAL_CODE = "externalCode";
	
	/**
	 * Send update to SAP ERP that customer enabled/disabled auto-renewal.
	 *
	 * @param externalCode
	 *          The universally unique identifier provided by SAP ERP.
	 * @param autorenew
	 *          The auto-renew flag value for the subscription.
	 */
	void autorenewSubscription(@Header(EXTERNAL_CODE) String externalCode, @Payload boolean autorenew);

	/**
	 * Executes call to change B2C subscription`s payment method into external
	 * systems. In case of inconsistent address or ESB failure
	 * SubscriptionUpdateException should be caught
	 *
	 * @param paymentInfoModel
	 *          - payment method which will be set
	 * @param externalCode
	 *          - external subscription code
	 */
	void changePaymentMethod(@Nonnull @Payload PaymentInfoModel paymentInfoModel,
			@Nonnull @Header(EXTERNAL_CODE) String externalCode);

	/**
	 * Updates subscription delivery address based on the given subscription id and delivery address.
	 * @param subscriptionId the external subscription code
	 * @param deliveryAddress the updated delivery address
	 */
	void updateDeliveryAddress(@Nonnull @Header(SUBSCRIPTION_ID) String subscriptionId,
			@Nonnull @Payload AddressModel deliveryAddress);
}
