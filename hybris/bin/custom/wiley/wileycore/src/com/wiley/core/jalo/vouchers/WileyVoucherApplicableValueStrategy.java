package com.wiley.core.jalo.vouchers;

import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.voucher.jalo.util.VoucherValue;


public class WileyVoucherApplicableValueStrategy
{
	public VoucherValue getApplicableValue(final AbstractOrder anOrder, final VoucherValue voucherValue)
	{
		VoucherValue voucherValueResult = voucherValue;
		if (voucherValue.getValue() > anOrder.getTotalPrice())
		{
			voucherValueResult = new VoucherValue(anOrder.getTotalPrice(), voucherValue.getCurrency());
		}
		return voucherValueResult;
	}
}
