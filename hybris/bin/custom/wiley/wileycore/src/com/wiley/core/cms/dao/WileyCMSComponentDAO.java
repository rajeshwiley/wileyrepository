package com.wiley.core.cms.dao;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.List;


/**
 * Created by Anton_Lukyanau on 1/4/2017.
 */
public interface WileyCMSComponentDAO
{
	List<AbstractCMSComponentModel> findNotSyncComponentsForPage(AbstractPageModel componentModel);
}
