package com.wiley.core.integration.ebp.dto;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * Object for passing data from action to EPB transformer
 */
public class EbpCreateUserPayload
{
	private CustomerModel customer;

	private String university;
	
	private String universityState;
	
	private String universityCountry;

	private String oldEmailAddress;

	private OrderModel order;
	

	public String getOldEmailAddress()
	{
		return oldEmailAddress;
	}

	public void setOldEmailAddress(final String oldEmailAddress)
	{
		this.oldEmailAddress = oldEmailAddress;
	}

	public String getUniversity()
	{
		return university;
	}

	public void setUniversity(final String university)
	{
		this.university = university;
	}

	public void setCustomer(final CustomerModel customer)
	{

		this.customer = customer;
	}

	public CustomerModel getCustomer()
	{

		return customer;
	}

	public String getUniversityState()
	{
		return universityState;
	}

	public void setUniversityState(final String universityState)
	{
		this.universityState = universityState;
	}

	public String getUniversityCountry()
	{
		return universityCountry;
	}

	public void setUniversityCountry(final String universityCountry)
	{
		this.universityCountry = universityCountry;
	}

	public OrderModel getOrder()
	{
		return order;
	}

	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}
}
