package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileycom.order.impl.WileycomSetPaymentAddressStrategyImpl;


/**
 * Extends {@link WileycomSetPaymentAddressStrategyImpl} to calculate cart after setting payment address
 * for getting correct taxes
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cSetPaymentAddressStrategyImpl extends WileycomSetPaymentAddressStrategyImpl
{
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Override
	public void setPaymentAddress(@Nonnull final CommerceCheckoutParameter commerceCheckoutParameter)
	{
		super.setPaymentAddress(commerceCheckoutParameter);
		CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setCart(commerceCheckoutParameter.getCart());
		commerceCartCalculationStrategy.calculateCart(commerceCartParameter);
	}

	@Required
	public void setCommerceCartCalculationStrategy(
			final CommerceCartCalculationStrategy commerceCartCalculationStrategy)
	{
		this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
	}
}
