package com.wiley.core.promotions.ruledefinitions.conditions;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.ruledefinitions.conditions.RuleQualifyingProductsConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyProductModel;

import static com.wiley.core.constants.WileyCoreConstants.ONLINE_CATALOG_VERSION;


public class WileyRuleQualifyingProductsAndVariantsConditionTranslator implements RuleConditionTranslator
{
	@Resource
	private ProductService productService;

	@Resource(name = "defaultRuleQualifyingProductsConditionTranslator")
	private RuleQualifyingProductsConditionTranslator ruleQualifyingProductsConditionTranslator;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private SessionService sessionService;

	private List<String> applicableForCatalogs;

	@Override
	public RuleIrCondition translate(final RuleCompilerContext context,
			final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition) throws RuleCompilerException
	{

		if (MapUtils.isNotEmpty(condition.getParameters()))
		{
			RuleParameterData productsParameter = condition.getParameters().get("products");

			Set<String> products = new HashSet<>();
			if (productsParameter.getValue() instanceof List)
			{
				List<String> codes = productsParameter.getValue();
				products.addAll(getProductCodes(codes));
			}
			productsParameter.setValue(new ArrayList<>(products));
		}

		return ruleQualifyingProductsConditionTranslator.translate(context, condition, conditionDefinition);
	}

	private Set<String> getProductCodes(final List<String> codes)
	{
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				catalogVersionService.setSessionCatalogVersions(
						catalogVersionService.getAllCatalogVersions().stream().filter(c -> c.getActive().equals(true))
								.collect(Collectors.toList()));
				return getProductVariantCodes(codes);
			}
		});
	}

	private Set<String> getProductVariantCodes(final List<String> productCodes)
	{
		Set<String> products = new HashSet<>();
		for (String productCode : productCodes)
		{
			products.add(productCode);
			ProductModel product = findProductInApplicableCatalog(productCode);
			if (product instanceof WileyProductModel
					&& CollectionUtils.isNotEmpty(product.getVariants()))
			{
				products.addAll(
						product.getVariants().stream().map(ProductModel::getCode).collect(Collectors.toList()));
			}
		}
		return products;
	}

	private ProductModel findProductInApplicableCatalog(final String code)
	{

		for (String catalog : applicableForCatalogs)
		{
			final CatalogVersionModel version = catalogVersionService.getCatalogVersion(catalog, ONLINE_CATALOG_VERSION);
			try
			{
				return productService.getProductForCode(version, code);
			}
			catch (UnknownIdentifierException notFoundException)
			{
				//ignore
			}
		}
		return null;
	}

	@Required
	public void setApplicableForCatalogs(final List<String> applicableForCatalogs)
	{
		this.applicableForCatalogs = applicableForCatalogs;
	}
}
