package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Address
{
	private String street;
	private String street2;
	private String city;
	private String stateProvince;
	private String postcodeZip;
	private String country;


	public String getStreet()
	{
		return street;
	}

	public void setStreet(final String street)
	{
		this.street = street;
	}

	public String getStreet2()
	{
		return street2;
	}

	public void setStreet2(final String street2)
	{
		this.street2 = street2;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getStateProvince()
	{
		return stateProvince;
	}

	public void setStateProvince(final String stateProvince)
	{
		this.stateProvince = stateProvince;
	}

	public String getPostcodeZip()
	{
		return postcodeZip;
	}

	public void setPostcodeZip(final String postcodeZip)
	{
		this.postcodeZip = postcodeZip;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}
}
