package com.wiley.core.wileybundle.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.wileybundle.dao.WileyBundleDao;


public class WileyBundleDaoImpl implements WileyBundleDao
{

	private final FlexibleSearchService flexibleSearchService;
	private static final String QUERY_FIND_WILEY_BUNDLES_BY_PRODUT_CODE =
			"SELECT {wb:PK} FROM {WileyBundle AS wb} WHERE {wb.sourceProductCode} = ?sourceProductCode";
	private static final String QUERY_FIND_OBSLETE_BUNDLES = "Select {wb:PK} FROM {" + WileyBundleModel._TYPECODE
			+ " AS wb} WHERE ({wb." + WileyBundleModel.SEQUENCEID + "} < ?sequenceId) or ({wb." + WileyBundleModel.SEQUENCEID
			+ "} is null)";
	
	@Autowired
	public WileyBundleDaoImpl(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public List<WileyBundleModel> getBundlesForProduct(@Nonnull final ProductModel product)
	{
		Map<String, String> params = new HashMap<>();
		params.put("sourceProductCode", product.getCode());
		return getQueryResult(QUERY_FIND_WILEY_BUNDLES_BY_PRODUT_CODE, params);
	}
	
	@Override
	public List<WileyBundleModel> getObsoleteBundles(@Nonnull final String sequenceId)
	{
		Map<String, String> params = new HashMap<>();
		params.put("sequenceId", sequenceId);
		return getQueryResult(QUERY_FIND_OBSLETE_BUNDLES, params);
	}
	
	private List<WileyBundleModel> getQueryResult(final String query, final Map<String, String> params)
	{
		final SearchResult<WileyBundleModel> result = flexibleSearchService.search(query, params);
		if (result.getCount() == 0)
		{
			return Collections.emptyList();
		}
		return result.getResult();
	}
}
