package com.wiley.core.wileybundle.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyBundleModel;

public interface WileyBundleDao
{
	List<WileyBundleModel> getBundlesForProduct(@Nonnull ProductModel product);

	List<WileyBundleModel> getObsoleteBundles(@Nonnull String sequenceId);
}
