package com.wiley.core.util.url;

import de.hybris.platform.core.model.ItemModel;


/**
 * Provide method shows if item's url is visible.
 * E.g. could be used for generating sitemap.xml, 404 response for pages not supposed to be hybris hosted
 *
 * @param <T>
 */
public interface UrlVisibilityStrategy<T extends ItemModel>
{
	/**
	 * Returns true if item's url should be visible
	 *
	 * @param item
	 * @return
	 */
	boolean isUrlForItemVisible(T item);
}
