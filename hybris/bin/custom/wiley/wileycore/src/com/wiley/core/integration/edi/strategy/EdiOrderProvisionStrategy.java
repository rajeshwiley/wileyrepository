package com.wiley.core.integration.edi.strategy;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Collection;
import java.util.List;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * Strategy that defines logic of provision of orders that are ready for export and marking orders as exported
 */
public interface EdiOrderProvisionStrategy
{
	/**
	 * Retrieves processes that are ready for export for store
	 *
	 * @param store
	 * 		- Base store
	 * @param paymentType
	 * @return - processes
	 */
	List<WileyExportProcessModel> getExportProcessesReadyForExport(BaseStoreModel store, String paymentType);

	/**
	 * Retrieves processes that are ready for daily report export for store
	 *
	 * @param store
	 * 		- Base store
	 * @param paymentType
	 * @return - processes
	 */
	List<WileyExportProcessModel> getExportProcessesReadyForDailyReportExport(BaseStoreModel store, String paymentType);

	/**
	 * Retrieves zero dollar physical partner order processes that are ready for export for store
	 *
	 * @param store
	 * 		- Base store
	 * @param paymentType
	 * @return - processes
	 */
	List<WileyExportProcessModel> getZeroDollarPhysicalPartnerOrderProcessesReadyForExport(BaseStoreModel store,
			String paymentType);

	/**
	 * Marks processes as exported
	 *
	 * @param processes
	 * 		- Collection of processes
	 */
	void markOrdersExported(Collection<WileyExportProcessModel> processes);

	/**
	 * Marks processes as report exported
	 *
	 * @param processes
	 * 		- Collection of processes
	 */
	void markDailyReportSent(Collection<WileyExportProcessModel> processes);

	/**
	 * Removes partial refunds processes from the list.
	 *
	 * @param processes
	 * @return - processes
	 */
	List<WileyExportProcessModel> removePartialRefundsProcesses(List<WileyExportProcessModel> processes);
}
