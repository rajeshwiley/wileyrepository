package com.wiley.core.wileyws.validator;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.wiley.core.wileyws.exceptions.WileyWsValidationException;


public class WileywsCurrencyValidator extends WileywsRelativePathValidator
{
	private static final Logger LOG = Logger.getLogger(WileywsCurrencyValidator.class);

	private static final String TYPE = "UnknownIdentifierError";
	private static final String FIELD_NAME = "currency";

	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return clazz != null;
	}

	@Override
	protected void validatePathObject(final Object target, final Errors errors)
	{
		final String currencyCode = (String) errors.getFieldValue(getPathToTheField());

		if (StringUtils.isEmpty(currencyCode))
		{
			return;
		}

		try
		{
			commonI18NService.getCurrency(currencyCode);
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException | IllegalStateException e)
		{
			LOG.error(e.getMessage(), e);
			throw new WileyWsValidationException(e.getMessage(), getPathToTheField(), TYPE);
		}
	}

	private String getPathToTheField()
	{
		return StringUtils.isBlank(getPath()) ? FIELD_NAME : getPath() + "." + FIELD_NAME;
	}
}
