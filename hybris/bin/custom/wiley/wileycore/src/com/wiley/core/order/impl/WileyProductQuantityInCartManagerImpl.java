package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.order.WileyProductQuantityInCartManager;
import com.wiley.core.product.WileyCountableProductService;


/**
 * Default implementation of {@link WileyProductQuantityInCartManager}
 *
 * @author Aliaksei_Zlobich
 */
// ProductQuantityInCartManager
public class WileyProductQuantityInCartManagerImpl implements WileyProductQuantityInCartManager
{

	private static final Logger LOG = Logger.getLogger(WileyProductQuantityInCartManagerImpl.class);

	private CommerceAddToCartStrategy commerceAddToCartStrategy;

	private CommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategy;


	private WileyCountableProductService wileyCountableProductService;

	private CartService cartService;

	private ModelService modelService;

	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter,
			final boolean forceQuantity) throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);

		final CartModel cart = parameter.getCart();
		final ProductModel product = parameter.getProduct();
		final long quantity = parameter.getQuantity();

		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);

		if (forceQuantity && !wileyCountableProductService.canProductHaveQuantity(product))
		{
			if (doesCartHaveProduct(cart, product)) // if product is in cart
			{
				// allowed to have single instance of this product in cart only,
				// fooling cart by building modification with zero added quantity
				return buildCartModificationWithZeroAddedQuantity(parameter, product);
			}
			else
			{
				// if client is trying to add more than maximum allowed quantity, force max quantity
				if (quantity > WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY)
				{
					parameter
							.setQuantity(WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY);
				}
			}
		}

		try
		{
			return commerceAddToCartStrategy.addToCart(parameter);
		}
		catch (IllegalStateException isex)
		{
			// IllegalStateException may occur in calculateCart() method if added product has no prices
			// to prevent from 500-error we catch it here and convert into CommerceCartModificationException
			// which is handler in AddToCart-controller
			throw new CommerceCartModificationException(StringUtils.EMPTY, isex);
		}
	}

	/**
	 * Method checks products's edition format and what action can be performed on this product.<br/>
	 * If the product doesn't feet requirements, {@link CommerceCartModificationException} is thrown.
	 *
	 * @param parameters
	 * @return cart modification
	 * @throws CommerceCartModificationException
	 * 		if product cannot have quantity greater then allowed.
	 */
	@Override
	@Nullable
	public CommerceCartModification updateQuantityForCartEntry(@Nonnull final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{

		final CartModel cart = parameters.getCart();
		final long quantity = parameters.getQuantity();

		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		final AbstractOrderEntryModel orderEntry = getOrderEntryByEntryNumber(cart, parameters.getEntryNumber());

		if (LOG.isDebugEnabled() && orderEntry != null)
		{
			LOG.debug(String.format("Could not find orderEntry by entry number [%s] in cart [%s]", parameters.getEntryNumber(),
					cart));
		}

		if (orderEntry != null)
		{
			// checking that product can have quantity greater than one in shopping cart.
			final ProductModel product = orderEntry.getProduct();
			if (product != null && !wileyCountableProductService.canProductHaveQuantity(product)
					&& quantity > WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY)
			{
				throw new CommerceCartModificationException(String.format("Product [%s] cannot have quantity greater than %d.",
						product.getCode(), WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY));
			}
		}
		return commerceUpdateCartEntryStrategy.updateQuantityForCartEntry(parameters);
	}

	@Override
	@Nullable
	public CommerceCartModification updatePointOfServiceForCartEntry(@Nonnull final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		return commerceUpdateCartEntryStrategy.updatePointOfServiceForCartEntry(parameters);
	}

	@Override
	@Nullable
	public CommerceCartModification updateToShippingModeForCartEntry(@Nonnull final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		return commerceUpdateCartEntryStrategy.updateToShippingModeForCartEntry(parameters);
	}

	@Override
	public void checkAndUpdateQuantityIfRequired(final CartModel cart, final List<CommerceCartModification> modifications)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		final List<AbstractOrderEntryModel> entries = cart.getEntries();

		final List<AbstractOrderEntryModel> entriesToUpdate = entries.stream()
				.filter(entry -> !wileyCountableProductService.canProductHaveQuantity(entry.getProduct()))
				.filter(entry -> entry.getQuantity() > WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY)
				.peek(entry ->
						entry.setQuantity((long) WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY))
				.peek(entry -> {
					final Iterator it = modifications.iterator();
					while (it.hasNext())
					{
						final CommerceCartModification modification = (CommerceCartModification) it.next();
						if (Objects.equals(entry, modification.getEntry()))
						{
							it.remove();
						}
					}
				})
				.collect(Collectors.toList());

		modelService.saveAll(entriesToUpdate);
	}

	private AbstractOrderEntryModel getOrderEntryByEntryNumber(final CartModel cart, final long entryNumber)
	{
		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			return entries.stream()
					.filter(entry -> entry.getEntryNumber().longValue() == entryNumber)
					.findFirst()
					.orElse(null);
		}
		return null;
	}

	private CommerceCartModification buildCartModificationWithZeroAddedQuantity(final CommerceCartParameter parameter,
			final ProductModel product)
	{
		CommerceCartModification modification = new CommerceCartModification();

		// we need to return an existing cart entry because it can be used later after successful cart modification.
		final CartEntryModel entryForProduct = getFirstCartEntryForProducts(parameter.getCart(), product);

		modification.setEntry(entryForProduct);
		modification.setProduct(product);
		modification.setQuantity(parameter.getQuantity());
		modification.setQuantityAdded(0);
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		return modification;
	}

	private CartEntryModel getFirstCartEntryForProducts(final CartModel cart, final ProductModel product)
	{
		final List<CartEntryModel> entriesForProduct = cartService.getEntriesForProduct(cart, product);
		return entriesForProduct.get(0);
	}

	private boolean doesCartHaveProduct(final CartModel cart, final ProductModel product)
	{
		final List<CartEntryModel> entriesForProduct = cartService.getEntriesForProduct(cart, product);
		return CollectionUtils.isNotEmpty(entriesForProduct);
	}

	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		return addToCart(parameter, true);
	}

	@Override
	public List<CommerceCartModification> addToCart(final List<CommerceCartParameter> parameterList)
			throws CommerceCartMergingException
	{
		List<CommerceCartModification> modifications = new ArrayList<>();
		try
		{
			if (CollectionUtils.isNotEmpty(parameterList))
			{
				for (CommerceCartParameter commerceCartParameter : parameterList)
				{
					modifications.add(addToCart(commerceCartParameter));
				}
			}
		}
		catch (final CommerceCartModificationException e)
		{
			throw new CommerceCartMergingException(e.getMessage(), e);
		}
		return modifications;
	}

	// Getters and Setters
	public CommerceAddToCartStrategy getCommerceAddToCartStrategy()
	{
		return commerceAddToCartStrategy;
	}

	@Required
	public void setCommerceAddToCartStrategy(
			final CommerceAddToCartStrategy commerceAddToCartStrategy)
	{
		this.commerceAddToCartStrategy = commerceAddToCartStrategy;
	}

	@Required
	public void setCommerceUpdateCartEntryStrategy(
			final CommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategy)
	{
		this.commerceUpdateCartEntryStrategy = commerceUpdateCartEntryStrategy;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public WileyCountableProductService getWileyCountableProductService()
	{
		return wileyCountableProductService;
	}

	@Required
	public void setWileyCountableProductService(final WileyCountableProductService wileyCountableProductService)
	{
		this.wileyCountableProductService = wileyCountableProductService;
	}
}
