package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Session
{
	private String id;
	private String updateStatus;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getUpdateStatus()
	{
		return updateStatus;
	}

	public void setUpdateStatus(final String updateStatus)
	{
		this.updateStatus = updateStatus;
	}
}
