package com.wiley.core.wiley.restriction;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 *
 * @author Dzmitryi_Halahayeu
 */
public interface WileyRestrictProductStrategy
{
	boolean isRestricted(ProductModel product);

	boolean isRestricted(CommerceCartParameter parameter);

	WileyRestrictionCheckResultDto createErrorResult(CommerceCartParameter parameter);
}
