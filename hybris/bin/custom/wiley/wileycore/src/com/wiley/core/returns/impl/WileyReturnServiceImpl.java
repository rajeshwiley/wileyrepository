package com.wiley.core.returns.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.impl.DefaultReturnService;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.event.EventService;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.event.StartExportProcessEvent;
import com.wiley.core.event.WileyOrderRefundEvent;
import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.core.returns.WileyReturnService;


public class WileyReturnServiceImpl extends DefaultReturnService implements WileyReturnService
{
	@Autowired
	private EventService eventService;

	@Override
	public void setRefundAmountToReturnRequest(final ReturnRequestModel returnRequest, final BigDecimal refundAmount)
	{
		final Double refundAmountDoubleValue = refundAmount != null ? refundAmount.doubleValue() : null;
		Preconditions.checkNotNull(returnRequest);
		returnRequest.setRefundAmount(refundAmountDoubleValue);
		getModelService().save(returnRequest);
	}

	@Override
	public Map<AbstractOrderEntryModel, Long> getAllReturnableEntries(final OrderModel order)
	{
		return super.getAllReturnableEntries(order).entrySet().stream()
				.filter(entry -> !(entry.getKey().getProduct() instanceof WileyGiftCardProductModel))
				.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
	}

	public void postRefundProcess(final PaymentTransactionEntryModel paymentTransactionEntry,
			final ReturnRequestModel request)
	{
		if (TransactionStatus.ACCEPTED.name().equals(paymentTransactionEntry.getTransactionStatus()))
		{
			eventService.publishEvent(new StartExportProcessEvent(request.getOrder(), paymentTransactionEntry,
					ExportProcessType.REFUND, WileyCoreConstants.EDI_SYSTEM_CODE, null));
			eventService.publishEvent(new WileyOrderRefundEvent(request));
		}
	}


	public WileyRefundService getWileyRefundService()
	{
		return (WileyRefundService) super.getRefundService();
	}

	@Override
	public ReturnRequestModel createReturnRequestWithAmount(final OrderModel orderModel, final BigDecimal refundAmount)
	{
		ReturnRequestModel request = createReturnRequest(orderModel);
		setRefundAmountToReturnRequest(request, refundAmount);
		return request;
	}
}
