package com.wiley.core.pin.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import com.wiley.core.model.PinModel;
import com.wiley.core.pin.dao.PinDao;
import com.wiley.core.pin.service.PinService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Default implementation of {@link PinService}.
 *
 * @author Gabor_Bata
 */
public class DefaultPinService implements PinService
{
	@Resource
	private PinDao pinDao;

	@Override
	public PinModel getPinForCode(final String code)
	{
		validateParameterNotNull(code, "Parameter code must not be null");
		final PinModel pinModel = pinDao.findPinByCode(code);
		if (pinModel == null)
		{
			throw new UnknownIdentifierException("Could not found PIN with the given code " + code);
		}
		return pinModel;
	}

	@Override
	public PinModel getPinForOrder(final AbstractOrderModel order)
	{
		validateParameterNotNull(order, "Parameter order must not be null");
		final PinModel pinModel = pinDao.findPinByOrder(order);
		if (pinModel == null)
		{
			throw new UnknownIdentifierException("Could not found PIN with the given order " + order.getCode());
		}
		return pinModel;
	}

	@Override
	public boolean isPinUsedForOrder(final AbstractOrderModel orderModel)
	{
		try
		{
			getPinForOrder(orderModel);
			return true;
		}
		catch (UnknownIdentifierException | IllegalArgumentException e)
		{
			return false;
		}
	}
}
