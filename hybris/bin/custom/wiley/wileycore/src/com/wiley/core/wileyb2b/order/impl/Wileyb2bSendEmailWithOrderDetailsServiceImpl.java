package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.order.email.Wileyb2bSendEmailWithOrderDetailsGateway;
import com.wiley.core.wileyb2b.order.Wileyb2bSendEmailWithOrderDetailsService;

import org.apache.commons.lang.StringUtils;


/**
 * {@link Wileyb2bSendEmailWithOrderDetailsService} implementation with usage of {@link Wileyb2bSendEmailWithOrderDetailsGateway}
 */
public class Wileyb2bSendEmailWithOrderDetailsServiceImpl implements Wileyb2bSendEmailWithOrderDetailsService
{

	@Resource
	private Wileyb2bSendEmailWithOrderDetailsGateway wileyb2bSendEmailWithOrderDetailsGateway;

	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Override
	public boolean sendEmailWithOrderDetails(final B2BCustomerModel b2BCustomer, final OrderModel order, final String email)
	{
		Preconditions.checkArgument(b2BCustomer != null, "Parameter b2BCustomer can not be null");
		Preconditions.checkArgument(order != null, "Parameter order can not be null");
		Preconditions.checkArgument(StringUtils.isNotEmpty(email), "Parameter email can not be null or empty");

		B2BUnitModel unit = b2bUnitService.getParent(b2BCustomer);

		return wileyb2bSendEmailWithOrderDetailsGateway.sendEmailWithOrderDetails(b2BCustomer.getUid(), email,
				order.getCode(), unit.getSapAccountNumber());
	}

}
