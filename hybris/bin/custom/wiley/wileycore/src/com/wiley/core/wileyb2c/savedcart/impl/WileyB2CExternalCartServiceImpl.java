package com.wiley.core.wileyb2c.savedcart.impl;

import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.savedCart.SavedCartGateway;
import com.wiley.core.wileyb2c.savedcart.WileyB2CExternalCartService;
import com.wiley.core.wileyb2c.savedcart.exception.GetSavedCartExternalSystemException;


public class WileyB2CExternalCartServiceImpl implements WileyB2CExternalCartService
{
	private static final Logger LOG = Logger.getLogger(WileyB2CExternalCartServiceImpl.class);

	@Resource
	private SavedCartGateway savedCartGateway;

	@Override
	public CartModel getExternalCart(final String cartId)
	{
		CartModel cart = null;
		try
		{
			cart = savedCartGateway.getCart(cartId);
		}
		catch (final ExternalSystemException exception)
		{
			final String errorMessage = "Failed to retrieve Cart by id " + cartId
					+ " due to external system exception";
			LOG.error(errorMessage, exception);
			LOG.error(ExceptionUtils.getRootCause(exception));
			throw new GetSavedCartExternalSystemException(errorMessage, exception);
		}
		return cart;
	}
}
