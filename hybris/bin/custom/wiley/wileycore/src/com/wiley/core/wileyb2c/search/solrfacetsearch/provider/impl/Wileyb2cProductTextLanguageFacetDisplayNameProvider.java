package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.provider.FacetDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;


/**
 */
public class Wileyb2cProductTextLanguageFacetDisplayNameProvider implements FacetDisplayNameProvider
{
	private CommonI18NService commonI18NService;

	@Override
	public String getDisplayName(final SearchQuery searchQuery, final String isoCode)
	{
		return commonI18NService.getLanguage(isoCode).getName();
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
