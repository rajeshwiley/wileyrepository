package com.wiley.core.util.savedvalues;

import de.hybris.platform.core.model.ItemModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;


/**
 * Holds information about changes done in itemModel
 * Class copied from ootb one
 *
 * @see com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.ItemModificationInfo
 */
public class ItemModificationInfo
{
	private final ItemModel model;
	private final Map<String, Object> originalValues = new HashMap();
	private final Map<String, Object> modifiedValues = new HashMap();
	private final Set<String> localizedAttributes = new HashSet();
	private boolean newFlag = false;

	public ItemModificationInfo(final ItemModel model)
	{
		this.model = model;
	}

	public void addEntry(final String attribute, final boolean localized, final Object originalValue, final Object modifiedValue)
	{
		if (!this.valuesEqual(originalValue, modifiedValue))
		{
			this.originalValues.put(attribute, originalValue);
			this.modifiedValues.put(attribute, modifiedValue);
			if (localized)
			{
				this.localizedAttributes.add(attribute);
			}
		}

	}

	public ItemModel getModel()
	{
		return this.model;
	}

	public Set<String> getModifiedAttributes()
	{
		return Collections.unmodifiableSet(this.originalValues.keySet());
	}

	public Object getOriginalValue(final String attribute)
	{
		return this.originalValues.get(attribute);
	}

	public Object getModifiedValue(final String attribute)
	{
		return this.modifiedValues.get(attribute);
	}

	public boolean isLocalized(final String attribute)
	{
		return this.localizedAttributes.contains(attribute);
	}

	public boolean isNew()
	{
		return this.newFlag;
	}

	public void setNew(final boolean isModelNew)
	{
		this.newFlag = isModelNew;
	}

	protected boolean valuesEqual(final Object value1, final Object value2)
	{
		return ObjectUtils.equals(value1, value2);
	}

}
