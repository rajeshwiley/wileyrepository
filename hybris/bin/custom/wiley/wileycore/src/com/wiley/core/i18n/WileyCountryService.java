package com.wiley.core.i18n;

import de.hybris.platform.core.model.c2l.CountryModel;


public interface WileyCountryService
{
	CountryModel findCountryByCode(String isoCode);

	CountryModel useDefaultCountryAsCurrent();
}