package com.wiley.core.aspect;

import de.hybris.platform.util.Config;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class JaloSessionAspect
{

	private static final String SESSION_REPLICATION_KEY = "session.replication.support";

	/**
	 * Preventing stale session when session is replicated across nodes (enabled in config). Defaults to legacy behavior
	 * when old session (not replicated) are used
	 */
	@Around("execution(static * de.hybris.platform.jalo.JaloSession.assureSessionNotStale(..))")
	public Object aroundAssureSessionNotStale(final ProceedingJoinPoint pjp) throws Throwable
	{
		return isSessionReplicationEnabled() ? true : pjp.proceed();
	}


	private boolean isSessionReplicationEnabled()
	{
		return Config.getBoolean(SESSION_REPLICATION_KEY, false);
	}
}
