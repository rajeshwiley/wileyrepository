package com.wiley.core.integration.cdm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCreateUserResponse {

  @JsonProperty("GUID")
  private String guid;

  @JsonProperty("Status")
  private String status;

  @JsonProperty("Error")
  private CDMCreateUserErrorResponse error;

  /**
   * @return the error
   */
  public CDMCreateUserErrorResponse getError() {
    return error;
  }

  /**
   * @param error
   *          the error to set
   */
  public void setError(final CDMCreateUserErrorResponse error) {
    this.error = error;
  }

  /**
   * @return the guid
   */
  public String getGuid() {
    return guid;
  }

  /**
   * @param guid
   *          the guid to set
   */
  public void setGuid(final String guid) {
    this.guid = guid;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status
   *          the status to set
   */
  public void setStatus(final String status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "CdmCreateUserResponse [guid=" + guid + ", status=" + status + ", error= " + error + "]";
  }

}
