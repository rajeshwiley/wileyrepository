package com.wiley.core.wileyb2c.cms2.model;

import com.wiley.core.jalo.Wileyb2cCMSSiteRestriction;
import com.wiley.core.model.WileyPartnerCompanyModel;
import de.hybris.platform.cms2.jalo.restrictions.UserRestriction;
import de.hybris.platform.cms2.model.UserRestrictionDescription;
import de.hybris.platform.cms2.model.restrictions.CMSUserRestrictionModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;


/**
 * @author Dzmitryi_Halahayeu
 */
public class
WileyPartnerCompanyHandler implements DynamicAttributeHandler<String, WileyPartnerCompanyModel>
{
	/**
	 * OOTB_CODE
	 * It's based on {@link UserRestrictionDescription#get(CMSUserRestrictionModel)}. Also similar logic is called from
	 * {@link Wileyb2cCMSSiteRestriction} like in {@link UserRestriction}
	 * It looks like OOTB this logic is implemented several times for support as Jalo, as Service model.
	 * I prefer to follow the same OOTB approach to prevent unexpected Hybris behavior
	 */
	@Override
	public String get(final WileyPartnerCompanyModel model)
	{
		return null;
	}

	@Override
	public void set(final WileyPartnerCompanyModel model, final String s)
	{
		throw new UnsupportedOperationException();
	}
}
