package com.wiley.core.util.seo;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.wileycom.seo.impl.WileyCatalogAwareSeoUrlResolver;


/**
 * Manager to provide URL mapping between SEO friendly and hybris specific URLs.
 */
public class SeoUrlMappingManager
{
	private static final Logger LOG = LoggerFactory.getLogger(SeoUrlMappingManager.class);

	private static final String STOREFRONT_WEBROOT = "storefront.webroot";
	private static final String PRODUCT_CATALOG = "ProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String LEVEL_CATEGORY_URL_FORMAT = "/%s/products/%s";
	private static final String PRODUCT_LINE_CATEGORY_URL_FORMAT = "/%s/products";
	private static final String SLASH = "/";
	private static final String SESSION_ID = ";wjsessionid";


	private Map<String, String> urlMappings = new HashMap<>();

	private AtomicBoolean isReinitializationRequired = new AtomicBoolean(false);

	@Resource
	private ProductService productService;

	@Resource
	private CategoryService categoryService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CommerceCategoryService commerceCategoryService;

	@Resource
	private WileyCatalogAwareSeoUrlResolver productModelUrlResolver;

	@Resource
	private AbstractUrlResolver<CategoryModel> categoryModelUrlResolver;

	@Resource
	private SiteConfigService siteConfigService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileyCategoryService defaultWileyCategoryService;

	public String getMappingForURI(final String requestURI) throws UnsupportedEncodingException
	{
		if (urlMappings.isEmpty() || isReinitializationRequired.get())
		{
			initializeMapping();
		}
		String key = URLEncoder.encode(requestURI, "utf-8").replaceAll("%2F", "/").replaceAll("%3B", ";").toLowerCase();
		
		// Need to remove the WJSESSION ID for urls which include session ids for mapping
		if (key.indexOf(SESSION_ID) !=  -1)
		{
		  key = key.substring(0, key.indexOf(SESSION_ID));
		}
		
		//we store SEO urls without ending slash but need to support both cases
		return urlMappings.get(StringUtils.removeEnd(key, SLASH));
	}

	public void reinitializeMapping()
	{
		isReinitializationRequired.set(true);
	}

	private void initializeMapping()
	{
		isReinitializationRequired.set(false);
		Map<String, String> mappingsMap = new HashMap<>();
		final String context = siteConfigService.getProperty(baseSiteService.getCurrentBaseSite().getUid() + STOREFRONT_WEBROOT);

		Set<CategoryModel> categories = getAllCategories();
		Set<ProductModel> products = getAllProducts(categories);


		LOG.debug("Initializing WEL SEO map for products: [{}], categories: [{}]",
				CollectionUtils.size(products),
				CollectionUtils.size(categories));

		putSeoMappingsForCategories(mappingsMap, categories, context);
		putSeoMappingsForProducts(mappingsMap, products, context);
		replaceSeoMap(mappingsMap);
	}

	private void replaceSeoMap(final Map<String, String> mappingsMap)
	{
		this.urlMappings = Collections.unmodifiableMap(mappingsMap);
	}

	private void putSeoMappingsForProducts(final Map<String, String> mappingsMap, final Set<ProductModel> products,
			final String context)
	{
		products.forEach(product -> {
			String seoUrl = productModelUrlResolver.resolve(product);

			final String internalProductUrl = getInternalProductUrl(product);
			putSeoMapping(mappingsMap, seoUrl, internalProductUrl);
			putSeoMapping(mappingsMap, context + seoUrl, internalProductUrl);
		});
	}

	private void putSeoMappingsForCategories(final Map<String, String> mappingsMap,
			final Set<CategoryModel> categories, final String context)
	{
		categories.forEach(category -> {
			String seoUrl = categoryModelUrlResolver.resolve(category);

			final String internalCategoryUrl = getInternalCategoryUrl(category);
			putSeoMapping(mappingsMap, seoUrl, internalCategoryUrl);
			putSeoMapping(mappingsMap, context + seoUrl, internalCategoryUrl);
		});
	}

	private void putSeoMapping(final Map<String, String> map, final String seoUrl, final String internalUrl) {
		Preconditions.checkNotNull(map);
		map.put(StringUtils.removeEnd(seoUrl, SLASH), internalUrl);
	}

	private Set<CategoryModel> getAllCategories()
	{
		String catalogId = baseSiteService.getCurrentBaseSite().getUid() + PRODUCT_CATALOG;
		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(catalogId, CATALOG_VERSION);

		Set<CategoryModel> categories = new HashSet<>();

		categoryService.getRootCategoriesForCatalogVersion(catalogVersion).forEach(rootCategory -> {
			categories.add(rootCategory);
			categoryService.getAllSubcategoriesForCategory(rootCategory).forEach(categories::add);
		});

		return categories;
	}

	private Set<ProductModel> getAllProducts(final Set<CategoryModel> categories)
	{
		Set<ProductModel> products = new HashSet<>();

		categories.forEach(category -> productService.getOnlineProductsForCategory(category).forEach(products::add));

		return products;
	}

	private String getInternalCategoryUrl(final CategoryModel category)
	{
		String url;
		if (category instanceof VariantValueCategoryModel)
		{
			url = getInternalUrlForVariantValueCategory((VariantValueCategoryModel) category);
		}
		else
		{
			url = String.format(PRODUCT_LINE_CATEGORY_URL_FORMAT, category.getCode());
		}
		return url;
	}

	private String getInternalUrlForVariantValueCategory(final VariantValueCategoryModel category)
	{
		CategoryModel primaryCategory =
				defaultWileyCategoryService
						.getPrimaryWileyCategoryForVariantCategory((VariantValueCategoryModel) category);
		if (primaryCategory == null)
		{
			final Collection<List<CategoryModel>> paths = commerceCategoryService.getPathsForCategory(category);
			if (CollectionUtils.isNotEmpty(paths))
			{
				List<CategoryModel> categoryPath = paths.iterator().next();
				primaryCategory = categoryPath.get(0);
			}
			else
			{
				primaryCategory = category;
			}
		}
		return String.format(LEVEL_CATEGORY_URL_FORMAT, primaryCategory.getCode(), category.getCode());
	}

	private String getInternalProductUrl(final ProductModel product)
	{
		return "/p/" + product.getCode();
	}
}
