package com.wiley.core.mpgs.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;
import com.wiley.core.mpgs.services.strategies.impl.WileyMPGSMerchantIdStrategyImpl;
import com.wiley.core.mpgs.services.strategies.impl.WileyasMPGSMerchantIdStrategyImpl;

import static com.wiley.core.constants.WileyCoreConstants.AS_SITE_ID;


public class WileyMPGSMerchantServiceImpl implements WileyMPGSMerchantService
{

	@Resource
	private WileyasMPGSMerchantIdStrategyImpl wileyasMPGSMerchantIdStrategy;

	@Resource
	private WileyMPGSMerchantIdStrategyImpl wileyMPGSMerchantIdStrategy;

	@Override
	public String getMerchantID(final AbstractOrderModel abstractOrderModel)
	{
		return getMerchantStrategy(abstractOrderModel).getMerchantID(abstractOrderModel);
	}

	@Override
	public WileyTnsMerchantModel getMerchant(final AbstractOrderModel abstractOrderModel)
	{
		return getMerchantStrategy(abstractOrderModel).getMerchant(abstractOrderModel);
	}

	private WileyMPGSMerchantIdStrategy getMerchantStrategy(final AbstractOrderModel abstractOrderModel)
	{
		final String baseSiteID = abstractOrderModel.getSite().getUid();
		return AS_SITE_ID.equals(baseSiteID) ? wileyasMPGSMerchantIdStrategy : wileyMPGSMerchantIdStrategy;
	}

	@Override
	public boolean isMerchantValid(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder", abstractOrder);
		final CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) abstractOrder.getPaymentInfo();
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder.paymentInfo", paymentInfo);
		final String paymentInfoMerchantId = paymentInfo.getMerchantId();
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder.paymentInfo.merchantId", paymentInfoMerchantId);

		final String abstractOrderMerchantID = getMerchantID(abstractOrder);
		final boolean isMerchantIdMatched = paymentInfoMerchantId.equals(abstractOrderMerchantID);

		return isMerchantIdMatched;
	}
}
