package com.wiley.core.wel.seo;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class WelSeoCategoryValidateInterceptor
		extends AbstractWelSeoCatalogAwareValidateInterceptor
		implements ValidateInterceptor<CategoryModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(WelSeoCategoryValidateInterceptor.class);

	@Autowired
	public WelSeoCategoryValidateInterceptor(final EventService eventService)
	{
		super(eventService);
	}

	@Override
	public void onValidate(final CategoryModel categoryModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		CatalogVersionModel catalogVersion = categoryModel.getCatalogVersion();
		if (catalogVersion != null && isCatalogAllowed(catalogVersion.getCatalog())
				&& StringUtils.equals(CATALOG_VERSION, catalogVersion.getVersion()))
		{
			LOG.debug("WelSeoProductValidateInterceptor executing on save Category");
			if (interceptorContext.isNew(categoryModel)
					|| interceptorContext.isModified(categoryModel, CategoryModel.NAME)
					|| interceptorContext.isModified(categoryModel, CategoryModel.SEOLABEL)
					|| interceptorContext.isModified(categoryModel, CategoryModel.SUPERCATEGORIES)
					|| interceptorContext.isModified(categoryModel, CategoryModel.CATEGORIES)
					|| interceptorContext.isModified(categoryModel, CategoryModel.PRODUCTS))
			{
				LOG.debug("Sending ClusterAwareEvent to invalidate WEL Seo Map on all server nodes.");
				publishClusterAwareEvent();
			}
		}

	}
}
