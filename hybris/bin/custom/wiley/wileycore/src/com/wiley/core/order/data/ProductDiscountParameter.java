package com.wiley.core.order.data;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.AbstractDiscountRowModel;

import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;


/**
 * Used with {@link com.wiley.core.order.WileyPriceFactory WileyPriceFactory} to transfer multiple params to method.
 */
public class ProductDiscountParameter
{

	private ProductModel product;

	private UserDiscountGroup userDiscountGroup;

	private UserModel user;

	private CurrencyModel currency;

	private Date date;

	private boolean net;

	private Optional<Predicate<AbstractDiscountRowModel>> additionalFilter;

	public ProductModel getProduct()
	{
		return product;
	}

	public void setProduct(final ProductModel product)
	{
		this.product = product;
	}

	public UserDiscountGroup getUserDiscountGroup()
	{
		return userDiscountGroup;
	}

	public void setUserDiscountGroup(final UserDiscountGroup userDiscountGroup)
	{
		this.userDiscountGroup = userDiscountGroup;
	}

	public UserModel getUser()
	{
		return user;
	}

	public void setUser(final UserModel user)
	{
		this.user = user;
	}

	public CurrencyModel getCurrency()
	{
		return currency;
	}

	public void setCurrency(final CurrencyModel currency)
	{
		this.currency = currency;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

	public boolean isNet()
	{
		return net;
	}

	public void setNet(final boolean net)
	{
		this.net = net;
	}

	public Optional<Predicate<AbstractDiscountRowModel>> getAdditionalFilter()
	{
		return additionalFilter;
	}

	public void setAdditionalFilter(
			final Optional<Predicate<AbstractDiscountRowModel>> additionalFilter)
	{
		this.additionalFilter = additionalFilter;
	}
}
