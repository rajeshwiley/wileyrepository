package com.wiley.core.wileyas.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.order.impl.DefaultAbstractOrderEntryService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileyas.order.dao.WileyasOrderEntryDao;


public class WileyasAbstractOrderEntryServiceImpl extends DefaultAbstractOrderEntryService
{
	private KeyGenerator guidKeyGenerator;

	private WileyasOrderEntryDao wileyasOrderEntryDao;

	@Override
	public AbstractOrderEntryModel createEntry(final AbstractOrderModel order)
	{
		AbstractOrderEntryModel result = super.createEntry(order);
		result.setGuid(getGuidKeyGenerator().generate().toString());
		return result;
	}

	@Override
	public AbstractOrderEntryModel createEntry(final ComposedTypeModel entryType, final AbstractOrderModel order)
	{
		AbstractOrderEntryModel result = super.createEntry(entryType, order);
		result.setGuid(getGuidKeyGenerator().generate().toString());
		return result;
	}

	protected KeyGenerator getGuidKeyGenerator()
	{
		return guidKeyGenerator;
	}

	@Required
	public void setGuidKeyGenerator(final KeyGenerator guidKeyGenerator)
	{
		this.guidKeyGenerator = guidKeyGenerator;
	}

	protected WileyasOrderEntryDao getWileyasOrderEntryDao()
	{
		return wileyasOrderEntryDao;
	}

	@Required
	public void setWileyasOrderEntryDao(final WileyasOrderEntryDao wileyasOrderEntryDao)
	{
		this.wileyasOrderEntryDao = wileyasOrderEntryDao;
	}
}
