package com.wiley.core.exceptions;

public class WileyDiscountNotFoundException extends RuntimeException
{
	public WileyDiscountNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
