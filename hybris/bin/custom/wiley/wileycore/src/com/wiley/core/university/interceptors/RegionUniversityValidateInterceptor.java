package com.wiley.core.university.interceptors;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.UniversityModel;


/**
 * Checks correctness region in university model (see {@link UniversityModel})
 */
public class RegionUniversityValidateInterceptor implements ValidateInterceptor<UniversityModel>
{

	private L10NService l10nService;

	@Override
	public void onValidate(final UniversityModel university, final InterceptorContext interceptorContext) throws
			InterceptorException
	{
		checkRegionRequiredForCountry(university.getCountry(), university.getRegion());
		validateRegionByCountry(university.getCountry(), university.getRegion());
	}

	private void validateRegionByCountry(final CountryModel country, final RegionModel region) throws InterceptorException
	{
		if (region != null)
		{
			if (!country.equals(region.getCountry()))
			{
				throw new InterceptorException(l10nService.getLocalizedString("error.university.region.incorrect.message"));
			}
		}
	}

	private void checkRegionRequiredForCountry(final CountryModel country, final RegionModel region) throws InterceptorException
	{
		if (!country.getRegions().isEmpty() && region == null)
		{
			throw new InterceptorException(l10nService.getLocalizedString("error.university.region.required.message"));
		}
	}

	@Required
	public void setL10nService(final L10NService l10nService)
	{
		this.l10nService = l10nService;
	}
}