package com.wiley.core.integration.edi.service;


import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface PurchaseOrderService
{
	String buildHybrisAuthorizationCode(@Nonnull OrderModel orderModel);

	String getCreditCardType(@Nonnull CreditCardPaymentInfoModel paymentInfo);

	BigDecimal getSettleOrRefundTransactionAmount(@Nonnull WileyExportProcessModel exportProcess);

	String getCCToken(@Nonnull OrderModel orderModel);

	String getOrderType(@Nonnull WileyExportProcessModel exportProcess);

	boolean isOrderRefunded(@Nonnull WileyExportProcessModel exportProcess);

	String buildTransactionId(@Nonnull WileyExportProcessModel exportProcess);

	String buildShipMethod(@Nonnull OrderModel orderModel);

	String buildCreditCardExpiry(@Nonnull CreditCardPaymentInfoModel paymentInfo);

}
