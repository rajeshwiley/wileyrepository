package com.wiley.core.product.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.YearMonth;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.dao.WileyProductDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


/**
 * Default implementation of {@link WileyProductService}.
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyProductService extends DefaultProductService implements WileyProductService
{

	private static final Logger LOG = Logger.getLogger(DefaultWileyProductService.class);
	private static final String EXAM_DATE_CLASSIFICATION_ATTRIBUTE_CODE = "WELClassification/1.0/WEL_CFA_CLASS.wel_cfa_exam_date";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private WileyCategoryService categoryService;

	@Resource
	private WileyProductDao wileyProductDao;

	@Resource
	private ClassificationService classificationService;

	@Resource
	private ProductReferenceService productReferenceService;

	@Resource
	private ImpersonationService impersonationService;

	@Resource
	private SearchRestrictionService searchRestrictionService;


	@Override
	public String getSystemRequirements(final ProductModel product)
	{
		ServicesUtil.validateParameterNotNull(product, "Parameter [product] cannot be null.");

		final Predicate<CategoryModel> categoryFilter = (categoryModel) ->
		{
			final Class<? extends CategoryModel> categoryClass = categoryModel.getClass();
			return
					CategoryModel.class.equals(categoryClass) || VariantCategoryModel.class.equals(
							categoryClass) || VariantValueCategoryModel.class.equals(
							categoryClass);
		};

		String systemRequirements = null;

		if (product instanceof WileyProductModel)
		{
			systemRequirements = getWileyProductSystemRequirements((WileyProductModel) product, categoryFilter);
		}
		else if (product instanceof WileyVariantProductModel)
		{
			systemRequirements = getWileyVariantProductSystemRequirements((WileyVariantProductModel) product, categoryFilter);
		}

		if (StringUtils.isEmpty(systemRequirements))
		{
			LOG.warn(String.format("System requirements for product [%s] were not found.", product));
		}

		return systemRequirements;
	}

	@Override
	public WileyProductModel getWileyProductForCode(final String code)
	{
		validateParameterNotNull(code, "Parameter code must not be null");
		final List<WileyProductModel> products = wileyProductDao.findWileyProductsByCode(code);

		validateIfSingleResult(products, format("Product with code '%s' not found!", code),
				format("Product code '%s' is not unique, %d products found!", code, Integer.valueOf(products.size())));

		return products.get(0);
	}

	@Override
	public WileyProductModel getWileyProductForCodeIfExists(final String code, final CatalogVersionModel catalogVersionModel)
	{
		validateParameterNotNull(code, "Parameter code must not be null");

		final ImpersonationContext context = new ImpersonationContext();

		final List<ProductModel> products = impersonationService.executeInContext(context,
				createFindProductsExecutor(code, catalogVersionModel));

		WileyProductModel wileyProduct = null;
		if (isNotEmpty(products) && products.get(0) instanceof WileyProductModel)
		{
			wileyProduct = (WileyProductModel) products.get(0);
		}

		return wileyProduct;
	}

	@Override
	public void saveProduct(final ProductModel product)
	{
		final ImpersonationContext context = new ImpersonationContext();
		impersonationService.executeInContext(context, createSaveAllExecutor());
	}

	private ImpersonationService.Executor<List<ProductModel>, RuntimeException> createFindProductsExecutor(
			final String code, final CatalogVersionModel catalogVersionModel)
	{
		return () ->
		{
			searchRestrictionService.disableSearchRestrictions();
			return getProductDao().findProductsByCode(catalogVersionModel, code);
		};
	}

	private ImpersonationService.Executor<Void, RuntimeException> createSaveAllExecutor()
	{
		return () ->
		{
			searchRestrictionService.disableSearchRestrictions();
			getModelService().saveAll();
			return null;
		};
	}

	@Override
	public boolean canBePartOfProductSet(@Nullable final ProductModel product)
	{
		return product != null && WileyVariantProductModel.class.equals(product.getClass());
	}

	/**
	 * Gets category service.
	 *
	 * @return the category service
	 */
	public WileyCategoryService getCategoryService()
	{
		return categoryService;
	}


	/**
	 * Sets category service.
	 *
	 * @param categoryService
	 * 		the category service
	 */
	public void setCategoryService(final WileyCategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	@Override
	public List<ProductModel> getProductsForCategoryExcludingSubcategories(final CategoryModel categoryModel)
	{
		validateParameterNotNull(categoryModel, "Parameter category was null");
		return wileyProductDao.findProductsByCategoryExcludingSubCategories(categoryModel).getResult();
	}


	@Override
	public List<WileyProductModel> getLatestWileyProductsFromCategoryRoot(final String categoryCode)
	{
		List<WileyProductModel> latestProducts = new ArrayList<WileyProductModel>();
		List<WileyProductModel> products = wileyProductDao.findWileyProductsFromCategoryRoot(categoryCode);
		Date latestDate = getLatestExamDate(products);
		if (latestDate != null)
		{
			latestProducts = getProductsWithExamDate(products, YearMonth.fromDateFields(latestDate));
		}
		else
		{
			LOG.warn(String.format("The Latest date for CFA courses was not found. CFA courses count: %s", products.size()));
		}

		return latestProducts;
	}

	/**
	 * Returns wileyProduct list from the root of category according to formattedDate.
	 *
	 * @param categoryCode
	 * 		the Category code
	 * @param date
	 * 		the date in a format yyyyMM
	 * @return the list of {@link WileyProductModel}
	 */
	@Override
	public List<WileyProductModel> getWileyProductsFromCategoryRoot(final String categoryCode, final Date date)
	{
		List<WileyProductModel> result = new ArrayList<WileyProductModel>();
		List<WileyProductModel> products = wileyProductDao.findWileyProductsFromCategoryRoot(categoryCode);
		if (date != null)
		{
			result = getProductsWithExamDate(products, YearMonth.fromDateFields(date));
		}
		else
		{
			return products;
		}
		return result;
	}

	@Override
	public List<ProductModel> getWileyProductsFromCategories(final String baseProductCategoryCode,
															 final String variantValueCategoryCode)
	{
		List<ProductModel> products = wileyProductDao.findWileyProductsFromCategories(
				baseProductCategoryCode, variantValueCategoryCode);
		return products;
	}

	@Override
	public List<ProductModel> filterProducts(final List<ProductModel> products)
	{
		validateParameterNotNull(products, "Parameter products was null");

		Map<String, TreeSet<ProductModel>> groupedProducts = new HashMap<>();
		for (ProductModel product : products)
		{
			String key;
			if (product instanceof VariantProductModel)
			{
				key = ((VariantProductModel) product).getBaseProduct().getCode();
			}
			else
			{
				key = product.getCode();
			}

			if (!groupedProducts.containsKey(key))
			{
				TreeSet<ProductModel> productsInBucket = new TreeSet<>(ProductComparator.INSTANSE);
				groupedProducts.put(key, productsInBucket);
			}
			groupedProducts.get(key).add(product);
		}

		final List<ProductModel> filteredProducts = new ArrayList<>();
		for (TreeSet<ProductModel> productsInBucket : groupedProducts.values())
		{
			filteredProducts.add(productsInBucket.first());
		}
		return filteredProducts;
	}

	private static class ProductComparator extends AbstractComparator<ProductModel>
	{
		public static final ProductComparator INSTANSE = new ProductComparator();

		@Override
		protected int compareInstances(final ProductModel product1, final ProductModel product2)
		{
			return compareValues(product1.getCode(), product2.getCode(), true);
		}
	}

	private List<WileyProductModel> getProductsWithExamDate(final List<WileyProductModel> products, final YearMonth examDate)
	{
		List<WileyProductModel> result = new ArrayList<WileyProductModel>();
		for (WileyProductModel product : products)
		{
			final FeatureList featureList = classificationService.getFeatures(product);
			// get date value by code from FeatureList
			final Feature feature = featureList.getFeatureByCode(EXAM_DATE_CLASSIFICATION_ATTRIBUTE_CODE);
			if (feature != null && feature.getValue() != null)
			{
				Date productExamDate = (Date) feature.getValue().getValue();
				if (productExamDate != null && examDate.equals(YearMonth.fromDateFields(productExamDate)))
				{
					result.add(product);
				}
			}
		}
		return result;
	}

	/**
	 * Returns true if the variant product is addon. False in other case
	 *
	 * @param variantProductModel
	 * 		the wiley variant product
	 * @return the boolean
	 */
	@Override
	public Boolean isAddon(final VariantProductModel variantProductModel)
	{
		Collection<ProductReferenceModel> productReferenceModels = productReferenceService.getProductReferencesForTargetProduct(
				variantProductModel, ProductReferenceTypeEnum.ADDON, true);
		if (!productReferenceModels.isEmpty())
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public Date getLatestExamDate(final List<WileyProductModel> products)
	{
		Date lastestDate = null;
		for (WileyProductModel product : products)
		{
			final FeatureList featureList = classificationService.getFeatures(product);
			// now get some value by code from FeatureList
			final Feature feature = featureList.getFeatureByCode(EXAM_DATE_CLASSIFICATION_ATTRIBUTE_CODE);
			if (feature != null && feature.getValue() != null)
			{
				Date productExamDate = (Date) feature.getValue().getValue();
				if (lastestDate == null || lastestDate.before(productExamDate))
				{
					lastestDate = productExamDate;
				}
			}
		}
		return lastestDate;
	}

	@Override
	public ProductModel getProductForIsbnAndTerm(final String isbn, final String term, final String catalogId,
												 final String catalogVersion)
	{
		validateParameterNotNull(isbn, "Parameter isbn must not be null");
		validateParameterNotNull(catalogId, "Parameter catalogId must not be null");
		validateParameterNotNull(catalogVersion, "Parameter catalogVersion must not be null");
		List<? extends ProductModel> products;
		CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(catalogId, catalogVersion);

		if (term != null)
		{
			products = wileyProductDao.findProductsByIsbnAndTerm(isbn, term, catalogVersionModel);
		}
		else
		{
			products = wileyProductDao.findProductsByIsbn(isbn, catalogVersionModel);
		}

		validateIfSingleResult(products, format("Product with isbn '%s' and term '%s' not found!", isbn, term),
				format("Product with isbn '%s' and term '%s' is not unique, %d products found!", isbn, term,
						Integer.valueOf(products.size())));

		return products.get(0);
	}

	@Override
	public ProductModel getProductForIsbn(final String isbn)
	{
		validateParameterNotNull(isbn, "Parameter isbn must not be null");

		List<ProductModel> products = wileyProductDao.findProductsByIsbn(isbn);

		validateIfSingleResult(products, format("Product with isbn '%s' not found!", isbn),
				format("Product with isbn '%s' is not unique, %d products found!", isbn,
						Integer.valueOf(products.size())));

		return products.get(0);
	}

	@Override
	public ProductModel getProductForIsbn(final String isbn, final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNull(isbn, "Parameter isbn must not be null");
		validateParameterNotNull(catalogVersion, "Parameter catalogVersion must not be null");
		List<? extends ProductModel> products;

		products = wileyProductDao.findProductsByIsbn(isbn, catalogVersion);

		//expect that list can be null ot return list with one product
		return isNotEmpty(products) ? products.get(0) : null;
	}

	private String getWileyVariantProductSystemRequirements(
			final WileyVariantProductModel variantProduct,
			final Predicate<CategoryModel> categoriesFilter
	)
	{
		String systemRequirements = variantProduct.getSystemRequirements();
		if (StringUtils.isEmpty(systemRequirements) && variantProduct.getBaseProduct() != null)
		{
			systemRequirements = getCategoryService().getSystemRequirementsFromCategoryTree(
					variantProduct.getBaseProduct().getSupercategories(),
					categoriesFilter);
		}

		return systemRequirements;
	}

	private String getWileyProductSystemRequirements(
			final WileyProductModel wileyProduct,
			final Predicate<CategoryModel> categoriesFilter
	)
	{
		String systemRequirements = wileyProduct.getSystemRequirements();

		if (StringUtils.isEmpty(systemRequirements))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Product [%s] doesn't have system requirements. Trying to get it from categories.",
						wileyProduct));
			}

			systemRequirements = getCategoryService().getSystemRequirementsFromCategoryTree(wileyProduct.getSupercategories(),
					categoriesFilter);
		}

		return systemRequirements;
	}

	@Override
	public boolean isProductForCodeExist(final CatalogVersionModel catalogVersion, final String productCode)
	{
		validateParameterNotNull(productCode, "product code must not be null");
		validateParameterNotNull(catalogVersion, "catalogVersion must not be null");
		final List<ProductModel> products = getProductDao().findProductsByCode(catalogVersion, productCode);
		return isNotEmpty(products);
	}

	@Nonnull
	@Override
	public List<ProductModel> getProductsToValidate(final ArticleApprovalStatus approvalStatus,
													final CatalogVersionModel catalogVersion)
	{
		return wileyProductDao.findProductsToValidate(approvalStatus, catalogVersion);
	}

	@Override
	public List<ProductModel> getProductsForMediaContainerInGalleryImages(final MediaContainerModel mediaContainer)
	{
		return wileyProductDao.findProductsByMediaContainerInGalleryImages(mediaContainer);
	}

	@Override
	public List<ProductModel> getProductsForMediaContainerInBackgroundImage(final MediaContainerModel mediaContainer)
	{
		return wileyProductDao.findProductsByMediaContainerInBackgroudImage(mediaContainer);
	}

	/**
	 * Updates modification time of products of catalogs versions. The catalogs have to be included in current session.
	 *
	 * @param productCode
	 * 		code of products to be updated
	 */
	@Override
	public void updateProductsModifiedTime(final String productCode)
	{
		validateParameterNotNull(productCode, "Parameter productCode must not be null");

		final List<ProductModel> products = getProductDao().findProductsByCode(productCode);

		if (CollectionUtils.isEmpty(products))
		{
			LOG.info(String.format("Product with code [%s] was not found", productCode));
			return;
		}

		products.stream().forEach(product -> product.setModifiedtime(new Date()));
		getModelService().saveAll(products);
	}

	@Override
	public boolean isBaseProductFromCategory(final ProductModel product, final String categoryCode)
	{
		validateParameterNotNull(categoryCode, "categoryCode must not be null");

		ProductModel baseProduct = product;
		if (product instanceof WileyVariantProductModel)
		{
			baseProduct = ((WileyVariantProductModel) product).getBaseProduct();
		}
		if (baseProduct != null)
		{
			List<CategoryModel> categories = baseProduct.getSupercategories().stream()
					.filter(category -> categoryCode.equals(category.getCode())).collect(Collectors.toList());
			return !categories.isEmpty();
		}
		return false;
	}
}
