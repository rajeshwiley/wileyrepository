package com.wiley.core.product.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Default implementation of {@link WileyProductEditionFormatService}
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyProductEditionFormatService implements WileyProductEditionFormatService
{

	private static final Logger LOG = Logger.getLogger(DefaultWileyProductEditionFormatService.class);

	/**
	 * Check if order has only digital products
	 *
	 * @param abstractOrder
	 * @return true if all products in given cart are digital.
	 */
	@Override
	public boolean isDigitalCart(final AbstractOrderModel abstractOrder)
	{
		for (final AbstractOrderEntryModel entry : abstractOrder.getEntries())
		{
			if (!isDigitalProduct(entry.getProduct()))
			{
				return false;
			}
		}

		return true;
	}

	/**
	 * Check if order has only physical products
	 *
	 * @param abstractOrder
	 * @return true if all products in given cart are digital.
	 */
	@Override
	public boolean isPhysicalCart(final AbstractOrderModel abstractOrder)
	{
		for (final AbstractOrderEntryModel entry : abstractOrder.getEntries())
		{
			if (!isPhysicalProduct(entry.getProduct()))
			{
				return false;
			}
		}

		return true;
	}

	@Override
	@Nonnull
	public List<ProductModel> getAllShippableProducts(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder", abstractOrder);

		LOG.debug(String.format("Got AbstractOrder [%s] to gather shippable products.", abstractOrder.getCode()));

		final List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		return entries.stream()
				.map(AbstractOrderEntryModel::getProduct)
				.filter(this::isProductShippable)
				.collect(Collectors.toList());
	}

	@Override
	public boolean hasCartDigitalProduct(@Nonnull final CartModel cartModel)
	{
		return cartModel.getEntries().stream()
				.map(AbstractOrderEntryModel::getProduct)
				.anyMatch(this::isDigitalProduct);
	}

	@Override
	public boolean isProductShippable(final ProductModel product)
	{
		final ProductEditionFormat editionFormat = product.getEditionFormat();

		return editionFormat == null || ProductEditionFormat.PHYSICAL == editionFormat
				|| ProductEditionFormat.DIGITAL_AND_PHYSICAL == editionFormat;
	}

	@Override
	public boolean isDigitalProduct(final ProductModel product)
	{
		return ProductEditionFormat.DIGITAL.equals(product.getEditionFormat());
	}
	
	private boolean isPhysicalProduct(final ProductModel product) 
	{
	  return ProductEditionFormat.PHYSICAL.equals(product.getEditionFormat());
	}
}
