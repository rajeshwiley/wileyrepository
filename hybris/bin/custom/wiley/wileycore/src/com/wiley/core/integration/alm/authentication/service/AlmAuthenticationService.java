package com.wiley.core.integration.alm.authentication.service;

import javax.annotation.Nonnull;

import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface AlmAuthenticationService
{
	@Nonnull
	AlmAuthenticationResponseDto validateSessionToken(@Nonnull String sessionToken);
}
