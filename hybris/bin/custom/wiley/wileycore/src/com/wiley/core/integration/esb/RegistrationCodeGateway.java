package com.wiley.core.integration.esb;

import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface RegistrationCodeGateway
{
	String CART_MODEL = "header_cart_model";

	/**
	 * Sends validation request for the given Registration code.
	 * And update the Cart according...
	 *
	 * @param regCode
	 * 		the registration code
	 * @return the product code associated with the regCode or false if the regCode is invalid
	 */
	@Nonnull
	CartModel validateRegCode(@Nonnull @Payload String regCode, @Nonnull @Header(CART_MODEL)CartModel cart);

}
