package com.wiley.core.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * Service extends functionality of {@link CommerceCartService}. The service provides additional logic for working with
 * multi-dimensional products
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyMultiDimensionalCommerceCartService extends CommerceCartService
{


	/**
	 * Adds products to cart and tries to collect products to product set. If product set exists in cart this method doesn't add
	 * the set.
	 *
	 * @param cart
	 * 		the cart
	 * @param parameters
	 * 		the parameters contains product and quantity to add it to cart.
	 * @return the list of cart modifications.
	 * @throws CommerceCartModificationException
	 * 		if a product cannot be added to cart or cart cannot be modified.
	 */
	@Nonnull
	List<CommerceCartModification> addToCart(@Nonnull CartModel cart, @Nonnull List<CommerceCartParameter> parameters) throws
			CommerceCartModificationException;

	/**
	 * Search and remove carts where Entries do not have products
	 *
	 * @param userModel
	 */
	void searchAndRemoveBrokenCarts(UserModel userModel);
}
