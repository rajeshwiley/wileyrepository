package com.wiley.core.integration.wileycore;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import javax.annotation.Nonnull;

import com.wiley.core.integration.wileycore.dto.Message;


/**
 * Wiley Core Gateway interface.
 *
 * @author Gabor_Bata
 */
public interface WileyCoreGateway
{

	/**
	 * Sends validation request for the given PIN code.
	 *
	 * @param pinCode
	 * 		the pin code
	 * @return the response message from Wiley Core
	 */
	@Nonnull
	Message validatePin(@Nonnull String pinCode, @Nonnull BaseSiteModel site);

	/**
	 * Sends activation request for the given PIN code.
	 *
	 * @param pinCode
	 * 		the pin code
	 * @param firstName
	 * 		the first name of the customer
	 * @param lastName
	 * 		the last name of the customer
	 * @return the response message from Wiley Core
	 */
	@Nonnull
	Message activatePin(@Nonnull String pinCode, @Nonnull String firstName, @Nonnull String lastName,
			@Nonnull BaseSiteModel site);

}
