package com.wiley.core.wileycom.customer.dao;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;


public interface WileycomCustomerAccountDao
{
	/**
	 * Finds digital order entries for ownerId
	 *
	 *
	 * @param customer
	 * 		customer uid
	 * @param baseStore
	 * @param pageableData
	 * @return List<OrderEntryModel>
	 */
	SearchPageData<OrderEntryModel> findDigitalOrderEntriesByOwner(CustomerModel customer,
																   BaseStoreModel baseStore,
																   PageableData pageableData);


	/**
	 * Finds nameSuffix by code
	 *
	 * @param suffixCode
	 * @return {@link NameSuffixModel}
	 */
	Optional<NameSuffixModel> findNameSuffixByCode(String suffixCode);

	/**
	 * Finds school by code
	 *
	 * @param schoolCode
	 * @return {@link SchoolModel}
	 */
	Optional<SchoolModel> findSchoolByCode(String schoolCode);

	/**
	 * Finds all nameSuffixes
	 *
	 * @return List<NameSuffixModel>
	 */
	List<NameSuffixModel> findAllNameSuffixes();

	/**
	 * Finds all school names
	 *
	 * @return List<SchoolModel>
	 */
	List<SchoolModel> findAllSchools();

	/**
	 * Finds all school names with limitation.
	 * Only every nth school is to be retrieved with the given maximum quantity, where n is the range.
	 *
	 * @param range range
	 * @param maxQuantity maximum quantity
	 * @return List<SchoolModel>
	 * @see <a href="https://jira.wiley.ru/browse/ECSC-12687">ECSC-12687: Limitation of School list values displaying</a>
	 * @see <a href="https://wiki.hybris.com/x/1pZzC#FlexibleSearch-PagingofSearchResults">Paging of Search Results</a>
	 */
	List<SchoolModel> findSchoolsWithLimitation(int range, int maxQuantity);
}
