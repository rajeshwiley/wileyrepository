package com.wiley.core.wileycom.customer.exception;

public class WileycomCustomerRegistrationConflictingIdException extends IllegalStateException
{
	public WileycomCustomerRegistrationConflictingIdException(final Throwable cause)
	{
		super(cause);
	}
}
