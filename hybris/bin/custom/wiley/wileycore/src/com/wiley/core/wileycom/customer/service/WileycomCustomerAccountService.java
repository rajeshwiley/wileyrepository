package com.wiley.core.wileycom.customer.service;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;


public interface WileycomCustomerAccountService extends WileyCustomerAccountService
{
	/**
	 * Gets Digital order entries for current session user
	 *
	 * @param currentCustomer
	 * @param currentBaseStore
	 * @param pageableData
	 * @return List<OrderEntryModel>
	 */
	SearchPageData<OrderEntryModel> getDigitalOrderEntries(CustomerModel currentCustomer,
			BaseStoreModel currentBaseStore, PageableData pageableData);

	/**
	 * Gets nameSuffix by code
	 *
	 * @param suffixCode
	 * @return Optional<NameSuffixModel>
	 */
	Optional<NameSuffixModel> getNameSuffixByCode(String suffixCode);

	/**
	 * Gets School by code
	 *
	 * @param schoolCode
	 * @return Optional<SchoolModel>
	 */
	Optional<SchoolModel> getSchoolByCode(String schoolCode);

	/**
	 * Gets all nameSuffixes
	 *
	 * @return List<NameSuffixModel>
	 */
	List<NameSuffixModel> getAllNameSuffixes();

	/**
	 * Gets all school names
	 *
	 * @return List<SchoolModel>
	 */
	List<SchoolModel> getAllSchools();

	/**
	 * Gets all school names with limitation.
	 * Only every nth school is to be retrieved with the given maximum quantity, where n is the range.
	 *
	 * @param range range
	 * @param maxQuantity maximum number of results
	 * @return List<SchoolModel>
	 * @see <a href="https://jira.wiley.ru/browse/ECSC-12687">ECSC-12687: Limitation of School list values displaying</a>
	 */
	List<SchoolModel> getSchoolsWithLimitation(int range, int maxQuantity);

	/**
	 * Reset customer's password in external systems with provided value
	 *
	 * @param uid
	 * @param newPassword
	 */
	void externalResetCustomerPassword(String uid, String newPassword) throws PasswordUpdateException;
}
