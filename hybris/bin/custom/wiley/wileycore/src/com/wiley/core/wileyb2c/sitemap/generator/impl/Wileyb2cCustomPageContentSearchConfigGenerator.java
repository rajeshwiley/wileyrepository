package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.converters.Converters;

import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotNull;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class Wileyb2cCustomPageContentSearchConfigGenerator extends AbstractWileyb2cSiteMapGenerator<String>
{
	/**
	 * OOTB_CODE from CustomPageSiteMapGenerator
	 */
	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<String> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	@Override
	protected List<String> getDataInternal(@NotNull final CMSSiteModel siteModel)
	{
		validateParameterNotNullStandardMessage("siteModel", siteModel);
		SiteMapConfigModel contentSearchConfig = siteModel.getContentSearchConfig();
		return contentSearchConfig == null ? Collections.emptyList() : ((List<String>) contentSearchConfig.getCustomUrls());
	}
}
