package com.wiley.core.healthcheck.checks;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;

import com.codahale.metrics.health.HealthCheck;


public class EdiHealthCheck extends HealthCheck
{
	private static final Logger LOG = Logger.getLogger(EdiHealthCheck.class);

	@Autowired
	private DefaultSftpSessionFactory coreSftpSessionFactory;

	@Override
	protected Result check() throws Exception
	{
		try
		{
			SftpSession sftpSession = coreSftpSessionFactory.getSession();

			if (sftpSession.isOpen())
			{
				sftpSession.close();
				return Result.healthy();
			}
		}
		catch (Exception e)
		{
			LOG.error("Can't connect to Wiley CORE sftp", e);
		}
		return Result.unhealthy("Can't connect to Wiley CORE sftp");
	}
}