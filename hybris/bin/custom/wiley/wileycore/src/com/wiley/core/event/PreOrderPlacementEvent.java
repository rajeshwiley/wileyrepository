package com.wiley.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;


public class PreOrderPlacementEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = 1L;

	public PreOrderPlacementEvent(final OrderProcessModel process)
	{
		super(process);
	}
}
