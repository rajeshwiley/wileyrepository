package com.wiley.core.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import com.wiley.core.enums.ExportProcessType;

/**
 * This event is used to start export processes
 */
public class StartExportProcessEvent extends AbstractEvent
{
	private OrderModel order;
	private PaymentTransactionEntryModel transactionEntry;
	private ExportProcessType exportProcessType;
	private String exportSystemCode;
	private OrderProcessModel businessProcess;

	/**
	 * Instantiate a new StartExportProcesses event
	 */
	public StartExportProcessEvent(final OrderModel order, final PaymentTransactionEntryModel transactionEntry,
			final ExportProcessType exportProcessType, final String exportSystemCode,
			final OrderProcessModel businessProcess)
	{
		this.order = order;
		this.transactionEntry = transactionEntry;
		this.exportProcessType = exportProcessType;
		this.exportSystemCode = exportSystemCode;
		this.businessProcess = businessProcess;
	}

	public OrderModel getOrder()
	{
		return order;
	}

	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}

	public ExportProcessType getExportProcessType()
	{
		return exportProcessType;
	}

	public String getExportSystemCode()
	{
		return exportSystemCode;
	}

	public OrderProcessModel getBusinessProcess()
	{
		return businessProcess;
	}

	public void setBusinessProcess(final OrderProcessModel businessProcess)
	{
		this.businessProcess = businessProcess;
	}

	public PaymentTransactionEntryModel getTransactionEntry()
	{
		return transactionEntry;
	}

	public void setTransactionEntry(final PaymentTransactionEntryModel transactionEntry)
	{
		this.transactionEntry = transactionEntry;
	}
}
