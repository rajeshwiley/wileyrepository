package com.wiley.core.product.interceptors.freetrial;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.model.WileyProductModel;


/**
 * Validate that {@link WileyProductModel} has only one reference on {@link WileyFreeTrialProductModel}
 */
public class WileyFreeTrialProductReferenceValidateInterceptor implements ValidateInterceptor<ProductReferenceModel>
{
	private L10NService l10nService;
	private ProductReferenceService productReferenceService;

	@Override
	public void onValidate(final ProductReferenceModel productReferenceModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		//exclude free trial models from checking
		if ((productReferenceModel.getSource() instanceof WileyFreeTrialProductModel)
				|| (productReferenceModel.getSource() instanceof WileyFreeTrialVariantProductModel))
		{
			return;
		}

		if (sourceProductHasFreeTrialReferences(productReferenceModel) && addedReferenceIsFreeTrial(productReferenceModel))
		{
			throw new InterceptorException(l10nService.getLocalizedString("error.wileytrialproduct.reference.exist.message"));
		}
	}

	private boolean addedReferenceIsFreeTrial(final ProductReferenceModel productReferenceModel)
	{
		return ProductReferenceTypeEnum.FREE_TRIAL.equals(productReferenceModel.getReferenceType());
	}

	private boolean sourceProductHasFreeTrialReferences(final ProductReferenceModel productReferenceModel)
	{
		final Collection<ProductReferenceModel> productFreeTrialReferences =
				productReferenceService.getProductReferencesForSourceProduct(productReferenceModel.getSource(),
						ProductReferenceTypeEnum.FREE_TRIAL, Boolean.TRUE);
		return CollectionUtils.isNotEmpty(productFreeTrialReferences);
	}

	public void setProductReferenceService(final ProductReferenceService productReferenceService)
	{
		this.productReferenceService = productReferenceService;
	}

	public void setL10nService(final L10NService l10nService)
	{
		this.l10nService = l10nService;
	}
}
