package com.wiley.core.event;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


public class AfterCronJobFinishedEvent extends AbstractEvent implements ClusterAwareEvent
{

	private CronJobResult cronJobResult;
	private CronJobStatus cronJobStatus;
	private String cronJobCode;
	private String cronJobType;
	private String jobCode;


	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return true;
	}

	public CronJobResult getCronJobResult()
	{
		return cronJobResult;
	}

	public void setCronJobResult(final CronJobResult cronJobResult)
	{
		this.cronJobResult = cronJobResult;
	}

	public String getCronJobCode()
	{
		return cronJobCode;
	}

	public void setCronJobCode(final String cronJobCode)
	{
		this.cronJobCode = cronJobCode;
	}

	public String getCronJobType()
	{
		return cronJobType;
	}

	public void setCronJobType(final String cronJobType)
	{
		this.cronJobType = cronJobType;
	}

	public CronJobStatus getCronJobStatus()
	{
		return cronJobStatus;
	}

	public void setCronJobStatus(final CronJobStatus cronJobStatus)
	{
		this.cronJobStatus = cronJobStatus;
	}

	public String getJobCode()
	{
		return jobCode;
	}

	public void setJobCode(final String jobCode)
	{
		this.jobCode = jobCode;
	}

}
