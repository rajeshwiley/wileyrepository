package com.wiley.core.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.WileyProductSummaryModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class Wileyb2cSubscriptionProductEntryPopulator implements Populator<ProductModel, AbstractOrderEntryModel>
{
	@Resource
	private ModelService modelService;

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final AbstractOrderEntryModel entry)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("productModel", productModel);
		validateParameterNotNullStandardMessage("entry", entry);

		if (!productModel.getSubtype().equals(WileyProductSubtypeEnum.SUBSCRIPTION))
		{
			throw new IllegalArgumentException("Product with code [" + productModel.getCode() + "] should have subtype "
					+ WileyProductSubtypeEnum.SUBSCRIPTION.toString() + " but has " + productModel.getSubtype().toString());
		}

		WileyProductSummaryModel productSummaryModel = modelService.create(WileyProductSummaryModel.class);
		entry.setProductSummary(productSummaryModel);

		productSummaryModel.setName(productModel.getName());
		final MediaModel picture = productModel.getPicture();
		if (picture != null)
		{
			productSummaryModel.setPictureUrl(picture.getURL());
		}
	}

}
