package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType.INDEX;
import static de.hybris.platform.solrfacetsearch.search.SearchQuery.QueryOperator.CONTAINS;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cCountryFilterListener implements FacetSearchListener
{
	static final String NOT_QUALIFIER = "-";
	private FieldNameProvider fieldNameProvider;

	private String countriesProperty;
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Override
	public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

		final IndexedType indexedType = facetSearchContext.getSearchQuery().getIndexedType();
		final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(countriesProperty);
		if (indexedProperty != null)
		{
			final String translatedField = fieldNameProvider.getFieldName(indexedProperty, null, INDEX);
			CountryModel currentCountry = wileycomI18NService.getCurrentCountry()
					.orElseGet(() -> wileycomI18NService.setDefaultCurrentCountry());
			String field = NOT_QUALIFIER + translatedField;
			final QueryField countryQuery = new QueryField(field, currentCountry.getIsocode());
			countryQuery.setQueryOperator(CONTAINS);
			if (filterQueryNotPresent(facetSearchContext, field))
			{
				facetSearchContext.getSearchQuery().addFilterQuery(countryQuery);
			}
		}
	}

	private boolean filterQueryNotPresent(final FacetSearchContext facetSearchContext, final String field)
	{
		return facetSearchContext.getSearchQuery().getFilterQueries().stream()
				.noneMatch(queryField -> queryField.getField().equals(field));
	}

	@Required
	public void setCountriesProperty(final String countriesProperty)
	{
		this.countriesProperty = countriesProperty;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@Override
	public void afterSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}

	@Override
	public void afterSearchError(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}
}
