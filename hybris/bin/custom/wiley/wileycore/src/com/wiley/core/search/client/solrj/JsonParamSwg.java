package com.wiley.core.search.client.solrj;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.wiley.core.search.client.solrj.facet.FacetSwg;


/**
 * Implementation of SOLR JSON Facet API https://cwiki.apache.org/confluence/display/solr/JSON+Request+API
 */
@JsonRootName("json")
public class JsonParamSwg implements Serializable
{
	private FacetSwg facet = new FacetSwg();

	@JsonProperty
	public FacetSwg getFacet()
	{
		return facet;
	}

	public void setFacet(final FacetSwg facet)
	{
		this.facet = facet;
	}
}
