package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * This class finds Main author of the book.
 * List of authors is ordered and the first author is the main author.
 * We will use it for ascending(A-Z) sorting by author
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cMainAuthorValueProvider extends AbstractWileycomValueProvider<String> {
    @Override
    protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
            throws FieldValueProviderException {
        final List<AuthorInfoModel> authorInformation = getAuthorInfos((VariantProductModel) model);
        if (CollectionUtils.isNotEmpty(authorInformation)) {
            Optional<String> mainAuthor = authorInformation
                    .stream()
                    .map(AuthorInfoModel::getName)
                    .filter(StringUtils::isNotEmpty)
                    .findFirst();
            return mainAuthor.isPresent() ? Arrays.asList(mainAuthor.get()) : Collections.emptyList();
        }
        return Collections.emptyList();
    }

    private List<AuthorInfoModel> getAuthorInfos(final VariantProductModel model) {
        return model.getBaseProduct().getAuthorInfos();
    }
}
