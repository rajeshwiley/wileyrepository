package com.wiley.core.wileycom.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.order.AbstractOrderEntryTypeService;
import de.hybris.platform.order.strategies.ordercloning.impl.DefaultCloneAbstractOrderStrategy;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;

import javax.annotation.Resource;


/**
 * Created by Uladzimir_Barouski on 6/20/2016.
 */
public class WileycomCloneAbstractOrderStrategy extends DefaultCloneAbstractOrderStrategy
{
	@Resource
	ModelService modelService;

	public WileycomCloneAbstractOrderStrategy(final TypeService typeService,
			final ItemModelCloneCreator itemModelCloneCreator,
			final AbstractOrderEntryTypeService abstractOrderEntryTypeService)
	{
		super(typeService, itemModelCloneCreator, abstractOrderEntryTypeService);
	}

	@Override
	public <T extends AbstractOrderModel> T clone(final ComposedTypeModel orderType, final ComposedTypeModel entryType,
			final AbstractOrderModel original, final String code, final Class abstractOrderClassResult,
			final Class abstractOrderEntryClassResult)
	{
		final AbstractOrderModel orderModel = super.clone(orderType, entryType, original, code, abstractOrderClassResult,
				abstractOrderEntryClassResult);
		return (T) orderModel;
	}
}
