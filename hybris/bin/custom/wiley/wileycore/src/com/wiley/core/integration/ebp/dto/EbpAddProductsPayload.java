package com.wiley.core.integration.ebp.dto;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * Object for passing data from action to EPB transformer
 */
public class EbpAddProductsPayload
{
	private OrderModel order;


	public OrderModel getOrder()
	{
		return order;
	}

	public void setOrder(final OrderModel order)
	{
		this.order = order;
	}
}
