package com.wiley.core.processengine.strategies.impl;

import de.hybris.platform.acceleratorservices.process.strategies.impl.AbstractProcessContextStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import com.wiley.core.model.cartprocessing.CartProcessModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Optional;

/**
 * Wiley strategy to impersonate site and initialize session context from the process model.
 */
public class WileyProcessContextResolutionStrategy extends AbstractProcessContextStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyProcessContextResolutionStrategy.class);

	@Override
	public BaseSiteModel getCmsSite(final BusinessProcessModel businessProcessModel) {
		LOG.error("Method params: getCmsSite WileyProcessContextResolutionStrategy");
		ServicesUtil.validateParameterNotNull(businessProcessModel, BUSINESS_PROCESS_MUST_NOT_BE_NULL_MSG);

		return Optional.of(businessProcessModel)
				.filter(businessProcess -> businessProcess instanceof CartProcessModel)
				.map(businessProcess -> ((CartProcessModel) businessProcess).getCart().getSite())
				.orElse(null);
	}

	@Override
	protected CustomerModel getCustomer(final BusinessProcessModel businessProcess)
	{
		return null;
	}
}