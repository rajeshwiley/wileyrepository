package com.wiley.core.wileycom.media.translators.adapter.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.wileycom.valuetranslator.adapter.AbstractWileycomImportAdapter;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomUpdateProductModelMediaImportAdapterImpl extends AbstractWileycomImportAdapter
{
	@Resource
	private ModelService modelService;
	@Resource
	private ProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Override
	public void performImport(final String cellValue, final Item item) throws UnresolvedValueException
	{
		Assert.notNull(item);
		Assert.hasText(cellValue);
		MediaModel mediaModel = modelService.get(item);
		ProductModel productModel = productService.getProductForCode(
				catalogVersionService.getCatalogVersion(getCatalogId(), getCatalogVersion()), cellValue);
		productModel.setPicture(mediaModel);
		modelService.save(productModel);
	}
}
