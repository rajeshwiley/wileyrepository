package com.wiley.core.util.savedvalues;

import de.hybris.platform.core.model.ItemModel;


/**
 * Is used for create and log modification done for itemModel
 * Interface is similar to ootb one
 *
 * @see com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.ItemModificationHistoryService
 */
public interface ItemModificationHistoryService
{
	/**
	 * Saves information about modifications 
	 *
	 * @param modificationInfo
	 */
	void logModifications(ItemModificationInfo modificationInfo);

	/**
	 * Returns information about changes done with itemModel
	 *
	 * @param itemModel
	 * @return
	 */
	ItemModificationInfo createModificationInfo(ItemModel itemModel);
}
