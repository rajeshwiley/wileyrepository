/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.wiley.core.enums.OrderType;
import com.wiley.core.pin.service.PinService;


/**
 * Listener for order confirmation events.
 */
public class OrderConfirmationEventListener extends AbstractWileySiteEventListener<OrderPlacedEvent>
{
	private PinService pinService;

	/**
	 * On site event.
	 *
	 * @param orderPlacedEvent
	 * 		the order placed event
	 */
	@Override
	protected void onSiteEvent(final OrderPlacedEvent orderPlacedEvent)
	{
		final OrderModel orderModel = orderPlacedEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = getBusinessProcessService().createProcess(
				"orderConfirmationEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
				"orderConfirmationEmailProcess");
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final OrderPlacedEvent event)
	{
		final OrderModel order = event.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return !isFreeTrialOrder(order) && !isPinOrder(order);
	}

	private boolean isFreeTrialOrder(final OrderModel order)
	{
		return order.getOrderType() == OrderType.FREE_TRIAL || order.getOrderType() == OrderType.FREE_TRIAL_WEB_SERVICE;
	}

	private boolean isPinOrder(final OrderModel order)
	{
		return pinService.isPinUsedForOrder(order);
	}

	public void setPinService(final PinService pinService)
	{
		this.pinService = pinService;
	}
}
