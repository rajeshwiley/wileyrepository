package com.wiley.core.delivery.interceptors;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;

import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * This interceptor removes last external delivery mode if it was changed.
 *
 * Created by Uladzimir_Barouski on 6/14/2016.
 */
public class WileycomExternalDeliveryModePrepareInterceptor implements PrepareInterceptor<AbstractOrderModel>
{
	@Override
	public void onPrepare(final AbstractOrderModel abstractOrderModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		// todo (improvement) delete related interceptors
//		if (!interceptorContext.isNew(abstractOrderModel)
//				&& interceptorContext.isModified(abstractOrderModel, abstractOrderModel.DELIVERYMODE))
//		{
//			registerExternalDeliveryForRemovalIfNeeded(interceptorContext, abstractOrderModel);
//		}
	}

	private void registerExternalDeliveryForRemovalIfNeeded(final InterceptorContext ctx, final AbstractOrderModel abstractOrder)
	{
		final ItemModelContextImpl context = (ItemModelContextImpl) abstractOrder.getItemModelContext();
		final ModelValueHistory history = context.getValueHistory();
		final DeliveryModeModel oldExternalDeliveryMode = (DeliveryModeModel) history
				.getOriginalValue(OrderModel.DELIVERYMODE);
		if (oldExternalDeliveryMode != null && oldExternalDeliveryMode instanceof ExternalDeliveryModeModel)
		{
			ctx.registerElementFor(oldExternalDeliveryMode, PersistenceOperation.DELETE);
		}
	}
}
