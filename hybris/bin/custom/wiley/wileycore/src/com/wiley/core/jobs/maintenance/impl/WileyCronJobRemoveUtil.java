package com.wiley.core.jobs.maintenance.impl;

import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;


public final class WileyCronJobRemoveUtil
{
	private WileyCronJobRemoveUtil()
	{
	}

	public static boolean isRemovableMedia(final ImpExMediaModel media, final String doNotRemoveMedias)
	{
		ServicesUtil.validateParameterNotNull(media, "Parameter media cannot be null.");
		ServicesUtil.validateParameterNotNull(doNotRemoveMedias, "Parameter doNotRemoveMedias cannot be null.");

		String[] medias = doNotRemoveMedias.split(",");
		return Arrays.stream(medias).noneMatch(m ->
		{
			String mediaCode = m.replaceAll(StringUtils.SPACE, StringUtils.EMPTY);
			return media.getCode().equalsIgnoreCase(mediaCode);
		});
	}

}
