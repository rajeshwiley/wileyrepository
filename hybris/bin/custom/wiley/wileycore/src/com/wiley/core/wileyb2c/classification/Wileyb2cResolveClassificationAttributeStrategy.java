package com.wiley.core.wileyb2c.classification;

import de.hybris.platform.core.model.product.ProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cResolveClassificationAttributeStrategy
{
	String resolveAttribute(ProductModel product, Wileyb2cClassificationAttributes attribute);

	<T> T resolveAttributeValue(ProductModel product, Wileyb2cClassificationAttributes attribute);

	boolean apply(Wileyb2cClassificationAttributes attribute);
}
