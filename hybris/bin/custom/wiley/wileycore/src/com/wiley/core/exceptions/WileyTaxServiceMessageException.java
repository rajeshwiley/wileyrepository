package com.wiley.core.exceptions;

public class WileyTaxServiceMessageException extends RuntimeException
{
	public WileyTaxServiceMessageException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
