package com.wiley.core.wileycom.storesession;

import com.wiley.core.storesession.WileyStoreSessionService;


public interface WileycomStoreSessionService extends WileyStoreSessionService
{
	void setCurrentCountry(String isoCode, boolean recalculateCart);
}
