package com.wiley.core.university.dao.impl;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;
import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.dao.UniversityDao;


/**
 * Default implementation of {@link UniversityDao}.
 */
public class WileyUniversityDao implements UniversityDao
{
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<UniversityModel> getActiveUniversitiesByRegion(@Nonnull final RegionModel region)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("region", region);

		List<UniversityModel> universityModelList;

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT {u:").append(UniversityModel.PK).append("}")
				.append("FROM {University AS u JOIN Region AS r on {u:").append(UniversityModel.REGION)
				.append("} = {r:").append(RegionModel.PK)
				.append("} } WHERE {r:").append(RegionModel.ISOCODESHORT).append("} = ?isocodeShort ")
				.append("AND {u:").append(UniversityModel.ACTIVE).append("} = ?active");


		FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(sb.toString());
		searchQuery.addQueryParameter("isocodeShort", region.getIsocodeShort());
		searchQuery.addQueryParameter("active", Boolean.TRUE);

		universityModelList = flexibleSearchService.<UniversityModel> search(searchQuery).getResult();

		if (universityModelList == null)
		{
			universityModelList = new ArrayList<>();
		}

		return ImmutableList.copyOf(universityModelList);
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
