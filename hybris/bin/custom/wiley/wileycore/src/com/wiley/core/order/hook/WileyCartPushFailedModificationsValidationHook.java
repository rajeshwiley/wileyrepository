/**
 *
 */
package com.wiley.core.order.hook;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.hooks.CartValidationHook;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 *
 */
public class WileyCartPushFailedModificationsValidationHook implements CartValidationHook
{
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Override
	public void afterValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications)
	{
		final List<CommerceCartModification> failedModifications = modifications.stream()
				.filter(modification -> !modification.getStatusCode().equals(CommerceCartModificationStatus.SUCCESS))
				.collect(Collectors.toList());
		wileyFailedCartModificationsStorageService.pushAll(failedModifications);
	}

	@Override
	public void beforeValidateCart(final CommerceCartParameter parameter, final List<CommerceCartModification> modifications)
	{
		// Do nothing
	}
}
