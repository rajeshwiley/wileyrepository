package com.wiley.core.integration.esb;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface EsbB2BUnitAddressGateway
{
	String B2B_UNIT = "header_unit";

	/**
	 * Executes call to add B2B unit's shipping address into external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		This model may not be persisted in DB
	 * @param unitModel
	 * 		The B2B unit existing in both external system and hybris DB.
	 * 		This model must have not null 'sapAccountNumber' value
	 */
	String addShippingAddress(@Nonnull @Payload AddressModel addressModel,
							   @Nonnull @Header(B2B_UNIT) B2BUnitModel unitModel);
	
	/**
	 * Deletes provided address from the given unit.
	 * @param addressModel address to delete
	 * @param unitModel - unit from address will be deleted
	 */
	void deleteShippingAddress(@Nonnull @Payload String addressId,
      @Nonnull @Header(B2B_UNIT) B2BUnitModel unitModel);

	/**
	 * Executes call to update b2b unit's shipping address in external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * This address must exist in external system with id equals to given addressModel.externalId
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * @param unitModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void updateShippingAddress(@Nonnull @Payload AddressModel addressModel,
								  @Nonnull @Header(B2B_UNIT) B2BUnitModel unitModel);

}
