package com.wiley.core.setup;

import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.jalo.CoreBasicDataCreator;
import de.hybris.platform.servicelayer.tenant.TenantService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collections;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JUnitTenantSetup
{
	private static final Logger LOG = LoggerFactory.getLogger(JUnitTenantSetup.class);

	private static final String JUNIT_TENANT_ID = "junit";
	private static final String CATALOG = "wileyProductCatalog";
	private static final String FACET_SEARCH_CONFIG_NAME = "wileyb2cIndex";


	@Resource
	private ValidationService validationService;

	@Resource
	private TenantService tenantService;

	@Resource
	private SetupImpexService setupImpexService;

	@Resource
	private SetupSolrIndexerService setupSolrIndexerService;

	public void activateJunitTenantSolrIndexerCronJobs()
	{
		if (isJUnitTenant())
		{
			LOG.info("Activating SOLR index job for catalog [" + CATALOG + "]");
			setupSolrIndexerService.createSolrIndexerCronJobs(FACET_SEARCH_CONFIG_NAME);
			setupSolrIndexerService.executeSolrIndexerCronJob(FACET_SEARCH_CONFIG_NAME, true);
		}
		else
		{
			LOG.info("Skipped full SOLR index because current tenant is not JUNIT");
		}
	}

	public void createJunitTenantTestData() throws Exception
	{
		if (isJUnitTenant())
		{
			LOG.info("Creating core and sample data for Integration tests...", true);
			createCoreData();
			createCommonWileyCoreData();
			createWelCoreData();
			createWelSampleCategories();
			createWelSampleProducts();
			createAgsCoreData();
			createWileycomCoreData();
			createWileyb2cCoreData();
			createWileyb2bcCoreData();
			createWileyasCoreData();
			createWileyasSampleProducts();
			createWileycomSampleProducts();
			createWileycomSampleOrders();
		}
		else
		{
			LOG.info("Skipped integration tests data because current tenant is not JUNIT");
		}
	}

	private boolean isJUnitTenant()
	{
		return JUNIT_TENANT_ID.equals(tenantService.getCurrentTenantId());
	}

	private void createWileyb2cCoreData()
	{
		setupImpexService.importImpexFile("/wileycominitialdata/import/coredata/stores/wileyb2c/store.impex", true);
		setupImpexService.importImpexFile("/wileycominitialdata/import/coredata/stores/wileyb2c/site.impex", true);
		LOG.info("Begin importing JUNIT Integration tests solr index data for store [wileyb2c]");
		setupImpexService.importImpexFile("/wileycominitialdata/import/coredata/stores/wileyb2c/solr.impex", true);

		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyb2c/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyb2c/site.impex", true);
	}

	private void createWileyb2bcCoreData()
	{
		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyb2b/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyb2b/site.impex", true);
	}

	private void createCoreData() throws Exception
	{
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/wileyCoreData.impex", true);
		new CoreBasicDataCreator().createEssentialData(Collections.EMPTY_MAP, null);
		setupImpexService.importImpexFile("/wileycore/test/testBasics.csv", true);

	}

	/**
	 * Create site common core data.
	 */
	private void createCommonWileyCoreData()
	{
		// please keep "itemtype.impex" first in list, as it corrects data-model
		setupImpexService.importImpexFile("/wileycore/import/common/itemtype.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/wileyCoreData.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/default-constraint-group.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/languages.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/worldregion.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/countries.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/constraints.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/b2c-constraints.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/common/b2c-constraints_en.impex", true);

		setupImpexService.importImpexFile("/wileycore/test/coredata/common/delivery-modes.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/themes.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/user-groups.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/common/promotionenginesetup.impex", true);
		validationService.reloadValidationEngine();
	}

	/**
	 * Create wel specific core data.
	 */
	private void createWelCoreData()
	{
		setupImpexService.importImpexFile("/wileycore/test/coredata/wel/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wel/store.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wel/site.impex", true);
	}

	/**
	 * Create ags core data.
	 */
	private void createAgsCoreData()
	{

		setupImpexService.importImpexFile("/wileycore/test/coredata/ags/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/ags/categories.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/ags/store.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/ags/site.impex", true);
	}

	/**
	 * Create Wileycom core data.
	 */
	private void createWileycomCoreData()
	{
		setupImpexService.importImpexFile("/wileycominitialdata/import/coredata/common/common-data.impex", true);
		setupImpexService.importImpexFile("/wileycore/import/cockpits/user-groups.impex", true);

		setupImpexService.importImpexFile(
				"/wileycominitialdata/import/coredata/productCatalogs/wileyProductCatalog/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wileycom/store.impex", true);
		setupImpexService.importImpexFile(
				"/wileycominitialdata/import/coredata/productCatalogs/wileyProductCatalog/categories.impex", true);

		setupImpexService.importImpexFile(
				"/wileycominitialdata/import/coredata/contentCatalogs/commonContentCatalog/catalog.impex", true);
		setupImpexService.importImpexFile(
				"/wileycominitialdata/import/coredata/contentCatalogs/wileycomContentCatalog/catalog.impex", true);
		setupImpexService.importImpexFile("/wileycominitialdata/import/coredata/contentCatalogs/dtsContentCatalog/catalog.impex",
				true);
		setupImpexService.importImpexFile(
				"/wileycominitialdata/import/coredata/contentCatalogs/micrositesContentCatalog/catalog.impex", true);
	}

	/**
	 * Create Wileyas core data.
	 */
	private void createWileyasCoreData()
	{
		setupImpexService.importImpexFile("/wileyasinitialdata/import/coredata/common/common-data.impex", true);

		setupImpexService.importImpexFile(
				"/wileyasinitialdata/import/coredata/productCatalogs/asProductCatalog/catalog.impex", true);
		setupImpexService.importImpexFile(
				"/wileyasinitialdata/import/coredata/contentCatalogs/asContentCatalog/catalog.impex", true);

		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyas/store.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/coredata/wileyas/site.impex", true);
	}

	private void createWileyasSampleProducts()
	{
		setupImpexService.importImpexFile(
				"/wileyasinitialdata/import/sampledata/productCatalogs/asProductCatalog/products.impex", true);
		setupImpexService.importImpexFile(
				"/wileyasinitialdata/import/sampledata/productCatalogs/asProductCatalog/prices.impex", true);
	}

	/**
	 * Create wiley sample categories.
	 */
	private void createWelSampleCategories()
	{
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wel/categories.impex", true);
	}

	/**
	 * Create wiley sample products.
	 */
	private void createWelSampleProducts()
	{
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wel/products.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wel/products-prices.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wel/products-stocklevels.impex", true);
	}

	/**
	 * Create wiley sample products.
	 */
	private void createWileycomSampleProducts()
	{
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/external-companies.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/categories.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/products.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/products-classifications.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/products-prices.impex", true);
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/products-stocklevels.impex", true);
	}

	/**
	 * Create wiley sample orders.
	 */
	private void createWileycomSampleOrders()
	{
		setupImpexService.importImpexFile("/wileycore/test/sampledata/wileycom/order/digital-orders.impex", true);
	}
}
