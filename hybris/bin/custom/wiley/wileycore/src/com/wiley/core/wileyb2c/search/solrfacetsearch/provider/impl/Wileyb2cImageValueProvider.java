package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ImageValueProvider;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Created by Uladzimir_Barouski on 3/7/2017.
 */
public class Wileyb2cImageValueProvider extends ImageValueProvider
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cImageValueProvider.class);

	private String mediaFormat;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model)
			throws FieldValueProviderException
	{
		if (model instanceof WileyPurchaseOptionProductModel)
		{
			final ProductModel baseProduct = ((WileyPurchaseOptionProductModel) model).getBaseProduct();
			final Collection<FieldValue> galleryImage = super.getFieldValues(indexConfig, indexedProperty, baseProduct);
			final MediaContainerModel externalImage = baseProduct.getExternalImage();
			if (CollectionUtils.isEmpty(galleryImage) && externalImage != null)
			{
				final MediaModel externalMedia = findExternalMedia(externalImage);
				if (externalMedia != null)
				{
					return createFieldValues(indexedProperty, externalMedia);
				}
				if (LOG.isDebugEnabled())
				{
					LOG.debug("No [" + mediaFormat + "] image found for product ["
							+ ((ProductModel) model).getCode() + "]");
				}
			}
			return galleryImage;
		}
		return Collections.emptyList();
	}


	private MediaModel findExternalMedia(final MediaContainerModel externalImage)
	{
		final MediaFormatModel mediaFormatModel = getMediaService().getFormat(getMediaFormat());
		if (mediaFormatModel != null)
		{
			try
			{
				final MediaModel media = getMediaContainerService().getMediaForFormat(externalImage, mediaFormatModel);
				if (media != null)
				{
					return media;
				}
			}
			catch (final ModelNotFoundException ignore)
			{
				// ignore
			}
		}
		return null;
	}

	@Required
	public void setMediaFormat(final String mediaFormat)
	{
		super.setMediaFormat(mediaFormat);
		this.mediaFormat = mediaFormat;
	}
}
