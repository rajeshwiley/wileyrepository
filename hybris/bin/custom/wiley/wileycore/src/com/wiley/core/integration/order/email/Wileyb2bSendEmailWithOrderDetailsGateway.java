package com.wiley.core.integration.order.email;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


/**
 * Gateway interface for Wiley B2B to trigger email sending with order details in PDF form
 */
public interface Wileyb2bSendEmailWithOrderDetailsGateway
{
	String ORDER_ID = "orderId";
	String EMAIL = "email";
	String SAP_ACCOUNT_NUMBER = "sapAccountNumber";

	boolean sendEmailWithOrderDetails(@Nonnull @Payload String userId,
			@Nonnull @Header(value = EMAIL) String email,
			@Nonnull @Header(ORDER_ID) String orderId,
			@Nonnull @Header(SAP_ACCOUNT_NUMBER) String sapAccountNumber);
}
