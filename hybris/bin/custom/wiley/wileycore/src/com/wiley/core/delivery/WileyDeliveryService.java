package com.wiley.core.delivery;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;


public interface WileyDeliveryService extends DeliveryService
{

	/**
	 * Retrieves supported delivery modes, if no one found, returns empty list
	 * @param baseStore - BaseStore
	 * @param currencyModel - Currency
	 * @param countryModel - Country
	 * @return - supported delivery modes list.
	 */
	List<DeliveryModeModel> getSupportedDeliveryModes(
			BaseStoreModel baseStore,
			CurrencyModel currencyModel,
			CountryModel countryModel);

	void sortDeliveryModes(List<DeliveryModeModel> deliveryModeModels, AbstractOrderModel abstractOrder);
}
