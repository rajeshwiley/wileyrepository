package com.wiley.core.wileyb2c.subscription.services;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.core.subscription.services.WileySubscriptionService;


public interface Wileyb2cSubscriptionService extends WileySubscriptionService
{
	/**
	 * Send update to SAP ERP that customer enabled/disabled auto-renewal.
	 *
	 * @param subscription
	 *          the subscription
	 * @param requireRenewal
	 *          boolean value if require renewal or not
	 */
	void autorenewSubscription(WileySubscriptionModel subscription, boolean requireRenewal);

	/**
	 * Update subscription delivery address based on the given subscription and delivery address.
	 *
	 * @param subscription the given subscription
	 * @param deliveryAddress the updated address
	 */
	void updateDeliveryAddress(WileySubscriptionModel subscription, AddressModel deliveryAddress)
			throws SubscriptionUpdateException;

	/**
	 * Checks if customer has subscription for product
	 * @param product
	 * @param customer
	 * @return
	 */
	boolean isCustomerHasSubscription(WileyProductModel product, CustomerModel customer);

	boolean isValidSubscriptionTermForProduct(WileyProductModel productModel,
			SubscriptionTermModel orderEntrySubscriptionTerm);
}
