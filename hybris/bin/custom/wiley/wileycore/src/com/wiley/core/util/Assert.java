package com.wiley.core.util;

import java.util.function.Supplier;


/**
 * Contains util method for assertions.
 */
public final class Assert
{

	private Assert()
	{
		throw new IllegalAccessError();
	}

	public static void state(boolean expression, final Supplier<String> message)
	{
		if (!expression)
		{
			throw new IllegalStateException(message.get());
		}
	}
}
