package com.wiley.core.servicelayer.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import java.util.Collection;

import javax.annotation.Nonnull;


/**
 * Dao to access hybris items in general way
 */
public interface WileyItemDao
{
	/**
	 * Find catalog-aware items including subtypes
	 *
	 * @param type
	 * 		Root composed type of items
	 * @param versions
	 * 		Catalog version list
	 * @return Collection of {@link ItemModel}
	 */
	@Nonnull
	Collection<ItemModel> findItems(@Nonnull ComposedTypeModel type,
			@Nonnull Collection<CatalogVersionModel> versions);
}
