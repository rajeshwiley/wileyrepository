package com.wiley.core.category.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.list.SetUniqueList;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.constants.WileyCoreConstants;


/**
 * Default implementation of {@link WileyCategoryService}.
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyCategoryService extends DefaultCategoryService implements WileyCategoryService
{
	private static final String PARTNERS_CATEGORY_CODE = "WEL_PARTNERS_CATEGORY";
	private static final String GIFT_CARDS_CATEGORY_CODE = "WEL_GIFT_CARDS_CATEGORY";

	private static final Logger LOG = Logger.getLogger(DefaultWileyCategoryService.class);

	@Override
	public String getSystemRequirementsFromCategoryTree(final Collection<CategoryModel> categories,
			final Predicate<CategoryModel> categoryFilter)
	{
		if (CollectionUtils.isNotEmpty(categories))
		{
			final Collection<CategoryModel> sourceCategories = filterCategories(categories, categoryFilter);
			final String systemRequirements = getSystemRequirementsFromCategories(sourceCategories);
			if (StringUtils.isEmpty(systemRequirements))
			{
				return getSystemRequirementsFromCategoryTree(getParentCategoriesCollection(sourceCategories), categoryFilter);
			}
			return systemRequirements;
		}
		else if (LOG.isDebugEnabled())
		{
			LOG.debug("Source collection is empty.");
		}
		return null;
	}

	/**
	 * `
	 * Gets primary wiley category (ex.: CPA, CMA, PMP etc.) for product.
	 *
	 * @param product
	 * 		the product
	 * @return the primary wiley category for product
	 */
	@Override
	public CategoryModel getPrimaryWileyCategoryForProduct(@Nullable final ProductModel product)
	{
		Assert.notNull(product);

		ProductModel baseProduct = product;
		List<CategoryModel> primaryCategories = new ArrayList<>();
		if (product instanceof VariantProductModel)
		{
			baseProduct = ((VariantProductModel) product).getBaseProduct();
		}

		// Get the first super-category from the product that isn't a classification category
		for (final CategoryModel category : baseProduct.getSupercategories())
		{
			primaryCategories.addAll(getPrimaryCategories(category));
		}
		return selectTopMostCategory(primaryCategories);
	}

	private CategoryModel selectTopMostCategory(final List<CategoryModel> primaryCategories)
	{
		CategoryModel topMostCategory = null;
		if (CollectionUtils.isNotEmpty(primaryCategories))
		{
			topMostCategory = primaryCategories.stream()
					.filter(this::isNotPartnerOrGiftCardCategory)
					.findFirst()
					.orElse(primaryCategories.get(0));
		}
		return topMostCategory;
	}

	private boolean isNotPartnerOrGiftCardCategory(final CategoryModel category)
	{
		return !GIFT_CARDS_CATEGORY_CODE.equals(category.getCode())
				&& !PARTNERS_CATEGORY_CODE.equals(category.getCode());
	}

	@Override
	public CategoryModel getPrimaryWileyCategoryForVariantCategory(final VariantValueCategoryModel variantValueCategoryModel)
	{
		List<ProductModel> products = variantValueCategoryModel.getProducts();
		CategoryModel primaryCategory = null;
		if (CollectionUtils.isNotEmpty(products))
		{
			primaryCategory = getPrimaryWileyCategoryForProduct(products.get(0));
		}
		return primaryCategory;
	}

	@Override
	public CategoryModel getPartnersCategory(final CatalogVersionModel catalogVersion)
	{
		return getCategoryForCode(catalogVersion, WileyCoreConstants.WEL_PARTNERS_CATEGORY_CODE);
	}

	@Override
	public boolean isCFACategory(final CategoryModel category)
	{
		return categoryCodeEqualsTo(category, WileyCoreConstants.WEL_CFA_CATEGORY_CODE);
	}

	@Override
	public boolean isCFALevelCategory(final CategoryModel category)
	{
		return categoryCodeEqualsTo(category, WileyCoreConstants.WEL_CFA_LEVEL_CATEGORY_CODE);
	}

	@Override
	public boolean isCFALevelVariantCategory(final CategoryModel category)
	{
		return categoryCodeMatchesRegex(category, WileyCoreConstants.WEL_CFA_LEVEL_VARIANT_CATEGORY_REGEX);
	}

	@Override
	public boolean isFreeTrialCategory(final CategoryModel category)
	{
		return categoryCodeMatchesRegex(category, WileyCoreConstants.WEL_FREE_TRIAL_CATEGORY_REGEX);
	}

	@Override
	public boolean isGiftCardsCategory(final CategoryModel category)
	{
		return categoryCodeEqualsTo(category, WileyCoreConstants.WEL_GIFT_CARDS_CATEGORY_CODE);
	}

	@Override
	public boolean isPartnersCategory(final CategoryModel category)
	{
		boolean isPartnersCategory = categoryCodeEqualsTo(category, WileyCoreConstants.WEL_PARTNERS_CATEGORY_CODE);
		if (!isPartnersCategory && category != null)
		{
			isPartnersCategory = getAllSupercategoriesForCategory(category)
					.stream()
					.anyMatch(superCategory -> categoryCodeEqualsTo(superCategory,
							WileyCoreConstants.WEL_PARTNERS_CATEGORY_CODE));
		}
		return isPartnersCategory;
	}

	@Override
	public boolean isSupplementsCategory(final CategoryModel category)
	{
		return categoryCodeMatchesRegex(category, WileyCoreConstants.WEL_SUPPLEMENTS_CATEGORY_REGEX);
	}

	private boolean categoryCodeMatchesRegex(final CategoryModel category, final String codeRegex)
	{
		return category != null && category.getCode() != null && category.getCode().matches(codeRegex);
	}

	private boolean categoryCodeEqualsTo(final CategoryModel category, final String code)
	{
		return category != null && category.getCode() != null && category.getCode().equals(code);
	}


	private List<CategoryModel> getPrimaryCategories(final CategoryModel category)
	{
		List<CategoryModel> primaryCategories = new ArrayList<>();

		if (!isNotPrimaryCategory(category))
		{
			primaryCategories.add(category);
		}
		primaryCategories.addAll(getPrimarySuperCategories(category));
		return primaryCategories;
	}

	private List<CategoryModel> getPrimarySuperCategories(final CategoryModel category)
	{
		List<CategoryModel> primaryCategories = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(category.getSupercategories()))
		{
			for (CategoryModel superCategory : category.getSupercategories())
			{
				primaryCategories.addAll(getPrimaryCategories(superCategory));
			}
		}
		return primaryCategories;
	}

	private boolean isNotPrimaryCategory(final CategoryModel category)
	{
		return !category.getClass().equals(CategoryModel.class)
						|| !catalogVersionRootCategoriesIncludes(category);
	}

	private boolean catalogVersionRootCategoriesIncludes(final CategoryModel category)
	{
		return category.getCatalogVersion().getRootCategories().stream()
				.anyMatch(category::equals);
	}

	Collection<CategoryModel> filterCategories(final Collection<CategoryModel> source, final Predicate<CategoryModel>
			categoryFilter)
	{
		if (categoryFilter != null)
		{
			return source.stream().filter(categoryFilter).collect(Collectors.toList());
		}
		else if (LOG.isDebugEnabled())
		{
			LOG.debug("categoryFilter is null.");
		}
		return source;
	}

	Collection<CategoryModel> getParentCategoriesCollection(final Collection<CategoryModel> categories)
	{
		if (CollectionUtils.isNotEmpty(categories))
		{
			List<CategoryModel> parents = new ArrayList<>();
			List<CategoryModel> uniqueParents = SetUniqueList.decorate(parents);
			for (CategoryModel category : categories)
			{
				uniqueParents.addAll(category.getSupercategories());
			}
			return parents;
		}
		else if (LOG.isDebugEnabled())
		{
			LOG.debug("Source collection is empty.");
		}
		return Collections.emptyList();
	}

	private String getSystemRequirementsFromCategories(final Collection<CategoryModel> categories)
	{
		if (CollectionUtils.isNotEmpty(categories))
		{
			return categories.stream().map(CategoryModel::getSystemRequirements).filter(StringUtils::isNotEmpty).findFirst()
					.orElse(null);
		}
		return null;
	}

	@Override
	public List<CategoryModel> getTopLevelCategoriesForCategory(final CategoryModel categoryModel)
	{
		if (CollectionUtils.isEmpty(categoryModel.getSupercategories())
				|| categoryModel.getSupercategories()
				.stream()
				.allMatch(category -> (category instanceof ClassificationClassModel))
				)
		{
			return Collections.singletonList(categoryModel);
		}

		List<CategoryModel> topLevelCategories = new LinkedList<>();
		for (CategoryModel category : categoryModel.getSupercategories())
		{
			if (category instanceof ClassificationClassModel)
			{
				continue;
			}
			topLevelCategories.addAll(getTopLevelCategoriesForCategory(category));
		}
		return topLevelCategories;
	}

	@Override
	public boolean isCategoryForCodeExists(final CatalogVersionModel catalogVersion, final String categoryCode)
	{
		try
		{
			getCategoryForCode(catalogVersion, categoryCode);
			return true;
		}
		catch (UnknownIdentifierException e)
		{
			return false;
		}
	}

	@Override
	public List<CategoryModel> getSubcategoriesWithDescendantProducts(final CategoryModel categoryModel)
	{
		List<CategoryModel> subCategories = categoryModel.getCategories();
		subCategories = subCategories.stream()
				.filter(this::hasDescendantProducts)
				.collect(Collectors.toList());
		return subCategories;
	}

	private boolean hasDescendantProducts(final CategoryModel category)
	{
		boolean result = CollectionUtils.isNotEmpty(category.getProducts());
		if (!result)
		{
			result = category.getCategories().stream()
					.anyMatch(this::hasDescendantProducts);
		}
		return result;
	}
}
