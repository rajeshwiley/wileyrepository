package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreditCardPaymentInfoCreateStrategy;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.paymentinfo.CardPaymentInfoCcOwnerGenerator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Strategy provides additional functionality which is necessary for Wiley.
 */
public class WileyCreditCardPaymentInfoCreateStrategy extends DefaultCreditCardPaymentInfoCreateStrategy
{
	private static final String AS_SITE = "asSite";

	@Resource
	private CMSSiteService cmsSiteService;
	@Resource
	private CardPaymentInfoCcOwnerGenerator wileyCardPaymentInfoCcOwnerGenerator;

	@Override
	public CreditCardPaymentInfoModel createCreditCardPaymentInfo(final SubscriptionInfoData subscriptionInfo,
			final PaymentInfoData paymentInfo, final AddressModel billingAddress, final CustomerModel customerModel,
			final boolean saveInAccount)
	{
		validateParameterNotNull(subscriptionInfo, "subscriptionInfo cannot be null");
		validateParameterNotNull(paymentInfo, "paymentInfo cannot be null");
		validateParameterNotNull(customerModel, "customerModel cannot be null");

		final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);
		// The payment info provided for the cart or the order.
		// For Phase 4 the payment info won't contain the billing address
		// since this information will be provided as a separate checkout step and stored in paymentAddress.
		final String siteUid = getCmsSiteService().getCurrentSite().getUid();
		if (!siteUid.equals(AS_SITE))
		{
			validateParameterNotNull(billingAddress, "billingAddress cannot be null");
			billingAddress.setBillingAddress(true);
			cardPaymentInfoModel.setBillingAddress(billingAddress);
		}
		cardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
		cardPaymentInfoModel.setUser(customerModel);
		cardPaymentInfoModel.setSubscriptionId(subscriptionInfo.getSubscriptionID());
		cardPaymentInfoModel.setNumber(paymentInfo.getCardAccountNumber());
		cardPaymentInfoModel.setType(CreditCardType.valueOf(CardTypeEnum.get(paymentInfo.getCardCardType()).name()
				.toUpperCase()));
		cardPaymentInfoModel.setCcOwner(getCCOwner(paymentInfo, billingAddress));
		cardPaymentInfoModel.setValidFromMonth(paymentInfo.getCardStartMonth());
		cardPaymentInfoModel.setValidFromYear(paymentInfo.getCardStartYear());
		if (paymentInfo.getCardExpirationMonth().intValue() > 0)
		{
			cardPaymentInfoModel.setValidToMonth(String.valueOf(paymentInfo.getCardExpirationMonth()));
		}
		if (paymentInfo.getCardExpirationYear().intValue() > 0)
		{
			cardPaymentInfoModel.setValidToYear(String.valueOf(paymentInfo.getCardExpirationYear()));
		}

		cardPaymentInfoModel.setSubscriptionId(subscriptionInfo.getSubscriptionID());
		cardPaymentInfoModel.setSaved(saveInAccount);
		if (StringUtils.isNotBlank(paymentInfo.getCardIssueNumber()))
		{
			cardPaymentInfoModel.setIssueNumber(Integer.valueOf(paymentInfo.getCardIssueNumber()));
		}

		return cardPaymentInfoModel;
	}

	@Override
	public CreditCardPaymentInfoModel saveSubscription(final CustomerModel customerModel, final CustomerInfoData customerInfoData,
			final SubscriptionInfoData subscriptionInfo, final PaymentInfoData paymentInfoData, final boolean saveInAccount)
	{
		validateParameterNotNull(customerInfoData, "customerInfoData cannot be null");
		validateParameterNotNull(subscriptionInfo, "subscriptionInfo cannot be null");
		validateParameterNotNull(paymentInfoData, "paymentInfoData cannot be null");

		final AddressModel billingAddress = getModelService().create(AddressModel.class);
		billingAddress.setFirstname(customerInfoData.getBillToFirstName());
		billingAddress.setLastname(customerInfoData.getBillToLastName());
		billingAddress.setLine1(customerInfoData.getBillToStreet1());
		billingAddress.setLine2(customerInfoData.getBillToStreet2());
		billingAddress.setTown(customerInfoData.getBillToCity());
		billingAddress.setPostalcode(customerInfoData.getBillToPostalCode());
		billingAddress.setExternalId(customerInfoData.getBillToExternalId());

		if (StringUtils.isNotBlank(customerInfoData.getBillToTitleCode()))
		{
			billingAddress.setTitle(getUserService().getTitleForCode(customerInfoData.getBillToTitleCode()));
		}

		final CountryModel country = getCommonI18NService().getCountry(customerInfoData.getBillToCountry());
		billingAddress.setCountry(country);
		if (StringUtils.isNotEmpty(customerInfoData.getBillToState()))
		{
			billingAddress.setRegion(getCommonI18NService().getRegion(country,
					country.getIsocode() + "-" + customerInfoData.getBillToState()));
		}
		final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
		billingAddress.setEmail(email);

		final CreditCardPaymentInfoModel cardPaymentInfoModel = createCreditCardPaymentInfo(subscriptionInfo, paymentInfoData,
				billingAddress, customerModel, saveInAccount);

		// Phone1 is required for sending it to Eloqua.
		billingAddress.setPhone1(customerInfoData.getBillToPhoneNumber());
		// mark address as billing
		billingAddress.setBillingAddress(true);
		billingAddress.setOwner(cardPaymentInfoModel);

		if (CustomerType.GUEST.equals(customerModel.getType()))
		{
			final StringBuilder name = new StringBuilder();
			if (!StringUtils.isBlank(customerInfoData.getBillToFirstName()))
			{
				name.append(customerInfoData.getBillToFirstName());
				name.append(' ');
			}
			if (!StringUtils.isBlank(customerInfoData.getBillToLastName()))
			{
				name.append(customerInfoData.getBillToLastName());
			}
			customerModel.setName(name.toString());
			getModelService().save(customerModel);
		}

		getModelService().saveAll(cardPaymentInfoModel, billingAddress);
		getModelService().refresh(customerModel);

		final List<PaymentInfoModel> paymentInfoModels = new ArrayList<PaymentInfoModel>(customerModel.getPaymentInfos());
		if (!paymentInfoModels.contains(cardPaymentInfoModel))
		{
			paymentInfoModels.add(cardPaymentInfoModel);
			if (saveInAccount)
			{
				customerModel.setPaymentInfos(paymentInfoModels);
				getModelService().save(customerModel);
			}

			getModelService().save(cardPaymentInfoModel);
			getModelService().refresh(customerModel);
		}
		return cardPaymentInfoModel;
	}

	@Override
	protected String getCCOwner(final PaymentInfoData paymentInfo, final AddressModel billingAddress)
	{
		String result = paymentInfo.getCardAccountHolderName();
		if (StringUtils.isEmpty(result) && billingAddress != null)
		{
			result = wileyCardPaymentInfoCcOwnerGenerator.getCCOwnerFromBillingAddress(billingAddress).orElse(null);
		}
		if (StringUtils.isEmpty(result))
		{
			result = StringUtils.EMPTY;
		}
		return result;
	}

	protected CMSSiteService getCmsSiteService()
	{
		return cmsSiteService;
	}
}
