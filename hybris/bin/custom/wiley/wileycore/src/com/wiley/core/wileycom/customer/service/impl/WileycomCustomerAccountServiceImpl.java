package com.wiley.core.wileycom.customer.service.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.PasswordCheckingStrategy;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.customer.service.impl.WileyCustomerAccountServiceImpl;
import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;
import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.users.strategy.WileycomChangeUserUidStrategy;
import com.wiley.core.wileycom.customer.dao.WileycomCustomerAccountDao;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.core.wileycom.users.service.WileycomUsersService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileycomCustomerAccountServiceImpl extends WileyCustomerAccountServiceImpl implements WileycomCustomerAccountService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomCustomerAccountServiceImpl.class);

	@Resource
	private PasswordCheckingStrategy wileyPasswordCheckingStrategy;

	@Resource
	private WileycomCustomerAccountDao wileycomCustomerAccountDao;

	@Resource
	private WileycomChangeUserUidStrategy wileycomChangeUserUidStrategy;

	@Resource(name = "wileycomCustomerRegistrationStrategy")
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategy;

	@Resource
	private WileyCustomerGateway wileyCustomerGateway;

	@Resource
	private WileycomUsersService wileycomUsersService;

	@Override
	public SearchPageData<OrderEntryModel> getDigitalOrderEntries(final CustomerModel customer,
			final BaseStoreModel baseStore,
			final PageableData pageableData)
	{
		validateParameterNotNull(customer, "Customer model cannot be null");
		validateParameterNotNull(baseStore, "Store must not be null");
		validateParameterNotNull(pageableData, "PageableData must not be null");
		return wileycomCustomerAccountDao.findDigitalOrderEntriesByOwner(customer, baseStore, pageableData);
	}

	@Override
	public Optional<NameSuffixModel> getNameSuffixByCode(final String suffixCode)
	{
		return wileycomCustomerAccountDao.findNameSuffixByCode(suffixCode);
	}

	@Override
	public Optional<SchoolModel> getSchoolByCode(final String schoolCode)
	{
		return wileycomCustomerAccountDao.findSchoolByCode(schoolCode);
	}

	@Override
	public List<NameSuffixModel> getAllNameSuffixes()
	{
		return wileycomCustomerAccountDao.findAllNameSuffixes();
	}

	@Override
	public List<SchoolModel> getAllSchools()
	{
		return wileycomCustomerAccountDao.findAllSchools();
	}

	@Override
	public List<SchoolModel> getSchoolsWithLimitation(int range, int maxQuantity)
	{
		return wileycomCustomerAccountDao.findSchoolsWithLimitation(range, maxQuantity);
	}

	@Override
	public void changeUid(final String newUid, final String currentPassword)
			throws DuplicateUidException, PasswordMismatchException
	{
		wileycomChangeUserUidStrategy.changeUid(newUid, currentPassword);
	}

	@Override
	public void changePassword(final UserModel userModel, final String oldPassword, final String newPassword)
			throws PasswordMismatchException
	{
		validateParameterNotNullStandardMessage("userModel", userModel);
		validateParameterNotNullStandardMessage("oldPassword", oldPassword);
		validateParameterNotNullStandardMessage("newPassword", newPassword);

		if (!getUserService().isAnonymousUser(userModel))
		{
			final User user = getModelService().toPersistenceLayer(userModel);
			if (wileyPasswordCheckingStrategy.checkPassword(user, oldPassword))
			{
				wileycomUsersService.updatePassword(userModel, oldPassword, newPassword);
			}
			else
			{
				throw new PasswordMismatchException(userModel.getUid());
			}
		}
	}

	@Override
	public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException
	{
		try
		{
			prepareCustomerModelForRegistration(customerModel);
			registerCustomerInExternalWileycomSystems(customerModel, password);
			registerCustomerInLocalHybrisSystem(customerModel, password);
		}
		catch (ExternalSystemException e)
		{
			String message = "Wiley external customer registration failed due to malformed request or system is unreachable!";
			LOG.error(message, e);
			throw new WileycomCustomerRegistrationFailureException(message, e);
		}
	}

	@Override
	protected void registerCustomer(final CustomerModel customerModel, final String password) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (password != null)
		{
			getUserService().setPassword(customerModel, password, getPasswordEncoding());
		}

		internalSaveCustomer(customerModel);
	}

	@Override
	public void externalResetCustomerPassword(final String uid, final String newPassword) throws PasswordUpdateException
	{
		validateParameterNotNullStandardMessage("uid", uid);
		validateParameterNotNullStandardMessage("newPassword", newPassword);

		try
		{
			wileyCustomerGateway.forceResetPassword(uid, newPassword);
		}
		catch (ExternalSystemException exception)
		{
			throw new PasswordUpdateException("External reset password process failed!", exception);
		}
	}

	/**
	 * Update the password for the user by decrypting and validating the token (please refer to
	 * {@link de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService#updatePassword(String,String)}).
	 */
	@Override
	public void updatePassword(final String token, final String newPassword) throws TokenInvalidatedException
	{
		Assert.hasText(token, "The field [token] cannot be empty");
		Assert.hasText(newPassword, "The field [newPassword] cannot be empty");

		final SecureToken data = getSecureTokenService().decryptData(token);
		if (getTokenValiditySeconds() > 0L)
		{
			final long delta = new Date().getTime() - data.getTimeStamp();
			if (delta / 1000 > getTokenValiditySeconds())
			{
				throw new IllegalArgumentException("token expired");
			}
		}

		final CustomerModel customer = getUserService().getUserForUID(data.getData(), CustomerModel.class);
		if (customer == null)
		{
			throw new IllegalArgumentException("user for token not found");
		}
		if (!token.equals(customer.getToken()))
		{
			throw new TokenInvalidatedException();
		}
		customer.setToken(null);
		customer.setLoginDisabled(false);
		getModelService().save(customer);

		// The password is not handled by Hybris in case of Wileycom pages.
		externalResetCustomerPassword(customer.getUid(), newPassword);
	}
	
	@Override
	public void updateCustomerProfile(final CustomerModel customerModel) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		internalSaveCustomer(customerModel);
		wileyCustomerGateway.updateCustomer(customerModel);
	}
	
	private void prepareCustomerModelForRegistration(final CustomerModel customerModel)
	{
		generateCustomerId(customerModel);
	}

	private void registerCustomerInExternalWileycomSystems(final CustomerModel customerModel, final String password)
	{
		wileyCustomerRegistrationStrategy.register(customerModel, password);
	}

	private void registerCustomerInLocalHybrisSystem(final CustomerModel customerModel, final String password)
			throws DuplicateUidException
	{
		registerCustomer(customerModel, password);
		getEventService().publishEvent(initializeEvent(new RegisterEvent(), customerModel));
	}
}

