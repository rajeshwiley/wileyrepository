package com.wiley.core.wileycom.seo.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;



public class WileyCatalogAwareSeoUrlResolver<T> extends AbstractUrlResolver<T>
{
	private Map<String, AbstractUrlResolver<T>> catalogResolvers;
	private AbstractUrlResolver<T> defaultResolver;

	@Override
	protected String resolveInternal(final T source)
	{
		String catalogId = getItemCatalogId(source);
		if (catalogId != null && catalogResolvers.containsKey(catalogId))
		{
			return catalogResolvers.get(catalogId).resolve(source);
		}
		else
		{
			return defaultResolver.resolve(source);
		}
	}

	private String getItemCatalogId(final T source)
	{
		CatalogModel catalog = null;
		if (source instanceof ProductModel)
		{
			catalog = ((ProductModel) source).getCatalogVersion().getCatalog();
		}
		else if (source instanceof CategoryModel)
		{
			catalog = ((CategoryModel) source).getCatalogVersion().getCatalog();
		}

		return catalog != null ? catalog.getId() : null;
	}

	@Required
	public void setCatalogResolvers(
			final Map<String, AbstractUrlResolver<T>> catalogResolvers)
	{
		this.catalogResolvers = catalogResolvers;
	}

	@Required
	public void setDefaultResolver(final AbstractUrlResolver<T> defaultResolver)
	{
		this.defaultResolver = defaultResolver;
	}
}
