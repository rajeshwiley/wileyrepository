package com.wiley.core.wileybundle.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.wiley.core.model.WileyBundleModel;


public interface WileyBundleService
{
	List<WileyBundleModel> getBundlesForProduct(ProductModel productModel);
}
