package com.wiley.core.payment.strategies;

import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import de.hybris.platform.core.model.order.AbstractOrderModel;


public interface CreateSubscriptionAuthorizeRequestStrategy
{
	WileySubscriptionAuthorizeRequest createRequest(AbstractOrderModel order);
}
