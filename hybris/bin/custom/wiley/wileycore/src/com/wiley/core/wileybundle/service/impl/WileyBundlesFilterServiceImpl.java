package com.wiley.core.wileybundle.service.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.servicelayer.i18n.impl.WileycomI18NServiceImpl;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileybundle.service.WileyBundlesFilterService;


class WileyBundlesFilterServiceImpl implements WileyBundlesFilterService
{
	private final WileyProductRestrictionService wileyb2cProductRestrictionService;
	private final ProductService productService;
	private final WileycomI18NServiceImpl wileycomI18NService;

	@Autowired
	WileyBundlesFilterServiceImpl(
			@Qualifier("wileyb2cProductRestrictionService")
			final WileyProductRestrictionService wileyb2cProductRestrictionService,
			final ProductService productService,
			final WileycomI18NServiceImpl wileycomI18NService)
	{
		this.wileyb2cProductRestrictionService = wileyb2cProductRestrictionService;
		this.productService = productService;
		this.wileycomI18NService = wileycomI18NService;
	}

	@Override
	public List<WileyBundleModel> filterBundles(@Nonnull final List<WileyBundleModel> bundles) {
		Preconditions.checkNotNull(bundles, "Bundles should no be null");

		List<WileyBundleModel> resultBundles = bundles;
		resultBundles = filterNotVisibleBundles(resultBundles);

		final Optional<CountryModel> currentCountryOpt = wileycomI18NService.getCurrentCountry();
		Preconditions.checkState(currentCountryOpt.isPresent(),
				"Can find session country during retrieving WileyBundles");
		final CountryModel currentCountry = currentCountryOpt.get();



		final List<WileyBundleModel> bundlesForCurrentCountry = filterBundlesForCountry(resultBundles, currentCountry);

		if (CollectionUtils.isNotEmpty(bundlesForCurrentCountry)) {
			return bundlesForCurrentCountry;
		}

		resultBundles = filterBundlesForCountrysFallbackCountries(resultBundles, currentCountry);

		return resultBundles;
	}

	private List<WileyBundleModel> filterNotVisibleBundles(final List<WileyBundleModel> bundles)
	{
		return bundles.stream()
				.filter(bundle -> {
					final ProductModel upsellProduct = productService.getProductForCode(bundle.getUpsellProductCode());
					return wileyb2cProductRestrictionService.isVisible(upsellProduct)
							&& wileyb2cProductRestrictionService.isAvailable(upsellProduct);
				})
				.collect(Collectors.toList());
	}

	private List<WileyBundleModel> filterBundlesForCountry(final List<WileyBundleModel> bundles,
			final CountryModel country) {

		List<WileyBundleModel> filteredBundles = bundles.stream().filter(bundleModel -> {
			final CountryModel bundleCountry = bundleModel.getCountry();
			if (bundleCountry == null) {
				return false;
			}
			boolean isBundleCountryIsTheSameAsCurrent = country.getIsocode().equals(bundleCountry.getIsocode());

			return isBundleCountryIsTheSameAsCurrent;
		}).collect(Collectors.toList());

		return filteredBundles;
	}

	private List<WileyBundleModel> filterBundlesForCountrysFallbackCountries(final List<WileyBundleModel> bundles,
			final CountryModel country) {

		final List<CountryModel> currentCountryAndFallbacks = wileycomI18NService.getFallbackCountries(country);

		final List<String> currentCountryAndFallbacksIsoCodes =
				currentCountryAndFallbacks.stream().map(CountryModel::getIsocode).collect(Collectors.toList());

		List<WileyBundleModel> filteredBundles = bundles.stream().filter(bundleModel -> {
			final CountryModel bundleCountry = bundleModel.getCountry();
			if (bundleCountry == null) {
				return false;
			}
			boolean isBundleCountryBelongsToFallbacks = currentCountryAndFallbacksIsoCodes.contains(bundleCountry.getIsocode());

			return isBundleCountryBelongsToFallbacks;
		}).collect(Collectors.toList());

		return filteredBundles;
	}
}
