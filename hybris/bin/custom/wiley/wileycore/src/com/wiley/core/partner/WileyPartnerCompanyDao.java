package com.wiley.core.partner;

import de.hybris.platform.servicelayer.user.daos.UserGroupDao;

import java.util.List;

import com.wiley.core.model.WileyPartnerCompanyModel;


/**
 * DAO for {@link WileyPartnerCompanyModel}.
 */
public interface WileyPartnerCompanyDao extends UserGroupDao
{
	List<WileyPartnerCompanyModel> getPartnersByParentUserGroup(String userGroupUid);

	List<WileyPartnerCompanyModel> getPartnersByCategory(String categoryCode);
}
