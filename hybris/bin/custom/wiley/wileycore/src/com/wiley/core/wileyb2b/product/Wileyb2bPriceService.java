package com.wiley.core.wileyb2b.product;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Price service for Wiley B2B sites which retrieves price data from external synchronous service.
 */
public class Wileyb2bPriceService extends AbstractBusinessService implements PriceService
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bPriceService.class);

	@Resource
	private EsbCartCalculationGateway esbCartCalculationGateway;

	@Resource
	private UserService userService;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	@Resource
	private B2BUnitService b2bUnitService;



	@Override
	public List<PriceInformation> getPriceInformationsForProduct(final ProductModel productModel)
	{
		try
		{
			final CustomerModel currentCustomer = getCustomerModel();

			final String sapAccountNumber = checkCurrentUserAndQuerySapnumber(currentCustomer);
			final String sessionCountryIsoCode = getSessionCountryIsoCode();
			final String sessionCurrencyIsoCode = getSessionCurrencyIsoCode();

			if (StringUtils.isNotEmpty(sapAccountNumber) && StringUtils.isNotEmpty(sessionCountryIsoCode) && StringUtils
					.isNotEmpty(sessionCurrencyIsoCode))
			{
				return esbCartCalculationGateway.getProductPriceInformations(productModel, sapAccountNumber,
						sessionCountryIsoCode, sessionCurrencyIsoCode);
			}
			else
			{
				LOG.debug("Getting business prices is skipped: sapNumber={} country={} currency={}", sapAccountNumber,
						sessionCountryIsoCode, sessionCurrencyIsoCode);
			}
		}
		catch (final ExternalSystemException ese)
		{
			LOG.error("External system detected an exception during Commerce price retrieving for product {}",
					productModel.getCode(), ese);
		}
		catch (final RuntimeException rte)
		{
			LOG.error("Unexpected exception during Commerce price retrieving for product {}", productModel.getCode(), rte);
		}
		// The fallback price is an empty list, that will be transformed into null-price on the screen.
		return getFallbackPrice();

	}

	private List<PriceInformation> getFallbackPrice()
	{
		return Collections.emptyList();
	}

	private String checkCurrentUserAndQuerySapnumber(final CustomerModel currentCustomer)
	{
		if (userService.isAnonymousUser(currentCustomer))
		{
			LOG.debug("An attempt to get business prices for anonymous");
			return null;
		}
		if (!(currentCustomer instanceof B2BCustomerModel))
		{
			LOG.debug("B2BCustomerModel was expected. Customer = " + currentCustomer);
			return null;
		}
		final CompanyModel company = b2bUnitService.getParent((B2BCustomerModel) currentCustomer);
		if (!(company instanceof B2BUnitModel))
		{
			LOG.debug("B2BUnitModel was expected. Company = " + company);
			return null;
		}
		return ((B2BUnitModel) company).getSapAccountNumber();
	}

	private String getSessionCountryIsoCode()
	{
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			return currentCountry.get().getIsocode();
		}
		else
		{
			return null;
		}
	}

	private String getSessionCurrencyIsoCode()
	{
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			final CurrencyModel currentCurrency = wileyCommonI18NService.getDefaultCurrency(
					currentCountry.get());
			if (currentCurrency != null)
			{
				return currentCurrency.getIsocode();
			}
		}
		return null;
	}

	private CustomerModel getCustomerModel()
	{
		return (CustomerModel) userService.getCurrentUser();
	}
}