package com.wiley.core.order.strategies.calculation.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.PriceValue;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.order.strategies.calculation.WileyFindExternalPriceStrategy;


/**
 * Implementation for searching external price factory.
 * Doesn't rely on price factory to avoid jalo layer conversion.
 */
public class WileyFindExternalPriceStrategyImpl implements WileyFindExternalPriceStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyFindExternalPriceStrategyImpl.class);

	@Override
	@Nullable
	public PriceValue findExternalPrice(final AbstractOrderEntryModel entry)
	{
		final AbstractOrderModel order = entry.getOrder();
		final String currencyIso = order.getCurrency().getIsocode();

		if (LOG.isDebugEnabled())
		{
			LOG.debug("AbstractOrderEntryModel.externalPriceValuesInternal=[{}] for order.code=[{}], entry with product=[{}]",
					entry.getExternalPriceValuesInternal(), order.getCode(), entry.getProduct().getCode());
		}

		PriceValue priceValue = null;
		final List<PriceValue> externalPriceValues = entry.getExternalPriceValues();
		if (CollectionUtils.isNotEmpty(externalPriceValues))
		{
			//null net attribute is the same as false, see jalo GeneratedAbstractOrder.isNetAsPrimitive
			final boolean isNet = Boolean.TRUE.equals(order.getNet());

			List<PriceValue> filteredPriceValues = externalPriceValues.stream()
					.filter(value -> value.isNet() == isNet && currencyIso.equals(value.getCurrencyIso()))
					.collect(Collectors.toList());

			if (filteredPriceValues.size() > 1)
			{
				LOG.warn("More then one external price value found [{}] for order.code=[{}] "
								+ "with currency.isocode=[{}] and order.net=[{}]",
						filteredPriceValues, order.getCode(), currencyIso, order.getNet());
			}

			if (CollectionUtils.isNotEmpty(filteredPriceValues))
			{
				priceValue = filteredPriceValues.get(0);
			}

		}

		LOG.debug("Return price value [{}] for AsbtractOrder.code=[{}] with currency=[{}] and AsbtractOrder.net=[{}]",
				priceValue, order.getCode(), currencyIso, order.getNet());
		return priceValue;
	}
}
