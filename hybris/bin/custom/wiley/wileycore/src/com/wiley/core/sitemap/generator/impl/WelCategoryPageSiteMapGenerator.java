package com.wiley.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.CategoryPageSiteMapGenerator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.util.url.UrlVisibilityStrategy;


public class WelCategoryPageSiteMapGenerator extends CategoryPageSiteMapGenerator
{
	@Resource
	private UrlVisibilityStrategy welCategoryUrlVisibilityStrategy;

	@Override
	protected List<CategoryModel> getDataInternal(final CMSSiteModel siteModel)
	{
		//in the custom query, restrictions' condition was excluded
		final String query = "SELECT {c.pk} FROM {Category AS c JOIN CatalogVersion AS cv ON {c.catalogVersion}={cv.pk} "
				+ " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
				+ " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site ";

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", siteModel);
		return doSearch(query, params, CategoryModel.class)
				.stream()
				.filter(category -> welCategoryUrlVisibilityStrategy.isUrlForItemVisible(category))
				.collect(Collectors.toList());
	}

}
