package com.wiley.core.payment.strategies;

public interface WPGRegionStrategy
{
	String getRegionByCurrency(String currency);
}
