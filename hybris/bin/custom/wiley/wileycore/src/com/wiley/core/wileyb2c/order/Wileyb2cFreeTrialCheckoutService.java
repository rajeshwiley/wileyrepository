package com.wiley.core.wileyb2c.order;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;


/**
 * Created by Uladzimir_Barouski on 9/20/2016.
 */
public interface Wileyb2cFreeTrialCheckoutService
{
	/**
	 * Place Free Trial Order
	 *
	 * @param code Free Trial code
	 * @return order code
	 */
	String placeFreeTrialOrder(String code) throws CommerceCartModificationException, InvalidCartException;
}
