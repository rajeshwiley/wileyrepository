package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartMergingStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;
import java.util.Objects;

import org.springframework.security.access.AccessDeniedException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Implementation for wileyb2c.
 */
public class Wileyb2cCommerceCartMergingStrategyImpl extends DefaultCommerceCartMergingStrategy
{
	/**
	 * OOTB_CODE
	 * This method is based on {@link DefaultCommerceCartMergingStrategy#validationBeforeMerge(CartModel, CartModel, List,
	 * UserModel)}.
	 * Custom logic is placed in {@link #isSameBaseStore(CartModel)}.
	 * TODO-1808: setting de.hybris.platform.commerceservices.service.data.CommerceCartParameter#setSubscriptionTerm
	 * removed during migration to 1808.
	 * It can be the gap for Phase 2 reanimation. Compare the 6.2 functionality if needed
	 *
	 * @param fromCart
	 * @param toCart
	 * @param modifications
	 * @param currentUser
	 * @throws CommerceCartMergingException
	 */
	protected void validationBeforeMerge(final CartModel fromCart, final CartModel toCart,
			final List<CommerceCartModification> modifications, final UserModel currentUser) throws CommerceCartMergingException
	{
		if (currentUser == null || getUserService().isAnonymousUser(currentUser))
		{
			throw new AccessDeniedException("Only logged user can merge carts!");
		}

		validateParameterNotNullStandardMessage("fromCart", fromCart);
		validateParameterNotNullStandardMessage("toCart", toCart);
		validateParameterNotNullStandardMessage("modifications", modifications);

		if (!isSameBaseStore(fromCart))
		{
			throw new CommerceCartMergingException(String.format("Current store %s is not equal to cart %s store %s",
					getBaseStoreService().getCurrentBaseStore().getName(), fromCart.getCode(), fromCart.getStore().getName()));
		}

		if (!isSameBaseStore(toCart))
		{
			throw new CommerceCartMergingException(String.format("Current store %s is not equal to cart %s store %s",
					getBaseStoreService().getCurrentBaseStore().getName(), toCart.getCode(), toCart.getStore().getName()));
		}

		if (Objects.equals(fromCart.getGuid(), toCart.getGuid()))
		{
			throw new CommerceCartMergingException("Cannot merge cart to itself!");
		}
	}

	private boolean isSameBaseStore(final CartModel toCart)
	{
		return getBaseStoreService().getCurrentBaseStore().equals(toCart.getStore());
	}
}
