package com.wiley.core.pages.dao.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForTemplateModel;
import de.hybris.platform.cms2.servicelayer.daos.impl.DefaultCMSPageDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.FlexibleSearchUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.pages.dao.WileyCMSPageDao;


public class WileyCMSPageDaoImpl extends DefaultCMSPageDao implements WileyCMSPageDao
{

	private static final String FIND_PAGES_BY_COMPONENT =
			"SELECT uniontable.PK FROM ( "
					+ "{{SELECT {apm." + AbstractPageModel.PK + "} FROM "
					+ "{" + AbstractPageModel._TYPECODE + " as apm "
					+ "JOIN " + ContentSlotForPageModel._TYPECODE + " as csfpm on {csfpm.page}={apm.PK} "
					+ " JOIN " + ContentSlotModel._TYPECODE + " as csm on {csfpm.contentSlot}={csm.pk} "
					+ " JOIN " + AbstractCMSComponentModel._ELEMENTSFORSLOT + " as elForSlot on {csm.pk}={elForSlot.source} "
					+ " JOIN " + AbstractCMSComponentModel._TYPECODE + " as c on {c.pk}={elForSlot.target} "
					+ " JOIN " + CatalogVersionModel._TYPECODE + " as cv on {c.catalogVersion}={cv.pk} "
					+ " JOIN " + CatalogModel._TYPECODE + " as catalog on {cv.catalog}={catalog.pk}"
					+ "} WHERE {c." + AbstractCMSComponentModel.UID + "} = ?uid AND"
					+ " {cv." + CatalogVersionModel.VERSION + "} LIKE ?catalogVersion}} "
					+ " UNION ALL "
					+ " {{SELECT {apm." + AbstractPageModel.PK + "} FROM "
					+ " {" + AbstractPageModel._TYPECODE + " as apm "
					+ " JOIN " + PageTemplateModel._TYPECODE + " as ptm on {apm.masterTemplate}={ptm.PK} "
					+ " JOIN " + ContentSlotForTemplateModel._TYPECODE + " as csftm on {csftm.pageTemplate}={ptm.PK} "
					+ " JOIN " + ContentSlotModel._TYPECODE + " as csm on {csftm.contentSlot}={csm.pk} "
					+ " JOIN " + AbstractCMSComponentModel._ELEMENTSFORSLOT + " as elForSlot on {csm.pk}={elForSlot.source} "
					+ " JOIN " + AbstractCMSComponentModel._TYPECODE + " as c on {c.pk}={elForSlot.target} "
					+ " JOIN " + CatalogVersionModel._TYPECODE + " as cv on {c.catalogVersion}={cv.pk} "
					+ " JOIN " + CatalogModel._TYPECODE + " as catalog on {cv.catalog}={catalog.pk}"
					+ "} WHERE {c." + AbstractCMSComponentModel.UID + "} = ?uid AND"
					+ " {cv." + CatalogVersionModel.VERSION + "} LIKE ?catalogVersion}} "
					+ ") uniontable";

	@Override
	public Collection<AbstractPageModel> findAllPagesByCategoryCode(final String typeCode,
			final Collection<CatalogVersionModel> catalogVersions, final String categoryCode)
	{

		final StringBuilder queryBuilder = new StringBuilder();
		final Map queryParameters = new HashMap();
		queryBuilder.append("SELECT {pk} FROM {" + typeCode + "} WHERE ");
		queryBuilder.append(buildCatalogVersionsStatement(catalogVersions, queryParameters));
		queryBuilder
				.append(" AND {" + WileyPartnerPageModel.PARTNERCATEGORYCODE + "} = ?partnerCategoryCode");
		queryParameters.put("partnerCategoryCode", categoryCode);

		final SearchResult<AbstractPageModel> result = search(queryBuilder.toString(), queryParameters);
		return result.getResult();

	}

	@Override
	public List<AbstractPageModel> findPagesByComponent(final AbstractCMSComponentModel componentModel)
	{

		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("uid", componentModel.getUid());
		queryParams.put("catalogVersion", componentModel.getCatalogVersion().getVersion());

		final List<AbstractPageModel> result = flexibleSearchService.<AbstractPageModel> search(
				new FlexibleSearchQuery(FIND_PAGES_BY_COMPONENT, queryParams)).getResult();
		return result;
	}

	protected String buildCatalogVersionsStatement(
			final Collection<CatalogVersionModel> catalogVersions, final Map queryParameters)
	{
		return FlexibleSearchUtils.buildOracleCompatibleCollectionStatement(
				"{catalogVersion} in (?catalogVersions)", "catalogVersions", "OR", catalogVersions,
				queryParameters);
	}

}
