package com.wiley.core.checkout.configuration.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.checkout.configuration.WileyOrderConfigurationService;
import com.wiley.core.model.WileyOrderConfigurationModel;

import static java.lang.String.format;


public class WileyOrderConfigurationServiceImpl implements WileyOrderConfigurationService
{
	private static final String ORDER_CHECKOUT_CONFIGURATION_ERROR_MSG =
			"Order configuration for order with code: %s, %d configurations "
					+ "found! There should be only 1 configuration!";
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<WileyOrderConfigurationModel> getOrderConfiguration(final AbstractOrderModel orderModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("orderModel", orderModel);
		final WileyOrderConfigurationModel modelExample = new WileyOrderConfigurationModel();
		modelExample.setOrderType(orderModel.getOrderType());

		List<WileyOrderConfigurationModel> configurations = flexibleSearchService.getModelsByExample(modelExample);

		if (configurations.size() > 1)
		{
			throw new AmbiguousIdentifierException(format(ORDER_CHECKOUT_CONFIGURATION_ERROR_MSG, orderModel.getCode(),
					configurations.size()));
		}

		return configurations
				.stream()
				.findFirst();
	}
}
