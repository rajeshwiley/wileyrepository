package com.wiley.core.mpgs.services.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.WileyMPGSPaymentInfoService;
import com.wiley.core.payment.strategies.impl.WileyCreditCardPaymentInfoCreateStrategy;



public class WileyMPGSPaymentInfoServiceImpl implements WileyMPGSPaymentInfoService
{
	private static final int LAST_SYMBOLS_COUNT = 4;

	@Autowired
	private ModelService modelService;

	@Autowired
	private CartService cartService;

	@Autowired
	private WileyCreditCardPaymentInfoCreateStrategy wileyCreditCardPaymentInfoCreateStrategy;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "checkoutFacade")
	private DefaultCheckoutFacade checkoutFacade;

	@Override
	public void setCreditCardPaymentInfo(final String tnsMerchantId, final WileyTokenizationResponse tokenizationResult,
			final Boolean saveInAccount)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");
		ServicesUtil.validateParameterNotNull(tokenizationResult, "[tokenizationResult] can't be null.");
		CartModel cartModel = cartService.getSessionCart();
		ServicesUtil.validateParameterNotNull(cartModel, "[cartModel] can't be null.");

		CustomerModel customerModel = (CustomerModel) cartModel.getUser();
		ServicesUtil.validateParameterNotNull(customerModel, "[customerModel] can't be null.");

		CreditCardPaymentInfoModel creditCardPaymentInfoModel = createCreditCardPaymentInfoModel(tnsMerchantId,
				tokenizationResult, customerModel, saveInAccount);

		setCustomerDefaultPaymentInfo(creditCardPaymentInfoModel);
		checkoutFacade.setPaymentDetails(creditCardPaymentInfoModel.getPk().toString());
	}

	@Override
	public void setCreditCardPaymentInfo(final String tnsMerchantId, final WileyTokenizationResponse tokenizationResult,
			final Boolean saveInAccount,
			final OrderModel orderModel)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");
		ServicesUtil.validateParameterNotNull(tokenizationResult, "[tokenizationResult] can't be null.");
		ServicesUtil.validateParameterNotNull(orderModel, "[orderModel] can't be null.");

		CustomerModel customerModel = (CustomerModel) orderModel.getUser();
		ServicesUtil.validateParameterNotNull(customerModel, "[customerModel] can't be null.");


		CreditCardPaymentInfoModel creditCardPaymentInfoModel = createCreditCardPaymentInfoModel(tnsMerchantId,
				tokenizationResult, customerModel, saveInAccount);

		setCustomerDefaultPaymentInfo(creditCardPaymentInfoModel);

		orderModel.setPaymentInfo(creditCardPaymentInfoModel);
		getModelService().saveAll(creditCardPaymentInfoModel, orderModel);
		getModelService().refresh(orderModel);
	}

	protected ModelService getModelService()
	{
		return modelService;
	}


	private void setCustomerDefaultPaymentInfo(final CreditCardPaymentInfoModel creditCardPaymentInfoModel)
	{
		CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
		ccPaymentInfoData.setId(creditCardPaymentInfoModel.getPk().toString());
		userFacade.setDefaultPaymentInfo(ccPaymentInfoData);
	}


	private CreditCardPaymentInfoModel createCreditCardPaymentInfoModel(
			final String tnsMerchantId,
			final WileyTokenizationResponse tokenizationResult,
			final CustomerModel customerModel,
			final Boolean saveInAccount)
	{
		SubscriptionInfoData subscriptionInfo = new SubscriptionInfoData();
		subscriptionInfo.setSubscriptionID(tokenizationResult.getToken());

		PaymentInfoData paymentInfo = createPaymentInfoData(tokenizationResult);
		AddressModel billingAddress = customerModel.getDefaultPaymentAddress();
		CreditCardPaymentInfoModel creditCardPaymentInfoModel =
				wileyCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(subscriptionInfo,
						paymentInfo, billingAddress, customerModel, saveInAccount);
		creditCardPaymentInfoModel.setMerchantId(tnsMerchantId);
		modelService.save(creditCardPaymentInfoModel);
		return creditCardPaymentInfoModel;
	}


	private PaymentInfoData createPaymentInfoData(final WileyTokenizationResponse subscriptionResult)
	{
		ServicesUtil.validateParameterNotNull(subscriptionResult, "[subscriptionResult] can't be null.");
		ServicesUtil.validateParameterNotNull(subscriptionResult.getCardNumber(), "[cardNumber] can't be null.");
		PaymentInfoData paymentInfo = new PaymentInfoData();
		paymentInfo.setCardAccountNumber(
				subscriptionResult.getCardNumber().substring(subscriptionResult.getCardNumber().length() - LAST_SYMBOLS_COUNT));
		paymentInfo.setCardCardType(CardTypeEnum.valueOf(subscriptionResult.getCardScheme().toLowerCase()).getStringValue());
		paymentInfo.setCardExpirationMonth(Integer.valueOf(subscriptionResult.getCardExpiryMonth()));
		paymentInfo.setCardExpirationYear(Integer.valueOf(subscriptionResult.getCardExpiryYear()));

		return paymentInfo;
	}
}
