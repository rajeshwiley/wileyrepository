package com.wiley.core.billingfrequency.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.billingfrequency.dao.WileyBillingFrequencyDao;


/**
 * Default implementation of {@link WileyBillingFrequencyDao}
 */
public class WileyBillingFrequencyDaoImpl implements WileyBillingFrequencyDao
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	private static final String QUERY_FIND_BILLING_FREQUENCIES_BY_CODE =
			"SELECT {pk} FROM {" + BillingFrequencyModel._TYPECODE + "}"
					+ " WHERE {" + BillingFrequencyModel.CODE + "} = ?code";

	@Nonnull
	@Override
	public List<BillingFrequencyModel> findBillingFrequenciesByCode(@Nonnull final String code)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", code);

		return flexibleSearchService.<BillingFrequencyModel> search(QUERY_FIND_BILLING_FREQUENCIES_BY_CODE,
				Collections.singletonMap(BillingFrequencyModel.CODE, code)).getResult();
	}
}
