package com.wiley.core.wiley.circuitbreaker.helper.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.function.BiConsumer;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.netflix.hystrix.HystrixCommandProperties;
import com.wiley.core.wiley.circuitbreaker.helper.PropertiesSetterHelper;


/**
 * Created by Georgii_Gavrysh on 10/19/2016.
 */
public class PropertiesSetterHelperImpl implements PropertiesSetterHelper
{

	private enum Property
	{
		THREAD_TIMEOUT_IN_MILLISECONDS("execution.isolation.thread.timeoutInMilliseconds"),
		REQUEST_VOLUME_THRESHOLD("circuitBreaker.requestVolumeThreshold"),
		SLEEP_WINDOW_IN_MILLISECONDS("circuitBreaker.sleepWindowInMilliseconds"),
		ERROR_THRESHOLD_PERCENTAGE("circuitBreaker.errorThresholdPercentage"),
		ROLLING_PERCENTILE_TIME_IN_MILLISECONDS("metrics.rollingPercentile.timeInMilliseconds"),
		METRICS_ROLLING_PERCENTILE_WINDOW_BUCKETS("metrics.rollingPercentile.numBuckets"),
		METRICS_ROLLING_STATS_TIME_IN_MILLISECONDS("metrics.rollingStats.timeInMilliseconds"),
		METRICS_ROLLING_STATS_WINDOW_BUCKETS("metrics.rollingStats.numBuckets"),
		EXECUTION_TIMEOUT_ENABLED("execution.timeout.enabled"),
		CIRCUIT_BREAKER_ENABLED("circuitBreaker.enabled"),
		FALLBACK_ENABLED("fallback.enabled"),
		REQUEST_CACHE_ENABLED("requestCache.enabled"),
		EXECUTION_ISOLATION_STRATEGY("execution.isolation.strategy"),
		EXECUTION_ISOLATION_SEMAPHORE_MAX_CONCURRENT_REQUESTS("execution.isolation.semaphore.maxConcurrentRequests");

		private final String propertyKey;

		Property(final String propertyKey)
		{
			this.propertyKey = propertyKey;
		}

		public String getPropertyKey()
		{
			return propertyKey;
		}
	}

	private abstract static class ValueSetter<V>
	{
		private void set(final HystrixCommandProperties.Setter propertiesSetter,
				final BiConsumer<HystrixCommandProperties.Setter, V> biConsumer,
				final String propertyValue)
		{
			if (StringUtils.isNotBlank(propertyValue))
			{
				final V value = parse(propertyValue);
				biConsumer.accept(propertiesSetter, value);
			}
		}

		abstract V parse(String strValue);

	}

	private static class BooleanSetter extends ValueSetter<Boolean>
	{
		@Override
		Boolean parse(final String strValue)
		{
			return BooleanUtils.toBoolean(strValue, "true", "false");
		}
	}

	private static class IntegerSetter extends ValueSetter<Integer>
	{
		@Override
		Integer parse(final String strValue)
		{
			return Integer.parseInt(strValue);
		}
	}

	private static class ExecutionIsolationStrategySetter extends ValueSetter<HystrixCommandProperties.ExecutionIsolationStrategy>
	{
		@Override
		HystrixCommandProperties.ExecutionIsolationStrategy parse(final String strValue)
		{
			return HystrixCommandProperties.ExecutionIsolationStrategy.valueOf(strValue);
		}
	}

	private static final ValueSetter<Boolean> BOOLEAN_SETTER = new BooleanSetter();
	private static final ValueSetter<Integer> INTEGER_SETTER = new IntegerSetter();
	private static final ValueSetter<HystrixCommandProperties.ExecutionIsolationStrategy> EXECUTION_ISOLATION_STRATEGY_SETTER =
			new ExecutionIsolationStrategySetter();

	private static final String HYSTRIX_COMMAND_PREFIX = "hystrix.command";

	@Resource
	private ConfigurationService configurationService;

	@Override
	public void configureCircuitBreakerCommand(@Nonnull final String commandKey,
			@Nonnull final HystrixCommandProperties.Setter propertiesSetter)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("commandKey", commandKey);
		ServicesUtil.validateParameterNotNullStandardMessage("propertiesSetter", propertiesSetter);

		setExecutionIsolationStrategy(propertiesSetter, HystrixCommandProperties.Setter::withExecutionIsolationStrategy,
				getConfigPropertyForCommand(commandKey, Property.EXECUTION_ISOLATION_STRATEGY));

		setBoolean(propertiesSetter, HystrixCommandProperties.Setter::withExecutionTimeoutEnabled,
				getConfigPropertyForCommand(commandKey, Property.EXECUTION_TIMEOUT_ENABLED));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withExecutionTimeoutInMilliseconds,
				getConfigPropertyForCommand(commandKey, Property.THREAD_TIMEOUT_IN_MILLISECONDS));

		setBoolean(propertiesSetter, HystrixCommandProperties.Setter::withCircuitBreakerEnabled,
				getConfigPropertyForCommand(commandKey, Property.CIRCUIT_BREAKER_ENABLED));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withCircuitBreakerRequestVolumeThreshold,
				getConfigPropertyForCommand(commandKey, Property.REQUEST_VOLUME_THRESHOLD));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withCircuitBreakerSleepWindowInMilliseconds,
				getConfigPropertyForCommand(commandKey, Property.SLEEP_WINDOW_IN_MILLISECONDS));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withCircuitBreakerErrorThresholdPercentage,
				getConfigPropertyForCommand(commandKey, Property.ERROR_THRESHOLD_PERCENTAGE));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withMetricsRollingPercentileWindowInMilliseconds,
				getConfigPropertyForCommand(commandKey, Property.ROLLING_PERCENTILE_TIME_IN_MILLISECONDS));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withMetricsRollingPercentileWindowBuckets,
				getConfigPropertyForCommand(commandKey, Property.METRICS_ROLLING_PERCENTILE_WINDOW_BUCKETS));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withMetricsRollingStatisticalWindowInMilliseconds,
				getConfigPropertyForCommand(commandKey, Property.METRICS_ROLLING_STATS_TIME_IN_MILLISECONDS));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withMetricsRollingStatisticalWindowBuckets,
				getConfigPropertyForCommand(commandKey, Property.METRICS_ROLLING_STATS_WINDOW_BUCKETS));

		setBoolean(propertiesSetter, HystrixCommandProperties.Setter::withFallbackEnabled,
				getConfigPropertyForCommand(commandKey, Property.FALLBACK_ENABLED));

		setBoolean(propertiesSetter, HystrixCommandProperties.Setter::withRequestCacheEnabled,
				getConfigPropertyForCommand(commandKey, Property.REQUEST_CACHE_ENABLED));

		setInteger(propertiesSetter, HystrixCommandProperties.Setter::withExecutionIsolationSemaphoreMaxConcurrentRequests,
				getConfigPropertyForCommand(commandKey, Property.EXECUTION_ISOLATION_SEMAPHORE_MAX_CONCURRENT_REQUESTS));
	}

	private void setBoolean(final HystrixCommandProperties.Setter propertiesSetter,
			final BiConsumer<HystrixCommandProperties.Setter, Boolean> biConsumer, final String value)
	{
		BOOLEAN_SETTER.set(propertiesSetter, biConsumer, value);
	}

	private void setExecutionIsolationStrategy(final HystrixCommandProperties.Setter propertiesSetter,
			final BiConsumer<HystrixCommandProperties.Setter, HystrixCommandProperties.ExecutionIsolationStrategy> biConsumer,
			final String value)
	{
		EXECUTION_ISOLATION_STRATEGY_SETTER.set(propertiesSetter, biConsumer, value);
	}

	private void setInteger(final HystrixCommandProperties.Setter propertiesSetter,
			final BiConsumer<HystrixCommandProperties.Setter, Integer> biConsumer, final String value)
	{
		INTEGER_SETTER.set(propertiesSetter, biConsumer, value);
	}

	private String getConfigPropertyForCommand(final String commandKey, final Property property)
	{
		final String propertyKey = property.getPropertyKey();
		return getConfigProperty(getFullConfigProperty(commandKey, propertyKey), getDefaultConfigProperty(propertyKey));
	}

	private String getFullConfigProperty(final String commandKey, final String configProperty)
	{
		return HYSTRIX_COMMAND_PREFIX + "." + commandKey + "." + configProperty;
	}

	private String getDefaultConfigProperty(final String configProperty)
	{
		return HYSTRIX_COMMAND_PREFIX + "." + configProperty;
	}

	private String getConfigProperty(final String property, final String defaultConfigProperty)
	{
		final Configuration configuration = configurationService.getConfiguration();
		String value = configuration.getString(property);
		//Use wiley circuit breaker common property if command specific property isn't specified
		if (StringUtils.isEmpty(value))
		{
			value = configuration.getString(defaultConfigProperty);
		}
		return value;
	}
}
