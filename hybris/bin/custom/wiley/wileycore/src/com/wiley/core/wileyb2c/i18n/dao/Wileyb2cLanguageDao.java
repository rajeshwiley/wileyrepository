package com.wiley.core.wileyb2c.i18n.dao;

import java.util.List;


public interface Wileyb2cLanguageDao
{
	List<String> findLanguageIsoCodesForBaseStore(String baseStoreUid);
}
