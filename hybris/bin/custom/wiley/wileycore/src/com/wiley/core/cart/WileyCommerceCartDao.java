package com.wiley.core.cart;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.enums.OrderType;


/**
 * Provides access for {@link CartModel}.
 */
public interface WileyCommerceCartDao extends CommerceCartDao
{
	List<CartModel> getAbandonCarts(Date modifiedBefore, BaseSiteModel site);

	/**
	 * Returns carts for site,user and order type.
	 * NOTE: It returns all the customer carts - that were saved and that were not.
	 *
	 * @param site
	 * @param user
	 * @param type
	 * @return list of carts sorted by modified time.
	 */
	@Nonnull
	List<CartModel> findAllCartsForStoreAndUserAndType(@Nonnull BaseSiteModel site, @Nonnull UserModel user,
			@Nonnull OrderType type);

	/**
	 * Returns carts for site and user
	 * NOTE: It returns all the customer carts - that were saved and that were not.
	 *
	 * @param site
	 * @param user
	 * @return list of carts sorted by modified time.
	 */
	List<CartModel> findAllCartsForUserAndSite(BaseSiteModel site, UserModel user);
}
