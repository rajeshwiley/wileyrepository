package com.wiley.core.wileyws.order.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.impl.DefaultOrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;

import java.util.Date;

import javax.annotation.Resource;

import com.wiley.core.wileycom.order.dao.WileycomOrderDao;
import com.wiley.core.wileyws.order.WileycomWsOrderHistoryService;


/**
 * Created by Anton_Lukyanau on 8/2/2016.
 */
public class WileycomWsOrderHistoryServiceImpl extends DefaultOrderHistoryService implements WileycomWsOrderHistoryService
{
	@Resource
	private WileycomOrderDao wileycomOrderDao;

	@Override
	public void createHistoryEntry(final OrderModel orderModel, final String description)
	{
		final OrderHistoryEntryModel historyModel = getModelService().create(OrderHistoryEntryModel.class);
		historyModel.setTimestamp(new Date());
		historyModel.setOrder(orderModel);
		historyModel.setDescription(description);
		getModelService().save(historyModel);
	}

	@Override
	public void createHistoryEntry(final OrderModel orderModel, final OrderModel snapshot, final String description)
	{
		final OrderHistoryEntryModel historyModel = getModelService().create(OrderHistoryEntryModel.class);
		historyModel.setTimestamp(new Date());
		historyModel.setOrder(orderModel);
		historyModel.setPreviousOrderVersion(snapshot);
		historyModel.setDescription(description);
		getModelService().save(historyModel);
	}
}
