package com.wiley.core.cms.service;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;


/**
 * Created by Sergiy_Mishkovets on 1/19/2018.
 */
public interface WileyCMSComponentService extends CMSComponentService
{
	<T extends AbstractCMSComponentModel> T getAbstractCMSComponentForCatalogVersion(String componentId, String version)
			throws CMSItemNotFoundException;
}
