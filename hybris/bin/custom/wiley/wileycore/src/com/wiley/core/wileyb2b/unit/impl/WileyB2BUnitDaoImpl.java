package com.wiley.core.wileyb2b.unit.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.wileyb2b.unit.WileyB2BUnitDao;


public class WileyB2BUnitDaoImpl implements WileyB2BUnitDao
{
	private static final String B2B_UNIT_FIND_QUERY = "select {PK} from {" + B2BUnitModel._TYPECODE + "} "
			+ "where {" + B2BUnitModel.SAPACCOUNTNUMBER + "}=?sapAccountNumber";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<B2BUnitModel> findB2BUnitBySapAccountNumber(final String sapAccountNumber)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(B2B_UNIT_FIND_QUERY);
		query.addQueryParameter("sapAccountNumber", sapAccountNumber);
		SearchResult<B2BUnitModel> search = flexibleSearchService.search(query);
		return search.getResult();
	}
}
