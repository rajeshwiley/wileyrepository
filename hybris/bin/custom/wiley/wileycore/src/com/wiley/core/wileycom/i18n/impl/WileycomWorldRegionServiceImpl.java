package com.wiley.core.wileycom.i18n.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;
import java.util.List;

import com.wiley.core.model.WorldRegionModel;
import com.wiley.core.wileycom.i18n.WileycomWorldRegionService;
import com.wiley.core.wileycom.i18n.dao.WileycomWorldRegionDao;
import com.wiley.facade.user.data.WorldRegionData;


public class WileycomWorldRegionServiceImpl implements WileycomWorldRegionService
{
	private WileycomWorldRegionDao wileycomWorldRegionDao;
	private Converter<WorldRegionModel, WorldRegionData> wileycomWorldRegionConverter;
	private Converter<CountryModel, CountryData> wileycomCountryConverter;
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public List<WorldRegionModel> getWorldRegions()
	{
		return getWileycomWorldRegionDao().findWorldRegions();
	}

	@Override
	public List<CountryData> getCountries()
	{
		final Collection<CountryModel> allCountries = getCommerceCommonI18NService().getAllCountries();
		return Converters.convertAll(allCountries, getWileycomCountryConverter());
	}

	public WileycomWorldRegionDao getWileycomWorldRegionDao()
	{
		return wileycomWorldRegionDao;
	}

	public void setWileycomWorldRegionDao(final WileycomWorldRegionDao wileycomWorldRegionDao)
	{
		this.wileycomWorldRegionDao = wileycomWorldRegionDao;
	}

	public Converter<WorldRegionModel, WorldRegionData> getWileycomWorldRegionConverter()
	{
		return wileycomWorldRegionConverter;
	}

	public void setWileycomWorldRegionConverter(
			final Converter<WorldRegionModel, WorldRegionData> wileycomWorldRegionConverter)
	{
		this.wileycomWorldRegionConverter = wileycomWorldRegionConverter;
	}

	public Converter<CountryModel, CountryData> getWileycomCountryConverter()
	{
		return wileycomCountryConverter;
	}

	public void setWileycomCountryConverter(final Converter<CountryModel, CountryData> wileycomCountryConverter)
	{
		this.wileycomCountryConverter = wileycomCountryConverter;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

}
