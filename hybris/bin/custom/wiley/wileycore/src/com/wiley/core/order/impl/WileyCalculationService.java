package com.wiley.core.order.impl;

import com.wiley.core.exceptions.WileyCurrencyMismatchException;
import com.wiley.core.order.strategies.calculation.WileyFindExternalPriceStrategy;
import com.wiley.core.order.strategies.calculation.WileyFindStackablePriceStrategy;
import com.wiley.core.price.WileyDiscountCalculationService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.product.WileyStackableProductService;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


public class WileyCalculationService extends DefaultCalculationService
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyCalculationService.class);

	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Resource
	private WileyFindExternalPriceStrategy wileyFindExternalPriceStrategy;
	@Resource
	private WileyFindStackablePriceStrategy wileyFindStackablePriceStrategy;
	@Resource
	private WileyStackableProductService wileyStackableProductService;
	@Resource
	private WileyDiscountCalculationService wileyDiscountCalculationService;

	private FindDeliveryCostStrategy findDeliveryCostStrategy;
	private FindPaymentCostStrategy findPaymentCostStrategy;
	private CommonI18NService commonI18NService;
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

	@Override
	public void calculate(@Nonnull final AbstractOrderModel order) throws CalculationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		super.calculate(order);
	}

	@Override
	public void recalculate(@Nonnull final AbstractOrderModel order) throws CalculationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		super.recalculate(order);
	}

	@Override
	protected void resetAdditionalCosts(final AbstractOrderModel order, final Collection<TaxValue> relativeTaxValues)
	{
		double deliveryCostValue = 0.0;
		// we should not calculate delivery costs for non-shippable products.
		if (wileyProductEditionFormatService.isDigitalCart(order))
		{
			LOG.debug("Skipping delivery cost calculation for digital order [{}].", order.getCode());
			order.setDeliveryMode(null);
		}
		else
		{
			LOG.debug("Calculating delivery cost for order [{}].", order.getCode());
			final PriceValue deliCost = findDeliveryCostStrategy.getDeliveryCost(order);
			LOG.debug("DeliveryCost [{}] is found for order [{}].", deliCost, order.getCode());
			if (deliCost != null)
			{
				deliveryCostValue = convertPriceIfNecessary(deliCost, order.getNet().booleanValue(), order.getCurrency(),
						relativeTaxValues).getValue();
			}
		}
		LOG.debug("Setting delivery cost [{}] for order [{}].", deliveryCostValue, order.getCode());
		order.setDeliveryCost(Double.valueOf(deliveryCostValue));

		// -----------------------------
		// set payment cost - convert if net or currency is different
		final PriceValue payCost = findPaymentCostStrategy.getPaymentCost(order);
		double paymentCostValue = 0.0;
		if (payCost != null)
		{
			paymentCostValue = convertPriceIfNecessary(payCost, order.getNet().booleanValue(), order.getCurrency(),
					relativeTaxValues).getValue();
		}
		order.setPaymentCost(Double.valueOf(paymentCostValue));
	}

	//Removed currency conversion part.
	//If currency doesn't match throw
	@Override
	public PriceValue convertPriceIfNecessary(final PriceValue pv, final boolean toNet, final CurrencyModel toCurrency,
			final Collection taxValues)
	{
		// net - gross - conversion
		double convertedPrice = pv.getValue();
		if (pv.isNet() != toNet)
		{
			convertedPrice = pv.getOtherPrice(taxValues).getValue();
			convertedPrice = commonI18NService.roundCurrency(convertedPrice, toCurrency.getDigits().intValue());
		}
		// currency conversion
		final String iso = pv.getCurrencyIso();
		if (iso != null && !iso.equals(toCurrency.getIsocode()))
		{
			String errorMessage = String.format("Price value %s currency doesn't match with order currency %s",
					pv.getCurrencyIso(), toCurrency.getIsocode());
			throw new WileyCurrencyMismatchException(errorMessage);
		}
		return new PriceValue(toCurrency.getIsocode(), convertedPrice, toNet);
	}

	@Override
	public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			final AbstractOrderModel order = entry.getOrder();
			final CurrencyModel curr = order.getCurrency();
			final int digits = curr.getDigits().intValue();
			BigDecimal totalPriceWithoutDiscount = BigDecimal.valueOf(entry.getSubtotalPrice());

			/*
			 * apply discounts (will be rounded each) convert absolute discount values
			 * in case their currency doesn't match the order currency
			 */
			// YTODO : use CalculationService methods to apply discounts
			final BigDecimal quantity = BigDecimal.valueOf(entry.getQuantity().doubleValue());
			final List<DiscountValue> appliedDiscounts = DiscountValue.apply(quantity.doubleValue(),
					totalPriceWithoutDiscount.doubleValue(), digits,
					convertDiscountValues(order, entry.getDiscountValues()), curr.getIsocode());
			entry.setDiscountValues(appliedDiscounts);
			BigDecimal totalPrice = totalPriceWithoutDiscount;
			for (DiscountValue appliedDiscount : appliedDiscounts)
			{
				totalPrice = subtractDiscount(totalPrice, appliedDiscount);
			}
			// set total price
			entry.setTotalPrice(Double.valueOf(totalPrice.doubleValue()));
			// apply tax values too
			// YTODO : use CalculationService methods to apply taxes
			calculateTotalTaxValues(entry);
			entry.setCalculated(Boolean.TRUE);
			getModelService().save(entry);
		}
	}

	BigDecimal subtractDiscount(final BigDecimal totalPrice, final DiscountValue appliedDiscount)
	{
		BigDecimal totalPriceWithoutAppliedDiscount = totalPrice.subtract(BigDecimal.valueOf(appliedDiscount
				.getAppliedValue()));
		return totalPriceWithoutAppliedDiscount.signum() < 0 ? BigDecimal.ZERO : totalPriceWithoutAppliedDiscount;
	}

	@Override
	public void setFindDeliveryCostStrategy(final FindDeliveryCostStrategy findDeliveryCostStrategy)
	{
		super.setFindDeliveryCostStrategy(findDeliveryCostStrategy);
		this.findDeliveryCostStrategy = findDeliveryCostStrategy;
	}

	@Override
	public void setFindPaymentCostStrategy(final FindPaymentCostStrategy findPaymentCostStrategy)
	{
		super.setFindPaymentCostStrategy(findPaymentCostStrategy);
		this.findPaymentCostStrategy = findPaymentCostStrategy;
	}


	@Override
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		super.setCommonI18NService(commonI18NService);
		this.commonI18NService = commonI18NService;
	}

	@Override
	protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		final Collection<TaxValue> entryTaxes = findTaxValues(entry);
		if (!isTaxCurrencySameAsOrderCurrency(entry))
		{
			entry.setTaxValues(entryTaxes);
		}

		final AbstractOrderModel order = entry.getOrder();
		final CurrencyModel curr = order.getCurrency();
		final int digits = curr.getDigits().intValue();
		final PriceValue subtotalPriceValue = findSubtotalPriceValue(entry);
		if (subtotalPriceValue != null)
		{
			if (entry.getStatus() == OrderStatus.CANCELLED)
			{
				// reset subtotal to 0 for quantity 0 case
				entry.setSubtotalPrice(0d);
				entry.setBasePrice(0d);
			}
			else
			{
				double subtotalPrice = commonI18NService.roundCurrency(subtotalPriceValue.getValue(), digits);
				entry.setSubtotalPrice(subtotalPrice);
				Double basePrice = subtotalPrice / entry.getQuantity();
				entry.setBasePrice(basePrice.isNaN() || basePrice.isInfinite() ? Double.valueOf(0) : basePrice);
			}
		}
		else
		{
			final PriceValue pv = findBasePrice(entry);
			if (pv == null)
			{
				throw new CalculationException("Cannot find price for currency " + entry.getOrder().getCurrency().getIsocode());
			}
			final PriceValue basePrice = convertPriceIfNecessary(pv, order.getNet(), curr, entryTaxes);
			entry.setBasePrice(basePrice.getValue());
			double subtotal = commonI18NService.roundCurrency(
					entry.getBasePrice() * entry.getQuantity(), digits);
			entry.setSubtotalPrice(subtotal);
		}
		final List<DiscountValue> entryDiscounts = findDiscountValues(entry);
		entry.setDiscountValues(entryDiscounts);
	}


	@Nullable
	protected PriceValue findSubtotalPriceValue(final AbstractOrderEntryModel entry) throws CalculationException
	{
		PriceValue subtotalPriceValue = wileyFindExternalPriceStrategy.findExternalPrice(entry);
		if (subtotalPriceValue == null && wileyStackableProductService.hasStackablePrice(entry.getProduct()))
		{
			subtotalPriceValue = wileyFindStackablePriceStrategy.findStackablePrice(entry);
		}
		return subtotalPriceValue;
	}


	private boolean isTaxCurrencySameAsOrderCurrency(final AbstractOrderEntryModel entry)
	{
		if (CollectionUtils.isNotEmpty(entry.getTaxValues()))
		{
			Optional<TaxValue> first = entry.getTaxValues().stream().findFirst();
			if (first.isPresent()) {
				String taxValueCurrency = first.get().getCurrencyIsoCode();
				return taxValueCurrency.equals(entry.getOrder().getCurrency().getIsocode());
			}
		}

		return true;
	}

	protected WileyProductEditionFormatService getWileyProductEditionFormatService()
	{
		return wileyProductEditionFormatService;
	}

	protected FindDeliveryCostStrategy getFindDeliveryCostStrategy()
	{
		return findDeliveryCostStrategy;
	}

	protected FindPaymentCostStrategy getFindPaymentCostStrategy()
	{
		return findPaymentCostStrategy;
	}

	@Override
	public void setOrderRequiresCalculationStrategy(final OrderRequiresCalculationStrategy orderRequiresCalculationStrategy)
	{
		super.setOrderRequiresCalculationStrategy(orderRequiresCalculationStrategy);
		this.orderRequiresCalculationStrategy = orderRequiresCalculationStrategy;
	}

	@Override
	protected void calculateTotals(final AbstractOrderModel order, boolean recalculate,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(order))
		{
			super.calculateTotals(order, recalculate, taxValueMap);
			calculateTaxableTotalPrice(order);
			calculateSubtotalWithoutDiscount(order);
			correctTotalPrice(order);
		}
	}

	/**
	 * This method is needed because in some cases order total could be negative, e.g.:
	 * sum of external discounts, discount rows(hybris ootb) greater order total.
	 *
	 * @param order
	 */
	void correctTotalPrice(final AbstractOrderModel order)
	{
		final Double totalPrice = order.getTotalPrice();
		final Double newTotalPrice = correctValue(totalPrice);
		if (!newTotalPrice.equals(totalPrice))
		{
			order.setTotalPrice(newTotalPrice);
			getModelService().save(order);
		}
	}

	private Double correctValue(final Double value)
	{
		return Math.max(0.0d, value);
	}

	protected void calculateTaxableTotalPrice(final AbstractOrderModel order)
	{
		final List<AbstractOrderEntryModel> modifiedEntries = new ArrayList<>(order.getEntries().size());
		final int digits = order.getCurrency().getDigits().intValue();
		Double sumTaxableTotalPrice = 0d;
		for (final AbstractOrderEntryModel entry : order.getEntries())
		{
			AbstractOrderEntryModel lastEntry = order.getEntries().get(order.getEntries().size() - 1);
			Double taxableTotalPrice;
			if (!Objects.equals(entry, lastEntry))
			{
				BigDecimal taxableEntryPrice = BigDecimal.valueOf(entry.getTotalPrice())
						.subtract(wileyDiscountCalculationService.getEntryOrderLevelProportionalDiscount(entry));
				taxableTotalPrice = correctValue(CoreAlgorithms.round(taxableEntryPrice.doubleValue(), digits));
				sumTaxableTotalPrice += taxableTotalPrice;
			}
			else
			{
				taxableTotalPrice = correctValue(CoreAlgorithms.round(order.getSubtotal()
						- order.getTotalDiscounts() - sumTaxableTotalPrice, digits));
			}

			entry.setTaxableTotalPrice(taxableTotalPrice);
			modifiedEntries.add(entry);
		}
		getModelService().saveAll(modifiedEntries);
	}

	protected void calculateSubtotalWithoutDiscount(final AbstractOrderModel orderModel)
	{
		Double subTotalWithoutDiscount = 0.0;

		for (AbstractOrderEntryModel entryModel : orderModel.getEntries())
		{
			subTotalWithoutDiscount += entryModel.getSubtotalPrice();
		}
		orderModel.setSubTotalWithoutDiscount(subTotalWithoutDiscount);
		getModelService().save(orderModel);
	}

}
