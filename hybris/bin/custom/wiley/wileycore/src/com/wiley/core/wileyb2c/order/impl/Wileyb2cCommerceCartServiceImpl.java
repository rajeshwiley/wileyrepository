package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.core.wileyb2c.order.Wileyb2cCommerceCartService;
import com.wiley.core.wileyb2c.order.dao.Wileyb2cCommerceCartDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class Wileyb2cCommerceCartServiceImpl extends DefaultCommerceCartService implements Wileyb2cCommerceCartService
{

	@Resource
	private ModelService modelService;

	@Resource
	private CMSSiteService cmsSiteService;

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	@Resource
	private ConfigurationService configurationService;

	private Wileyb2cCommerceCartDao wileyb2cCommerceCartDao;

	@Override
	public void saveContinueUrlForCart(@Nullable final String continueUrl, @Nonnull final CartModel cartModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartModel", cartModel);

		cartModel.setCartPageContinueUrl(continueUrl);
		modelService.save(cartModel);
	}

	@Override
	public CartModel getCartForGuidAndSiteAndUser(final String guid, final BaseSiteModel site, final UserModel user)
	{
		validateParameterNotNull(site, "site cannot be null");
		validateParameterNotNull(user, "user cannot be null");

		CartModel result = null;

		final Optional<BaseStoreModel> optionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(site);
		if (optionalBaseStore.isPresent())
		{
			result = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(guid, optionalBaseStore.get(), user);
		}
		return result;
	}

	@Override
	public CartModel getCartForGuidAndSite(final String guid, final BaseSiteModel site)
	{
		validateParameterNotNull(guid, "guid cannot be null");
		validateParameterNotNull(site, "site cannot be null");

		CartModel result = null;

		final Optional<BaseStoreModel> optionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(site);
		if (optionalBaseStore.isPresent())
		{
			result = wileyb2cCommerceCartDao.getCartForGuidAndStore(guid, optionalBaseStore.get());
		}
		return result;
	}

	@Override
	public List<CartModel> getCartsForSiteAndUser(final BaseSiteModel site, final UserModel user)
	{
		validateParameterNotNull(site, "site cannot be null");
		validateParameterNotNull(user, "user cannot be null");

		List<CartModel> result = Collections.emptyList();

		final Optional<BaseStoreModel> optionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(site);
		if (optionalBaseStore.isPresent())
		{
			result = wileyb2cCommerceCartDao.getCartsForStoreAndUser(optionalBaseStore.get(), user);
		}
		return result;
	}

	@Override
	public void setCommerceCartDao(final CommerceCartDao commerceCartDao)
	{
		super.setCommerceCartDao(commerceCartDao);
		this.wileyb2cCommerceCartDao = (Wileyb2cCommerceCartDao) commerceCartDao;
	}
}
