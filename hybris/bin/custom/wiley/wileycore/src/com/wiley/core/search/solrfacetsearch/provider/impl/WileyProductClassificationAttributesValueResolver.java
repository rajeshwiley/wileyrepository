package com.wiley.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductClassificationAttributesValueResolver;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;


public class WileyProductClassificationAttributesValueResolver extends ProductClassificationAttributesValueResolver
{
	@Override
	protected boolean addFieldValue(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty,
			final Object value, final String qualifier) throws FieldValueProviderException
	{
		Object newValue = value;
		if (value instanceof ClassificationAttributeValueModel)
		{
			newValue = ((ClassificationAttributeValueModel) value).getCode();
		}
		return super.addFieldValue(document, batchContext, indexedProperty, newValue, qualifier);
	}
}
