package com.wiley.core.pin.dao;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.core.model.PinModel;

/**
 * Interface to lookup Pins from persistence layer
 */
public interface PinDao
{
    /**
     * Returns {@link PinModel} with the matching PIN code.
     *
     * @param pinCode
     * 		PIN unique identifier
     * @return PinModel item if found or null
     */
    PinModel findPinByCode(String pinCode);

    /**
     * Returns {@link PinModel} with the matching order.
     *
     * @param order
     * 		the order model
     * @return PinModel item if found or null
     */
    PinModel findPinByOrder(AbstractOrderModel order);
}
