/**
 *
 */
package com.wiley.core.order.hook;

import de.hybris.platform.commerceservices.order.hook.CommerceCartCalculationMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyOrderDiscountGroupService;


/**
 *
 */
public class WileyCartCalculationMethodDiscountGroupHook implements CommerceCartCalculationMethodHook
{
	@Autowired
	protected WileyOrderDiscountGroupService wileyOrderDiscountGroupService;

	@Override
	public void beforeCalculate(final CommerceCartParameter parameter)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(parameter);
	}

	@Override
	public void afterCalculate(final CommerceCartParameter parameter)
	{
		// Do nothing
	}
}
