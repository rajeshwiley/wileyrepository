package com.wiley.core.promotions.actions;

import com.wiley.core.model.WileyPartnerCompanyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;

public class WileySetPartnerDiscountGroupActionStrategy extends WileySetOrderDiscountGroupActionStrategy
{

	@Override
	protected UserDiscountGroup getDiscountGroup(final AbstractOrderModel order)
	{
		WileyPartnerCompanyModel wileyPartner = order.getWileyPartner();
		return getPartnerDiscountGroupForId(wileyPartner);
	}

	public UserDiscountGroup getPartnerDiscountGroupForId(final WileyPartnerCompanyModel partnerCompany)
	{
		UserDiscountGroup discountGroup = null;
		if (partnerCompany != null && partnerCompany.getUserDiscountGroup() != null)
		{
			discountGroup = partnerCompany.getUserDiscountGroup();
		}
		return discountGroup;
	}
}
