package com.wiley.core.wileyb2c.classification;

/**
 * @author Dzmitryi_Halahayeu
 */
public enum Wileyb2cClassificationAttributes
{
	PUBLICATION_DATE("publicationDate"), ISBN13("isbn13"), NUMBER_OF_PAGES("numberOfPages"), PRINT_ISSN("printIssn"),
	ONLINE_ISSN("onlineIssn"), VOLUME_AND_ISSUE("volumeAndIssue"), IMPACT_FACTOR("impactFactor"), SOCIETY_LINK("societyLink");

	private final String code;

	Wileyb2cClassificationAttributes(final String name)
	{
		this.code = name;
	}

	public String getCode()
	{
		return code;
	}
}
