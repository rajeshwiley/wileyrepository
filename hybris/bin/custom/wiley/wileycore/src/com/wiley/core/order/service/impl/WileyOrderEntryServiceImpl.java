package com.wiley.core.order.service.impl;

import com.wiley.core.order.dao.WileyOrderEntryDao;
import com.wiley.core.order.service.WileyOrderEntryService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;


public class WileyOrderEntryServiceImpl implements WileyOrderEntryService
{
	private static final String ENTRY_NOT_FOUND = "Entry with guid [%s] not found";
	private static final String ENTRY_NOT_UNIQUE = "Entry with guid [%s] isn't unique";

	private final WileyOrderEntryDao wileyOrderEntryDao;
	private final UserService userService;

	@Autowired
	public WileyOrderEntryServiceImpl(final WileyOrderEntryDao wileyOrderEntryDao, final UserService userService) {
		this.wileyOrderEntryDao = wileyOrderEntryDao;
		this.userService = userService;
	}

	@Override
	public OrderEntryModel getOrderEntryForGUID(final String guid)
	{
		List<OrderEntryModel> entries = wileyOrderEntryDao.findOrderEntryByGuid(guid);
		validateIfSingleResult(entries, format(ENTRY_NOT_FOUND, guid), format(ENTRY_NOT_UNIQUE, guid));
		return entries.get(0);
	}

	public SearchPageData<AbstractOrderEntryModel> getOrderEntriesForBusinessValues(final BaseSiteModel baseSite,
			final String userId, final String businessItemId, final String businessKey, final List<String> statuses,
			final PageableData pagination)
	{
		UserModel user = null;
		if (StringUtils.isNotEmpty(userId))
		{
			user = userService.getUserForUID(userId);
		}

		List<OrderStatus> orderEntryStatuses = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(statuses))
		{
			orderEntryStatuses = statuses.stream()
					.map(OrderStatus::valueOf).collect(Collectors.toList());
		}

		return wileyOrderEntryDao.findOrderEntriesByBusinessValues(baseSite,
				user, businessItemId, businessKey, pagination, orderEntryStatuses);
	}

	@Override
	public boolean isOrderEntryExistsForGuid(@Nonnull final String guid)
	{
		validateParameterNotNullStandardMessage("guid", guid);

		if (wileyOrderEntryDao.findOrderEntryByGuid(guid).isEmpty())
		{
			return false;
		}
		return true;
	}
}
