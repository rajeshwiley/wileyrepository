package com.wiley.core.order.strategies.calculation;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.util.PriceValue;


/**
 * Strategy that focuses on resolving stackable {@link PriceValue} for the given {@link AbstractOrderEntryModel}.
 */
public interface WileyFindStackablePriceStrategy
{
	/**
	 * Resolves external subtotal price value for the given {@link AbstractOrderEntryModel}.
	 *
	 * @param entry
	 * 		{@link AbstractOrderEntryModel}
	 * @return {@link PriceValue}
	 * @throws CalculationException
	 * 		if price wasn't found
	 */
	PriceValue findStackablePrice(AbstractOrderEntryModel entry) throws CalculationException;
}
