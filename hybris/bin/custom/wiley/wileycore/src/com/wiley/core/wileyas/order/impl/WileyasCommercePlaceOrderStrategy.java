package com.wiley.core.wileyas.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.core.order.impl.WileyCommercePlaceOrderStrategy;


public class WileyasCommercePlaceOrderStrategy extends WileyCommercePlaceOrderStrategy
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	protected void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		OrderModel order = result.getOrder();

		order.getEntries().forEach(entry -> entry.setStatus(OrderStatus.CREATED));
		modelService.saveAll(order.getEntries());

		super.beforeSubmitOrder(parameter, result);
	}

	@Override
	protected void fillPaymentAddress(final CartModel cartModel, final OrderModel orderModel)
	{
		// CHANGE: For Phase 4 the billing address stored in paymentAddress.
		final AddressModel paymentAddress = cartModel.getPaymentAddress();
		orderModel.setPaymentAddress(paymentAddress);
	}
}
