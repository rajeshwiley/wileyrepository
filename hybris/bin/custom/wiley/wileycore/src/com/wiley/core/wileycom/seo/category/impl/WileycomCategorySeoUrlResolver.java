package com.wiley.core.wileycom.seo.category.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.resolver.impl.WileyAbstractUrlResolver;


public class WileycomCategorySeoUrlResolver extends WileyAbstractUrlResolver<CategoryModel>
{
	private static final int TOP_LEVEL_PATH_POSITION = 1;
	private static final String TOP_CATEGORY_NAME = "{topCategoryName}";
	private static final String SUBCATEGORY_NAME = "{categoryName}";
	private static final String SUBCATEGORY_CODE = "{categoryCode}";


	private String fullUrlPattern;
	private String topUrlPattern;
	private CategoryService categoryService;


	private String constructOnlyTopSeoUrl(final CategoryModel category)
	{
		String seoName = getSeoName(category);
		return topUrlPattern
				.replace(SUBCATEGORY_NAME, seoName)
				.replace(SUBCATEGORY_CODE, encodeCode(category.getCode()));
	}

	private String constructFullSeoUrl(final CategoryModel topLevelCategory, final CategoryModel category)
	{
		String topSeoName = getSeoName(topLevelCategory);
		String categorySeoName = getSeoName(category);

		return fullUrlPattern
				.replace(TOP_CATEGORY_NAME, topSeoName)
				.replace(SUBCATEGORY_NAME, categorySeoName)
				.replace(SUBCATEGORY_CODE, encodeCode(category.getCode()));
	}

	protected String getSeoName(final CategoryModel category)
	{
		return StringUtils.isNotEmpty(category.getSeoName()) ? urlEncode(category.getSeoName()) : urlSafe(category.getName());
	}

	private CategoryModel getTopLevelCategory(final CategoryModel category)
	{
		final List<CategoryModel> path = getFirstCategoryPath(category);
		//for normal categories path is: root-topLevel-...-category
		//if path length < 2 it means given category is either root or topLevel - top level category cannot be returned
		return path.size() > (TOP_LEVEL_PATH_POSITION + 1) ? path.get(TOP_LEVEL_PATH_POSITION) : null;
	}

	private List<CategoryModel> getFirstCategoryPath(final CategoryModel category)
	{
		return categoryService.getPathsForCategory(category).iterator().next();
	}

	private String encodeCode(final String code)
	{
		//OOTB rules for code handling
		return urlEncode(code).replaceAll("\\+", "%20");
	}

	@Override
	protected String getKey(final CategoryModel source)
	{
		return getClass().getName() + "." + source.getPk();
	}

	@Override
	protected String resolveInternal(final CategoryModel category)
	{
		CategoryModel topLevelCategory = getTopLevelCategory(category);
		return topLevelCategory != null ? constructFullSeoUrl(topLevelCategory, category) : constructOnlyTopSeoUrl(category);
	}

	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	@Required
	public void setFullUrlPattern(final String fullUrlPattern)
	{
		this.fullUrlPattern = fullUrlPattern;
	}

	@Required
	public void setTopUrlPattern(final String topUrlPattern)
	{
		this.topUrlPattern = topUrlPattern;
	}
}
