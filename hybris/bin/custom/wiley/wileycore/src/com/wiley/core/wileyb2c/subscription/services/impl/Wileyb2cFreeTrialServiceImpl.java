package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Collection;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.util.Assert;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialService;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class Wileyb2cFreeTrialServiceImpl implements Wileyb2cFreeTrialService
{

	@Autowired
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy freeTrialSubscriptionTermCheckingStrategy;

	@Autowired
	private Wileyb2cSubscriptionService subscriptionService;

	@Override
	public boolean isProductFreeTrial(final WileyProductModel product)
	{
		validateParameterNotNull(product, "product must not be null");
		return subscriptionService.isProductSubscription(product)
				&& productHasFreeTrialSubscriptionTerm(product.getSubscriptionTerms());
	}

	@Override
	public SubscriptionTermModel getFreeTrialForProduct(@Nonnull final WileyProductModel productModel)
			throws IllegalArgumentException
	{
		validateParameterNotNull(productModel, "ProductModel must not be null");
		Assert.state(productModel.getSubscriptionTerms() != null, () -> "SubscriptionTerms must not be null.");

		for (SubscriptionTermModel subscriptionTerm : productModel.getSubscriptionTerms())
		{
			if (freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(subscriptionTerm))
			{
				return subscriptionTerm;
			}
		}
		throw new IllegalArgumentException(String.format("No FreeTrial found for product: [%s]", productModel.getCode()));
	}

	@Override
	public boolean isFreeTrialEntry(final AbstractOrderEntryModel orderEntry) {
		final WileyProductModel product = (WileyProductModel) orderEntry.getProduct();
		final boolean productFreeTrial = isProductFreeTrial(product);
		SubscriptionTermModel entryTerm = orderEntry.getSubscriptionTerm();
		return productFreeTrial && freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(entryTerm);
	}

	private boolean productHasFreeTrialSubscriptionTerm(final Collection<SubscriptionTermModel> subscriptionTerms)
	{
		return subscriptionTerms.stream()
				.anyMatch(subscriptionTermModel -> freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(subscriptionTermModel));
	}
}
