package com.wiley.core.externaltax.xml.caching.impl;


import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.beans.factory.annotation.Required;


public class WileyVarArgsPropsCacheKey implements CacheKey
{
	private final Object[] params;
	private List<List<String>> excludedFields;

	public WileyVarArgsPropsCacheKey(final Object... params)
	{
		this.params = params;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof WileyVarArgsPropsCacheKey))
		{
			return false;
		}

		final WileyVarArgsPropsCacheKey that = (WileyVarArgsPropsCacheKey) o;

		return isEquals(that);
	}

	protected boolean isEquals(final WileyVarArgsPropsCacheKey that)
	{
		if (params == that.params)
		{
			return true;
		}
		if (params == null || that.params == null)
		{
			return false;
		}

		if (that.params.length != params.length)
		{
			return false;
		}

		return isEqualsPerFields(that);
	}

	private boolean isEqualsPerFields(final WileyVarArgsPropsCacheKey that)
	{
		for (int i = 0; i < that.params.length; i++)
		{
			if (i < excludedFields.size())
			{
				if (!EqualsBuilder.reflectionEquals(params[i], that.params[i], excludedFields.get(i)))
				{
					return false;
				}
			}
			else
			{
				if (!Objects.equals(params[i], that.params[i]))
				{
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		int hashCode = 17;
		final int multiplier = 37;
		for (int i = 0; i < params.length; i++)
		{
			if (i < excludedFields.size())
			{
				hashCode = HashCodeBuilder.reflectionHashCode(
						hashCode,
						multiplier,
						params[i],
						false,
						(Class) null,
						excludedFields.get(i).toArray(new String[0]));
			}
			else
			{
				hashCode = HashCodeBuilder.reflectionHashCode(
						hashCode,
						multiplier,
						params[i]);
			}
		}
		return hashCode;
	}

	@Override
	public CacheUnitValueType getCacheValueType()
	{
		return CacheUnitValueType.SERIALIZABLE;
	}

	@Override
	public Object getTypeCode()
	{
		return "__VarArgsPropsKey__";
	}

	@Override
	public String getTenantId()
	{
		return Registry.getCurrentTenant().getTenantID();
	}

	@Required
	public void setExcludedFields(final List<List<String>> excludedFields)
	{
		this.excludedFields = excludedFields;
	}
}
