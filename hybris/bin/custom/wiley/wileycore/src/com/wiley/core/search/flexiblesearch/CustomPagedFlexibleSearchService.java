package com.wiley.core.search.flexiblesearch;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.Map;


/**
 * Created by Mikhail_Asadchy on 06.10.2016.
 */
public interface CustomPagedFlexibleSearchService extends PagedFlexibleSearchService
{
	<T> SearchPageData<T> search(String query, Map<String, ?> queryParams,
			String countQuery, Map<String, ?> countQueryParams, PageableData pageableData);
}
