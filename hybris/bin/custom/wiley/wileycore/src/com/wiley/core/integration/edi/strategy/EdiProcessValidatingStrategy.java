package com.wiley.core.integration.edi.strategy;

import java.util.Collection;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * Strategy to filter EDI processes for excluding not valid for reporting orders
 */
public interface EdiProcessValidatingStrategy
{
	/**
	 * Finds not valid processes and collects them.
	 * @param processes - all processes
	 * @return - not valid processes Only
	 */
	Collection<WileyExportProcessModel> getRejectedProcesses(Collection<WileyExportProcessModel> processes);
}
