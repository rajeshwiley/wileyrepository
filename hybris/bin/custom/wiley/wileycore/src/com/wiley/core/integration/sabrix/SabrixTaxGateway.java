package com.wiley.core.integration.sabrix;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.validation.constraints.NotNull;

import com.wiley.core.integration.sabrix.dto.Wileyb2cTaxResponse;


public interface SabrixTaxGateway {

	@NotNull
	Wileyb2cTaxResponse calculateTaxForOrder(@NotNull AbstractOrderModel order);

}
