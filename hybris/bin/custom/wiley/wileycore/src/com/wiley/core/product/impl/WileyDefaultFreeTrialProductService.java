package com.wiley.core.product.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyFreeTrialProductService;
import com.wiley.core.product.dao.WileyFreeTrialProductDao;

import static java.lang.String.format;


/**
 * Created by Aliaksei_Zlobich on 3/16/2016.
 */
public class WileyDefaultFreeTrialProductService implements WileyFreeTrialProductService
{

	@Resource
	private WileyFreeTrialProductDao freeTrialProductDao;

	@Resource
	private ProductReferenceService productReferenceService;

	@Override
	public WileyFreeTrialProductModel getFreeTrialProductByCode(@Nonnull final String code)
	{
		Assert.notNull(code);
		final List<WileyFreeTrialProductModel> freeTrials = freeTrialProductDao.findFreeTrialProductsByCode(code);

		ServicesUtil.validateIfSingleResult(freeTrials,
				format("%s with code '%s' not found!", WileyFreeTrialProductModel._TYPECODE, code),
				format("%s code '%s' is not unique! Found %d items.", WileyFreeTrialProductModel._TYPECODE, code,
						freeTrials.size()));

		return freeTrials.get(0);
	}

	@Nullable
	@Override
	public WileyFreeTrialProductModel getRelatedFreeTrialForProduct(final ProductModel productModel)
			
	{
		Collection<ProductReferenceModel> referenceList = productReferenceService
				.getProductReferencesForSourceProduct(productModel, ProductReferenceTypeEnum.FREE_TRIAL, true);
		if (CollectionUtils.isEmpty(referenceList) && (productModel instanceof WileyVariantProductModel))
		{
			final ProductModel baseFreeTrialModel = ((WileyVariantProductModel) productModel).getBaseProduct();
			referenceList = productReferenceService
					.getProductReferencesForSourceProduct(baseFreeTrialModel, ProductReferenceTypeEnum.FREE_TRIAL, true);
		}
		WileyFreeTrialProductModel relatedFreeTrialProduct = null;
		if (!CollectionUtils.isEmpty(referenceList))
		{
			relatedFreeTrialProduct = (WileyFreeTrialProductModel) referenceList.iterator().next().getTarget();
		}
		return relatedFreeTrialProduct;
	}
}
