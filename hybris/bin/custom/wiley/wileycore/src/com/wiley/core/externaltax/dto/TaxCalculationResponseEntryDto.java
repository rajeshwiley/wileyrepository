package com.wiley.core.externaltax.dto;

import java.util.List;


public class TaxCalculationResponseEntryDto
{
	private Integer id;
	private List<TaxValueDto> taxes;

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public List<TaxValueDto> getTaxes()
	{
		return taxes;
	}

	public void setTaxes(final List<TaxValueDto> taxes)
	{
		this.taxes = taxes;
	}
}
