package com.wiley.core.payment.command;

import de.hybris.platform.payment.commands.CaptureCommand;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.response.WileyCaptureResult;


public class WileyPaymentCaptureCommand implements CaptureCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPaymentCaptureCommand.class);
	@Autowired
	private WileyPaymentGateway wileyPaymentGateway;

	@Override
	public CaptureResult perform(final CaptureRequest request)
	{
		Preconditions.checkArgument(request instanceof WileyCaptureRequest,
				"[request] param should instance of WileyCaptureRequest");
		WileyCaptureRequest wileyCaptureRequest = (WileyCaptureRequest) request;
		WileyCaptureResult result;
		try
		{
			result = wileyPaymentGateway.capture(wileyCaptureRequest);

			if (result.getStatus() == WileyTransactionStatusEnum.SUCCESS
					&& (result.getTotalAmount() == null
					|| result.getTotalAmount().compareTo(wileyCaptureRequest.getTotalAmount()) != 0))
			{
				LOG.error("Captured amount '{}' differs from requested amount '{}'.", result.getTotalAmount(),
						wileyCaptureRequest.getTotalAmount());
				result = createFailureResult(WileyTransactionStatusEnum.AMOUNT_MISMATCH);
			}
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to capture payment due to unexpected error", ex);
			result = createFailureResult(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR);

		}
		return result;
	}

	private WileyCaptureResult createFailureResult(final WileyTransactionStatusEnum status)
	{
		WileyCaptureResult captureResult = new WileyCaptureResult();
		captureResult.setStatus(status);
		captureResult.setTransactionStatus(TransactionStatus.ERROR);
		return captureResult;
	}
}
