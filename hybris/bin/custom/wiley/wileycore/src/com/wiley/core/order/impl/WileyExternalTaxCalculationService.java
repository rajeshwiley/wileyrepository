package com.wiley.core.order.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.TaxValue;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


public class WileyExternalTaxCalculationService extends WileyCalculationService
{
	// Overrides Total tax calculation to avoid tax calculation for each qty of product

	@Override
	protected void calculateTotalTaxValues(final AbstractOrderEntryModel entry)
	{
		final AbstractOrderModel order = entry.getOrder();
		final double totalPrice = entry.getTotalPrice().doubleValue();
		final CurrencyModel curr = order.getCurrency();
		final int digits = curr.getDigits().intValue();
		//As we use external tax service to calculate tax for specified quantity we should apply taxValue for 1 unit of product
		final double quantity = 1.0;
		entry.setTaxValues(TaxValue.apply(quantity, totalPrice, digits, entry.getTaxValues(), order.getNet().booleanValue(),
				curr.getIsocode()));
	}

	//Overrides Total tax calculation to avoid tax calculation for each qty of product
	@Override
	protected void addAbsoluteEntryTaxValue(final long entryQuantity, final TaxValue taxValue,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		Map<Set<TaxValue>, Double> taxGroupMap = taxValueMap.get(taxValue);
		Double quantitySum = null;
		//As we use external tax service to calculate tax for specified quantity we should apply taxValue for 1 unit of product
		final int quantity = 1;
		final Set<TaxValue> absoluteTaxGroupKey = Collections.singleton(taxValue);
		if (taxGroupMap == null)
		{
			taxGroupMap = new LinkedHashMap<Set<TaxValue>, Double>(4);
			taxValueMap.put(taxValue, taxGroupMap);
		}
		else
		{
			quantitySum = taxGroupMap.get(absoluteTaxGroupKey);
		}
		taxGroupMap.put(absoluteTaxGroupKey, Double.valueOf((quantitySum != null ? quantitySum.doubleValue() : 0) + quantity));
	}
}
