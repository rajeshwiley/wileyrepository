package com.wiley.core.wileycom.product.valuetranslator.adapter.impl;

import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.wileycom.valuetranslator.adapter.AbstractWileycomImportAdapter;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomTemporaryRestrictionImportAdapterImpl extends AbstractWileycomImportAdapter
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomTemporaryRestrictionImportAdapterImpl.class);
	private static final String DELIMITER = "\\|";
	@Resource
	private ModelService modelService;
	private String dateFormat;

	/**
	 * Import {@link de.hybris.platform.voucher.model.DateRestrictionModel} to specified voucher
	 *
	 * @param cellValue
	 * 		String representation of {@link de.hybris.platform.voucher.model.DateRestrictionModel}.
	 * 		For example: 22.12.2015 10:22:33+0300|22.12.2017 12:22:33+0300 where 22.12.2015 10:22:33+0300 - start date
	 * 		and 22.12.2017 12:22:33+0300 end date
	 * @param promotionVoucher
	 * 		promotion voucher to import
	 * @return the code of product if found
	 * @throws UnresolvedValueException
	 * 		when valueExpr is in invalid format
	 */
	@Override
	public void performImport(final String cellValue, final Item promotionVoucher) throws UnresolvedValueException
	{
		Assert.hasText(cellValue);
		Assert.notNull(promotionVoucher);
		String[] temporaryRestrictionStrings = cellValue.split(DELIMITER);
		if (temporaryRestrictionStrings.length != 2)
		{
			throwUnresolvedValueException();
		}
		PromotionVoucherModel promotionVoucherModel = modelService.get(promotionVoucher);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Date startDate = null, endDate = null;
		try
		{
			startDate = simpleDateFormat.parse(temporaryRestrictionStrings[0]);
			endDate = simpleDateFormat.parse(temporaryRestrictionStrings[1]);
		}
		catch (ParseException exception)
		{
			LOG.warn("Cannot parse date for temporary restriction  for promotion voucher " + promotionVoucherModel.getCode(),
					exception);
			throwUnresolvedValueException();
		}

		promotionVoucherModel.getRestrictions().stream().filter(restriction -> restriction instanceof DateRestrictionModel)
				.forEach(restriction -> modelService.remove(restriction));

		DateRestrictionModel dateRestrictionModel = new DateRestrictionModel();
		dateRestrictionModel.setStartDate(startDate);
		dateRestrictionModel.setEndDate(endDate);
		dateRestrictionModel.setVoucher(promotionVoucherModel);
		modelService.save(dateRestrictionModel);
	}

	private void throwUnresolvedValueException() throws UnresolvedValueException
	{
		throw new UnresolvedValueException("Temporary restriction has invalid format. Valid format is <start date-time>|<end"
				+ " date-time> where data-time has format " + dateFormat);
	}

	@Required
	public void setDateFormat(final String dateFormat)
	{
		this.dateFormat = dateFormat;
	}
}
