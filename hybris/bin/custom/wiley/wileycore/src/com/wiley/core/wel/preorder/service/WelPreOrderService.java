package com.wiley.core.wel.preorder.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Optional;


public interface WelPreOrderService
{
	Optional<ProductModel> getActiveProductForPreOrderProduct(ProductModel preOrderProduct);

	boolean isPreOrder(AbstractOrderModel abstractOrder);
}
