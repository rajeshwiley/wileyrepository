package com.wiley.core.integration;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Interface for working with external cart modifications. Base function are storing {@link ExternalCartModification}.<br/>
 * The storage uses entry number and product code as a key for storage.
 */
public interface ExternalCartModificationsStorageService
{

	/**
	 * If the storage is enabled, pushes all external cart modification at the end. FIFO.
	 *
	 * @param cartModel
	 * 		cart
	 * @param externalCartModifications
	 * 		modifications which will be stored.
	 */
	void pushAll(@Nonnull CartModel cartModel, @Nonnull List<ExternalCartModification> externalCartModifications);

	/**
	 * If the storage is enabled, removes all external cart modification from the storage and returns them.
	 *
	 * @param cartModel
	 * @return all external cart modifications related to cart or empty list.
	 */
	@Nonnull
	List<ExternalCartModification> popAll(@Nonnull CartModel cartModel);

	/**
	 * Returns {@link ExternalCartModification} by cart and cart entry. <br/>
	 * Removes found cart modification from the storage.<br/>
	 *
	 * Required fields for cart entry are:
	 * <ul>
	 * <li>entryNumber</li>
	 * <li>product</li>
	 * </ul>
	 *
	 * @param cartModel
	 * @param cartEntryModel
	 * @return
	 */
	@Nonnull
	Optional<ExternalCartModification> popLastModificationForCartAndEntry(@Nonnull CartModel cartModel,
			@Nonnull CartEntryModel cartEntryModel);

	/**
	 * Enable storage for the current session.
	 */
	void enableStorageForCurrentSession();

	/**
	 * Disable storage for the current session.
	 */
	void disableStorageForCurrentSession();

}
