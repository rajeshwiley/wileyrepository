package com.wiley.core.integration.cdm.service;

import com.wiley.core.integration.cdm.dto.CDMCreateUserResponse;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;


public interface WileyCdmCustomerService
{
	CDMCreateUserResponse createCDMCustomer(@Nonnull CustomerModel customer, @Nonnull BaseSiteModel site);
}
