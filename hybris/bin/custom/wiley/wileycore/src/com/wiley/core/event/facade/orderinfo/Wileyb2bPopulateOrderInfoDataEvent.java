package com.wiley.core.event.facade.orderinfo;

import com.wiley.core.event.facade.WileyOneObjectFacadeEvent;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public class Wileyb2bPopulateOrderInfoDataEvent extends WileyOneObjectFacadeEvent<OrderInfoData>
{
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public Wileyb2bPopulateOrderInfoDataEvent(final OrderInfoData source) {
        super(source);
    }
}
