package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileyb2c.classification.impl.Wileyb2cResolvePublicationDatePLPStrategy;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * This class find newest publication date from all variants and change it to UTC timezone.
 * It's done because it's impossible to send request with date range to Solr with timezone.
 * It will give business user possibility to use server date during boost rule creation
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cNewestPublicationDateValueProvider extends AbstractWileycomValueProvider<Date>
		implements FieldValueProvider, Serializable
{

	private static final String TIME_ZONE_PATTERN = "Z";
	static final String DATE_PATTERN = Wileyb2cResolvePublicationDatePLPStrategy.DATE_PATTERN + TIME_ZONE_PATTERN;
	private static final String UTC_TIMEZONE = "+0000";
	private transient WileyProductRestrictionService wileyProductRestrictionService;
	private transient Wileyb2cClassificationService wileyb2cClassificationService;


	@Override
	protected List<Date> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model)
			throws FieldValueProviderException
	{

		final ProductModel baseProduct = ((VariantProductModel) model).getBaseProduct();

		final Optional<Date> newestPublicationDate = baseProduct.getVariants().stream().filter(
				wileyProductRestrictionService::isSearchable).map(variantProductModel -> wileyb2cClassificationService
				.resolveAttribute(variantProductModel, Wileyb2cClassificationAttributes.PUBLICATION_DATE))
				.filter(StringUtils::isNoneEmpty).map(dateString ->
				{
					try
					{
						return new SimpleDateFormat(DATE_PATTERN).parse(dateString + UTC_TIMEZONE);
					}
					catch (ParseException e)
					{
						throw new RuntimeException("Could not parse date " + dateString, e);
					}
				}).min(Comparator.reverseOrder());
		if (newestPublicationDate.isPresent())
		{
			return Collections.singletonList(newestPublicationDate.get());
		}

		return Collections.emptyList();
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}

	@Required
	public void setWileyb2cClassificationService(
			final Wileyb2cClassificationService wileyb2cClassificationService)
	{
		this.wileyb2cClassificationService = wileyb2cClassificationService;
	}
}
