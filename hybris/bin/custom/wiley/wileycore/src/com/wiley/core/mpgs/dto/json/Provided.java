package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Provided
{
	public Card getCard()
	{
		return card;
	}

	public void setCard(final Card card)
	{
		this.card = card;
	}

	private Card card;

}
