package com.wiley.core.order.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.dao.WileyOrderProcessDao;


public class WileyOrderProcessDaoImpl implements WileyOrderProcessDao
{
	private static final String PROCESS_ACTIVE_FOR_QUERY = "select {pk} from {WileyOrderProcess} where {activeFor} = ?order";
	private static final String PROCESS_IN_STATE_FOR_ORDER_QUERY =
			"select {pk} from {OrderProcess} where "
					+ "{" + OrderProcessModel.ORDER + "} = ?orderModel and "
					+ "{" + OrderProcessModel.STATE + "} = ?processState";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<WileyOrderProcessModel> findProcessActiveFor(@Nonnull final OrderModel order)
	{
		Preconditions.checkNotNull(order, "Missing order parameter");

		FlexibleSearchQuery query = new FlexibleSearchQuery(PROCESS_ACTIVE_FOR_QUERY);
		query.setDisableCaching(true);
		query.addQueryParameter("order", order);
		SearchResult<WileyOrderProcessModel> results = flexibleSearchService.search(query);

		WileyOrderProcessModel process = results.getCount() == 1 ? results.getResult().get(0) : null;
		return Optional.ofNullable(process);
	}

	@Override
	public Optional<OrderProcessModel> findProcessInSateForOrder(@Nonnull final OrderModel orderModel,
			@Nonnull final ProcessState processState)
	{
		Preconditions.checkNotNull(orderModel, "Missing orderModel parameter");
		Preconditions.checkNotNull(orderModel, "Missing processState parameter");

		FlexibleSearchQuery query = new FlexibleSearchQuery(PROCESS_IN_STATE_FOR_ORDER_QUERY);
		query.setDisableCaching(true);
		query.addQueryParameter("orderModel", orderModel);
		query.addQueryParameter("processState", processState);
		SearchResult<OrderProcessModel> results = flexibleSearchService.search(query);

		OrderProcessModel process = results.getCount() > 0 ? results.getResult().get(0) : null;
		return Optional.ofNullable(process);
	}
}
