package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;
import com.wiley.core.integration.b2baccounts.WileyB2BAccountsGateway;
import com.wiley.core.wileyb2b.order.Wileyb2bCommerceAddToCartStrategy;
import com.wiley.core.wileycom.order.impl.WileycomCommerceAddToCartStrategyImpl;
import com.wiley.core.wileycom.product.exception.ProductHasAlreadyBeenPurchasedException;


/**
 * Wileyb2b specific add to cart strategy.
 */
public class Wileyb2bCommerceAddToCartStrategyImpl extends WileycomCommerceAddToCartStrategyImpl
		implements Wileyb2bCommerceAddToCartStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCommerceAddToCartStrategyImpl.class);

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Resource
	private WileyB2BAccountsGateway wileyB2BAccountsGateway;

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	public void addAllEntriesToCart(final CartModel fromCartModel, final CartModel toCartModel,
			final List<CommerceCartModification> modifications)
			throws CommerceCartModificationException
	{
		final List<AddToCartData> addToCartDataList = new ArrayList<>(fromCartModel.getEntries().size());

		try
		{
			CommerceCartParameter cartEntryParameter;
			for (AbstractOrderEntryModel entry : fromCartModel.getEntries())
			{
				cartEntryParameter = getCommerceCartParameterForCartEntry(toCartModel, entry);

				addToCartDataList.add(addToCartInternal(cartEntryParameter));
			}

			if (hasAnyProductBeenAdded(addToCartDataList))
			{
				getCalculationStrategy().calculateCart(getCommerceCartParameterForCart(toCartModel));
			}


			for (AddToCartData addToCartData : addToCartDataList)
			{
				if (hasProductBeenAdded(addToCartData))
				{
					doExternalModification(addToCartData);
				}
				modifications.add(addToCartData.getModification());
			}
		}
		finally
		{
			for (AddToCartData addToCartData : addToCartDataList)
			{
				this.afterAddToCart(addToCartData.getCommerceCartParameter(), addToCartData.getModification());
			}
		}
	}

	private CommerceCartParameter getCommerceCartParameterForCartEntry(final CartModel toCartModel,
			final AbstractOrderEntryModel entry)
	{
		final CommerceCartParameter parameter = getCommerceCartParameterForCart(toCartModel);
		parameter.setProduct(entry.getProduct());
		parameter.setPointOfService(entry.getDeliveryPointOfService());
		parameter.setQuantity(entry.getQuantity() == null ? 0L : entry.getQuantity().longValue());
		parameter.setUnit(entry.getUnit());
		parameter.setCreateNewEntry(false);
		return parameter;
	}

	private CommerceCartParameter getCommerceCartParameterForCart(final CartModel toCartModel)
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(toCartModel);
		parameter.setEnableHooks(true);
		return parameter;
	}

	protected AddToCartData addToCartInternal(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		ServicesUtil.validateParameterNotNull(cartModel, "Parameter cartModel cannot be null.");
		ServicesUtil.validateParameterNotNull(productModel, "Parameter productModel cannot be null.");
		final long quantityToAdd = parameter.getQuantity();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();
		final boolean forceNewEntry = parameter.isCreateNewEntry();

		final AddToCartData result = new AddToCartData();
		result.setCommerceCartParameter(parameter);

		this.beforeAddToCart(parameter);
		validateAddToCart(parameter);

		LOG.debug("Adding product [{}] to cart [{}].", productModel.getCode(), cartModel.getCode());

		try
		{
			getProductService().getProductForCode(productModel.getCode());
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.debug(String.format("Cannot add product [%s] to cart [%s].", productModel.getCode(), cartModel.getCode()), e);

			CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
			modification.setQuantityAdded(0);
			modification.setQuantity(parameter.getQuantity());
			final CartEntryModel entry = new CartEntryModel();
			entry.setProduct(productModel);
			modification.setEntry(entry);

			result.setModification(modification);
			return result;
		}

		UnitModel orderableUnit = getUnitModel(productModel, parameter.getUnit());

		// So now work out what the maximum allowed to be added is (note that this may be negative!)
		final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
				deliveryPointOfService);
		final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
		final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

		// We are allowed to add items to the cart
		CartEntryModel cartEntryModel;

		if (deliveryPointOfService == null)
		{
			// Modify the cart
			cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange,
					orderableUnit, APPEND_AS_LAST, !forceNewEntry);
		}
		else
		{
			// Find the entry to modify
			final Integer entryNumber = getEntryForProductAndPointOfService(cartModel, productModel,
					deliveryPointOfService);

			// Modify the cart
			cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange,
					orderableUnit, entryNumber, entryNumber >= 0 && !forceNewEntry);

			if (cartEntryModel != null)
			{
				cartEntryModel.setDeliveryPointOfService(deliveryPointOfService);
			}
		}

		result.setActualAllowedQuantityChange(actualAllowedQuantityChange);
		result.setCartLevelAfterQuantityChange(cartLevelAfterQuantityChange);

		if (cartEntryModel != null)
		{
			result.setCartEntryModel(cartEntryModel);
			result.setEntryBeforeRecalculation(createCartEntryStub(cartEntryModel.getEntryNumber(), cartEntryModel.getProduct()));
		}
		else
		{
			LOG.warn("Unable to enrich AddToCartData result with some data: cartEntryModel in NULL");
		}

		// if product has been added to cart, return CommerceCartModification with status SUCCESS
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		result.setModification(modification);

		return result;
	}

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		CommerceCartModification modification = new CommerceCartModification();
		try
		{
			AddToCartData addToCartData = addToCartInternal(parameter);
			if (hasProductBeenAdded(addToCartData))
			{
				getCalculationStrategy().calculateCart(parameter);
				doExternalModification(addToCartData);
			}
			modification = addToCartData.getModification();
		}
		finally
		{
			this.afterAddToCart(parameter, modification);
		}
		return modification;
	}

	@Override
	protected void validateAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
	{
		super.validateAddToCart(parameters);

		final ProductModel productModel = parameters.getProduct();
		final B2BCustomerModel customer = (B2BCustomerModel) parameters.getUser();
		final String sapAccountNumber = customer.getDefaultB2BUnit().getSapAccountNumber();
		
		try
		{
			if (isCheckLicenseRequired(productModel) && wileyB2BAccountsGateway.checkLicense(productModel, customer))
			{
				// wrapping ProductHasAlreadyBeenPurchasedException into CommerceCartModificationException
				// to support OOTB interface
				final String errorMessage = String.format("Product [%s] has already been purchased by b2b unit [%s].",
						productModel.getCode(), sapAccountNumber);
				throw new CommerceCartModificationException(errorMessage,
						new ProductHasAlreadyBeenPurchasedException(errorMessage));
			}
		}
		catch (final ExternalSystemException e)
		{
			//failed to get external license - skip
			LOG.warn(
					String.format("Failed to receive external license for product [%s] and b2b unit [%s] because: [%s]",
							productModel.getCode(), sapAccountNumber, e.getMessage()), e);
		}
	}

	protected boolean isCheckLicenseRequired(final ProductModel productModel)
	{
		boolean isCheckLicenseRequired = false;
		final WileyProductSubtypeEnum subtype = productModel.getSubtype();
		if (WileyProductSubtypeEnum.OBOOK.equals(subtype))
		{
			isCheckLicenseRequired = true;
		}
		return isCheckLicenseRequired;
	}

	protected boolean hasProductBeenAdded(final AddToCartData addToCartData)
	{
		final String statusCode = addToCartData.getModification().getStatusCode();
		final boolean result = CommerceCartModificationStatus.UNAVAILABLE.equals(statusCode)
				|| CommerceCartModificationStatus.NO_STOCK.equals(statusCode);
		return !result;
	}

	private boolean hasAnyProductBeenAdded(final List<AddToCartData> addToCartDataList)
	{
		return addToCartDataList.stream().anyMatch(entry -> hasProductBeenAdded(entry));
	}

	protected void doExternalModification(final AddToCartData addToCartData)
	{
		final CommerceCartParameter parameter = addToCartData.getCommerceCartParameter();
		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		final long quantityToAdd = parameter.getQuantity();

		final Optional<ExternalCartModification> externalModificationForEntry =
				externalCartModificationsStorageService.popLastModificationForCartAndEntry(cartModel,
						addToCartData.getEntryBeforeRecalculation());

		final long actualAllowedQuantityChange = addToCartData.getActualAllowedQuantityChange();
		final long cartLevelAfterQuantityChange = addToCartData.getCartLevelAfterQuantityChange();

		// Create the cart modification result
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setQuantity(quantityToAdd);
		if (externalModificationForEntry.isPresent())
		{
			final CartEntryModel entry = new CartEntryModel();
			entry.setProduct(productModel);
			modification.setEntry(entry);
			modification.setQuantityAdded(
					actualAllowedQuantityChange - (cartLevelAfterQuantityChange - externalModificationForEntry.get()
							.getQuantity()));
			modification.setStatusCode(externalModificationForEntry.get().getStatusCode());
			modification.setMessage(externalModificationForEntry.get().getMessage());
			modification.setMessageType(externalModificationForEntry.get().getMessageType());
		}
		else
		{
			modification.setQuantityAdded(actualAllowedQuantityChange);
			modification.setEntry(addToCartData.getCartEntryModel());
			modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		}

		addToCartData.setModification(modification);
	}

	private UnitModel getUnitModel(final ProductModel productModel, final UnitModel unit)
			throws CommerceCartModificationException
	{
		if (unit == null)
		{
			try
			{
				return getProductService().getOrderableUnit(productModel);
			}
			catch (final ModelNotFoundException e)
			{
				throw new CommerceCartModificationException(e.getMessage(), e);
			}
		}
		return unit;
	}

	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return quantityToAdd;
	}

	private CartEntryModel createCartEntryStub(final Integer entryNumber, final ProductModel productModel)
	{
		final CartEntryModel stub = new CartEntryModel();
		stub.setEntryNumber(entryNumber);
		stub.setProduct(productModel);
		return stub;
	}

	private CommerceCartCalculationStrategy getCalculationStrategy()
	{
		return getCommerceCartCalculationStrategy();
	}

	public static class AddToCartData
	{
		private CommerceCartParameter commerceCartParameter;
		private CommerceCartModification modification;
		private CartEntryModel entryBeforeRecalculation;
		private CartEntryModel cartEntryModel;
		private long actualAllowedQuantityChange;
		private long cartLevelAfterQuantityChange;

		public CommerceCartParameter getCommerceCartParameter()
		{
			return commerceCartParameter;
		}

		public void setCommerceCartParameter(final CommerceCartParameter commerceCartParameter)
		{
			this.commerceCartParameter = commerceCartParameter;
		}

		public CommerceCartModification getModification()
		{
			return modification;
		}

		public void setModification(final CommerceCartModification modification)
		{
			this.modification = modification;
		}

		public CartEntryModel getEntryBeforeRecalculation()
		{
			return entryBeforeRecalculation;
		}

		public void setEntryBeforeRecalculation(final CartEntryModel entryBeforeRecalculation)
		{
			this.entryBeforeRecalculation = entryBeforeRecalculation;
		}

		public CartEntryModel getCartEntryModel()
		{
			return cartEntryModel;
		}

		public void setCartEntryModel(final CartEntryModel cartEntryModel)
		{
			this.cartEntryModel = cartEntryModel;
		}

		public long getActualAllowedQuantityChange()
		{
			return actualAllowedQuantityChange;
		}

		public void setActualAllowedQuantityChange(final long actualAllowedQuantityChange)
		{
			this.actualAllowedQuantityChange = actualAllowedQuantityChange;
		}

		public long getCartLevelAfterQuantityChange()
		{
			return cartLevelAfterQuantityChange;
		}

		public void setCartLevelAfterQuantityChange(final long cartLevelAfterQuantityChange)
		{
			this.cartLevelAfterQuantityChange = cartLevelAfterQuantityChange;
		}
	}
}
