package com.wiley.core.security;

/**
 * @author Mikhail_Kondratyev
 */
public interface WileyMessageEncoder
{
	/**
	 * Hashes the provided message.
	 *
	 * @param message
	 * 		message to hash
	 * @return the hash
	 */
	String encode(String message);

	/**
	 * Validates the hash for the provided message.
	 *
	 * @param message
	 * 		message to hash
	 * @param hash
	 * 		previosly calculated hash
	 * @return {@code true} if the hash corresponds the provided message, {@code false} otherwise
	 */
	boolean check(String message, String hash);
}
