package com.wiley.core.wileyb2c.serialization.json;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cVariantDto
{
	private String code;
	private Map<String, Double> priceByCurrencies = new HashMap<>();
	private boolean countable;
	private boolean hasExternalStores;
	private String publicationDate;
	private String purchaseOptionName;
	private String purchaseOptionDescription;
	private String url;

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getCode()
	{
		return code;
	}

	public void addPrice(final String currency, final Double priceData)
	{
		this.priceByCurrencies.put(currency, priceData);
	}

	public Map<String, Double> getPriceByCurrencies()
	{
		return priceByCurrencies;
	}

	public boolean isCountable()
	{
		return countable;
	}

	public void setCountable(final boolean countable)
	{
		this.countable = countable;
	}

	public boolean isHasExternalStores()
	{
		return hasExternalStores;
	}

	public void setHasExternalStores(final boolean hasExternalStores)
	{
		this.hasExternalStores = hasExternalStores;
	}

	public String getPublicationDate()
	{
		return publicationDate;
	}

	public void setPublicationDate(final String publicationDate)
	{
		this.publicationDate = publicationDate;
	}

	public String getPurchaseOptionName()
	{
		return purchaseOptionName;
	}

	public void setPurchaseOptionName(final String purchaseOptionName)
	{
		this.purchaseOptionName = purchaseOptionName;
	}

	public String getPurchaseOptionDescription()
	{
		return purchaseOptionDescription;
	}

	public void setPurchaseOptionDescription(final String purchaseOptionDescription)
	{
		this.purchaseOptionDescription = purchaseOptionDescription;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}
}
