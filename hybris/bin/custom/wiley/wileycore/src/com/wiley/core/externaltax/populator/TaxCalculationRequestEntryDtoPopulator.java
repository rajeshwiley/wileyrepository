package com.wiley.core.externaltax.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;
import com.wiley.core.externaltax.strategies.WileyTaxCodeStrategy;
import com.wiley.core.product.WileyProductEditionFormatService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationRequestEntryDtoPopulator implements Populator<AbstractOrderEntryModel, TaxCalculationRequestEntryDto>
{
	private BaseStoreService baseStoreService;
	private Map<String, WileyTaxCodeStrategy> wileyTaxCodeStrategies;
	private String defaultWileyTaxCodeStrategy;
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public void populate(final AbstractOrderEntryModel source, final TaxCalculationRequestEntryDto target)
			throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setId(source.getEntryNumber().toString());
		target.setAmount(source.getTaxableTotalPrice());

		final Pair<String, String> taxStrategyAndCode = getTaxStrategyAndCode(source);
		target.setCodeType(taxStrategyAndCode.getLeft());
		target.setCode(taxStrategyAndCode.getRight());
		target.setProductType(getProductTypeCode(source));
	}

	private String getProductTypeCode(final AbstractOrderEntryModel source)
	{
		return wileyProductEditionFormatService.isProductShippable(source.getProduct()) ? WileyCoreConstants.PHYSICAL_PRODUCT :
				WileyCoreConstants.DIGITAL_PRODUCT;
	}

	Pair<String, String> getTaxStrategyAndCode(final AbstractOrderEntryModel entry)
	{
		String taxStrategy = getTaxStrategyForCurrentBaseStore();
		Optional<String> taxCode = getTaxCodeForStrategy(taxStrategy, entry);
		if (taxCode.isPresent())
		{
			return new ImmutablePair<>(taxStrategy, taxCode.get());
		}
		else
		{
			taxCode = getTaxCodeForStrategy(defaultWileyTaxCodeStrategy, entry);
			return new ImmutablePair<>(defaultWileyTaxCodeStrategy, taxCode.orElse(null));
		}
	}

	Optional<String> getTaxCodeForStrategy(final String taxStrategy, final AbstractOrderEntryModel entry)
	{

		final WileyTaxCodeStrategy taxCodeStrategy = wileyTaxCodeStrategies.get(taxStrategy);
		return taxCodeStrategy.getTaxCode(entry);
	}

	private String getTaxStrategyForCurrentBaseStore()
	{
		return baseStoreService.getCurrentBaseStore().getTaxCodeStrategyQualifier();
	}

	@Required
	public void setDefaultWileyTaxCodeStrategy(final String defaultWileyTaxCodeStrategy)
	{
		this.defaultWileyTaxCodeStrategy = defaultWileyTaxCodeStrategy;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	@Required
	public void setWileyTaxCodeStrategies(final Map<String, WileyTaxCodeStrategy> wileyTaxCodeStrategies)
	{
		this.wileyTaxCodeStrategies = wileyTaxCodeStrategies;
	}

	@Required
	public void setWileyProductEditionFormatService(final WileyProductEditionFormatService wileyProductEditionFormatService)
	{
		this.wileyProductEditionFormatService = wileyProductEditionFormatService;
	}
}
