package com.wiley.core.product;

import com.wiley.core.model.WileyProductVariantSetModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;


/**
 * Interface for working with {@link WileyProductVariantSetModel}.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyProductVariantSetService
{


	/**
	 * Gets wiley product set by products which the set contains.
	 *
	 * @param products
	 * 		the products
	 * @return list of {@link WileyProductVariantSetInformation} for founded product sets
	 */
	@Nonnull
	List<WileyProductVariantSetInformation> getWileyProductSetsByProducts(@Nonnull Set<ProductModel> products);

	/**
	 * Gets wiley product sets that belong to given base product
	 *
	 * @param product
	 * 		base product
	 * @return list of {@link WileyProductVariantSetInformation} for founded product sets
	 */
	@Nonnull
	List<WileyProductVariantSetInformation> getWileyProductSetsByBaseProduct(@Nonnull ProductModel product);

	/**
	 * Gets set entries.
	 *
	 * @param set
	 * 		the set
	 * @return the set entries
	 */
	@Nonnull
	List<GenericVariantProductModel> getSetEntries(@Nonnull WileyProductVariantSetModel set);

}
