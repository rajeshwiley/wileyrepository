package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.dto.BillingInfo;

import java.util.Currency;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;
import com.wiley.core.payment.strategies.CreateSubscriptionAuthorizeRequestStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;


public class CreateSubscriptionAuthorizeRequestStrategyImpl implements CreateSubscriptionAuthorizeRequestStrategy
{
	@Autowired
	private WileyTransactionIdGeneratorStrategy transactionStrategy;

	@Autowired
	private AmountToPayCalculationStrategy amountToPayCalculationStrategy;

	@Autowired
	private WileyCustomerAccountService wileyCustomerAccountService;

	@Override
	public WileySubscriptionAuthorizeRequest createRequest(final AbstractOrderModel order)
	{
		CreditCardPaymentInfoModel paymentInfo = findPaymentInfo(order);
		UserModel user = order.getUser();

		return new WileySubscriptionAuthorizeRequest(order.getSite().getUid(),
				paymentInfo.getSubscriptionId(),
				Currency.getInstance(order.getCurrency().getIsocode()),
				amountToPayCalculationStrategy.getOrderAmountToPay(order),
				createBillingInfo(user, Optional.ofNullable(paymentInfo.getBillingAddress())),
				order.getStore().getPaymentProvider());
	}

	private BillingInfo createBillingInfo(final UserModel user, final Optional<AddressModel> paymentInfoBillingAddress)
	{
		AddressModel paymentAddress = paymentInfoBillingAddress.orElse(wileyCustomerAccountService.getPaymentAddress(user));
		Preconditions.checkState(paymentAddress != null, "Customer has no default payment address");

		final BillingInfo billingInfo = new BillingInfo();
		if (paymentAddress.getCountry() != null)
		{
			billingInfo.setCountry(paymentAddress.getCountry().getNumeric());
		}
		billingInfo.setPostalCode(paymentAddress.getPostalcode());
		billingInfo.setStreet1(paymentAddress.getLine1());
		return billingInfo;
	}

	private CreditCardPaymentInfoModel findPaymentInfo(final AbstractOrderModel order)
	{
		Preconditions.checkState(order.getUser() instanceof CustomerModel, "Order should belong to CustomerModel");
		CustomerModel customer = (CustomerModel) order.getUser();
		Preconditions.checkState(customer.getDefaultPaymentInfo() instanceof CreditCardPaymentInfoModel,
				"Customer payment info should be Credit Card Payment Info");
		return (CreditCardPaymentInfoModel) customer.getDefaultPaymentInfo();
	}
}
