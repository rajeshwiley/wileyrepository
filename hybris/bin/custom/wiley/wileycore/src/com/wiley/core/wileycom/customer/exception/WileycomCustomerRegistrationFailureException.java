package com.wiley.core.wileycom.customer.exception;

public class WileycomCustomerRegistrationFailureException extends IllegalStateException
{
	public WileycomCustomerRegistrationFailureException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
