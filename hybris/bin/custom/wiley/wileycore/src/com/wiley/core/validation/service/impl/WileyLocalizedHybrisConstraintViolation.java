package com.wiley.core.validation.service.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.validation.services.impl.DefaultHybrisConstraintViolation;
import de.hybris.platform.validation.services.impl.LocalizedHybrisConstraintViolation;

import java.util.Locale;


/**
 * OOTB copy of {@link LocalizedHybrisConstraintViolation} logic. This class adjust message generation.
 * OOTB takes violation language from session locale so if 'en_US' is violated, session locale could be 'en', that is not valid.
 * This class uses violation locale for message.
 */
public class WileyLocalizedHybrisConstraintViolation extends DefaultHybrisConstraintViolation
{
	private static final String IN_LANGUAGE = "de.hybris.platform.validation.violation.inLanguage";
	private static final String FALLBACK_LANGUAGE_ISO_CODE = "en";
	private static final String DOT_CHARACTER = ".";
	private static final int STRING_START_INDEX = 0;
	private static final int CHARACTERS_TO_REMOVE = 1;

	private Locale locale;
	private CommonI18NService commonI18NService;

	@Override
	public String getLocalizedMessage()
	{
		String violationMsg = super.getLocalizedMessage();
		if (this.locale == null)
		{
			return violationMsg;
		}
		else
		{
			violationMsg = removeDotFromViolationMessage(violationMsg);
			Locale locale = this.i18nService.getCurrentLocale();
			String inLanguageMsg = this.bundleProvider.getResourceBundle(locale).getString(IN_LANGUAGE);
			LanguageModel language = commonI18NService.getLanguage(this.locale.toString());
			String displayLanguage =  language != null ? language.getIsocode() : FALLBACK_LANGUAGE_ISO_CODE;
			return violationMsg + " " + inLanguageMsg + ": " + displayLanguage + ".";
		}
	}

	private String removeDotFromViolationMessage(final String violationMsg)
	{
		return violationMsg.endsWith(DOT_CHARACTER) ?
				violationMsg.substring(STRING_START_INDEX, violationMsg.length() - CHARACTERS_TO_REMOVE) :
				violationMsg;
	}

	public void setViolationLanguage(final Locale locale)
	{
		this.locale = locale;
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
