package com.wiley.core.wileyb2b.unit;

import de.hybris.platform.b2b.model.B2BUnitModel;


public interface WileyB2BUnitService
{
	B2BUnitModel getB2BUnitForSapAccountNumber(String sapAccountNumber);

	void handleB2BUnitActiveStatusUpdated(String sapAccountNumber);
}
