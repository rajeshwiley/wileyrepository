package com.wiley.core.strategies.robotsmetatag;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;


public interface RobotsMetatagResolverService
{
	String resolveByPageType(AbstractPageModel page);

	boolean needResolveByPageType(AbstractPageModel page);

	boolean needResolveByPageUid(AbstractPageModel page);

	String resolveByPageUid(String code);
}
