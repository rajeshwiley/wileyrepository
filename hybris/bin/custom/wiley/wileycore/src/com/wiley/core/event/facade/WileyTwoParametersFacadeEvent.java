package com.wiley.core.event.facade;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;


/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class WileyTwoParametersFacadeEvent<PARAM1 extends Serializable, PARAM2 extends Serializable, RESULT extends Serializable>
        extends AbstractEvent
{
    private PARAM1 firstParameter;
    private PARAM2 secondParameter;
    private RESULT result;

    public WileyTwoParametersFacadeEvent(final PARAM1 firstParameter, final PARAM2 secondParameter) {
        this.firstParameter = firstParameter;
        this.secondParameter = secondParameter;
    }

    public PARAM1 getFirstParameter() {
        return firstParameter;
    }

    public void setFirstParameter(final PARAM1 firstParameter) {
        this.firstParameter = firstParameter;
    }

    public PARAM2 getSecondParameter() {
        return secondParameter;
    }

    public void setSecondParameter(final PARAM2 secondParameter) {
        this.secondParameter = secondParameter;
    }

    public RESULT getResult() {
        return result;
    }

    public void setResult(final RESULT result) {
        this.result = result;
    }
}
