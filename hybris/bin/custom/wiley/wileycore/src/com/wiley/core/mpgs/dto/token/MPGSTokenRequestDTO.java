package com.wiley.core.mpgs.dto.token;

import com.wiley.core.mpgs.dto.json.Session;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;



public class MPGSTokenRequestDTO
{
	private SourceOfFunds sourceOfFunds;
	private Session session;

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public Session getSession()
	{
		return session;
	}

	public void setSession(final Session session)
	{
		this.session = session;
	}
}
