package com.wiley.core.ordercancel;


import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.impl.executors.ImmediateCancelRequestExecutor;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;


import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.ordercancel.PayPalCancelRequestExecutor;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.wel.preorder.service.WelPreOrderService;


public class WelCancelRequestExecutor extends ImmediateCancelRequestExecutor
{
	private static final String WEL_ORDER_PROCESS_DEFINITION_NAME = "wel-order-process";

	@Resource(name = "paypalCancelRequestExecutor")
	PayPalCancelRequestExecutor paypalCancelRequestExecutor;

	@Resource
	private PaymentTransactionService paymentTransactionService;

	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Autowired
	private BusinessProcessService businessProcessService;

	@Autowired
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;

	@Autowired
	private WelPreOrderService welPreOrderService;

	@Override
	public void processCancelRequest(final OrderCancelRequest orderCancelRequest,
			final OrderCancelRecordEntryModel cancelRequestRecordEntry) throws OrderCancelException
	{
		ServicesUtil.validateParameterNotNull(orderCancelRequest, "[orderCancelRequest] can't be null");
		ServicesUtil.validateParameterNotNull(cancelRequestRecordEntry, "[cancelRequestRecordEntry] can't be null");
		OrderModel order = orderCancelRequest.getOrder();
		PaymentTransactionModel paymentTransaction = paymentTransactionService.getLastPaymentTransaction(order);
		if (PaypalConstants.PAYMENT_PROVIDER_NAME.equalsIgnoreCase(paymentTransaction.getPaymentProvider()))
		{
			paypalCancelRequestExecutor.processCancelRequest(orderCancelRequest, cancelRequestRecordEntry);
			return;
		}
		boolean isMPGSOrder = wileyMPGSPaymentProviderService.isMPGSProviderGroup(paymentTransaction.getPaymentProvider());
		if (isMPGSOrder && welPreOrderService.isPreOrder(order))
		{
			stopFulfilmentProcess(order);
		}
		super.processCancelRequest(orderCancelRequest, cancelRequestRecordEntry);
	}

	private void stopFulfilmentProcess(final OrderModel order)
	{
		ServicesUtil.validateParameterNotNull(order, "[order] can't be null");
		order.getOrderProcess().stream().forEach(op ->
		{
			if (op.getProcessDefinitionName().equals(WEL_ORDER_PROCESS_DEFINITION_NAME))
			{
				final BusinessProcessEvent event = BusinessProcessEvent
						.builder(op.getCode() + "_waitForProductActivationOrCancel").withChoice("CANCELLED").build();
				businessProcessService.triggerEvent(event);
			}
		});
	}
}
