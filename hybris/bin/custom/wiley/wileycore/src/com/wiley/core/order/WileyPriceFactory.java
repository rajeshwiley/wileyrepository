package com.wiley.core.order;

import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.util.DiscountValue;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.order.data.ProductDiscountParameter;


/**
 * Contains methods which are specific for Wiley.
 */
public interface WileyPriceFactory
{
	/**
	 * Finds product discount according to method parameters.
	 *
	 * @param productDiscountParameter
	 * 		required params:<br/>
	 * 		<ul>
	 * 		<li><b>product</b></li>
	 * 		<li><b>userDiscountGroup</b></li>
	 * 		<li><b>user</b></li>
	 * 		<li><b>currency</b></li>
	 * 		<li><b>date</b></li>
	 * 		<li><b>additional filter</b> can be used to perform additional filtering of DiscountRows.</li>
	 * 		</ul>
	 * @return list of discounts.
	 * @throws JaloPriceFactoryException
	 * 		if some exception occurred while searching product discounts.
	 */
	@Nonnull
	List<DiscountValue> getProductDiscountValues(@Nonnull ProductDiscountParameter productDiscountParameter)
			throws JaloPriceFactoryException;

}
