package com.wiley.core.externaltax;

import de.hybris.platform.externaltax.ExternalTaxDocument;
import de.hybris.platform.util.TaxValue;

import java.util.List;


/**
 * {@link ExternalTaxDocument} extended in order to contains information about status of tax calculation operation for each order
 * item
 */
public class WileyExternalTaxDocument extends ExternalTaxDocument
{
	private List<TaxValue> handlingCostTaxes;

	public void setHandlingCostTaxes(final List<TaxValue> handlingCostTaxes)
	{
		this.handlingCostTaxes = handlingCostTaxes;
	}

	public List<TaxValue> getHandlingCostTaxes()
	{
		return handlingCostTaxes;
	}
}
