package com.wiley.core.cms.service.impl;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.cms.dao.WileyCMSComponentDAO;
import com.wiley.core.cms.service.WileyCMSService;
import com.wiley.core.pages.dao.WileyCMSPageDao;



public class WileyCMSServiceImpl implements WileyCMSService
{

	@Resource
	private WileyCMSComponentDAO wileyCMSComponentDAO;

	@Resource
	private WileyCMSPageDao wileyCMSPageDao;

	@Override
	public List<AbstractCMSComponentModel> findNotSyncComponentsForPage(final AbstractPageModel pageModel)
	{
		return wileyCMSComponentDAO.findNotSyncComponentsForPage(pageModel);
	}

	@Override
	public List<AbstractPageModel> findPagesByComponent(final AbstractCMSComponentModel componentModel)
	{
		return wileyCMSPageDao.findPagesByComponent(componentModel);
	}
}