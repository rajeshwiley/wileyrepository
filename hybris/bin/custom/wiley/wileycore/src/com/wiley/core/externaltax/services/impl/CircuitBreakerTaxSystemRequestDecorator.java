package com.wiley.core.externaltax.services.impl;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.externaltax.services.TaxSystemRequestCommand;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;
import com.wiley.core.wiley.circuitbreaker.helper.PropertiesSetterHelper;


/**
 * Decorates request to external tax system by circuit breaker which monitors for failures.
 * Once the failures reach a certain threshold, the circuit breaker trips, and all further calls to the circuit breaker return
 * with an error, without the protected call being made at all
 */
public class CircuitBreakerTaxSystemRequestDecorator implements TaxSystemRequestCommand
{
	private static final String HYSTRIX_COMMAND_KEY_EXTERNAL_TAX_REST_CALL = "taxRestCall";

	private PropertiesSetterHelper propertiesSetterHelper;

	private TaxSystemRequestCommand taxSystemRequestCommand;

	private HystrixCommandProperties.Setter propertiesSetter;

	/**
	 * Set properties for @HystrixCommand
	 * All other properties are left default. Please find values here https://github.com/Netflix/Hystrix/wiki/Configuration
	 */
	@PostConstruct
	public void postConstruct()
	{
		configureCircuitBreaker();
	}

	@Override
	@Nonnull
	public Double execute(@Nonnull final TaxServiceRequest taxServiceRequest, @Nonnull final String orderCode,
			final int entryNumber)
	{
		final HystrixCommand.Setter commandSetter = HystrixCommand.Setter.withGroupKey(
				HystrixCommandGroupKey.Factory.asKey(WileyCoreConstants.HYSTRIX_TAX_SYSTEM_GROUP))
				.andCommandKey(HystrixCommandKey.Factory.asKey(HYSTRIX_COMMAND_KEY_EXTERNAL_TAX_REST_CALL))
				.andCommandPropertiesDefaults(propertiesSetter);

		HystrixCommand<Double> command = new HystrixCommand<Double>(commandSetter)
		{
			@Override
			protected Double run() throws Exception
			{
				Double result;
				try
				{
					result = taxSystemRequestCommand.execute(taxServiceRequest, orderCode, entryNumber);

				}
				catch (TaxSystemResponseError e)
				{
					// All exceptions thrown from the run() method except for HystrixBadRequestException count as failures and
					// trigger getFallback() and circuit-breaker logic.
					throw new HystrixBadRequestException(e.getMessage(), e);
				}
				return result;
			}
		};

		Double result;
		try
		{
			result = command.execute();
		}
		catch (HystrixBadRequestException e)
		{
			// You can wrap the exception that you would like to throw in HystrixBadRequestException and retrieve it via
			// getCause(). The HystrixBadRequestException is intended for use cases such as reporting illegal arguments or
			// non-system failures that should not count against the failure metrics and should not trigger fallback logic.
			throw new TaxSystemException(e.getCause());
		}
		catch (HystrixRuntimeException e)
		{
			if (e.getCause() instanceof TaxSystemException)
			{
				throw (TaxSystemException) e.getCause();
			}
			else
			{
				throw new TaxSystemException(String.format("The external tax system is unreachable for order [%1$s] entry [%2$s]",
						orderCode, entryNumber), e);
			}
		}

		Assert.notNull(result);

		return result;
	}

	private void configureCircuitBreaker()
	{
		propertiesSetter = HystrixCommandProperties.defaultSetter();
		propertiesSetterHelper.configureCircuitBreakerCommand(HYSTRIX_COMMAND_KEY_EXTERNAL_TAX_REST_CALL, propertiesSetter);
	}

	@Required
	public void setPropertiesSetterHelper(final PropertiesSetterHelper propertiesSetterHelper)
	{
		this.propertiesSetterHelper = propertiesSetterHelper;
	}

	public void setTaxSystemRequestCommand(final TaxSystemRequestCommand taxSystemRequestCommand)
	{
		this.taxSystemRequestCommand = taxSystemRequestCommand;
	}
}
