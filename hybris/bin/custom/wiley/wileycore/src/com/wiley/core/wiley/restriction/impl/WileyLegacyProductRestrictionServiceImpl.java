package com.wiley.core.wiley.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 * @author Dzmitryi_Halahayeu
 * 
 * This Class is deprecated and will be removed later in PH4, Please use WileyProductRestrictionServiceImpl instead.
 * 
 */
public class WileyLegacyProductRestrictionServiceImpl implements WileyProductRestrictionService
{
	private List<WileyRestrictProductStrategy> availabilityStrategies;
	private List<WileyRestrictProductStrategy> visibilityStrategies;
	private List<WileyRestrictProductStrategy> searchabilityStrategies;

	@Nonnull
	@Override
	public WileyRestrictionCheckResultDto isAvailable(@Nonnull final CommerceCartParameter parameter)
	{
		for (final WileyRestrictProductStrategy strategy : availabilityStrategies)
		{
			if (strategy.isRestricted(parameter))
			{
				return strategy.createErrorResult(parameter);
			}
		}
		return WileyRestrictionCheckResultDto.successfulResult();
	}

	@Nonnull
	@Override
	public WileyRestrictionCheckResultDto isVisible(@Nonnull final CommerceCartParameter parameter)
	{
		for (WileyRestrictProductStrategy strategy : visibilityStrategies)
		{
			if (strategy.isRestricted(parameter))
			{
				return strategy.createErrorResult(parameter);
			}
		}
		return WileyRestrictionCheckResultDto.successfulResult();
	}

	@Nonnull
	@Override
	public boolean isSearchable(@Nonnull final ProductModel productModel)
	{
		Assert.notNull(productModel);
		for (final WileyRestrictProductStrategy strategy : searchabilityStrategies)
		{
			if (strategy.isRestricted(productModel))
			{
				return false;
			}
		}
		return true;
	}

	@Nonnull
	@Override
	public boolean isAvailable(@Nonnull final ProductModel product)
	{
		for (final WileyRestrictProductStrategy strategy : availabilityStrategies)
		{
			if (strategy.isRestricted(product))
			{
				return false;
			}
		}
		return true;
	}

	@Nonnull
	@Override
	public boolean isVisible(@Nonnull final ProductModel product)
	{
		for (WileyRestrictProductStrategy strategy : visibilityStrategies)
		{
			if (strategy.isRestricted(product))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Pay attention product must not be saved after variant filtering
	 *
	 * @param product
	 * @return product with filtered variants
	 */
	@Override
	public ProductModel filterRestrictedProductVariants(@Nonnull final ProductModel product)
	{
		List<VariantProductModel> visibleVariants = product.getVariants().stream()
				.filter(variant -> isVisible(variant)).collect(Collectors.toList());
		product.setVariants(visibleVariants);
		return product;
	}


	@Required
	public void setAvailabilityStrategies(
			final List<WileyRestrictProductStrategy> availabilityStrategies)
	{
		this.availabilityStrategies = availabilityStrategies;
	}

	@Required
	public void setVisibilityStrategies(final List<WileyRestrictProductStrategy> visibilityStrategies)
	{
		this.visibilityStrategies = visibilityStrategies;
	}

	public void setSearchabilityStrategies(
			final List<WileyRestrictProductStrategy> searchabilityStrategies)
	{
		this.searchabilityStrategies = searchabilityStrategies;
	}
}
