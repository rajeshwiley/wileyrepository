package com.wiley.core.wiley.classification;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;

import java.util.List;
import java.util.Set;

import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;


/**
 * Created by Uladzimir_Barouski on 12/29/2016.
 */
public interface WileyClassificationService
{
	/**
	 * Filters product features classification attributes assignment
	 *
	 * @param products
	 * @return set of attributes that are set to yes.
	 */
	Set<ClassAttributeAssignmentModel> getFilteredAssignmentForProducts(List<WileyVariantProductModel> products);

	/**
	 * Creates CMSProductAttributes base on filtered assignments set and current CMSProductAttributes set assigned
	 * to component.
	 *
	 * @param assignments
	 * @param variantProductComparisonTableComponentModel
	 */
	void mergeAssigmentsWithCMSProductAttributes(Set<ClassAttributeAssignmentModel> assignments,
			VariantProductComparisonTableComponentModel variantProductComparisonTableComponentModel);

	/**
	 * Check if feature is visible
	 *
	 * @param feature
	 * @return
	 */
	boolean isFeatureVisible(ProductFeatureModel feature);
}
