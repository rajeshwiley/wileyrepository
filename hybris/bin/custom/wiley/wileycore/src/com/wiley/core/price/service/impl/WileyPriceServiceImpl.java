package com.wiley.core.price.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.price.dao.WileyPriceDao;
import com.wiley.core.price.service.WileyPriceService;
import com.wiley.core.price.strategy.EffectiveDatesPriceReplacementStrategy;
import com.wiley.core.product.WileyProductService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.util.Collections.singletonMap;


public class WileyPriceServiceImpl implements WileyPriceService
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyPriceServiceImpl.class);

	private WileyPriceDao wileyPriceDao;

	private WileyProductService wileyProductService;

	private ImpersonationService impersonationService;

	private UserService userService;

	private EffectiveDatesPriceReplacementStrategy effectiveDatesPriceReplacementStrategy;

	private CatalogVersionService catalogVersionService;

	@Override
	public PriceRowModel getPriceRowForPriceId(final String priceId)
	{
		LOG.debug("Fetching the PriceRowModel for price id : {} " + priceId);
		final List<PriceRowModel> priceRowModels = wileyPriceDao.find(singletonMap(PriceRowModel.CODE, priceId));
		validateIfSingleResult(priceRowModels, PriceRowModel.class, PriceRowModel.CODE, priceId);
		return priceRowModels.get(0);
	}

	@Override
	public boolean isPriceRowAlreadyExist(final String priceId)
	{
		LOG.debug("Checking the PriceRowModel for price id : " + priceId);
		final List<PriceRowModel> priceRowModels = wileyPriceDao.find(singletonMap(PriceRowModel.CODE, priceId));
		return CollectionUtils.isNotEmpty(priceRowModels);
	}

	@Override
	public List<PriceRowModel> getPricesForProduct(final ProductModel productModel, final CurrencyModel currencyModel)
	{
		return wileyPriceDao.findPricesByProduct(productModel, currencyModel);
	}

	@Override
	@Transactional
	public void savePriceUpdate(final PriceRowModel price)
	{
		//Hybris fetches all the price records, which have the same values of productId, currency, country, and minQuantity
		// as the inbound message.
		List<PriceRowModel> prices = getPrices(price);

		effectiveDatesPriceReplacementStrategy.replacePrices(prices, price);

		executeAsAnonymousUser(() -> {
			wileyProductService.updateProductsModifiedTime(price.getProductId());
			return null;
		});
	}

	private List<PriceRowModel> getPrices(final PriceRowModel price)
	{
		return executeAsAnonymousUser(() -> {
			return wileyPriceDao.findPrices(price);
		});
	}

	private <R, T extends Throwable> R executeAsAnonymousUser(final ImpersonationService.Executor<R, T> wrapper) throws T
	{
		ImpersonationContext context = new ImpersonationContext();
		context.setUser(userService.getAnonymousUser());
		context.setCatalogVersions(catalogVersionService.getAllCatalogVersions());
		return impersonationService.executeInContext(context, wrapper);

	}

	public void setWileyPriceDao(final WileyPriceDao wileyPriceDao)
	{
		this.wileyPriceDao = wileyPriceDao;
	}

	public void setWileyProductService(final WileyProductService wileyProductService)
	{
		this.wileyProductService = wileyProductService;
	}

	public void setImpersonationService(final ImpersonationService impersonationService)
	{
		this.impersonationService = impersonationService;
	}

	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public void setEffectiveDatesPriceReplacementStrategy(
			final EffectiveDatesPriceReplacementStrategy effectiveDatesPriceReplacementStrategy)
	{
		this.effectiveDatesPriceReplacementStrategy = effectiveDatesPriceReplacementStrategy;
	}

	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
