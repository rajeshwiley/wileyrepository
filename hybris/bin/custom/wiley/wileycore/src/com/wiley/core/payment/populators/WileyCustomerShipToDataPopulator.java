package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.CustomerShipToDataPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;




public class WileyCustomerShipToDataPopulator extends CustomerShipToDataPopulator
{
	@Override
	public void populate(final CartModel source, final CustomerShipToData target) throws ConversionException
	{
		super.populate(source, target);
		final AddressModel deliveryAddress = source.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			final CountryModel countryModel = deliveryAddress.getCountry();
			if (countryModel != null)
			{
				target.setCountryNumericIsocode(countryModel.getNumeric());
				target.setShipToCountryName(countryModel.getName());
			}

			if (deliveryAddress.getRegion() != null)
			{
				target.setShipToStateName(deliveryAddress.getRegion().getName());
			}

			if (deliveryAddress.getTitle() != null)
			{
				target.setShipToTitleName(deliveryAddress.getTitle().getName());
			}

		}

	}
}
