package com.wiley.core.integration.esb.service.impl;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.integration.esb.service.Wileyb2bCartEntryService;


/**
 * Default implementation of {@link Wileyb2bCartEntryService}
 */
public class Wileyb2bCartEntryServiceImpl implements Wileyb2bCartEntryService
{
	@Nonnull
	@Override
	public Optional<CartEntryModel> findCartEntryByIsbn(@Nonnull final CartModel cart, @Nonnull final String isbn)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);
		ServicesUtil.validateParameterNotNullStandardMessage("isbn", isbn);

		return cart.getEntries().stream()
				.filter(entry -> isbn.equals(entry.getProduct().getIsbn()))
				.map(entry -> (CartEntryModel) entry)
				.findFirst();
	}
}
