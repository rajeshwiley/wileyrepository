package com.wiley.core.product.attributehandler;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import com.wiley.core.enums.SubscriptionTermFrequencyValue;


/**
 * Calculates Subscription Term value
 */
public class SubscriptionTermFrequencyValueAttributeHandler extends
		AbstractDynamicAttributeHandler<SubscriptionTermFrequencyValue, SubscriptionTermModel>
{
	@Override
	public SubscriptionTermFrequencyValue get(final SubscriptionTermModel subscriptionTerm)
	{
		if (subscriptionTerm == null || subscriptionTerm.getTermOfServiceNumber() == null) {
			return SubscriptionTermFrequencyValue.UNKNOWN;
		}

		switch (subscriptionTerm.getTermOfServiceFrequency()) {
			case MONTHLY: return handleMonthlyFrequency(subscriptionTerm);
			case ANNUALLY: return handleAnnuallyFrequency(subscriptionTerm);
			default: return SubscriptionTermFrequencyValue.UNKNOWN;
		}
	}

	private SubscriptionTermFrequencyValue handleAnnuallyFrequency(final SubscriptionTermModel subscriptionTerm) {
		int subscriptionNumber = subscriptionTerm.getTermOfServiceNumber();
		return subscriptionNumber > 0 ? SubscriptionTermFrequencyValue.YEARLY : SubscriptionTermFrequencyValue.UNKNOWN;
	}

	private SubscriptionTermFrequencyValue handleMonthlyFrequency(final SubscriptionTermModel subscriptionTerm) {
		int subscriptionNumber = subscriptionTerm.getTermOfServiceNumber();
		if (subscriptionNumber > 0 && subscriptionNumber < 12) {
			return SubscriptionTermFrequencyValue.MONTHLY;
		} else if (subscriptionNumber >= 12) {
			return SubscriptionTermFrequencyValue.YEARLY;
		}
		return SubscriptionTermFrequencyValue.UNKNOWN;
	}
}
