package com.wiley.core.mpgs.services.impl;


import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;


import org.apache.commons.lang.StringUtils;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;



public class WileyMPGSMessageServiceImpl implements WileyMPGSMessageService
{

	private static final String MESSAGE_KEY_ISSUE_AUTHORIZATION = "mpgs.checkout.result.authorize.";

	public static final String MESSAGE_KEY_HYBRIS_EXCEPTION = "mpgs.checkout.result.hybris_exception";

	public String getStorefrontMessageKey(@Nonnull final PaymentTransactionEntryModel paymentEntry)
	{
		ServicesUtil.validateParameterNotNull(paymentEntry, "[paymentEntry] can't be null.");

		String transactionStatus = paymentEntry.getTransactionStatus();
		if (WileyMPGSConstants.HYBRIS_EXCEPTION.equalsIgnoreCase(transactionStatus))
		{
			return MESSAGE_KEY_HYBRIS_EXCEPTION;
		}
		return StringUtils.EMPTY;

	}

	@Override
	public String getAuthorizationStorefrontMessageKey(final PaymentTransactionEntryModel paymentEntry)
	{
		ServicesUtil.validateParameterNotNull(paymentEntry, "[paymentEntry] can't be null.");

		StringBuilder authorizationStorefrontMessageKey = new StringBuilder();
		String transactionStatus = paymentEntry.getTransactionStatus();
		if (WileyMPGSConstants.HYBRIS_EXCEPTION.equalsIgnoreCase(transactionStatus)) {
			return MESSAGE_KEY_HYBRIS_EXCEPTION;
		} else if (WileyMPGSConstants.RETURN_STATUS_ERROR.equalsIgnoreCase(transactionStatus)) {
			String errorMessageKey = MESSAGE_KEY_ISSUE_AUTHORIZATION + WileyMPGSConstants.RETURN_STATUS_ERROR;
			return errorMessageKey.toLowerCase();
     	} else {
			String messageKey = MESSAGE_KEY_ISSUE_AUTHORIZATION + paymentEntry
					.getTransactionStatusDetails();
			return messageKey.toLowerCase();
		}

	}
}
