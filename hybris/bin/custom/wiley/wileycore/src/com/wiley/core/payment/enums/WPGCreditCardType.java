package com.wiley.core.payment.enums;

import java.util.HashMap;
import java.util.Map;


public enum WPGCreditCardType
{
	VISA("V"),
	MASTER("M"),
	AMEX("A"),
	DISCOVER("R"),
	DINERS("D");

	private final String stringValue;

	private static final Map<String, WPGCreditCardType> LOOKUP = new HashMap<>();

	static
	{
		for (final WPGCreditCardType cardType : WPGCreditCardType.values())
		{
			LOOKUP.put(cardType.getStringValue(), cardType);
		}
	}

	WPGCreditCardType(final String stringValue)
	{
		this.stringValue = stringValue;
	}

	public String getStringValue()
	{
		return this.stringValue;
	}

	public static WPGCreditCardType get(final String stringValue)
	{
		return LOOKUP.get(stringValue);
	}
}
