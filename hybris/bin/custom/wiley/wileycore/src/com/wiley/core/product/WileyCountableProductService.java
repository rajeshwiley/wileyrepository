package com.wiley.core.product;

import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;


/**
 * Service for recognizing if product countable or uncountable
 *
 * @author Dzmitryi_Halahayeu
 */
public interface WileyCountableProductService
{

	/**
	 * Checks if product can have quantity
	 *
	 * @param product
	 *          the product
	 * @return true if product can have quantity else false
	 */
	boolean canProductHaveQuantity(@Nonnull ProductModel product);
}
