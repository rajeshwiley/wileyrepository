package com.wiley.core.exceptions;

public class WileyCurrencyMismatchException extends RuntimeException
{
	public WileyCurrencyMismatchException(final String message)
	{
		super(message);
	}
}
