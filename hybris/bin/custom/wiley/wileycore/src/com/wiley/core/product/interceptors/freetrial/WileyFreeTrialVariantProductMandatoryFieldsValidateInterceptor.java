package com.wiley.core.product.interceptors.freetrial;

import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyFreeTrialVariantProductModel;


/**
 * Validate mandatory fields for {@link com.wiley.core.model.WileyFreeTrialVariantProductModel}
 */
public class WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptor
		extends AbstractWileyFreeTrialProductsValidatorInterceptor
		implements ValidateInterceptor<WileyFreeTrialVariantProductModel>
{
	private L10NService l10nService;

	@Override
	public void onValidate(final WileyFreeTrialVariantProductModel pModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{
		if (!validateCommonFields(pModel) || pModel.getBaseProduct() == null)
		{
			throw new InterceptorException(
					l10nService.getLocalizedString("error.wileytrialproduct.Required.fields.missing.message"));
		}
	}

	@Required
	public void setL10nService(final L10NService l10nService)
	{
		this.l10nService = l10nService;
	}
}
