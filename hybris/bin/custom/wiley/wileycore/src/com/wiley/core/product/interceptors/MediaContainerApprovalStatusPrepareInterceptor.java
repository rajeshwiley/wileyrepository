package com.wiley.core.product.interceptors;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.product.WileyProductService;


public class MediaContainerApprovalStatusPrepareInterceptor implements PrepareInterceptor<MediaContainerModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(MediaContainerApprovalStatusPrepareInterceptor.class);

	@Resource
	private UserService userService;
	@Resource
	private WileyProductService productService;

	@Resource
	private ModelService modelService;

	@Override
	public void onPrepare(final MediaContainerModel mediaContainerModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		try
		{
			final UserGroupModel productEditorGroup =
					userService.getUserGroupForUID(WileyCoreConstants.PRODUCTEDITORGROUP_UID);
			final UserModel currentUser = userService.getCurrentUser();
			if (interceptorContext == null || (
					interceptorContext.isModified(mediaContainerModel) && !interceptorContext.isNew(mediaContainerModel)))
			{
				final boolean memberOfGroup = userService.isMemberOfGroup(currentUser, productEditorGroup);
				final List<ProductModel> productsContainsMediaContainer = new ArrayList<>();

				productsContainsMediaContainer.addAll(
						productService.getProductsForMediaContainerInBackgroundImage(mediaContainerModel));
				productsContainsMediaContainer.addAll(
						productService.getProductsForMediaContainerInGalleryImages(mediaContainerModel));

				if (!productsContainsMediaContainer.isEmpty())
				{
					for (ProductModel productModel : productsContainsMediaContainer)
					{
						if (memberOfGroup)
						{
							productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
						}
						if (mediaContainerModel.equals(productModel.getBackgroundImage()))
						{
							productModel.setBackgroundImage(mediaContainerModel);
						}

						final List<MediaContainerModel> galleryImages = productModel.getGalleryImages();
						if (!CollectionUtils.isEmpty(galleryImages))
						{
							final List<MediaContainerModel> newGalleryImages = new ArrayList<>(galleryImages.size());
							for (MediaContainerModel galleryImage : galleryImages)
							{
								if (mediaContainerModel.equals(galleryImage))
								{
									newGalleryImages.add(mediaContainerModel);
								}
								else
								{
									newGalleryImages.add(galleryImage);
								}
							}
							productModel.setGalleryImages(newGalleryImages);
						}
					}

					modelService.saveAll(productsContainsMediaContainer);
				}
			}
		}
		catch (UnknownIdentifierException e)
		{
			LOG.debug("[{}] not found.", WileyCoreConstants.PRODUCTEDITORGROUP_UID, e);
		}
	}
}
