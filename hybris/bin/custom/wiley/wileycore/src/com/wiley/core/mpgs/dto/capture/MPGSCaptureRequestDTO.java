package com.wiley.core.mpgs.dto.capture;


import com.wiley.core.mpgs.dto.json.Transaction;


public class MPGSCaptureRequestDTO
{
	private String apiOperation;
	private Transaction transaction;

	public String getApiOperation()
	{
		return apiOperation;
	}

	public void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}



}
