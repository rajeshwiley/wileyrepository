package com.wiley.core.wileyb2c.customer;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;


public interface WileyB2CCustomerAccountService extends WileycomCustomerAccountService
{
	void convertGuestToCustomer(String pwd, String orderGUID, boolean acceptPromotions) throws DuplicateUidException;
}
