package com.wiley.core.mpgs.request;




public class WileyTokenizationRequest
{
	private String urlPath;
	private String sessionId;
	private String sourceOfFundsType;
	private String paymentProvider;

	public WileyTokenizationRequest(final String url, final String sessionId, final String paymentProvider)
	{
		this.urlPath = url;
		this.sessionId = sessionId;
		this.paymentProvider = paymentProvider;
	}

	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public String getSourceOfFundsType()
	{
		return sourceOfFundsType;
	}

	public void setSourceOfFundsType(final String sourceOfFundsType)
	{
		this.sourceOfFundsType = sourceOfFundsType;
	}

}
