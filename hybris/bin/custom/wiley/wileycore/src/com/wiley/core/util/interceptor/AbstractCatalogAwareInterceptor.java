package com.wiley.core.util.interceptor;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Set;


/**
 * Contains methods for validation mandatory attributes.
 */
public class AbstractCatalogAwareInterceptor
{
	private Set<String> catalogIds;
	private String catalogVersionStage;

	protected boolean isCatalogAllowed(final CatalogModel catalog)
	{
		return catalogIds.contains(catalog.getId());
	}

	protected boolean isVersionAllowed(final String catalogVersion)
	{
	    return catalogVersionStage.equalsIgnoreCase(catalogVersion);
	}

	@Deprecated
	public boolean isProductWithRestriction(final ProductModel productModel)
	{
		CatalogModel catalog = productModel.getCatalogVersion().getCatalog();
		return isCatalogAllowed(catalog);
	}

	public void setCatalogIds(final Set<String> catalogIds)
	{
		this.catalogIds = catalogIds;
	}

	public void setCatalogVersionStage(final String catalogVersion)
	{
		this.catalogVersionStage = catalogVersion;
	}
}
