package com.wiley.core.externalcompany;

import com.wiley.core.model.ExternalCompanyModel;


/**
 * B2C specific service to search {@link ExternalCompanyModel}
 */
public interface WileycomExternalCompanyService
{
	/**
	 * Search ExternalCompany by externalId
	 *
	 * @param externalId
	 * 		{@link ExternalCompanyModel#EXTERNALID}
	 * @return Found ExternalCompanyModel item
	 */
	ExternalCompanyModel getExternalCompany(String externalId);
}
