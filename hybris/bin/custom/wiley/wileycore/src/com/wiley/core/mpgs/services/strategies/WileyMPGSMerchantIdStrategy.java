package com.wiley.core.mpgs.services.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.core.model.WileyTnsMerchantModel;


public interface WileyMPGSMerchantIdStrategy
{
	String getMerchantID(AbstractOrderModel abstractOrderModel);

	WileyTnsMerchantModel getMerchant(AbstractOrderModel abstractOrderModel);

	String getHostedSessionScriptUrl(String merchantId);
}
