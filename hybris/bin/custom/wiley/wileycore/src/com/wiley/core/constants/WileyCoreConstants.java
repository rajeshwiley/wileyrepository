package com.wiley.core.constants;

import java.util.regex.Pattern;


/**
 * Global class for all WileyCore constants. You can add global constants for your extension into this class.
 */
public final class WileyCoreConstants extends GeneratedWileyCoreConstants
{
	public static final String EXTENSIONNAME = "wileycore";
	public static final String UNIVERSITY_OTHER = "Other";
	public static final String HYBRIS_DATA_DIR_KEY = "HYBRIS_DATA_DIR";
	public static final String USA_COUNTRY_ISO_CODE = "US";
	public static final String CA_COUNTRY_ISO_CODE = "CA";
	public static final String JP_COUNTRY_ISO_CODE = "JP";
	public static final String CN_COUNTRY_ISO_CODE = "CN";
	public static final String DEFAULT_COUNTRY_ISO_CODE = USA_COUNTRY_ISO_CODE;
	public static final String WILEY_STOREFRONT_ID = "WILEY_STOREFRONT_ID";
	public static final String HYSTRIX_TAX_SYSTEM_GROUP = "hystrix_tax_system_group";
	public static final String PREFERRED_PARTNER_GROUP_UID = "preferredPartner";
	public static final String WGU_PARTNER_GROUP_UID = "wguPartner";
	public static final String KPMG_GROUP_UID = "kpmgPartner";
	public static final String SPECIAL_PARTNER_GROUP_UID = "specialPartner";
	public static final String CURRENT_STORE_SESSION_ATTRIBUTE = "currentStore";
	public static final String DISABLE_ASSORTMENTS_SESSION_ATTRIBUTE = "disableAssortmentCheck";
	public static final String PRE_ORDER_PLACEMENT_EMAIL_PROCESS = "preOrderPlacementEmailProcess";
	public static final String ORDER_HAS_BEEN_FAILED_EMAIL_PROCESS = "orderHasBeenFailedEmailProcess";
	public static final String PAYMENT_MPGS_HOSTED_SESSION_SCRIPT_URL_TEMPLATE = "payment.mpgs.hostedSessionScriptUrl.template";

	public static final String WEL_CFA_LEVEL_CATEGORY_CODE = "WEL_CFA_LEVEL";
	public static final String WEL_CFA_CATEGORY_CODE = "WEL_CFA_CATEGORY";
	public static final String WEL_GIFT_CARDS_CATEGORY_CODE = "WEL_GIFT_CARDS_CATEGORY";
	public static final String WEL_PARTNERS_CATEGORY_CODE = "WEL_PARTNERS_CATEGORY";
	public static final String WEL_SUPPLEMENTS_CATEGORY_REGEX = ".*_SUPPLEMENTS.*";
	public static final String WEL_FREE_TRIAL_CATEGORY_REGEX = ".*_FREE_TRIAL.*";
	public static final String WEL_CFA_LEVEL_VARIANT_CATEGORY_REGEX = ".*WEL_CFA_LEVEL.*";
	public static final String BASKET_VALIDATION_UNVAILABLE = "basket.validation.unavailable";
	public static final String PRODUCT_IS_NOT_PURCHASABLE = "basket.error.product.isNotPurchasable";
	public static final String PRODUCT_IS_NOT_PURCHASABLE_FOR_COUNTRY = "basket.error.product.isNotPurchasableFromThisCountry";
	public static final String DEFAULT_CATEGORY_CODE = "<enter category code>";

	public static final String STAGED_CATALOG_VERSION = "Staged";
	public static final String ONLINE_CATALOG_VERSION = "Online";

	public static final String DIGITAL_PRODUCT = "Digital";
	public static final String PHYSICAL_PRODUCT = "Physical";

	public static final String PRODUCTEDITORGROUP_UID = "producteditorgroup";

	//profile name also should be changed in [local.properties] -> [spring.profiles.active] property
	public static final String WILEY_PHASE2A_PROFILE_NAME = "wiley_phase2a";

	public static final String WILEY_ADD_TO_LEGACY_CART_ACTION_UID = "AddToLegacyCartAction";

	public static final String SLASH = "/";

	public static final String STUDENT_COUPON_CODE = "STUDENT";
	/**
	 * The constant WILEY_EBP_DATE_TIME_FORMAT_NAME.
	 */
	public static final String WILEY_EBP_DATE_TIME_FORMAT_NAME = "yyyy-MM-dd'T'HH:mm:ssZ";

	public static final String EMAIL_REGEX_STRING = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\\b";
	public static final Pattern EMAIL_REGEX = Pattern.compile(EMAIL_REGEX_STRING);

	public static final String CHARACTERS_REGEX_STRING = "^[\\u0020-\\u007F\\u00A0-\\u00FF]*$";
	public static final Pattern CHARACTERS_REGEX = Pattern.compile(CHARACTERS_REGEX_STRING);

	public static final int MIN_PASSWORD_LENGTH = 6;
	public static final int MAX_FIELD_LENGTH = 256;

	private WileyCoreConstants()
	{
		//empty
	}

	public static final int DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY = 1;

	public static final String WILEYB2CSTOREFRONT_DEFAULT_SITE_PROPERTY = "wileyb2cstorefront.default.site";

	public static final String WILEY_HOMEPAGE_URL = "wiley.homepage.url";

	/**
	 * EDI export code
	 */
	public static final String EDI_SYSTEM_CODE = "edi";

	public static final String ANALYTICS_SCRIPT_URL_CONFIG_KEY = "adobe.analytics.script.url";

	/**
	 * I18N constants
	 */
	public static final String CURRENT_COUNTRY_SESSION_ATTR = "sessionCountry";
	public static final String LEGACY_CART_REGION_AU = "au";
	public static final String LEGACY_CART_REGION_EU = "eu";
	public static final String TAX_SHORT_MESSAGE_CODE = "product.tax.<REGION_CODE>.short.message";
	public static final String TAX_TOOLTIP_CODE = "product.tax.<REGION_CODE>.tooltip";

	public static final String COUNTRY_US = "us";
	public static final String COUNTRY_DEFAULT = "default";
	public static final String PUBLICATION_DATE_FORMAT = "product.publication.date.format.<REGION_CODE>";

	public static final String TAB_CONTAINER_TABS = "TAB_CONTAINER_TABS";
	public static final String SOURCE_SYSTEM_HYBRIS = "hybris";
	public static final String ALM_AUTO_CREATION_STRATEGY = "ALM";
	public static final String AS_SITE_ID = "asSite";
	public static final String WILEY_COM_SITE_ID = "wileyb2c";

	public static final String DEFAULT_MERCHANT_ID_PROP = "payment.mpgs.merchantId.default";
	public static final String DEFAULT_MPGS_CARD_VERIFY_STRATEGY = "wileyCardVerifyStrategy";
	public static final String DEFAULT_MPGS_CARD_NO_VERIFY_STRATEGY = "wileyCardNoVerifyStrategy";

	public static final String WEL_GMAT_CATEGORY_CODE = "WEL_GMAT_CATEGORY";
}
