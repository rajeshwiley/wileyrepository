package com.wiley.core.wileycom.product.valuetranslator;

import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;

import com.wiley.core.wileycom.product.valuetranslator.adapter.WileycomProductRestrictionImportAdapter;
import com.wiley.core.wileycom.valuetranslator.AbstractDefaultWileycomSpecialValueTranslator;
import com.wiley.core.wileycom.valuetranslator.AbstractWileycomSpecialValueTranslator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Implementation of {@link AbstractWileycomSpecialValueTranslator} to add
 * {@link de.hybris.platform.voucher.model.ProductRestrictionModel} to
 * {@link de.hybris.platform.voucher.model.PromotionVoucherModel}. Import logic is delegated to
 * {@link WileycomProductRestrictionImportAdapter}
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileycomProductRestrictionTranslator
		extends AbstractDefaultWileycomSpecialValueTranslator
{
	private static final String DEFAULT_IMPORT_ADAPTER_NAME = "wileycomProductRestrictionImportAdapter";

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		super.init(columnDescriptor);
		final String catalogId = columnDescriptor.getDescriptorData().getModifier("catalogId");
		final String catalogVersion = columnDescriptor.getDescriptorData().getModifier("catalogVersion");
		validateParameterNotNull(catalogId, "CatalogId must not be null.");
		validateParameterNotNull(catalogVersion, "CatalogVersion must not be null.");
		getWileycomImportAdapter().setCatalog(catalogVersion, catalogId);
	}

	@Override
	protected String getDefaultImportAdapterName()
	{
		return DEFAULT_IMPORT_ADAPTER_NAME;
	}


}
