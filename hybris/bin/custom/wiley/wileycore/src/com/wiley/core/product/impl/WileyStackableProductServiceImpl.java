package com.wiley.core.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.product.WileyStackableProductService;


public class WileyStackableProductServiceImpl implements WileyStackableProductService
{
	@Override
	public boolean hasStackablePrice(@Nonnull final ProductModel product)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);

		return Boolean.TRUE.equals(product.getStackablePrice());
	}
}
