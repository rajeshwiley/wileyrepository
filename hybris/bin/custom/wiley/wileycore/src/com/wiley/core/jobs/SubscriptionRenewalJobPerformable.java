package com.wiley.core.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.events.SubscriptionRenewalEvent;
import com.wiley.core.subscription.services.WileySubscriptionService;


/**
 * This job gets all subscriptions which should be renewed and publishes {@link SubscriptionRenewalEvent} for each subscription.
 *
 * @author Aliaksei_Zlobich
 */
public class SubscriptionRenewalJobPerformable extends AbstractJobPerformable<CronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(SubscriptionRenewalJobPerformable.class);

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Resource
	private EventService eventService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		LOG.info("Starting job for cron job [{}].", cronJob.getCode());

		// calling service to get subscriptions for publishing event.
		final List<WileySubscriptionModel> subscriptionsToRenew =
				wileySubscriptionService.getAllSubscriptionsWhichShouldBeRenewed();

		if (LOG.isDebugEnabled() && CollectionUtils.isEmpty(subscriptionsToRenew))
		{
			LOG.debug("There are no subscriptions for renewing.");
		}

		// publishing SubscriptionRenewalEvent for each subscription.
		for (WileySubscriptionModel subscription : subscriptionsToRenew)
		{
			LOG.debug("Publishing SubscriptionRenewalEvent for willey subscription [{}].", subscription.getCode());

			eventService.publishEvent(createSubscriptionRenewalEvent(subscription));
		}

		LOG.info("Completed job for cron job [{}].", cronJob.getCode());
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private SubscriptionRenewalEvent createSubscriptionRenewalEvent(final WileySubscriptionModel subscription)
	{
		final SubscriptionRenewalEvent event = new SubscriptionRenewalEvent();
		event.setSubscription(subscription);
		return event;
	}

	public void setWileySubscriptionService(final WileySubscriptionService wileySubscriptionService)
	{
		this.wileySubscriptionService = wileySubscriptionService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
