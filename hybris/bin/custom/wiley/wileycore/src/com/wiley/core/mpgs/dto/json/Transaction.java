package com.wiley.core.mpgs.dto.json;


import java.math.BigDecimal;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction
{
	private BigDecimal amount;
	private String currency;
	private String id;
	private String reference;
	private String targetTransactionId;

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(final BigDecimal amount)
	{
		this.amount = amount;
	}

	public String getReference()
	{
		return reference;
	}

	public void setReference(final String reference)
	{
		this.reference = reference;
	}

	public String getTargetTransactionId()
	{
		return targetTransactionId;
	}

	public void setTargetTransactionId(final String targetTransactionId)
	{
		this.targetTransactionId = targetTransactionId;
	}
}
