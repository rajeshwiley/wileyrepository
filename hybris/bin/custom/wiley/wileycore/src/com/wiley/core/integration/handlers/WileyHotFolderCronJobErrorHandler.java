package com.wiley.core.integration.handlers;

import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.impex.jalo.ErrorHandler;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.ImpExReader;
import de.hybris.platform.jalo.enumeration.EnumerationValue;

import org.apache.log4j.Logger;


/**
 * Created by Uladzimir_Barouski on 10/13/2016.
 */
public class WileyHotFolderCronJobErrorHandler implements ErrorHandler
{
	private static final Logger LOG = Logger.getLogger(WileyHotFolderCronJobErrorHandler.class);
	private final CronJob cronjob;

	public WileyHotFolderCronJobErrorHandler(final CronJob cronjob) {
		this.cronjob = cronjob;
	}

	@Override
	public RESULT handleError(final ImpExException impexException, final ImpExReader impExReader)
	{
		EnumerationValue errorMode = this.cronjob.getErrorMode();
		if (errorMode != null && GeneratedCronJobConstants.Enumerations.ErrorMode.PAUSE.equals(errorMode.getCode())) {
			LOG.warn("Pause error mode is not supported, will fail");
			return RESULT.FAIL;
		} else if (errorMode != null && GeneratedCronJobConstants.Enumerations.ErrorMode.IGNORE.equals(errorMode.getCode())) {
			LOG.error("Exception occurred, will ignore: " + impexException);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Import Full Trace: ", impexException);
			}
			return RESULT.IGNORE;
		} else {
			return RESULT.FAIL;
		}
	}
}
