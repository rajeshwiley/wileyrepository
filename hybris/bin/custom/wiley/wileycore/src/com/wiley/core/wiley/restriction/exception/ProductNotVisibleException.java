package com.wiley.core.wiley.restriction.exception;

/**
 * @author Dzmitryi_Halahayeu
 */
public class ProductNotVisibleException extends RuntimeException
{
	private final String errorMessageCode;
	// Disabling Sonar to turn off the Serializable fields check (the same approach is used in the OOTB class GlobalMessage)
	private final Object[] errorMessageParameters; //NOSONAR

	public ProductNotVisibleException(final String message, final String errorMessageCode, final Object[] errorMessageParameters)
	{
		super(message);
		this.errorMessageCode = errorMessageCode;
		this.errorMessageParameters = errorMessageParameters;
	}

	public String getErrorMessageCode()
	{
		return errorMessageCode;
	}

	public Object[] getErrorMessageParameters()
	{
		return errorMessageParameters;
	}
}
