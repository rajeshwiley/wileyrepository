package com.wiley.core.integration.alm.user.service.impl;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.alm.user.AlmUserGateway;
import com.wiley.core.integration.alm.user.dto.UserDto;
import com.wiley.core.integration.alm.user.service.AlmUserService;


public class AlmUserServiceImpl implements AlmUserService
{
	private static final Logger LOG = LoggerFactory.getLogger(AlmUserServiceImpl.class);

	@Autowired
	private AlmUserGateway almUserGateway;

	@Nonnull
	@Override
	public UserDto getUserData(@Nonnull final String userId)
	{
		return almUserGateway.getUserData(userId);
	}
}
