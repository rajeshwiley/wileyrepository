package com.wiley.core.order.strategies.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.strategies.AbstractOrderModificationStrategy;
import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;


public class WileyDefaultOrderCancelabilityStrategyImpl extends AbstractOrderModificationStrategy implements
		WileyOrderCancelabilityStrategy
{
	private Set<OrderStatus> orderCancelableStatuses;
	private Set<OrderStatus> orderEntryCancelableStatuses;

	@Override
	public boolean isCancelable(final OrderModel order)
	{
		return order.getStatus() != OrderStatus.CANCELLED
				&& hasCancelableEntries(order);
	}

	@Override
	public boolean hasCancelableEntries(final OrderModel order)
	{
		return hasAllEntriesInState(order, getOrderCancelableStatuses())
				&& hasNotCanceledEntries(order);
	}

	@Override
	public boolean isCancelableEntryState(final OrderStatus entryState)
	{
		return getOrderEntryCancelableStatuses().contains(entryState);
	}

	@Override
	public boolean isCancelableOrderEntry(final OrderModel order, final OrderStatus entryState)
	{
		return isCorrectSourceSystem(order) && isCancelableEntryState(entryState);
	}

	private boolean hasNotCanceledEntries(final OrderModel order)
	{
		return !hasAllEntriesInState(order, Collections.singleton(OrderStatus.CANCELLED));
	}

	private Set<OrderStatus> getOrderCancelableStatuses()
	{
		return orderCancelableStatuses;
	}

	private Set<OrderStatus> getOrderEntryCancelableStatuses()
	{
		return orderEntryCancelableStatuses;
	}

	@Required
	public void setOrderCancelableStatuses(final Set<OrderStatus> orderCancelableStatuses)
	{
		this.orderCancelableStatuses = orderCancelableStatuses;
	}

	@Required
	public void setOrderEntryCancelableStatuses(final Set<OrderStatus> orderEntryCancelableStatuses)
	{
		this.orderEntryCancelableStatuses = orderEntryCancelableStatuses;
	}
}
