package com.wiley.core.search;


import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData;
import com.wiley.facades.content.ContentData;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cContentSearchService
{
	/**
	 * Refine an exiting search. The query object allows more complex queries using facet selection.
	 *
	 * @param searchQueryData
	 * 		the search query object
	 * @param pageableData
	 * 		the page to return
	 * @return the search results
	 */
	Wileyb2cContentSearchPageData<SolrSearchQueryData, ContentData> searchAgain(SolrSearchQueryData searchQueryData,
			PageableData pageableData);
}
