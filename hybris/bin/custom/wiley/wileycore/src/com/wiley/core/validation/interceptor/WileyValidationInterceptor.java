package com.wiley.core.validation.interceptor;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.couponservices.model.AbstractCouponModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidationConfigurationException;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.exceptions.ValidationViolationException;
import de.hybris.platform.validation.interceptors.ValidationInterceptor;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.SeverityThresholdAwareChecker;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintDeclarationException;
import javax.validation.ConstraintDefinitionException;
import javax.validation.GroupDefinitionException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.validation.service.ConstraintGroupService;


public class WileyValidationInterceptor extends ValidationInterceptor
{
	private ConstraintGroupService constraintGroupService;

	private ValidationService validationService;

	private SeverityThresholdAwareChecker thresholdFilter;

	private static final Logger LOG = Logger.getLogger(WileyValidationInterceptor.class);

	@Override
	public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException
	{
		if (LOG.isDebugEnabled())
		{
			if (model instanceof AbstractCouponModel)
			{
				AbstractCouponModel coupon = (AbstractCouponModel) model;
				LOG.debug(
						"Coupon name: " + coupon.getName() + " code:" + coupon.getCouponId() + " is active:" + coupon.getActive()
								+ " start date:" + coupon.getStartDate() + " end date:" + coupon.getEndDate());
			}
		}

		if (!isValidatedWithCatalogConstrains(model))
		{
			super.onValidate(model, ctx);
		}
	}

	private boolean isValidatedWithCatalogConstrains(final Object model) throws InterceptorException
	{
		if (model instanceof ItemModel)
		{
			final Collection<ConstraintGroupModel> constraintGroupsForItem =
					constraintGroupService.getCatalogSpecificConstraintGroupForItem((ItemModel) model);
			if (CollectionUtils.isNotEmpty(constraintGroupsForItem))
			{
				validateWithConstraintGroup(model, constraintGroupsForItem);
				return true;
			}
		}
		return false;
	}

	private void validateWithConstraintGroup(final Object model, final Collection<ConstraintGroupModel> constraintGroupForItem)
			throws ValidationConfigurationException, ValidationViolationException
	{
		Set<HybrisConstraintViolation> constraintViolations;
		try
		{
			constraintViolations = validationService.validate(model, constraintGroupForItem);
		}
		catch (ConstraintDefinitionException | GroupDefinitionException | ConstraintDeclarationException exc)
		{
			throw new ValidationConfigurationException("Validation configuration exception", exc, this);
		}

		if (thresholdFilter.containsViolationsOfSeverity(constraintViolations))
		{
			throw new ValidationViolationException(constraintViolations);
		}
	}

	@Required
	public void setThresholdChecker(final SeverityThresholdAwareChecker thresholdFilter)
	{
		this.thresholdFilter = thresholdFilter;
		super.setThresholdChecker(thresholdFilter);
	}

	@Required
	public void setValidationService(final ValidationService validationService)
	{
		this.validationService = validationService;
		super.setValidationService(validationService);
	}

	@Required
	public void setConstraintGroupService(final ConstraintGroupService constraintGroupService)
	{
		this.constraintGroupService = constraintGroupService;
	}
}
