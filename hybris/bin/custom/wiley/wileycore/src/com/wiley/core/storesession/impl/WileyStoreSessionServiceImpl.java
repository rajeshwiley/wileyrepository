package com.wiley.core.storesession.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.storesession.impl.DefaultStoreSessionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.storesession.WileyStoreSessionService;


/**
 * Wiley store session service implementation with ability of optional cart recalculation.
 */
public class WileyStoreSessionServiceImpl extends DefaultStoreSessionService implements WileyStoreSessionService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyStoreSessionServiceImpl.class);

	@Autowired
	private ModelService modelService;

	/**
	 * Set current currency to session and calculate cart.
	 *
	 * @param isocode
	 * 		Currency isocode
	 */
	@Override
	public void setCurrentCurrency(final String isocode)
	{
		setCurrentCurrency(isocode, true);
	}

	/**
	 * Adjusted OOTB {@link DefaultStoreSessionService#setCurrentCurrency(String)} method.
	 * Modification: cart recalculation is modified to be optional, see <code>recalculateCart</code> parameter doc for details.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void setCurrentCurrency(final String isocode, final boolean recalculateCart)
	{
		Collection<CurrencyModel> currencies = getCommerceCommonI18NService().getAllCurrencies();
		if (currencies.isEmpty())
		{
			LOG.debug("No supported currencies found for the current site, look for all session currencies instead.");
			currencies = getCommonI18NService().getAllCurrencies();
		}
		Assert.notEmpty(currencies,
				"No supported currencies found for the current site. Please create currency for proper base store.");
		CurrencyModel currencyModel = null;
		for (final CurrencyModel currency : currencies)
		{
			if (StringUtils.equals(currency.getIsocode(), isocode))
			{
				currencyModel = currency;
			}
		}
		Assert.notNull(currencyModel, "Currency to set is not supported.");

		if (getCommonI18NService().getCurrentCurrency() != null)
		{
			if (!getCommonI18NService().getCurrentCurrency().getIsocode().equals(currencyModel.getIsocode()))
			{
				getCommonI18NService().setCurrentCurrency(currencyModel);
			}
		}
		else
		{
			getCommonI18NService().setCurrentCurrency(currencyModel);
		}

		if (getCartService().hasSessionCart())
		{
			final CartModel cart = getCartService().getSessionCart();
			cart.setCurrency(currencyModel);

			if (recalculateCart)
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(cart);
				recalculateCart(parameter);
			}
			else
			{
				cart.setCalculated(false);
				modelService.save(cart);
			}
		}
	}

	private void recalculateCart(final CommerceCartParameter parameter)
	{
		try
		{
			getCommerceCartService().recalculateCart(parameter);
		}
		catch (final CalculationException e)
		{
			LOG.warn("Could not recalculate the session cart: {}", e.getMessage());
		}
	}
}