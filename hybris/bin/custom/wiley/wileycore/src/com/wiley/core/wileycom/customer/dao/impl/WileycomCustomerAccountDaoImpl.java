package com.wiley.core.wileycom.customer.dao.impl;

import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileycom.customer.dao.WileycomCustomerAccountDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileycomCustomerAccountDaoImpl implements WileycomCustomerAccountDao
{
	private static final String QUERY_FIND_DIGITAL_ORDER_ENTRIES_BY_OWNER = "SELECT {e." + OrderEntryModel.PK + "} FROM"
			+ " {" + OrderEntryModel._TYPECODE + " AS e"
			+ " JOIN " + OrderModel._TYPECODE + " AS o ON {e." + OrderEntryModel.ORDER + "} = {o." + OrderModel.PK + "}"
			+ " JOIN " + ProductModel._TYPECODE + " AS p ON {e." + OrderEntryModel.PRODUCT + "} = {p." + ProductModel.PK + "}}"
			+ " WHERE {o." + OrderModel.USER + "} = ?customer AND "
			+ " {o." + OrderModel.STORE + "} = ?store AND "
			+ " {o." + OrderModel.VERSIONID + "} IS NULL AND "
			+ "{p." + ProductModel.EDITIONFORMAT + "} in ({{select {PK} from {ProductEditionFormat} where {code}='DIGITAL'}}) AND"
			+ "{p." + ProductModel.SUBTYPE + "} in "
			+ "({{select {PK} from {WileyProductSubtypeEnum} where {code}!='REGISTRATION_CODE'}})"
			+ " ORDER BY {o." + OrderModel.DATE + "} DESC";

	private static final String QUERY_FIND_NAME_SUFFIX_BY_CODE = "SELECT {s." + NameSuffixModel.PK + "} FROM "
			+ "{" + NameSuffixModel._TYPECODE + " AS s}"
			+ "WHERE {s." + NameSuffixModel.CODE + "} = ?code";

	private static final String QUERY_FIND_SCHOOL_BY_CODE = "SELECT {s." + SchoolModel.PK + "} FROM "
			+ "{" + SchoolModel._TYPECODE + " AS s}"
			+ "WHERE {s." + SchoolModel.CODE + "} = ?code";

	private static final String QUERY_FIND_ALL_NAME_SUFFIXES = "SELECT {s." + NameSuffixModel.PK + "} FROM "
			+ "{" + NameSuffixModel._TYPECODE + " AS s}";

	private static final String QUERY_FIND_ALL_SCHOOL_NAMES = "SELECT {s." + SchoolModel.PK + "} FROM "
			+ "{" + SchoolModel._TYPECODE + " AS s}";

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private PagedFlexibleSearchService pagedFlexibleSearchService;

	@Override
	@Nonnull
	public SearchPageData<OrderEntryModel> findDigitalOrderEntriesByOwner(final CustomerModel customer,
			final BaseStoreModel baseStore,
			final PageableData pageableData)
	{
		validateParameterNotNull(customer, "Customer must not be null");
		validateParameterNotNull(baseStore, "Store must not be null");
		validateParameterNotNull(pageableData, "Pageable Data must not be null");


		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("customer", customer);
		queryParams.put("store", baseStore);

		return pagedFlexibleSearchService.search(QUERY_FIND_DIGITAL_ORDER_ENTRIES_BY_OWNER, queryParams, pageableData);
	}

	@Override
	public Optional<NameSuffixModel> findNameSuffixByCode(final String suffixCode)
	{
		validateParameterNotNull(suffixCode, "Suffix code can't be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("code", suffixCode);
		SearchResult<NameSuffixModel> searchResult = flexibleSearchService.search(QUERY_FIND_NAME_SUFFIX_BY_CODE, params);
		return searchResult.getResult().stream().findFirst();
	}

	@Override
	public Optional<SchoolModel> findSchoolByCode(final String schoolCode)
	{
		validateParameterNotNull(schoolCode, "School code can't be null");

		final Map<String, Object> params = new HashMap<>();
		params.put("code", schoolCode);
		SearchResult<SchoolModel> searchResult = flexibleSearchService.search(QUERY_FIND_SCHOOL_BY_CODE, params);
		return searchResult.getResult().stream().findFirst();
	}

	@Override
	public List<NameSuffixModel> findAllNameSuffixes()
	{
		SearchResult<NameSuffixModel> searchResult = flexibleSearchService.search(QUERY_FIND_ALL_NAME_SUFFIXES);
		return searchResult.getResult();
	}

	@Override
	public List<SchoolModel> findAllSchools()
	{
		SearchResult<SchoolModel> searchResult = flexibleSearchService.search(QUERY_FIND_ALL_SCHOOL_NAMES);
		return searchResult.getResult();
	}

	@Override
	public List<SchoolModel> findSchoolsWithLimitation(int range, int maxQuantity)
	{
		Assert.state(range > 0);
		Assert.state(maxQuantity > 0);
		final List<SchoolModel> schools = new ArrayList<>(maxQuantity);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_ALL_SCHOOL_NAMES);
		query.setCount(range);
		query.setNeedTotal(true);

		int start = 0;
		int total;

		do
		{
			query.setStart(start);
			final SearchResult<SchoolModel> searchResult = flexibleSearchService.search(query);
			total = searchResult.getTotalCount();
			start += range;
			if (total > 0)
			{
				schools.add(searchResult.getResult().get(0));
			}
		}
		while (start < total && schools.size() < maxQuantity);

		return schools;
	}
}
