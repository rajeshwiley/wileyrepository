package com.wiley.core.order.impl;

import com.wiley.core.model.WileyGiftCardProductModel;
import de.hybris.platform.commerceservices.order.impl.OrderEntryModifiableChecker;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public class WileyOrderEntryModifiableChecker extends OrderEntryModifiableChecker
{
	@Override
	public boolean canModify(final AbstractOrderEntryModel entry)
	{
		boolean isGiftCard = entry.getProduct() instanceof WileyGiftCardProductModel;
		return !isGiftCard && super.canModify(entry);
	}
}
