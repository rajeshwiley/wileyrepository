package com.wiley.core.product.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.util.interceptor.AbstractCatalogAwareInterceptor;


/**
 * Checks unique ISBN for all Product that contains isbn
 */
public class ProductIsbnValidateInterceptor extends AbstractCatalogAwareInterceptor
		implements ValidateInterceptor<ProductModel>
{
	private WileyProductService wileyProductService;

	@Override
	public void onValidate(final ProductModel product, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (!isProductWithRestriction(product))
		{
			return;
		}
		if (StringUtils.isBlank(product.getIsbn()))
		{
			return;
		}

		final ProductModel productByISBN = wileyProductService.getProductForIsbn(product.getIsbn(),
				product.getCatalogVersion());

		if (productByISBN != null && product.equals(productByISBN))
		{
			// we update the same product.
			return;
		}

		if (productByISBN == null)
		{
			// everything is good. There are no products with the same ISBN.
			return;
		}

		throw new InterceptorException(String.format("ISBN [%s] is not unique for CatalogVersion [%s]", product.getIsbn(),
				product.getCatalogVersion()));
	}

	public void setWileyProductService(final WileyProductService wileyProductService)
	{
		this.wileyProductService = wileyProductService;
	}
}
