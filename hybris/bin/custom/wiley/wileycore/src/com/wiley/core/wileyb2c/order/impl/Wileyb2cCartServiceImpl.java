package com.wiley.core.wileyb2c.order.impl;

import com.wiley.core.event.facade.orderinfo.Wileyb2cPopulateOrderInfoDataEvent;
import com.wiley.core.order.impl.WileyCartServiceImpl;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class Wileyb2cCartServiceImpl extends WileyCartServiceImpl {
    @Override
    public void populateFromCurrentCart(final OrderInfoData orderInfoData) {
        Wileyb2cPopulateOrderInfoDataEvent event = new Wileyb2cPopulateOrderInfoDataEvent(orderInfoData);
        getApplicationEventPublisher().publishEvent(event);
    }
}
