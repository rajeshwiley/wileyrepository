package com.wiley.core.discount;

import com.wiley.core.model.WileyExternalDiscountModel;


public interface WileyExternalDiscountService
{
	WileyExternalDiscountModel getWileyExternalDiscountForGuid(String guid);
	
	boolean isWileyExternalDiscountExistedForGuid(String guid);
}
