package com.wiley.core.payment.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;

public interface AmountToPayCalculationStrategy
{
	/**
	 * Returns amount to be charged for given order. Order should be calculated before passing into this method.
	 * @param orderModel order to return amount for
	 * @return amount to be paid.
	 * @throws java.lang.IllegalStateException when order is not calculated
	 */
	BigDecimal getOrderAmountToPay(AbstractOrderModel orderModel);
}
