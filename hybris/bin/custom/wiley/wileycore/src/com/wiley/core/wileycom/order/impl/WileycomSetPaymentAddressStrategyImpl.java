package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.order.impl.WileySetPaymentAddressStrategyImpl;
import com.wiley.core.wileycom.customer.WileycomExternalAddressService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomSetPaymentAddressStrategyImpl extends WileySetPaymentAddressStrategyImpl
{
	private static final Logger LOG = Logger.getLogger(WileycomSetPaymentAddressStrategyImpl.class);

	@Resource
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	@Resource
	private WileycomExternalAddressService wileycomExternalAddressService;


	@Override
	public void setPaymentAddress(@Nonnull final CommerceCheckoutParameter commerceCheckoutParameter)
	{
		super.setPaymentAddress(commerceCheckoutParameter);
		final CustomerModel currentUser = checkoutCustomerStrategy.getCurrentUserForCheckout();
		final AddressModel addressModel = commerceCheckoutParameter.getAddress();
		
		//billing address already saved to the cart
		//trying to save it to external system, continue checkout in case of failure
		if (addressModel != null)
		{
			try
			{
				wileycomExternalAddressService.addBillingAddress(currentUser, addressModel, true);
			}
			catch (ExternalSystemException | IllegalArgumentException ex)
			{
				LOG.warn(String.format("External system could not process billing address for user %s",
						currentUser.getUid()), ex);
			}
		}
	}

}
