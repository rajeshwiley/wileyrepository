package com.wiley.core.wileyb2c.sitemap.populators;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.CategoryModelToSiteMapUrlDataPopulator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;


/**
 * Created by Maksim_Kozich on 21.09.17.
 */
public class Wileyb2cCategoryModelToSiteMapUrlDataPopulator extends CategoryModelToSiteMapUrlDataPopulator
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cCategoryModelToSiteMapUrlDataPopulator.class);

	@Override
	public void populate(final CategoryModel categoryModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		superPopulate(categoryModel, siteMapUrlData);
		if (siteMapUrlData.getLoc() == null)
		{
			getLogger().error("Resolved relative URL for category '" + categoryModel.getCode() + "' is null");
		}
	}

	protected void superPopulate(final CategoryModel categoryModel, final SiteMapUrlData siteMapUrlData)
	{
		super.populate(categoryModel, siteMapUrlData);
	}

	protected Logger getLogger()
	{
		return LOG;
	}
}
