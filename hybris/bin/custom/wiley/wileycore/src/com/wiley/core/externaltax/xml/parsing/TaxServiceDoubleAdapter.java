package com.wiley.core.externaltax.xml.parsing;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;


/**
 * Marshal/unmarshal double value using predefined {@link Locale#US}.
 */
public class TaxServiceDoubleAdapter extends XmlAdapter<String, Double>
{
	private NumberFormat format = DecimalFormat.getInstance(Locale.US);


	@Override
	public Double unmarshal(final String v) throws Exception
	{
		return format.parse(v).doubleValue();
	}

	@Override
	public String marshal(final Double v) throws Exception
	{
		return format.format(v);
	}
}
