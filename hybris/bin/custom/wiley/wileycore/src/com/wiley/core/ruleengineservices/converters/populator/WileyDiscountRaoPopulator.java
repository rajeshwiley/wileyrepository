package com.wiley.core.ruleengineservices.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyDiscountRaoPopulator implements Populator<DiscountValue, DiscountRAO>
{
	@Override
	public void populate(final DiscountValue discountValue, final DiscountRAO discountRao) throws ConversionException
	{
		validateParameterNotNullStandardMessage("discountValue", discountValue);
		validateParameterNotNullStandardMessage("discountRao", discountRao);

		discountRao.setValue(BigDecimal.valueOf(discountValue.getValue()));
		if (discountValue.isAbsolute())
		{
			discountRao.setCurrencyIsoCode(discountValue.getCurrencyIsoCode());
		}
	}
}
