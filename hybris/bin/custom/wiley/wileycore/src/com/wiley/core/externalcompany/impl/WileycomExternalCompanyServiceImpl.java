package com.wiley.core.externalcompany.impl;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.wiley.core.externalcompany.WileycomExternalCompanyService;
import com.wiley.core.model.ExternalCompanyModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.lang.String.format;


/**
 * {@link WileycomExternalCompanyService} implementation
 */
public class WileycomExternalCompanyServiceImpl implements WileycomExternalCompanyService
{

	@Resource
	private DefaultGenericDao<ExternalCompanyModel> externalCompanyGenericDao;

	/**
	 * @throws IllegalStateException
	 * 		if {@code externalId} is null or empty
	 * @throws UnknownIdentifierException
	 * 		if service doesn't find company
	 * @throws AmbiguousIdentifierException
	 * 		if serivce finds more than one company
	 */
	@Override
	public ExternalCompanyModel getExternalCompany(final String externalId)
	{
		Preconditions.checkState(StringUtils.isNotEmpty(externalId), "Parameter externalId should not be null or empty");

		List<ExternalCompanyModel> companies = externalCompanyGenericDao.find(
				Collections.singletonMap(ExternalCompanyModel.EXTERNALID, externalId));

		validateIfSingleResult(companies, format("External company with external id ['%s'] was not found!", externalId),
				format("External company with external id ['%s'] is not unique, [%d] external companies found!", externalId,
						Integer.valueOf(companies.size())));

		return companies.get(0);
	}

}
