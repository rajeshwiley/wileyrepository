package com.wiley.core.wileycom.storesession.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.storesession.WileycomCountryStoreSessionService;


/**
 * Wileycom store session service implementation
 */
public class WileycomCountryStoreSessionServiceImpl implements WileycomCountryStoreSessionService
{

	private static final Logger LOG = LoggerFactory.getLogger(WileycomCountryStoreSessionServiceImpl.class);

	@Autowired
	private WileycomI18NService wileycomI18NService;

	@Autowired
	private WileyCountryService wileyCountryService;

	@Resource
	private CartService cartService;

	private CommerceCartService commerceCartService;

	@Override
	public Optional<CountryModel> updateCurrentCountryIfRequired(final Optional<String> isocode,
			final boolean shouldCartBeRecalculated)
	{

		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();

		Optional<CountryModel> result;
		if (isocode.isPresent())
		{
			boolean isCountryCodeMatch = currentCountry.isPresent() && isocode.get().equals(currentCountry.get().getIsocode());
			if (isCountryCodeMatch)
			{
				result = Optional.empty();
				LOG.debug("Provided country [{}] matches current country", isocode);
			}
			else
			{
				CountryModel country = wileyCountryService.findCountryByCode(isocode.get());

				ServicesUtil.validateParameterNotNull(country,
						String.format("Invalid country isocode provided [%s]", isocode.get()));

				wileycomI18NService.setCurrentCountry(country);

				triggerCartRecalculation(shouldCartBeRecalculated);

				result = Optional.of(country);
				LOG.debug("Current country [{}] updated with provided value [{}]", country.getIsocode(), isocode);
			}
		}
		else
		{
			if (currentCountry.isPresent())
			{
				result = Optional.empty();
				LOG.debug("There is no country code provided. Keep using current country [{}]",
						currentCountry.get().getIsocode());
			}
			else
			{
				CountryModel defaultCurrentCountry = wileycomI18NService.setDefaultCurrentCountry();

				triggerCartRecalculation(shouldCartBeRecalculated);

				result = Optional.empty();
				LOG.debug("There is no country code provided. Fallback to default value [{}]",
						defaultCurrentCountry.getIsocode());
			}
		}
		return result;
	}

	protected void triggerCartRecalculation(final boolean shouldCartBeRecalculated)
	{
		if (shouldCartBeRecalculated && cartService.hasSessionCart())
		{
			final CartModel cart = cartService.getSessionCart();

			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(cart);
				commerceCartService.recalculateCart(parameter);
				LOG.debug("Recalculation triggered for cart [{}]", cart.getCode());
			}
			catch (final CalculationException e)
			{
				throw new RuntimeException("Could not recalculate the session cart.", e);
			}

		}
		else
		{
			LOG.debug(
					"Either cart recalculation was not allowed due to shouldCartBeRecalculated is [{}], "
							+ "or there is no cart attached to current user session [{}] (skipping cart recalculation)",
					shouldCartBeRecalculated, cartService.hasSessionCart());
		}
	}

	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}

	public void setWileyCountryService(final WileyCountryService wileyCountryService)
	{
		this.wileyCountryService = wileyCountryService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

}
