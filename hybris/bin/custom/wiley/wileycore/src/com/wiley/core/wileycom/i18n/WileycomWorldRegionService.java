package com.wiley.core.wileycom.i18n;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;

import com.wiley.core.model.WorldRegionModel;


public interface WileycomWorldRegionService
{
	/**
	 * Returns all world regions available in the system
	 *
	 * @return
	 */
	List<WorldRegionModel> getWorldRegions();

	/**
	 * Returns all countries available in the system
	 *
	 * @return
	 */
	List<CountryData> getCountries();

}
