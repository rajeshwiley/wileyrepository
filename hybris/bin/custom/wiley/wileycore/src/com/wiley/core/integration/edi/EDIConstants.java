package com.wiley.core.integration.edi;

public interface EDIConstants
{
	String CARD_TYPE_PAYPAL = "PP";
	String PAYMENT_TYPE_PAYPAL = "PP";
	String PAYMENT_TYPE_CREDIT_CARD = "CC";
	String EDI_GRAB_ORDERS_TIME_PROPERTY_PATTERN = "%s.edi.export.time.limit";

	/**
	 * The constant EDI_GRAB_ALL_PENDING_ORDERS_PROPERTY is needed to ignore grab orders time and get ALL pending orders
	 */
	String EDI_GRAB_ALL_PENDING_ORDERS_PROPERTY = "edi.export.ignore.time.limit";

	/**
	 * The constant EDI_DEFAULT_GRAB_ORDERS_TIME.
	 */
	String EDI_DEFAULT_GRAB_ORDERS_TIME = "23:15:00";

	/**
	 * The constant WILEY_ORDER_EXPORTED_EVENT_NAME.
	 */
	String WILEY_ORDER_EXPORTED_EVENT_NAME = "WileyOrderExported";

	/**
	 * The constant EDI_SUPPORTED_STORES_PROPERTY_PATTERN is used to define property name to configure
	 */
	String EDI_SUPPORTED_STORES_PROPERTY_PATTERN = "%s.edi.supported.stores";

	/**
	 * The constant EDI_SUPPORTED_STORES_DEFAULT is used to define default EDI_SUPPORTED_STORES_PROPERTY_PATTERN value
	 */
	String EDI_SUPPORTED_STORES_DEFAULT = "wel,ags";
}
