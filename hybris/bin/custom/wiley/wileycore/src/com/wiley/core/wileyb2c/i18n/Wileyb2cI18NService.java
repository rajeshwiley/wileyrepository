/**
 *
 */
package com.wiley.core.wileyb2c.i18n;

import java.util.Optional;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


public interface Wileyb2cI18NService extends WileycomI18NService
{
	/**
	 * Get tax short message based on session world region
	 *
	 * @return {@link String} tax short message if required
	 */
	Optional<String> getCurrentCountryTaxShortMsg();

	/**
	 * Get tax tooltip based on session world region
	 *
	 * @return {@link String} tooltip if required
	 */
	Optional<String> getCurrentCountryTaxTooltip();

	/**
	 * Get date format based on session world region
	 *
	 * @return {@link String} date format if required
	 */
	String getCurrentDateFormat();
}
