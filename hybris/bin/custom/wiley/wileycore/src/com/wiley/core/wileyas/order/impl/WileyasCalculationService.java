package com.wiley.core.wileyas.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Collections;

import com.wiley.core.order.impl.WileyExternalTaxCalculationService;


public class WileyasCalculationService extends WileyExternalTaxCalculationService
{
	@Override
	protected void calculateTotalTaxValues(final AbstractOrderEntryModel entry)
	{
		if (entry.getQuantity() != 0)
		{
			super.calculateTotalTaxValues(entry);
		}
		else
		{
			entry.setTaxValues(Collections.emptyList());
		}
	}
}
