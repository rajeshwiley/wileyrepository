package com.wiley.core.integration.ebp;

import javax.annotation.Nonnull;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.integration.ebp.dto.EbpAddProductsResponse;
import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.core.integration.ebp.dto.EbpCreateUserResponse;

/**
 * The main service for communicating with EBP.
 *
 * @author Uladzimir_Barouski
 */
public interface EbpGateway
{
	/**
	 * Updates UserNode in EBP.
	 *
	 * @param ebpCreateUserPayload
	 * @return service response.
	 */
	@Nonnull
	EbpCreateUserResponse createEbpUser(@Nonnull EbpCreateUserPayload ebpCreateUserPayload);

	/**
	 * Adds products to EBP.
	 *
	 * @param ebpAddProductsPayload
	 * @return service response.
	 */
	@Nonnull
	EbpAddProductsResponse addProducts(@Nonnull EbpAddProductsPayload ebpAddProductsPayload);
}
