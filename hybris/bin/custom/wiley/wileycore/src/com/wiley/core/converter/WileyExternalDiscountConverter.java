package com.wiley.core.converter;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import com.wiley.core.model.WileyExternalDiscountModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyExternalDiscountConverter implements Converter<WileyExternalDiscountModel, DiscountValue>
{
	@Override
	public DiscountValue convert(final WileyExternalDiscountModel externalDiscount) throws ConversionException
	{
		validateParameterNotNullStandardMessage("wileyExternalDiscount", externalDiscount);

		double value = externalDiscount.getValue() == null ? 0.0 : externalDiscount.getValue();
		
		if (externalDiscount.getAbsolute() != null && externalDiscount.getAbsolute())
		{
			return DiscountValue.createAbsolute(externalDiscount.getGuid(), value, externalDiscount.getCurrency().getIsocode());
		}
		return DiscountValue.createRelative(externalDiscount.getGuid(), value);
	}

	@Override
	public DiscountValue convert(final WileyExternalDiscountModel wileyExternalDiscountModel, final DiscountValue discountValue)
			throws ConversionException
	{
		throw new UnsupportedOperationException("DiscountValue is immutable");
	}
}
