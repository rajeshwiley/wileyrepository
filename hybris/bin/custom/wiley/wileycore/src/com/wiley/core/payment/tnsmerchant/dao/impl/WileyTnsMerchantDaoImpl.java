package com.wiley.core.payment.tnsmerchant.dao.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.payment.tnsmerchant.dao.WileyTnsMerchantDao;

import static com.wiley.core.constants.GeneratedWileyCoreConstants.Relations.WILEYTNSMERCHANT2COUNTRY;
import static com.wiley.core.constants.GeneratedWileyCoreConstants.Relations.WILEYTNSMERCHANT2CURRENCY;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyTnsMerchantDaoImpl implements WileyTnsMerchantDao
{
	private static final String CURRENCY_ISO_CODE = "currencyIsoCode";
	private static final String COUNTRY_ISO_CODE = "countryIsoCode";
	private static final String FIND_BY_COUNTRY_ISO_CODE = "SELECT {" + WileyTnsMerchantModel.PK + "} "
			+ "FROM {" + WileyTnsMerchantModel._TYPECODE + " AS m "
			+ "JOIN " + WILEYTNSMERCHANT2COUNTRY + " AS m2cntr ON {m." + WileyTnsMerchantModel.PK + "}={m2cntr.source} "
			+ "JOIN " + CountryModel._TYPECODE + " AS cntr ON {m2cntr.target}={cntr." + CountryModel.PK + "} "
			+ "JOIN " + WILEYTNSMERCHANT2CURRENCY + " AS m2cur ON {m." + WileyTnsMerchantModel.PK + "}={m2cur.source} "
			+ "JOIN " + CurrencyModel._TYPECODE + " AS cur ON {m2cur.target}={cur." + CurrencyModel.PK + "}} "
			+ "WHERE {cntr." + CountryModel.ISOCODE + "} = ?" + COUNTRY_ISO_CODE + " "
			+ "AND {cur." + CurrencyModel.ISOCODE + "} = ?" + CURRENCY_ISO_CODE;

	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<WileyTnsMerchantModel> findTnsMerchant(final String countryIsoCode, final String currencyIsoCode)
	{
		validateParameterNotNull(countryIsoCode, "Parameter countryIsoCode must not be null.");
		validateParameterNotNull(currencyIsoCode, "Parameter currencyIsoCode must not be null.");

		final Map<String, String> params = new HashMap<>();
		params.put(COUNTRY_ISO_CODE, countryIsoCode);
		params.put(CURRENCY_ISO_CODE, currencyIsoCode);

		final SearchResult<WileyTnsMerchantModel> searchResult = flexibleSearchService.search(FIND_BY_COUNTRY_ISO_CODE, params);
		return searchResult.getResult();
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
