package com.wiley.core.healthcheck.checks;

import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;


/**
 * The type Solr health check.
 */
public class SolrHealthCheck extends HealthCheck
{
	private static final Logger LOG = LoggerFactory.getLogger(SolrHealthCheck.class);

	@Resource
	private SolrSearchProvider solrStandaloneSearchProvider;
	@Resource
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;
	@Resource
	private FacetSearchConfigService facetSearchConfigService;

	@Override
	protected Result check() throws Exception
	{
		try
		{
			final Set<SolrClient> servers = getSolrClientsForFacetSearchConfig(getFacetSearchConfig());

			for (final SolrClient server : servers)
			{
				if (!checkSolrServerAvailability(server))
				{
					return Result.unhealthy("Solr server is unavailable");
				}
			}
		}
		catch (final IOException | SolrServerException e)
		{
			LOG.error("An exception occurred during Solr health check", e);
			return Result.unhealthy(buildErrorString(e));
		}

		return Result.healthy();
	}

	private Set<SolrClient> getSolrClientsForFacetSearchConfig(final FacetSearchConfig facetSearchConfig)
			throws SolrServiceException
	{
		Set<SolrClient> solrClients = new HashSet<>();

		for (Map.Entry<String, IndexedType> entry : facetSearchConfig.getIndexConfig().getIndexedTypes().entrySet())
		{
			//TODO-REFACT-1808 Check if the 3rd argument ('qualifier') has an appropriate value
			Index index = solrStandaloneSearchProvider.resolveIndex(facetSearchConfig, entry.getValue(), entry.getKey());
			SolrClient solrClient = null;
			solrClient = solrStandaloneSearchProvider.getClient(index);
			solrClients.add(solrClient);
		}

		return solrClients;
	}

	private boolean checkSolrServerAvailability(final SolrClient solrClient) throws IOException, SolrServerException
	{
		return "OK".equalsIgnoreCase((String) solrClient.ping().getResponse().get("status"));
	}

	private FacetSearchConfig getFacetSearchConfig() throws NoValidSolrConfigException, FacetConfigServiceException
	{
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = solrFacetSearchConfigSelectionStrategy
				.getCurrentSolrFacetSearchConfig();
		return facetSearchConfigService.getConfiguration(solrFacetSearchConfigModel.getName());
	}

	private String buildErrorString(final Exception e)
	{
		return e.getClass().getSimpleName() + ": " + e.getMessage();
	}
}