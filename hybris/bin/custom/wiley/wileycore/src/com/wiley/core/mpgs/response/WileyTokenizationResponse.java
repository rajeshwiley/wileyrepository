package com.wiley.core.mpgs.response;


public class WileyTokenizationResponse extends WileyResponse
{
	private String cardNumber;
	private String cardScheme;
	private String cardExpiryMonth;
	private String cardExpiryYear;
	private String token;

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getCardScheme()
	{
		return cardScheme;
	}

	public void setCardScheme(final String cardScheme)
	{
		this.cardScheme = cardScheme;
	}

	public String getCardExpiryMonth()
	{
		return cardExpiryMonth;
	}

	public void setCardExpiryMonth(final String cardExpiryMonth)
	{
		this.cardExpiryMonth = cardExpiryMonth;
	}

	public String getCardExpiryYear()
	{
		return cardExpiryYear;
	}

	public void setCardExpiryYear(final String cardExpiryYear)
	{
		this.cardExpiryYear = cardExpiryYear;
	}

}
