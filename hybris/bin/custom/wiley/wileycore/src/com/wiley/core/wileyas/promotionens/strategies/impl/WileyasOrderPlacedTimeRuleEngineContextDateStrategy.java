package com.wiley.core.wileyas.promotionens.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Date;

import com.wiley.core.promotions.strategies.WileyRuleEngineContextDateStrategy;


public class WileyasOrderPlacedTimeRuleEngineContextDateStrategy implements WileyRuleEngineContextDateStrategy
{
	@Override
	public Date getRuleEngineContextDate(final AbstractOrderModel order)
	{
		return order.getDate();
	}
}
