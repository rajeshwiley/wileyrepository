package com.wiley.core.wiley.order.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.primitives.Longs;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;
import com.wiley.core.wiley.order.WileyOrderHistoryService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyOrderPaymentServiceImpl implements WileyOrderPaymentService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyOrderPaymentServiceImpl.class);

	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	@Autowired
	private WileyOrderHistoryService orderHistoryService;

	@Override
	public Collection<PendingPaymentActon> getPendingPaymentActions(final OrderModel order)
	{
		OrderModel previousOrderVersion = orderHistoryService.getPreviousOrderVersion(order);
		List<PendingPaymentActon> pendingActions = new ArrayList<>();
		if (previousOrderVersion != null)
		{
			if (isPaymentModeChanged(order, previousOrderVersion) || isCreditCardChanged(order, previousOrderVersion)
					|| isCurrencyChanged(order, previousOrderVersion))
			{
				handlePaymentAndCurrencyChange(order, previousOrderVersion, pendingActions);
			}
			else if (isTotalsChanged(order, previousOrderVersion))
			{
				if (isEligibleForRefund(previousOrderVersion))
				{
					handleTotalsChange(order, previousOrderVersion, pendingActions);
				}
				else
				{
					addChargeAction(calculateTotalWithTax(order), order, pendingActions);
				}
			}
		}
		else
		{
			addChargeAction(calculateTotalWithTax(order), order, pendingActions);
		}
		return pendingActions;
	}

	@Override
	public Collection<PendingPaymentActon> getPendingPaymentActions(final OrderModel order,
			final PaymentActonType paymentActonType)
	{
		validateParameterNotNullStandardMessage("order", order);
		validateParameterNotNullStandardMessage("paymentActonType", paymentActonType);

		List<PendingPaymentActon> filteredPendingPaymentActions = getPendingPaymentActions(order).stream()
				.filter(pa -> pa.getAction() == paymentActonType)
				.collect(Collectors.toList());
		return filteredPendingPaymentActions;
	}

	private boolean isCreditCardChanged(final OrderModel order, final OrderModel previousOrderVersion)
	{
		final PaymentInfoModel paymentInfo = order.getPaymentInfo();
		final PaymentInfoModel previousPaymentInfo = previousOrderVersion.getPaymentInfo();
		if (paymentInfo instanceof CreditCardPaymentInfoModel && previousPaymentInfo instanceof CreditCardPaymentInfoModel)
		{
			String subscriptionId = ((CreditCardPaymentInfoModel) paymentInfo).getSubscriptionId();
			String previousSubscriptionId = ((CreditCardPaymentInfoModel) previousPaymentInfo).getSubscriptionId();

			return !subscriptionId.equals(previousSubscriptionId);
		}
		return false;
	}

	private void handlePaymentAndCurrencyChange(final OrderModel order, final OrderModel previousOrderVersion,
			final List<PendingPaymentActon> pendingActions)
	{
		if (isEligibleForRefund(previousOrderVersion))
		{
			addRefundActions(calculateTotalWithTax(previousOrderVersion), order, pendingActions);
		}
		addChargeAction(calculateTotalWithTax(order), order, pendingActions);
	}

	private boolean isEligibleForRefund(final OrderModel previousOrderVersion)
	{
		String previousPaymentMode = previousOrderVersion.getPaymentMode().getCode().toUpperCase();
		return PaymentModeEnum.CARD.equals(PaymentModeEnum.valueOf(previousPaymentMode))
				|| PaymentModeEnum.PAYPAL.equals(PaymentModeEnum.valueOf(previousPaymentMode));
	}

	private void handleTotalsChange(final OrderModel order, final OrderModel previousOrderVersion,
			final List<PendingPaymentActon> pendingActions)
	{
		BigDecimal changedAmount = calculateAmountChange(order, previousOrderVersion);
		if (BigDecimal.ZERO.compareTo(changedAmount) > 0)
		{
			addRefundActions(changedAmount.abs(), order, pendingActions);
		}
		else
		{
			addChargeAction(changedAmount, order, pendingActions);
		}
	}

	private void addChargeAction(final BigDecimal amount, final OrderModel order,
			final List<PendingPaymentActon> pendingActions)
	{
		PaymentTransactionModel latestTransaction = findLatestTransaction(order);
		boolean isAuthorized = latestTransaction != null && isAuthorized(latestTransaction, amount, order.getCurrency());
		boolean isCaptured = latestTransaction != null && isCaptured(latestTransaction, amount, order.getCurrency());

		if (isAuthorized && isCaptured)
		{
			LOG.debug("No pending charge payment action found for order {}", order.getCode());

		}
		else if (isAuthorized && !isCaptured)
		{
			pendingActions.add(createPendingAction(amount, order, latestTransaction, PaymentActonType.CAPTURE));
		}
		else
		{
			pendingActions.add(createPendingAction(amount, order, null, PaymentActonType.AUTHORIZE));
		}
	}

	private boolean isCaptured(final PaymentTransactionModel latestTransaction, final BigDecimal amount,
			final CurrencyModel currency)
	{
		PaymentTransactionEntryModel entry = paymentTransactionService.getAcceptedTransactionEntry(latestTransaction,
				PaymentTransactionType.CAPTURE);
		return entry != null && doesEntryMatch(entry, amount, currency);
	}

	private boolean isAuthorized(final PaymentTransactionModel latestTransaction, final BigDecimal amount,
			final CurrencyModel currency)
	{
		PaymentTransactionEntryModel entry = paymentTransactionService.getAcceptedTransactionEntry(latestTransaction,
				PaymentTransactionType.AUTHORIZATION);
		return entry != null && doesEntryMatch(entry, amount, currency);
	}

	private PendingPaymentActon createPendingAction(final BigDecimal amount, final OrderModel order,
			final PaymentTransactionModel transaction,
			final PaymentActonType type)
	{
		PendingPaymentActon action = new PendingPaymentActon();
		action.setAction(type);
		action.setAmount(commonI18NService.roundCurrency(amount.doubleValue(), order.getCurrency().getDigits()));
		action.setTransaction(transaction);
		return action;
	}

	private PaymentTransactionModel findLatestTransaction(final OrderModel order)
	{

		return order.getPaymentTransactions()
				.stream()
				.sorted((t1, t2) -> Longs.compare(t2.getCreationtime().getTime(), t1.getCreationtime().getTime()))
				.filter(t -> CollectionUtils.isNotEmpty(t.getEntries()))
				.findFirst().orElse(null);

	}

	private boolean doesEntryMatch(final PaymentTransactionEntryModel entry,
			final BigDecimal amount, final CurrencyModel currency)
	{
		return entry.getAmount().compareTo(amount) == 0
				&& entry.getCurrency().getIsocode().equals(currency.getIsocode());
	}

	private void addRefundActions(final BigDecimal amount, final OrderModel order, final List<PendingPaymentActon> pendingActions)
	{
		BigDecimal amountLeft = amount;
		Iterator<PaymentTransactionModel> transactions = order.getPaymentTransactions().iterator();
		while (amountLeft.compareTo(BigDecimal.ZERO) > 0 && transactions.hasNext())
		{
			PaymentTransactionModel transaction = transactions.next();


			PaymentTransactionEntryModel captureTransaction =
					paymentTransactionService.getAcceptedTransactionEntry(transaction, PaymentTransactionType.CAPTURE);
			if (captureTransaction != null)
			{
				BigDecimal amountAvailableToRefund = captureTransaction.getAmount()
						.subtract(calculateAmountRefunded(transaction));

				if (amountAvailableToRefund.compareTo(amountLeft) >= 0)
				{
					pendingActions.add(createPendingAction(amountLeft, order, transaction, PaymentActonType.REFUND));
					return;
				}
				else if (amountAvailableToRefund.compareTo(BigDecimal.ZERO) > 0)
				{
					pendingActions.add(createPendingAction(amountAvailableToRefund,
							order, transaction, PaymentActonType.REFUND));
					amountLeft = amountLeft.subtract(amountAvailableToRefund);
				}
			}
		}
		if (amountLeft.compareTo(BigDecimal.ZERO) > 0)
		{
			throw new IllegalStateException("Unable to create transactions refund list. "
					+ "Not enough available transactions to refund");
		}
	}

	private BigDecimal calculateAmountRefunded(final PaymentTransactionModel transaction)
	{
		return paymentTransactionService.getAcceptedTransactionEntries(transaction,
				PaymentTransactionType.REFUND_FOLLOW_ON)
				.stream()
				.filter(transactionEntry -> transactionEntry.getAmount() != null)
				.map(PaymentTransactionEntryModel::getAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	private boolean isTotalsChanged(final OrderModel order, final OrderModel previousOrderVersion)
	{
		return BigDecimal.ZERO.compareTo(calculateAmountChange(order, previousOrderVersion)) != 0;
	}

	private boolean isCurrencyChanged(final OrderModel order, final OrderModel previousOrderVersion)
	{
		return !order.getCurrency().getIsocode().equals(previousOrderVersion.getCurrency().getIsocode());
	}

	private boolean isPaymentModeChanged(final OrderModel order, final OrderModel previousOrderVersion)
	{
		return !order.getPaymentMode().getCode().equals(previousOrderVersion.getPaymentMode().getCode());
	}

	private BigDecimal calculateAmountChange(final OrderModel order, final OrderModel previousOrderVersion)
	{
		BigDecimal cost = calculateTotalWithTax(order);

		if (previousOrderVersion != null)
		{
			cost = cost.subtract(calculateTotalWithTax(previousOrderVersion));
		}

		return cost;
	}

	private BigDecimal calculateTotalWithTax(final OrderModel order)
	{
		BigDecimal total = BigDecimal.valueOf(order.getTotalPrice());
		total = total.add(BigDecimal.valueOf(order.getTotalTax()));
		return total;
	}

	@Override
	public BigDecimal calculateAmountChanged(final OrderModel model)
	{
		final OrderModel perviousOrderVersion = orderHistoryService.getPreviousOrderVersion(model);
		return calculateAmountChange(model, perviousOrderVersion);
	}

	@Override
	public boolean isCurrencyChanged(final OrderModel model)
	{
		final OrderModel previousOrderVersion = orderHistoryService.getPreviousOrderVersion(model);
		return isCurrencyChanged(model, previousOrderVersion);
	}
}
