package com.wiley.core.mpgs.services;


import javax.annotation.Nonnull;


public interface WileyUrlGenerationService
{
	String getRetrieveSessionUrl(@Nonnull String tnsMerchantId, @Nonnull String sessionId);

	String getVerifyUrl(@Nonnull String orderGuid, @Nonnull String transactionId,
			@Nonnull String tnsMerchantId);
	
	String getTokenizationUrl(@Nonnull String tnsMerchantId);
	
	String getAuthorizationURL(@Nonnull String tnsMerchantId, 
			@Nonnull String orderGuid, @Nonnull String transactionId);

	String getCaptureUrl(@Nonnull String tnsMerchantId, 
			@Nonnull String orderGuid, @Nonnull String transactionId);

	String getRefundFollowOnUrl(@Nonnull String tnsMerchantId, 
			@Nonnull String orderGuid, @Nonnull String transactionId);
	
}
