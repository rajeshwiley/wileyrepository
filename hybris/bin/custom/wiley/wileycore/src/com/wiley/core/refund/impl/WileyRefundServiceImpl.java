package com.wiley.core.refund.impl;

import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.commands.result.RefundResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.refund.OrderRefundEntry;
import de.hybris.platform.refund.impl.DefaultRefundService;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.constants.PaypalConstants;
import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.response.WileyRefundResult;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.refund.OrderRefundException;
import com.wiley.core.refund.OrderRefundRequest;
import com.wiley.core.refund.WileyPaypalRefundService;
import com.wiley.core.refund.WileyRefundCalculationService;
import com.wiley.core.refund.WileyRefundCheckStrategy;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.core.returns.WileyReturnService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class WileyRefundServiceImpl extends DefaultRefundService implements WileyRefundService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyRefundServiceImpl.class);
	private static final String REFUND_ORDER_HISTORY_ENTRY_DESCRIPTION = "Refund request for order: ";

	@Autowired
	private WileyRefundCalculationService refundCalculationService;
	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private CardPaymentService cardPaymentService;
	@Autowired
	private WileyPaypalRefundService wileyPaypalRefundService;
	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private PaymentTransactionService transactionService;
	@Autowired
	private WileyRefundCheckStrategy refundCheckStrategy;
	@Autowired
	private WileyOrderPaymentService wileyOrderPaymentService;
	@Autowired
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;
	@Autowired
	private WileyReturnService wileyReturnService;
	@Autowired
	private UserService userService;

	@Override
	public PaymentTransactionEntryModel processRefundPaymentTransaction(final OrderModel order, final BigDecimal refundAmount)
	{
		PaymentTransactionModel paymentTransactionWithCapturePayment =
				transactionService.getAcceptedTransactionEntry(order, PaymentTransactionType.CAPTURE).getPaymentTransaction();
		return refundPaymentTransaction(paymentTransactionWithCapturePayment, order, refundAmount);
	}

	private PaymentTransactionEntryModel refundPaymentTransaction(final PaymentTransactionModel transaction,
			final OrderModel order,
			final BigDecimal refundAmount)
	{
		final PaymentTransactionEntryModel authTransactionEntry = transactionService.getAcceptedTransactionEntry(transaction,
				PaymentTransactionType.AUTHORIZATION);
		final PaymentTransactionEntryModel captureTransactionEntry = transactionService.getAcceptedTransactionEntry(transaction,
				PaymentTransactionType.CAPTURE);

		validateRefundAvailability(order, authTransactionEntry, refundAmount);

		PaymentTransactionType transactionType = PaymentTransactionType.REFUND_FOLLOW_ON;
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, transactionType);
		String requestId = captureTransactionEntry.getRequestId();
		String requestToken = authTransactionEntry.getRequestToken();
		Currency currency = Currency.getInstance(order.getCurrency().getIsocode());
		String paymentProvider = transaction.getPaymentProvider();

		final WileyFollowOnRefundRequest refundRequest = new WileyFollowOnRefundRequest(
				newEntryCode,
				requestId,
				requestToken,
				currency,
				refundAmount,
				paymentProvider,
				order.getSite().getUid(),
				order.getCode());

		final RefundResult refundResult;

		if (PaypalConstants.PAYMENT_PROVIDER_NAME.equals(paymentProvider))
		{
			refundResult = wileyPaypalRefundService.performPayPalRefund(refundRequest);
			return createTransactionEntry(transaction, refundResult, newEntryCode);

		}
		else if (wileyMPGSPaymentProviderService.isMPGSProviderGroup(paymentProvider))
		{
			return wileyMPGSPaymentService.refundFollowOn(transaction, refundAmount);
		}
		else
		{
			refundResult = cardPaymentService.refundFollowOn(refundRequest);
			return createTransactionEntry(transaction, refundResult, newEntryCode);
		}
	}

	public boolean isFullyRefundedOrder(final OrderModel order)
	{
		return refundCheckStrategy.isFullyRefundedOrder(order);
	}

	public boolean isOrderRefundedByOneTransaction(final WileyExportProcessModel process)
	{
		return refundCheckStrategy.isOrderRefundedByOneTransaction(process);
	}

	private void validateRefundAvailability(final OrderModel order,
			final PaymentTransactionEntryModel authTransactionEntry,
			final BigDecimal totalRefundAmount)
	{
		if (totalRefundAmount == null || totalRefundAmount.compareTo(BigDecimal.ZERO) <= 0)
		{
			validationFailed(order.getCode(), totalRefundAmount,
					"Incorrect refund amount given.");
		}
		if (totalRefundAmount.compareTo(refundCalculationService
				.calculateMaximumAvailableRefundAmountForOrder(order)) > 0)
		{
			validationFailed(order.getCode(), totalRefundAmount,
					"Refund amount is greater than allowed.");
		}
		if (authTransactionEntry == null)
		{
			validationFailed(order.getCode(), totalRefundAmount,
					"Accepted Authorization transaction entry not found.");
		}
	}

	private void validationFailed(final String orderCode, final BigDecimal amount, final String message)
	{
		String fullMessage = message + "Cannot process refund transaction for order ";
		LOG.error("Refund availability for order '{}' and expected amount '{}' failed: {}", orderCode,
				amount, fullMessage);
		throw new IllegalArgumentException(fullMessage);
	}

	private PaymentTransactionEntryModel createTransactionEntry(final PaymentTransactionModel transaction,
			final RefundResult result, final String newEntryCode)
	{
		PaymentTransactionEntryModel entry = getModelService().create(PaymentTransactionEntryModel.class);
		entry.setAmount(result.getTotalAmount());

		if (result.getCurrency() != null)
		{
			entry.setCurrency(commonI18NService.getCurrency(result.getCurrency().getCurrencyCode()));
		}

		entry.setType(PaymentTransactionType.REFUND_FOLLOW_ON);
		entry.setRequestId(result.getRequestId());
		entry.setRequestToken(result.getRequestToken());
		entry.setTime(result.getRequestTime() == null ? new Date() : result.getRequestTime());
		entry.setPaymentTransaction(transaction);
		if (result.getTransactionStatus() != null)
		{
			entry.setTransactionStatus(result.getTransactionStatus().toString());
		}
		entry.setCode(newEntryCode);

		if (result instanceof WileyRefundResult)
		{
			WileyRefundResult wileyRefundResult = (WileyRefundResult) result;
			if (PaypalConstants.PAYMENT_PROVIDER_NAME.equals(wileyRefundResult.getPaymentProvider()))
			{
				processPayPalTransactionDetails(wileyRefundResult, entry);
			}
			else
			{
				processWPGTransactionDetails(wileyRefundResult, entry);
			}
		}
		else
		{
			LOG.error("Wrong refund result type. Expected type " + WileyRefundResult.class + " actual type "
					+ result.getClass());
		}

		getModelService().saveAll(entry, transaction);
		return entry;
	}

	private void processPayPalTransactionDetails(
			final WileyRefundResult refundResult, final PaymentTransactionEntryModel entry)
	{
		if (StringUtils.isNotEmpty(refundResult.getPayPalTransactionErrorDetails()))
		{
			entry.setTransactionStatusDetails(refundResult.getPayPalTransactionErrorDetails());
		}
		else
		{
			entry.setTransactionStatusDetails(refundResult.getTransactionStatusDetails().toString());
		}
	}

	private void processWPGTransactionDetails(final WileyRefundResult refundResult, final PaymentTransactionEntryModel entry)
	{
		entry.setTransactionStatusDetails(refundResult.getWpgTransactionStatus().getDescription());
		entry.setWpgMerchantResponse(refundResult.getMerchantResponse());
		entry.setWpgResponseCode(refundResult.getWpgTransactionStatus().getCode());
	}

	@Override
	public boolean processRefundForModifiedOrder(final OrderModel order)
	{
		List<PendingPaymentActon> paymentActions = getPaymentActions(order);
		for (final PendingPaymentActon pendingPaymentAction : paymentActions)
		{
			final String refundTransactionResult = executeRefundAction(pendingPaymentAction, order);
			if (!TransactionStatus.ACCEPTED.name().equals(refundTransactionResult))
			{
				return false;
			}
		}
		return true;
	}


	private boolean processRefundForOrder(final ReturnRequestModel request)
	{
		OrderModel order = request.getOrder();

		final PaymentTransactionEntryModel transaction = processRefundPaymentTransaction(order,
				BigDecimal.valueOf(request.getRefundAmount()));

		boolean refundPaymentSucceeded = TransactionStatus.ACCEPTED.name().equals(transaction.getTransactionStatus());

		if (!refundPaymentSucceeded)
		{
			LOG.warn(String.format("Expected transaction status %s is different from actual %s",
					TransactionStatus.ACCEPTED.name(), transaction.getTransactionStatus()));
		}

		setStatusForRefundEntries(refundPaymentSucceeded ? ReturnStatus.COMPLETED : ReturnStatus.PAYMENT_FAILED,
				transaction.getCode(), getRefunds(request));

		wileyReturnService.postRefundProcess(transaction, request);

		return refundPaymentSucceeded;
	}

	private void setStatusForRefundEntries(final ReturnStatus status, final String entryCode,
			final List<RefundEntryModel> refunds)
	{
		for (final RefundEntryModel refund : refunds)
		{
			refund.setStatus(status);
			refund.setTransactionCode(entryCode);
		}
		getModelService().saveAll(refunds);
	}


	private void createOrderRefundHistoryEntry(final OrderModel orderModel, final String description)
	{
		final OrderHistoryEntryModel historyModel = getModelService().create(OrderHistoryEntryModel.class);
		historyModel.setTimestamp(new Date());
		historyModel.setOrder(orderModel);
		historyModel.setDescription(description);
		UserModel user = userService.getCurrentUser();
		if (user instanceof EmployeeModel)
		{
			historyModel.setEmployee((EmployeeModel) user);
		}
		else
		{
			LOG.warn("Cannot set not employee {} into the history entry on refund of the order {}",
					user.getName(), orderModel.getCode());
		}

		getModelService().save(historyModel);
	}

	private List<PendingPaymentActon> getPaymentActions(final OrderModel order)
	{
		return wileyOrderPaymentService.getPendingPaymentActions(order).stream()
				.filter(this::isRefundAction)
				.collect(Collectors.toList());

	}

	private boolean isRefundAction(final PendingPaymentActon paymentAction)
	{
		return PaymentActonType.REFUND.equals(paymentAction.getAction());
	}

	private String executeRefundAction(final PendingPaymentActon paymentAction, final OrderModel order)
	{
		final PaymentTransactionModel transaction = paymentAction.getTransaction();
		final BigDecimal refundAmount = BigDecimal.valueOf(paymentAction.getAmount());

		return refundPaymentTransaction(transaction, order, refundAmount).getTransactionStatus();
	}

	private List<RefundEntryModel> createRefundEntries(final List<OrderRefundEntry> entries,
			final ReturnRequestModel request)
	{
		List<RefundEntryModel> result = new ArrayList<>();

		for (final OrderRefundEntry entry : entries)
		{
			RefundEntryModel refundEntry = wileyReturnService.createRefund(
					request,
					entry.getOrderEntry(),
					entry.getNotes(),
					entry.getExpectedQuantity(),
					ReturnAction.IMMEDIATE,
					entry.getRefundReason());
			result.add(refundEntry);
		}
		getModelService().saveAll(result);

		return result;
	}

	protected String createOrderHistoryEntryDescription(final List<RefundEntryModel> refunds)
	{
		StringBuilder description = new StringBuilder();
		for (final RefundEntryModel refund : refunds)
		{
			description.append(refund.getOrderEntry().getProduct().getName()).append(", ")
					.append(refund.getExpectedQuantity()).append(", ")
					.append(refund.getReason()).append(", ")
					.append(refund.getNotes()).append('\n');
		}
		return description.toString();
	}

	@Override
	public void processRefund(final OrderRefundRequest refundRequest) throws OrderRefundException
	{
		OrderModel orderModel = refundRequest.getOrder();
		if (isEligibleForRefund(orderModel))
		{
			BigDecimal refundAmount = refundRequest.getRefundAmount();
			final ReturnRequestModel request
					= wileyReturnService.createReturnRequestWithAmount(orderModel, refundAmount);

			final List<RefundEntryModel> refundEntries
					= createRefundEntries(refundRequest.getEntriesToRefund(), request);

			createOrderRefundHistoryEntry(orderModel,
					REFUND_ORDER_HISTORY_ENTRY_DESCRIPTION + createOrderHistoryEntryDescription(refundEntries));

			final boolean refundPaymentSucceeded = processRefundForOrder(request);

			if (!refundPaymentSucceeded)
			{
				throw new OrderRefundException(orderModel.getCode(), "Order refund failed");
			}
		}
		else
		{
			throw new OrderRefundException(orderModel.getCode(), "Order is not eligible for refund");
		}

	}

	private boolean isEligibleForRefund(final AbstractOrderEntryModel orderEntryModel)
	{
		if (orderEntryModel == null)
		{
			return false;
		}

		if (orderEntryModel.getProduct() instanceof WileyGiftCardProductModel)
		{
			return false;
		}

		return true;
	}

	@Override
	public boolean isEligibleForRefund(final OrderModel orderModel)
	{
		if (orderModel == null)
		{
			return false;
		}

		if (getModelService().isModified(orderModel))
		{
			getModelService().refresh(orderModel);
		}

		OrderStatus orderStatus = orderModel.getStatus();

		if (!OrderStatus.COMPLETED.equals(orderStatus) && !OrderStatus.PAYMENT_CAPTURED.equals(orderStatus))
		{
			return false;
		}

		if (transactionService.getAcceptedTransactionEntry(orderModel, PaymentTransactionType.CAPTURE) == null)
		{
			return false;
		}

		if (isFullyRefundedOrder(orderModel))
		{
			return false;
		}

		return hasRefundableEntries(orderModel.getEntries());
	}

	@Override
	public List<AbstractOrderEntryModel> getRefundableEntries(final List<AbstractOrderEntryModel> entries)
	{
		return entries.stream()
				.filter(this::isEligibleForRefund)
				.collect(Collectors.toList());
	}

	private boolean hasRefundableEntries(final List<AbstractOrderEntryModel> entries)
	{
		return entries.stream()
				.anyMatch(this::isEligibleForRefund);
	}

	@Override
	public BigDecimal getMaxRefundAmount(final OrderModel orderModel)
	{
		return refundCalculationService.calculateMaximumAvailableRefundAmountForOrder(orderModel);
	}
}