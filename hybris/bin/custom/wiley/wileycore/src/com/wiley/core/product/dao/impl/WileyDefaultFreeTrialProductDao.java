package com.wiley.core.product.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.product.dao.WileyFreeTrialProductDao;


/**
 * Created by Aliaksei_Zlobich on 3/16/2016.
 */
public class WileyDefaultFreeTrialProductDao extends DefaultGenericDao<WileyFreeTrialProductModel>
		implements WileyFreeTrialProductDao
{

	public WileyDefaultFreeTrialProductDao()
	{
		super(WileyFreeTrialProductModel._TYPECODE);
	}

	public WileyDefaultFreeTrialProductDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<WileyFreeTrialProductModel> findFreeTrialProductsByCode(final String code)
	{
		return find(Collections.singletonMap(WileyFreeTrialProductModel.CODE, code));
	}
}
