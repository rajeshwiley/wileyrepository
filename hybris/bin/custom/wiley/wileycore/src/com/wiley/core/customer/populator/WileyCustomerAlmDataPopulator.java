package com.wiley.core.customer.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.alm.user.dto.UserDto;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;



public class WileyCustomerAlmDataPopulator implements Populator<UserDto, CustomerModel>
{

	@Autowired
	private UserService userService;

	private static final String ALM_USERGROUP = "almusergroup";
	private static final String CUSTOMER_GROUP = "customergroup";

	@Override
	public void populate(final UserDto source, final CustomerModel target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("userDto", source);
		validateParameterNotNullStandardMessage("customerModel", target);

		target.setUid(source.getUserId());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setEmailAddress(source.getEmail());
		UserGroupModel almUserGroup = userService.getUserGroupForUID(ALM_USERGROUP);
		UserGroupModel customerGroup = userService.getUserGroupForUID(CUSTOMER_GROUP);
		target.setGroups(new HashSet<>(Arrays.asList(customerGroup, almUserGroup)));

	}
}
