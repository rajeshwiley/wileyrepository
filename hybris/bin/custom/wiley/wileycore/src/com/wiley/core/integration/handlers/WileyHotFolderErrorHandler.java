package com.wiley.core.integration.handlers;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchException;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupHelper;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.support.ErrorMessage;
import org.springframework.util.Assert;


/**
 * Custom implementation of OOTB
 * {@link de.hybris.platform.acceleratorservices.dataimport.batch.task.ErrorHandler ErrorHandler}.<br/>
 * In this implementation we skip common exception logging.
 */
public class WileyHotFolderErrorHandler
{

	@Resource
	private CleanupHelper cleanupHelper;

	/**
	 * Method handles error message. Checks if original exception is instance of {@link BatchException}
	 * and run clean up process for hot folder.
	 *
	 * @param errorMessage
	 * 		error message with exception in payload.
	 */
	public void onError(@Nonnull final ErrorMessage errorMessage)
	{
		Assert.notNull(errorMessage);
		Assert.notNull(errorMessage.getPayload());

		final Throwable messagingException = errorMessage.getPayload();
		final Throwable exception = messagingException.getCause(); // original exception

		if (exception instanceof BatchException)
		{
			cleanupHelper.cleanup(((BatchException) exception).getHeader(), true);
		}
	}

}
