package com.wiley.core.resolver.impl;

import de.hybris.platform.commerceservices.url.impl.AbstractUrlResolver;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.log4j.Logger;


/**
 * Created by Uladzimir_Barouski on 6/26/2017.
 */
public abstract class WileyAbstractUrlResolver<T> extends AbstractUrlResolver<T>
{
	private static final Logger LOG = Logger.getLogger(AbstractUrlResolver.class);

	/**
	 * OOTB_CODE move Cleanup logic from urlSafe to separate method
	 *
	 * @param text
	 * 		the text to sanitize
	 * @return
	 */
	@Override
	protected String urlSafe(final String text)
	{
		String encodedText = urlEncode(text);

		// Cleanup the text
		return urlCleanup(encodedText);
	}

	protected String urlEncode(final String text)
	{
		if (text == null || text.isEmpty())
		{
			return "";
		}

		String encodedText;
		try
		{
			encodedText = URLEncoder.encode(text, "utf-8");
		}
		catch (final UnsupportedEncodingException encodingException)
		{
			encodedText = text;
			LOG.debug(encodingException.getMessage(), encodingException);
		}
		return encodedText;
	}

	protected String urlCleanup(final String text)
	{
		String cleanedText = text;
		cleanedText = cleanedText.replaceAll("%2F", "/");
		cleanedText = cleanedText.replaceAll("[^%A-Za-z0-9]+", "+");
		return cleanedText;
	}
}
