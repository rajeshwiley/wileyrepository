package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import static com.wiley.core.enums.WileyProductLifecycleEnum.DISCONTINUED;
import static de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType.INDEX;
import static de.hybris.platform.solrfacetsearch.search.SearchQuery.QueryOperator.EQUAL_TO;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cProductLifecycleFilterListener implements FacetSearchListener
{
	static final String NOT_QUALIFIER = "-";
	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	private FieldNameProvider fieldNameProvider;

	private String lifecycleProperty;

	@Override
	public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{
		final IndexedType indexedType = facetSearchContext.getIndexedType();
		final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(lifecycleProperty);
		if (indexedProperty != null)
		{
			final String translatedField = fieldNameProvider.getFieldName(indexedProperty,
					commerceCommonI18NService.getCurrentLanguage().getIsocode(), INDEX);
			String field = NOT_QUALIFIER + translatedField;
			final QueryField lifecycleQuery = new QueryField(field, DISCONTINUED.toString());
			lifecycleQuery.setQueryOperator(EQUAL_TO);
			if (filterQueryNotPresent(facetSearchContext, field))
			{
				facetSearchContext.getSearchQuery().addFilterQuery(lifecycleQuery);
			}
		}
	}

	private boolean filterQueryNotPresent(final FacetSearchContext facetSearchContext, final String field)
	{
		return facetSearchContext.getSearchQuery().getFilterQueries().stream()
				.noneMatch(queryField -> queryField.getField().equals(field));
	}

	@Required
	public void setLifecycleProperty(final String lifecycleProperty)
	{
		this.lifecycleProperty = lifecycleProperty;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@Override
	public void afterSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}

	@Override
	public void afterSearchError(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}
}
