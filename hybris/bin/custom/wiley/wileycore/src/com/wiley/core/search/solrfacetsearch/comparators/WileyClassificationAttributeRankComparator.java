package com.wiley.core.search.solrfacetsearch.comparators;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.Comparator;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


public class WileyClassificationAttributeRankComparator implements Comparator<FacetValue>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyClassificationAttributeRankComparator.class);

	private String systemVersion;
	private String systemId;

	private ClassificationSystemService classificationSystemService;

	@Override
	public int compare(final FacetValue facetValue1, final FacetValue facetValue2)
	{
		LOG.debug("Comparing {} and {}", facetValue1.getName(), facetValue2.getName());
		final int result = ObjectUtils.compare(getValue(facetValue1), getValue(facetValue2));
		LOG.debug("The result of comparation between [{}] and [{}] is: [{}]", facetValue1.getName(), facetValue2.getName(),
				result);
		return result;
	}

	private Integer getValue(final FacetValue facetValue)
	{
		return getClassificationAttributeValueModel(facetValue.getName()).getRank();
	}

	private ClassificationAttributeValueModel getClassificationAttributeValueModel(final String classificationAttributeValueCode)
	{
		final ClassificationSystemVersionModel classificationSystemVersion = classificationSystemService.getSystemVersion(
				systemId, systemVersion);
		return classificationSystemService.getAttributeValueForCode(classificationSystemVersion,
				classificationAttributeValueCode);
	}

	public String getSystemVersion()
	{
		return systemVersion;
	}

	@Required
	public void setSystemVersion(final String systemVersion)
	{
		this.systemVersion = systemVersion;
	}

	public String getSystemId()
	{
		return systemId;
	}

	@Required
	public void setSystemId(final String systemId)
	{
		this.systemId = systemId;
	}

	public ClassificationSystemService getClassificationSystemService()
	{
		return classificationSystemService;
	}

	@Required
	public void setClassificationSystemService(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}
}
