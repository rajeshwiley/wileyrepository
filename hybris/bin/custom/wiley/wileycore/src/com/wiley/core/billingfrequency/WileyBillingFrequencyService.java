package com.wiley.core.billingfrequency;

import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;

import javax.annotation.Nonnull;


/**
 * Provides method for manipulating {@link BillingFrequencyModel}.
 */
public interface WileyBillingFrequencyService
{


	/**
	 * Returns BillingFrequency for code.
	 *
	 * @param code
	 * @return billingFrequency
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 * 		if there is no billing frequency for code
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 * 		if there are more then one billing frequencies for code
	 */
	@Nonnull
	BillingFrequencyModel getBillingFrequencyByCode(@Nonnull String code);

}
