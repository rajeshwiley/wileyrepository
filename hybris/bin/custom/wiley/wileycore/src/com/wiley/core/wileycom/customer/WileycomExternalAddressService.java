package com.wiley.core.wileycom.customer;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.wiley.core.exceptions.ExternalSystemException;


/**
 * Created by Uladzimir_Barouski on 7/11/2016.
 */
public interface WileycomExternalAddressService
{
	/**
	 * Sends address to ESB and in case of success save address to db.
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	void addShippingAddress(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault);

	/**
	 * Sends address to ESB and always save address to db
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	void addAddressFromCheckout(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault);

	/**
	 * Deletes address from ESB.
	 *
	 * @param customer
	 * @param address
	 */
	void deleteAddress(CustomerModel customer, AddressModel address);

	/**
	 * Updates address on ESB and if success save address to db.
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	void updateShippingAddress(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault);

	/**
	 * Updates address on ESB and if success save address to db from checkout.
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	void updateAddressFromCheckout(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault);

	/**
	 * Sends address to ESB.
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 * @throws ExternalSystemException
	 * @throws IllegalArgumentException
	 */
	void addBillingAddress(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault)
			throws ExternalSystemException, IllegalArgumentException;

	/**
	 * Sends address information to external system.
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	void updateBillingAddress(CustomerModel customer, AddressModel address, boolean makeThisAddressTheDefault);
}
