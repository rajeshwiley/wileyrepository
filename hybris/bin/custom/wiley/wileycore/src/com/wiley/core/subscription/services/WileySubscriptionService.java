/**
 *
 */
package com.wiley.core.subscription.services;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;



/**
 * Service for subscriptions.
 */
public interface WileySubscriptionService
{
	/**
	 * Retrieves subscription page for the current user
	 *
	 * @param pageableData
	 * @return
	 */
	SearchPageData<WileySubscriptionModel> getSubscriptionPage(PageableData pageableData);

	/**
	 * Service to get WileySubscription by code
	 *
	 * @param code
	 * @return
	 */
	WileySubscriptionModel getSubscriptionByCode(String code);


	/**
	 * Service to get WileySubscription  by customer and status
	 *
	 * @param customer
	 * @param status
	 * @return
	 */
	WileySubscriptionModel getSubscription(UserModel customer,
			SubscriptionStatus status);

	/**
	 * Create WileySubscriptionModel and save it
	 *
	 * @param orderEntry
	 * @return WileySubscriptionModel if orderEntry is instance of SubscriptionModel and WileySubscription is successfully saved.
	 * Null in other case
	 * @throws ModelSavingException
	 */
	WileySubscriptionModel createSubscriptionFromOrder(AbstractOrderEntryModel orderEntry) throws ModelSavingException;

	/**
	 * Checks the running/waiting renewal business process agains given subscription
	 *
	 * @param subscriptionCode
	 * 		Code of subscription to be checked
	 * @return True if there is progressing renewal process.
	 */
	boolean isSubscriptionInProgress(String subscriptionCode);

	/**
	 * Service to update WileySubscription autoRenew
	 *
	 * @param subscription
	 * @return
	 */
	WileySubscriptionModel updateAutoRenew(WileySubscriptionModel subscription);

	/**
	 * This methos sets given status to all subscription in given collection
	 *
	 * @param subscriptions
	 * @param newStatus
	 */
	void updateSubscriptionStatus(Collection<WileySubscriptionModel> subscriptions, SubscriptionStatus newStatus);

	/**
	 * This service returns all WileySubscriptions whose expiration time has come regardless renewal flags
	 *
	 * @return all WileySubscriptions whose expiration time has come regardless renewal flags
	 */
	@Nonnull
	List<WileySubscriptionModel> getAllExpiredSubscriptions();

	/**
	 * Method returns all wiley subscriptions which should be renewed on the moment of a method invocation.
	 *
	 * @return the all subscriptions which should be renewed on the moment of a method invocation.
	 */
	@Nonnull
	List<WileySubscriptionModel> getAllSubscriptionsWhichShouldBeRenewed();


	/**
	 * Method checks if any {@link de.hybris.platform.subscriptionservices.model.SubscriptionProductModel SubscriptionProduct}
	 * exists in a cart.
	 *
	 * @param cart
	 * 		the cart
	 * @return return true if cart has at least one any SubscriptionProduct.
	 */
	boolean doesAnySubscriptionProductExistInCart(@Nonnull CartModel cart);

	/**
	 * Replaces payment info item connected to given subscription.
	 * Method does real cloning of PaymentInfoItem, update value via external system and removed old paymentInfo if it isn't
	 * connected to something
	 *
	 * @param subscription
	 * 		target subscription
	 * @param newPaymentInfo
	 * 		paymentInfo item taken usually from account
	 */
	void updatePaymentInfo(WileySubscriptionModel subscription, PaymentInfoModel newPaymentInfo);

	/**
	 * Returns all payment transactions related to certain subscription
	 *
	 * @param pageableData
	 * @return list of transactions acceptable for pagination
	 */
	SearchPageData<WileySubscriptionPaymentTransactionModel> getSubscriptionBillingActivities(String subscriptionCode,
			PageableData pageableData);

	/**
	 * Search for subscriptions by its external code (identifier).
	 *
	 * @param externalCode
	 * 		The external universally unique identifier of the subscription. Expected to be provided by SAP ERP.
	 * @return internal codes of all found subscriptions.
	 */
	List<WileySubscriptionModel> getInternalCodesByExternalCode(@Nonnull String externalCode);

	/**
	 * Returns true if product has subscription subtype
	 * @param product
	 * @return
	 */
	boolean isProductSubscription(WileyProductModel product);

	boolean isSubscriptionCartEntry(AbstractOrderEntryModel orderEntry);

}
