package com.wiley.core.jobs.maintenance;

import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.impex.model.cronjob.ImpExExportCronJobModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.jobs.maintenance.impl.WileyCronJobRemoveUtil;


/**
 * Created by Raman_Hancharou on 4/14/2017.
 */
public class ImpExExportCronJobRemoveInterceptor implements RemoveInterceptor<ImpExExportCronJobModel>
{
	@Resource
	private ModelService modelService;

	@Value("${impex.doNotRemove.media}")
	private String doNotRemoveMedias;

	@Override
	public void onRemove(final ImpExExportCronJobModel job, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		Collection<Object> toRemove = new ArrayList<>();
		final ImpExMediaModel jobMedia = job.getJobMedia();
		if (jobMedia != null && WileyCronJobRemoveUtil.isRemovableMedia(jobMedia, doNotRemoveMedias))
		{
			toRemove.add(jobMedia);
		}
		if (!toRemove.isEmpty())
		{
			modelService.removeAll(toRemove);
		}
	}

}
