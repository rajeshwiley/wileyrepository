package com.wiley.core.mpgs.command.impl;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyCaptureCommand;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.response.WileyCaptureResponse;


public class WileyMPGSCaptureCommandImpl implements WileyCaptureCommand
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSCaptureCommandImpl.class);


	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyCaptureResponse perform(final WileyCaptureRequest captureRequest)
	{
		WileyCaptureResponse response;
		try
		{
			response = wileyMpgsPaymentGateway.capture(captureRequest);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to capture card due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyCaptureResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to capture payment due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyCaptureResponse::new);
		}
		return response;
	}
}