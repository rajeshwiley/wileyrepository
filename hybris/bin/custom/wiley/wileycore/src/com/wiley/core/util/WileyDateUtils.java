package com.wiley.core.util;

import com.google.common.base.Preconditions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public final class WileyDateUtils
{

	private WileyDateUtils() { }


	public static long getDateDiff(final Date date1, final Date date2, final TimeUnit timeUnit)
	{
		final long diffInMs = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMs, TimeUnit.MILLISECONDS);
	}

	/**
	 * Returns closest past LocalDateTime
	 * @param configuredTime - LocalTime instance
	 * @return - closest past LocalDateTime
	 */
	public static LocalDateTime getClosestPastLocalDateTime(final LocalTime configuredTime)
	{
		final LocalDate today = LocalDate.now();
		final LocalDateTime localDateTime = LocalDateTime.of(today, configuredTime);
		final LocalDate closestPastLocalDate;
		if (isFuture(localDateTime)) {
			closestPastLocalDate = today.minusDays(1);
		} else {
			closestPastLocalDate = today;
		}
		return LocalDateTime.of(closestPastLocalDate, configuredTime);
	}

	private static boolean isFuture(final LocalDateTime dateTime) {
		return dateTime.isAfter(LocalDateTime.now());
	}

	public static boolean pastDate(final LocalDateTime date, final DatePreciseMode preciseMode) {
		Preconditions.checkNotNull(date);
		final LocalDateTime currentDate = LocalDateTime.now();
		switch (preciseMode) {
			case ROUND_TO_DAY:
				return date.toLocalDate().isBefore(currentDate.toLocalDate());
			case PRECISE:
			default:
				return date.isBefore(currentDate);
		}
	}

	/**
	 * Converts dateString to date instance
	 * @param dateString - string to convert
	 * @param pattern - pattern
	 * @return - converted date, null if conversion problems occurs
	 * @throws ParseException - in case of parsing exception
	 */
	public static Date convertDateStringToDate(final String dateString, final String pattern) throws ParseException {
		Preconditions.checkNotNull(dateString);
		Preconditions.checkNotNull(pattern);
		final DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
		format.setLenient(false);
		return format.parse(dateString);
	}

	public static Date convertToDate(final LocalDateTime dateToConvert) {
		return java.util.Date
				.from(dateToConvert.atZone(ZoneId.systemDefault())
						.toInstant());
	}

	public enum DatePreciseMode {
		PRECISE,
		ROUND_TO_DAY
	}
}
