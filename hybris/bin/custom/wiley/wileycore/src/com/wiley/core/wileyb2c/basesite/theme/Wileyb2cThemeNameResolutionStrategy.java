package com.wiley.core.wileyb2c.basesite.theme;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;


/**
 * B2C strategy to resolve theme name, based on baseSite
 */
public interface Wileyb2cThemeNameResolutionStrategy
{
	/**
	 * Resolve theme name
	 *
	 * @return theme name
	 */
	String resolveThemeName(BaseSiteModel baseSite);

	/**
	 * Get default theme from configuration
	 *
	 * @return default theme name
	 */
	String getDefaultThemeName();
}
