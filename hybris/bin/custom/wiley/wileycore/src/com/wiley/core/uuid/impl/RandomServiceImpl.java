package com.wiley.core.uuid.impl;

import java.util.UUID;

import com.wiley.core.uuid.RandomService;


/**
 * Service for generating UIID. Could be easily replaced in tests by mock instance
 *
 * @author Dzmitryi_Halahayeu
 */
public class RandomServiceImpl implements RandomService
{


	@Override
	public String randomString()
	{
		return UUID.randomUUID().toString();
	}
}
