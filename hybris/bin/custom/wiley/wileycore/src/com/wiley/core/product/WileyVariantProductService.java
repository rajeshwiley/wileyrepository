package com.wiley.core.product;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Optional;
import com.wiley.core.model.WileyVariantProductModel;


/**
 * Service for working with {@link com.wiley.core.model.WileyVariantProductModel}.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyVariantProductService
{

	/**
	 * Returns the WileyVariantProduct with the specified ISBN belonging to the specified
	 * CatalogVersion.
	 *
	 * @param isbn
	 * 		the isbn.
	 * @param catalogVersion
	 * 		catalog version.
	 * @return the WileyVariantProduct matching the specified ISBN and CatalogVersion.
	 * @throws de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
	 * 		if no WileyVariantProduct with the specified ISBN and Catalog is found.
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 * 		if more than one WileyVariantProduct with the specified ISBN and Catalog is found
	 */
	Optional<WileyVariantProductModel> findProductByISBNIfExists(String isbn,
			CatalogVersionModel catalogVersion);

}
