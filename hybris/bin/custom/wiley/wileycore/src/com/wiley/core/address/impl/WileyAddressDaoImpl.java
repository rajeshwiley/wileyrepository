package com.wiley.core.address.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultAddressDao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.address.WileyAddressDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyAddressDaoImpl extends DefaultAddressDao implements WileyAddressDao
{
	private static final String FIND_BY_ID_QUERY = "SELECT {" + AddressModel.PK + "} FROM {" + AddressModel._TYPECODE + "} "
			+ "WHERE {" + AddressModel.PK + "} = ?addressId";

	private static final String FIND_BY_EXTERNAL_ID_QUERY =
			"SELECT {" + AddressModel.PK + "} FROM {" + AddressModel._TYPECODE + "} "
					+ "WHERE {" + AddressModel.EXTERNALID + "} = ?addressExternalId"
					+ " AND {" + AddressModel.OWNER + "} = ?ownerId";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<AddressModel> findAddressById(final String addressId)
	{
		Optional<AddressModel> addressModel = Optional.empty();
		if (StringUtils.isNotEmpty(addressId))
		{
			SearchResult<AddressModel> searchResult = flexibleSearchService.search(FIND_BY_ID_QUERY,
					Collections.singletonMap("addressId", addressId));
			addressModel = searchResult.getResult().stream().findFirst();
		}
		return addressModel;

	}

	@Override
	public Optional<AddressModel> findAddressByExternalIdAndOwner(final String addressExternalId, final String ownerId)
	{
		validateParameterNotNull(ownerId, "Owner ID must not be null!");

		Optional<AddressModel> addressModel = Optional.empty();
		if (StringUtils.isNotEmpty(addressExternalId))
		{
			final Map<String, Object> params = new HashMap<>();
			params.put("ownerId", ownerId);
			params.put("addressExternalId", addressExternalId);
			SearchResult<AddressModel> searchResult = flexibleSearchService.search(FIND_BY_EXTERNAL_ID_QUERY, params);
			addressModel = searchResult.getResult().stream().findFirst();
		}
		return addressModel;
	}
}
