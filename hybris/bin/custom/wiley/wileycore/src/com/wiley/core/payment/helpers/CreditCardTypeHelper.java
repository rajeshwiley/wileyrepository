package com.wiley.core.payment.helpers;

import de.hybris.platform.core.enums.CreditCardType;

import com.wiley.core.enums.CreditCardTypeCode;


/**
 * Contains helper methods related to credit card types
 */
public final class CreditCardTypeHelper
{
	private CreditCardTypeHelper()
	{
	}

	/**
	 * Returns CreditCardTypeCode for CreditCardType
	 *
	 * @param creditCardType
	 * 		- Credit card type code
	 * @return - Credit card type code or null if none match
	 */
	public static CreditCardTypeCode getCreditCardTypeCode(final CreditCardType creditCardType)
	{
		switch (creditCardType)
		{
			case AMEX:
				return CreditCardTypeCode.AX;
			case VISA:
				return CreditCardTypeCode.VS;
			case MASTER:
				return CreditCardTypeCode.MC;
			case DISCOVER:
				return CreditCardTypeCode.DS;
			case DINERS:
				return CreditCardTypeCode.DC;
			default:
				return null;
		}
	}
}
