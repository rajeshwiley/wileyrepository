package com.wiley.core.integration.invoice.service;

import javax.annotation.Nonnull;

import com.wiley.core.integration.invoice.dto.InvoiceDto;


public interface InvoiceService
{
	InvoiceDto getInvoice(@Nonnull String referenceId);
}
