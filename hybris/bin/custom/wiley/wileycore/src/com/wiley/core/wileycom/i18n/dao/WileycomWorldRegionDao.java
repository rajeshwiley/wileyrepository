package com.wiley.core.wileycom.i18n.dao;

import java.util.List;

import com.wiley.core.model.WorldRegionModel;


public interface WileycomWorldRegionDao
{
	/**
	 * Returns all world regions available in the system
	 *
	 * @return
	 */
	List<WorldRegionModel> findWorldRegions();
}
