package com.wiley.core.wileycom.util.date;

import java.util.Date;

import javax.validation.constraints.NotNull;


/**
 * Created by Georgii_Gavrysh on 6/21/2016.
 */
public interface WileycomDateService
{
	/**
	 *
	 * @param date
	 * @return number of calendar days from till date
	 */
	int daysNumberTillDate(@NotNull Date date);
}
