package com.wiley.core.payment.command;

import de.hybris.platform.payment.commands.SubscriptionAuthorizationCommand;
import de.hybris.platform.payment.commands.request.SubscriptionAuthorizationRequest;
import de.hybris.platform.payment.commands.result.AuthorizationResult;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;


public class WileyAuthorizeCommand implements SubscriptionAuthorizationCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPaymentCaptureCommand.class);
	@Autowired
	private WileyPaymentGateway wileyPaymentGateway;

	@Override
	public AuthorizationResult perform(final SubscriptionAuthorizationRequest request)
	{
		Preconditions.checkArgument(request instanceof WileySubscriptionAuthorizeRequest,
				"[request] param should instance of WileySubscriptionAuthorizeRequest");
		WileySubscriptionAuthorizeRequest wileyAuthorizeRequest = (WileySubscriptionAuthorizeRequest) request;
		WileyAuthorizeResult result;
		try
		{
			result = wileyPaymentGateway.authorizePayment(wileyAuthorizeRequest);
			result.setRequestToken(request.getSubscriptionID());

			if (result.getStatus() == WileyTransactionStatusEnum.SUCCESS
					&& (result.getTotalAmount() == null
					|| result.getTotalAmount().compareTo(wileyAuthorizeRequest.getTotalAmount()) != 0))
			{
				LOG.error("Authorized amount '{}' differs from requested amount '{}'.", result.getTotalAmount(),
						wileyAuthorizeRequest.getTotalAmount());
				result = createFailureResult(WileyTransactionStatusEnum.AMOUNT_MISMATCH);
			}
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to authorize payment due to unexpected error", ex);
			result = createFailureResult(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR);

		}
		return result;
	}

	private WileyAuthorizeResult createFailureResult(final WileyTransactionStatusEnum status)
	{
		WileyAuthorizeResult result = new WileyAuthorizeResult();
		result.setStatus(status);
		result.setTransactionStatus(TransactionStatus.ERROR);
		return result;
	}
}
