/**
 *
 */
package com.wiley.core.product.dao;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyProductModel;


/**
 *
 */
public interface WileyProductDao
{

	/**
	 * Returns for the given wileyProduct <code>code</code> the {@link WileyProductModel}
	 * collection.
	 *
	 * @param code
	 * 		the wileyProduct <code>code</code>
	 * @return a {@link WileyProductModel}
	 * @throws IllegalArgumentException
	 * 		if the given <code>code</code> is <code>null</code>
	 */
	List<WileyProductModel> findWileyProductsByCode(String code);

	/**
	 * Returns the wileyProduct list from the root of category.
	 *
	 * @param categoryCode
	 * 		the Category code
	 * @return the list of {@link WileyProductModel}
	 */
	List<WileyProductModel> findWileyProductsFromCategoryRoot(String categoryCode);

	/**
	 * Returns the products list from the root baseProductCategoryCode and variantValueCategoryCode
	 *
	 * @param baseProductCategoryCode
	 * 		category, base product should belong to
	 * @param variantValueCategoryCode
	 * 		variantValueCategory, product variant should belong to
	 * @return
	 */
	List<ProductModel> findWileyProductsFromCategories(String baseProductCategoryCode,
			String variantValueCategoryCode);

	/**
	 * Returns for the given {@link CategoryModel} all found
	 * {@link ProductModel}s.
	 */
	SearchResult<ProductModel> findProductsByCategoryExcludingSubCategories(CategoryModel categoryModel);

	/**
	 * Find {@code SubscriptionProductModel}s by isbn and term.
	 * However it returns a {@code List}, in typical cases it is a single result.
	 *
	 * @param isbn
	 * 		the isbn of given {@code SubscriptionProductModel}
	 * @param term
	 * 		the term of given {@code SubscriptionProductModel}
	 * @param catalogVersionModel
	 * 		the catalogVersionModel to lookup in
	 * @return the List of {@code SubscriptionProductModel} if found or null
	 */
	List<SubscriptionProductModel> findProductsByIsbnAndTerm(String isbn, String term, CatalogVersionModel catalogVersionModel);

	/**
	 * Find {@code ProductModel}s by isbn. However it returns a {@code List}, in typical cases it is a single result.
	 *
	 * @param isbn
	 * 		the isbn of given {@code ProductModel}
	 * @param catalogVersionModel
	 * 		the catalogVersionModel to lookup in
	 * @return the List of {@code ProductModel} if found or null
	 */
	List<ProductModel> findProductsByIsbn(String isbn, CatalogVersionModel catalogVersionModel);

	/**
	 * Find {@code ProductModel}s by isbn. However it returns a {@code List}, in typical cases it is a single result.
	 *
	 * @param isbn
	 * @return
	 */
	List<ProductModel> findProductsByIsbn(String isbn);


	@Nonnull
	List<ProductModel> findProductsToValidate(@Nonnull ArticleApprovalStatus approvalStatus,
			@Nonnull CatalogVersionModel catalogVersion);

	/**
	 * Find {@code ProductModel}s by mediaContainer in galleryImages.
	 *
	 * @param mediaContainer
	 * @return the list of {@link WileyProductModel}
	 */
	List<ProductModel> findProductsByMediaContainerInGalleryImages(MediaContainerModel mediaContainer);

	/**
	 * Find {@code ProductModel}s by MediaContainer in backgroundImage.
	 *
	 * @param mediaContainer
	 * @return the list of {@link WileyProductModel}
	 */
	List<ProductModel> findProductsByMediaContainerInBackgroudImage(MediaContainerModel mediaContainer);
}
