package com.wiley.core.wileycom.valuetranslator.adapter;

import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;


/**
 * This interdace use to delegate import logic to spring bean.
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomImportAdapter
{
	/**
	 * Import cell value for item
	 *
	 * @param cellValue
	 * 		cell value
	 * @param item
	 * 		importing item
	 * @throws UnresolvedValueException
	 * 		when value could be processed and line should be dumped
	 */
	void performImport(String cellValue, Item item)
			throws UnresolvedValueException;

	/**
	 * Import set catalog to adapter
	 *
	 * @param catalogVersion
	 * 		catalog version
	 * @param catalogId
	 * 		catalog id
	 */
	void setCatalog(String catalogVersion, String catalogId);
}
