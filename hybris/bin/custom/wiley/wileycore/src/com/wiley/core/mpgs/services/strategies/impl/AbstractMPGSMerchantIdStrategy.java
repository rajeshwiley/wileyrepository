package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;


public abstract class AbstractMPGSMerchantIdStrategy implements WileyMPGSMerchantIdStrategy
{
	@Resource
	private ConfigurationService configurationService;

	@Override
	public String getHostedSessionScriptUrl(final String merchantId)
	{
		final String urlTemplate = configurationService.getConfiguration().getString(
				WileyCoreConstants.PAYMENT_MPGS_HOSTED_SESSION_SCRIPT_URL_TEMPLATE);

		return String.format(urlTemplate, merchantId);
	}
}