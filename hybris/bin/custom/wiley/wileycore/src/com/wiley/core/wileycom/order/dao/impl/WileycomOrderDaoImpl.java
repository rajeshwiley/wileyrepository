package com.wiley.core.wileycom.order.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.daos.impl.DefaultOrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import com.wiley.core.wileycom.order.dao.WileycomOrderDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileycomOrderDaoImpl extends DefaultOrderDao implements WileycomOrderDao
{
	private static final String FIND_ORDERS_BY_CODE = "SELECT {" + OrderModel.PK + "}, {" + OrderModel.CREATIONTIME
			+ "}, {" + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + "} WHERE {" + OrderModel.CODE + "} = ?code AND  {"
			+ OrderModel.VERSIONID + "} IS NULL";

	private static final String QUERY_FIND_LAST_ORDER = "SELECT {" + OrderModel.PK + "}"
			+ " FROM {" + OrderModel._TYPECODE + "}"
			+ " WHERE {" + OrderModel.USER + "} = ?customer"
			+ " AND {" + OrderModel.STORE + "} = ?store"
			+ " ORDER BY {creationtime} DESC";

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public OrderModel findOrdersByCode(@NotNull final String orderCode)
	{
		validateParameterNotNull(orderCode, "GUID must not be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("code", orderCode);

		final List<OrderModel> result = getFlexibleSearchService().<OrderModel> search(
				new FlexibleSearchQuery(FIND_ORDERS_BY_CODE, queryParams)).getResult();

		ServicesUtil.validateIfSingleResult(result, String.format("Could not find Order for code [%s]", orderCode),
				String.format("There are more then one Order with code [%s]", orderCode));

		return result.get(0);
	}

	@Override
	public Optional<OrderModel> getLastOrderForCustomer(final CustomerModel customerModel)
	{
		Assert.notNull(customerModel);

		Optional<OrderModel> result = Optional.empty();

		final Map<String, Object> params = new HashMap<>();
		params.put("customer", customerModel);
		params.put("store", baseStoreService.getCurrentBaseStore());

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_LAST_ORDER, params);
		query.setCount(1);

		final SearchResult<OrderModel> searchResult = getFlexibleSearchService().search(query);

		if (searchResult.getTotalCount() > 0)
		{
			result = Optional.of(searchResult.getResult().get(0));
		}

		return result;
	}
}
