package com.wiley.core.wiley.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;

public class WileyProductRestrictionServiceImpl implements WileyProductRestrictionService
{
	@Autowired
	private BaseStoreService baseStoreService;
	@Autowired
	private ApplicationContext applicationContext;

	private static final Logger LOG = LoggerFactory.getLogger(WileyProductRestrictionServiceImpl.class);

	@Nonnull
	@Override
	public WileyRestrictionCheckResultDto isAvailable(@Nonnull final CommerceCartParameter parameter)
	{
		final Set<WileyRestrictProductStrategy> availabilityStrategies = getProductRestrictionStrategySet(
				baseStoreService.getCurrentBaseStore().getProductAvailabilityStrategies());
		for (final WileyRestrictProductStrategy strategy : availabilityStrategies)
		{
			if (strategy.isRestricted(parameter))
			{
				return strategy.createErrorResult(parameter);
			}
		}
		return WileyRestrictionCheckResultDto.successfulResult();
	}

	@Nonnull
	@Override
	public WileyRestrictionCheckResultDto isVisible(@Nonnull final CommerceCartParameter parameter)
	{
		final Set<WileyRestrictProductStrategy> visibilityStrategies = getProductRestrictionStrategySet(
				baseStoreService.getCurrentBaseStore().getProductVisibilityStrategies());
		for (final WileyRestrictProductStrategy strategy : visibilityStrategies)
		{
			if (strategy.isRestricted(parameter))
			{
				return strategy.createErrorResult(parameter);
			}
		}
		return WileyRestrictionCheckResultDto.successfulResult();
	}

	@Nonnull
	@Override
	public boolean isSearchable(@Nonnull final ProductModel productModel)
	{
		Assert.notNull(productModel);
		final Set<WileyRestrictProductStrategy> searchabilityStrategies = getProductRestrictionStrategySet(
				baseStoreService.getCurrentBaseStore().getProductSearchabilityStrategies());
		for (final WileyRestrictProductStrategy strategy : searchabilityStrategies)
		{
			if (strategy.isRestricted(productModel))
			{
				return false;
			}
		}
		return true;
	}

	@Nonnull
	@Override
	public boolean isAvailable(@Nonnull final ProductModel product)
	{
		final Set<WileyRestrictProductStrategy> availabilityStrategies = getProductRestrictionStrategySet(
				baseStoreService.getCurrentBaseStore().getProductAvailabilityStrategies());
		for (final WileyRestrictProductStrategy strategy : availabilityStrategies)
		{
			if (strategy.isRestricted(product))
			{
				return false;
			}
		}
		return true;
	}

	@Nonnull
	@Override
	public boolean isVisible(@Nonnull final ProductModel product)
	{
		final Set<WileyRestrictProductStrategy> visibilityStrategies = getProductRestrictionStrategySet(
				baseStoreService.getCurrentBaseStore().getProductVisibilityStrategies());
		for (final WileyRestrictProductStrategy strategy : visibilityStrategies)
		{
			if (strategy.isRestricted(product))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Pay attention product must not be saved after variant filtering
	 *
	 * @param product
	 * @return product with filtered variants
	 */
	@Override
	public ProductModel filterRestrictedProductVariants(@Nonnull final ProductModel product)
	{
		final List<VariantProductModel> visibleVariants = product.getVariants().stream()
				.filter(variant -> isVisible(variant)).collect(Collectors.toList());
		product.setVariants(visibleVariants);
		return product;
	}

	private Set<WileyRestrictProductStrategy> getProductRestrictionStrategySet(final Set<String> strategies)
	{
		final Set<WileyRestrictProductStrategy> strategyBeanSet = new HashSet<>();
		if (strategies != null && !strategies.isEmpty())
		{
			for (final String strategy : strategies)
			{
				try
				{
					final WileyRestrictProductStrategy restrictionstrategy = applicationContext.getBean(strategy,
							WileyRestrictProductStrategy.class);
					strategyBeanSet.add(restrictionstrategy);
				}
				catch (final NoSuchBeanDefinitionException e)
				{
					LOG.error(String.format("Bean with name [%s] does not exist.", strategy), e);
				}
				catch (final BeanNotOfRequiredTypeException e)
				{
					LOG.error(String.format("Bean with name [%s] is not of type WileyRestrictProductStrategy.", strategy), e);
				}
			}
		}
		return strategyBeanSet;
	}

}
