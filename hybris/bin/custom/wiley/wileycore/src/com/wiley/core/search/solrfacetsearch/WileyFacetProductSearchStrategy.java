package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.solr.Index;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFacetProductSearchStrategy extends WileyFacetSearchStrategy
{
	private WileySearchVariantsHelper wileySearchVariantsHelper;

	@Required
	public void setWileySearchVariantsHelper(final WileySearchVariantsHelper wileySearchVariantsHelper)
	{
		this.wileySearchVariantsHelper = wileySearchVariantsHelper;
	}

	@Override
	protected SearchResult postProcessSearchResult(final SolrClient solrClient, final FacetSearchContext facetSearchContext,
			final Index index,
			final SearchResult searchResult)
			throws FacetSearchException, SolrServerException, IOException
	{
		wileySearchVariantsHelper.queryForVariants(solrClient, index, searchResult,
				facetSearchContext.getSearchQuery());

		return searchResult;
	}
}
