package com.wiley.core.regCode.service.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;


public class RegcodeCartValidationStrategyImpl implements CartValidationStrategy
{
	@Override
	public List<CommerceCartModification> validateCart(final CommerceCartParameter parameter)
	{
		if (parameter.getCart() != null)
		{
			validateUpdatedCart(parameter.getCart());
			if (StringUtils.isNotEmpty(parameter.getProductCodeForRegcode()))
			{
				validateUpdatedCart(parameter.getCart(), parameter.getProductCodeForRegcode());
			}
			return Collections.emptyList();
		}
		throw new RegCodeValidationException("Registration code cannot be processed");
	}

	private void validateUpdatedCart(final CartModel cart)
	{
		if (cart.getEntries() == null || cart.getEntries().size() != 1)
		{
			throw new RegCodeValidationException("Registration code cannot be processed");
		}
	}

	private void validateUpdatedCart(final CartModel cart, final String productCode)
	{
		validateUpdatedCart(cart);
		if (cart.getEntries().get(0).getProduct() == null || !productCode.equals(cart.getEntries().get(0).getProduct()
				.getCode()))
		{
			throw new RegCodeInvalidException("Registration code doesn't match to passed registration code");
		}
	}

	@Override
	@Deprecated
	public List<CommerceCartModification> validateCart(final CartModel cartModel)
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		return this.validateCart(parameter);
	}

}
