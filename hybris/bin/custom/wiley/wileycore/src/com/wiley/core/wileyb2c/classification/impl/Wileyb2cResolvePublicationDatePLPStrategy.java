package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Set;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * Created by Uladzimir_Barouski on 3/15/2017.
 */
public class Wileyb2cResolvePublicationDatePLPStrategy  extends AbstractWileyb2cResolveClassificationAttributeStrategy
{
	public static final String DATE_PATTERN = "MM/dd/yy";
	static final Set<Wileyb2cClassificationAttributes> CLASSIFICATION_ATTRIBUTES =
			Collections.singleton(Wileyb2cClassificationAttributes.PUBLICATION_DATE);

	@Override
	protected String processFeature(final Feature feature)
	{
		return new SimpleDateFormat(DATE_PATTERN).format(feature.getValue().getValue());
	}

	@Override
	protected Set<Wileyb2cClassificationAttributes> getAttributes()
	{
		return CLASSIFICATION_ATTRIBUTES;
	}

}
