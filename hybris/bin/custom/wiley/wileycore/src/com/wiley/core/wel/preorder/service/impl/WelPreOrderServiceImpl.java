package com.wiley.core.wel.preorder.service.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.dao.WelProductReferencesDao;
import com.wiley.core.wel.preorder.service.WelPreOrderService;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.lang.Validate.isTrue;


public class WelPreOrderServiceImpl implements WelPreOrderService
{
	@Autowired
	private WelProductReferencesDao welProductReferencesDao;

	@Resource(name = "welProductLifecycleStatusStrategy")
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;

	@Override
	public Optional<ProductModel> getActiveProductForPreOrderProduct(final ProductModel preOrderProduct)
	{
		validateParameterNotNull(preOrderProduct, "preOrderProduct must not be null!");
		isTrue(Objects.equals(preOrderProduct.getLifecycleStatus(), WileyProductLifecycleEnum.PRE_ORDER),
				"product must be pre-order product");

		final List<ProductModel> activeProducts = welProductReferencesDao.findSourceReferenceProducts(
				preOrderProduct, ProductReferenceTypeEnum.NEXT_VERSION);
		//Every pre-order product has only one active product
		return CollectionUtils.isEmpty(activeProducts) ? Optional.empty() : Optional.of(activeProducts.get(0));
	}

	@Override
	public boolean isPreOrder(final AbstractOrderModel abstractOrder)
	{
		validateParameterNotNull(abstractOrder, "abstractOrder must not be null!");
		List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		if (entries.isEmpty())
		{
			return false;
		}
		ProductModel product = entries.get(0).getProduct();
		WileyProductLifecycleEnum lifecycleStatus = welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(product);

		return WileyProductLifecycleEnum.PRE_ORDER.equals(lifecycleStatus);

	}

}
