package com.wiley.core.servicelayer.dao.processdefinition.impl;

import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.servicelayer.dao.processdefinition.WileyProcessDefinitionDao;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyProcessDefinitionDaoImpl implements WileyProcessDefinitionDao
{
	private static final String ALL_DEFINITIONS_QUERY = "select {pk} from {DynamicProcessDefinition} order by {code},{version}";
	private static final String DEFINITIONS_BY_CODE_QUERY =
			"select {pk} from {DynamicProcessDefinition} where {code}=?code order by {code},{version}";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<DynamicProcessDefinitionModel> findAllProcessDefinitions()
	{
		return flexibleSearchService.<DynamicProcessDefinitionModel>search(ALL_DEFINITIONS_QUERY).getResult();
	}

	@Override
	public List<DynamicProcessDefinitionModel> findProcessDefinitionsByCode(final String code)
	{
		return flexibleSearchService
				.<DynamicProcessDefinitionModel>search(DEFINITIONS_BY_CODE_QUERY, Collections.singletonMap("code", code))
				.getResult();
	}
}
