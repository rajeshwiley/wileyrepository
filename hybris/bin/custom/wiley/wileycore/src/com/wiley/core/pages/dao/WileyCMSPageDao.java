package com.wiley.core.pages.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.Collection;
import java.util.List;


public interface WileyCMSPageDao {

  Collection<AbstractPageModel> findAllPagesByCategoryCode(String typeCode,
      Collection<CatalogVersionModel> versions, String categoryCode);

  List<AbstractPageModel> findPagesByComponent(AbstractCMSComponentModel componentModel);
}
