package com.wiley.core.externaltax.xml.caching.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.region.CacheRegion;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.xml.caching.TaxServiceCacheAccess;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;


public class TaxServiceCacheAccessImpl implements TaxServiceCacheAccess
{
	private String tenantId;
	private CacheRegion cacheRegion;


	@PostConstruct
	public void init()
	{
		this.tenantId = Registry.getCurrentTenantNoFallback().getTenantID();
	}

	@Override
	public Optional<Double> get(final TaxServiceRequest taxServiceRequest)
	{
		final TaxServiceCacheKey key = new TaxServiceCacheKey(taxServiceRequest, tenantId);

		Double taxServiceResponse = (Double) cacheRegion.get(key);
		return Optional.ofNullable(taxServiceResponse);
	}

	@Override
	public void put(final TaxServiceRequest taxServiceRequest, final Double taxAmount)
	{
		final TaxServiceCacheKey key = new TaxServiceCacheKey(taxServiceRequest, tenantId);
		cacheRegion.getWithLoader(key, new TaxServiceSimpleCacheValueLoader(taxAmount));
	}

	@Required
	public void setCacheRegion(final CacheRegion cacheRegion)
	{
		this.cacheRegion = cacheRegion;
	}
}
