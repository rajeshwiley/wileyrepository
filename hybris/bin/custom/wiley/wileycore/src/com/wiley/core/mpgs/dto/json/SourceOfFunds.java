package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class SourceOfFunds
{
	private String type;
	private Provided provided;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public Provided getProvided()
	{
		return provided;
	}

	public void setProvided(final Provided provided)
	{
		this.provided = provided;
	}
}
