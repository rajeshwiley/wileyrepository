package com.wiley.core.price.strategy;

import de.hybris.platform.europe1.model.PriceRowModel;

import java.util.Collection;


public interface EffectiveDatesPriceReplacementStrategy
{
	void replacePrices(Collection<PriceRowModel> prices, PriceRowModel price);
}
