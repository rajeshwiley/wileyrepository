package com.wiley.core.mpgs.constants;

public interface WileyMPGSConstants
{
	String MPGS_PAYMENT_PROVIDER = "MPGS";
	String SOURCE_OF_FUNDS_TYPE = "CARD";
	String PAYMENT_MPGS_VENDOR_ID = "payment.mpgs.vendor.id";

	//Error Handling
	String MESSAGE_KEY_ISSUE_COMMON_MESSAGE = "mpgs.checkout.result.common.issue";

	String HYBRIS_EXCEPTION = "HYBRIS_EXCEPTION";

	String RETURN_ORDER_STATUS_AUTHORIZED = "AUTHORIZED";

	// MPSGS Statuses
	String RETURN_STATUS_SUCCESS = "SUCCESS";
	String RETURN_STATUS_ERROR = "ERROR";
	String RETURN_STATUS_FAILURE = "FAILURE";
	String RETURN_STATUS_UNKNOWN = "UNKNOWN";

	//MPGS api operation
	String API_OPERATION_AUTHORIZE = "AUTHORIZE";
	String API_OPERATION_CAPTURE = "CAPTURE";
	String API_OPERATAION_REFUND = "REFUND";
	String API_OPERATAION_VOID = "VOID";
	String API_OPERATAION_VERIFY = "VERIFY";
}
