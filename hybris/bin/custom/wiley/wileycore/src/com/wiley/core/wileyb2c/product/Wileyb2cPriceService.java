package com.wiley.core.wileyb2c.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.List;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public interface Wileyb2cPriceService extends PriceService
{
	List<PriceInformation> findPricesByProductAndSubscriptionTerm(ProductModel productModel,
			SubscriptionTermModel subscriptionTerm);
}
