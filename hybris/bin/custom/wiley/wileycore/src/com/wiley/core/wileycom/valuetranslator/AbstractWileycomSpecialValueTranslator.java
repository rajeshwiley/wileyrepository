package com.wiley.core.wileycom.valuetranslator;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.impex.jalo.translators.SpecialValueTranslator;
import de.hybris.platform.jalo.Item;

import org.apache.commons.lang.StringUtils;


/**
 * Implementation of {@link AbstractSpecialValueTranslator} for adding restriction to
 * {@link de.hybris.platform.voucher.model.PromotionVoucherModel} in  convenient way.
 * We use {@link SpecialValueTranslator} because we don't have separate attribute for
 * this restriction because OOTB all restrictions are stored together using one-to-many relation.
 * This class delegate logic of import to adapter that injected using Spring
 *
 * @author Dzmitryi_Halahayeu
 */
public abstract class AbstractWileycomSpecialValueTranslator<T> extends AbstractSpecialValueTranslator
{
	private static final String MODIFIER_NAME_ADAPTER = "adapter";
	private T wileycomImportAdapter;

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		String beanName = columnDescriptor.getDescriptorData().getModifier(MODIFIER_NAME_ADAPTER);

		if (StringUtils.isBlank(beanName))
		{
			beanName = getDefaultImportAdapterName();
		}
		wileycomImportAdapter = (T) Registry.getApplicationContext().getBean(beanName);
	}

	public T getWileycomImportAdapter()
	{
		return wileycomImportAdapter;
	}

	protected abstract String getDefaultImportAdapterName();

	@Override
	public String performExport(final Item item) throws ImpExException
	{
		return super.performExport(item);
	}

	@Override
	public boolean isEmpty(final String cellValue)
	{
		return super.isEmpty(cellValue) || cellValue.startsWith("<ignore>");
	}
}
