package com.wiley.core.wileyb2c.subscription.services;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import com.wiley.core.model.WileyProductModel;


/**
 * Service for FreeTrial related checkout functionality
 */
public interface Wileyb2cFreeTrialService
{
	/**
	 * Checks if product is subscription and has free trial term
	 * @param product
	 * @return
	 */
	boolean isProductFreeTrial(WileyProductModel product);

	/**
	 * Returns FreeTrial subscription term for product or throws IllegalArgumentException if there is no FreeTrial term
	 *
	 * @param productModel - product
	 * @return - FreeTrial SubscriptionTerm for product
	 */
	SubscriptionTermModel getFreeTrialForProduct(WileyProductModel productModel);

	/**
	 * Checks if order entry is purposed to purchase free trial product
	 * @param orderEntry - order entry
	 * @return - true if entry is proper for purchasing free trial product
	 */
	boolean isFreeTrialEntry(AbstractOrderEntryModel orderEntry);
}
