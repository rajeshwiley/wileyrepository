package com.wiley.core.mpgs.services;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.payment.WileyPaymentService;


public interface WileyMPGSPaymentService extends WileyPaymentService
{
	/**
	 * Request for the gateway to retrieve payment session
	 *
	 * @param cart
	 * @param sessionId
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel retrieveSession(@Nonnull AbstractOrderModel cart, @Nonnull String sessionId);

	/**
	 * Request for the gateway to verify
	 *
	 * @param cart
	 * @param sessionId
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel verify(@Nonnull AbstractOrderModel cart, @Nonnull String sessionId);

	/**
	 * Request for the gateway to store payment instrument (e.g. credit or debit cards) against a token
	 *
	 * @param retrieveSessionTransactionEntry
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel tokenizeCart(@Nonnull CartModel cart,
			@Nonnull PaymentTransactionEntryModel retrieveSessionTransactionEntry,	Boolean saveInAccount);

	/**
	 * Request for the gateway to store payment instrument (e.g. credit or debit cards) against a token
	 *
	 * @param retrieveSessionTransactionEntry
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel tokenizeOrder(@Nonnull OrderModel order,
			@Nonnull PaymentTransactionEntryModel retrieveSessionTransactionEntry,	Boolean saveInAccount);


	/**
	 * Request for the gateway to authorization
	 *
	 * @param abstractOrder
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel authorize(@Nonnull AbstractOrderModel abstractOrder);


	/**
	 * Request to capture funds previously reserved by an authorization
	 *
	 * @param transaction
	 * @return PaymentTransactionEntryModel
	 */
	Optional<PaymentTransactionEntryModel> capture(@Nonnull PaymentTransactionModel transaction);


	/**
	 * Request for the gateway to make a refund
	 *
	 * @param paymentTransaction
	 * @param refundAmount
	 * @return PaymentTransactionEntryModel
	 */
	PaymentTransactionEntryModel refundFollowOn(@Nonnull PaymentTransactionModel paymentTransaction,
			@Nonnull BigDecimal refundAmount);

}
