package com.wiley.core.externaltax.strategies;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Optional;


public interface WileyTaxCodeStrategy
{
	Optional<String> getTaxCode(AbstractOrderEntryModel entry);
}
