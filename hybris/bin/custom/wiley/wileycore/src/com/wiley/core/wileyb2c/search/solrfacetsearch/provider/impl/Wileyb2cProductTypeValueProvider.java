/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 */
public class Wileyb2cProductTypeValueProvider extends AbstractWileycomValueProvider<String>
{
	public static final String TEXTBOOK_LABEL = "productType.textbook";
	public static final String COURSES_MARKER = "productType.course";
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		final WileyPurchaseOptionProductModel productModel = (WileyPurchaseOptionProductModel) model;
		final List<String> productTypes = new ArrayList<>();
		productTypes.add(wileyb2cClassificationService.resolveClassificationClass(productModel).getName());
		if (Boolean.TRUE.equals(productModel.getBaseProduct().getTextbook()))
		{
			productTypes.add(Localization.getLocalizedString(TEXTBOOK_LABEL));
		}

		Collection<CategoryModel> courses = productModel.getBaseProduct().getCourses();
		if (courses != null && courses.size() > 0)
		{
			productTypes.add(Localization.getLocalizedString(COURSES_MARKER));
		}

		return productTypes;
	}

	@Required
	public void setWileyb2cClassificationService(
			final Wileyb2cClassificationService wileyb2cClassificationService)
	{
		this.wileyb2cClassificationService = wileyb2cClassificationService;
	}
}
