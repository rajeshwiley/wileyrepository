package com.wiley.core.event.facade;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;


/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class WileyTwoParametersFacadeEventWithoutResult<PARAM1 extends Serializable, PARAM2 extends Serializable>
        extends AbstractEvent
{
    private PARAM1 firstParameter;
    private PARAM2 secondParameter;

    public WileyTwoParametersFacadeEventWithoutResult(final PARAM1 firstParameter, final PARAM2 secondParameter) {
        this.firstParameter = firstParameter;
        this.secondParameter = secondParameter;
    }

    public PARAM1 getFirstParameter() {
        return firstParameter;
    }

    public void setFirstParameter(final PARAM1 firstParameter) {
        this.firstParameter = firstParameter;
    }

    public PARAM2 getSecondParameter() {
        return secondParameter;
    }

    public void setSecondParameter(final PARAM2 secondParameter) {
        this.secondParameter = secondParameter;
    }
}
