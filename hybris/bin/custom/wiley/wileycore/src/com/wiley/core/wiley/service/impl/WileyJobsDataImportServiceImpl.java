package com.wiley.core.wiley.service.impl;

import com.wiley.core.wiley.service.WileyJobsDataImportService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.TriggerModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.function.Function;

public class WileyJobsDataImportServiceImpl implements WileyJobsDataImportService
{
    private static final Logger LOG = LoggerFactory.getLogger(WileyJobsDataImportServiceImpl.class);
    private static final String ACTIVE_ATTRIBUTE = "active";

    public enum ExecutionType { JOB, CRON_JOB }
    public enum ExecutionStateAction { ACTIVATE, DEACTIVATE }

    @Resource
    private CronJobService cronJobService;

    @Resource
    private ModelService modelService;

    @Override
    public boolean deactivateCronJob(final String cronJobCode)
    {
        toggleTriggersState(cronJobCode, ExecutionType.CRON_JOB, ExecutionStateAction.DEACTIVATE);
        return setActiveItem(cronJobCode, cronJobService::getCronJob, false);
    }

    @Override
    public boolean activateCronJob(final String cronJobCode, final boolean activateTriggers)
    {
        if (activateTriggers) {
            toggleTriggersState(cronJobCode, ExecutionType.CRON_JOB, ExecutionStateAction.ACTIVATE);
        }
        return setActiveItem(cronJobCode, cronJobService::getCronJob, true);
    }

    @Override
    public boolean activateJob(final String jobCode)
    {
        toggleTriggersState(jobCode, ExecutionType.JOB, ExecutionStateAction.ACTIVATE);
        return setActiveItem(jobCode, cronJobService::getJob, true);

    }

    @Override
    public boolean deactivateJob(final String jobCode)
    {
        toggleTriggersState(jobCode, ExecutionType.JOB, ExecutionStateAction.DEACTIVATE);
        return setActiveItem(jobCode, cronJobService::getJob, false);
    }

    @Override
    public boolean executeCronJob(final String cronJobCode, final boolean activateTriggers)
    {
        activateCronJob(cronJobCode, false);
        try
        {
            executeCronJob(cronJobCode);
            if (activateTriggers)
            {
                toggleTriggersState(cronJobCode, ExecutionType.CRON_JOB, ExecutionStateAction.ACTIVATE);
            }
            return true;
        }
        catch (UnknownIdentifierException e)
        {
            LOG.info("CronJob execution with code={} was skipped as not found", cronJobCode);
            return false;
        }
    }

    private void executeCronJob(final String cronJobCode)
    {
        CronJobModel cronJob = cronJobService.getCronJob(cronJobCode);
        cronJobService.performCronJob(cronJob, true);
        LOG.info("CronJob with code={} was executed", cronJobCode);
    }

    private void toggleTriggersState(final String code, final ExecutionType type, final ExecutionStateAction action)
    {
        try
        {
            Collection<TriggerModel> triggers = null;
            if (type == ExecutionType.JOB)
            {
                triggers = cronJobService.getJob(code).getTriggers();
            }
            else if (ExecutionType.CRON_JOB == type)
            {
                triggers = cronJobService.getCronJob(code).getTriggers();
            }
            if (CollectionUtils.isNotEmpty(triggers))
            {
                boolean setActive = ExecutionStateAction.ACTIVATE == action;
                triggers
                        .stream()
                        .filter(trigger -> trigger.getActive() != setActive)
                        .forEach(trigger -> setActiveItem(null, id -> trigger, setActive));
            }
        }
        catch (UnknownIdentifierException e)
        {
            LOG.info("Triggers for Item with code={} was skipped due to wasn't found", code);
        }
    }

    /**
     * Set active attribute for model
     *
     * @param id
     * 		Item identificator
     * @param itemRetrieval
     * 		Function to get item
     * @param isActive
     * 		Flag value
     * @return True if item was found and updated, otherwise false
     */
    private boolean setActiveItem(final String id, final Function<String, ItemModel> itemRetrieval, final boolean isActive)
    {
        try
        {
            ItemModel item = itemRetrieval.apply(id);
            modelService.setAttributeValue(item, ACTIVE_ATTRIBUTE, isActive);
            modelService.save(item);
            LOG.info("Item with id={} was set to active={}", id, isActive);
            return true;
        }
        catch (UnknownIdentifierException e)
        {
            LOG.info("Item with id={} was skipped due to wasn't found", id);
            return false;
        }
    }
}