package com.wiley.core.users.strategy.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.authentication.strategies.WileyPasswordCheckingStrategy;
import com.wiley.core.customer.service.impl.WileyCustomerAccountServiceImpl;
import com.wiley.core.users.strategy.WileycomChangeUserUidStrategy;
import com.wiley.core.wileycom.users.service.impl.WileycomUsersServiceImpl;


public class WileycomChangeUserUidStrategyImpl implements WileycomChangeUserUidStrategy
{

	@Autowired
	private WileyCustomerAccountServiceImpl wileyCustomerAccountService;
	@Autowired
	private UserService userService;
	@Autowired
	private WileycomUsersServiceImpl wileycomUsersService;
	@Autowired
	private WileyPasswordCheckingStrategy passwordCheckingStrategy;
	@Autowired
	private ModelService modelService;

	@Override
	public void changeUid(final String newUid, final String currentPassword)
			throws DuplicateUidException, PasswordMismatchException
	{
		final CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();

		if (passwordCheckingStrategy.checkPassword(modelService.toPersistenceLayer(currentUser), currentPassword)) {
			changeUid(currentUser, newUid, currentPassword);
		}
		else {
			throw new PasswordMismatchException("Incorrect password entered during update uid for customer: ["
					+ currentUser.getUid() + "], PK: [" + currentUser.getPk() + "]");
		}
	}

	@Override
	public void changeUid(final CustomerModel currentUser, final String newUid, final String password)
			throws DuplicateUidException
	{
		final String newUidLower = newUid.toLowerCase();
		if (wileycomUsersService.updateUserId(currentUser, newUidLower, password)) {
			updateUidInternally(newUidLower, currentUser);
		}

	}

	private void updateUidInternally(final String newUid, final CustomerModel currentUser) throws DuplicateUidException
	{
		currentUser.setOriginalUid(newUid);
		// check uniqueness only if the uids are case insensitive different
		if (!currentUser.getUid().equalsIgnoreCase(newUid))
		{
			wileyCustomerAccountService.checkUidUniqueness(newUid);
		}
		currentUser.setUid(newUid);
		modelService.save(currentUser);
	}
}
