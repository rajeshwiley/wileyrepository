package com.wiley.core.delivery.impl;

import de.hybris.platform.commerceservices.delivery.impl.DefaultDeliveryService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.jalo.order.delivery.JaloDeliveryModeException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.util.PriceValue;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.delivery.WileyCountryZoneDeliveryModeDao;
import com.wiley.core.delivery.WileyDeliveryService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyDeliveryServiceImpl extends DefaultDeliveryService implements WileyDeliveryService
{
	private static final Logger LOG = Logger.getLogger(WileyDeliveryService.class);

	@Autowired
	private WileyCountryZoneDeliveryModeDao countryZoneDeliveryModeDao;

	@Override
	public List<DeliveryModeModel> getSupportedDeliveryModes(final BaseStoreModel baseStore, final CurrencyModel currencyModel,
			final CountryModel countryModel)
	{
		List<DeliveryModeModel> deliveryModeModels =
				countryZoneDeliveryModeDao.getSupportedDeliveryModes(baseStore, currencyModel, countryModel);
		return deliveryModeModels != null ? deliveryModeModels : Collections.emptyList();
	}

	@Override
	public void sortDeliveryModes(final List<DeliveryModeModel> deliveryModeModels, final AbstractOrderModel abstractOrder)
	{
		deliveryModeModels.sort((deliveryMode1, deliveryMode2) -> {
			PriceValue o1PriceValue = getDeliveryCostForDeliveryModeAndAbstractOrder(deliveryMode1, abstractOrder);
			PriceValue o2PriceValue = getDeliveryCostForDeliveryModeAndAbstractOrder(deliveryMode2, abstractOrder);
			if (o1PriceValue == null && o2PriceValue != null)
			{
				return -1;
			}
			else if (o1PriceValue != null && o2PriceValue == null)
			{
				return 1;
			}
			else if (o1PriceValue == null && o2PriceValue == null)
			{
				return 0;
			}
			return Double.compare(o1PriceValue.getValue(), o2PriceValue.getValue());
		});
	}

	@Override
	public PriceValue getDeliveryCostForDeliveryModeAndAbstractOrder(final DeliveryModeModel deliveryMode,
			final AbstractOrderModel abstractOrder)
	{
		validateParameterNotNull(deliveryMode, "deliveryMode model cannot be null");
		validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");
		final DeliveryMode deliveryModeSource = getModelService().getSource(deliveryMode);
		try
		{
			final AbstractOrder abstractOrderSource = getModelService().getSource(abstractOrder);
			return deliveryModeSource.getCost(abstractOrderSource);
		}
		catch (final JaloDeliveryModeException e)
		{
			LOG.warn("Failed to get delivery cost for order: " + abstractOrder.getCode(), e);
			return null;
		}
	}
}