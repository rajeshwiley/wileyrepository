package com.wiley.core.wiley.session.storage;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileyFailedCartModificationsStorageService
{
	/**
	 * Pushes all failed cart modification at the end. FIFO.
	 *
	 * @param failedCartModifications
	 * 		modifications which will be stored.
	 */
	void pushAll(@Nonnull List<CommerceCartModification> failedCartModifications);

	/**
	 * Removes all failed cart modification from the storage and returns them.
	 *
	 * @return all failed cart modifications related to cart or empty list.
	 */
	@Nonnull
	List<CommerceCartModification> popAll();

	/**
	 * Removes all failed cart modification DTO from the storage and returns them.
	 *
	 * @return all failed cart modifications DTO related to cart or empty list.
	 */
	@Nonnull
	List<CartModificationData> popAllData();

	/**
	 * Verify that cart calculation results
	 *
	 * @return true if fails
	 */
	boolean isCartCalculationFailed();
}
