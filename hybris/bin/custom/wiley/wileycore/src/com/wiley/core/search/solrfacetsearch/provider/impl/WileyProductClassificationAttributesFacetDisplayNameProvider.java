/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type classification attribute value facet display name provider.
 */
public class WileyProductClassificationAttributesFacetDisplayNameProvider extends AbstractFacetValueDisplayNameProvider
{
	private static final Logger LOG = Logger.getLogger(WileyProductClassificationAttributesFacetDisplayNameProvider.class);

	private String systemVersion;
	private String systemId;

	private ClassificationSystemService classificationSystemService;

	/**
	 * Gets display name.
	 *
	 * @param query
	 * 		the query
	 * @param property
	 * 		the property
	 * @param facetValue
	 * 		the facet value
	 * @return the display name
	 */
	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty property, final String facetValue)
	{
		String result = facetValue;
		try
		{
			final ClassificationSystemVersionModel classificationSystemVersion = classificationSystemService.getSystemVersion(
					systemId, systemVersion);
			ClassificationAttributeValueModel model = classificationSystemService.getAttributeValueForCode(
					classificationSystemVersion, facetValue);
			result = model.getName();
		}
		catch (SystemException | IllegalArgumentException e)
		{
			LOG.warn("Unable to resolve facet display name for facetValue '" + facetValue + "' in classification system '"
					+ getSystemId() + ":" + getSystemVersion() + "'; reason: " + e.getMessage());
		}
		return result;
	}

	private Integer getValue(final FacetValue facetValue)
	{
		return getClassificationAttributeValueModel(facetValue.getName()).getRank();
	}

	private ClassificationAttributeValueModel getClassificationAttributeValueModel(final String classificationAttributeValueCode)
	{
		final ClassificationSystemVersionModel classificationSystemVersion = classificationSystemService.getSystemVersion(
				systemId, systemVersion);
		return classificationSystemService.getAttributeValueForCode(classificationSystemVersion,
				classificationAttributeValueCode);
	}

	public String getSystemVersion()
	{
		return systemVersion;
	}

	@Required
	public void setSystemVersion(final String systemVersion)
	{
		this.systemVersion = systemVersion;
	}

	public String getSystemId()
	{
		return systemId;
	}

	@Required
	public void setSystemId(final String systemId)
	{
		this.systemId = systemId;
	}

	public ClassificationSystemService getClassificationSystemService()
	{
		return classificationSystemService;
	}

	@Required
	public void setClassificationSystemService(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}
}
