package com.wiley.core.servicelayer.processdefinition;

import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;

import java.util.List;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyProcessDefinitionService
{
	List<DynamicProcessDefinitionModel> getAllProcessDefinitions();

	List<DynamicProcessDefinitionModel> getProcessDefinitionsByCode(String code);
}
