/**
 *
 */
package com.wiley.core.order.impl;

import de.hybris.platform.acceleratorservices.order.impl.DefaultCartServiceForAccelerator;
import de.hybris.platform.commerceservices.externaltax.RecalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartHashCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceOrderParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.couponservices.service.data.CouponResponse;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.product.ProductService;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyCartServiceForAccelerator extends DefaultCartServiceForAccelerator
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCartServiceForAccelerator.class);

	@Autowired
	private CommerceCartHashCalculationStrategy commerceCartHashCalculationStrategy;

	private ProductService productService;

	private CouponService couponService;

	@Override
	public void setSessionCart(final CartModel cart)
	{
		if (cart == null)
		{
			removeSessionCart();
		}
		else
		{
			getSessionService().setAttribute(SESSION_CART_PARAMETER_NAME, cart);
			final CommerceOrderParameter parameter = new CommerceOrderParameter();
			parameter.setOrder(cart);
			final String orderCalculationHash = commerceCartHashCalculationStrategy
					.buildHashForAbstractOrder(parameter);
			getSessionService().setAttribute(RecalculateExternalTaxesStrategy.SESSION_ATTIR_ORDER_RECALCULATION_HASH,
					orderCalculationHash);
		}
	}

	@Override
	public void removeSessionCart()
	{
		if (hasSessionCart())
		{
			final CartModel sessionCart = getSessionCart();
			getModelService().remove(sessionCart);
			getSessionService().removeAttribute(SESSION_CART_PARAMETER_NAME);
			getSessionService()
					.removeAttribute(RecalculateExternalTaxesStrategy.SESSION_ATTIR_ORDER_RECALCULATION_HASH);
		}
	}

	@Override
	public void changeCurrentCartUser(final UserModel user)
	{
		validateParameterNotNull(user, "user must not be null!");
		if (hasSessionCart())
		{
			CartModel cart = getSessionCart();
			final String cartUserUid = cart.getUser() != null ? cart.getUser().getUid() : null;

			if (!user.getUid().equals(cartUserUid))
			{
				super.changeCurrentCartUser(user);
				reapplyVouchers(cart);
			}
		}
	}

	private void reapplyVouchers(final CartModel cart)
	{
		Collection<String> appliedCoupons = cart.getAppliedCouponCodes();

		if (CollectionUtils.isEmpty(appliedCoupons))
		{
			return;
		}

		for (String code : appliedCoupons)
		{
			couponService.releaseCouponCode(code, cart);
			CouponResponse couponResponse = couponService.redeemCoupon(code, cart);
			if (!couponResponse.getSuccess())
			{
				LOG.info("Coupon {} cannot be reapplied to cart {}", code, cart.getCode());
			}
		}
	}

	@Override
	public AbstractOrderEntryModel addNewEntry(final ComposedTypeModel entryType, final CartModel order,
			final ProductModel product, final long qty, final UnitModel unit, final int number, final boolean addToPresent)
	{
		UnitModel usedUnit = unit;
		if (usedUnit == null)
		{
			usedUnit = getUnit(product);
		}
		return super.addNewEntry(entryType, order, product, qty, usedUnit, number, addToPresent);
	}

	protected UnitModel getUnit(final ProductModel productModel)
	{
		return getProductService().getOrderableUnit(productModel);
	}

	public CouponService getCouponService()
	{
		return couponService;
	}

	@Required
	public void setCouponService(final CouponService couponService)
	{
		this.couponService = couponService;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}
}
