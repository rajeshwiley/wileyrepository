package com.wiley.core.order.strategies;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.strategies.impl.DefaultCreateOrderFromCartStrategy;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;


public class WileyCreateOrderFromCartStrategy extends DefaultCreateOrderFromCartStrategy
{

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	public OrderModel createOrderFromCart(final CartModel cart) throws InvalidCartException
	{
		OrderModel order = super.createOrderFromCart(cart);
		order.setSourceSystem(WileyCoreConstants.SOURCE_SYSTEM_HYBRIS);
		return order;
	}
}
