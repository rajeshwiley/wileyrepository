package com.wiley.core.event.models;

import de.hybris.platform.commerceservices.event.ForgottenPwdEvent;


public class WileyForgottenPasswordEventModel extends ForgottenPwdEvent
{
	private String redirectType;

	public WileyForgottenPasswordEventModel(final String token, final String type)
	{
		super(token);
		this.redirectType = type;
	}

	public String getRedirectType()
	{
		return redirectType;
	}
}
