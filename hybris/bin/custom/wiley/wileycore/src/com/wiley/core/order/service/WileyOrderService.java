package com.wiley.core.order.service;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;


public interface WileyOrderService
{
	OrderModel getOrderForGUID(String guid, BaseSiteModel basesite);

	boolean isOrderEligibleForPayment(OrderModel orderModel);

	boolean isNewOrder(OrderModel orderModel);

	boolean isExternalOrder(OrderModel order);

	boolean isOrderExistsForGuid(String guid, BaseSiteModel baseSite);
}
