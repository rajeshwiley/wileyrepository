package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.jalo.ExternalDeliveryMode;
import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.order.WileycomDeliveryService;


/**
 * Strategy updates delivery mode for current abstract order
 */
public class WileycomFindDeliveryCostStrategy implements FindDeliveryCostStrategy
{
	private static final Logger LOG = Logger.getLogger(WileycomFindDeliveryCostStrategy.class);

	@Resource
	private ModelService modelService;

	private WileycomCommerceDeliveryModeValidationStrategy wileycomCommerceDeliveryModeValidationStrategy;

	private WileycomDeliveryService wileycomDeliveryService;

	@Override
	public PriceValue getDeliveryCost(final AbstractOrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);

		List<ExternalDeliveryModeModel> supportedDeliveryModes =
				getWileycomDeliveryService().getSupportedExternalDeliveryModeListForOrder(order);
		getWileycomCommerceDeliveryModeValidationStrategy().invalidateCartDeliveryMode(order, supportedDeliveryModes);

		final AbstractOrder orderItem = modelService.getSource(order);
		final ExternalDeliveryMode dMode = (ExternalDeliveryMode) orderItem.getDeliveryMode();
		if (dMode != null)
		{
			final PriceValue deliveryCost = dMode.getCost(orderItem);
			if (hasSameCurrency(deliveryCost, order))
			{
				return deliveryCost;
			}
		}
		return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
	}

	private boolean hasSameCurrency(final PriceValue deliveryCost, final AbstractOrderModel order)
	{
		return deliveryCost != null && order.getCurrency().getIsocode().equals(deliveryCost.getCurrencyIso());
	}

	public WileycomCommerceDeliveryModeValidationStrategy getWileycomCommerceDeliveryModeValidationStrategy()
	{
		// HOTFIX for https://jira.wiley.ru/browse/ECSC-10633
		// TODO reason of this problem will be investigated
		if (wileycomCommerceDeliveryModeValidationStrategy == null)
		{
			wileycomCommerceDeliveryModeValidationStrategy = Registry.getApplicationContext().getBean(
					"wileyb2cCommerceDeliveryModeValidationStrategy", WileycomCommerceDeliveryModeValidationStrategy.class);
		}
		return wileycomCommerceDeliveryModeValidationStrategy;
	}

	public WileycomDeliveryService getWileycomDeliveryService()
	{
		// HOTFIX for https://jira.wiley.ru/browse/ECSC-10633
		// TODO reason of this problem will be investigated
		if (wileycomDeliveryService == null)
		{

			wileycomDeliveryService = Registry.getApplicationContext().getBean("wileycomDeliveryService",
					WileycomDeliveryService.class);
		}
		return wileycomDeliveryService;
	}
}
