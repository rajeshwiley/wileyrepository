package com.wiley.core.integration;

import org.springframework.util.Assert;

import com.wiley.core.order.data.CartModificationMessageType;


/**
 * This DTO is primary used in {@link ExternalCartModificationsStorageService}
 * and holds necessary data related to external modification of cart.<br/>
 * <br/>
 * It's immutable implementation.
 */
public class ExternalCartModification
{
	private Integer entryNumber;

	private String productCode;

	private String statusCode;

	private Long quantity;

	private String message;

	private CartModificationMessageType messageType;

	/**
	 * Code of the calculation event.
	 */
	private String eventCode;

	public ExternalCartModification(final Integer entryNumber, final String productCode, final String statusCode,
			final Long quantity,
			final String message,
			final CartModificationMessageType messageType,
			final String eventCode)
	{
		Assert.state(entryNumber != null, "entryNumber should not be null.");
		Assert.state(productCode != null, "productCode should not be null.");
		Assert.state(statusCode != null, "statusCode should not be null.");

		this.entryNumber = entryNumber;
		this.productCode = productCode;
		this.statusCode = statusCode;
		this.quantity = quantity;
		this.message = message;
		this.messageType = messageType;
		this.eventCode = eventCode;
	}

	public Integer getEntryNumber()
	{
		return entryNumber;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public String getMessage()
	{
		return message;
	}

	public CartModificationMessageType getMessageType()
	{
		return messageType;
	}

	public String getEventCode()
	{
		return eventCode;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof ExternalCartModification))
		{
			return false;
		}

		final ExternalCartModification that = (ExternalCartModification) o;

		if (!entryNumber.equals(that.entryNumber))
		{
			return false;
		}
		if (!productCode.equals(that.productCode))
		{
			return false;
		}
		if (!statusCode.equals(that.statusCode))
		{
			return false;
		}
		return eventCode != null ? eventCode.equals(that.eventCode) : that.eventCode == null;

	}

	@Override
	public int hashCode()
	{
		int result = entryNumber.hashCode();
		result = 31 * result + productCode.hashCode();
		result = 31 * result + statusCode.hashCode();
		result = 31 * result + (eventCode != null ? eventCode.hashCode() : 0);
		return result;
	}

	public static final class Builder
	{
		private Integer entryNumber;
		private String productCode;
		private String statusCode;
		private Long quantity;
		private String message;
		private CartModificationMessageType messageType;
		private String eventCode;

		private Builder()
		{
		}

		public static Builder get()
		{
			return new Builder();
		}

		public Builder withEntryNumber(final Integer entryNumber)
		{
			this.entryNumber = entryNumber;
			return this;
		}

		public Builder withProductCode(final String productCode)
		{
			this.productCode = productCode;
			return this;
		}

		public Builder withStatusCode(final String statusCode)
		{
			this.statusCode = statusCode;
			return this;
		}

		public Builder withQuantity(final Long quantity)
		{
			this.quantity = quantity;
			return this;
		}

		public Builder withMessage(final String message)
		{
			this.message = message;
			return this;
		}

		public Builder withMessageType(final CartModificationMessageType messageType)
		{
			this.messageType = messageType;
			return this;
		}

		public Builder withEventCode(final String eventCode)
		{
			this.eventCode = eventCode;
			return this;
		}

		public ExternalCartModification build()
		{
			ExternalCartModification externalCartModification = new ExternalCartModification(entryNumber, productCode, statusCode,
					quantity, message, messageType, eventCode);
			return externalCartModification;
		}
	}
}
