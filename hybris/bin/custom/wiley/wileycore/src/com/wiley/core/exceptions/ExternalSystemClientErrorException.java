package com.wiley.core.exceptions;

/**
 * This exception is thrown if bad request was sent.
 */
public class ExternalSystemClientErrorException extends ExternalSystemException
{
	public ExternalSystemClientErrorException() { super(); }

	public ExternalSystemClientErrorException(final String message)
	{
		super(message);
	}

	public ExternalSystemClientErrorException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemClientErrorException(final Throwable cause)
	{
		super(cause);
	}
}
