package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileyb2c.classification.Wileyb2cResolveClassificationAttributeStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cClassificationServiceImpl implements Wileyb2cClassificationService
{
	private List<Wileyb2cResolveClassificationAttributeStrategy> strategies;

	@Nonnull
	@Override
	public ClassificationClassModel resolveClassificationClass(@Nonnull final ProductModel productModel)
	{
		List<ClassificationClassModel> classes = productModel.getSupercategories().stream()
				.filter(category -> category instanceof ClassificationClassModel)
				.map(category -> ClassificationClassModel.class.cast(category))
				.collect(Collectors.toList());
		Assert.notEmpty(classes);
		if (classes.size() > 1)
		{
			throw new IllegalStateException("Couldn't resolve classification class for product " + productModel.getCode()
					+ "There is more than one classification class. Classification classes: "
					+ classes.stream().map(ClassificationClassModel::getCode).collect(Collectors.joining(",")));
		}
		return classes.get(0);
	}

	@Nonnull
	@Override
	public String resolveAttribute(@Nonnull final ProductModel product, @Nonnull final Wileyb2cClassificationAttributes attribute)
	{
		Assert.notNull(strategies);
		Assert.notNull(product);
		Assert.notNull(attribute);
		for (final Wileyb2cResolveClassificationAttributeStrategy strategy : strategies)
		{
			if (strategy.apply(attribute))
			{
				return strategy.resolveAttribute(product, attribute);
			}
		}
		throw new IllegalStateException("Can't resolve strategy for " + attribute.getCode());
	}

	@Override
	public <T> T resolveAttributeValue(final ProductModel product, final Wileyb2cClassificationAttributes attribute)
	{
		Assert.notNull(strategies);
		Assert.notNull(product);
		Assert.notNull(attribute);
		for (final Wileyb2cResolveClassificationAttributeStrategy strategy : strategies)
		{
			if (strategy.apply(attribute))
			{
				return strategy.resolveAttributeValue(product, attribute);
			}
		}
		throw new IllegalStateException("Can't resolve strategy for " + attribute.getCode());
	}

	@Required
	public void setStrategies(
			final List<Wileyb2cResolveClassificationAttributeStrategy> strategies)
	{
		this.strategies = strategies;
	}
}
