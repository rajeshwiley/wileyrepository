package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


/**
 * @author Maksim_Kozich
 */
public class Wileyb2cFacetLimitPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_SORT_TYPE> implements
		Populator<SearchQueryPageableData<SolrSearchQueryData>,
				SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, IndexedType, IndexedProperty, SearchQuery, INDEXED_TYPE_SORT_TYPE>>
{

	@Override
	public void populate(
			final SearchQueryPageableData<SolrSearchQueryData> source,
			final SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, IndexedType, IndexedProperty, SearchQuery,
					INDEXED_TYPE_SORT_TYPE> target)
	{
		WileySearchQuery wileySearchQuery = (WileySearchQuery) target.getSearchQuery();
		wileySearchQuery.setFacetCode(source.getSearchQueryData().getFacetCode());
		wileySearchQuery.setUseFacetViewMoreLimit(source.getSearchQueryData().getUseFacetViewMoreLimit());
		wileySearchQuery.setFacetLimit(source.getSearchQueryData().getFacetLimit());
	}
}
