package com.wiley.core.wel.preorder.strategy;

import de.hybris.platform.core.model.product.ProductModel;

import com.wiley.core.enums.WileyProductLifecycleEnum;


public interface ProductLifecycleStatusStrategy
{
	WileyProductLifecycleEnum getLifecycleStatusForProduct(ProductModel productModel);

	WileyProductLifecycleEnum getLifecycleStatusForProduct(String productCode);

}
