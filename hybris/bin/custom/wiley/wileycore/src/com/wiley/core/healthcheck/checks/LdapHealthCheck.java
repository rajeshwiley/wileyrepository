package com.wiley.core.healthcheck.checks;

import de.hybris.platform.ldap.connection.ConnectionData;
import de.hybris.platform.ldap.connection.LDAPConnection;
import de.hybris.platform.ldap.connection.LDAPConnectionFactory;
import de.hybris.platform.ldap.exception.LDAPUnavailableException;
import de.hybris.platform.ldap.jalo.LDAPManager;

import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;


public class LdapHealthCheck extends HealthCheck
{
	private static final Logger LOG = LoggerFactory.getLogger(LdapHealthCheck.class);

	@Override
	protected Result check() throws Exception
	{
		LDAPConnection ldapConnection = null;
		Result result;

		try
		{
			ldapConnection = LDAPConnectionFactory.getLDAPConnection(new ConnectionData(LDAPManager.getInstance().getConfig()));
			result = Result.healthy();
		}
		catch (LDAPUnavailableException | NamingException e)
		{
			LOG.error("Exception occured when establishing connection to remote LDAP server, assuming its not available.", e);
			result = Result.unhealthy(buildErrorString(e));
		}
		finally
		{
			if (ldapConnection != null)
			{
				ldapConnection.close();
			}
		}

		return result;
	}

	private String buildErrorString(final Exception e)
	{
		return e.getClass().getSimpleName() + ": " + e.getMessage();
	}
}