package com.wiley.core.customer.service.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;

import java.util.Date;

import javax.annotation.Resource;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.event.models.WileyForgottenPasswordEventModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyCustomerAccountServiceImpl extends DefaultCustomerAccountService implements WileyCustomerAccountService
{
	@Resource
	private UserDao userDao;

	@Override
	public void changePassword(final UserModel userModel, final String oldPassword, final String newPassword)
			throws PasswordMismatchException
	{
		validateParameterNotNullStandardMessage("customerModel", userModel);
		if (!getUserService().isAnonymousUser(userModel))
		{

			if (getPasswordEncoderService().isValid(userModel, oldPassword))
			{
				getUserService().setPassword(userModel, newPassword, userModel.getPasswordEncoding());
				getModelService().save(userModel);
			}
			else
			{
				throw new PasswordMismatchException(userModel.getUid());
			}
		}
	}

	@Override
	public void forgottenPassword(final CustomerModel customerModel, final ResetPasswordRedirectType redirectType)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		final long timeStamp = getTokenValiditySeconds() > 0L ? new Date().getTime() : 0L;
		final SecureToken data = new SecureToken(customerModel.getUid(), timeStamp);
		final String token = getSecureTokenService().encryptData(data);
		customerModel.setToken(token);
		getModelService().save(customerModel);
		WileyForgottenPasswordEventModel eventModel = new WileyForgottenPasswordEventModel(token,
				ResetPasswordRedirectType.getOrDefaultRedirectType(redirectType).name());
		getEventService().publishEvent(initializeEvent(eventModel, customerModel));
	}

	/**
	 * Fill values for customer info. First and Last name are retrieved from corresponding default payment address. OOTB name
	 * field left unchanged. Other fields filling process is delegated to parent's method.
	 *
	 * @param customer
	 * 		the customer
	 * @throws DuplicateUidException
	 * 		the duplicate uid exception
	 */
	protected void fillValuesForCustomerInfo(final CustomerModel customer, final OrderModel orderModel)
			throws DuplicateUidException
	{
		String originalCustomerName = customer.getName();
		super.fillValuesForCustomerInfo(customer);
		customer.setName(originalCustomerName);

		AddressModel paymentAddress;
		// Pull the name from the guest's billing info
		if (customer.getDefaultPaymentAddress() != null)
		{
			paymentAddress = customer.getDefaultPaymentAddress();
		}
		else
		{
			paymentAddress = orderModel.getPaymentAddress();
		}
		customer.setFirstName(paymentAddress.getFirstname());
		customer.setLastName(paymentAddress.getLastname());
	}

	@Override
	public void updateCustomerProfile(final CustomerModel customerModel) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		internalSaveCustomer(customerModel);
	}

	@Override
	public AddressModel getPaymentAddress(final UserModel customerModel)
	{
		AddressModel paymentAddress = customerModel.getDefaultPaymentAddress();
		if (paymentAddress == null && customerModel instanceof CustomerModel) {
			PaymentInfoModel paymentInfoModel = ((CustomerModel) customerModel).getDefaultPaymentInfo();
			if (paymentInfoModel != null) {
				paymentAddress = paymentInfoModel.getBillingAddress();
			}
		}
		return paymentAddress;
	}

	@Override
	public void convertGuestToCustomer(final String pwd, final String orderGUID) throws DuplicateUidException
	{
		super.convertGuestToCustomer(pwd, orderGUID);
	}
	
	@Override
	public boolean isRegisteredUser(final String uid)
	{
		return (userDao.findUserByUID(uid) != null);
	}

	@Override
	public void checkUidUniqueness(final String newUid) throws DuplicateUidException
	{
		super.checkUidUniqueness(newUid);
	}
	
	private boolean isAddressShippingOrBilling(final AddressModel addressModel)
	{
		return addressModel.getShippingAddress() || addressModel.getBillingAddress();
	}

	private void updateCustomerDefaultAddressConsideringType(final CustomerModel customerModel, final AddressModel addressModel)
	{
		if (addressModel.getBillingAddress())
		{
			customerModel.setDefaultPaymentAddress(addressModel);
		}
		if (addressModel.getShippingAddress())
		{
			customerModel.setDefaultShipmentAddress(addressModel);
		}
	}
}
