package com.wiley.core.wiley.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.strategies.cart.validation.WileyCartParameterValidator;
import com.wiley.core.wileycom.order.impl.WileycomCommerceAddToCartStrategyImpl;


/**
 * @author Dzmitryi_Halahayeu
 */
public abstract class WileyCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomCommerceAddToCartStrategyImpl.class);

	private List<WileyCartParameterValidator> cartParameterValidators;

	@Override
	protected void validateAddToCart(final CommerceCartParameter parameters) throws CommerceCartModificationException
	{
		final ProductModel productModel = parameters.getProduct();
		final CartModel cartModel = parameters.getCart();

		LOG.debug("Validating input parameters for cart [{}] and product [{}].", cartModel.getCode(), productModel.getCode());

		super.validateAddToCart(parameters);

		for (WileyCartParameterValidator cartParameterValidator : getCartParameterValidators())
		{
			cartParameterValidator.validateAddToCart(parameters);
		}
	}

	public List<WileyCartParameterValidator> getCartParameterValidators()
	{
		return cartParameterValidators;
	}

	@Required
	public void setCartParameterValidators(final List<WileyCartParameterValidator> cartParameterValidators)
	{
		if (CollectionUtils.isEmpty(cartParameterValidators))
		{
			throw new IllegalStateException("cartParameterValidators should not be empty");
		}
		this.cartParameterValidators = cartParameterValidators;
	}
}
