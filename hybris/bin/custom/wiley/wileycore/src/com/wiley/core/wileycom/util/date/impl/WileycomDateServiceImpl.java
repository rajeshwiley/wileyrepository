package com.wiley.core.wileycom.util.date.impl;

import de.hybris.platform.servicelayer.time.TimeService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.wileycom.util.date.WileycomDateService;

import static java.time.temporal.ChronoUnit.DAYS;
import org.springframework.util.Assert;


/**
 * Created by Georgii_Gavrysh on 6/21/2016.
 */
public class WileycomDateServiceImpl implements WileycomDateService
{

	@Autowired
	TimeService timeService;

	@Override
	public int daysNumberTillDate(@Nonnull final Date date)
	{
		Assert.notNull(date);
		final LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		final Date currentTime = timeService.getCurrentTime();
		final LocalDate now = currentTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return Math.toIntExact(DAYS.between(now, localDate));
	}

	public TimeService getTimeService()
	{
		return timeService;
	}
}
