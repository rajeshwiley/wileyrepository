package com.wiley.core.integration.edi.strategy.impl;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.dao.EdiOrderDao;
import com.wiley.core.integration.edi.strategy.EdiOrderProvisionStrategy;
import com.wiley.core.integration.edi.strategy.EdiProcessValidatingStrategy;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class ProcessBasedEdiOrderProvisionStrategy implements EdiOrderProvisionStrategy
{
	private static final Logger LOG = Logger.getLogger(ProcessBasedEdiOrderProvisionStrategy.class);

	@Resource(name = "ediOrderDao")
	private EdiOrderDao ediOrderDao;

	@Resource(name = "ediProcessValidatingStrategy")
	private EdiProcessValidatingStrategy refundedTheSameDayProcessesValidationStrategy;

	@Resource
	private BusinessProcessService businessProcessService;

	@Resource
	private ModelService modelService;

	@Resource
	private WileyRefundService wileyRefundService;

	@Override
	public List<WileyExportProcessModel> getExportProcessesReadyForExport(final BaseStoreModel store, final String paymentType)
	{
		LOG.info("Retrieving processes ready for export ...");
		final List<WileyExportProcessModel> exportProcesses = ediOrderDao.getExportProcessesReadyForExport(store, paymentType);
		LOG.info("Found processes: " + exportProcesses.size());
		return removeSettlesAndRefundsThatWereMadeAtTheSamePeriod(exportProcesses);
	}

	@Override
	public List<WileyExportProcessModel> getExportProcessesReadyForDailyReportExport(final BaseStoreModel store,
			final String paymentType)
	{
		LOG.info("Retrieving processes ready for daily report export ...");
		final List<WileyExportProcessModel> exportProcesses = ediOrderDao.getExportProcessesReadyForDailyReportExport(store,
				paymentType);
		LOG.info("Found processes: " + exportProcesses.size());
		return removeSettlesAndRefundsThatWereMadeAtTheSamePeriod(exportProcesses);
	}

	@Override
	public List<WileyExportProcessModel> getZeroDollarPhysicalPartnerOrderProcessesReadyForExport(final BaseStoreModel store,
			final String paymentType)
	{
		LOG.info("Retrieving zero dollar physical partner order processes ready for export ...");
		final List<WileyExportProcessModel> exportProcesses = new ArrayList<>();
		if (EDIConstants.PAYMENT_TYPE_CREDIT_CARD.equals(paymentType))
		{
			exportProcesses.addAll(ediOrderDao.getZeroDollarPhysicalOrderProcessesReadyForExport(store));
		}
		LOG.info("Found zero dollar physical partner order processes for export: " + exportProcesses.size());
		return exportProcesses;
	}

	/**
	 * Remove settle and refunds (full or partial, which in sum are full) that were made at the same period.
	 *
	 * @param exportProcesses
	 * @return - list of processes
	 */
	private List<WileyExportProcessModel> removeSettlesAndRefundsThatWereMadeAtTheSamePeriod(
			final List<WileyExportProcessModel> exportProcesses)
	{

		if (CollectionUtils.isEmpty(exportProcesses))
		{
			return Collections.emptyList();
		}

		Collection<WileyExportProcessModel> rejectedProcesses =
				refundedTheSameDayProcessesValidationStrategy.getRejectedProcesses(exportProcesses);

		final List<WileyExportProcessModel> allowedForExportProcesses = new ArrayList<>(
				CollectionUtils.removeAll(exportProcesses, rejectedProcesses));


		if (CollectionUtils.isNotEmpty(rejectedProcesses))
		{
			LOG.debug("Finishing [{}] rejected order processes not included to report.");
			markOrdersExported(rejectedProcesses);
			markDailyReportSent(rejectedProcesses);
		}

		return allowedForExportProcesses;
	}

	@Override
	public void markOrdersExported(final Collection<WileyExportProcessModel> processes)
	{
		if (CollectionUtils.isNotEmpty(processes))
		{
			processes.forEach(process -> {
				LOG.debug(String.format("Marking order [%s] as exported to Core(EDI)", process.getOrder().getCode()));
				process.setDone(true);
				process.setOrderExported(true);
				modelService.save(process);
				businessProcessService.triggerEvent(process.getCode() + "_" + EDIConstants.WILEY_ORDER_EXPORTED_EVENT_NAME);

			});
		}
	}

	@Override
	public void markDailyReportSent(final Collection<WileyExportProcessModel> processes)
	{
		if (CollectionUtils.isNotEmpty(processes))
		{
			processes.forEach(process -> {
				LOG.debug(String.format("Marking order [%s] as daily report sent", process.getOrder().getCode()));
				process.setDailyReportSent(true);
				modelService.save(process);
			});
		}
	}

	@Override
	public List<WileyExportProcessModel> removePartialRefundsProcesses(final List<WileyExportProcessModel> processes)
	{
		return processes.stream().filter(process -> process.getExportType().equals(ExportProcessType.SETTLE) || wileyRefundService
				.isOrderRefundedByOneTransaction(process)).collect(Collectors.toList());
	}


}
