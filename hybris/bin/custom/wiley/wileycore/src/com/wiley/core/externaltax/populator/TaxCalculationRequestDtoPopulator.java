package com.wiley.core.externaltax.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.dto.TaxAddressDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;
import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationRequestDtoPopulator implements Populator<AbstractOrderModel, TaxCalculationRequestDto>
{

	private Converter<AddressModel, TaxAddressDto> taxAddressDtoConverter;
	private Converter<AbstractOrderEntryModel, TaxCalculationRequestEntryDto> taxCalculationRequestEntryDtoConverter;
	private WileyTaxAddressStrategy taxAddressStrategy;

	@Override
	public void populate(final AbstractOrderModel source, final TaxCalculationRequestDto target) throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setSiteId(source.getSite().getUid());
		target.setDate(getDateForAbstractOrder(source));
		target.setCurrency(source.getCurrency().getIsocode());
		target.setEntries(taxCalculationRequestEntryDtoConverter.convertAll(source.getEntries()));
		target.setPaymentAddress(convertAddress(taxAddressStrategy.resolvePaymentAddress(source)));
		target.setDeliveryAddress(convertAddress(taxAddressStrategy.resolveDeliveryAddress(source)));
		target.setHandlingCost(0.0d);
		target.setShippingCost(source.getDeliveryCost());
	}

	private Date getDateForAbstractOrder(final AbstractOrderModel abstractOrder)
	{
		Date date;
		if (abstractOrder instanceof CartModel)
		{
			date = new Date();
		}
		else
		{
			date = abstractOrder.getDate();
		}
		return date;
	}

	private TaxAddressDto convertAddress(final AddressModel address)
	{
		if (address != null)
		{
			return taxAddressDtoConverter.convert(address);
		}
		return null;
	}

	@Required
	public void setTaxAddressDtoConverter(
			final Converter<AddressModel, TaxAddressDto> taxAddressDtoConverter)
	{
		this.taxAddressDtoConverter = taxAddressDtoConverter;
	}

	@Required
	public void setTaxCalculationRequestEntryDtoConverter(
			final Converter<AbstractOrderEntryModel, TaxCalculationRequestEntryDto> taxCalculationRequestEntryDtoConverter)
	{
		this.taxCalculationRequestEntryDtoConverter = taxCalculationRequestEntryDtoConverter;
	}

	@Required
	public void setTaxAddressStrategy(final WileyTaxAddressStrategy taxAddressStrategy)
	{
		this.taxAddressStrategy = taxAddressStrategy;
	}
}
