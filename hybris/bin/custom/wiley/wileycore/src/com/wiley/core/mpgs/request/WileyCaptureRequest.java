package com.wiley.core.mpgs.request;



import java.math.BigDecimal;


public class WileyCaptureRequest
{
	private String urlPath;
	private BigDecimal amount;
	private String paymentProvider;
	private String currency;
	private String transactionReference;


	public WileyCaptureRequest(final String urlPath, final BigDecimal amount,
			final String paymentProvider, final String currency)
	{

		this.urlPath = urlPath;
		this.amount = amount;
		this.paymentProvider = paymentProvider;
		this.currency = currency;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(final BigDecimal totalAmount)
	{
		this.amount = totalAmount;
	}

	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getTransactionReference()
	{
		return transactionReference;
	}

	public void setTransactionReference(final String transactionReference)
	{
		this.transactionReference = transactionReference;
	}
}
