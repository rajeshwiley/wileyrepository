package com.wiley.core.uuid;

/**
 * Service for generating random values. Could be easily replaced in tests by mock instance
 *
 * @author Dzmitryi_Halahayeu
 */
public interface RandomService
{

	String randomString();
}
