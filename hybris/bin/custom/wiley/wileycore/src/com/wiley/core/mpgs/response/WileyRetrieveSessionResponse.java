package com.wiley.core.mpgs.response;

public class WileyRetrieveSessionResponse extends WileyResponse
{
	private String sessionId;

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

}
