package com.wiley.core.wileycom.stock.dao;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * DAO for selecting warehouse according country and base store
 *
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomWarehouseDao
{

	/**
	 * fetch marked as default warehouses that correspond passed country and base store.
	 *
	 * @param baseStore
	 * 		the base store
	 * @param countryModel
	 * 		the country model
	 * @return default warehouses for base store and country
	 */
	List<WarehouseModel> getDefaultWarehousesForBaseStore(@Nonnull BaseStoreModel baseStore,
			@Nonnull CountryModel countryModel);
}
