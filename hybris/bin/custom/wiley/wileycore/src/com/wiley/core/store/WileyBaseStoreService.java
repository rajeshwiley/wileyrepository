package com.wiley.core.store;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Contains methods for working with base stores.
 */
public interface WileyBaseStoreService
{

	/**
	 * Returns uid of the first available store for site
	 *
	 * @param siteUid
	 * 		uid of the target site.
	 * @return store's uid
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 * 		if there is not site for provided code.
	 * @throws IllegalStateException
	 * 		if site has no sores.
	 */
	@Nonnull
	String getBaseStoreUidForSite(@Nonnull String siteUid);

	/**
	 * Returns the base store for base site.
	 *
	 * @param baseSiteModel
	 * 		target base site.
	 * @return optional base store. base store is not present if base site has no sores.
	 */
	@Nonnull
	Optional<BaseStoreModel> getBaseStoreForBaseSite(@Nonnull BaseSiteModel baseSiteModel);

	/**
	 * Same method to return base store for base site.
	 *
	 * @param baseSiteModel
	 * 		target base site.
	 * @return base store
	 */
	@Nonnull
	BaseStoreModel acquireBaseStoreByBaseSite(@Nonnull BaseSiteModel baseSiteModel);
}
