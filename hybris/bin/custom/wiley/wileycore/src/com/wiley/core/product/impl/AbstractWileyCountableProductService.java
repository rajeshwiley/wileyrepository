package com.wiley.core.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;

import com.wiley.core.product.WileyCountableProductService;


/**
 * @author Dzmitryi_Halahayeu
 */
public  abstract class AbstractWileyCountableProductService implements WileyCountableProductService
{
	private static final Logger LOG = Logger.getLogger(DefaultWileyProductEditionFormatService.class);

	protected abstract boolean doCanProductHaveQuantity(@Nonnull ProductModel product);

	@Override
	public boolean canProductHaveQuantity(@Nonnull final ProductModel product)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Got product [%s].", product));
		}
		return doCanProductHaveQuantity(product);
	}

}
