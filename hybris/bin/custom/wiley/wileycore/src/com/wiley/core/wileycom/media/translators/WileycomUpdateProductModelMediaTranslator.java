package com.wiley.core.wileycom.media.translators;

import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;

import com.wiley.core.wileycom.valuetranslator.AbstractWileycomAllowEmptySpecialValueTranslator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomUpdateProductModelMediaTranslator extends AbstractWileycomAllowEmptySpecialValueTranslator
{
	private static final String DEFAULT_IMPORT_ADAPTER_NAME = "wileycomUpdateProductModelMediaImportAdapter";

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		super.init(columnDescriptor);
		final String catalogId = columnDescriptor.getDescriptorData().getModifier("catalogId");
		final String catalogVersion = columnDescriptor.getDescriptorData().getModifier("catalogVersion");
		validateParameterNotNull(catalogId, "CatalogId must not be null.");
		validateParameterNotNull(catalogVersion, "CatalogVersion must not be null.");
		getWileycomImportAdapter().setCatalog(catalogVersion, catalogId);
	}

	@Override
	protected String getDefaultImportAdapterName()
	{
		return DEFAULT_IMPORT_ADAPTER_NAME;
	}
}
