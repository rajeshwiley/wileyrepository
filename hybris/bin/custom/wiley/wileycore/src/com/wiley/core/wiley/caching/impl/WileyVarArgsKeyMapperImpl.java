package com.wiley.core.wiley.caching.impl;

import de.hybris.platform.regioncache.key.CacheKey;

import javax.ws.rs.NotSupportedException;

import org.apache.log4j.Logger;

import com.wiley.core.wiley.caching.WileyCacheKeyMapper;


public class WileyVarArgsKeyMapperImpl implements WileyCacheKeyMapper
{
	private static final Logger LOG = Logger.getLogger(WileyVarArgsKeyMapperImpl.class);

	@Override
	public CacheKey map(final Object keyObj)
	{
		if (keyObj == null)
		{
			LOG.error("Invalid Product Price Cache key: " + keyObj);
			return null;
		}
		else
		{
			if (keyObj instanceof Object[])
			{
				return wrap((Object[]) keyObj);
			}
			else
			{
				return wrap(keyObj);
			}
		}
	}

	/**
	 * Wrap key object with {@link CacheKey} implementation
	 * @param keyObj object to wrap
	 * @return wrapped key
	 */
	protected CacheKey wrap(final Object... keyObj)
	{
		throw new NotSupportedException("Use lookup-method to inject proper cache wrapper");
	}
}
