package com.wiley.core.promotions.actions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;

public class WileySetStudentDiscountGroupActionStrategy extends WileySetOrderDiscountGroupActionStrategy
{
	@Override
	protected UserDiscountGroup getDiscountGroup(final AbstractOrderModel order)
	{
		return UserDiscountGroup.STUDENT;
	}
}
