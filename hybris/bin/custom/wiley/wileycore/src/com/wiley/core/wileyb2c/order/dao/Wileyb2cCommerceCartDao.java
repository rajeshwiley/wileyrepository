package com.wiley.core.wileyb2c.order.dao;

import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


/**
 * Wileyb2c specific methods for cart dao.
 */
public interface Wileyb2cCommerceCartDao extends CommerceCartDao
{

	/**
	 * Returns cart by guid, site and user.
	 *
	 * @param guid
	 * @param site
	 * @param user
	 * @return the last cart.
	 */
	@Nullable
	CartModel getCartForGuidAndStoreAndUser(@Nullable String guid, @Nonnull BaseStoreModel site, @Nonnull UserModel user);

	/**
	 * Returns cart by guid and store
	 *
	 * @param guid
	 * @param baseStore
	 * @return the last cart
	 */
	@Nullable
	CartModel getCartForGuidAndStore(@Nonnull String guid, @Nonnull BaseStoreModel baseStore);

	/**
	 * Returns carts for store and user.
	 *
	 * @param store
	 * @param user
	 * @return list of carts.
	 */
	@Nonnull
	List<CartModel> getCartsForStoreAndUser(@Nonnull BaseStoreModel store, @Nonnull UserModel user);

}
