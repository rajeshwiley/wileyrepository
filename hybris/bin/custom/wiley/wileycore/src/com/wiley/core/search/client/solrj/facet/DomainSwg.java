package com.wiley.core.search.client.solrj.facet;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * JSON Facet API, domain object
 */
public class DomainSwg
{
	private String excludeTags;

	public DomainSwg(final String excludeTags)
	{
		this.excludeTags = excludeTags;
	}

	@JsonProperty
	public String getExcludeTags()
	{
		return excludeTags;
	}

	public void setExcludeTags(final String excludeTags)
	{
		this.excludeTags = excludeTags;
	}
}
