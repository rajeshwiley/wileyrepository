package com.wiley.core.wiley.strategies.cart.validation.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;
import com.wiley.core.wiley.strategies.cart.validation.WileyCartParameterValidator;


public class WileyProductAvailabilityCartParameterValidator implements WileyCartParameterValidator
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyProductAvailabilityCartParameterValidator.class);

	@Resource
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void validateAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		Preconditions.checkNotNull(parameter.getProduct(), "Product should not be empty");
		WileyRestrictionCheckResultDto result = wileyProductRestrictionService.isAvailable(parameter);
		if (!result.isSuccess()) {
			LOG.info("Cart validation failed for product: [{}], error message [{}]",
					parameter.getProduct().getCode(), result.getErrorMessage());
			throw new CommerceCartModificationException(result.getErrorMessage(),
					new ProductNotVisibleException(result.getErrorMessage(), result.getErrorMessageCode(),
							result.getErrorMessageParameters()));
		}
	}
}
