package com.wiley.core.wileyb2c.product.access;

import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;


/**
 * Generating redirect url to access digital product
 *
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cDownloadDigitalProductService
{
	String generateRedirectUrl(@Nonnull OrderEntryModel orderEntry) throws Exception;
}
