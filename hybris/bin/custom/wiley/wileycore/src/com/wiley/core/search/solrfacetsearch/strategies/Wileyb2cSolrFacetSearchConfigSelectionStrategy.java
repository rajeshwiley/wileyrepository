package com.wiley.core.search.solrfacetsearch.strategies;

import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.solrfacetsearch.daos.SolrFacetSearchConfigDao;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cSolrFacetSearchConfigSelectionStrategy implements SolrFacetSearchConfigSelectionStrategy
{
	private String searchConfigName;
	private SolrFacetSearchConfigDao facetSearchConfigDao;

	@Override
	@Nonnull
	public SolrFacetSearchConfigModel getCurrentSolrFacetSearchConfig() throws NoValidSolrConfigException
	{
		final SolrFacetSearchConfigModel solrFacetSearchConfigByName = facetSearchConfigDao.findFacetSearchConfigByName(
				searchConfigName);
		if (solrFacetSearchConfigByName == null)
		{
			throw new NoValidSolrConfigException("Could find solr configuration for name " + searchConfigName);
		}
		return solrFacetSearchConfigByName;
	}

	public String getSearchConfigName()
	{
		return searchConfigName;
	}

	@Required
	public void setSearchConfigName(final String searchConfigName)
	{
		this.searchConfigName = searchConfigName;
	}

	public SolrFacetSearchConfigDao getFacetSearchConfigDao()
	{
		return facetSearchConfigDao;
	}

	@Required
	public void setFacetSearchConfigDao(final SolrFacetSearchConfigDao facetSearchConfigDao)
	{
		this.facetSearchConfigDao = facetSearchConfigDao;
	}
}
