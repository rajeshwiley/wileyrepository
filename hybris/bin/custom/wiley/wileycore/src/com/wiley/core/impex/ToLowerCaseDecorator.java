package com.wiley.core.impex;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;


/**
 * Perform attribute import during impex import process by setting value to lowercase. Initial use case is importing
 * PrincipalGroupRelation for customer with uid that contains capital letters and transformed to lowercase OOTB during import.
 * Example for impex usage: source(uid)[cellDecorator=com.wiley.core.impex.ToLowerCaseDecorator];
 *
 * @author Maksim_Kozich on 19.05.2016.
 */
public class ToLowerCaseDecorator extends AbstractImpExCSVCellDecorator
{
	@Override
	public String decorate(final int i, final Map<Integer, String> map)
	{
		String name = map.get(Integer.valueOf(i));
		if (name != null)
		{
			name = name.toLowerCase();
		}

		return name;
	}
}
