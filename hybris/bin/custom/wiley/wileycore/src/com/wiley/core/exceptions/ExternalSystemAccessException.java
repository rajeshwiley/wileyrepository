package com.wiley.core.exceptions;

/**
 * This exception is thrown if external system doesn't responded (some network error).
 */
public class ExternalSystemAccessException extends ExternalSystemException
{
	public ExternalSystemAccessException(final String message)
	{
		super(message);
	}

	public ExternalSystemAccessException(final Throwable cause)
	{
		super(cause);
	}

	public ExternalSystemAccessException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
