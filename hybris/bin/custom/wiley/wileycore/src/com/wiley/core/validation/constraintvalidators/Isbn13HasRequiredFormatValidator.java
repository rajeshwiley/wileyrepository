package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.Isbn13HasRequiredFormat;


public class Isbn13HasRequiredFormatValidator
		extends AbstractWileyProductConstraintValidator<Isbn13HasRequiredFormat>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validateIsbn13HasRequiredFormat(productModel.getFeatures());
	}
}
