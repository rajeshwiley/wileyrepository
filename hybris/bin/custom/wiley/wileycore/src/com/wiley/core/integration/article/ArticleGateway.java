package com.wiley.core.integration.article;

import javax.annotation.Nonnull;

import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.core.integration.article.service.ArticleService;


/**
 * Don't use gateway directly. Use {@link ArticleService} instead of it
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface ArticleGateway
{
	ArticleDto getArticleData(@Nonnull String articleId);
}
