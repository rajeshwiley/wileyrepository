package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.Map;

import static com.wiley.core.search.solrfacetsearch.WileyFacetSearchService.RANGE_PATTERN;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cFacetValueDisplayNameProvider extends AbstractFacetValueDisplayNameProvider
{
	@Override
	public String getDisplayName(final SearchQuery searchQuery, final IndexedProperty indexedProperty, final String facetValue)
	{
		final Map<String, ValueRangeSet> queryValueRangeSets = indexedProperty.getQueryValueRangeSets();
		for (final ValueRangeSet valueRangeSet : queryValueRangeSets.values())
		{
			for (final ValueRange valueRange : valueRangeSet.getValueRanges())
			{
				final String rangeValue = String.format(RANGE_PATTERN, valueRange.getFrom(), valueRange.getTo());
				if (rangeValue.equals(facetValue))
				{
					return valueRange.getName();
				}
			}
		}
		return null;
	}
}
