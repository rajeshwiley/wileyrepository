package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.externaltax.strategies.WileyTaxCodeStrategy;


public class WileyJournalDhIdTaxCodeStrategy implements WileyTaxCodeStrategy
{
	private static final String JOURNAL_FEATURE_CODE = "asClassificationCatalog/1.0/journal.journal";

	@Resource
	private ClassificationService classificationService;

	@Override
	public Optional<String> getTaxCode(final AbstractOrderEntryModel entry)
	{
		Optional<String> taxCode = Optional.empty();

		final FeatureList features = classificationService.getFeatures(entry.getProduct());
		final Feature feature = features.getFeatureByCode(JOURNAL_FEATURE_CODE);
		if (feature != null && feature.getValue() != null)
		{
			final FeatureValue featureValue = feature.getValue();
			if (featureValue.getValue() instanceof ClassificationAttributeValueModel)
			{
				taxCode = Optional.ofNullable(((ClassificationAttributeValueModel) featureValue.getValue()).getCode());
			}
		}
		return taxCode;
	}
}
