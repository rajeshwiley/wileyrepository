package com.wiley.core.wileyb2c.product;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.wiley.core.enums.WileyProductSubtypeEnum;


/**
 * {@link Wileyb2cCourseProductService} implementation
 */
public class Wileyb2cCourseProductServiceImpl implements Wileyb2cCourseProductService
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cCourseProductServiceImpl.class);
	private static final boolean ACTIVE_REFERENCES_ONLY = true;

	@Resource
	private ProductReferenceService productReferenceService;

	@Nonnull
	@Override
	public ProductModel getReferencedEprofProduct(@Nonnull final ProductModel courseProduct)
	{
		Preconditions.checkNotNull(courseProduct, "Parameter 'courseProduct' should not be null");
		Preconditions.checkArgument(courseProduct.getSubtype() == WileyProductSubtypeEnum.COURSE,
				"Product 'courseProduct' should have COURSE subtype");

		final Collection<ProductReferenceModel> eprofProductReferenceList =
				productReferenceService.getProductReferencesForTargetProduct(courseProduct,
						ProductReferenceTypeEnum.WILEY_PLUS_COURSE, ACTIVE_REFERENCES_ONLY);

		Preconditions.checkState(CollectionUtils.isNotEmpty(eprofProductReferenceList) && eprofProductReferenceList.size() == 1,
				String.format(
						"EPROF product should have one product reference to target course product [%s]. Found list [%s]",
						courseProduct.getCode(), eprofProductReferenceList)
		);

		return eprofProductReferenceList.iterator().next().getSource();
	}

	@Nonnull
	@Override
	public List<ProductModel> getReferencedBundleProducts(@Nonnull final ProductModel eprofProduct,
			@Nonnull final ProductModel courseProduct)
	{

		Preconditions.checkNotNull(eprofProduct, "Parameter 'eprofProduct' should not be null");
		Preconditions.checkArgument(eprofProduct.getSubtype() == WileyProductSubtypeEnum.COURSE,
				"Product 'eprofProduct' should have COURSE subtype");
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		Preconditions.checkArgument(!eprofProduct.getPurchasable(), "Product 'eprofProduct' should be not purchasable");
		Preconditions.checkNotNull(courseProduct, "Parameter 'courseProduct' should not be null");
		Preconditions.checkArgument(courseProduct.getSubtype() == WileyProductSubtypeEnum.COURSE,
				"Product 'courseProduct' should have COURSE subtype");

		Collection<ProductReferenceModel> eprofProductSetReferenceList =
				productReferenceService.getProductReferencesForSourceProduct(eprofProduct,
						ProductReferenceTypeEnum.WILEY_PLUS_PRODUCT_SET, ACTIVE_REFERENCES_ONLY);


		if (CollectionUtils.isEmpty(eprofProductSetReferenceList))
		{
			return Collections.emptyList();
		}

		final List<ProductModel> bundleProducts = new ArrayList<>();
		for (final ProductReferenceModel eprofProductSetReference : eprofProductSetReferenceList)
		{
			final ProductModel bundleProduct = eprofProductSetReference.getTarget();

			if (isBoundBySetComponentReference(bundleProduct, courseProduct))
			{
				bundleProducts.add(bundleProduct);
			}
			else
			{
				LOG.warn("Bundle product [{}] hasn't any reference to cource product [{}]", bundleProduct.getCode(),
						courseProduct.getCode());
			}
		}
		return bundleProducts;
	}

	/**
	 * Method checks whether bundle product has PRODUCT_SET_COMPONENT reference to course.
	 *
	 * @param bundleProduct
	 * 		Bundle product, source product in reference
	 * @param courseProduct
	 * 		Course product, target product in reference
	 * @return True, if reference exists, otherwise returns false
	 */
	private boolean isBoundBySetComponentReference(final ProductModel bundleProduct, final ProductModel courseProduct)
	{
		Collection<ProductReferenceModel> bundleToCourseReferenceList =
				productReferenceService.getProductReferencesForSourceAndTarget(bundleProduct, courseProduct,
						ACTIVE_REFERENCES_ONLY);

		return bundleToCourseReferenceList.stream()
				.anyMatch(reference -> ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT == reference.getReferenceType());
	}



}
