package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cResolveClassificationAttributeStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public abstract class AbstractWileyb2cResolveClassificationAttributeStrategy
		implements Wileyb2cResolveClassificationAttributeStrategy
{
	@Resource
	private ClassificationService classificationService;

	@Override
	public String resolveAttribute(final ProductModel product, final Wileyb2cClassificationAttributes attribute)
	{
		final Feature feature = getFeatureByAttribute(product, attribute);
		if (feature == null)
		{
			return StringUtils.EMPTY;
		}
		return processFeature(feature);
	}

	@Override
	public <T> T resolveAttributeValue(final ProductModel product, final Wileyb2cClassificationAttributes attribute)
	{
		final Feature feature = getFeatureByAttribute(product, attribute);
		if (feature != null)
		{
			return (T) feature.getValue().getValue();
		}
		return null;
	}

	protected Feature getFeatureByAttribute(final ProductModel product, final Wileyb2cClassificationAttributes attribute)
	{
		final FeatureList features = classificationService.getFeatures(product);
		if (features != null)
		{
			for (final Feature feature : features)
			{
				if (feature.getClassAttributeAssignment().getClassificationAttribute().getCode().equals(attribute.getCode())
						&& feature.getValue() != null)
				{
					return feature;
				}
			}
		}
		return null;
	}

	@Override
	public boolean apply(final Wileyb2cClassificationAttributes attribute)
	{
		return getAttributes().contains(attribute);
	}

	protected abstract String processFeature(Feature feature);

	protected abstract Set<Wileyb2cClassificationAttributes> getAttributes();

}
