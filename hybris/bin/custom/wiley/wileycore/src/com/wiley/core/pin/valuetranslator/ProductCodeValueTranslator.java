package com.wiley.core.pin.valuetranslator;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.AbstractValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkArgument;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


/**
 * Pin import specific value translator to fetch productCode by isbn and optional term
 * by using {@code DefaultProductCodeValueTranslatorAdapter}.
 */
public class ProductCodeValueTranslator extends AbstractValueTranslator
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductCodeValueTranslator.class);
	private static final String DEFAULT_ADAPTER = "productCodeValueTranslatorAdapter";

	@Override
	public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException
	{
		try
		{
			String catalogId = this.getColumnDescriptor().getDescriptorData().getModifier("catalogId");
			String catalogVersion = this.getColumnDescriptor().getDescriptorData().getModifier("catalogVersion");

			validateParameterNotNull(catalogId, "CatalogId must not be null.");
			validateParameterNotNull(catalogVersion, "CatalogVersion must not be null.");
			checkArgument(isNotBlank(valueExpr), "Value for this column must not be blank");

			return getAdapter().importValue(valueExpr, toItem, catalogId, catalogVersion);
		}
		catch (Exception e)
		{
			LOG.error("Error occurred during importing value expression: [" + valueExpr + "]", e);
			setError();
			throw new JaloInvalidParameterException(e, 0);
		}
	}

	@Override
	public String exportValue(final Object objectToExport) throws JaloInvalidParameterException
	{
		return getAdapter().exportValue(objectToExport);
	}

	private ProductCodeValueTranslatorAdapter getAdapter()
	{
		String adapter = this.getColumnDescriptor().getDescriptorData().getModifier("adapter");
		if (adapter == null) {
			adapter = DEFAULT_ADAPTER;
		}
		return Registry.getApplicationContext().getBean(adapter, ProductCodeValueTranslatorAdapter.class);
	}
}
