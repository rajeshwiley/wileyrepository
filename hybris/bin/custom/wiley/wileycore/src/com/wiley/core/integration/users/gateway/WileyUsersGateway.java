package com.wiley.core.integration.users.gateway;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.validation.constraints.NotNull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface WileyUsersGateway
{
	String HEADER_NEW_PASSWORD = "header-NewPassword";
	String HEADER_OLD_PASSWORD = "header-OldPassword";
	String HEADER_USER = "header-user";

	Boolean updateUserId(@Payload String newUid, @Header(HEADER_NEW_PASSWORD) String password,
			@Header(HEADER_USER) UserModel user);

	Boolean authenticate(@Payload UserModel user, @Header(HEADER_NEW_PASSWORD) String password);

	Boolean updatePassword(@NotNull @Header(HEADER_USER) UserModel user,
			@NotNull @Header(HEADER_OLD_PASSWORD) String oldPassword,
			@NotNull @Payload String newPassword);

	CustomerModel searchCustomerByEmail(@Payload String userId);

}
