package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.AbstractSiteMapGenerator;
import de.hybris.platform.acceleratorservices.sitemap.renderer.SiteMapContext;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commons.model.renderer.RendererTemplateModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;

import com.wiley.core.locale.WileyLocaleService;


public abstract class AbstractWileyb2cSiteMapGenerator<T> extends AbstractSiteMapGenerator<T>
{
	private static final String SITE_MAP_CONTEXT_BEAN_ID = "wileyb2cSiteMapContext";
	private ApplicationContext applicationContext;
	private WileyLocaleService wileyLocaleService;
	private I18NService i18nService;

	public File render(final CMSSiteModel site, final CurrencyModel currencyModel, final LanguageModel languageModel,
			final RendererTemplateModel rendererTemplateModel, final List<T> models, final String filePrefix,
			final Integer index) throws
			IOException
	{
		final String prefix = (index != null) ? String.format(filePrefix + "-%s-%s-%s-", languageModel.getIsocode(),
				currencyModel.getIsocode(), index) : String.format(filePrefix + "-%s-%s-", languageModel.getIsocode(),
				currencyModel.getIsocode());
		final File siteMap = File.createTempFile(prefix, ".xml");

		final ImpersonationContext context = new ImpersonationContext();
		context.setSite(site);
		context.setCurrency(currencyModel);
		context.setLanguage(languageModel);

		return getImpersonationService().executeInContext(context, () ->
		{
			setEncodedLocale(languageModel);
			i18nService.setLocalizationFallbackEnabled(true);

			final List<SiteMapUrlData> siteMapUrlDataList = getSiteMapUrlData(models);
			validateSiteMapUrlData(siteMapUrlDataList);
			final SiteMapContext siteMapContext = (SiteMapContext) applicationContext.getBean(SITE_MAP_CONTEXT_BEAN_ID);
			siteMapContext.init(site, getSiteMapPageEnum());
			siteMapContext.setSiteMapUrlData(siteMapUrlDataList);
			final BufferedWriter output = new BufferedWriter(new FileWriter(siteMap));
			try
			{
				// the template media is loaded only for english language.
				getCommonI18NService().setCurrentLanguage(getCommonI18NService().getLanguage("en"));
				getRendererService().render(rendererTemplateModel, siteMapContext, output);
			}
			finally
			{
				IOUtils.closeQuietly(output);
			}

			return siteMap;
		});
	}

	protected void validateSiteMapUrlData(final List<SiteMapUrlData> siteMapUrlDataList)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("siteMapUrlDataList", siteMapUrlDataList);
		siteMapUrlDataList.forEach(siteMapUrlData ->
		{
			ServicesUtil.validateParameterNotNullStandardMessage("siteMapUrlData", siteMapUrlData);
			ServicesUtil.validateParameterNotNullStandardMessage("siteMapUrlData.loc", siteMapUrlData.getLoc());
		});
	}

	private void setEncodedLocale(final LanguageModel languageModel)
	{
		wileyLocaleService.setEncodedLocale(languageModel.getIsocode().replace("_", "-").toLowerCase());
	}

	@Required
	@Override
	public void setApplicationContext(final ApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}

	@Required
	public void setWileyLocaleService(final WileyLocaleService wileyLocaleService)
	{
		this.wileyLocaleService = wileyLocaleService;
	}

	@Required
	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}
}
