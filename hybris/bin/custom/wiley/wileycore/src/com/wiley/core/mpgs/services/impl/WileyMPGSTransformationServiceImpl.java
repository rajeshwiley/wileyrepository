package com.wiley.core.mpgs.services.impl;


import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSTransformationServiceImpl implements WileyTransformationService
{
	@Override
	public String transformStatusIfSuccessful(@Nonnull final String status)
	{
		ServicesUtil.validateParameterNotNull(status, "[status] can't be null.");
		return WileyMPGSConstants.RETURN_STATUS_SUCCESS.equalsIgnoreCase(status) ? TransactionStatus.ACCEPTED.name() : status;
	}

	@Override
	public Date transformStringToDate(@Nonnull final String date)
	{
		ServicesUtil.validateParameterNotNull(date, "[date] can't be null.");

		DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
		TemporalAccessor accessor = timeFormatter.parse(date);
		return Date.from(Instant.from(accessor));
	}
}
