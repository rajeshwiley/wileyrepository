package com.wiley.core.product.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.util.interceptor.AbstractCatalogAwareInterceptor;


/**
 * Created by Uladzimir_Barouski on 5/25/2016.
 */
public class ProductIsbnMandatoryValidateInterceptor extends AbstractCatalogAwareInterceptor
		implements ValidateInterceptor<ProductModel>
{
	private static final Logger LOG = Logger.getLogger(ProductIsbnMandatoryValidateInterceptor.class);

	@Override
	public void onValidate(final ProductModel product, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		LOG.debug("Resolving [Validate] Isbn Not Empty interceptor for product with code [" + product.getCode() + "]");
		if (isProductWithRestriction(product) && StringUtils.isBlank(product.getIsbn())) {
			throw new InterceptorException("ISBN field required");
		}
	}
}
