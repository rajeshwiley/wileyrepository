package com.wiley.core.cms.service.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.cms.service.WileyCMSComponentService;


/**
 * Created by Sergiy_Mishkovets on 1/19/2018.
 */
public class WileyCMSComponentServiceImpl extends DefaultCMSComponentService implements WileyCMSComponentService
{
	private static final String COMPONENT_NOT_FOUND_MESSAGE =
			"No AbstractCMSComponentModel found for componentId [%s] and catalogVersion [%s]";

	@Resource
	private CatalogService catalogService;

	@Override
	public AbstractCMSComponentModel getAbstractCMSComponentForCatalogVersion(final String componentId, final String version)
			throws CMSItemNotFoundException
	{
		List<AbstractCMSComponentModel> components = this.getCmsComponentDao().findCMSComponentsByIdAndCatalogVersions(
				componentId, this.catalogService.getSessionCatalogVersions());
		Optional<AbstractCMSComponentModel> component = components.stream()
				.filter(c -> c.getCatalogVersion().getVersion().equals(version))
				.findFirst();

		return component.orElseThrow(
				() -> new CMSItemNotFoundException(String.format(COMPONENT_NOT_FOUND_MESSAGE, componentId, version)));
	}
}
