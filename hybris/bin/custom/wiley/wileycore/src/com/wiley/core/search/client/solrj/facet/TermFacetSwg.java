package com.wiley.core.search.client.solrj.facet;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * JSON Facet API, term facet object
 */
public class TermFacetSwg
{
	public static final String DEFAULT_TYPE = "terms";
	public static final String ASC = "asc";
	public static final String DESC = "desc";
	private String type;
	private String field;
	private DomainSwg domain;
	private int mincount;
	private int limit;
	private Map<String, String> facet = new HashMap<>();
	private Map<String, String> sort = new HashMap<>();

	public TermFacetSwg(final String field, final int mincount, final int limit)
	{
		this.type = DEFAULT_TYPE;
		this.field = field;
		this.mincount = mincount;
		this.limit = limit;
	}

	@JsonProperty
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	@JsonProperty
	public String getField()
	{
		return field;
	}

	public void setField(final String field)
	{
		this.field = field;
	}

	@JsonProperty
	public DomainSwg getDomain()
	{
		return domain;
	}

	public void setDomain(final DomainSwg domain)
	{
		this.domain = domain;
	}

	@JsonProperty
	public int getMincount()
	{
		return mincount;
	}

	public void setMincount(final int mincount)
	{
		this.mincount = mincount;
	}

	@JsonProperty
	public int getLimit()
	{
		return limit;
	}

	public void setLimit(final int limit)
	{
		this.limit = limit;
	}

	@JsonProperty
	public Map<String, String> getFacet()
	{
		return facet;
	}

	@JsonProperty
	public Map<String, String> getSort()
	{
		return sort;
	}

	public void setFacet(final Map<String, String> facet)
	{
		this.facet = facet;
	}

}
