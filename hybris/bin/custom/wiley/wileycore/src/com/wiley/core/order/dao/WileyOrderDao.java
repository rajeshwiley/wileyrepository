package com.wiley.core.order.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.daos.OrderDao;

import java.util.List;


public interface WileyOrderDao extends OrderDao
{

	List<OrderModel> findOrdersByGuid(String guid, BaseSiteModel baseSite);

	/**
	 * Get the pre-orders with active base products.
	 *
	 * @return the list of pre-orders with active products
	 */
	List<OrderModel> findPreOrdersWithActiveProducts();
}
