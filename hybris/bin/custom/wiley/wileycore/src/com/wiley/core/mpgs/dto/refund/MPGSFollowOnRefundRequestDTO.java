package com.wiley.core.mpgs.dto.refund;

import com.wiley.core.mpgs.dto.json.Transaction;


public class MPGSFollowOnRefundRequestDTO
{
	private String apiOperation;
	private Transaction transaction;

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}

	public String getApiOperation()
	{
		return apiOperation;
	}

	public void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}
}
