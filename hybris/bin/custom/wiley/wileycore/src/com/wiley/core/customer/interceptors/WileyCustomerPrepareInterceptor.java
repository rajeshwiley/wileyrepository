package com.wiley.core.customer.interceptors;

import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;


/**
 * Created by Maksim_Kozich on 30.12.2015.
 */
public class WileyCustomerPrepareInterceptor implements PrepareInterceptor<CustomerModel> {
    @Resource
    private CustomerNameStrategy customerNameStrategy;

    /**
     * Gets customer name strategy.
     *
     * @return the customer name strategy
     */
    public CustomerNameStrategy getCustomerNameStrategy() {
        return customerNameStrategy;
    }

    /**
     * Sets customer name strategy.
     *
     * @param customerNameStrategy the customer name strategy
     */
    public void setCustomerNameStrategy(final CustomerNameStrategy customerNameStrategy) {
        this.customerNameStrategy = customerNameStrategy;
    }

    /**
     * Fill customer name from first and last name using customerNameStrategy.
     *
     * @param customerModel      the customer model
     * @param interceptorContext the interceptor context
     * @throws InterceptorException the interceptor exception
     */
    @Override
    public void onPrepare(final CustomerModel customerModel, final InterceptorContext interceptorContext)
            throws InterceptorException {
        if (StringUtils.isBlank(customerModel.getName())) {
            customerModel.setName(customerNameStrategy.getName(customerModel.getFirstName(), customerModel.getLastName()));
        } else {
            fillFirstNameLastName(customerModel);
        }
        if (customerModel.getCustomerID() == null) {
            customerModel.setCustomerID(UUID.randomUUID().toString());
        }
    }

    private void fillFirstNameLastName(final CustomerModel customerModel) {
        if (StringUtils.isNotBlank(customerModel.getFirstName())
                && StringUtils.isNotBlank(customerModel.getLastName())) {
            return;
        }
        final String[] names = customerNameStrategy.splitName(customerModel.getName());
        if (names.length >= 1 && StringUtils.isBlank(customerModel.getFirstName())) {
            customerModel.setFirstName(names[0]);
        }
        if (names.length >= 2 && StringUtils.isBlank(customerModel.getLastName())) {
            customerModel.setLastName(names[1]);
        }
    }
}
