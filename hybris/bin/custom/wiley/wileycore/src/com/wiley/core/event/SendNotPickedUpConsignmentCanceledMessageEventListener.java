/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.orderprocessing.events.SendNotPickedUpConsignmentCanceledMessageEvent;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * Listener for SendNotPickedUpConsignmentMessageEvent events.
 */
public class SendNotPickedUpConsignmentCanceledMessageEventListener
		extends AbstractWileySiteEventListener<SendNotPickedUpConsignmentCanceledMessageEvent>
{
	/**
	 * On site event.
	 *
	 * @param sendNotPickedUpConsignmentCanceledMessageEvent
	 * 		the send not picked up consignment canceled message event
	 */
	@Override
	protected void onSiteEvent(
			final SendNotPickedUpConsignmentCanceledMessageEvent sendNotPickedUpConsignmentCanceledMessageEvent)
	{
		final ConsignmentModel consignmentModel = sendNotPickedUpConsignmentCanceledMessageEvent.getProcess().getConsignment();
		final ConsignmentProcessModel consignmentProcessModel = getBusinessProcessService().createProcess(
				"sendNotPickedUpConsignmentCanceledEmailProcess-" + consignmentModel.getCode() + "-" + System.currentTimeMillis(),
				"sendNotPickedUpConsignmentCanceledEmailProcess");
		consignmentProcessModel.setConsignment(consignmentModel);
		getModelService().save(consignmentProcessModel);
		getBusinessProcessService().startProcess(consignmentProcessModel);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final SendNotPickedUpConsignmentCanceledMessageEvent event)
	{
		final AbstractOrderModel order = event.getProcess().getConsignment().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2C.equals(site.getChannel());
	}
}
