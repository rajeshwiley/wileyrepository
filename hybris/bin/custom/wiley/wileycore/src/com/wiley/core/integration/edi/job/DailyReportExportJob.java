package com.wiley.core.integration.edi.job;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.integration.edi.WileyDailyReportExportGateway;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class DailyReportExportJob extends WileyAbstractExportJob
{

	private static final Logger LOG = Logger.getLogger(DailyReportExportJob.class);

	@Resource
	private WileyDailyReportExportGateway wileyDailyReportExportGateway;

	protected void handleExport(final String paymentType, final BaseStoreModel baseStore)
	{
		LOG.info(String.format("Running daily report mailing for store: [%s], payment type: [%s]", baseStore.getUid(),
				paymentType));
		try
		{
			final String dailyReportMailList = configurationService.getConfiguration().getString(
					"wiley.orders.daily.report.to.email.list");
			if (StringUtils.isEmpty(dailyReportMailList))
			{
				LOG.warn("No recipients have been provided for DailyReportExportJob!..");
				return;
			}
			final List<WileyExportProcessModel> wileyExportProcesses =
					orderProvisionStrategy.getExportProcessesReadyForDailyReportExport(baseStore, paymentType);
			wileyDailyReportExportGateway.exportOrders(wileyExportProcesses, baseStore, LocalDateTime.now(), paymentType,
					dailyReportMailList);
		}
		catch (Exception e)
		{
			LOG.error("Exception occurs during running daily report mailing for store: [" + baseStore.getUid() + "]", e);
		}
	}

}