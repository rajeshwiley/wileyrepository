/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;


/**
 * The type Order cancelled event.
 */
public class OrderCancelledEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Order cancelled event.
	 *
	 * @param process
	 * 		the process
	 */
	public OrderCancelledEvent(final OrderProcessModel process)
	{
		super(process);
	}
}
