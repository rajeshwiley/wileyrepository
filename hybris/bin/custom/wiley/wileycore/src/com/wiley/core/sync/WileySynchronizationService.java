package com.wiley.core.sync;

import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobResult;

import java.util.Collection;
import java.util.List;


public interface WileySynchronizationService
{
	CatalogVersionSyncJobModel getCatalogVersionSyncJob(String code);

	CronJobResult performSynchronization(Collection<? extends ItemModel> items, CatalogVersionSyncJobModel syncJobModel);

	<T> List<T> getSynchronizationTargets(T item);

}
