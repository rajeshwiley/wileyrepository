package com.wiley.core.pin.service.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.integration.wileycore.WileyCoreGateway;
import com.wiley.core.integration.wileycore.dto.Message;
import com.wiley.core.integration.wileycore.dto.PinResponseCode;
import com.wiley.core.model.PinModel;
import com.wiley.core.pin.service.PinIntegrationService;
import com.wiley.core.pin.service.PinService;

import static java.util.Optional.ofNullable;
import static org.apache.commons.lang.StringUtils.EMPTY;


/**
 * Default implementation of {@code PinIntegrationService}.
 *
 * @author Tamas_Szebeni
 */
public class DefaultPinIntegrationService implements PinIntegrationService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultPinIntegrationService.class);

	@Resource
	private PinService pinService;

	@Resource
	private WileyCoreGateway wileyCoreGateway;

	@Override
	public boolean isPinUsedForOrderPlacement(final AbstractOrderModel orderModel)
	{
		return pinService.isPinUsedForOrder(orderModel);
	}

	@Override
	public boolean validatePinForOrder(final AbstractOrderModel orderModel)
	{
		return notifyWileyCoreSystem(orderModel, PinIntegrationOperation.VALIDATE);
	}

	@Override
	public boolean activatePinForOrder(final AbstractOrderModel orderModel)
	{
		return notifyWileyCoreSystem(orderModel, PinIntegrationOperation.ACTIVATE);
	}

	private boolean notifyWileyCoreSystem(final AbstractOrderModel orderModel,
			final PinIntegrationOperation operation)
	{
		PinModel pinModel = pinService.getPinForOrder(orderModel);
		try
		{
			final AtomicBoolean success = new AtomicBoolean(false);
			UserModel user = orderModel.getUser();
			if (user instanceof CustomerModel)
			{
				CustomerModel customer = (CustomerModel) user;
				String firstName = ofNullable(customer.getFirstName()).orElse(EMPTY);
				String lastName = ofNullable(customer.getLastName()).orElse(EMPTY);
				Message message = operation.apply(wileyCoreGateway,
						new PinParams(pinModel.getCode(), firstName, lastName, orderModel.getSite()));
				message.getPins().getPinresponse().stream().findAny().ifPresent(pinResponse ->
				{
					boolean result = PinResponseCode.SUCCESS == pinResponse.getResponsecode();
					if (!result)
					{
						LOG.error("Operation {} failed for PIN \"{}\" with description \"{}\".", operation,
								pinResponse.getPin(), pinResponse.getResponsedescription());
					}
					success.set(result);
				});
			}
			else
			{
				LOG.error("Could not resolve customer for order \"{}\"", orderModel.getCode());
			}
			return success.get();
		}
		catch (Exception e)
		{
			LOG.error("Operation {} failed for PIN \"{}\" with message \"{}\".", operation, pinModel.getCode(),
					e.getMessage(), e);
			return false;
		}
	}

	private enum PinIntegrationOperation
	{
		VALIDATE((gateway, params) -> gateway.validatePin(params.getCode(), params.getSite())),
		ACTIVATE((gateway, params) -> gateway
				.activatePin(params.getCode(), params.getFirstName(), params.getLastName(), params.getSite()));

		private BiFunction<WileyCoreGateway, PinParams, Message> operation;

		PinIntegrationOperation(final BiFunction<WileyCoreGateway, PinParams, Message> operation)
		{
			this.operation = operation;
		}

		public Message apply(final WileyCoreGateway wileyCoreGateway, final PinParams params)
		{
			return operation.apply(wileyCoreGateway, params);
		}
	}

	private static class PinParams
	{
		private String code;
		private String firstName;
		private String lastName;
		private BaseSiteModel site;

		PinParams(final String code, final String firsrName, final String lastName, final BaseSiteModel site)
		{
			this.code = code;
			this.firstName = firsrName;
			this.lastName = lastName;
			this.site = site;
		}

		public String getCode()
		{
			return code;
		}

		public void setCode(final String code)
		{
			this.code = code;
		}

		public String getFirstName()
		{
			return firstName;
		}

		public void setFirstName(final String firstName)
		{
			this.firstName = firstName;
		}

		public String getLastName()
		{
			return lastName;
		}

		public void setLastName(final String lastName)
		{
			this.lastName = lastName;
		}

		public BaseSiteModel getSite()
		{
			return site;
		}

		public void setSite(final BaseSiteModel site)
		{
			this.site = site;
		}
	}
}
