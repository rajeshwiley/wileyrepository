package com.wiley.core.wiley.order;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.wiley.core.enums.OrderType;


public interface WileyCommerceCartService extends CommerceCartService
{
	/**
	 * Find latest modified cart for the specified site, user and general type.
	 *
	 * @param site
	 * @param user
	 * @return Latest modified general type cart.
	 */
	CartModel getLatestGeneralCartForSiteUser(BaseSiteModel site, UserModel user);

	/**
	 * Find a list of user carts by given type and site.
	 *
	 * @param site
	 * @param user
	 * @return Latest modified general type cart.
	 */
	List<CartModel> getAllCartsForUserAndType(BaseSiteModel site, UserModel user, OrderType type);

	/**
	 * @param cart
	 * 		{@link CartModel} cart to be removed.
	 */
	void removeCart(CartModel cart);
}
