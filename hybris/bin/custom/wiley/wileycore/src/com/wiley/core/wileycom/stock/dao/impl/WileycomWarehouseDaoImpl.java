package com.wiley.core.wileycom.stock.dao.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.store.BaseStoreModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.wiley.core.wileycom.stock.dao.WileycomWarehouseDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomWarehouseDaoImpl extends AbstractItemDao implements WileycomWarehouseDao
{
	private static final String WAREHOUSE_FOR_BASE_STORE_AND_COUNTRY_QUERY =
			"SELECT {w.pk} FROM {BaseStore2WarehouseRel as r JOIN Warehouse as w "
					+ "ON {r.target} = {w.pk} JOIN Country2WarehouseRelation as c on {w.pk} = {c.target}}"
					+ " WHERE {w.default} = ?default AND {r.source} = ?baseStore AND {c.source} = ?country";

	/**
	 * fetch marked as default warehouses that correspond passed country and base store.
	 *
	 * @param baseStore
	 * 		the base store
	 * @param countryModel
	 * 		the country model
	 * @return default warehouses for base store and country
	 */
	public List<WarehouseModel> getDefaultWarehousesForBaseStore(@Nonnull final BaseStoreModel baseStore,
			@Nonnull final CountryModel countryModel)
	{
		validateParameterNotNull(baseStore, "baseStore cannot be null");
		validateParameterNotNull(countryModel, "country cannot be null");
		final Map<String, Object> params = new HashMap<String, Object>(2);
		params.put("baseStore", baseStore);
		params.put("country", countryModel);
		params.put("default", Boolean.TRUE);
		return getFlexibleSearchService().<WarehouseModel> search(WAREHOUSE_FOR_BASE_STORE_AND_COUNTRY_QUERY, params)
				.getResult();
	}
}
