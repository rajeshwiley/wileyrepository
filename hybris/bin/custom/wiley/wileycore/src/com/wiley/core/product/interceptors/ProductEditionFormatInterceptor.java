package com.wiley.core.product.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ProductEditionFormatInterceptor implements ValidateInterceptor<ProductModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductEditionFormatInterceptor.class);

	private static final String[] VALIDATED_CATALOGS = {"welProductCatalog"};

	@Override
	public void onValidate(final ProductModel product, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		LOG.debug("Resolving [Prepare] interceptor for product with code [" + product.getCode() + "]");
		String catalogId = product.getCatalogVersion().getCatalog().getId();
		boolean isCatalogWithRestriction = Stream.of(VALIDATED_CATALOGS).anyMatch(
				catalog -> catalog.equalsIgnoreCase(catalogId)
		);
		if (isCatalogWithRestriction && product.getEditionFormat() == null) {
			throw new InterceptorException(
					String.format("ProductEditionFormat should not be null for catalog: [%s]", catalogId)
			);
		}
	}
}
