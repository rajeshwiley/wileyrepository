package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Populator to fill ship to address
 */
public class ShipToWPGHttpRequestPopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	static final String WPG_CUSTOM_SHIPTO_TITLE_NAME = "WPG_CUSTOM_shipto_title_name";
	static final String WPG_CUSTOM_SHIPTO_FIRST_NAME = "WPG_CUSTOM_shipto_first_name";
	static final String WPG_CUSTOM_SHIPTO_LAST_NAME = "WPG_CUSTOM_shipto_last_name";
	static final String WPG_CUSTOM_SHIPTO_LINE1 = "WPG_CUSTOM_shipto_line1";
	static final String WPG_CUSTOM_SHIPTO_LINE2 = "WPG_CUSTOM_shipto_line2";
	static final String WPG_CUSTOM_SHIPTO_CITY = "WPG_CUSTOM_shipto_city";
	static final String WPG_CUSTOM_SHIPTO_STATE_NAME = "WPG_CUSTOM_shipto_state_name";
	static final String WPG_CUSTOM_SHIPTO_POSTAL_CODE = "WPG_CUSTOM_shipto_postalcode";
	static final String WPG_CUSTOM_SHIPTO_COUNTRY_NAME = "WPG_CUSTOM_shipto_country_name";

	@Override
	public void populate(final CreateSubscriptionRequest createSubscriptionRequest, final PaymentData paymentData)
			throws ConversionException
	{
		CustomerShipToData shipTo = createSubscriptionRequest.getCustomerShipToData();
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_TITLE_NAME, shipTo.getShipToTitleName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_FIRST_NAME, shipTo.getShipToFirstName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_LAST_NAME, shipTo.getShipToLastName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_LINE1, shipTo.getShipToStreet1());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_LINE2, shipTo.getShipToStreet2());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_CITY, shipTo.getShipToCity());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_STATE_NAME, shipTo.getShipToStateName());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_POSTAL_CODE, shipTo.getShipToPostalCode());
		addRequestQueryParam(paymentData, WPG_CUSTOM_SHIPTO_COUNTRY_NAME, shipTo.getShipToCountryName());
	}
}
