package com.wiley.core.healthcheck.checks;

import org.springframework.beans.factory.annotation.Required;

import com.codahale.metrics.health.HealthCheck;
import com.wiley.core.healthcheck.MonitoredGateway;


/**
 * Class is used for performing health check for gateways
 */
public class MonitoredGatewayHealthCheck extends HealthCheck
{
	private MonitoredGateway gateway;

	@Required
	public void setGateway(final MonitoredGateway gateway)
	{
		this.gateway = gateway;
	}

	@Override
	protected Result check() throws Exception
	{
		return gateway.doHealthCheck();
	}
}