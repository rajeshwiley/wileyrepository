package com.wiley.core.wileyb2b.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;

public interface WileyB2BCommerceUserService
{

	/**
	 * Creates customer in hybris and updates it into external system
	 *
	 * @param customerModel
	 * @param createUser
	 * @throws DuplicateUidException
	 */
	void saveCustomer(CustomerModel customerModel, boolean createUser);

	/**
	 * Creates customer
	 * 
	 * @param customerModel
	 * @return
	 */
	B2BCustomerModel createCustomer(B2BCustomerModel customerModel, boolean createUser);

}
