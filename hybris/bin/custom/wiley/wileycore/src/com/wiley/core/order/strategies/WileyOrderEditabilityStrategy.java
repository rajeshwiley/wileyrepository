package com.wiley.core.order.strategies;

import de.hybris.platform.core.model.order.OrderModel;

public interface WileyOrderEditabilityStrategy
{

	boolean isEditable(OrderModel order);
	
	boolean hasAllEditableEntries(OrderModel order);
}
