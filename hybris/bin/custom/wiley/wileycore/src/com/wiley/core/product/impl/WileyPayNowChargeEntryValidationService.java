package com.wiley.core.product.impl;

import de.hybris.platform.subscriptionservices.model.OneTimeChargeEntryModel;
import de.hybris.platform.subscriptionservices.model.impl.DefaultOneTimeChargeEntryValidationService;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;


public class WileyPayNowChargeEntryValidationService extends DefaultOneTimeChargeEntryValidationService
{
	private String payNowBillingTime;

	@Nonnull
	@Override
	public Collection<String> validate(final Collection<OneTimeChargeEntryModel> oneTimeChargeEntries)
	{
		Collection<String> messages = super.validate(oneTimeChargeEntries);
		checkPayNowMandatority(oneTimeChargeEntries, messages);
		return messages;
	}

	private void checkPayNowMandatority(final Collection<OneTimeChargeEntryModel> oneTimeChargeEntries,
			final Collection<String> messages)
	{
		boolean payNowMissed = true;
		if (oneTimeChargeEntries != null)
		{
			payNowMissed = oneTimeChargeEntries.stream().noneMatch(entry ->
					entry.getBillingEvent() != null && payNowBillingTime.equalsIgnoreCase(
							entry.getBillingEvent().getCode()));
		}
		if (payNowMissed)
		{
			messages.add(Localization.getLocalizedString("subscriptionpriceplan.validation.paynow.mandatory", new Object[0]));
		}
	}

	@Required
	public void setPayNowBillingTime(final String value)
	{
		payNowBillingTime = value;
	}
}
