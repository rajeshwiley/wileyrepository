package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * Created by Uladzimir_Barouski on 5/25/2017.
 */
public class Wileyb2cProductDescriptionValueProvider extends AbstractWileycomValueProvider<String>
{
	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		ProductModel product = ((VariantProductModel) model).getBaseProduct();
		String description = product.getDescriptionEnriched();
		if (StringUtils.isEmpty(description))
		{
			description	= product.getDescription();
		}
		return Collections.singletonList(description);
	}
}
