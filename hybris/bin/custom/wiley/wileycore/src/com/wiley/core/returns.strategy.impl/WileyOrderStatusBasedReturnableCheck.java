package com.wiley.core.returns.strategy.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Returnable check that checks if order is in incorrect orders status to restrict refund.
 */
public class WileyOrderStatusBasedReturnableCheck implements ReturnableCheck
{

	private List<OrderStatus> allowedOrderStatuses;

	@Override
	public boolean perform(final OrderModel orderModel, final AbstractOrderEntryModel abstractOrderEntryModel, final long l)
	{
		if (CollectionUtils.isNotEmpty(allowedOrderStatuses)) {
			if (!allowedOrderStatuses.contains(orderModel.getStatus())) {
				return false;
			}
		}
		return true;
	}

	@Required
	public void setAllowedOrderStatuses(final List<OrderStatus> allowedOrderStatuses)
	{
		this.allowedOrderStatuses = allowedOrderStatuses;
	}
}
