/**
 *
 */
package com.wiley.core.order.hook;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.hook.CommerceCartCalculationMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.event.WileyOrderEntryEvent;
import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 *
 */
public class WileyCartValidateProductRestrictionsMethodHook implements CommerceCartCalculationMethodHook
{
	private static final String SUBSCRIPTION_CART_ENTRIES_NOT_AVAILABLE_MESSAGE_KEY =
			"checkout.multi.subscription.term.unavailable";
	@Autowired
	private ModelService modelService;
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;
	@Autowired
	private WileySubscriptionService wileySubscriptionService;
	@Resource
	private EventService eventService;
	@Resource
	private TransactionTemplate transactionTemplate;

	private boolean sendOrderEntryRemovalEvent;
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void afterCalculate(final CommerceCartParameter parameter)
	{
		// Do nothing
	}

	@Override
	public void beforeCalculate(final CommerceCartParameter parameter)
	{

		final List<CommerceCartModification> modifications = validateCart(parameter);
		final List<CommerceCartModification> failedModifications = modifications.stream()
				.filter(modification -> !modification.getStatusCode().equals(CommerceCartModificationStatus.SUCCESS))
				.collect(Collectors.toList());

		wileyFailedCartModificationsStorageService.pushAll(failedModifications);

	}

	private List<CommerceCartModification> validateCart(final CommerceCartParameter parameter)
	{
		final CartModel cartModel = parameter.getCart();

		if (cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{

			return cartModel.getEntries().stream().map(
					orderEntryModel -> validateCartEntry(cartModel, (CartEntryModel) orderEntryModel)).collect(
					Collectors.toList());
		}
		else
		{
			return Collections.emptyList();
		}
	}

	private CommerceCartModification validateCartEntry(final CartModel cartModel, final CartEntryModel cartEntryModel)
	{
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setProduct(cartEntryModel.getProduct());
		commerceCartParameter.setSubscriptionTerm(cartEntryModel.getSubscriptionTerm());
		if (wileyProductRestrictionService.isAvailable(commerceCartParameter).isSuccess())
		{
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			modification.setQuantityAdded(cartEntryModel.getQuantity());
			modification.setQuantity(cartEntryModel.getQuantity());
			modification.setEntry(cartEntryModel);

			return modification;
		}
		else
		{
			return removeCartEntryModel(cartModel, cartEntryModel);
		}
	}

	private CommerceCartModification removeCartEntryModel(final CartModel cartModel, final CartEntryModel cartEntryModel)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		if (wileySubscriptionService.isSubscriptionCartEntry(cartEntryModel))
		{
			modification.setMessage(SUBSCRIPTION_CART_ENTRIES_NOT_AVAILABLE_MESSAGE_KEY);
			modification.setMessageParameters(new Object[] { cartEntryModel.getSubscriptionTerm().getName(),
					cartEntryModel.getProduct().getName() });
		}
		else
		{
			modification.setMessage(WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
			Object[] parameters = new Object[] { cartEntryModel.getProduct().getName() };
			if (StringUtils.isBlank(cartEntryModel.getProduct().getName()))
			{
				final ProductModel product = cartEntryModel.getProduct();
				if (product instanceof VariantProductModel)
				{
					final VariantProductModel variantProduct = (VariantProductModel) product;
					parameters = new Object[] { variantProduct.getBaseProduct().getName() };
				}
			}
			modification.setMessageParameters(parameters);
		}
		modification.setMessageType(CartModificationMessageType.INFO);
		modification.setQuantityAdded(0);
		modification.setQuantity(0);

		final CartEntryModel entry = new CartEntryModel();
		entry.setProduct(cartEntryModel.getProduct());

		modification.setEntry(entry);

		if (sendOrderEntryRemovalEvent)
		{
			transactionTemplate.execute(new TransactionCallbackWithoutResult()
			{
				@Override
				protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
				{
					WileyOrderEntryEvent wileyOrderEntryEvent = new WileyOrderEntryEvent("DELETED", cartEntryModel);
					eventService.publishEvent(wileyOrderEntryEvent);
					modelService.remove(cartEntryModel);
				}
			});
		}
		else
		{
			modelService.remove(cartEntryModel);
		}
		modelService.refresh(cartModel);

		return modification;
	}

	@Required
	public void setWileyProductRestrictionService(final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}

	@Required
	public void setSendOrderEntryRemovalEvent(final boolean sendOrderEntryRemovalEvent)
	{
		this.sendOrderEntryRemovalEvent = sendOrderEntryRemovalEvent;
	}

	public void setTransactionTemplate(final TransactionTemplate transactionTemplate)
	{
		this.transactionTemplate = transactionTemplate;
	}
}
