package com.wiley.core.jobs;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.wiley.core.model.WileyRelativeUrlCronJobModel;
import com.wiley.core.servicelayer.WileyItemService;

import static org.apache.commons.lang.LocaleUtils.toLocale;


/**
 * Job change absolute urls to relative
 */
public class WileyRelativeUrlJobPerformable extends AbstractJobPerformable<WileyRelativeUrlCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyRelativeUrlJobPerformable.class);

	@Resource
	private ModelService modelService;

	@Resource
	private WileyItemService wileyItemService;

	@Override
	public PerformResult perform(final WileyRelativeUrlCronJobModel cronJob)
	{
		Stopwatch stopwatch = Stopwatch.createStarted();
		LOG.info("Starting job with code={} to change absolute urls to relative", cronJob.getCode());

		final Set<LanguageModel> languages = cronJob.getBaseStore().getLanguages();
		final Locale[] locales = languages.stream()
				.map(language -> toLocale(language.getIsocode()))
				.collect(Collectors.toList())
				.toArray(new Locale[languages.size()]);

		final Map<String, String> replacement = cronJob.getReplacement();

		LOG.info("Looking for values to replace: [{}]", Arrays.toString(replacement.entrySet().toArray()));

		for (AttributeDescriptorModel descriptor : cronJob.getDescriptors())
		{
			final Collection<ItemModel> items = wileyItemService.getCatalogAwareItems(descriptor.getEnclosingType(),
					cronJob.getCatalogVersions());
			final String qualifier = descriptor.getQualifier();
			LOG.info("Checking {} models for {}.{} descriptor (including {} subtypes)", items.size(),
					descriptor.getEnclosingType().getCode(), qualifier, descriptor.getEnclosingType().getCode());

			for (ItemModel item : items)
			{
				if (descriptor.getLocalized())
				{
					modify(item, qualifier, replacement, locales);
				}
				else
				{
					modify(item, qualifier, replacement);
				}
			}
			modelService.saveAll(items);

			if (clearAbortRequestedIfNeeded(cronJob))
			{
				LOG.info("Requested abort for cron job with code={}", cronJob.getCode());
				return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
			}

		}

		LOG.info("Successfully checked items and finished job with code={} in {}", cronJob.getCode(), stopwatch.stop());
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	private void modify(final ItemModel item, final String qualifier, final Map<String, String> replacement)
	{

		final String value = modelService.getAttributeValue(item, qualifier);
		if (StringUtils.isNotEmpty(value))
		{
			final String updatedValue = replaceAll(value, replacement);
			if (!value.equals(updatedValue))
			{
				LOG.info("Updating [{}.{}] value of item with pk=[{}]. Old value is [{}], new value is [{}].",
						item.getItemtype(), qualifier, item.getPk(), value, updatedValue);
				modelService.setAttributeValue(item, qualifier, updatedValue);
			}
		}
	}

	private void modify(final ItemModel item, final String qualifier, final Map<String, String> replacement,
			final Locale[] locales)
	{
		final Map<Locale, String> values = new HashMap<>(modelService.getAttributeValues(item, qualifier, locales));

		if (MapUtils.isEmpty(values))
		{
			return;
		}

		final boolean[] changed = { false };

		values.entrySet().stream().filter(entry -> StringUtils.isNotEmpty(entry.getValue())).forEach(entry ->
		{
			final String value = entry.getValue();
			final String updatedValue = replaceAll(value, replacement);
			if (!value.equals(updatedValue))
			{
				LOG.info("Updating [{}.{}({})] value of item with pk=[{}]. Old value is [{}], new value is [{}].",
						item.getItemtype(), qualifier, entry.getKey(), item.getPk(), value, updatedValue);
				entry.setValue(updatedValue);
				changed[0] = true;
			}

		});

		if (changed[0])
		{
			modelService.setAttributeValue(item, qualifier, values);
		}
	}

	private String replaceAll(final String value, final Map<String, String> replace)
	{
		final StrBuilder builder = new StrBuilder(value);
		replace.forEach((key, val) -> builder.replaceAll(key, val));
		return builder.toString();
	}

	@Override
	public boolean isAbortable()
	{
		return true;
	}
}
