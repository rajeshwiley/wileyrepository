package com.wiley.core.promotions.actions.rao;

import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.AbstractRuleExecutableSupport;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.RAOAction;

import java.util.Map;


public class WileySetOrderDiscountGroupRAOAction extends AbstractRuleExecutableSupport implements RAOAction
{
	@Override
	protected void validateParameters(final Map<String, Object> parameters)
	{
	}

	@Override
	public boolean performActionInternal(final RuleActionContext context)
	{
		final CartRAO cartRao = context.getCartRao();
		final RuleEngineResultRAO result = context.getRuleEngineResultRao();

		final WileySetOrderDiscountGroupRAO applyPartnerDiscountRAO = new WileySetOrderDiscountGroupRAO();
		getRaoUtils().addAction(cartRao, applyPartnerDiscountRAO);
		result.getActions().add(applyPartnerDiscountRAO);

		setRAOMetaData(context, applyPartnerDiscountRAO);
		context.insertFacts(context, applyPartnerDiscountRAO);
		context.updateFacts(context, cartRao);

		return true;
	}
}