package com.wiley.core.mpgs.services;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyTnsMerchantModel;


public interface WileyMPGSMerchantService
{
	String getMerchantID(AbstractOrderModel abstractOrderModel);

	WileyTnsMerchantModel getMerchant(AbstractOrderModel abstractOrderModel);

	boolean isMerchantValid(@Nonnull AbstractOrderModel abstractOrder);
}
