package com.wiley.core.wileyb2c.subscription.dao;

import java.util.List;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.dao.WileySubscriptionDao;


public interface Wileyb2cSubscriptionDao extends WileySubscriptionDao
{
	/**
	 * Dao to get subscription by customer, product
	 *
	 * @param productCode
	 * @param customerCode
	 * @return
	 */
	List<WileySubscriptionModel> findSubscriptionsByProductAndCustomer(String productCode, String customerCode);
}
