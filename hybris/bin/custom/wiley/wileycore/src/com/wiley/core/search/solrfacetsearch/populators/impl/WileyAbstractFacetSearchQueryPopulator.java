package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetSearchQueryOperatorTranslator;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.search.client.solrj.WileySolrQuery;


/**
 * OOTB copy of {@link AbstractFacetSearchQueryPopulator}. Changed the second type parameter to {@link WileySolrQuery}.
 */
public abstract class WileyAbstractFacetSearchQueryPopulator implements Populator<SearchQueryConverterData, WileySolrQuery>
{
	public static final String GROUP_FACET_PATTERN = "unique(%s)";
	public static final String GROUP_FACET = "group";

	private FieldNameTranslator fieldNameTranslator;
	private SearchQuery.Operator defaultOperator;
	private FacetSearchQueryOperatorTranslator facetSearchQueryOperatorTranslator;

	public FieldNameTranslator getFieldNameTranslator()
	{
		return this.fieldNameTranslator;
	}

	@Required
	public void setFieldNameTranslator(final FieldNameTranslator fieldNameTranslator)
	{
		this.fieldNameTranslator = fieldNameTranslator;
	}

	public SearchQuery.Operator getDefaultOperator()
	{
		return this.defaultOperator;
	}

	@Required
	public void setDefaultOperator(final SearchQuery.Operator defaultOperator)
	{
		this.defaultOperator = defaultOperator;
	}

	public FacetSearchQueryOperatorTranslator getFacetSearchQueryOperatorTranslator()
	{
		return this.facetSearchQueryOperatorTranslator;
	}

	@Required
	public void setFacetSearchQueryOperatorTranslator(final FacetSearchQueryOperatorTranslator facetSearchQueryOperatorTranslator)
	{
		this.facetSearchQueryOperatorTranslator = facetSearchQueryOperatorTranslator;
	}

	protected String convertQueryField(final SearchQuery searchQuery, final QueryField queryField)
	{
		String convertedField = this.fieldNameTranslator.translate(searchQuery, queryField.getField(),
				FieldNameProvider.FieldType.INDEX);
		StringBuilder query = new StringBuilder();
		query.append(convertedField);
		query.append(':');
		String operator;
		String separator;
		if (queryField.getValues().size() == 1)
		{
			String convertedValues = queryField.getValues().iterator().next();
			operator = ClientUtils.escapeQueryChars(convertedValues);
			separator = this.facetSearchQueryOperatorTranslator.translate(operator, queryField.getQueryOperator());
			query.append(separator);
		}
		else
		{
			ArrayList convertedValues = new ArrayList(queryField.getValues().size());
			Iterator<String> valueIterator = queryField.getValues().iterator();

			String convertedValue;
			while (valueIterator.hasNext())
			{
				operator = valueIterator.next();
				convertedValue = ClientUtils.escapeQueryChars(operator);
				convertedValues.add(this.facetSearchQueryOperatorTranslator.translate(convertedValue,
						queryField.getQueryOperator()));
			}

			SearchQuery.Operator operator1 = this.resolveQueryFieldOperator(searchQuery, queryField);
			separator = " " + operator1 + " ";
			convertedValue = StringUtils.join(convertedValues, separator);
			query.append('(');
			query.append(convertedValue);
			query.append(')');
		}

		return query.toString();
	}

	protected String convertRawQuery(final SearchQuery searchQuery, final RawQuery rawQuery)
	{
		StringBuilder query = new StringBuilder();
		query.append('(');
		if (rawQuery.getField() != null)
		{
			String convertedField = this.fieldNameTranslator.translate(searchQuery, rawQuery.getField(),
					FieldNameProvider.FieldType.INDEX);
			query.append(convertedField);
			query.append(":(");
		}

		query.append(rawQuery.getQuery());
		if (rawQuery.getField() != null)
		{
			query.append(')');
		}

		query.append(')');
		return query.toString();
	}

	protected SearchQuery.Operator resolveQueryFieldOperator(final SearchQuery searchQuery, final QueryField queryField)
	{
		return queryField.getOperator() != null ? queryField.getOperator() : this.resolveOperator(searchQuery);
	}

	protected SearchQuery.Operator resolveOperator(final SearchQuery searchQuery)
	{
		return searchQuery.getDefaultOperator() != null ? searchQuery.getDefaultOperator() : this.getDefaultOperator();
	}


	@Nullable
	protected String getGroupFacet(final SearchQuery searchQuery)
	{

		if (searchQuery.isGroupFacets())
		{
			String groupFieldName = searchQuery.getIndexedType().getGroupFieldName();
			String translatedField = this.getFieldNameTranslator().translate(searchQuery, groupFieldName,
					FieldNameProvider.FieldType.INDEX);
			return String.format(GROUP_FACET_PATTERN, translatedField);
		}

		return null;
	}
}
