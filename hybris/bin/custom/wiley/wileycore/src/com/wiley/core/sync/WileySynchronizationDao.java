package com.wiley.core.sync;

import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;

import java.util.List;


public interface WileySynchronizationDao
{
	List<CatalogVersionSyncJobModel> findCatalogVersionSyncJobs(String code);

	<T> List<T> findSynchronizationTargetsBySource(T item);

}
