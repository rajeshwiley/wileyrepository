package com.wiley.core.wiley.search.sollrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.DocumentData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseResultsPopulator;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileySearchResponseResultsPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE,
		INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE, ITEM>
		extends
		SearchResponseResultsPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE,
				INDEXED_TYPE_SORT_TYPE, ITEM>
{
	/**
	 * OOTB_CODE Based on SearchResponseResultsPopulator#convertGroupResultDocuments.
	 * Customization: we set first document as variant too. In don't due in OOTB grouping model looks like:
	 * Variant product and other variants that displayed. In our system we have base product with all variants
	 */
	@Override
	protected ITEM convertGroupResultDocuments(final SearchQuery searchQuery, final SearchResultGroup group)
	{
		final DocumentData<SearchQuery, Document> documentData = createDocumentData();
		documentData.setSearchQuery(searchQuery);
		int documentIndex = 0;
		final List<Document> variants = new ArrayList<>();
		for (final Document document : group.getDocuments())
		{
			if (documentIndex == 0)
			{
				documentData.setDocument(document);
			}
			variants.add(document);
			documentIndex++;
		}
		documentData.setVariants(variants);
		return getSearchResultConverter().convert(documentData);
	}
}