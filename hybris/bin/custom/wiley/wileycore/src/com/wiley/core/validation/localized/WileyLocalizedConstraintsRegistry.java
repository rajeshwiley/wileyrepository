package com.wiley.core.validation.localized;

import de.hybris.platform.validation.localized.ConstraintModelValidator;
import de.hybris.platform.validation.localized.LocalizedAttributeConstraint;
import de.hybris.platform.validation.localized.LocalizedConstraintsRegistry;
import de.hybris.platform.validation.localized.TypeLocalizedConstraints;
import de.hybris.platform.validation.model.constraints.AbstractConstraintModel;
import de.hybris.platform.validation.model.constraints.AttributeConstraintModel;
import de.hybris.platform.validation.services.ConstraintService;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;


/**
 * OOTB copy from {@link LocalizedConstraintsRegistry#loadLocalizedConstraintsGroupedByType()}.
 * Changed only usage of {@link WileyLocalizedAttributeConstraint} instead of {@link LocalizedAttributeConstraint}.
 */
public class WileyLocalizedConstraintsRegistry extends LocalizedConstraintsRegistry
{
	private ConstraintService constraintService;
	private ConstraintModelValidator constraintValidator;

	protected Map<String, TypeLocalizedConstraints> loadLocalizedConstraintsGroupedByType()
	{
		final HashMap<String, TypeLocalizedConstraints> typeLocalizedConstraints = new HashMap<>();
		for (AbstractConstraintModel constraint : constraintService.getAllConstraints())
		{
			if (this.constraintValidator.checkAndDisableInvalidConstraints(constraint) && this.isLocalizedConstraint(constraint))
			{
				String className = constraint.getTarget().getName();
				if (!typeLocalizedConstraints.containsKey(className))
				{
					typeLocalizedConstraints.put(className, new TypeLocalizedConstraints(className));
				}

				LocalizedAttributeConstraint localizedConstraint =
						new WileyLocalizedAttributeConstraint((AttributeConstraintModel) constraint);
				typeLocalizedConstraints.get(className).addConstraint(localizedConstraint);
			}
		}

		return typeLocalizedConstraints;
	}

	@Required
	public void setConstraintService(final ConstraintService constraintService)
	{
		super.setConstraintService(constraintService);
		this.constraintService = constraintService;
	}

	@Required
	public void setConstraintValidator(final ConstraintModelValidator constraintValidator)
	{
		super.setConstraintValidator(constraintValidator);
		this.constraintValidator = constraintValidator;
	}
}
