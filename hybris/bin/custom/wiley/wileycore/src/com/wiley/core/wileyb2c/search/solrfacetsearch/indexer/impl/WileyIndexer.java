package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.impl.DefaultIndexer;
import de.hybris.platform.solrfacetsearch.solr.Index;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexer extends DefaultIndexer
{
	private WileyProductRestrictionService wileyb2cProductRestrictionServiceForDelete;

	@Override
	public void removeItemsByPk(final Collection<PK> pks, final FacetSearchConfig facetSearchConfig,
			final IndexedType indexedType, final Index index)
			throws IndexerException, InterruptedException
	{
		final List<PK> filteredPks = filterItems(pks);
		super.removeItemsByPk(filteredPks, facetSearchConfig, indexedType, index);
	}

	private List<PK> filterItems(final Collection<PK> items)
	{
		return items.stream().filter(pk -> {
			ProductModel productModel = getModelService().get(pk);
			return !wileyb2cProductRestrictionServiceForDelete.isSearchable(productModel);
		}).collect(Collectors.toList());
	}

	@Required
	public void setWileyb2cProductRestrictionServiceForDelete(
			final WileyProductRestrictionService wileyb2cProductRestrictionServiceForDelete)
	{
		this.wileyb2cProductRestrictionServiceForDelete = wileyb2cProductRestrictionServiceForDelete;
	}
}
