package com.wiley.core.product;

import com.google.common.collect.Lists;
import com.wiley.core.category.WileyCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.variants.interceptor.GenericVariantProductValidateInterceptor;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class WelGenericVariantProductValidateInterceptor extends GenericVariantProductValidateInterceptor
{
    private WileyCategoryService categoryService;
    
    /*
        The implementation has been copied from base class in order to fix ClassCastException:
            Wiley Partner categories are configured as instances of CategoryModel and may be assigned for variant products.
            Casting CategoryModel to VariantValueCategoryModel causes error in OOTB version.
            Note that Partner category can be distinguished from other CategoryModels
                by presence of WEL_PARTNER_CATEGORY supercategory
     */
    @Override
    protected void validateBaseProductSuperCategories(final GenericVariantProductModel genericVariant,
                                                      final Collection<CategoryModel> variantValueCategories)
            throws InterceptorException
    {
        ProductModel baseProduct = genericVariant.getBaseProduct();
        if (baseProduct == null)
        {
            throw new InterceptorException(this.getL10NService().getLocalizedString("error.genericvariantproduct.nobaseproduct",
                    new Object[]{genericVariant.getCode()}));
        }
        else
        {
            final ArrayList variantCategoriesOfVariantValueCategories = Lists.newArrayList();
            CollectionUtils.forAllDo(variantValueCategories, new Closure() {
                public void execute(final Object input) {
                    //this line is absent in OOTB code and was added intentionally
                    if (!isPartnersCategory(input))
                    {
                        VariantValueCategoryModel variantValueCat = (VariantValueCategoryModel) input;
                        variantCategoriesOfVariantValueCategories.addAll(variantValueCat.getSupercategories());
                    }
                }
            });
            Collection superCategories = baseProduct.getSupercategories();
            if (CollectionUtils.isNotEmpty(superCategories)) {
                final ArrayList baseVariantCategories = Lists.newArrayList();
                CollectionUtils.forAllDo(superCategories, new Closure()
                {
                    public void execute(final Object input)
                    {
                        if (input instanceof VariantCategoryModel)
                        {
                            baseVariantCategories.add((VariantCategoryModel) input);
                        }

                    }
                });
                if (baseVariantCategories.size() != variantCategoriesOfVariantValueCategories.size()) {
                    throw new InterceptorException(this.getL10NService().getLocalizedString(
                            "error.genericvariantproduct.nosameamountofvariantcategories",
                            new Object[]{genericVariant.getCode(),
                            Integer.valueOf(variantCategoriesOfVariantValueCategories.size()),
                            baseProduct.getCode(), Integer.valueOf(baseVariantCategories.size())}));
                }

                Iterator var8 = variantCategoriesOfVariantValueCategories.iterator();

                while (var8.hasNext())
                {
                    CategoryModel varCategory = (CategoryModel) var8.next();
                    if (!baseVariantCategories.contains(varCategory))
                    {
                        throw new InterceptorException(this.getL10NService().getLocalizedString(
                                "error.genericvariantproduct.variantcategorynotinbaseproduct",
                                new Object[]{varCategory.getCode(), genericVariant.getCode(), baseProduct.getCode()}));
                    }
                }
            }

        }
    }

    @Override
    protected void validateSupercategories(final Collection<CategoryModel> superCategories) throws InterceptorException
    {
        boolean wrongCategoryExists = CollectionUtils.exists(superCategories, new Predicate() {
            public boolean evaluate(final Object object) {
                boolean wrongCategoryExists = false;
                if (object instanceof VariantValueCategoryModel)
                {
                    VariantValueCategoryModel variantValueCategoryModel = (VariantValueCategoryModel) object;
                    List variantValueSupercategories = variantValueCategoryModel.getSupercategories();
                    if (CollectionUtils.isEmpty(variantValueSupercategories))
                    {
                        wrongCategoryExists = true;
                    }
                    else
                    {
                        CategoryModel categoryModel = (CategoryModel) variantValueSupercategories.iterator().next();
                        wrongCategoryExists = !(categoryModel instanceof VariantCategoryModel);
                    }
                } else
                //this line is absent in OOTB code and was added intentionally
                if (!isPartnersCategory(object))
                {
                    wrongCategoryExists = true;
                }

                return wrongCategoryExists;
            }
        });
        if (wrongCategoryExists) {
            throw new InterceptorException(this.localizeForKey("error.genericvariantproduct.wrongsupercategory"));
        }
    }

    protected String localizeForKey(final String key) {
        return this.getL10NService().getLocalizedString(key);
    }
    

    protected boolean isPartnersCategory(final Object category) {
        return category instanceof CategoryModel && getCategoryService().isPartnersCategory((CategoryModel) category);
    }

    public WileyCategoryService getCategoryService() {
        return categoryService;
    }

    public void setCategoryService(final WileyCategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
