package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.apache.commons.lang.StringUtils;


public class Wileyb2cLocalizedFacetDisplayNameProvider extends AbstractFacetValueDisplayNameProvider 
{

  private static String facetPropertyString = "facet.property.%s.%s";

  @Override
  public String getDisplayName(final SearchQuery paramSearchQuery,
      final IndexedProperty paramIndexedProperty, final String facetValue) 
  {
    String returnString = null;
    if (StringUtils.isBlank(facetValue)) 
    {
      returnString = "";
    } else {
      returnString = String.format(facetPropertyString, paramIndexedProperty.getName(), facetValue);
    }

    return returnString;
  }
}
