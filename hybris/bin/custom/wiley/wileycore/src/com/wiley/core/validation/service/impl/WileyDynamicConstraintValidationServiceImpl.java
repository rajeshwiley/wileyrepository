package com.wiley.core.validation.service.impl;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.model.BrightCoveMediaContainerModel;
import com.wiley.core.validation.service.WileyDynamicConstraintValidationService;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


public class WileyDynamicConstraintValidationServiceImpl implements WileyDynamicConstraintValidationService
{
	private List<String> galleryContainerRequiredFormats;
	private List<String> brightcoveContainerRequiredFormats;
	private List<String> backgroundImageRequiredFormats;
	private List<String> externalImagesRequiredFormats;

	private Pattern isbn13Pattern;
	private Pattern issnPattern;

	@Override
	public boolean validateFirstGalleryContainerIsNotVideo(final Collection<MediaContainerModel> gallery)
	{
		if (CollectionUtils.isNotEmpty(gallery))
		{
			return !(CollectionUtils.get(gallery, 0) instanceof BrightCoveMediaContainerModel);
		}
		return true;
	}

	@Override
	public boolean validateGalleryImagesContainerContainsRequiredFormats(final Collection<MediaContainerModel> gallery)
	{
		if (CollectionUtils.isNotEmpty(gallery))
		{
			for (final MediaContainerModel mediaContainerModel : gallery)
			{
				if (!(mediaContainerModel instanceof BrightCoveMediaContainerModel))
				{
					if (!hasImageFormats(mediaContainerModel, galleryContainerRequiredFormats))
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public boolean validateExternalImageContainerContainsRequiredFormats(final MediaContainerModel externalImage)
	{
		return externalImage == null || hasImageFormats(externalImage, externalImagesRequiredFormats);
	}

	@Override
	public boolean validateGalleryBrightcoveContainerContainsRequiredFormats(final Collection<MediaContainerModel> gallery)
	{
		if (CollectionUtils.isNotEmpty(gallery))
		{
			for (final MediaContainerModel mediaContainerModel : gallery)
			{
				if (mediaContainerModel instanceof BrightCoveMediaContainerModel)
				{
					if (!hasImageFormats(mediaContainerModel, brightcoveContainerRequiredFormats))
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public boolean validateBackgroundImageContainerContainsRequiredFormats(final MediaContainerModel backgroundImage)
	{
		return backgroundImage == null || hasImageFormats(backgroundImage, backgroundImageRequiredFormats);
	}

	@Override
	public boolean validateIsbn13HasRequiredFormat(final List<ProductFeatureModel> features)
	{
		return isValidAttributeValue(findFeatureForAttributeCode(features, Wileyb2cClassificationAttributes.ISBN13.getCode()),
				isbn13Pattern);
	}

	@Override
	public boolean validateOnlineIssnHasRequiredFormat(final List<ProductFeatureModel> features)
	{
		return isValidAttributeValue(findFeatureForAttributeCode(features,
				Wileyb2cClassificationAttributes.ONLINE_ISSN.getCode()), issnPattern);
	}

	@Override
	public boolean validatePrintIssnHasRequiredFormat(final List<ProductFeatureModel> features)
	{
		return isValidAttributeValue(findFeatureForAttributeCode(features,
				Wileyb2cClassificationAttributes.PRINT_ISSN.getCode()), issnPattern);
	}

	private ProductFeatureModel findFeatureForAttributeCode(final List<ProductFeatureModel> features, final String code)
	{
		ProductFeatureModel featureFound = null;
		if (CollectionUtils.isNotEmpty(features))
		{
			featureFound = features.stream()
					.filter(feature -> code.equals(feature.getClassificationAttributeAssignment()
							.getClassificationAttribute().getCode()))
					.findFirst()
					.orElse(null);
		}
		return featureFound;
	}

	private boolean isValidAttributeValue(final ProductFeatureModel feature, final Pattern patternToMatch)
	{

		return feature == null || patternToMatch.matcher((String) feature.getValue()).matches();
	}

	private boolean hasImageFormats(final MediaContainerModel container, final List<String> formats)
	{
		return formats.stream()
				.filter(format -> hasImageWithQualifier(container, format))
				.count() == formats.size();
	}

	private boolean hasImageWithQualifier(final MediaContainerModel container, final String qualifier)
	{
		return container.getMedias()
				.stream()
				.filter(media -> media.getMediaFormat() != null && qualifier.equals(media.getMediaFormat().getQualifier()))
				.findFirst().isPresent();
	}

	private List<String> propertyToList(final String propertyValue)
	{
		final String[] values = propertyValue.split(",");
		return Arrays.asList(values).stream().map(String::trim).collect(Collectors.toList());
	}

	@Value("${validation.gallery.brightcove.required.formats}")
	public void setBrightcoveContainerRequiredFormats(final String brightcoveContainerRequiredFormats)
	{
		this.brightcoveContainerRequiredFormats = propertyToList(brightcoveContainerRequiredFormats);
	}

	@Value("${validation.gallery.image.required.formats}")
	public void setGalleryContainerRequiredFormats(final String galleryContainerRequiredFormats)
	{
		this.galleryContainerRequiredFormats = propertyToList(galleryContainerRequiredFormats);
	}

	@Value("${validation.background.image.required.formats}")
	public void setBackgroundImageRequiredFormats(final String backgroundImageRequiredFormats)
	{
		this.backgroundImageRequiredFormats = propertyToList(backgroundImageRequiredFormats);
	}

	@Value("${validation.external.image.required.formats}")
	public void setExternalImagesRequiredFormats(final String externalImagesRequiredFormats)
	{
		this.externalImagesRequiredFormats = propertyToList(externalImagesRequiredFormats);
	}

	@Value("${validation.isbn13.pattern}")
	public void setIsbn13Pattern(final String pattern)
	{
		this.isbn13Pattern = Pattern.compile(pattern);
	}

	@Value("${validation.issn.pattern}")
	public void setIssnPattern(final String pattern)
	{
		this.issnPattern = Pattern.compile(pattern);
	}
}
