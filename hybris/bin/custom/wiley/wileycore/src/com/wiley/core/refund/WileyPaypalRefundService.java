package com.wiley.core.refund;

import de.hybris.platform.payment.commands.result.RefundResult;

import com.wiley.core.payment.request.WileyFollowOnRefundRequest;


public interface WileyPaypalRefundService
{
	RefundResult performPayPalRefund(WileyFollowOnRefundRequest request);
}
