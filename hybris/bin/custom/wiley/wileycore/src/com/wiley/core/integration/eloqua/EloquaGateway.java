package com.wiley.core.integration.eloqua;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.core.healthcheck.MonitoredGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;


/**
 * The main service for communicating with Eloqua.
 *
 * @author Aliaksei_Zlobich
 */
public interface EloquaGateway extends MonitoredGateway
{

	/**
	 * Search for eloqua contact optional.
	 *
	 * @param customer
	 * 		the customer
	 * @param site
	 * 		site from order, because there is no site in session for business process
	 * @return the object optional which contains ContactDto if Contact was found.
	 */
	@Nonnull
	Optional<ContactDto> searchForEloquaContact(@Nonnull CustomerModel customer, @Nonnull BaseSiteModel site);

	/**
	 * Creates new Contact in Eloqua.
	 *
	 * @param customer
	 * 		customer
	 * @param address
	 * 		address
	 * @param subscribeToUpdates
	 * 		allows customer to subscribe to Eloqua updates.
	 * @param site
	 * 		site from order, because there is no site in session for business process
	 * @return created Contact with populated fields.
	 */
	@Nonnull
	ContactDto createEloquaContact(@Nonnull CustomerModel customer, @Nonnull AddressModel address,
			@Nullable Boolean subscribeToUpdates, @Nonnull BaseSiteModel site);

	/**
	 * Updates Contact in Eloqua.
	 *
	 * @param customer
	 * 		customer
	 * @param address
	 * 		address
	 * @param subscribeToUpdates
	 * 		allows customer to subscribe to Eloqua updates.
	 * @param site
	 * 		site from order, because there is no site in session for business process
	 * @return updated Contact with populated fields.
	 */
	@Nonnull
	ContactDto updateEloquaContact(@Nonnull CustomerModel customer, @Nonnull AddressModel address,
			@Nullable Boolean subscribeToUpdates, @Nonnull BaseSiteModel site);

	/**
	 * Updates CustomObjectDate with purchasing data in Eloqua.
	 *
	 * @param orderEntry
	 * 		order entry
	 * @return created CustomObjectDate with filled fields.
	 */
	@Nonnull
	CustomObjectDataDto createEloquaTransactionalRecord(@Nonnull OrderEntryModel orderEntry);

}
