package com.wiley.core.mpgs.command;


import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;



public interface WileyTokenizationCommand extends WileyCommand<WileyTokenizationRequest, WileyTokenizationResponse>
{
}
