package com.wiley.core.integration.handlers;

import javax.annotation.Nonnull;

import org.springframework.messaging.support.ErrorMessage;

import com.codahale.metrics.health.HealthCheck.Result;


/**
 * Used to handle errors occurred during health check.
 */
public class HealthCheckErrorHandler
{
	public Result onError(@Nonnull final ErrorMessage errorMessage)
	{
		return Result.unhealthy("There was an error during health check");
	}
}