package com.wiley.core.integration.failedordersexport;

import com.wiley.core.model.WileyFailedOrdersExportCronJobModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

public class WileyFailedOrdersExportJob extends AbstractJobPerformable<WileyFailedOrdersExportCronJobModel>
{

    @Autowired
    private WileyFailedOrderProcessesExportGateway wileyFailedOrderProcessesExportGateway;

    @Override
    public PerformResult perform(final WileyFailedOrdersExportCronJobModel wileyFailedOrdersExportCronJobModel)
    {
        LocalDateTime now = LocalDateTime.now();

        wileyFailedOrderProcessesExportGateway.exportFailedOrderProcesses(
                wileyFailedOrdersExportCronJobModel.getBaseStore(), now.minusDays(1), now);
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}
