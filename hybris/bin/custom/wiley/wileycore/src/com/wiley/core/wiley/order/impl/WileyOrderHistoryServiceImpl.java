package com.wiley.core.wiley.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.impl.DefaultOrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.primitives.Longs;
import com.wiley.core.wiley.order.WileyOrderHistoryService;


public class WileyOrderHistoryServiceImpl extends DefaultOrderHistoryService implements WileyOrderHistoryService
{
	@Override
	public OrderModel createHistorySnapshot(final OrderModel order)
	{
		OrderModel snapshot = super.createHistorySnapshot(order);

		final Iterator<AbstractOrderEntryModel> currentEntriesIterator = order.getEntries().iterator();
		final Iterator<AbstractOrderEntryModel> snapshotEntriesIterator = snapshot.getEntries().iterator();

		while (snapshotEntriesIterator.hasNext() && currentEntriesIterator.hasNext())
		{
			final AbstractOrderEntryModel snapshotEntry = snapshotEntriesIterator.next();
			final AbstractOrderEntryModel currentEntry = currentEntriesIterator.next();
			
			// reset snapshotEntry business keys, because they are unique
			snapshotEntry.setBusinessKey(null);
			if (currentEntry instanceof OrderEntryModel)
			{
				snapshotEntry.setOriginalOrderEntry((OrderEntryModel) currentEntry);
			}
		}

		return snapshot;
	}

	@Override
	public OrderModel getPreviousOrderVersion(final OrderModel order)
	{
		OrderHistoryEntryModel latestHistoryEntry = null;
		List<OrderHistoryEntryModel> historyEntries = order.getHistoryEntries();

		if (CollectionUtils.isNotEmpty(historyEntries))
		{
			latestHistoryEntry = historyEntries.stream()
					.sorted((e1, e2) -> Longs.compare(e2.getTimestamp().getTime(), e1.getTimestamp().getTime()))
					.findFirst().orElse(null);
		}
		return latestHistoryEntry != null ? latestHistoryEntry.getPreviousOrderVersion() : null;
	}
}
