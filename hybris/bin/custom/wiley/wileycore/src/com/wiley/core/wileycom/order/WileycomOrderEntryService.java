package com.wiley.core.wileycom.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Contains util methods for working with order entries.
 *
 * Author Herman_Chukhrai (EPAM)
 */
public interface WileycomOrderEntryService
{
	/**
	 * Searches order entry by product code in order directly.
	 *
	 * @param order
	 * 		order
	 * @param isbn
	 * 		isbn of order entry's product
	 * @return optional object.
	 */
	Optional<AbstractOrderEntryModel> findOrderEntryByIsbn(@Nonnull AbstractOrderModel order, @Nonnull String isbn);
}
