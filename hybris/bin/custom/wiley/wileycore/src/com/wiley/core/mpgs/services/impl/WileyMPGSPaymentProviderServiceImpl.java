package com.wiley.core.mpgs.services.impl;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;


public class WileyMPGSPaymentProviderServiceImpl implements WileyMPGSPaymentProviderService
{
	private String paymentProviderVersion;

	@Override
	public String generatePaymentProvider()
	{
		return WileyMPGSConstants.MPGS_PAYMENT_PROVIDER + paymentProviderVersion;
	}

	@Override
	public boolean isMPGSProviderGroup(final String provider)
	{
		return StringUtils.isNotEmpty(provider) && provider.startsWith(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
	}

	public void setPaymentProviderVersion(final String paymentProviderVersion)
	{
		this.paymentProviderVersion = paymentProviderVersion;
	}
}
