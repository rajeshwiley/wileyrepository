package com.wiley.core.wileycom.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;

import java.util.Optional;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomRestrictVisibilityByCountryStrategy implements WileyRestrictProductStrategy
{
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Override
	public boolean isRestricted(final ProductModel product)
	{
		final Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent()) {
			return CollectionUtils.isNotEmpty(product.getCountryRestrictions()) && product.getCountryRestrictions()
					.contains(currentCountry.get());
		} else {
			return false;
		}
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE_FOR_COUNTRY,
				String.format("Product [%s] is not available due to country check.", parameter.getProduct().getCode()), null);
	}

}
