package com.wiley.core.integration.eloqua.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * Please see http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_contact_e7898519cda9de10f74778481e420.htm
 *
 * @author Aliaksei_Zlobich
 */
@XmlRootElement(name = "Contact")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactDto
{
	private String type = "Contact";

	private Integer accountId;

	private String accountName;

	private String address1;

	private String address2;

	private String address3;

	private Integer bouncebackDate;

	private String businessPhone;

	private String city;

	private String country;

	private String emailAddress;

	// available values http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_formattype_27ba2b624de2a17b82ed2ddf403d58a1.htm
	private String emailFormatPreference; // enum type in Eloqua

	private String fax;

	@XmlElement
	private List<FieldValueDto> fieldValues;

	private String firstName;

	private Boolean isBounceback;

	private Boolean isSubscribed;

	private Boolean isTracked;

	private String lastName;

	private String mobilePhone;

	private String postalCode;

	private String province;

	private String salesPerson;

	private Integer subscriptionDate;

	private String title;

	private Integer unsubscriptionDate;

	private Integer accessedAt;

	private Integer createdAt;

	private Integer createdBy;

	private String currentStatus;

	// available values http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_requestdepth_f48692a58e42b9b8751a531c7955becb.htm
	private String depth; // enum type in Eloqua

	private String description;

	private Integer folderId;

	private Integer id;

	private String name;

	// available values http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_instancepermissions_3b3d9c891a06e6a6bd13d46b191964.htm
	private String permissions; // enum type in Eloqua

	private Integer scheduledFor;

	private String sourceTemplateId;

	private Integer updatedAt;

	private Integer updatedBy;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public Integer getAccountId()
	{
		return accountId;
	}

	public void setAccountId(final Integer accountId)
	{
		this.accountId = accountId;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(final String accountName)
	{
		this.accountName = accountName;
	}

	public String getAddress1()
	{
		return address1;
	}

	public void setAddress1(final String address1)
	{
		this.address1 = address1;
	}

	public String getAddress2()
	{
		return address2;
	}

	public void setAddress2(final String address2)
	{
		this.address2 = address2;
	}

	public String getAddress3()
	{
		return address3;
	}

	public void setAddress3(final String address3)
	{
		this.address3 = address3;
	}

	public Integer getBouncebackDate()
	{
		return bouncebackDate;
	}

	public void setBouncebackDate(final Integer bouncebackDate)
	{
		this.bouncebackDate = bouncebackDate;
	}

	public String getBusinessPhone()
	{
		return businessPhone;
	}

	public void setBusinessPhone(final String businessPhone)
	{
		this.businessPhone = businessPhone;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getEmailFormatPreference()
	{
		return emailFormatPreference;
	}

	public void setEmailFormatPreference(final String emailFormatPreference)
	{
		this.emailFormatPreference = emailFormatPreference;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(final String fax)
	{
		this.fax = fax;
	}

	public List<FieldValueDto> getFieldValues()
	{
		return fieldValues;
	}

	public void setFieldValues(final List<FieldValueDto> fieldValues)
	{
		this.fieldValues = fieldValues;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public Boolean getIsBounceback()
	{
		return isBounceback;
	}

	public void setIsBounceback(final Boolean isBounceback)
	{
		this.isBounceback = isBounceback;
	}

	public Boolean getIsSubscribed()
	{
		return isSubscribed;
	}

	public void setIsSubscribed(final Boolean isSubscribed)
	{
		this.isSubscribed = isSubscribed;
	}

	public Boolean getIsTracked()
	{
		return isTracked;
	}

	public void setIsTracked(final Boolean isTracked)
	{
		this.isTracked = isTracked;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getMobilePhone()
	{
		return mobilePhone;
	}

	public void setMobilePhone(final String mobilePhone)
	{
		this.mobilePhone = mobilePhone;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getProvince()
	{
		return province;
	}

	public void setProvince(final String province)
	{
		this.province = province;
	}

	public String getSalesPerson()
	{
		return salesPerson;
	}

	public void setSalesPerson(final String salesPerson)
	{
		this.salesPerson = salesPerson;
	}

	public Integer getSubscriptionDate()
	{
		return subscriptionDate;
	}

	public void setSubscriptionDate(final Integer subscriptionDate)
	{
		this.subscriptionDate = subscriptionDate;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(final String title)
	{
		this.title = title;
	}

	public Integer getUnsubscriptionDate()
	{
		return unsubscriptionDate;
	}

	public void setUnsubscriptionDate(final Integer unsubscriptionDate)
	{
		this.unsubscriptionDate = unsubscriptionDate;
	}

	public Integer getAccessedAt()
	{
		return accessedAt;
	}

	public void setAccessedAt(final Integer accessedAt)
	{
		this.accessedAt = accessedAt;
	}

	public Integer getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(final Integer createdAt)
	{
		this.createdAt = createdAt;
	}

	public Integer getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(final Integer createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getCurrentStatus()
	{
		return currentStatus;
	}

	public void setCurrentStatus(final String currentStatus)
	{
		this.currentStatus = currentStatus;
	}

	public String getDepth()
	{
		return depth;
	}

	public void setDepth(final String depth)
	{
		this.depth = depth;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public Integer getFolderId()
	{
		return folderId;
	}

	public void setFolderId(final Integer folderId)
	{
		this.folderId = folderId;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getPermissions()
	{
		return permissions;
	}

	public void setPermissions(final String permissions)
	{
		this.permissions = permissions;
	}

	public Integer getScheduledFor()
	{
		return scheduledFor;
	}

	public void setScheduledFor(final Integer scheduledFor)
	{
		this.scheduledFor = scheduledFor;
	}

	public String getSourceTemplateId()
	{
		return sourceTemplateId;
	}

	public void setSourceTemplateId(final String sourceTemplateId)
	{
		this.sourceTemplateId = sourceTemplateId;
	}

	public Integer getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(final Integer updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public Integer getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(final Integer updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString()
	{
		// Only fields described in IDD https://confluence.wiley.ru/display/ECSC/IDD%3A+Eloqua+integration
		// is converted to String.
		return MoreObjects.toStringHelper(this)
				.add("emailAddress", emailAddress)
				.add("firstName", firstName)
				.add("lastName", lastName)
				.add("country", country)
				.add("businessPhone", businessPhone)
				.add("address1", address1)
				.add("address2", address2)
				.add("city", city)
				.add("province", province)
				.add("postalCode", postalCode)
				.add("fieldValues", fieldValues)
				.toString();
	}
}
