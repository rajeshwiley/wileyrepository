package com.wiley.core.countrystore.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wiley.core.countrystore.WileyCountryStoreConfigurationDao;
import com.wiley.core.model.WileyCountryStoreConfigurationModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyCountryStoreConfigurationDaoImpl implements WileyCountryStoreConfigurationDao
{
	private static final String FIND_BY_COUNTRY_BASESTORE_ID =
			"SELECT {" + WileyCountryStoreConfigurationModel.PK + "} FROM {" + WileyCountryStoreConfigurationModel._TYPECODE
					+ "} " + "WHERE {" + WileyCountryStoreConfigurationModel.COUNTRY + "} = ?countryId"
					+ " AND {" + WileyCountryStoreConfigurationModel.BASESTORE + "} = ?baseStoreId";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<WileyCountryStoreConfigurationModel> findConfigurationByCountryAndBasestore(final CountryModel countryModel,
			final BaseStoreModel baseStoreModel)
	{
		validateParameterNotNull(countryModel, "CountryModel not be null!");
		validateParameterNotNull(baseStoreModel, "BaseStoreModel must not be null!");

		final List<CurrencyModel> currencyModels;

		final Map<String, Object> params = new HashMap<>();
		params.put("countryId", countryModel.getPk());
		params.put("baseStoreId", baseStoreModel.getPk());
		final SearchResult<WileyCountryStoreConfigurationModel> searchResult = flexibleSearchService.search(
				FIND_BY_COUNTRY_BASESTORE_ID, params);

		return searchResult.getResult();
	}
}
