package com.wiley.core.order.service;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;

import java.util.List;

import javax.annotation.Nonnull;


public interface WileyOrderEntryService
{
	/**
	 * Get order entry details
	 *
	 * @param guid
	 * 		the guid of order entry
	 * @return
	 */
	OrderEntryModel getOrderEntryForGUID(String guid);

	SearchPageData<AbstractOrderEntryModel> getOrderEntriesForBusinessValues(BaseSiteModel baseSite,
			String userId, String businessItemId, String businessKey, List<String> statuses,
			PageableData pagination);

	boolean isOrderEntryExistsForGuid(@Nonnull String guid);
}
