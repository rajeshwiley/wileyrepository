package com.wiley.core.wileycom.valuetranslator;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;

import com.wiley.core.wileycom.valuetranslator.adapter.WileycomImportAdapter;


/**
 * Implementation of {@link AbstractWileycomSpecialValueTranslator} that using {@link WileycomImportAdapter}
 * for delegating logic
 *
 * @author Dzmitryi_Halahayeu
 */
public abstract class AbstractDefaultWileycomSpecialValueTranslator
		extends AbstractWileycomSpecialValueTranslator<WileycomImportAdapter>
{
	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		getWileycomImportAdapter().performImport(cellValue, processedItem);
	}


}
