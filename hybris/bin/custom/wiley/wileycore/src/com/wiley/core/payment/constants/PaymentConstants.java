package com.wiley.core.payment.constants;

public final class PaymentConstants
{
	public static final class WPG
	{
		public static final String OPERATION_KEY = "operation";
		public static final String TRANSACTION_KEY = "transID";
		public static final String OPERATION_AUTH = "auth";
		public static final String OPERATION_VALIDATE = "validate";
	}
}
