package com.wiley.core.order.strategies.calculation.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.util.PriceValue;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.jalo.WileyEurope1PriceFactory;
import com.wiley.core.order.strategies.calculation.WileyFindStackablePriceStrategy;


public class WileyFindStackablePriceStrategyImpl implements WileyFindStackablePriceStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyFindStackablePriceStrategyImpl.class);

	private static final int MINIMUM_QTY = 1;

	@Resource
	private WileyEurope1PriceFactory wileyPriceFactory;

	/**
	 * Calculates Total price based from priceRows of product based on list iterator concept
	 * If there is any PriceRow which does not have minqtd as 1 it throws exception else it calculates
	 * total stackable price.
	 */
	@Override
	public PriceValue findStackablePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		try
		{
			final List<PriceRow> priceRows = removeDuplicatePriceRow(wileyPriceFactory.getPriceRowsForOrderEntry(entry));
			return calculateStackablePriceSubtotal(entry, priceRows);
		}
		catch (final JaloPriceFactoryException e)
		{
			throw new CalculationException(e);
		}
	}

	/**
	 * Sum subtotal price from priceRows
	 *
	 * @param entry
	 * 		order entry
	 * @param priceRows
	 * 		sorted list of price rows, that has no duplications by minqty
	 * @throws CalculationException
	 * 		if the first price row has minqty higher then 1 or if {@code priceRows} list is empty
	 * 		order entry
	 */
	protected PriceValue calculateStackablePriceSubtotal(final AbstractOrderEntryModel entry,
			final List<PriceRow> priceRows) throws CalculationException
	{

		if (CollectionUtils.isNotEmpty(priceRows))
		{
			double total = 0.0;
			final Currency currency = priceRows.get(0).getCurrency();
			final boolean isNet = priceRows.get(0).isNetAsPrimitive();

			checkThatPriceRowForQtyOneExists(entry, priceRows);

			Collections.reverse(priceRows);
			final ListIterator<PriceRow> priceRowIterator = priceRows.listIterator();
			PriceRow priceRow = priceRowIterator.next();

			for (long i = entry.getQuantity(); i > 0; i--)
			{
				while (priceRowIterator.hasNext() && i < priceRow.getMinqtd())
				{
					priceRow = priceRowIterator.next();
				}
				total += priceRow.getPrice();
			}
			return new PriceValue(currency.getIsocode(), total, isNet);
		}

		final CurrencyModel currency = entry.getOrder().getCurrency();
		final String message =
				String.format("Could not find price for cart entry [%s] with cart currency [%s].",
						entry.getGuid(), currency.getIsocode());
		LOG.warn(message);
		throw new CalculationException(message);

	}

	/**
	 * Basically perform the check that we have Stackable PriceRow for q-ty "1".
	 * @throws CalculationException
	 */
	private void checkThatPriceRowForQtyOneExists(final AbstractOrderEntryModel entry, final List<PriceRow> priceRows)
			throws CalculationException
	{
		PriceRow firstPriceRow = priceRows.get(0);
		long firstMinQtyd = firstPriceRow.getMinqtd();
		if (!(firstMinQtyd == MINIMUM_QTY))
		{
			final String message = String.format("Error occurred while calculating stackable price for product.code=[%s], "
							+ "AbstractOrder.code=[%s], AbstractOrder.currency=[%s]."
							+ "First PriceRow should have minimal MinQty=[%s] but has [%s]", entry.getProduct().getCode(),
					entry.getOrder().getCode(), entry.getOrder().getCurrency().getIsocode(), MINIMUM_QTY, firstMinQtyd);
			throw new CalculationException(message);
		}
	}

	private List<PriceRow> removeDuplicatePriceRow(final List<PriceRow> priceRows)
	{
		long previousMinQty = 0;
		ListIterator<PriceRow> priceRowIterator = priceRows.listIterator();
		while (priceRowIterator.hasNext())
		{
			PriceRow priceRow = priceRowIterator.next();
			if (priceRow.getMinqtd() == previousMinQty)
			{
				priceRowIterator.remove();
			}
			previousMinQty = priceRow.getMinqtd();
		}
		return priceRows;
	}



}
