package com.wiley.core.paymentinfo.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.paymentinfo.CardPaymentInfoCcOwnerGenerator;


/**
 * Default implementation of {@link CardPaymentInfoCcOwnerGenerator}.
 */
public class WileyCardPaymentInfoCcOwnerGeneratorImpl implements CardPaymentInfoCcOwnerGenerator
{
	@Override
	@Nonnull
	public Optional<String> getCCOwnerFromBillingAddress(@Nonnull final AddressModel billingAddress)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("billingAddress", billingAddress);

		// the implementation is a copy-paste from
		// de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreditCardPaymentInfoCreateStrategy
		// .getCCOwner()
		return Optional.of(billingAddress.getFirstname() + " " + billingAddress.getLastname());
	}
}
