package com.wiley.core.payment.strategies.impl;


import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.store.WileyBaseStoreService;


public class WPGVendorIdStrategyImpl implements WPGVendorIdStrategy
{
	private String vendorIdKeyTemplate;

	@Autowired
	private ConfigurationService configurationService;

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	@Override
	public Integer getVendorId(final String siteId)
	{
		Integer vendorId = configurationService.getConfiguration().getInteger(
				String.format(getVendorIdKeyTemplate(), wileyBaseStoreService.getBaseStoreUidForSite(siteId)), null);
		Preconditions.checkArgument(vendorId != null, "WPG Vendor Id is not configured for site '%s'", siteId);
		return vendorId;
	}

	public String getVendorIdKeyTemplate()
	{
		return vendorIdKeyTemplate;
	}

	public void setVendorIdKeyTemplate(final String vendorIdKeyTemplate)
	{
		this.vendorIdKeyTemplate = vendorIdKeyTemplate;
	}
}
