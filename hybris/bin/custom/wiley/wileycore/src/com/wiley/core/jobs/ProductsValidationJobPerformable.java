package com.wiley.core.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.wiley.core.model.ProductsValidationCronJobModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.validation.ProductsBatchValidator;
import com.wiley.core.product.validation.ProductsValidationService;

import static java.util.stream.Collectors.toList;


/**
 * Runs a core logic of "products validation on demand" functionality.
 * First of all, retrieves products which have to be validated. The further processing happens in batch mode.
 * The size of batch is configured via {@code product.validation.batchSize} property.
 * <p>For every batch, gets products, and then validates them one by one. All the validation errors are collected and persisted
 * at the very last step. The products which do not have associated validation errors are
 * {@link ArticleApprovalStatus#APPROVED approved}, otherwise marked as {@link ArticleApprovalStatus#UNAPPROVED invalid}
 */
public class ProductsValidationJobPerformable extends AbstractJobPerformable<ProductsValidationCronJobModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductsValidationJobPerformable.class);

	@Resource
	private ProductsBatchValidator productsBatchValidator;

	@Resource
	private ProductsValidationService productsValidationService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ValidationService validationService;

	@Resource
	private WileyProductService productService;

	@Override
	public PerformResult perform(final ProductsValidationCronJobModel cronJob)
	{
		final Stopwatch timer = Stopwatch.createStarted();
		LOG.info("Starting job for cron job [{}].", cronJob.getCode());

		final CatalogVersionModel catalogVersion = cronJob.getProductCatalogVersion();
		catalogVersionService.setSessionCatalogVersions(Collections.singletonList(catalogVersion));

		final List<ProductModel> products = productService.getProductsToValidate(cronJob.getProductStatus(),
				cronJob.getProductCatalogVersion());
		LOG.info("Job is validating [{}] product status and [{}/{}] catalog version.", cronJob.getProductStatus().getCode(),
				cronJob.getProductCatalogVersion().getCatalog().getId(), cronJob.getProductCatalogVersion().getVersion());

		final List<List<ProductModel>> idsBatchesToValidate = Lists.partition(products, getBatchSize());

		LOG.info("There are [{}] product pending for validation in [{}] batches", products.size(),
				idsBatchesToValidate.size());


		final List<ConstraintGroupModel> constraintGroups = cronJob.getConstraintGroups();
		LOG.info("Constraint groups [{}] will be used for checking of product validity",
				constraintGroups.stream().map(ConstraintGroupModel::getId).collect(toList()));

		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final UserModel currentUser = userService.getCurrentUser();

		final CronJobResult jobResult = idsBatchesToValidate.stream().parallel()
				.map(productBatch ->
				{
					// required if running batch processing in parallel mode
					Registry.setCurrentTenant(currentTenant);
					userService.setCurrentUser(currentUser);
					catalogVersionService.setSessionCatalogVersions(Collections.singletonList(catalogVersion));

					CronJobResult result;
					try
					{
						result = productsBatchValidator.validateProducts(productBatch, cronJob, constraintGroups);
					}
					catch (Exception e)
					{
						LOG.warn(String.format("Processing of batch has failed. The following"
								+ " products were not validated die unexpected error: %s", productBatch), e);
						result = CronJobResult.FAILURE;
					}
					return result;
				})
				.reduce(CronJobResult.SUCCESS, new CronJobResultComputer());

		LOG.info("Completed job for cron job [{}] with status [{}] in [{}]", cronJob.getCode(), jobResult, timer.stop());
		return new PerformResult(jobResult, CronJobStatus.FINISHED);
	}

	private int getBatchSize()
	{
		return configurationService.getConfiguration().getInt("product.validation.batchSize", 1);
	}

	/**
	 * Treats result as successful if all batches were run without failures
	 */
	private static class CronJobResultComputer implements BinaryOperator<CronJobResult>
	{
		@Override
		public CronJobResult apply(final CronJobResult accumulativeCronJobResult, final CronJobResult batchCronJobResult)
		{
			return accumulativeCronJobResult.equals(CronJobResult.FAILURE) || batchCronJobResult.equals(CronJobResult.FAILURE) ?
					CronJobResult.FAILURE :
					CronJobResult.SUCCESS;
		}
	}
}