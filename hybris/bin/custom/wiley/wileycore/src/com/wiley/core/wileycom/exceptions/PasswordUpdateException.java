package com.wiley.core.wileycom.exceptions;

/**
 * Created by Sergiy_Mishkovets on 7/14/2016.
 */
public class PasswordUpdateException extends RuntimeException
{
	public PasswordUpdateException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
