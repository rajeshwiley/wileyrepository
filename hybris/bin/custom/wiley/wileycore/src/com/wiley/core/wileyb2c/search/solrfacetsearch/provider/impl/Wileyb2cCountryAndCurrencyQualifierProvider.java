package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import de.hybris.platform.solrfacetsearch.provider.QualifierProvider;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cCountryAndCurrencyQualifierProvider implements QualifierProvider
{
	public static final String COUNTRIES = "COUNTRIES_TO_UPDATE";
	static final Set<Class<?>> SUPPORTED_TYPES = initSupportedTypes();

	@Resource
	private WileycomI18NService wileycomI18NService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private SessionService sessionService;
	@Resource
	private BaseSiteService baseSiteService;

	private static Set<Class<?>> initSupportedTypes()
	{
		final Set<Class<?>> types = new HashSet<>();
		types.add(CurrencyModel.class);
		types.add(CountryModel.class);
		return types;
	}

	@Override
	public Set<Class<?>> getSupportedTypes()
	{
		return SUPPORTED_TYPES;
	}

	@Override
	public Collection<Qualifier> getAvailableQualifiers(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
	{
		Objects.requireNonNull(facetSearchConfig, "facetSearchConfig is null");
		Objects.requireNonNull(indexedType, "indexedType is null");
		final List<Wileyb2cCountryAndCurrencyQualifier> qualifiers = new ArrayList<>();

		final Collection<CountryModel> countriesToUpdate = sessionService.getAttribute(COUNTRIES);

		final Collection<CountryModel> allCountries = facetSearchConfig.getIndexConfig().getCountries();
		Assert.notNull(allCountries);
		Collection<CountryModel> countries;
		if (countriesToUpdate != null)
		{
			countries = CollectionUtils.intersection(allCountries, countriesToUpdate);
		}
		else
		{
			countries = allCountries;
		}

		for (final CountryModel country : countries)
		{
			for (final CurrencyModel currencyModel : facetSearchConfig.getIndexConfig().getCurrencies())
			{
				final CountryModel indexCountry = facetSearchConfig.getIndexConfig().getCountries().contains(country) ?
						country : country.getFallbackCountries().get(0);
				qualifiers.add(new Wileyb2cCountryAndCurrencyQualifier(indexCountry, currencyModel));
			}
		}

		return Collections.unmodifiableList(qualifiers);
	}

	@Override
	public boolean canApply(final IndexedProperty indexedProperty)
	{
		return indexedProperty.isLocalized() && indexedProperty.isCurrency();
	}

	public void applyQualifier(final Qualifier qualifier)
	{
		Objects.requireNonNull(qualifier, "qualifier is null");
		if (!(qualifier instanceof Wileyb2cCountryAndCurrencyQualifier))
		{
			throw new IllegalArgumentException("provided qualifier is not of expected type");
		}
		else
		{
			final Wileyb2cCountryAndCurrencyQualifier wileyb2cCountryAndCurrencyQualifier =
					(Wileyb2cCountryAndCurrencyQualifier) qualifier;
			wileycomI18NService.setCurrentCountry(wileyb2cCountryAndCurrencyQualifier.getCountry());
			commonI18NService.setCurrentCurrency(wileyb2cCountryAndCurrencyQualifier.getCurrency());
		}
	}

	public Qualifier getCurrentQualifier()
	{
		final CurrencyModel currency = commonI18NService.getCurrentCurrency();
		CountryModel country;
		// Has been implemented selector for default country for wileyws/v2/products/search
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			country = currentCountry.get();
		}
		else
		{
			country = wileycomI18NService.getDefaultCountry();
		}
		final Collection<CountryModel> indexCountries =
				baseSiteService.getCurrentBaseSite().getSolrFacetSearchConfiguration().getCountries();
		final CountryModel indexCountry = indexCountries.contains(country) ? country : country.getFallbackCountries().get(0);
		return currency == null ? null : new Wileyb2cCountryAndCurrencyQualifier(indexCountry, currency);
	}


	public static class Wileyb2cCountryAndCurrencyQualifier implements Qualifier
	{
		private final CountryModel country;
		private final CurrencyModel currency;
		private final String fieldQualifier;

		public Wileyb2cCountryAndCurrencyQualifier(final CountryModel country, final CurrencyModel currency)
		{
			Objects.requireNonNull(country, "country is null");
			Objects.requireNonNull(currency, "currency is null");
			this.country = country;
			this.currency = currency;
			this.fieldQualifier = country.getIsocode() + "_" + currency.getIsocode();
		}

		public CountryModel getCountry()
		{
			return country;
		}

		public CurrencyModel getCurrency()
		{
			return currency;
		}

		public <U> U getValueForType(final Class<U> type)
		{
			Objects.requireNonNull(type, "type is null");
			if (type.equals(CountryModel.class))
			{
				return (U) this.country;
			}
			else if (type.equals(CurrencyModel.class))
			{
				return (U) this.currency;
			}
			else
			{
				throw new IllegalArgumentException("type not supported");
			}
		}

		public String toFieldQualifier()
		{
			return fieldQualifier;
		}

		@Override
		public boolean equals(final Object o)
		{
			if (this == o)
			{
				return true;
			}
			if (o == null || getClass() != o.getClass())
			{
				return false;
			}

			final Wileyb2cCountryAndCurrencyQualifier that = (Wileyb2cCountryAndCurrencyQualifier) o;

			return fieldQualifier != null ? fieldQualifier.equals(that.fieldQualifier) : that.fieldQualifier == null;
		}

		@Override
		public int hashCode()
		{
			return fieldQualifier != null ? fieldQualifier.hashCode() : 0;
		}
	}
}