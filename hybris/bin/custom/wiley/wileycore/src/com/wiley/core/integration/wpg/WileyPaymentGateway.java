package com.wiley.core.integration.wpg;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Payload;

import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;
import com.wiley.core.payment.response.WileyCaptureResult;
import com.wiley.core.payment.response.WileyRefundResult;


public interface WileyPaymentGateway
{
	@Nonnull
	WileyRefundResult tokenRefund(@Nonnull @Payload WileyFollowOnRefundRequest request);

	@Nonnull
	WileyAuthorizeResult authorizePayment(@Nonnull @Payload WileySubscriptionAuthorizeRequest request);

	@Nonnull
	WileyCaptureResult capture(@Nonnull @Payload WileyCaptureRequest request);

}
