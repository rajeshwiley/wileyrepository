package com.wiley.core.event.facade.orderinfo;

import com.wiley.core.event.facade.WileyOneObjectFacadeEvent;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class WileyPopulateOrderInfoDataEvent extends WileyOneObjectFacadeEvent<OrderInfoData> {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public WileyPopulateOrderInfoDataEvent(final OrderInfoData source) {
        super(source);
    }
}
