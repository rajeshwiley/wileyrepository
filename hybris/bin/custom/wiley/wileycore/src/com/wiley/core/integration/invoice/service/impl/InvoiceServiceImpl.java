package com.wiley.core.integration.invoice.service.impl;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.invoice.InvoiceGateway;
import com.wiley.core.integration.invoice.dto.InvoiceDto;
import com.wiley.core.integration.invoice.service.InvoiceService;


public class InvoiceServiceImpl implements InvoiceService
{
	@Autowired
	private InvoiceGateway invoiceGateway;

	@Override
	public InvoiceDto getInvoice(@Nonnull final String referenceId)
	{
		return invoiceGateway.getInvoice(referenceId);
	}
}
