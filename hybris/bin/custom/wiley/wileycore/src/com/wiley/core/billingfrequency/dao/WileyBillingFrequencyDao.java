package com.wiley.core.billingfrequency.dao;

import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * Provides access for {@link BillingFrequencyModel}.
 */
public interface WileyBillingFrequencyDao
{

	/**
	 * Searches billing frequencies by code
	 *
	 * @param code
	 * @return list of {@link BillingFrequencyModel}.
	 */
	@Nonnull
	List<BillingFrequencyModel> findBillingFrequenciesByCode(@Nonnull String code);

}
