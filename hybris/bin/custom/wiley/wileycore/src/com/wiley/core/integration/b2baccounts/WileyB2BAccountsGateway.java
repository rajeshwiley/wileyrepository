package com.wiley.core.integration.b2baccounts;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * Created by Raman_Hancharou on 8/9/2016.
 */
public interface WileyB2BAccountsGateway
{

	String B2B_CUSTOMER = "b2b_customer";

	/**
	 * The gateway checks whether a product is owned by the organization and
	 * available for purchase or not.
	 *
	 * @param productModel
	 *          The product model for checking
	 * @param customer
	 *          B2BCustomer
	 */
	@Nonnull
	Boolean checkLicense(@Nonnull @Payload ProductModel productModel,
			@Nonnull @Header(B2B_CUSTOMER) B2BCustomerModel customer);
}
