package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.cron;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.enums.IndexerOperationValues;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.indexer.cron.SolrIndexerCronJobModel;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.WileyIndexerService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileySolrIndexerJob extends de.hybris.platform.solrfacetsearch.indexer.cron.SolrIndexerJob
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySolrIndexerJob.class);
	private static final int TIMEOUT_MIN = 5;

	@Resource
	private CronJobService cronJobService;

	@Resource
	private WileyIndexerService wileyIndexerService;

	@Override
	public PerformResult performIndexingJob(final CronJobModel cronJob)
	{
		if (cronJob instanceof SolrIndexerCronJobModel)
		{
			while (isIndexRunning((SolrIndexerCronJobModel) cronJob))
			{
				if (clearAbortRequestedIfNeeded(cronJob))
				{
					return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
				}
				LOG.warn("Execution of job " + cronJob.getCode() + " will be paused for " + TIMEOUT_MIN + " min");
				try
				{
					setCronJobStatus(cronJob, CronJobStatus.PAUSED);
					TimeUnit.MINUTES.sleep(TIMEOUT_MIN);
				}
				catch (InterruptedException e)
				{
					LOG.error(e.getMessage(), e);
					Thread.currentThread().interrupt();
					return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
				}
			}
			if (CronJobStatus.PAUSED.equals(cronJob.getStatus()))
			{
				setCronJobStatus(cronJob, CronJobStatus.RUNNINGRESTART);
			}
		}
		return super.performIndexingJob(cronJob);
	}

	private boolean isIndexRunning(final SolrIndexerCronJobModel currentJob)
	{
		final String currentSearchConfig = currentJob.getFacetSearchConfig().getName();
		final List<CronJobModel> cronJobs = cronJobService.getRunningOrRestartedCronJobs();
		for (final CronJobModel cronJobModel : cronJobs)
		{
			if (cronJobModel instanceof SolrIndexerCronJobModel)
			{
				final SolrIndexerCronJobModel solrIndexerCronJobModel = (SolrIndexerCronJobModel) cronJobModel;
				if (isNotSelf(cronJobModel, currentJob) && isJobForSameSearchConfig(solrIndexerCronJobModel,
						currentSearchConfig))
				{
					LOG.warn("Job " + solrIndexerCronJobModel.getCode() + " for facet search config "
							+ currentSearchConfig + " is already running. It's impossible to run several "
							+ "cron jobs for one config simultaneously");
					return true;
				}
			}
		}
		return false;
	}

	private boolean isJobForSameSearchConfig(final SolrIndexerCronJobModel solrIndexerCronJobModel,
			final String currentSearchConfig)
	{
		return solrIndexerCronJobModel
				.getFacetSearchConfig().getName().equals(currentSearchConfig);
	}

	private boolean isNotSelf(final CronJobModel cronJobModel, final SolrIndexerCronJobModel solrIndexerCronJobModel)
	{
		return !solrIndexerCronJobModel.getCode().equals(cronJobModel.getCode());
	}

	@Override
	protected void indexItems(final SolrIndexerCronJobModel solrIndexerCronJob, final FacetSearchConfig facetSearchConfig)
			throws IndexerException
	{
		if (solrIndexerCronJob.getIndexerOperation() == IndexerOperationValues.RESTORE)
		{
			LOG.info("Starting restore index process for SOLR index with config: [{}]",
					solrIndexerCronJob.getFacetSearchConfig().getName());
			final Map<String, String> indexerHints = solrIndexerCronJob.getIndexerHints();
			this.wileyIndexerService.restoreIndex(facetSearchConfig, indexerHints);
		}
		else
		{
			super.indexItems(solrIndexerCronJob, facetSearchConfig);
		}
	}

	/**
	 * Abort will work only if job waits via sleep method.
	 * It can't help during indexing process
	 *
	 * @return
	 */
	@Override
	public boolean isAbortable()
	{
		return true;
	}

	private void setCronJobStatus(final CronJobModel cronJob, final CronJobStatus status)
	{
		cronJob.setStatus(status);
		modelService.save(cronJob);
	}
}
