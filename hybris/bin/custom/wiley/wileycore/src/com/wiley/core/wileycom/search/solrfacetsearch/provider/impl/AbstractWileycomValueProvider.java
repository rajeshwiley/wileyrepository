package com.wiley.core.wileycom.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * @author Dzmitryi_Halahayeu
 */
public abstract class AbstractWileycomValueProvider<T> extends AbstractPropertyFieldValueProvider
		implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;
	private WileycomI18NService wileycomI18NService;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList<>();
		if (indexedProperty.isLocalized())
		{
			Collection<LanguageModel> languages = indexConfig.getLanguages();
			final Locale currentLocale = this.i18nService.getCurrentLocale();
			final Optional<CountryModel> currentCountry = this.wileycomI18NService.getCurrentCountry();
			for (LanguageModel language : languages)
			{

				for (final CountryModel country : indexConfig.getCountries())
				{
					try
					{
						final String localeName = language.getIsocode() + "_" + country.getIsocode();
						Locale locale = this.localeService.getLocaleByString(localeName);
						this.i18nService.setCurrentLocale(locale);
						this.wileycomI18NService.setCurrentCountry(country);
						List<T> values = collectValues(indexConfig, indexedProperty, model);
						fieldValues.addAll(createFieldValue(values, indexedProperty, localeName));
					}
					finally
					{
						this.i18nService.setCurrentLocale(currentLocale);
						this.wileycomI18NService.setCurrentCountry(currentCountry.isPresent() ? currentCountry.get() : null);
					}
				}
			}
		}
		else
		{
			List<T> values = collectValues(indexConfig, indexedProperty, model);
			fieldValues.addAll(createFieldValue(values, indexedProperty, null));
		}
		return fieldValues;
	}

	protected abstract List<T> collectValues(IndexConfig indexConfig, IndexedProperty indexedProperty,
			Object model) throws FieldValueProviderException;

	private List<FieldValue> createFieldValue(final Collection<T> values, final IndexedProperty indexedProperty,
			final String locale)
	{
		if (CollectionUtils.isEmpty(values))
		{
			return Collections.emptyList();
		}

		final List<FieldValue> fieldValues = new ArrayList<>();

		final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, locale);
		for (final String fieldName : fieldNames)
		{
			fieldValues.addAll(values.stream().map(value -> new FieldValue(fieldName, value)).collect(Collectors.toList()));
		}

		return fieldValues;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	@Required
	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}
}
