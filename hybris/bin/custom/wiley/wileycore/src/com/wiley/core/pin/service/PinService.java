package com.wiley.core.pin.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.core.model.PinModel;

/**
 * PIN service.
 *
 * @author Gabor_Bata
 */
public interface PinService
{
	/**
	 * Returns the PIN model with the specified code.
	 *
	 * @param code
	 *           the PIN code
	 * @return the PIN model with the specified code.
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 *            if no PIN with the specified code is found
	 * @throws IllegalArgumentException
	 *            if parameter code is <code>null</code>
	 */
	PinModel getPinForCode(String code);

	/**
	 * Returns the PIN model with the specified order.
	 *
	 * @param order
	 *           the order
	 * @return the PIN model with the specified order.
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 *            if no PIN with the specified order is found
	 * @throws IllegalArgumentException
	 *            if parameter order is <code>null</code>
	 */
	PinModel getPinForOrder(AbstractOrderModel order);

	/**
	 * Check if Order was placed by PIN activation flow
	 *
	 * @param order the order
	 * @return
	 * 		true if it was placed by activating a PIN
	 * 		false otherwise
	 * @throws IllegalArgumentException
	 *            if parameter order is <code>null</code>
	 */
	boolean isPinUsedForOrder(AbstractOrderModel order);
}
