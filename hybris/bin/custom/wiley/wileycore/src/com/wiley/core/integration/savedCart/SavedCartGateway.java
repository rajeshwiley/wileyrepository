package com.wiley.core.integration.savedCart;

import de.hybris.platform.core.model.order.CartModel;

import org.springframework.messaging.handler.annotation.Payload;


public interface SavedCartGateway
{
	/**
	 * Find cart details by id in external system and maps it to Hybris cart
	 *
	 * @param cartId Unique identifier of cart
	 * @return
	 */
	CartModel getCart(@Payload String cartId);
}
