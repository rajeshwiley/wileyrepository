package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.order.CommerceDeliveryModeValidationStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * A strategy for external delivery mode validation
 */
public interface WileycomCommerceDeliveryModeValidationStrategy extends CommerceDeliveryModeValidationStrategy
{

	void invalidateCartDeliveryMode(@Nonnull AbstractOrderModel orderModel,
			@Nonnull List<ExternalDeliveryModeModel> supportedDeliveryModes);
}
