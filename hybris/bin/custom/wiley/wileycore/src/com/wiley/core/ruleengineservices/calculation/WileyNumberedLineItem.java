package com.wiley.core.ruleengineservices.calculation;

import de.hybris.order.calculation.money.Money;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ruleengineservices.calculation.NumberedLineItem;


/**
 * LineItem implementation that incorporates logic implemented for {@link AbstractOrderEntryModel#SUBTOTALPRICE}
 */
public class WileyNumberedLineItem extends NumberedLineItem
{
	private final Money subtotalPrice;

	public WileyNumberedLineItem(final Money basePrice, final Money subtotalPrice)
	{
		super(basePrice);
		this.subtotalPrice = subtotalPrice;
	}

	public WileyNumberedLineItem(final Money basePrice, final Money subtotalPrice, final int numberOfUnits)
	{
		super(basePrice, numberOfUnits);
		this.subtotalPrice = subtotalPrice;
	}

	@Override
	public Money getSubTotal()
	{
		return this.getSubtotalPrice();
	}

	public Money getSubtotalPrice()
	{
		return subtotalPrice;
	}
}
