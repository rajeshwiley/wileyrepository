package com.wiley.core.wileycom.storesession.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.storesession.impl.WileyStoreSessionServiceImpl;
import com.wiley.core.wileycom.storesession.WileycomStoreSessionService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileycomStoreSessionServiceImpl extends WileyStoreSessionServiceImpl implements WileycomStoreSessionService
{

	private static final Logger LOG = LoggerFactory.getLogger(WileycomStoreSessionServiceImpl.class);

	private final WileycomI18NService wileycomI18NService;
	private final WileyCountryService wileyCountryService;
	private final CartService cartService;
	private final CommerceCartService commerceCartService;

	@Autowired
	public WileycomStoreSessionServiceImpl(final WileycomI18NService wileycomI18NService,
			final WileyCountryService wileyCountryService,
			@Qualifier("cartService") final CartService cartService,
			@Qualifier("commerceCartService") final CommerceCartService commerceCartService)
	{
		this.wileycomI18NService = wileycomI18NService;
		this.wileyCountryService = wileyCountryService;
		this.cartService = cartService;
		this.commerceCartService = commerceCartService;
	}

	@Override
	public void setCurrentCountry(final String isoCode, final boolean recalculateCart)
	{
		validateParameterNotNull(isoCode, "IsoCode mustn't be null");
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		boolean isCountryCodeMatch = currentCountry.isPresent() && isoCode.equals(currentCountry.get().getIsocode());
		if (isCountryCodeMatch)
		{
			LOG.debug("Provided country [{}] matches current country", isoCode);
		}
		else
		{
			CountryModel country = wileyCountryService.findCountryByCode(isoCode);
			validateParameterNotNull(isoCode, String.format("Invalid country isoCode provided [%s]", isoCode));

			wileycomI18NService.setCurrentCountry(country);
			triggerCartRecalculation(recalculateCart);
			LOG.debug("Current country [{}] updated with provided value [{}]", country.getIsocode(), isoCode);
		}
	}

	protected void triggerCartRecalculation(final boolean recalculateCart)
	{
		if (recalculateCart && cartService.hasSessionCart())
		{
			final CartModel cart = cartService.getSessionCart();

			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(cart);
				commerceCartService.recalculateCart(parameter);
				LOG.debug("Recalculation triggered for cart [{}]", cart.getCode());
			}
			catch (final CalculationException e)
			{
				throw new RuntimeException("Could not recalculate the session cart.", e);
			}

		}
		else
		{
			LOG.debug("Either cart recalculation was not allowed due to recalculateCart is [{}], "
							+ "or there is no cart attached to current user session [{}] (skipping cart recalculation)",
					recalculateCart, cartService.hasSessionCart());
		}
	}

}
