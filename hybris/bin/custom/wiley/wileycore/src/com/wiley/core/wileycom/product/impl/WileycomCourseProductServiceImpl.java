package com.wiley.core.wileycom.product.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileycom.product.WileycomCourseProductService;


/**
 * This service retrieves course product specific data
 */
public class WileycomCourseProductServiceImpl implements WileycomCourseProductService
{
	@Resource
	private WileyProductService productService;

	@Resource
	private ProductReferenceService productReferenceService;

	@Override
	public List<ProductModel> getAvailableSetsForCourse(final String isbn)
	{
		ProductModel courseProduct = productService.getProductForIsbn(isbn);
		Collection<ProductReferenceModel> productReferenceList = productReferenceService.getProductReferencesForSourceProduct(
				courseProduct,
				ProductReferenceTypeEnum.PRODUCT_SET, true);
		List<ProductModel> productSetList = productReferenceList.stream().map(ref -> ref.getTarget()).collect(
				Collectors.toList());
		return productSetList;
	}
}
