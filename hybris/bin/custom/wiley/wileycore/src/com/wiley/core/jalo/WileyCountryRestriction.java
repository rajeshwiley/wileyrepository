package com.wiley.core.jalo;

import de.hybris.platform.jalo.SessionContext;

public class WileyCountryRestriction extends GeneratedWileyCountryRestriction
{
	@Override
	public String getDescription(final SessionContext sessionContext)
	{
		return "Page is restricted by country";
	}
}
