package com.wiley.core.mpgs.request;

public class WileyRetrieveSessionRequest
{
	private String urlPath;
	private final String paymentProvider;

	public WileyRetrieveSessionRequest(final String url, final String paymentProvider)
	{
		this.urlPath = url;
		this.paymentProvider = paymentProvider;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public String getPaymentProvider()
	{
		return paymentProvider;
	}
}
