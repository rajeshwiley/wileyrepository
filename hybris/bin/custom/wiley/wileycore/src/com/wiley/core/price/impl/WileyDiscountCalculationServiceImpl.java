package com.wiley.core.price.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.wiley.core.price.WileyDiscountCalculationService;


public class WileyDiscountCalculationServiceImpl implements WileyDiscountCalculationService
{

	/**
	 * Note: scale should be grater then 10 to avoid rounding issues
	 */
	private static final int PRICE_WEIGHT_SCALE = 16;

	@Override
	public double getTotalDiscount(final AbstractOrderModel abstractOrder)
	{
		return getProductsDiscountsAmount(abstractOrder) + getOrderDiscountsAmount(abstractOrder);
	}

	@Override
	public BigDecimal getEntryProportionalDiscount(final AbstractOrderEntryModel entry)
	{
		BigDecimal discounts = BigDecimal.valueOf(entry.getDiscountValues()
				.stream()
				.mapToDouble(DiscountValue::getAppliedValue)
				.sum());

		discounts = discounts.add(getEntryOrderLevelProportionalDiscount(entry));
		return discounts;
	}

	@Override
	public BigDecimal getEntryOrderLevelProportionalDiscount(final AbstractOrderEntryModel entry)
	{
		final AbstractOrderModel order = entry.getOrder();
		BigDecimal discounts = BigDecimal.ZERO;

		if (order.getTotalDiscounts() > 0 && order.getSubtotal() > 0)
		{
			BigDecimal entryPriceWeight = BigDecimal.valueOf(entry.getTotalPrice())
					.divide(BigDecimal.valueOf(order.getSubtotal()), PRICE_WEIGHT_SCALE, RoundingMode.CEILING);

			discounts = discounts
					.add(entryPriceWeight.multiply(BigDecimal.valueOf(order.getTotalDiscounts())));
		}
		return discounts;
	}

	protected double getProductsDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;

		final List<AbstractOrderEntryModel> entries = source.getEntries();
		if (entries != null)
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				final List<DiscountValue> discountValues = entry.getDiscountValues();
				if (discountValues != null)
				{
					for (final DiscountValue dValue : discountValues)
					{
						discounts += dValue.getAppliedValue();
					}
				}
			}
		}
		return discounts;
	}

	protected double getOrderDiscountsAmount(final AbstractOrderModel source)
	{
		double discounts = 0.0d;
		final List<DiscountValue> discountList = source.getGlobalDiscountValues(); // discounts on the cart itself
		if (discountList != null && !discountList.isEmpty())
		{
			for (final DiscountValue discount : discountList)
			{
				final double value = discount.getAppliedValue();
				if (value > 0.0d)
				{
					discounts += value;
				}
			}
		}

		return discounts;
	}
}
