package com.wiley.core.exceptions;

/**
 * This exception is thrown if external system responded (response server error).
 */
public class ExternalSystemInternalErrorException extends ExternalSystemException
{
	public ExternalSystemInternalErrorException(final String message)
	{
		super(message);
	}

	public ExternalSystemInternalErrorException(final Throwable cause)
	{
		super(cause);
	}

	public ExternalSystemInternalErrorException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
