package com.wiley.core.payment.strategies;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.strategies.PaymentTransactionStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

public interface WileyPaymentTransactionStrategy extends PaymentTransactionStrategy
{
    PaymentTransactionEntryModel savePaymentTransactionEntry(CustomerModel customerModel, CurrencyModel currency,
			CreateSubscriptionResult subscriptionResult, PaymentTransactionType transactionType);
}
