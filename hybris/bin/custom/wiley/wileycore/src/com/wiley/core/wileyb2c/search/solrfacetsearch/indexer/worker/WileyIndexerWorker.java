package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.worker;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.indexer.workers.IndexerWorkerParameters;
import de.hybris.platform.solrfacetsearch.indexer.workers.impl.DefaultIndexerWorker;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexerWorker extends DefaultIndexerWorker
{
	private WileycomI18NService wileycomI18NService;

	private WileyCountryService wileyCountryService;

	private String workerSessionCountry;

	private BaseSiteService baseSiteService;

	private I18NService i18NService;

	private BaseSiteModel baseSiteModel;

	protected void superInitializeSession()
	{
		super.initializeSession();
	}

	protected void superInitialize(final IndexerWorkerParameters workerParameters)
	{
		super.initialize(workerParameters);
	}

	@Override
	protected void initializeSession()
	{
		superInitializeSession();
		baseSiteService.setCurrentBaseSite(baseSiteModel, true);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(workerSessionCountry));
		i18NService.setLocalizationFallbackEnabled(true);
	}

	@Override
	public void initialize(final IndexerWorkerParameters workerParameters)
	{
		superInitialize(workerParameters);
		baseSiteModel = baseSiteService.getCurrentBaseSite();
		workerSessionCountry = wileycomI18NService.getCurrentCountry().orElse(wileycomI18NService.setDefaultCurrentCountry())
				.getIsocode();
	}

	@Required
	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}

	@Required
	public void setWileyCountryService(final WileyCountryService wileyCountryService)
	{
		this.wileyCountryService = wileyCountryService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}
}