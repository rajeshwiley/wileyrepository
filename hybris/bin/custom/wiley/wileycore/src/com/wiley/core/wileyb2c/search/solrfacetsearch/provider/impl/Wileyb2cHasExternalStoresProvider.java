package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cHasExternalStoresProvider extends AbstractWileycomValueProvider<String>
{
	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			return Collections.singletonList(
					String.valueOf(CollectionUtils.isNotEmpty(((ProductModel) model).getExternalStores())));
		}
		else
		{
			return Collections.emptyList();
		}

	}

}
