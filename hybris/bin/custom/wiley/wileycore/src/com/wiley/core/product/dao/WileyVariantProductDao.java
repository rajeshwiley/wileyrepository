package com.wiley.core.product.dao;


import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.Optional;
import com.wiley.core.model.WileyVariantProductModel;


/**
 * Contains methods for searching {@link com.wiley.core.model.WileyVariantProductModel} models.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyVariantProductDao {




  /**
   *  Returns the WileyVariantProduct with the specified ISBN belonging to the specified
   * CatalogVersion.
   *
   * @param isbn
   * @param catalogVersion
   * @return  Optional<WileyVariantProductModel>
   */
  Optional<WileyVariantProductModel> findProductByISBNIfExists(String isbn,
      CatalogVersionModel catalogVersion);

}
