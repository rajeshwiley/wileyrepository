package com.wiley.core.pin.dao.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.ImmutableMap;
import com.wiley.core.model.PinModel;
import com.wiley.core.pin.dao.PinDao;


/**
 * Default implementation for accessing Pin models from persistence layer.
 */
public class DefaultPinDao extends DefaultGenericDao<PinModel> implements PinDao
{
	public DefaultPinDao()
	{
		super(PinModel._TYPECODE);
	}

	@Override
	public PinModel findPinByCode(final String pinCode)
	{
		List<PinModel> pins = find(ImmutableMap.of(PinModel.CODE, pinCode));
		return CollectionUtils.isNotEmpty(pins) ? pins.get(0) : null;
	}

	@Override
	public PinModel findPinByOrder(final AbstractOrderModel order)
	{
		List<PinModel> pins = find(ImmutableMap.of(PinModel.ORDER, order));
		return CollectionUtils.isNotEmpty(pins) ? pins.get(0) : null;
	}
}
