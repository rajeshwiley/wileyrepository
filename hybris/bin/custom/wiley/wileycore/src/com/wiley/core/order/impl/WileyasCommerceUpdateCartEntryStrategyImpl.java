package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.event.EventService;

import javax.annotation.Resource;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.event.WileyOrderEntryEvent;


public class WileyasCommerceUpdateCartEntryStrategyImpl extends DefaultCommerceUpdateCartEntryStrategy
{
	@Resource
	private EventService eventService;

	@Resource
	private TransactionTemplate transactionTemplate;

	@Override
	protected CommerceCartModification modifyEntry(final CartModel cartModel, final AbstractOrderEntryModel entryToUpdate,
			final long actualAllowedQuantityChange, final long newQuantity, final Integer maxOrderQuantity)
	{
		return transactionTemplate.execute(new TransactionCallback<CommerceCartModification>()
		{
			@Override
			public CommerceCartModification doInTransaction(final TransactionStatus transactionStatus)
			{
				WileyOrderEntryEvent wileyOrderEntryEvent = new WileyOrderEntryEvent("DELETED", entryToUpdate);
				eventService.publishEvent(wileyOrderEntryEvent);
				return superModifyEntry(cartModel, entryToUpdate, actualAllowedQuantityChange, newQuantity, maxOrderQuantity);
			}
		});
	}

	private CommerceCartModification superModifyEntry(final CartModel cartModel, final AbstractOrderEntryModel entryToUpdate,
			final long actualAllowedQuantityChange, final long newQuantity, final Integer maxOrderQuantity)
	{
		return super.modifyEntry(cartModel, entryToUpdate, actualAllowedQuantityChange, newQuantity, maxOrderQuantity);
	}
}
