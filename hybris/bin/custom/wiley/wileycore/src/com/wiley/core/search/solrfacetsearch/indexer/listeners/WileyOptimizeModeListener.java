package com.wiley.core.search.solrfacetsearch.indexer.listeners;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.listeners.OptimizeModeListener;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.util.StopWatch;


/**
 * Substitution for ootb listener.
 * OptimizeModeListener doesn't wait for searcher, WileyOptimizeModeListener waits (by parameter waitSearcher=true).
 */
public class WileyOptimizeModeListener extends OptimizeModeListener
{
	private static final Logger LOG = Logger.getLogger(WileyOptimizeModeListener.class);
	private static final boolean DO_WAIT_SEARCHER = true;
	private static final boolean DO_NOT_WAIT_FLUSH = false;

	protected void optimize(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType, final Index index)
			throws IndexerException
	{
		LOG.info("Performing optimize on " + index.getName() + " (" + facetSearchConfig.getName() + "/" + indexedType
				.getUniqueIndexedTypeCode() + ")");

		SolrClient solrClient = null;
		try
		{
			SolrSearchProvider provider = getSolrSearchProviderFactory().getSearchProvider(facetSearchConfig, indexedType);
			solrClient = provider.getClientForIndexing(index);

		}
		catch (SolrServiceException e)
		{
			LOG.error("Listener failed on getting SOLR client");
			throw new IndexerException(e);
		}

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		try
		{
			solrClient.optimize(index.getName(), DO_NOT_WAIT_FLUSH, DO_WAIT_SEARCHER);
			stopWatch.stop();
			LOG.info("SOLR optimization finished in " + stopWatch.getTotalTimeMillis() + " ms");
		}
		catch (IOException | SolrServerException e)
		{
			LOG.error("Optimization failed, check SOLR logs");
			throw new IndexerException(e);
		}
		finally
		{
			IOUtils.closeQuietly(solrClient);
		}


	}
}
