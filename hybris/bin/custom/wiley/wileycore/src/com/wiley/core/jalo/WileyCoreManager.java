package com.wiley.core.jalo;

import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncJob;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloImplementationManager;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import de.hybris.platform.util.JspContext;

import java.util.Map;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.setup.CoreSystemSetup;
import com.wiley.core.setup.JUnitTenantSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 */
@SuppressWarnings("PMD")
public class WileyCoreManager extends GeneratedWileyCoreManager
{

	static
	{
		new Registry.Init()
		{
			@Override
			protected void startup()
			{
				JaloImplementationManager.replaceCoreJaloClass(CatalogVersionSyncJob.class, WileyCatalogVersionSyncJob.class);
			}
		};
	}

	/**
	 * Gets instance.
	 *
	 * @return the instance
	 */

	public static final WileyCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WileyCoreManager) em.getExtension(WileyCoreConstants.EXTENSIONNAME);
	}

	@Override
	public void notifyInitializationEnd(final Map<String, String> params, final JspContext ctx) throws Exception
	{
		JUnitTenantSetup junitTenantSetup = Registry.getApplicationContext().getBean("junitTenantSetup", JUnitTenantSetup.class);
		junitTenantSetup.createJunitTenantTestData();
		junitTenantSetup.activateJunitTenantSolrIndexerCronJobs();
		super.notifyInitializationEnd(params, ctx);
	}
}
