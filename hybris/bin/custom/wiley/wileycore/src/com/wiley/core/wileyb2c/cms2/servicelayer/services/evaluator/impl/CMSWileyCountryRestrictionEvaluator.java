package com.wiley.core.wileyb2c.cms2.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.core.model.c2l.CountryModel;

import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyCountryRestrictionModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


public class CMSWileyCountryRestrictionEvaluator implements CMSRestrictionEvaluator<WileyCountryRestrictionModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CMSWileyCountryRestrictionEvaluator.class);

	@Autowired
	private WileycomI18NService wileycomI18NService;

	@Override
	public boolean evaluate(final WileyCountryRestrictionModel wileyCountryRestrictionModel,
			final RestrictionData restrictionData)
	{
		final List<CountryModel> restrictionCountries = wileyCountryRestrictionModel.getCountries();
		final Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent() && CollectionUtils.isNotEmpty(restrictionCountries)) {
			boolean containsCurrentCountry = restrictionCountries.contains(currentCountry.get());
			return wileyCountryRestrictionModel.isAllow() ? containsCurrentCountry : !containsCurrentCountry;
		} else {
			LOG.info("No country in session is selected. Wiley Country restriction will not be evaluated");
			return true;
		}

	}
}
