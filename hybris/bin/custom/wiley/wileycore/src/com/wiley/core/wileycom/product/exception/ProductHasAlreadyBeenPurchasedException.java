package com.wiley.core.wileycom.product.exception;

/**
 * Created by Raman_Hancharou on 8/10/2016.
 */
public class ProductHasAlreadyBeenPurchasedException extends RuntimeException
{
	public ProductHasAlreadyBeenPurchasedException(final String message)
	{
		super(message);
	}
}
