package com.wiley.core.integration.edi.job;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.integration.edi.WileyEdiExportGateway;
import com.wiley.core.integration.edi.WileyShippableExcelExportGateway;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * This job is responsible for running EDI export every some time specified in configuration
 */
public class EdiExportJob extends WileyAbstractExportJob
{

	private static final Logger LOG = Logger.getLogger(EdiExportJob.class);

	@Resource
	private WileyEdiExportGateway wileyEdiExportGateway;
	@Resource
	private WileyShippableExcelExportGateway wileyShippableExcelExportGateway;

	protected void handleExport(final String paymentType, final BaseStoreModel baseStore)
	{
		LOG.info(String.format("Running EDI export for store: [%s], payment type: [%s]", baseStore.getUid(), paymentType));
		try
		{
			final String shippableReportMailList = configurationService.getConfiguration().getString(
					"wiley.orders.shippable.report.to.email.list");

			if (StringUtils.isEmpty(shippableReportMailList))
			{
				LOG.warn("No recipients have been provided for EdiExportJob!..");
				return;
			}

			final List<WileyExportProcessModel> wileyExportProcesses = orderProvisionStrategy.getExportProcessesReadyForExport(
					baseStore, paymentType);
			wileyShippableExcelExportGateway.exportOrders(wileyExportProcesses, baseStore, LocalDateTime.now(), paymentType,
					shippableReportMailList);

			final List<WileyExportProcessModel> wileyExportProcessesForEDI = orderProvisionStrategy.removePartialRefundsProcesses(
					wileyExportProcesses);
			wileyExportProcessesForEDI.addAll(orderProvisionStrategy.getZeroDollarPhysicalPartnerOrderProcessesReadyForExport(
					baseStore, paymentType));

			wileyEdiExportGateway.exportOrders(wileyExportProcessesForEDI, baseStore, LocalDateTime.now(), paymentType,
					shippableReportMailList);
		}
		catch (Exception e)
		{
			LOG.error("Exception occurs during running export for store: [" + baseStore.getUid() + "]", e);
		}
	}
}