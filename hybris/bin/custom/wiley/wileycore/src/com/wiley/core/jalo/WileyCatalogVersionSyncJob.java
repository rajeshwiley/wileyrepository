package com.wiley.core.jalo;

import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncJob;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.cronjob.jalo.CronJob.CronJobResult;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.event.EventService;

import com.wiley.core.event.AfterCronJobFinishedEvent;


public class WileyCatalogVersionSyncJob extends CatalogVersionSyncJob
{

	@Override
	protected CronJobResult performCronJob(final CronJob cronJob)
	{
		final CronJobResult result = super.performCronJob(cronJob);
		sendFinishedEvent(result, cronJob);
		return result;
	}

	private void sendFinishedEvent(final CronJobResult cronJobResult, final CronJob cronJob)
	{
		final AfterCronJobFinishedEvent event = prepareEvent(cronJobResult, cronJob);
		final EventService eventService = (EventService) Registry.getApplicationContext().getBean("eventService");
		eventService.publishEvent(event);
	}

	private AfterCronJobFinishedEvent prepareEvent(final CronJobResult cronJobResult, final CronJob cronJob)
	{
		final AfterCronJobFinishedEvent event = new AfterCronJobFinishedEvent();
		event.setCronJobResult(getEnumValue("CronJobResult", cronJobResult.getResult().getCode()));
		event.setCronJobStatus(getEnumValue("CronJobStatus", cronJobResult.getStatus().getCode()));
		event.setCronJobCode(cronJob.getCode());
		event.setCronJobType(cronJob.getComposedType().getCode());
		event.setJobCode(cronJob.getJob().getCode());
		return event;
	}

	private <T extends HybrisEnumValue> T getEnumValue(final String enumCode, final String valueCode)
	{
		final EnumerationService enumerationService = (EnumerationService) Registry.getApplicationContext()
				.getBean("enumerationService");
		return enumerationService.getEnumerationValue(enumCode, valueCode);
	}

}
