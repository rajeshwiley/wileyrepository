package com.wiley.core.wileyas.jalo;

import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrderEntry;

import java.util.Date;

import com.wiley.core.jalo.WileyEurope1PriceFactory;


/**
 * Price factory with specific logic for Wiley AS.
 */
public class WileyasOrderEditEurope1PriceFactory extends WileyEurope1PriceFactory
{
	@Override
	protected Date getBasePriceDate(final AbstractOrderEntry entry)
	{
		final SessionContext ctx = getSession().getSessionContext();
		return entry.getOrder(ctx).getDate(ctx);
	}
}