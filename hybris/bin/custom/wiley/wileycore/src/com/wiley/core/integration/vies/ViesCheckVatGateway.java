package com.wiley.core.integration.vies;

import com.wiley.core.integration.vies.dto.CheckVatResponseDto;


public interface ViesCheckVatGateway
{
	CheckVatResponseDto checkVat(String countryCode, String vatNumber);
}
