package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cResolveIssnStrategy extends AbstractWileyb2cResolveClassificationAttributeStrategy
{
	private static final String REGEX_ISSN = "(\\d{4})(\\d{4})";
	private static final String ISSN_FORMAT_PATTERN = "$1-$2";
	static final ImmutableSet<Wileyb2cClassificationAttributes>
			CLASSIFICATION_ATTRIBUTES = Sets.immutableEnumSet(Wileyb2cClassificationAttributes.PRINT_ISSN,
			Wileyb2cClassificationAttributes.ONLINE_ISSN);

	@Override
	protected String processFeature(final Feature feature)
	{
		return feature.getValue().getValue().toString().replaceFirst(REGEX_ISSN, ISSN_FORMAT_PATTERN);
	}

	@Override
	protected Set<Wileyb2cClassificationAttributes> getAttributes()
	{
		return CLASSIFICATION_ATTRIBUTES;
	}

}