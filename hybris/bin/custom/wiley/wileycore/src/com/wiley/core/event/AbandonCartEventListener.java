package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.time.Instant;
import java.util.Date;

import com.wiley.core.model.AbandonCartEmailHistoryEntryModel;
import com.wiley.core.model.cartprocessing.CartProcessModel;


/**
 * Listener for abandon cart event.
 */
public class AbandonCartEventListener extends AbstractWileySiteEventListener<AbandonCartEvent>
{
	/**
	 * On site event.
	 *
	 * @param abandonCartEvent
	 * 		the abandon cart event
	 */
	@Override
	protected void onSiteEvent(final AbandonCartEvent abandonCartEvent)
	{
		final CartModel cartModel = abandonCartEvent.getCart();
		final CartProcessModel cartProcessModel = (CartProcessModel) getBusinessProcessService().createProcess(
				"abandonCartEmailProcess-" + cartModel.getCode() + "-" + System.currentTimeMillis(),
				"abandonCartEmailProcess");
		cartProcessModel.setCart(cartModel);
		getModelService().save(cartProcessModel);
		createHistoryEntry(cartModel);
		getBusinessProcessService().startProcess(cartProcessModel);


	}

	private void createHistoryEntry(final CartModel cartModel)
	{
		final Instant instant = Instant.now();
		final AbandonCartEmailHistoryEntryModel abandonCartEmailInformation = getModelService().create(
				AbandonCartEmailHistoryEntryModel.class);
		abandonCartEmailInformation.setOrder(cartModel);
		abandonCartEmailInformation.setEmailSentDate(Date.from(instant));
		getModelService().save(abandonCartEmailInformation);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final AbandonCartEvent event)
	{
		final CartModel cart = event.getCart();
		ServicesUtil.validateParameterNotNullStandardMessage("event.cart", cart);
		final BaseSiteModel site = cart.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.cart.site", site);
		return true;
	}
}
