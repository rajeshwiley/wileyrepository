package com.wiley.core.payment.request;

import de.hybris.platform.payment.commands.request.FollowOnRefundRequest;

import java.math.BigDecimal;
import java.util.Currency;


public class WileyFollowOnRefundRequest extends FollowOnRefundRequest
{
	private String siteId;
	private String urlPath;
	private String orderCode;
	private String transactionReference;

	public WileyFollowOnRefundRequest(final String merchantTransactionCode, final String requestId, final String requestToken,
			final Currency currency,
			final BigDecimal totalAmount, final String paymentProvider, final String siteId, final String orderCode)
	{
		super(merchantTransactionCode, requestId, requestToken, currency, totalAmount, paymentProvider);
		this.siteId = siteId;
		this.orderCode = orderCode;
	}
	public WileyFollowOnRefundRequest(final String urlPath, final String paymentProvider, final Currency currency,
			final BigDecimal totalAmount)
	{
		super(null, null, null, currency, totalAmount, paymentProvider);
		this.urlPath = urlPath;
	}
	public String getSiteId()
	{
		return siteId;
	}

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public String getTransactionReference()
	{
		return transactionReference;
	}

	public void setTransactionReference(final String transactionReference)
	{
		this.transactionReference = transactionReference;
	}
}
