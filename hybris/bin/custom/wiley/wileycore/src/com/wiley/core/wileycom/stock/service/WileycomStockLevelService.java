package com.wiley.core.wileycom.stock.service;

import de.hybris.platform.ordersplitting.model.StockLevelModel;

import javax.annotation.Nonnull;


/**
 * Created by Raman_Hancharou on 4/4/2017.
 */
public interface WileycomStockLevelService
{
	/**
	 * Find stockLevel by code.
	 *
	 * @param stockLevelCode
	 * 		the stock level code
	 * @return the stock level model
	 */
	StockLevelModel findStockLevelByCode(@Nonnull String stockLevelCode);
}
