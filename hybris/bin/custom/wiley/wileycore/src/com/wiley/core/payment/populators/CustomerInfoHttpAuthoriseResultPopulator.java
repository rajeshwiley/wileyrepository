package com.wiley.core.payment.populators;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;
import java.util.Map;


public class CustomerInfoHttpAuthoriseResultPopulator
		extends AbstractResultPopulator<Map<String, String>, CreateSubscriptionResult>
{
	private static final String TRANSACTION_ID_PARAM = "transID";

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource
	private WileyCustomerAccountService wileyCustomerAccountService;

	@Override
	public void populate(final Map<String, String> wpgResponseParams, final CreateSubscriptionResult createSubscriptionResult)
			throws ConversionException
	{
		CustomerInfoData customerInfoData = new CustomerInfoData();

		final AddressModel paymentAddress = getBillingAddress(wpgResponseParams.get(TRANSACTION_ID_PARAM));
		if (paymentAddress != null)
		{
			customerInfoData.setBillToCity(paymentAddress.getTown());
			customerInfoData.setBillToCompany(paymentAddress.getCompany());
			customerInfoData.setBillToEmail(paymentAddress.getEmail());
			customerInfoData.setBillToFirstName(paymentAddress.getFirstname());
			customerInfoData.setBillToLastName(paymentAddress.getLastname());
			customerInfoData.setBillToPhoneNumber(paymentAddress.getPhone1());
			customerInfoData.setBillToStreet1(paymentAddress.getStreetname());
			customerInfoData.setBillToStreet2(paymentAddress.getStreetnumber());
			customerInfoData.setBillToPostalCode(paymentAddress.getPostalcode());
			customerInfoData.setBillToExternalId(paymentAddress.getExternalId());

			if (paymentAddress.getRegion() != null)
			{
				customerInfoData.setBillToState(paymentAddress.getRegion().getIsocodeShort());
			}
			if (paymentAddress.getCountry() != null && paymentAddress.getCountry().getIsocode() != null)
			{
				customerInfoData.setBillToCountry(paymentAddress.getCountry().getIsocode().toUpperCase());
			}
			createSubscriptionResult.setCustomerInfoData(customerInfoData);
		}
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	public AddressModel getBillingAddress(final String transactionId)
	{
		CartModel sessionCart = getCartService().getSessionCart();
		AddressModel selectedAddress;
		AddressModel cartBillingAddress = sessionCart.getPaymentAddress();
		AddressModel pendingBillingAddress = ((CustomerModel) sessionCart.getUser()).getPendingBillingAddress();
		AddressModel customerPaymentAddress = wileyCustomerAccountService.getPaymentAddress(sessionCart.getUser());

		if (isTransactionAddress(cartBillingAddress, transactionId))
		{
			selectedAddress = cartBillingAddress;
		} else if (isTransactionAddress(pendingBillingAddress, transactionId))
		{
			selectedAddress = pendingBillingAddress;
		} else if (customerPaymentAddress != null)
		{
			selectedAddress = customerPaymentAddress;
		} else
		{
			selectedAddress = sessionCart.getDeliveryAddress();
		}
		return selectedAddress;
	}

	private boolean isTransactionAddress(final AddressModel address, final String transactionId)
	{
		return address != null && transactionId.equals(address.getTransactionId());
	}
}
