package com.wiley.core.aspect;

import de.hybris.platform.cronjob.jalo.CronJob;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Aspect
public class WileyCronJobSendEmailOnFailingAspect
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyCronJobSendEmailOnFailingAspect.class);


	@Around("execution(* de.hybris.platform.cronjob.jalo.CronJob.setCronJobResult(..))")
	public void aroundSetCronJobResultWriteLogMessage(final ProceedingJoinPoint pjp) {
		proceedAspect(pjp);
		CronJob cronJob = (CronJob) pjp.getTarget();
		if (isFailedCronJobResult(cronJob)) {
			LOG.error(String.format("CronJob [%s] has failed, result: [%s]",
					cronJob.getCode(), cronJob.getResult().getName()));
		}
	}


	private boolean isFailedCronJobResult(final CronJob cronJob)
	{
		if (cronJob == null || cronJob.getResult() == null || cronJob.getResult().getName() == null) {
			return false;
		}

		switch (cronJob.getResult().getName())
		{
			case "ERROR":
			case "FAILURE":
			case "UNKNOWN":
				return true;
			default:
				return false;
		}
	}

	private void proceedAspect(final ProceedingJoinPoint pjp)
	{
		try
		{
			pjp.proceed();
		}
		catch (Throwable throwable)
		{
			LOG.error("Error during proceeding an aspect.", throwable);
		}
	}

}
