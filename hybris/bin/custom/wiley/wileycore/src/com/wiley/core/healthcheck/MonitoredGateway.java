package com.wiley.core.healthcheck;

import com.codahale.metrics.health.HealthCheck.Result;


public interface MonitoredGateway
{
	Result doHealthCheck();
}