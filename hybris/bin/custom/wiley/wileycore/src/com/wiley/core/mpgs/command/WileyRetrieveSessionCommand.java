package com.wiley.core.mpgs.command;


import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;


public interface WileyRetrieveSessionCommand extends WileyCommand<WileyRetrieveSessionRequest, WileyRetrieveSessionResponse>
{
}