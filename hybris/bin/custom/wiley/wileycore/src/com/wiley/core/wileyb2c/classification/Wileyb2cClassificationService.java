package com.wiley.core.wileyb2c.classification;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cClassificationService
{

	ClassificationClassModel resolveClassificationClass(ProductModel productModel);


	String resolveAttribute(ProductModel product, Wileyb2cClassificationAttributes attribute);

	<T> T resolveAttributeValue(ProductModel product, Wileyb2cClassificationAttributes attribute);
}
