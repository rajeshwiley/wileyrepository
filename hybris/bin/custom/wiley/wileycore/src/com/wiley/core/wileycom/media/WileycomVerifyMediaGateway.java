package com.wiley.core.wileycom.media;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Payload;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomVerifyMediaGateway
{

	/**
	 * Verify is URL correct
	 *
	 * @param url
	 * @throws com.wiley.core.exceptions.ExternalSystemException
	 * 		in case URL is broken
	 */
	void verifyURL(@Nonnull @Payload String url);
}
