package com.wiley.core.wel.preorder.cms2.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.Objects;
import com.wiley.core.model.WelPreOrderConfirmationCMSRestrictionModel;


/**
 * It is used by the PreOrderConfirmationTextParagraph component only,
 * that is used on the WEL order confirmation page for PreOrder products.
 * A different component will be always shown(Restriction doesn't work for other components).
 * The general contract is: if there is no pre-order, the PreOrderConfirmationTextParagraph is hidden.
 */
public class PreOrderConfirmationTextParagraphRestrictionEvaluator
		extends AbstractWelCMSRestrictionEvaluator<WelPreOrderConfirmationCMSRestrictionModel>
{
	public static final String PRE_ORDER_CONFIRMATION_TEXT_PARAGRAPH = "PreOrderConfirmationTextParagraph";

	@Override
	public boolean evaluate(final WelPreOrderConfirmationCMSRestrictionModel welPreOrderConfirmationCMSRestrictionModel,
			final RestrictionData restrictionData)
	{
		String componentUid = getComponentUid(restrictionData);
		if (!Objects.equals(componentUid, PRE_ORDER_CONFIRMATION_TEXT_PARAGRAPH))
		{
			return Boolean.TRUE;
		}

		final Boolean isPreOrderConfirmationTextShown = isPreOrder();

		return isPreOrderConfirmationTextShown;
	}
}
