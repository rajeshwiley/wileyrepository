package com.wiley.core.integration.handlers;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.util.Assert;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileySqsInboundMessageTransformErrorHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySqsInboundMessageTransformErrorHandler.class);
	private static final String AWS_MESSAGE_ID = "aws_messageId";
	private static final String AWS_QUEUE = "aws_queue";
	private static final String MESSAGE_GROUP_ID = "MessageGroupId";

	public void onError(@Nonnull final ErrorMessage errorMessage)
	{
		Assert.notNull(errorMessage);
		Assert.notNull(errorMessage.getPayload());

		final Throwable messagingException = errorMessage.getPayload();
		final Throwable originalException = messagingException.getCause();

		if (messagingException instanceof MessagingException)
		{
			final MessageHeaders headers = ((MessagingException) messagingException)
					.getFailedMessage().getHeaders();

			LOG.error("SQS Failed to process inbound message with "
					+ buildStringValueForHeader(headers, AWS_MESSAGE_ID)
					+ " and " + buildStringValueForHeader(headers, MESSAGE_GROUP_ID)
					+ " from " + buildStringValueForHeader(headers, AWS_QUEUE), originalException);
		}
		else
		{
			LOG.error("SQS Unexpected error during processing of inbound message", messagingException);
		}
	}

	private String buildStringValueForHeader(final MessageHeaders headers, final String header)
	{
		return header + "=" + headers.get(header);
	}
}
