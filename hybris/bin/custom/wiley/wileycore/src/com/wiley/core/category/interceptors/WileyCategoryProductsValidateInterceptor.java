package com.wiley.core.category.interceptors;

import com.wiley.core.category.WileyCategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This is opposite side validation similar to one performed by
 * OOTB de.hybris.platform.variants.interceptor.GenericVariantProductValidateInterceptor
 * (or our custom WelGenericVariantProductValidateInterceptor) but performed for Category. It verify that given category
 * does not contain any Generic Variants if it is not VariantCategoryModel or Partner's Category.
 *
 * Product validator works well in most cases but when we create new category in cockpit wizard (with added products) it
 * fails to detect wrong product-category relation. It was the reason to create Category side validator
 *
 */
public class WileyCategoryProductsValidateInterceptor implements ValidateInterceptor<CategoryModel>
{
	@Autowired
	private WileyCategoryService wileyCategoryService;
	@Override
	public void onValidate(final CategoryModel categoryModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (categoryModel instanceof VariantCategoryModel
				|| wileyCategoryService.isPartnersCategory(categoryModel)
				|| CollectionUtils.isEmpty(categoryModel.getProducts())) {
			return;
		}
		for (ProductModel product : categoryModel.getProducts()) {
			if (product instanceof GenericVariantProductModel) {
				throw new InterceptorException("Only variant or partner category can include variant products. "
						+ "Partner category must be a child of WEL_PARTNERS_CATEGORY supercategory.");
			}
		}
	}
}
