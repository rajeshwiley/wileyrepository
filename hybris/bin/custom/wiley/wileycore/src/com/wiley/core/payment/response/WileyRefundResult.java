package com.wiley.core.payment.response;

import de.hybris.platform.payment.commands.result.RefundResult;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;


public class WileyRefundResult extends RefundResult
{
	private WileyTransactionStatusEnum wpgTransactionStatus;
	private String payPalTransactionErrorDetails;
	private String merchantResponse;

	public String getMerchantResponse()
	{
		return merchantResponse;
	}

	public void setMerchantResponse(final String merchantResponse)
	{
		this.merchantResponse = merchantResponse;
	}

	public WileyTransactionStatusEnum getWpgTransactionStatus()
	{
		return wpgTransactionStatus;
	}

	public void setWpgTransactionStatus(final WileyTransactionStatusEnum wpgTransactionStatus)
	{
		this.wpgTransactionStatus = wpgTransactionStatus;
	}

	public void setPayPalTransactionErrorDetails(final String payPalTransactionErrorDetails)
	{
		this.payPalTransactionErrorDetails = payPalTransactionErrorDetails;
	}

	public String getPayPalTransactionErrorDetails()
	{
		return payPalTransactionErrorDetails;
	}
}
