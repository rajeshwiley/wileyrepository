package com.wiley.core.subscription.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceRenewal;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.esb.EsbB2CSubscriptionGateway;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.core.subscription.dao.WileySubscriptionDao;
import com.wiley.core.subscription.services.SubscriptionMergingStrategy;
import com.wiley.core.subscription.services.WileySubscriptionService;

import static com.wiley.core.enums.WileyProductSubtypeEnum.SUBSCRIPTION;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;


/**
 * Service for subscriptions. Default implementation of {@link WileySubscriptionService}.
 */
public class WileySubscriptionServiceImpl implements WileySubscriptionService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySubscriptionServiceImpl.class);
	public static final ZoneId SUBSCRIPTION_EXPIRATION_ZONE_ID = ZoneId.systemDefault();

	@Resource
	private WileySubscriptionDao subscriptionDao;

	@Resource
	private TimeService timeService;

	@Resource
	private WileySubscriptionCodeGenerator wileySubscriptionCodeGenerator;

	@Resource
	private ModelService modelService;

	@Resource
	private SubscriptionMergingStrategy subscriptionMergingStrategy;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private EsbB2CSubscriptionGateway esbB2CSubscriptionGateway;

	/**
	 * Sets model service.
	 *
	 * @param modelService
	 * 		the model service
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * Sets subscription dao.
	 *
	 * @param subscriptionDao
	 * 		the subscription dao
	 */
	public void setSubscriptionDao(final WileySubscriptionDao subscriptionDao)
	{
		this.subscriptionDao = subscriptionDao;
	}

	/**
	 * Sets subscription merging strategy.
	 *
	 * @param subscriptionMergingStrategy
	 * 		the subscription merging strategy
	 */
	public void setSubscriptionMergingStrategy(
			final SubscriptionMergingStrategy subscriptionMergingStrategy)
	{
		this.subscriptionMergingStrategy = subscriptionMergingStrategy;
	}

	@Override
	public SearchPageData<WileySubscriptionModel> getSubscriptionPage(final PageableData pageableData)
	{
		final UserModel currentUser = userService.getCurrentUser();
		final Collection<CatalogVersionModel> catalogVersions = catalogVersionService.getSessionCatalogVersions();
		return subscriptionDao.getSubscriptions(currentUser, catalogVersions, pageableData);
	}

	@Override
	public WileySubscriptionModel getSubscriptionByCode(final String code)
	{

		final List<WileySubscriptionModel> subscriptions = subscriptionDao.getSubscription(code);
		validateIfSingleResult(subscriptions, format("WileySubscription for code  '%s' not found!", code),
				format("WileySubscription code '%s' is not unique, %d subscriptions found!", code,
						Integer.valueOf(subscriptions.size())));

		return subscriptions.get(0);
	}

	@Override
	public WileySubscriptionModel getSubscription(final UserModel customer,
			final SubscriptionStatus status)
	{
		final List<WileySubscriptionModel> subscriptions = subscriptionDao.getSubscription(customer,
				status);

		if (CollectionUtils.isEmpty(subscriptions))
		{
			return null;
		}
		if (subscriptions.size() > 1)
		{
			throw new AmbiguousIdentifierException(
					format(
							"WileySubscription for customer '%s' and status '%s' is not unique, %d subscriptions found!",
							customer, status, Integer.valueOf(subscriptions.size())));
		}
		return subscriptions.get(0);
	}

	@Override
	public WileySubscriptionModel createSubscriptionFromOrder(final AbstractOrderEntryModel orderEntry) throws
			ModelSavingException
	{
		final AbstractOrderModel order = orderEntry.getOrder();
		ServicesUtil.validateParameterNotNull(order, "Order can not be null");
		final ProductModel product = orderEntry.getProduct();
		final Set<WileySubscriptionModel> modelsToSave = new HashSet<>();
		if (product instanceof SubscriptionProductModel)
		{
			final SubscriptionProductModel subscriptionProduct = (SubscriptionProductModel) product;
			final SubscriptionTermModel term = subscriptionProduct.getSubscriptionTerm();
			final WileySubscriptionModel result = modelService.create(WileySubscriptionModel.class);
			result.setCode(wileySubscriptionCodeGenerator.generateCode(orderEntry));
			result.setStartDate(order.getDate());
			result.setProduct(subscriptionProduct);
			result.setHybrisRenewal(true);
			final UserModel user = order.getUser();
			if (user instanceof CustomerModel)
			{
				final CustomerModel customer = (CustomerModel) user;
				result.setCustomer(customer);
				final WileySubscriptionModel activeSubscription = getSubscription(customer, SubscriptionStatus.ACTIVE);
				result.setStatus(SubscriptionStatus.ACTIVE);
				result.setExpirationDate(subscriptionMergingStrategy.mergeSubscriptions(activeSubscription, result));
				if (activeSubscription != null)
				{
					activeSubscription.setStatus(SubscriptionStatus.EXPIRED); // need to check if not expired!
					modelsToSave.add(activeSubscription);
				}
			}

			result.setRenewalEnabled(getRenewalStatus(term, order));

			result.setOrderEntry(orderEntry);
			result.setIsbn(subscriptionProduct.getIsbn());

			modelsToSave.add(result);
			modelService.saveAll(modelsToSave);
			return result;
		}
		return null;
	}

	/**
	 * Set renewal of Subscription based on SubscriptionTerm and TotalPrice
	 *
	 * @param term
	 * 		TermOfServiceRenewal of Subscription
	 * @param order
	 * 		Customer Order
	 */
	private boolean getRenewalStatus(final SubscriptionTermModel term, final AbstractOrderModel order)
	{
		return !(isTotalPriceZero(order) || TermOfServiceRenewal.NON_RENEWING.equals(term.getTermOfServiceRenewal()));
	}

	private boolean isTotalPriceZero(final AbstractOrderModel orderModel)
	{
		return null != orderModel && null != orderModel.getTotalPrice() && Math.abs(orderModel.getTotalPrice())
				< 2 * Double.MIN_VALUE;
	}

	public boolean isSubscriptionInProgress(final String subscriptionCode)
	{
		return CollectionUtils.isNotEmpty(subscriptionDao.findRenewalProcessesForSubscription(subscriptionCode,
				ProcessState.RUNNING, ProcessState.WAITING));
	}

	@Override
	public WileySubscriptionModel updateAutoRenew(final WileySubscriptionModel subscription)
	{
		if (subscription.getRenewalEnabled())
		{
			modelService.save(subscription);
		}
		return subscription;
	}

	@Override
	public void updateSubscriptionStatus(final Collection<WileySubscriptionModel> subscriptions,
			final SubscriptionStatus newStatus)
	{
		subscriptions.stream().forEach(subscription -> subscription.setStatus(newStatus));
		modelService.saveAll(subscriptions);
	}

	@Nonnull
	@Override
	/** The current implementation is based on TimeService
	 * and the assumption, that CronJob is triggered after midnight of new day
	 */
	public List<WileySubscriptionModel> getAllExpiredSubscriptions()
	{
		final Date currentTime = timeService.getCurrentTime();

		LOG.debug("Current time: {}", currentTime);

		final ZonedDateTime currentDateTime = ZonedDateTime.ofInstant(currentTime.toInstant(), SUBSCRIPTION_EXPIRATION_ZONE_ID);
		// !! The strict assumption that this method id called at the beginning of new date
		// the moment is the end of expirationDay that is equal to begin of new day
		final ZonedDateTime startOfNewDay = currentDateTime.truncatedTo(ChronoUnit.DAYS);

		return subscriptionDao.findSubscriptionsWithExpirationDateBefore(SubscriptionStatus.ACTIVE, startOfNewDay);
	}

	@Override
	@Nonnull
	public List<WileySubscriptionModel> getAllSubscriptionsWhichShouldBeRenewed()
	{
		// getting time of the execution context. Usually it is system time.
		final Date currentTime = timeService.getCurrentTime();

		LOG.debug("Current time: {}", currentTime);

		final ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(currentTime.toInstant(), SUBSCRIPTION_EXPIRATION_ZONE_ID);

		final List<WileySubscriptionModel> activeSubscriptions = getActiveSubscriptionsExpiredOnDay(zonedDateTime);

		// returns only subscriptions which require renewal.
		return activeSubscriptions.stream()
				.filter(subscription -> Boolean.TRUE.equals(subscription.getHybrisRenewal()))
				.filter(this::isRequiredRenewal)
				.collect(Collectors.toList());
	}

	@Override
	public boolean doesAnySubscriptionProductExistInCart(@Nonnull final CartModel cart)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		boolean hasAnySubscription = false;

		final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
		if (CollectionUtils.isNotEmpty(cartEntries))
		{
			hasAnySubscription = cartEntries.stream()
					.map(AbstractOrderEntryModel::getProduct)
					.anyMatch(product -> product instanceof SubscriptionProductModel);
		}

		return hasAnySubscription;
	}

	@Override
	@Transactional
	public void updatePaymentInfo(final WileySubscriptionModel subscription, final PaymentInfoModel newPaymentInfo)
	{
		final PaymentInfoModel clonedPaymentInfo = modelService.clone(newPaymentInfo);

		clonedPaymentInfo.setOwner(subscription);
		clonedPaymentInfo.setDuplicate(true);
		clonedPaymentInfo.setOriginal(newPaymentInfo);

		try
		{
			esbB2CSubscriptionGateway.changePaymentMethod(clonedPaymentInfo, subscription.getExternalCode());

			final PaymentInfoModel previousPinfo = subscription.getPaymentInfo();
			subscription.setPaymentInfo(clonedPaymentInfo);

			modelService.saveAll(subscription, clonedPaymentInfo);
			if (previousPinfo != null && previousPinfo.getDuplicate())
			{
				modelService.remove(previousPinfo);
			}
		}
		catch (final RuntimeException ex)
		{
			final String message = String.format("Subscription %s cannot be updated with new PaymentInfo",
					subscription.getCode());
			LOG.error(message, ex);

			modelService.detach(clonedPaymentInfo);

			throw ex;
		}
	}

	@Override
	public SearchPageData<WileySubscriptionPaymentTransactionModel>
	getSubscriptionBillingActivities(final String subscriptionCode, final PageableData pageableData)
	{
		final UserModel currentUser = userService.getCurrentUser();
		return subscriptionDao.getSubscriptionBillingActivities(subscriptionCode, currentUser, pageableData);
	}

	@Override
	public List<WileySubscriptionModel> getInternalCodesByExternalCode(@Nonnull final String externalCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("externalCode", externalCode);
		return subscriptionDao.getInternalCodesByExternalCode(externalCode);
	}

	boolean isRequiredRenewal(final WileySubscriptionModel wileySubscriptionModel)
	{
		// this business logic can be reused in the future.
		return Boolean.TRUE.equals(wileySubscriptionModel.getRenewalEnabled()) && Boolean.TRUE.equals(
				wileySubscriptionModel.getRequireRenewal());
	}

	private List<WileySubscriptionModel> getActiveSubscriptionsExpiredOnDay(final ZonedDateTime expirationDay)
	{
		final ZonedDateTime startOfCurrentDay = expirationDay.truncatedTo(ChronoUnit.DAYS); // start of the current expirationDay
		final ZonedDateTime startOfNextDay = startOfCurrentDay.plusDays(1); // start of the next expirationDay
		// startOfNextDay is not included time according to an interface of SubscriptionDao.
		return subscriptionDao.findSubscriptionsWithExpirationDateBetween(SubscriptionStatus.ACTIVE, startOfCurrentDay,
				startOfNextDay);
	}

	@Override
	public boolean isProductSubscription(final WileyProductModel product)
	{
		validateParameterNotNull(product, "product must not be null!");
		return SUBSCRIPTION.equals(product.getSubtype());
	}

	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	@Override
	public boolean isSubscriptionCartEntry(final AbstractOrderEntryModel orderEntry)
	{
		Preconditions.checkNotNull(orderEntry, "OrderEntry should be not null");
		return orderEntry.getSubscriptionTerm() != null
				&& isProductSubscription((WileyProductModel) orderEntry.getProduct());
	}
}
