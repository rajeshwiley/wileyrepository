package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;


public class Wileyb2cQueryParametersListener implements FacetSearchListener
{
	private static final String EDISMAX_QUERY_TYPE = "edismax";
	private static final String PARAM_MIMIMUM_SHOULD_MATCH = "mm";
	private static final String PARAM_SPELLCHECK_ACCURACY = "spellcheck.accuracy";
	private static final String PARAM_BOOST_FUNCTION = "bf";
	private static final String PARAM_QUERY_FIELDS = "qf";
	private static final String PARAM_QUERY_PARSER_TYPE = "defType";
	private static final String BOOST_SEPARATOR = "^";
	private static final Pattern FIELD_IN_FUNCTION_EXTRACT_PATTERN = Pattern.compile("(\\{.*\\})");

	@Autowired
	private FieldNameTranslator fieldNameTranslator;

	@Override
	public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{
		IndexedType indexedType = facetSearchContext.getIndexedType();
		String minimumShouldMatch = indexedType.getMinimumShouldMatch();
		SearchQuery searchQuery = facetSearchContext.getSearchQuery();

		if (StringUtils.isNotBlank(minimumShouldMatch))
		{
			searchQuery.addRawParam(PARAM_MIMIMUM_SHOULD_MATCH, minimumShouldMatch);
		}

		Double spellcheckAccuracy = indexedType.getSpellcheckAccuracy();
		if (spellcheckAccuracy != null)
		{
			searchQuery.addRawParam(PARAM_SPELLCHECK_ACCURACY, spellcheckAccuracy.toString());
		}
		String boostFunction = indexedType.getBoostFunction();

		if (StringUtils.isNotBlank(boostFunction))
		{
			searchQuery.addRawParam(PARAM_BOOST_FUNCTION, translateFunctionField(searchQuery, boostFunction));
		}

		if (ArrayUtils.isEmpty(searchQuery.getRawParams().get(PARAM_QUERY_FIELDS)))
		{
			searchQuery.addRawParam(PARAM_QUERY_FIELDS, getQueryFields(searchQuery));
		}
		searchQuery.addRawParam(PARAM_QUERY_PARSER_TYPE, EDISMAX_QUERY_TYPE);
	}

	private String getQueryFields(final SearchQuery searchQuery)
	{
		List<String> fields = searchQuery.getFreeTextPhraseQueries().stream()
				.map(field -> fieldNameTranslator.translate(searchQuery, field.getField(),
						FieldNameProvider.FieldType.INDEX) + BOOST_SEPARATOR + field.getBoost())
				.collect(Collectors.toList());
		fields.add("_query_");
		return StringUtils.join(fields, " ");
	}

	private String translateFunctionField(final SearchQuery searchQuery, final String boostFunction)
	{
		try (Scanner scanner = new Scanner(boostFunction))
		{
			String fieldName = scanner.findInLine(FIELD_IN_FUNCTION_EXTRACT_PATTERN);
			String translatedFieldName = null;

			if (StringUtils.isNotBlank(fieldName) && fieldName.length() > 2)
			{
				String extractedFieldName = fieldName.substring(1, fieldName.length() - 1);
				translatedFieldName = fieldNameTranslator.translate(searchQuery, extractedFieldName,
						FieldNameProvider.FieldType.INDEX);
			}

			if (StringUtils.isNotBlank(translatedFieldName))
			{
				return boostFunction.replace(fieldName, translatedFieldName);
			}
			else
			{
				throw new IllegalArgumentException("Unable to find and translate field name in boost function "
						+ boostFunction);
			}
		}
	}

	@Override
	public void afterSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}

	@Override
	public void afterSearchError(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}
}
