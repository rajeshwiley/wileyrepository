package com.wiley.core.wiley.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.Optional;

import javax.annotation.Resource;


import org.joda.time.LocalDate;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 * Replaces Hybris OOTB Frontend_ProductOfflineDate flexible search restriction
 * to be able to disable it for pin workflow.
 */
public class WileyRestrictProductOfflineDateStrategy implements WileyRestrictProductStrategy
{
	@Resource
	private ProductService productService;

	@Override
	public boolean isRestricted(final ProductModel product)
	{


		LocalDate today = LocalDate.now();
		Optional<LocalDate> productOnlineDate = Optional.ofNullable(
				product.getOnlineDate() != null ? LocalDate.fromDateFields(product.getOnlineDate()) : null);
		Optional<LocalDate> productOfflineDate = Optional.ofNullable(
				product.getOfflineDate() != null ? LocalDate.fromDateFields(product.getOfflineDate()) : null);
		// Corresponds to Hybris OOTB Frontend_ProductOfflineDate restriction
		//( {onlineDate} IS NULL OR {onlineDate} <= ?session.user.currentDate)
		// AND ( {offlineDate} IS NULL OR {offlineDate} >= ?session.user.currentDate)
		boolean available = ((!productOnlineDate.isPresent() || productOnlineDate.get().isBefore(today)
				|| productOnlineDate.get().isEqual(today))
				&& (!productOfflineDate.isPresent() || productOfflineDate.get().isEqual(today)
				|| productOfflineDate.get().isAfter(today)));
		return !available;
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}


	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available because of Offline/Online date",
						parameter.getProduct().getCode()), null);
	}
}
