package com.wiley.core.wiley.process.strategies.impl;

import com.wiley.core.model.cartprocessing.CartProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import org.apache.log4j.Logger;

import com.wiley.core.wiley.process.strategies.WileyProcessContextStoreResolutionStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyProcessContextStoreResolutionStrategyImpl
		implements WileyProcessContextStoreResolutionStrategy
{
	private static final Logger LOG = Logger.getLogger(WileyProcessContextStoreResolutionStrategyImpl.class);

	/**
	 * Resolves store to be used for the given business process. Implementation is based on
	 * DefaultProcessContextResolutionStrategy#getCmsSite
	 *
	 * @param businessProcess
	 * 		the business process
	 * @return the base store
	 */
	@Override
	public BaseStoreModel getBaseStore(final BusinessProcessModel businessProcess)
	{
		ServicesUtil.validateParameterNotNull(businessProcess, "businessProcess must not be null");

		if (businessProcess instanceof StoreFrontProcessModel)
		{
			return ((StoreFrontProcessModel) businessProcess).getStore();
		}

		if (businessProcess instanceof OrderProcessModel)
		{
			return ((OrderProcessModel) businessProcess).getOrder().getStore();
		}

		if (businessProcess instanceof ConsignmentProcessModel)
		{
			return ((ConsignmentProcessModel) businessProcess).getConsignment().getOrder().getStore();
		}

		if (businessProcess instanceof CartProcessModel)
		{
			return ((CartProcessModel) businessProcess).getCart().getStore();
		}
		if (businessProcess instanceof ReturnProcessModel)
		{
			return ((ReturnProcessModel) businessProcess).getReturnRequest().getOrder().getStore();
		}

		LOG.info("Unsupported BusinessProcess type [" + businessProcess.getClass().getSimpleName() + "] for item ["
				+ businessProcess + "]");
		return null;
	}
}
