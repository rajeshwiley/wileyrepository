package com.wiley.core.integration.cdm.service.impl;


import com.wiley.core.integration.cdm.CDMGateway;
import com.wiley.core.integration.cdm.dto.CDMCreateUserResponse;
import com.wiley.core.integration.cdm.service.WileyCdmCustomerService;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import javax.annotation.Nonnull;


public class WileyCdmCustomerServiceImpl implements WileyCdmCustomerService
{
	private final CDMGateway cdmGateway;

	@Autowired
	public WileyCdmCustomerServiceImpl(final CDMGateway cdmGateway)
	{
		this.cdmGateway = cdmGateway;
	}


	@Override
	public CDMCreateUserResponse createCDMCustomer(@Nonnull final CustomerModel customer, @Nonnull final BaseSiteModel site)
	{
		Assert.notNull(customer);

		return cdmGateway.createCDMCustomer(customer, site);
	}
}
