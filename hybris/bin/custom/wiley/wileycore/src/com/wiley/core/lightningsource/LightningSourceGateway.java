package com.wiley.core.lightningsource;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


/**
 * Created by Georgii_Gavrysh on 9/6/2016.
 */
public interface LightningSourceGateway
{
	String DEFAULT_COUNTRY_ISOCODE = "default-country-isocode";

	/**
	 * get a link for download from external lightning source
	 * @param abstractOrderEntryModel
	 * @return
	 */
	@Nonnull String getLink(@Header(DEFAULT_COUNTRY_ISOCODE) String defaultCountryIsocode,
			@Nonnull @Payload AbstractOrderEntryModel abstractOrderEntryModel);
}
