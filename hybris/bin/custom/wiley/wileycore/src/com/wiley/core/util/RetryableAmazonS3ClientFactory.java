package com.wiley.core.util;

import de.hybris.platform.amazon.media.services.impl.DefaultS3StorageServiceFactory;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.amazonaws.services.s3.AmazonS3;


public class RetryableAmazonS3ClientFactory extends DefaultS3StorageServiceFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(RetryableAmazonS3ClientFactory.class);
	private static final String RETRY_PROPERTY_FLAG_NAME = "s3MediaStorageStrategy.objectMetaData.retry";

	private int attempts = 1;
	private List<String> involvedMethods;

	private ConfigurationService configurationService;

	@Override
	public AmazonS3 getS3Service(final String accessKey, final String secretAccessKey, final String endPoint)
	{
		boolean isRetry = configurationService.getConfiguration().getBoolean(RETRY_PROPERTY_FLAG_NAME, false);
		AmazonS3 s3service;
		if (isRetry)
		{
			s3service = (AmazonS3) Proxy.newProxyInstance(
					AmazonS3.class.getClassLoader(),
					new Class[] { AmazonS3.class },
					new RetryableAmazonS3Proxy(createAmazonS3(accessKey, secretAccessKey, endPoint), attempts, involvedMethods)
			);
		}
		else
		{
			s3service = createAmazonS3(accessKey, secretAccessKey, endPoint);
		}

		return s3service;
	}

	AmazonS3 createAmazonS3(final String accessKey, final String secretAccessKey, final String endPoint)
	{
		return super.getS3Service(accessKey, secretAccessKey, endPoint);
	}


	private static class RetryableAmazonS3Proxy implements InvocationHandler
	{
		private Object ootbAmazon;
		private int attempts;
		private List<String> involvedMethods;

		RetryableAmazonS3Proxy(final Object o, final int attempts, final List<String> involvedMethods)
		{
			ootbAmazon = o;
			this.attempts = attempts;
			this.involvedMethods = involvedMethods;
		}

		public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable
		{
			try
			{
				if (isMethodInvolved(method))
				{
					LOG.debug("Method {} called with retry", method);
					return invokeWithRetry(method, args);
				}
				else
				{
					return method.invoke(ootbAmazon, args);
				}
			}
			catch (Exception e)
			{
				LOG.error("Invocation Exception with arguments:" + Arrays.toString(args), e);
				throw e;
			}
		}

		private Object invokeWithRetry(final Method method, final Object[] args)
				throws Throwable
		{
			Throwable throwable = null;
			for (int attempt = 0; attempt < attempts; attempt++)
			{
				try
				{
					if (attempt > 0)
					{
						LOG.debug("Attempt {} to retry method {} ", attempt, method.getName());
					}
					return method.invoke(ootbAmazon, args);
				}
				catch (Exception e)
				{
					LOG.debug("RETRYABLE: ", e);
					throwable = e;
				}
			}
			throw throwable != null ? throwable : new IllegalStateException("Method " + method.getName()
					+ " was not invoked and no exception occured");
		}

		private boolean isMethodInvolved(final Method method)
		{
			String signature = method.toString();
			String shrinkMethodSingature = signature.substring(signature.indexOf(method.getName()));
			return involvedMethods.stream().anyMatch(shrinkMethodSingature::startsWith);
		}
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public void setAttempts(final int attempts)
	{
		this.attempts = attempts;
	}

	@Required
	public void setInvolvedMethods(final List<String> involvedMethods)
	{
		this.involvedMethods = involvedMethods;
	}
}