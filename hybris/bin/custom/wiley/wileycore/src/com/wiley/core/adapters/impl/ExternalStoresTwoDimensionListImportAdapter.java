package com.wiley.core.adapters.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.adapters.AbstractLinksTwoDimensionListImportAdapter;
import com.wiley.core.model.WileyWebLinkModel;


/**
 * Created by Aliaksei_Zhvaleuski on 29.03.2017.
 */
public class ExternalStoresTwoDimensionListImportAdapter extends AbstractLinksTwoDimensionListImportAdapter
{
	@Autowired
	private ModelService modelService;

	@Override
	public void saveModels(final ProductModel product, final List<WileyWebLinkModel> webLinks)
	{
		product.setExternalStores(webLinks);
		modelService.save(product);
	}
}
