package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;


/**
 * @author Maksim_Kozich
 */
public class Wileyb2cFacetSearchConfigPopulator implements Populator<SolrFacetSearchConfigModel, FacetSearchConfig>
{
	@Override
	public void populate(final SolrFacetSearchConfigModel solrFacetSearchConfigModel, final FacetSearchConfig facetSearchConfig)
			throws ConversionException
	{
		facetSearchConfig.setFacetLimit(solrFacetSearchConfigModel.getFacetLimit());
		facetSearchConfig.setFacetViewMoreLimit(solrFacetSearchConfigModel.getFacetViewMoreLimit());
	}
}
