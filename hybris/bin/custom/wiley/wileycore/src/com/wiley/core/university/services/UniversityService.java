package com.wiley.core.university.services;


import de.hybris.platform.core.model.c2l.RegionModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.model.UniversityModel;


/**
 * Service for working with UniverityModel that is used in student verification checkout flow.
 */
public interface UniversityService
{
	/**
	 * Retrieves universityModel by code.
	 *
	 * @param universityCode
	 * @return
	 */
	Optional<UniversityModel> getUniversityByCode(@Nonnull String universityCode);


	/**
	 * Retrieves list of active universities for region
	 *
	 * @param region
	 * @return
	 */
	@Nonnull
	List<UniversityModel> getUniversitiesByRegion(@Nonnull RegionModel region);
}
