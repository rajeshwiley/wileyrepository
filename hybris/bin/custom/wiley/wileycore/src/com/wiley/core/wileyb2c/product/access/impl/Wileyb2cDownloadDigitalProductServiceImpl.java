package com.wiley.core.wileyb2c.product.access.impl;

import de.hybris.platform.core.model.order.OrderEntryModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductService;
import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductStrategy;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cDownloadDigitalProductServiceImpl implements Wileyb2cDownloadDigitalProductService
{
	@Autowired
	private List<Wileyb2cDownloadDigitalProductStrategy> wileycomDownloadDigitalProductStrategies;

	@Override
	public String generateRedirectUrl(@Nonnull final OrderEntryModel orderEntry) throws Exception
	{
		for (Wileyb2cDownloadDigitalProductStrategy downloadDigitalProductStrategy : wileycomDownloadDigitalProductStrategies)
		{
			if (downloadDigitalProductStrategy.apply(orderEntry))
			{
				return downloadDigitalProductStrategy.generateRedirectUrl(orderEntry);
			}
		}
		throw new IllegalStateException(String.format("Could resolve strategy for order entry  with entry number %s "
				+ "and order code %s", orderEntry.getEntryNumber(), orderEntry.getOrder().getCode()));
	}

}
