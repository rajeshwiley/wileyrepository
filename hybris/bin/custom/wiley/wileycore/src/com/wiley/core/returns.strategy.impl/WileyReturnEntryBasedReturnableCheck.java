package com.wiley.core.returns.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.impl.DefaultReturnEntryBasedReturnableCheck;

public class WileyReturnEntryBasedReturnableCheck extends DefaultReturnEntryBasedReturnableCheck
{

	@Override
	public boolean perform(final OrderModel order, final AbstractOrderEntryModel orderEntry, final long returnQuantity)
	{
		boolean isReturnable = false;
		if (returnQuantity >= 1L && orderEntry.getQuantity() >= returnQuantity)
		{
			isReturnable = true;
		}
		return isReturnable;
	}
}
