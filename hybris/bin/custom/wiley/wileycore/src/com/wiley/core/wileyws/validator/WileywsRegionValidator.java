package com.wiley.core.wileyws.validator;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.wileyws.exceptions.WileyWsValidationException;


public class WileywsRegionValidator extends WileywsRelativePathValidator
{
	private static final Logger LOG = Logger.getLogger(WileywsRegionValidator.class);

	private static final String TYPE = "UnknownRegionError";
	private static final String FIELD_REGION_NAME = "state";
	private static final String FIELD_COUNTRY_NAME = "country";

	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	@Override
	public boolean supports(final Class<?> clazz)
	{
		return clazz != null;
	}

	@Override
	protected void validatePathObject(final Object target, final Errors errors)
	{
		final String regionIso = (String) errors.getFieldValue(getRegionPathToTheField());
		final String countryIso = (String) errors.getFieldValue(getCountryPathToTheField());

		if (StringUtils.isEmpty(countryIso) || StringUtils.isEmpty(regionIso))
		{
			return;
		}

		try
		{
			CountryModel countryModel = commonI18NService.getCountry(countryIso);
			wileyCommonI18NService.getRegionForShortCode(countryModel, regionIso);
		}
		catch (IllegalArgumentException | UnknownIdentifierException e)
		{
			LOG.error(e.getMessage(), e);
			throw new WileyWsValidationException(e.getMessage(), getRegionPathToTheField(), TYPE);
		}
	}

	private String getRegionPathToTheField()
	{
		return StringUtils.isBlank(getPath()) ? FIELD_REGION_NAME : getPath() + "." + FIELD_REGION_NAME;
	}

	private String getCountryPathToTheField()
	{
		return StringUtils.isBlank(getPath()) ? FIELD_COUNTRY_NAME : getPath() + "." + FIELD_COUNTRY_NAME;
	}
}
