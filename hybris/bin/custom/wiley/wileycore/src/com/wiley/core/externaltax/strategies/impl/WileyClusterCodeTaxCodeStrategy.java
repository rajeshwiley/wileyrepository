package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Optional;

import com.wiley.core.externaltax.strategies.WileyTaxCodeStrategy;


public class WileyClusterCodeTaxCodeStrategy implements WileyTaxCodeStrategy 
{
	@Override
	public Optional<String> getTaxCode(final AbstractOrderEntryModel entry)
	{
		return Optional.ofNullable(entry.getProduct().getIsbn());
	}
}
