package com.wiley.core.wileyb2c.jalo;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.jalo.SubscriptionTerm;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialService;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


/**
 * Helper class that groups utility and helper methods for  for B2C price factory
 */
public class WileyB2CPriceFactorySubscriptionHelper
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyB2CPriceFactorySubscriptionHelper.class);

	private final Wileyb2cFreeTrialService wileyb2cFreeTrialService;

	private final ModelService modelService;

	private final Wileyb2cSubscriptionService wileyb2cSubscriptionService;

	@Autowired
	public WileyB2CPriceFactorySubscriptionHelper(
			final Wileyb2cFreeTrialService wileyB2CFreeTrialService,
			final ModelService modelService, final Wileyb2cSubscriptionService wileyb2cSubscriptionService)
	{
		this.wileyb2cFreeTrialService = wileyB2CFreeTrialService;
		this.modelService = modelService;
		this.wileyb2cSubscriptionService = wileyb2cSubscriptionService;
	}

	SubscriptionTerm getSubscriptionTerm(final PriceRow priceRow) {
		try
		{
			return (SubscriptionTerm) priceRow.getAttribute(PriceRowModel.SUBSCRIPTIONTERM);

		}
		catch (JaloSecurityException e)
		{
			LOG.error("Unable to obtain " + PriceRowModel.SUBSCRIPTIONTERM, e + "for price row");
			return null;
		}
	}

	SubscriptionTerm getSubscriptionTerm(final AbstractOrderEntry orderEntry, final SessionContext ctx) {
		try
		{
			return (SubscriptionTerm) orderEntry.getAttribute(ctx, OrderEntryModel.SUBSCRIPTIONTERM);

		}
		catch (JaloSecurityException e)
		{
			LOG.error("Unable to obtain " + PriceRowModel.SUBSCRIPTIONTERM, e + "for orderEntry");
			return null;
		}
	}

	boolean isSubscriptionProduct(final Product product) {
		Preconditions.checkNotNull(product);
		ProductModel productModel = modelService.toModelLayer(product);
		return productModel instanceof WileyProductModel && wileyb2cSubscriptionService.isProductSubscription(
				(WileyProductModel) productModel);
	}

	boolean isFreeTrialSubscriptionEntry(final AbstractOrderEntry orderEntry) {
		AbstractOrderEntryModel entryModel = modelService.toModelLayer(orderEntry);
		Preconditions.checkArgument(entryModel != null, "OrderEntry should not be null");
		Preconditions.checkArgument(entryModel.getProduct() != null
				&& entryModel.getProduct() instanceof WileyProductModel,
				"OrderEntry should be associated with valid WileyProduct");
		return wileyb2cFreeTrialService.isFreeTrialEntry(entryModel);
	}

	boolean isPriceRowContainsSubscriptionTerm(final SubscriptionTerm subscriptionTerm, final PriceRow row)
	{
		SubscriptionTerm termForRow = getSubscriptionTerm(row);
		return termForRow != null && StringUtils.equals(subscriptionTerm.getId(), termForRow.getId());
	}

	boolean isPriceRowAllowedForSubscriptionProduct(final Product product, final PriceRow priceRow) {
		WileyProductModel productModel = modelService.toModelLayer(product);
		PriceRowModel rowModel = modelService.toModelLayer(priceRow);
		SubscriptionTermModel priceRowSubscriptionTerm = rowModel.getSubscriptionTerm();
		return wileyb2cSubscriptionService.isValidSubscriptionTermForProduct(productModel, priceRowSubscriptionTerm);
	}
}
