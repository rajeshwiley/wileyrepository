package com.wiley.core.pages;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.Collection;
import java.util.List;

import com.wiley.core.model.WileyPartnerPageModel;


public interface WileyCMSPageService
{

	WileyPartnerPageModel getPartnerPageByCategoryId(String categoryCode);

	List<AbstractPageModel> findPagesByComponent(AbstractCMSComponentModel componentModel);

	Collection<AbstractPageModel> findAllPagesByCategoryCode(String typeCode,
			Collection<CatalogVersionModel> catalogVersions, String categoryCode);
}
