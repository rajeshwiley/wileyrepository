package com.wiley.core.wiley.order.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.List;

import com.wiley.core.cart.WileyCommerceCartDao;
import com.wiley.core.enums.OrderType;
import com.wiley.core.wiley.order.WileyCommerceCartService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyCommerceCartServiceImpl extends DefaultCommerceCartService implements WileyCommerceCartService
{
	private WileyCommerceCartDao wileyCommerceCartDao;

	@Override
	public CartModel getLatestGeneralCartForSiteUser(final BaseSiteModel site, final UserModel user)
	{
		validateParameterNotNull(site, "site cannot be null");
		validateParameterNotNull(user, "user cannot be null");

		final List<CartModel> cartsForStoreAndUserAndType = wileyCommerceCartDao.findAllCartsForStoreAndUserAndType(site, user,
				OrderType.GENERAL);

		return cartsForStoreAndUserAndType.stream().findFirst().orElse(null);
	}

	@Override
	public List<CartModel> getAllCartsForUserAndType(final BaseSiteModel site,
			final UserModel user, final OrderType type)
	{
		validateParameterNotNull(site, "site cannot be null");
		validateParameterNotNull(user, "user cannot be null");
		if (type != null)
		{
			return wileyCommerceCartDao.findAllCartsForStoreAndUserAndType(site, user, type);
		}
		else
		{
			return wileyCommerceCartDao.findAllCartsForUserAndSite(site, user);
		}
	}

	@Override
	public void removeCart(final CartModel cart)
	{
		getModelService().remove(cart);
	}

	/**
	 * OOTB validateCart from {@link DefaultCommerceCartService}.
	 * Modification: calculateCart method was previously executed every time,
	 * now is executed now only in case of error cart modifications.
	 */
	@Override
	public List<CommerceCartModification> validateCart(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameter.getCart();
		validateParameterNotNull(cartModel, "Cart model cannot be null");

		final List<CommerceCartModification> modifications = getCartValidationStrategy().validateCart(parameter);

		// We only care about modifications that weren't successful
		final List<CommerceCartModification> errorModifications = new ArrayList<CommerceCartModification>(modifications.size());
		for (final CommerceCartModification modification : modifications)
		{
			if (!CommerceCartModificationStatus.SUCCESS.equals(modification.getStatusCode()))
			{
				errorModifications.add(modification);
			}
		}

		if (!errorModifications.isEmpty())
		{
			calculateCart(parameter);
		}

		return errorModifications;
	}

	@Override
	public void setCommerceCartDao(final CommerceCartDao commerceCartDao)
	{
		super.setCommerceCartDao(commerceCartDao);
		wileyCommerceCartDao = (WileyCommerceCartDao) commerceCartDao;
	}
}
