package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.converters.Converters;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


public class Wileyb2cHomePageSiteMapGenerator extends AbstractWileyb2cSiteMapGenerator<ContentPageModel>
{
	private CMSPageService cmsPageService;

	/**
	 * OOTB_CODE from HomePageSiteMapGenerator
	 */
	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<ContentPageModel> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	/**
	 * OOTB_CODE from HomePageSiteMapGenerator
	 */
	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final ContentPageModel homepage = cmsPageService.getHomepage();
		return homepage != null ? Collections.singletonList(homepage) : Collections.emptyList();
	}

	@Required
	public void setCmsPageService(final CMSPageService cmsPageService)
	{
		this.cmsPageService = cmsPageService;
	}
}
