package com.wiley.core.healthcheck.checks;

import com.codahale.metrics.health.HealthCheck;


public class ProductPricesHealthCheck extends HealthCheck
{
	@Override
	protected HealthCheck.Result check() throws Exception
	{
		return HealthCheck.Result.healthy("dummy check");
	}
}