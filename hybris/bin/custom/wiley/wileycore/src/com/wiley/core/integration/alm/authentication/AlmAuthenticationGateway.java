package com.wiley.core.integration.alm.authentication;

import javax.annotation.Nonnull;

import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;
import com.wiley.core.integration.alm.authentication.service.AlmAuthenticationService;



/**
 * Don't use gateway directly. Use {@link AlmAuthenticationService} instead of it
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface AlmAuthenticationGateway
{
	@Nonnull
	AlmAuthenticationResponseDto validateSessionToken(@Nonnull String sessionToken);
}
