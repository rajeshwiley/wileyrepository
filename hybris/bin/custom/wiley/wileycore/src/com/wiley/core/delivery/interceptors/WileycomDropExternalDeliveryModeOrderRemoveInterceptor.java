package com.wiley.core.delivery.interceptors;

import de.hybris.platform.order.interceptors.DefaultAbstractOrderRemoveInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;


/**
 * Delete External Delivery Mode during Abstract Order removing.
 *
 * Created by Uladzimir_Barouski on 6/14/2016.
 */
public class WileycomDropExternalDeliveryModeOrderRemoveInterceptor extends DefaultAbstractOrderRemoveInterceptor
{
	@Override
	public void onRemove(final Object model, final InterceptorContext interceptorContext) throws InterceptorException
	{
//		if (model instanceof AbstractOrderModel)
//		{
//			DeliveryModeModel deliveryModeModel = ((AbstractOrderModel) model).getDeliveryMode();
//			if (deliveryModeModel instanceof ExternalDeliveryModeModel && deliveryModeModel != null)
//			{
//				interceptorContext.getModelService().remove(deliveryModeModel);
//			}
//		}
//		super.onRemove(model, interceptorContext);
	}
}
