package com.wiley.core.order;

import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


/**
 * Manager contains logic for controlling products quiantity in cart.
 * Thus, according to business logic, some products can have only single instance in cart.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyProductQuantityInCartManager extends CommerceAddToCartStrategy, CommerceUpdateCartEntryStrategy
{


	/**
	 * @param parameter
	 * 		the parameter
	 * @param enableEditionFormatChecks
	 * 		enable checks of product's edition format.
	 * @return the commerce cart modification
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 * @see CommerceAddToCartStrategy#addToCart(CommerceCartParameter)
	 */
	CommerceCartModification addToCart(CommerceCartParameter parameter, boolean enableEditionFormatChecks)
			throws CommerceCartModificationException;

	/**
	 * Set quantity to max allowed for cart entries with digital products if quantity greater then maximum allowed value.
	 *
	 * @param cart
	 * 		the cart
	 */
	void checkAndUpdateQuantityIfRequired(CartModel cart, List<CommerceCartModification> modifications);

}
