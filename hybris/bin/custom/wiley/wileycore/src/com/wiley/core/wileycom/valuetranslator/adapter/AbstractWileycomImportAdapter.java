package com.wiley.core.wileycom.valuetranslator.adapter;


public abstract class AbstractWileycomImportAdapter implements WileycomImportAdapter
{
	private String catalogId;
	private String catalogVersion;

	@Override
	public void setCatalog(final String catalogVersion, final String catalogId)
	{
		this.catalogVersion = catalogVersion;
		this.catalogId = catalogId;
	}

	public String getCatalogId()
	{
		return catalogId;
	}

	public String getCatalogVersion()
	{
		return catalogVersion;
	}

}