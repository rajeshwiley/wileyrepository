package com.wiley.core.returns;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.ReturnService;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;


public interface WileyReturnService extends ReturnService
{
	/**
	 * Set refund amount for return entry
	 * @param returnRequest - Return request, should NOT be null, else throws NullPointerException
	 * @param refundAmount - Refunded amount
	 */
	void setRefundAmountToReturnRequest(ReturnRequestModel returnRequest, BigDecimal refundAmount);

	void postRefundProcess(PaymentTransactionEntryModel paymentTransactionEntry, ReturnRequestModel request);

	ReturnRequestModel createReturnRequestWithAmount(OrderModel orderModel, BigDecimal refundAmount);
}
