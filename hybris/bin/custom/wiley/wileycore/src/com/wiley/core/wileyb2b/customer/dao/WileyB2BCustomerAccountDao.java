package com.wiley.core.wileyb2b.customer.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.customer.dao.CustomerAccountDao;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;

public interface WileyB2BCustomerAccountDao extends CustomerAccountDao
{
	/**
	 * Finds orders for the specified B2B Unit in the current session's active catalog versions
	 *
	 * @param b2bUnit the B2B unit
	 * @param store   The current store
	 * @param status required statuses
	 * @param pageableData
	 * @return The list of orders placed by any customer assigned to given B2BUnit
	 */
	SearchPageData<OrderModel> findOrdersByUnitAndStore(B2BUnitModel b2bUnit, BaseStoreModel store,
														OrderStatus[] status, PageableData pageableData);
}
