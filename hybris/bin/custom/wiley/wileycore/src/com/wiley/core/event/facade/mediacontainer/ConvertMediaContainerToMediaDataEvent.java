package com.wiley.core.event.facade.mediacontainer;

import com.wiley.core.event.facade.WileyTwoParametersFacadeEvent;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class ConvertMediaContainerToMediaDataEvent
        extends WileyTwoParametersFacadeEvent<CMSSiteModel, MediaContainerModel, String> {
    public ConvertMediaContainerToMediaDataEvent(final CMSSiteModel firstParameter, final MediaContainerModel secondParameter) {
        super(firstParameter, secondParameter);
    }
}
