package com.wiley.core.email.actions;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.email.service.WileyDefaultEmailService;
import com.wiley.core.model.cartprocessing.CartProcessModel;


/**
 * A process action to send emails.
 */
public class WileySendEmailAction extends AbstractSimpleDecisionAction {

    private WileyDefaultEmailService wileyEmailService;

    private static final Logger LOGGER = LogManager.getLogger(WileySendEmailAction.class);

    @Resource
    private EmailService emailService;

    /**
     * @return the wileyEmailService
     */
    protected WileyDefaultEmailService getWileyEmailService() {
        return wileyEmailService;
    }


    /**
     * @param wileyEmailService
     *          the wileyEmailService to set
     */
    @Required
    public void setWileyEmailService(final WileyDefaultEmailService wileyEmailService) {
        this.wileyEmailService = wileyEmailService;
    }

    @Override
    public Transition executeAction(final BusinessProcessModel businessProcessModel)
            throws RetryLaterException {
        boolean transactionStaus = false;
        LOGGER.debug("Entering wiley action");
        if (businessProcessModel instanceof CartProcessModel) {
            for (final EmailMessageModel email : businessProcessModel.getEmails()) {
                transactionStaus = getWileyEmailService().send(email);
            }
        } else {
            for (final EmailMessageModel email : businessProcessModel.getEmails()) {
                transactionStaus = emailService.send(email);
            }
        }
        LOGGER.debug("Leaving wiley action");
        if (transactionStaus) {
            return Transition.OK;
        } else {
            return Transition.NOK;
        }
    }
}
