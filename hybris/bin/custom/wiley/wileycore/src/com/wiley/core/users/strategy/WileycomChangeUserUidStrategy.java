package com.wiley.core.users.strategy;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * Encapsulates logic of changing customers UID
 */
public interface WileycomChangeUserUidStrategy
{
	/**
	 * Changes User uid
	 * @param newUid - new uid for current logged customer
	 * @param currentPassword - current password
	 * @throws DuplicateUidException - if new uid already exists
	 * @throws PasswordMismatchException - if password is incorrect
	 */
	void changeUid(String newUid, String currentPassword)
			throws DuplicateUidException, PasswordMismatchException;

	void changeUid(CustomerModel currentUser, String newUid, String password) throws DuplicateUidException;
}
