package com.wiley.core.integration.edi;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface WileyEdiExportGateway
{
	/**
	 * Triggers execution of ediExportRequestChannel Spring Integration channel. This channel performs export of EDI xml
	 * to the Wiley FTP Server
	 *
	 * @param processes
	 * 		The list or export processes for EDI export.
	 * @param baseStore
	 * 		baseStore - reference to base store model. Edi export is performed separately for each base store.
	 * 		This parameter is used to create Routing xml element and fileName of generate xml.
	 * @param localDateTime - ???
	 * @param paymentType paymentType of orders that are currently exporting,
	 *                       this argument is required for generating proper proper details in export.(like exported file name)
	 */

	void exportOrders(
			@Nonnull List<WileyExportProcessModel> processes,
			@Nonnull BaseStoreModel baseStore,
			@Nonnull LocalDateTime localDateTime,
			@Nonnull String paymentType,
			@Nonnull String mailTo);

}
