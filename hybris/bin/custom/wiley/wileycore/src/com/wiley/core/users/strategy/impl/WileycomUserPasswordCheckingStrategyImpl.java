package com.wiley.core.users.strategy.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.PasswordCheckingStrategy;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.core.wileycom.users.service.WileycomUsersService;




/**
 * Created by Mikhail_Asadchy on 13.06.2016.
 */
public class WileycomUserPasswordCheckingStrategyImpl implements PasswordCheckingStrategy
{

	@Resource
	private ModelService modelService;

	@Resource
	private WileycomUsersService wileycomUsersService;

	@Override
	public boolean checkPassword(final User user, final String password)
	{
		final UserModel userModel = modelService.toModelLayer(user);

		return wileycomUsersService.authenticate(userModel, password);
	}

}
