package com.wiley.core.mpgs.command.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyVerifyCommand;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyVerifyResponse;


public class WileyMPGSVerifyCommandImpl implements WileyVerifyCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSVerifyCommandImpl.class);

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyVerifyResponse perform(final WileyVerifyRequest wileyVerifyRequest)
	{
		WileyVerifyResponse response;
		try
		{
			response = wileyMpgsPaymentGateway.verify(wileyVerifyRequest);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to verify card due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyVerifyResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to verify card due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyVerifyResponse::new);
		}
		return response;
	}
}
