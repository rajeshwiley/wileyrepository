package com.wiley.core.interceptors;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.model.WileyCountryStoreConfigurationModel;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyCountryStoreConfigurationValidateInterceptor implements ValidateInterceptor<WileyCountryStoreConfigurationModel>
{
	@Override
	public void onValidate(final WileyCountryStoreConfigurationModel wileyCountryStoreConfigurationModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{
		final CountryModel country = wileyCountryStoreConfigurationModel.getCountry();
		final List<RegionModel> subRegions = wileyCountryStoreConfigurationModel.getRegions();
		final Collection<RegionModel> regions = country.getRegions();
		if (CollectionUtils.isNotEmpty(subRegions) && !CollectionUtils.containsAll(regions, subRegions))
		{
			throw new InterceptorException("Country Store Configuration can contain only regions that exist in Country");
		}
	}
}
