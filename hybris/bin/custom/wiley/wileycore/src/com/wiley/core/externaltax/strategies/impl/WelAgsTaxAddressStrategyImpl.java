package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;


public class WelAgsTaxAddressStrategyImpl implements WileyTaxAddressStrategy
{
	@Override
	public AddressModel resolvePaymentAddress(final AbstractOrderModel abstractOrder)
	{
		return abstractOrder.getUser().getDefaultPaymentAddress();
	}

	@Override
	public AddressModel resolveDeliveryAddress(final AbstractOrderModel abstractOrder)
	{
		AddressModel address;
		if (abstractOrder.getDeliveryAddress() == null)
		{
			address = abstractOrder.getUser().getDefaultShipmentAddress();
		}
		else
		{
			address = abstractOrder.getDeliveryAddress();
		}
		return address;
	}
}
