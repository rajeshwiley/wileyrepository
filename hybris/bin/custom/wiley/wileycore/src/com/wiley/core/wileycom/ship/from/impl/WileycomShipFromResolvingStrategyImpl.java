package com.wiley.core.wileycom.ship.from.impl;

import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.wileycom.ship.from.WileycomShipFromResolvingStrategy;
import com.wiley.core.wileycom.stock.strategies.WileycomPrimaryStockLevelStrategy;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomShipFromResolvingStrategyImpl implements WileycomShipFromResolvingStrategy
{
	private static final Logger LOG = Logger.getLogger(WileycomShipFromResolvingStrategyImpl.class.getName());

	private WarehouseSelectionStrategy warehouseSelectionStrategy;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private StockService stockService;
	@Resource
	private WileycomPrimaryStockLevelStrategy wileycomPrimaryStockLevelStrategy;
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;



	@Override
	public AddressModel resolveShipFromAddress(@Nonnull final ProductModel product)
	{
		validateParameterNotNull(product, "product cannot be null");

		if (wileyProductEditionFormatService.isDigitalProduct(product))
		{
			Assert.notNull(product.getExternalCompany());
			return product.getExternalCompany().getAddress();
		}
		else
		{
			WarehouseModel warehouse = findWarehouse(product);
			if (warehouse.getAddress() == null)
			{
				String errorText = "Warehouse " + warehouse.getCode()
						+ " doesn't have address. Resolving of ship from address failed";
				LOG.error(errorText);
				throw new IllegalArgumentException(errorText);
			}
			return warehouse.getAddress();
		}
	}

	private WarehouseModel findWarehouse(final ProductModel product)
	{
		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		List<WarehouseModel> warehouses = warehouseSelectionStrategy.getWarehousesForBaseStore(currentBaseStore);

		if (!warehouses.isEmpty())
		{
			Collection<StockLevelModel> stockLevels = stockService.getStockLevels(product, warehouses);
			if (!stockLevels.isEmpty())
			{
				StockLevelModel primaryStockLevel = wileycomPrimaryStockLevelStrategy.getPrimaryStockLevel(stockLevels);
				return primaryStockLevel.getWarehouse();
			}
		}

		WarehouseModel defaultWarehouse = product.getDefaultWarehouse();
		if (defaultWarehouse == null)
		{
			throw new IllegalStateException(
					"Unable to resolve warehouse for product " + product.getCode() + " and base store " + currentBaseStore
							.getUid());
		}
		return defaultWarehouse;
	}

	public void setWarehouseSelectionStrategy(
			final WarehouseSelectionStrategy warehouseSelectionStrategy)
	{
		this.warehouseSelectionStrategy = warehouseSelectionStrategy;
	}
}
