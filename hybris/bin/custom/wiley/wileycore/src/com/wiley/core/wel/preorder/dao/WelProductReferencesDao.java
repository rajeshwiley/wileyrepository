package com.wiley.core.wel.preorder.dao;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


public interface WelProductReferencesDao
{
	List<ProductModel> findSourceReferenceProducts(ProductModel targetProduct,
			ProductReferenceTypeEnum referenceType);
}
