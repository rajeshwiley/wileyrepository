package com.wiley.core.payment.strategies;

public interface WileyTransactionIdGeneratorStrategy {
    String generateTransactionId();
}
