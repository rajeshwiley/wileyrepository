package com.wiley.core.exceptions;

/**
 * This exception can be thrown by external system if something went wrong.
 */
public class ExternalSystemException extends RuntimeException
{
	public ExternalSystemException() { super(); }

	public ExternalSystemException(final String message)
	{
		super(message);
	}

	public ExternalSystemException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemException(final Throwable cause)
	{
		super(cause);
	}
}
