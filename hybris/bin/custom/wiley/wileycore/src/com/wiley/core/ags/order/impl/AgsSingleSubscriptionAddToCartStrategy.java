package com.wiley.core.ags.order.impl;

import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.AbstractCommerceCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This strategy takes care that only one subscription product (with any quantity) can be added to cart.<br/>
 * Repeated adding to cart replaces the existing product with the new item. So, only the latest added item remains in the cart.
 *
 * @author Aliaksei_Zlobich
 */
public class AgsSingleSubscriptionAddToCartStrategy extends AbstractCommerceCartStrategy implements CommerceAddToCartStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(AgsSingleSubscriptionAddToCartStrategy.class);

	private CommerceAddToCartStrategy commerceAddToCartStrategy;



	@Override
	public CommerceCartModification addToCart(@Nonnull final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{

		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);

		final ProductModel productToAdd = parameter.getProduct();
		final CartModel cart = parameter.getCart();

		ServicesUtil.validateParameterNotNullStandardMessage("product", productToAdd);
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		LOG.debug("Method params: product [{}], cart [{}]", productToAdd.getCode(), cart.getCode());

		List<AbstractOrderEntryModel> subscriptionEntriesToRemove = null;

		// single promotion behavior should be applied only for mater cart.
		// See https://wiki.hybris.com/display/release5/subscriptionservices+Extension+-
		// +Technical+Guide#subscriptionservicesExtension-TechnicalGuide-MulticartImplementation
		if (isMasterCart(cart) && isSubscription(productToAdd))
		{
			// trying to find order entries with subscription products to remove them after adding new one.
			subscriptionEntriesToRemove = findSubscriptionOrderEntriesInCart(cart);
		}

		LOG.debug("Order entries to remove [{}].", subscriptionEntriesToRemove);

		LOG.debug("Adding product [{}] to cart [{}].", productToAdd.getCode(), cart.getCode());
		final CommerceCartModification modification = commerceAddToCartStrategy.addToCart(parameter);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("CommerceCartModification: status [{}], addedQuantity [{}].\n"
							+ "CommerceCartParameter: product [{}], cart [{}], quantity [{}]", modification.getStatusCode(),
					modification.getQuantityAdded(), parameter.getProduct(), parameter.getCart().getCode(),
					parameter.getQuantity());
		}

		// if product was added successfully and we need to remove order entries.
		if (isProductAddedSuccessfully(modification) && CollectionUtils.isNotEmpty(subscriptionEntriesToRemove))
		{
			LOG.debug("Removing order entries with old subscription products.");

			getModelService().removeAll(subscriptionEntriesToRemove);
			getModelService().refresh(cart);
			normalizeEntryNumbers(cart);
			getCommerceCartCalculationStrategy().calculateCart(parameter);
		}

		return modification;
	}

	@Override
	public List<CommerceCartModification> addToCart(final List<CommerceCartParameter> parameterList)
			throws CommerceCartMergingException
	{
		List<CommerceCartModification> modifications = new ArrayList<>();
		try
		{
			if (CollectionUtils.isNotEmpty(parameterList))
			{
				for (CommerceCartParameter commerceCartParameter : parameterList)
				{
					modifications.add(addToCart(commerceCartParameter));
				}
			}
		}
		catch (final CommerceCartModificationException e)
		{
			throw new CommerceCartMergingException(e.getMessage(), e);
		}
		return modifications;
	}

	private List<AbstractOrderEntryModel> findSubscriptionOrderEntriesInCart(final CartModel cart)
	{
		return cart.getEntries().stream()
				.filter(entry -> isSubscription(entry.getProduct()))
				.collect(Collectors.toList());
	}

	private boolean isSubscription(final ProductModel product)
	{
		return product instanceof SubscriptionProductModel;
	}

	private boolean isProductAddedSuccessfully(final CommerceCartModification modification)
	{
		return modification.getQuantityAdded() > 0;
	}

	private boolean isMasterCart(final CartModel cart)
	{
		return cart.getParent() == null;
	}

	public CommerceAddToCartStrategy getCommerceAddToCartStrategy()
	{
		return commerceAddToCartStrategy;
	}

	public void setCommerceAddToCartStrategy(
			final CommerceAddToCartStrategy commerceAddToCartStrategy)
	{
		this.commerceAddToCartStrategy = commerceAddToCartStrategy;
	}
}
