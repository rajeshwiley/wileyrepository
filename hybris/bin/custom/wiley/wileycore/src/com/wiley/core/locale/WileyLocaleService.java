package com.wiley.core.locale;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.util.List;


public interface WileyLocaleService
{
	String getDefaultEncodedLocale();
	String getCurrentEncodedLocale();
	String getEncodedLocale(CountryModel country, LanguageModel language);
	CountryModel getCountryForEncodedLocale(String encodedLocale);
	void setEncodedLocale(String encodedLocale);
	boolean isValidEncodedLocale(String encodedLocale);
	List<String> getAllEncodedLocales();
}
