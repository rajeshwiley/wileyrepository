package com.wiley.core.jobs.maintenance;

import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.ImmutableMap;
import com.wiley.core.model.CleanUpTaskLogsMaintenanceCronJobModel;


public class CleanupTaskLogsStrategy implements
		MaintenanceCleanupStrategy<ProcessTaskLogModel, CleanUpTaskLogsMaintenanceCronJobModel> {

	private static final Logger LOG = Logger.getLogger(CleanupTaskLogsStrategy.class);

	private static final String TASK_LOGS_QUERY = "SELECT {" + ProcessTaskLogModel.PK + "}"
			+ " FROM {" + ProcessTaskLogModel._TYPECODE + "}"
			+ " WHERE {" + ProcessTaskLogModel.RETURNCODE + "} in (?returnCodes) "
			+ " AND  {" + ProcessTaskLogModel.ENDDATE + "} <= ?daysOld";

	private ModelService modelService;

	@Override
	public FlexibleSearchQuery createFetchQuery(final CleanUpTaskLogsMaintenanceCronJobModel cjm) {
		FlexibleSearchQuery query = new FlexibleSearchQuery(
				TASK_LOGS_QUERY, ImmutableMap.of(
				"returnCodes", cjm.getReturnCodes(),
				"daysOld", LocalDate.now().minusDays(cjm.getDaysOld())
				)
			);
		query.setResultClassList(Arrays.asList(ProcessTaskLogModel.class));
		return query;
	}

	@Override
	public void process(final List<ProcessTaskLogModel> elements) {
		LOG.info("Removing " + elements.size() + " " + " log records now.");
		modelService.removeAll(elements);
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
