package com.wiley.core.servicelayer.dao.processdefinition;

import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;

import java.util.List;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyProcessDefinitionDao
{
	List<DynamicProcessDefinitionModel> findAllProcessDefinitions();

	List<DynamicProcessDefinitionModel> findProcessDefinitionsByCode(String code);
}
