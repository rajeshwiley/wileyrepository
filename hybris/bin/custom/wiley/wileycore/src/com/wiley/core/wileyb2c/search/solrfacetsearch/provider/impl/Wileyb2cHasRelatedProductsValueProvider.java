package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * Created by Raman_Hancharou on 6/13/2017.
 */
public class Wileyb2cHasRelatedProductsValueProvider extends AbstractWileycomValueProvider<Boolean>
{
	private ProductReferenceService productReferenceService;

	@Override
	protected List<Boolean> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			return Collections.singletonList(CollectionUtils.isNotEmpty(productReferenceService
					.getProductReferencesForSourceProduct((ProductModel) model, ProductReferenceTypeEnum.RELATED, Boolean.TRUE)));
		}
		else
		{
			return Collections.emptyList();
		}
	}

	@Required
	public void setProductReferenceService(
			final ProductReferenceService productReferenceService)
	{
		this.productReferenceService = productReferenceService;
	}
}
