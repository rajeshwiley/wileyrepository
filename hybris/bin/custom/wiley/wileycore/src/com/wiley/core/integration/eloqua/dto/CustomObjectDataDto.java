package com.wiley.core.integration.eloqua.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * Please see http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_customobjectdata_315e8ed5966b8bbee2a1fc5782e532.htm
 *
 * @author Aliaksei_Zlobich
 */
@XmlRootElement(name = "CustomObjectData")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomObjectDataDto
{

	private String type = "CustomObjectData";

	private Integer contactId;

	@XmlElement
	private List<FieldValueDto> fieldValues;

	private Integer id;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public Integer getContactId()
	{
		return contactId;
	}

	public void setContactId(final Integer contactId)
	{
		this.contactId = contactId;
	}

	public List<FieldValueDto> getFieldValues()
	{
		return fieldValues;
	}

	public void setFieldValues(final List<FieldValueDto> fieldValues)
	{
		this.fieldValues = fieldValues;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("type", type)
				.add("id", id)
				.add("contactId", contactId)
				.add("fieldValues", fieldValues)
				.toString();
	}
}
