package com.wiley.core.wileyb2c.weblinks.providers;

import com.wiley.core.model.WileyWebLinkModel;


/**
 * Provider responsible for obtaining actual url from given link object
 */
public interface WileyWebLinkUrlProvider
{
	/**
	 * Returns url that given links represent
	 * @param webLink link
	 * @return url
	 * @throws IllegalArgumentException if url cannot be determined
	 */
	String getWebLinkUrl(WileyWebLinkModel webLink);
}
