package com.wiley.core.wileyb2c.externaltax.services.impl;

import de.hybris.platform.commerceservices.externaltax.CalculateExternalTaxesStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.externaltax.ExternalTaxDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.sabrix.SabrixTaxGateway;
import com.wiley.core.integration.sabrix.dto.Wileyb2cTaxResponse;
import com.wiley.core.externaltax.services.impl.TaxSystemResponseError;


public class Wileyb2cCalculateExternalTaxesStrategy implements CalculateExternalTaxesStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cCalculateExternalTaxesStrategy.class);

	@Autowired
	private SabrixTaxGateway sabrixTaxGateway;

	@Override
	public ExternalTaxDocument calculateExternalTaxes(final AbstractOrderModel order)
	{
		ExternalTaxDocument result = new ExternalTaxDocument();
		try
		{
			Wileyb2cTaxResponse calculationResult = sabrixTaxGateway.calculateTaxForOrder(order);
			order.setTotalTax(calculationResult.getOrderTotalTax());
			order.setTaxCalculated(true);
		}
		catch (TaxSystemResponseError ex)
		{
			LOG.warn("Sabrix returned error message '" + ex.getMessage() + "' for order code " + order.getCode());
			order.setTaxCalculated(false);
		}
		catch (Exception e)
		{
			LOG.error("Error during execution Sabrix tax calculation chain. Order code: " + order.getCode(), e);
			order.setTaxCalculated(false);
		}
		return result;
	}
}
