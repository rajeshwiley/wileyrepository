package com.wiley.core.payment.enums;


public enum WileyTransactionStatusEnum
{

	SUCCESS("0", "The transaction has completed successfully."),
	DECLINED("1", "The transaction has been processed and declined."),
	CANCELLED("2", "The transaction has been cancelled by the user."),
	REFERRAL("3", "The transaction has been referred."),
	HOTCARD("4", "The payment card used in this transaction is potentially stolen."),
	THREE_D_FAIL("5", "The user has failed 3D Secure security check."),
	UNAVAILABLE("6", "The Gateway service is unavailable."),
	INTERNALFAIL("7", "The Gateway has suffered an internal error. The Gateway Manager will need to be contacted."),
	VENDOR_ID_INVALID("101", "The provided Vendor ID is invalid."),
	VENDOR_SECRET_INVALID("102", "The provided Vendor Secret is invalid."),
	VENDOR_DISABLED("103", "The provided Vendor has been disabled."),
	VENDOR_TEST_ONLY("104", "The provided Vendor is not enabled for production."),
	VENDOR_SETUP_INVALID("105", "The provided Vendor has an incomplete or corrupt setup. Contact Gateway Manager."),
	VENDOR_UNKNOWN_ERROR("106", "An unknown Vendor related error has occurred. Contact Gateway Manager."),
	VENDOR_TIMEOUT("107", "A digest security timeout has occurred."),
	USER_ID_INVALID("201", "The provided User ID is invalid."),
	USER_UNAUTHORISED("202", "The provided User is not authorised to use this device."),
	USER_SETUP_INVALID("203", "The provided User has an incomplete or corrupt setup."),
	USER_DISABLED("204", "Contact Gateway Manager."),
	USER_UNKNOWN_ERROR("205", "The User ID is disabled"),
	TRANS_ID_MISSING("301", "An unknown User related error has occurred. Contact Gateway Manager."),
	TRANS_DATA_MISSING("302", "The transaction was presented with either an invalid or empty Transaction ID."),
	TRANS_SECURITY_FAIL("303", "One or more of the required elements of data are missing."),
	TRANS_INTEGRITY_FAIL("304", "The security countermeasure is invalid."),
	TRANS_UNKNOWN_ERROR("305", "An internal integrity check failed. Contact Gateway Manager."),
	PED_PROBE_FAIL("401", "An internal data error has occurred."),
	PED_PROBE_MERCHANT_FAIL("402", "The local PED hardware is not available."),
	PED_AUTH_FAIL("403", "The merchant services are not available."),
	HTTP_SERVER_UNAVAILABLE("500", "The authorisation token for this PED is invalid or expired."),
	COM_WPG_FAIL("900", "The server contacted is not able to service your request. Please try another."),
	COM_MERCHANT_FAIL("901", "Communication failure in The Gateway"),
	UNKNOWN_FAILURE("999", "Communication failure with Payment Card Processor"),


	UNEXPECTED_CODE_FROM_WPG("UnknownCode", "Unexpected error code has been received from WPG. Transaction is marked as failed"),
	SECURITY_TOKEN_MISMATCH_FROM_WPG("SecurityMismatch", "Wrong security token received from WPG"),
	WPG_COMMUNICATION_FAILURE("WPGCommunicationFailure", "There were problems in communication with WPG"),
	GENERAL_SYSTEM_ERROR("GeneralSystemError", "There were technical problems during transaction process. "
			+ "Please check logs for details"),
	AMOUNT_MISMATCH("CaptureAmountMismatch", "Amount requested to capture from WPG differs from actually captured.");


	private final String code;
	private final String description;

	WileyTransactionStatusEnum(final String code, final String description)
	{
		this.code = code;
		this.description = description;
	}

	public String getCode()
	{
		return code;
	}

	public String getDescription()
	{
		return description;
	}

	public static WileyTransactionStatusEnum findByCode(final String code)
	{
		for (WileyTransactionStatusEnum statusEnum : values())
		{
			if (statusEnum.getCode().equals(code))
			{
				return statusEnum;
			}
		}
		return null;
	}

}
