package com.wiley.core.partner.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserGroupModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.order.WileyFindProductsInCartService;
import com.wiley.core.partner.WileyPartnerCompanyDao;
import com.wiley.core.partner.WileyPartnerService;

import static org.springframework.util.Assert.notNull;


public class WileyPartnerServiceImpl implements WileyPartnerService
{
	private static final Logger LOG = Logger.getLogger(WileyPartnerServiceImpl.class);

	@Resource
	private WileyPartnerCompanyDao wileyPartnerCompanyDao;

	@Resource
	private WileyFindProductsInCartService wileyFindProductsInCartService;

	@Override
	public WileyPartnerCompanyModel getPartnerById(final String partnerId)
	{
		if (StringUtils.isEmpty(partnerId))
		{
			return null;
		}
		WileyPartnerCompanyModel partnerCompany = null;
		final UserGroupModel partnerGroup = wileyPartnerCompanyDao.findUserGroupByUid(partnerId);

		if (partnerGroup instanceof WileyPartnerCompanyModel)
		{
			partnerCompany = (WileyPartnerCompanyModel) partnerGroup;
		}
		else
		{
			LOG.error("Cannot find Wiley Partner Company with id " + partnerId
					+ ". This company does not exist or has the wrong type");
		}

		return partnerCompany;
	}

	@Override
	public boolean isPartnerOrder(@Nonnull final AbstractOrderModel orderModel)
	{
		notNull(orderModel);

		return orderModel.getWileyPartner() != null;
	}

	@Override
	public Optional<String> getPartnerCode(@Nonnull final AbstractOrderModel orderModel)
	{
		notNull(orderModel);

		Optional<String> result = Optional.empty();

		final WileyPartnerCompanyModel partner = orderModel.getWileyPartner();
		if (partner != null)
		{	
			result = Optional.of(partner.getUid());
		}

		return result;
	}

	@Override
	public boolean partnerBelongsToUserGroup(@Nonnull final String partnerCompanyUid, @Nonnull final String userGroupUid)
	{
		notNull(partnerCompanyUid);
		notNull(userGroupUid);
		return wileyPartnerCompanyDao.getPartnersByParentUserGroup(userGroupUid).stream()
				.anyMatch(partnerWithParentUserGroup ->
					 partnerCompanyUid != null && partnerCompanyUid.equals(partnerWithParentUserGroup.getUid())
				);
	}

	@Override
	public List<WileyPartnerCompanyModel> getPartnersByCategory(@Nonnull final String categoryCode)
	{
		return wileyPartnerCompanyDao.getPartnersByCategory(categoryCode);
	}

	@Override
	public boolean orderContainsPartnerProduct(final WileyPartnerCompanyModel wileyPartner,
			final AbstractOrderModel abstractOrder)
	{
		boolean hasPartnerProduct = false;
		if (wileyPartner != null && CollectionUtils.isNotEmpty(wileyPartner.getPartnerCategories()))
		{
			hasPartnerProduct = wileyPartner.getPartnerCategories().stream()
					.filter(category -> wileyFindProductsInCartService.containsAtLeastOneProduct(abstractOrder, category))
					.findAny().isPresent();
		}
		return hasPartnerProduct;
	}
}
