package com.wiley.core.integration.esb.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeliveryOptionResponseDto
{
	@JsonProperty("code")
	private String code;
	@JsonProperty("cost")
	private Double cost;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public Double getCost()
	{
		return cost;
	}

	public void setCost(final Double cost)
	{
		this.cost = cost;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("code", code)
				.add("cost", cost)
				.toString();
	}
}
