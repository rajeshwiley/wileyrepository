package com.wiley.core.mpgs.services;

import de.hybris.platform.core.model.order.OrderModel;

import com.wiley.core.mpgs.response.WileyTokenizationResponse;



public interface WileyMPGSPaymentInfoService
{
	void setCreditCardPaymentInfo(String tnsMerchantId, WileyTokenizationResponse subscriptionResult,
			Boolean saveInAccount);
	void setCreditCardPaymentInfo(String tnsMerchantId, WileyTokenizationResponse subscriptionResult,
			Boolean saveInAccount, OrderModel orderModel);
}
