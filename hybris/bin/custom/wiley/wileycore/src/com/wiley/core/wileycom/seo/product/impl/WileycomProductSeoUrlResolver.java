package com.wiley.core.wileycom.seo.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.resolver.impl.WileyAbstractUrlResolver;


/**
 * Created by Uladzimir_Barouski on 6/26/2017.
 */
public class WileycomProductSeoUrlResolver extends WileyAbstractUrlResolver<ProductModel>
{
	public static final String LOCALIZED_PRODUCT_NAME = "{localized-product-name}";
	public static final String PRODUCT_CODE = "{product-code}";

	private String urlPattern;

	@Override
	protected String resolveInternal(final ProductModel source)
	{
		final ProductModel baseProduct = getBaseProduct(source);

		String url = urlPattern;

		if (url.contains(LOCALIZED_PRODUCT_NAME))
		{
			String localizedName = getSeoName(baseProduct);
			url = url.replace(LOCALIZED_PRODUCT_NAME, localizedName);
		}
		if (url.contains(PRODUCT_CODE))
		{
			url = url.replace(PRODUCT_CODE, source.getCode());
		}

		return url;
	}

	protected String getSeoName(final ProductModel product)
	{
		return StringUtils.isNotEmpty(product.getSeoName()) ? urlEncode(product.getSeoName()) : urlSafe(product.getName());
	}

	@Required
	public void setUrlPattern(final String urlPattern)
	{
		this.urlPattern = urlPattern;
	}

	protected ProductModel getBaseProduct(final ProductModel product)
	{
		ProductModel current = product;

		while (current instanceof VariantProductModel)
		{
			final ProductModel baseProduct = ((VariantProductModel) current).getBaseProduct();
			if (baseProduct == null)
			{
				break;
			}
			else
			{
				current = baseProduct;
			}
		}
		return current;
	}
}
