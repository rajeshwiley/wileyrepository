package com.wiley.core.wel.customer.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;

import com.wiley.core.customer.service.impl.WileyCustomerAccountServiceImpl;
import com.wiley.core.event.NewCustomerEvent;


public class WelCustomerAccountServiceImpl extends WileyCustomerAccountServiceImpl
{
	@Override
	public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException
	{
		super.register(customerModel, password);
		getEventService().publishEvent(new NewCustomerEvent(customerModel));
	}
}
