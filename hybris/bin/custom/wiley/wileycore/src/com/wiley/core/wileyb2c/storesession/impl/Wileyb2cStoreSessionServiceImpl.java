package com.wiley.core.wileyb2c.storesession.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.lang3.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.storesession.impl.WileyStoreSessionServiceImpl;
import com.wiley.core.wileyb2c.storesession.Wileyb2cStoreSessionService;

import java.util.Optional;


public class Wileyb2cStoreSessionServiceImpl extends WileyStoreSessionServiceImpl implements Wileyb2cStoreSessionService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyStoreSessionServiceImpl.class);

	private static final char SPECIAL_LOCALE_SEPARATOR = '-';
	private static final int SPECIAL_LOCALE_TOKEN_AMOUNT = 2;
	private static final String SPECIAL_LOCALE_DEFAULT_LANGUAGE = "en";

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private BaseStoreService baseStoreService;

	/**
	 * @param specialLocale
	 * 		Locale that is
	 */
	public boolean isValidSpecialLocale(@Nullable final String specialLocale)
	{
		return extractCountry(specialLocale) != null;
	}

	@Nullable
	private CountryModel extractCountry(@Nullable final String specialLocale)
	{
		final StrTokenizer tokenizer = new StrTokenizer(specialLocale, SPECIAL_LOCALE_SEPARATOR);
		tokenizer.setIgnoreEmptyTokens(true);
		if (tokenizer.getTokenList().size() == SPECIAL_LOCALE_TOKEN_AMOUNT)
		{
			final String languageIsocode = tokenizer.next().toLowerCase();
			final String countryIsocode = tokenizer.next().toUpperCase();

			CountryModel country = null;
			try
			{
				country = commonI18NService.getCountry(countryIsocode);

			}
			catch (final UnknownIdentifierException e)
			{
				LOG.debug("Can't find country [{}] from special locale [{}]", countryIsocode, specialLocale);
			}

			if (country != null && commerceCommonI18NService.getAllCountries().contains(country)
					&& languageIsocode.equals(SPECIAL_LOCALE_DEFAULT_LANGUAGE))
			{
				return country;
			}


		}
		return null;
	}

	@Override
	public void setCurrentSpecialLocale(@Nonnull final String specialLocale)
	{
		wileycomI18NService.setCurrentCountry(extractCountry(specialLocale));
	}

	@Override
	public String getCurrentSpecialLocale()
	{
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent()) {
			return buildSpecialLocale(currentCountry.get());
		} else {
			return null;
		}
	}

	@Override
	public String getDefaultSpecialLocale()
	{
		return buildSpecialLocale(wileycomI18NService.getDefaultCountry());
	}

	private String buildSpecialLocale(@Nonnull final CountryModel country)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(SPECIAL_LOCALE_DEFAULT_LANGUAGE);
		builder.append(SPECIAL_LOCALE_SEPARATOR);
		builder.append(country.getIsocode().toLowerCase());
		return builder.toString();
	}

	@Override
	public void setCurrentCountry(final String isoCode)
	{
		final CountryModel country = commonI18NService.getCountry(isoCode);
		if (commerceCommonI18NService.getAllCountries().contains(country))
		{
			wileycomI18NService.setCurrentCountry(country);
		}
		else
		{
			throw new IllegalArgumentException(String.format("Setting [%s] country failed, "
							+ "country isn't allowed for current [%s] base store",
					isoCode, baseStoreService.getCurrentBaseStore().getUid()));
		}


	}


}
