package com.wiley.core.payment;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.PaymentModeService;

import java.util.List;

public interface WileyPaymentModeService extends PaymentModeService {

    List<PaymentModeModel> getSupportedPaymentModes(AbstractOrderModel orderModel);
}
