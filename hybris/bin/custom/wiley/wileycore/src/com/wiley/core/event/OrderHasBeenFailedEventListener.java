package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.wiley.core.constants.WileyCoreConstants;


public class OrderHasBeenFailedEventListener extends AbstractSiteEventListener<OrderHasBeenFailedEvent>
{
	private ModelService modelService;
	private BusinessProcessService businessProcessService;


	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * On site event.
	 *
	 * @param orderHasBeenFailedEvent
	 * 		the order has been failed event
	 */
	@Override
	protected void onSiteEvent(final OrderHasBeenFailedEvent orderHasBeenFailedEvent)
	{
		final OrderModel orderModel = orderHasBeenFailedEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				WileyCoreConstants.ORDER_HAS_BEEN_FAILED_EMAIL_PROCESS + "-" + orderModel.getCode() + "-"
						+ System.currentTimeMillis(), WileyCoreConstants.ORDER_HAS_BEEN_FAILED_EMAIL_PROCESS);
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	@Override
	protected boolean shouldHandleEvent(final OrderHasBeenFailedEvent event)
	{
		final OrderModel order = event.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return true;
	}
}
