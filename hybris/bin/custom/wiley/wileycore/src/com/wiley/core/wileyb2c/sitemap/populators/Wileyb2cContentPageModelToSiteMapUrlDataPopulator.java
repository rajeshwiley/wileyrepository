package com.wiley.core.wileyb2c.sitemap.populators;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ContentPageModelToSiteMapUrlDataPopulator;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;


/**
 * Created by Maksim_Kozich on 21.09.17.
 */
public class Wileyb2cContentPageModelToSiteMapUrlDataPopulator extends ContentPageModelToSiteMapUrlDataPopulator
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cContentPageModelToSiteMapUrlDataPopulator.class);

	@Override
	public void populate(final ContentPageModel contentPageModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		superPopulate(contentPageModel, siteMapUrlData);
		if (siteMapUrlData.getLoc() == null)
		{
			getLogger().error("Resolved relative URL for content page '" + contentPageModel.getUid() + "' is null");
		}
	}

	protected void superPopulate(final ContentPageModel contentPageModel, final SiteMapUrlData siteMapUrlData)
	{
		super.populate(contentPageModel, siteMapUrlData);
	}

	protected Logger getLogger()
	{
		return LOG;
	}
}
