package com.wiley.core.search.client.solrj.facet;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonProperty;


/**
 * JSON Facet API, facet object
 */
public class FacetSwg implements Serializable
{
	private String sort;
	private Map<String, Object> facets = new HashMap<>();

	public void addFacet(final TermFacetSwg termFacet)
	{
		facets.put(termFacet.getField(), termFacet);
	}

	public void addQueryFacet(final QueryFacetSwg facet)
	{
		facets.put(facet.getQuery(), facet);
	}

	public void setSort(final String sort)
	{
		this.sort = sort;
	}

	@JsonProperty
	public String getSort()
	{
		return sort;
	}

	public void setFacets(final Map<String, Object> facets)
	{
		this.facets = facets;
	}

	@JsonAnyGetter
	public Map<String, Object> getFacets()
	{
		return facets;
	}
}
