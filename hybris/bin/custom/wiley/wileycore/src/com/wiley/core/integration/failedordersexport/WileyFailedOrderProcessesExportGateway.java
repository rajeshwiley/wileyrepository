package com.wiley.core.integration.failedordersexport;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;

public interface WileyFailedOrderProcessesExportGateway {

    /**
     * Performs exporting info about failed Order fulfillment processes
     * @param baseStore - BaseSite
     * @param from - start date to check for failed processes
     * @param to - end date to check for failed processes
     */
    void exportFailedOrderProcesses(BaseStoreModel baseStore, LocalDateTime from, LocalDateTime to);

}
