package com.wiley.core.wileycom.stock.service.impl;

import de.hybris.platform.ordersplitting.model.StockLevelModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.wileycom.stock.dao.WileycomStockLevelDao;
import com.wiley.core.wileycom.stock.service.WileycomStockLevelService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Created by Raman_Hancharou on 4/4/2017.
 */
public class WileycomStockLevelServiceImpl implements WileycomStockLevelService
{

	@Resource
	private WileycomStockLevelDao wileycomStockLevelDao;

	@Override
	public StockLevelModel findStockLevelByCode(@Nonnull final String stockLevelCode)
	{
		validateParameterNotNull(stockLevelCode, "StockLevelCode cannot be null");
		return wileycomStockLevelDao.findStockLevelByCode(stockLevelCode);
	}
}
