/**
 *
 */
package com.wiley.core.servicelayer.i18n;

import de.hybris.platform.core.model.c2l.CountryModel;

import java.util.Optional;

import javax.annotation.Nonnull;


public interface WileycomI18NService
{

	/**
	 * Set country to session
	 *
	 * @param countryModel
	 * 		country item to set
	 */
	void setCurrentCountry(CountryModel countryModel);


	/**
	 * Get country from session
	 *
	 * @return {@link CountryModel} item that is set to session
	 */
	Optional<CountryModel> getCurrentCountry();


	/**
	 * Set default country to session.
	 *
	 * @return Return country, that was set to session.
	 */
	CountryModel setDefaultCurrentCountry();

	/**
	 * Get default country
	 *
	 * @return Return country
	 */
	@Nonnull
	CountryModel getDefaultCountry();

}
