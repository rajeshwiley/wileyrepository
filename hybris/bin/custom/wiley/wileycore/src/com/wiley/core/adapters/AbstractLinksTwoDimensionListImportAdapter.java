package com.wiley.core.adapters;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.text.StrTokenizer;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;


/**
 * Created by Aliaksei_Zhvaleuski on 29.03.2017.
 */
public abstract class AbstractLinksTwoDimensionListImportAdapter
		extends AbstractTwoDimensionListImportAdapter<WileyWebLinkModel>
{

	private static final String ULR_PARAMETER_NAME = "URL";
	private static final boolean IS_MANDATORY = true;

	@Autowired
	private ModelService modelService;

	@Override
	public WileyWebLinkModel convertToModel(@Nonnull final String token, @Nonnull final ProductModel productModel)
			throws ImpExException
	{
		Preconditions.checkNotNull(token, "token parameter should not be null");
		Preconditions.checkNotNull(productModel, "productModel parameter should not be null");

		final StrTokenizer linkTokenizer = getSecondDimensionTokenizer(token);

		final WileyWebLinkModel wileyWebLinkModel = modelService.create(WileyWebLinkModel.class);

		final String typeValue = getNextValue(linkTokenizer, IS_MANDATORY, WileyWebLinkModel.TYPE);
		wileyWebLinkModel.setType(WileyWebLinkTypeEnum.valueOf(typeValue));

		final String urlValue = getNextValue(linkTokenizer, IS_MANDATORY, ULR_PARAMETER_NAME);
		wileyWebLinkModel.setParameters(Collections.singletonList(urlValue));

		return wileyWebLinkModel;
	}

}
