package com.wiley.core.i18n;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Locale;
import java.util.List;

import javax.annotation.Nonnull;


public interface WileyCommonI18NService
{
	/**
	 * Returns region for country and two-letter ISO code that specifies a state
	 * or province. The codes were derived from ISO 3166-2 codes, for non-standard
	 * Armed Forces regions values were provided by Wiley. See
	 * https://jira.wiley.ru/browse/ECSC-5892 for details.
	 *
	 * @param countryModel
	 * @param shortRegionCode
	 * @return regionModel
	 */
	RegionModel getRegionForShortCode(CountryModel countryModel, String shortRegionCode);

	/**
	 * Checks if the the language for given code is valud or not
	 *
	 * @param value
	 * @return boolean
	 */
	boolean isValidLanguage(String value);

	/**
	 * Returns Default Currency for specified countryModel and current baseStore.
	 *
	 * @param countryModel
	 * @return currencyModel
	 * @throws IllegalStateException
	 * 		if there is no default currency configured
	 * @throws IllegalArgumentException
	 * 		if currentBasestore is not set
	 */
	CurrencyModel getDefaultCurrency(@Nonnull CountryModel countryModel) throws IllegalStateException;

	/**
	 * Returns Default Currency for specified countryModel and baseStore.
	 *
	 * @param countryModel
	 * @param baseStoreModel
	 * @return currencyModel
	 * @throws IllegalStateException
	 * 		if there is no default currency configured
	 */
	CurrencyModel getDefaultCurrency(@Nonnull CountryModel countryModel, @Nonnull BaseStoreModel baseStoreModel)
			throws IllegalStateException;

	/**
	 * Returns all visible regions for specific country and base store
	 *
	 * @param countryModel
	 * @param baseStoreModel
	 * @return
	 */
	List<RegionModel> getConfigurableRegionsForCurrentBaseStore(@Nonnull CountryModel countryModel,
			@Nonnull BaseStoreModel baseStoreModel);

	/**
	 * Get locale by country iso code.
	 * If country iso is null then return current locale.
	 *
	 * @param countryIso
	 * 		for the locale
	 * @return locale
	 */
	Locale getLocaleByCountryIsoCodeWithCurrentLanguage(String countryIso);
}
