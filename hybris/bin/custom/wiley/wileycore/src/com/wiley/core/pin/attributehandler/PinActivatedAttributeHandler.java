package com.wiley.core.pin.attributehandler;

import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import com.wiley.core.model.PinModel;


/**
 * Dynamic attribute handler to specify if a Pin is already used, which means it has an order already associated with.
 */
public class PinActivatedAttributeHandler implements DynamicAttributeHandler<Boolean, PinModel>
{
    @Override
    public Boolean get(final PinModel model)
    {
        return model.getOrder() != null;
    }

    @Override
    public void set(final PinModel model, final Boolean aBoolean)
    {
        throw new UnsupportedOperationException();
    }
}
