package com.wiley.core.validation.localized;

import de.hybris.platform.validation.localized.LocalizedAttributeConstraint;
import de.hybris.platform.validation.model.constraints.AttributeConstraintModel;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.lang3.LocaleUtils;

import static org.apache.commons.lang3.LocaleUtils.toLocale;


/**
 * Class adjust <code>languages</code> (i.e. list of locales) generation of OOTB {@link LocalizedAttributeConstraint}
 * and uses {@link LocaleUtils#toLocale(String)} for valid generation of underscored language isocode (like 'en_US')
 * to {@link Locale}.
 */
public class WileyLocalizedAttributeConstraint extends LocalizedAttributeConstraint
{
	private List<Locale> languages;

	public WileyLocalizedAttributeConstraint(final AttributeConstraintModel constraint)
	{
		super(constraint);
		this.languages = constraint.getLanguages().stream().map(lang -> toLocale(lang.getIsocode())).collect(Collectors.toList());
	}

	@Override
	public List<Locale> getLanguages()
	{
		return Collections.unmodifiableList(this.languages);
	}
}
