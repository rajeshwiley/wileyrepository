package com.wiley.core.wileycom.users.service.impl;

//import com.wiley.core.authentication.data.users.WileyUserAuthenticateRequest;

import de.hybris.platform.core.model.user.UserModel;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.users.gateway.WileyUsersGateway;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.core.wileycom.users.service.WileycomUsersService;
import com.wiley.core.wileycom.users.service.exception.WileyUserBadRequestException;
import com.wiley.core.wileycom.users.service.exception.WileyUserUnreachableException;


public class WileycomUsersServiceImpl implements WileycomUsersService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomUsersServiceImpl.class);

	@Resource
	private WileyUsersGateway wileyUsersGateway;

	@Override
	public boolean authenticate(final UserModel user, final String password)
	{
		try
		{
			return wileyUsersGateway.authenticate(user, password);
		}
		catch (ExternalSystemInternalErrorException | ExternalSystemAccessException e)
		{
			LOG.error("Login failed for user: " + user.getUid(), e);
			throw new WileyUserUnreachableException(e.getMessage(), e);
		}
		catch (final ExternalSystemBadRequestException e)
		{
			LOG.error("Login failed for user: " + user.getUid(), e);
			throw new WileyUserBadRequestException(e.getMessage(), e);
		}
	}

	@Override
	public boolean updatePassword(@NotNull final UserModel user, @NotNull final String oldPassword,
			@NotNull final String newPassword)
	{
		Assert.notNull(user);
		Assert.notNull(oldPassword);
		Assert.notNull(newPassword);
		try
		{
			final boolean isPasswordUpdated = wileyUsersGateway.updatePassword(user, oldPassword, newPassword);
			LOG.debug("wileyUsersGateway was executed to update password of [{}] user. Result: [{}]", user.getUid(),
					isPasswordUpdated);
			return isPasswordUpdated;
		}
		/*
			catching RuntimeException because if users request isn't authorized(401 error), ExternalSystemResponseErrorHandler
			wouldn't be called. Please refer org.springframework.security.oauth2.client.http.OAuth2ErrorHandler.handleError
		 */
		catch (final RuntimeException e)
		{
			LOG.debug("wileyUsersGateway failed on password update", e);
			throw new PasswordUpdateException(e.getMessage(), e);
		}
	}

	/**
	 * Updates USERS uid
	 *
	 * @param userModel
	 * 		- user to update
	 * @param newUid
	 * 		- new uid
	 * @param password
	 * @return true if password was updated
	 */
	public boolean updateUserId(@NotNull final UserModel userModel, @NotNull final String newUid, @NotNull final String password)
	{
		Assert.notNull(userModel);
		Assert.notNull(newUid);
		Assert.notNull(password);
		return wileyUsersGateway.updateUserId(newUid, password, userModel);
	}
}
