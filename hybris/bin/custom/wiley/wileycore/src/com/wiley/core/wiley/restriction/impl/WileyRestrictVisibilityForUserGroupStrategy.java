package com.wiley.core.wiley.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyRestrictVisibilityForUserGroupStrategy implements WileyRestrictProductStrategy
{
	@Resource
	private UserService userService;

	@Override
	public boolean isRestricted(final ProductModel product)
	{
		final Set<UserGroupModel> currentUserGroups = userService.getAllUserGroupsForUser(userService.getCurrentUser());
		return isRestricted(product, currentUserGroups);
	}

	private boolean isRestricted(final ProductModel product, final Set<UserGroupModel> currentUserGroups)
	{
		boolean restricted = CollectionUtils.isNotEmpty(product.getUserGroups()) && !currentUserGroups.stream().anyMatch(
				userGroupModel -> product.getUserGroups().contains(userGroupModel));
		if (!restricted)
		{
			if (product instanceof VariantProductModel)
			{
				restricted = isRestricted(((VariantProductModel) product).getBaseProduct(), currentUserGroups);
			}
			else if (CollectionUtils.isNotEmpty(product.getVariants()))
			{
				//The base product should be restricted if all variants restricted
				restricted = isAllProductVariantsRestricted(product.getVariants(), currentUserGroups);
			}
		}
		return restricted;
	}

	private boolean isAllProductVariantsRestricted(final Collection<VariantProductModel> variantProductModelList,
			final Set<UserGroupModel> currentUserGroups)
	{
		for (VariantProductModel variant : variantProductModelList)
		{
			boolean restricted = CollectionUtils.isNotEmpty(variant.getUserGroups()) && !currentUserGroups.stream().anyMatch(
					userGroupModel -> variant.getUserGroups().contains(userGroupModel));
			if (!restricted)
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available for user group", parameter.getProduct().getCode()), null
		);
	}

}
