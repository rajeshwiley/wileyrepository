package com.wiley.core.mpgs.services.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;


public interface CardVerificationStrategy
{
	/**
	 * Implements the way to verify card data
	 *
	 * @param cart
	 * @param sessionId
	 * @return
	 */
	PaymentTransactionEntryModel verify(AbstractOrderModel cart, String sessionId);
}
