package com.wiley.core.payment.request;

import de.hybris.platform.payment.commands.request.SubscriptionAuthorizationRequest;
import de.hybris.platform.payment.dto.BillingInfo;

import java.math.BigDecimal;
import java.util.Currency;

public class WileySubscriptionAuthorizeRequest extends SubscriptionAuthorizationRequest
{
	private final String site;

	public WileySubscriptionAuthorizeRequest(final String site, final String subscriptionID,
											 final Currency currency, final BigDecimal totalAmount,
											 final BillingInfo billingInfo, final String paymentProvider)
	{
		super(null, subscriptionID, currency, totalAmount, billingInfo, paymentProvider);
		this.site = site;
	}

	public String getSite()
	{
		return site;
	}
}
