package com.wiley.core.search.client.solrj.facet;



import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * JSON Facet API, query facet object
 */
public class QueryFacetSwg
{
	public static final String DEFAULT_TYPE = "query";
	private String type;
	private String query;
	private Map<String, String> facet = new HashMap<>();

	public QueryFacetSwg(final String query)
	{
		this.type = DEFAULT_TYPE;
		this.query = query;
	}

	@JsonProperty
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	@JsonProperty("q")
	public String getQuery()
	{
		return query;
	}

	public void setQuery(final String query)
	{
		this.query = query;
	}

	@JsonProperty
	public Map<String, String> getFacet()
	{
		return facet;
	}

	public void setFacet(final Map<String, String> facet)
	{
		this.facet = facet;
	}
}
