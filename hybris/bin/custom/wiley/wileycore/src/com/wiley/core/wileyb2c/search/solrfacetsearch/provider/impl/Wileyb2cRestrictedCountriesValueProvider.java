package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * Created by Uladzimir_Barouski on 3/10/2017.
 */
public class Wileyb2cRestrictedCountriesValueProvider extends AbstractWileycomValueProvider
{
	@Override
	protected List collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object product)
			throws FieldValueProviderException
	{

		final List<String> restrictedCountries = getRestrictedCountries((ProductModel) product);
		if (CollectionUtils.isEmpty(restrictedCountries))
		{
			return Collections.emptyList();
		}
		return restrictedCountries;
	}

	private List<String> getRestrictedCountries(final ProductModel variant)
	{
		return variant.getCountryRestrictions()
				.stream()
				.map(CountryModel::getIsocode)
				.collect(Collectors.toList());
	}
}
