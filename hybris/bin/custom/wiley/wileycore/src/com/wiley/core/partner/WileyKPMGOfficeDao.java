package com.wiley.core.partner;

import java.util.List;

import com.wiley.core.model.KpmgOfficeModel;


/**
 * Wiley KPMG Office DAO
 */
public interface WileyKPMGOfficeDao
{
	/**
	 *
	 * Executes flexible search query to find KPMG Office Models associated with give Wiley Partner Company
	 * @param partnerId identifier of Wiley Partner Company
	 * @return the list of KPMG Offices or empty list
	 */
	List<KpmgOfficeModel> findOffices(String partnerId);

	/**
	 * Finds KPMG Office by its code
	 * @param code
	 * @return
	 */
	KpmgOfficeModel findOfficeByCode(String code);
}
