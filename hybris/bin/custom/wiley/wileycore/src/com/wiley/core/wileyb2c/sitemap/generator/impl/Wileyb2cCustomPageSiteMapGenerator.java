package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.converters.Converters;

import java.util.List;


public class Wileyb2cCustomPageSiteMapGenerator extends AbstractWileyb2cSiteMapGenerator<String>
{
	/**
	 * OOTB_CODE from CustomPageSiteMapGenerator
	 */
	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<String> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	/**
	 * OOTB_CODE from CustomPageSiteMapGenerator
	 */
	@Override
	protected List<String> getDataInternal(final CMSSiteModel siteModel)
	{
		return (List<String>) siteModel.getSiteMapConfig().getCustomUrls();
	}
}
