package com.wiley.core.mpgs.services.impl;


import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentEntryService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentInfoService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;


public class WileyMPGSPaymentEntryServiceImpl implements WileyMPGSPaymentEntryService
{
	@Autowired
	private PaymentService paymentService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private WileyMPGSPaymentInfoService wileyMPGSPaymentInfoService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@Autowired
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;

	@Resource
	private AmountToPayCalculationStrategy amountToPayCalculationStrategy;

	@Override
	public PaymentTransactionEntryModel createRetrieveSessionEntry(
			@Nonnull final WileyRetrieveSessionResponse retrieveSessionResponse,
			@Nonnull final PaymentTransactionModel paymentTransaction)
	{
		ServicesUtil.validateParameterNotNull(retrieveSessionResponse, "[retrieveSessionResponse] can't be null.");
		ServicesUtil.validateParameterNotNull(paymentTransaction, "[paymentTransaction] can't be null.");

		PaymentTransactionEntryModel retrieveSessionEntry = modelService.create(PaymentTransactionEntryModel.class);
		retrieveSessionEntry.setType(PaymentTransactionType.RETRIEVE_SESSION);
		retrieveSessionEntry.setTime(Date.from(Instant.now()));
		retrieveSessionEntry.setTransactionStatus(retrieveSessionResponse.getStatus());
		retrieveSessionEntry.setTransactionStatusDetails(retrieveSessionResponse.getStatusDetails());
		retrieveSessionEntry.setPaymentTransaction(paymentTransaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction,
				PaymentTransactionType.RETRIEVE_SESSION);
		retrieveSessionEntry.setCode(newEntryCode);
		retrieveSessionEntry.setRequestId(retrieveSessionResponse.getSessionId());
		retrieveSessionEntry.setCurrency(paymentTransaction.getOrder().getCurrency());
		modelService.saveAll(paymentTransaction, retrieveSessionEntry);

		return retrieveSessionEntry;
	}

	@Override
	public PaymentTransactionEntryModel createVerifyEntry(@Nonnull final WileyVerifyResponse verifyResponse,
			@Nonnull final PaymentTransactionModel paymentTransaction)
	{
		ServicesUtil.validateParameterNotNull(verifyResponse, "[verifyResponse] can't be null.");
		ServicesUtil.validateParameterNotNull(paymentTransaction, "[paymentTransaction] can't be null.");

		PaymentTransactionEntryModel verifyEntry = modelService.create(PaymentTransactionEntryModel.class);
		verifyEntry.setType(PaymentTransactionType.VERIFY);
		verifyEntry.setTime(verifyResponse.getTimeOfRecord());
		verifyEntry.setTransactionStatus(verifyResponse.getStatus());
		verifyEntry.setTransactionStatusDetails(verifyResponse.getStatusDetails());
		verifyEntry.setPaymentTransaction(paymentTransaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction, PaymentTransactionType.VERIFY);
		verifyEntry.setCode(newEntryCode);
		verifyEntry.setRequestId(verifyResponse.getTransactionId());
		if (verifyEntry.getCurrency() != null)
		{
			verifyEntry.setCurrency(commonI18NService.getCurrency(verifyResponse.getCurrency()));
		}
		modelService.saveAll(paymentTransaction, verifyEntry);

		return verifyEntry;
	}

	@Override
	public PaymentTransactionEntryModel createTokenizationEntry(@Nonnull final WileyTokenizationResponse tokenizationResponse,
			@Nonnull final PaymentTransactionModel transaction, final Boolean saveInAccount)
	{

		ServicesUtil.validateParameterNotNull(tokenizationResponse, "[tokenizationResult] can't be null.");
		ServicesUtil.validateParameterNotNull(transaction, "[paymentTransaction] can't be null.");
		PaymentTransactionEntryModel tokenizationEntry = modelService.create(PaymentTransactionEntryModel.class);
		tokenizationEntry.setType(PaymentTransactionType.TOKENIZATION);
		tokenizationEntry.setTime(Date.from(Instant.now()));
		tokenizationEntry.setTransactionStatus(tokenizationResponse.getStatus());
		tokenizationEntry.setTransactionStatusDetails(tokenizationResponse.getStatusDetails());
		tokenizationEntry.setPaymentTransaction(transaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, PaymentTransactionType.TOKENIZATION);
		tokenizationEntry.setCode(newEntryCode);

		if (TransactionStatus.ACCEPTED.name().equalsIgnoreCase(tokenizationResponse.getStatus()))
		{
			String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(transaction.getOrder());
			tokenizationEntry.setRequestToken(tokenizationResponse.getToken());
			wileyMPGSPaymentInfoService.setCreditCardPaymentInfo(tnsMerchantId, tokenizationResponse, saveInAccount);
			//Saved below, info added into transaction on tokenization step because before it was empty
			transaction.setInfo(transaction.getOrder().getPaymentInfo());
		}
		modelService.saveAll(transaction, tokenizationEntry);
		return tokenizationEntry;
	}

	@Override
	public PaymentTransactionEntryModel createTokenizationEntry(@Nonnull final WileyTokenizationResponse tokenizationResponse,
			@Nonnull final PaymentTransactionModel transaction, final Boolean saveInAccount, @Nonnull final OrderModel orderModel)
	{

		ServicesUtil.validateParameterNotNull(tokenizationResponse, "[tokenizationResult] can't be null.");
		ServicesUtil.validateParameterNotNull(transaction, "[paymentTransaction] can't be null.");
		ServicesUtil.validateParameterNotNull(orderModel, "[orderModel] can't be null.");
		PaymentTransactionEntryModel tokenizationEntry = modelService.create(PaymentTransactionEntryModel.class);
		tokenizationEntry.setType(PaymentTransactionType.TOKENIZATION);
		tokenizationEntry.setTime(Date.from(Instant.now()));
		tokenizationEntry.setTransactionStatus(tokenizationResponse.getStatus());
		tokenizationEntry.setTransactionStatusDetails(tokenizationResponse.getStatusDetails());
		tokenizationEntry.setPaymentTransaction(transaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, PaymentTransactionType.TOKENIZATION);
		tokenizationEntry.setCode(newEntryCode);

		if (TransactionStatus.ACCEPTED.name().equalsIgnoreCase(tokenizationResponse.getStatus()))
		{
			String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(transaction.getOrder());
			tokenizationEntry.setRequestToken(tokenizationResponse.getToken());
			wileyMPGSPaymentInfoService.setCreditCardPaymentInfo(tnsMerchantId, tokenizationResponse, saveInAccount, orderModel);
		}
		modelService.saveAll(transaction, tokenizationEntry);
		return tokenizationEntry;
	}

	@Override
	public PaymentTransactionEntryModel createAuthorizationPaymentTransactionEntry(
			final WileyAuthorizationResponse authorizationResponse, final PaymentTransactionModel paymentTransaction)
	{

		ServicesUtil.validateParameterNotNull(authorizationResponse, "[authorizationResponse] can't be null.");
		ServicesUtil.validateParameterNotNull(paymentTransaction, "[paymentTransaction] can't be null.");

		PaymentTransactionEntryModel authorizationEntry = modelService.create(PaymentTransactionEntryModel.class);
		authorizationEntry.setType(PaymentTransactionType.AUTHORIZATION);
		authorizationEntry.setTransactionStatus(authorizationResponse.getStatus());
		authorizationEntry.setTransactionStatusDetails(authorizationResponse.getStatusDetails());
		authorizationEntry.setPaymentTransaction(paymentTransaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction,
				PaymentTransactionType.AUTHORIZATION);
		authorizationEntry.setCode(newEntryCode);
		authorizationEntry.setTime(authorizationResponse.getTransactionCreatedTime());

		authorizationEntry.setAmount(authorizationResponse.getTotalAmount());
		authorizationEntry.setRequestToken(authorizationResponse.getToken());
		authorizationEntry.setRequestId(authorizationResponse.getTransactionId());
		if (authorizationResponse.getCurrency() != null)
		{
			authorizationEntry.setCurrency(commonI18NService.getCurrency(authorizationResponse.getCurrency()));
		}
		modelService.saveAll(authorizationEntry, paymentTransaction);

		return authorizationEntry;
	}

	@Override
	public PaymentTransactionEntryModel createCaptureEntry(@Nonnull final WileyCaptureResponse captureResponse,
			@Nonnull final PaymentTransactionModel transaction)
	{
		ServicesUtil.validateParameterNotNull(captureResponse, "[captureResult] can't be null.");
		ServicesUtil.validateParameterNotNull(transaction, "[transaction] can't be null.");

		PaymentTransactionEntryModel captureEntry = modelService.create(PaymentTransactionEntryModel.class);
		captureEntry.setType(PaymentTransactionType.CAPTURE);
		captureEntry.setTransactionStatus(captureResponse.getStatus());
		captureEntry.setTransactionStatusDetails(captureResponse.getStatusDetails());
		captureEntry.setPaymentTransaction(transaction);
		captureEntry.setTnsMerchantId(wileyMPGSMerchantService.getMerchantID(transaction.getOrder()));
		String captureEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, PaymentTransactionType.CAPTURE);
		captureEntry.setCode(captureEntryCode);
		captureEntry.setAmount(captureResponse.getTransactionAmount());
		captureEntry.setRequestId(captureResponse.getTransactionId());
		captureEntry.setTime(captureResponse.getTransactionCreatedTime());

		if (captureResponse.getTransactionCurrency() != null)
		{
			captureEntry.setCurrency(commonI18NService.getCurrency(captureResponse.getTransactionCurrency()));
		}

		modelService.saveAll(transaction, captureEntry);

		return captureEntry;
	}

	@Override
	public PaymentTransactionEntryModel createFollowOnRefundEntry(final WileyFollowOnRefundResponse refundResponse,
			final PaymentTransactionModel paymentTransaction)
	{
		ServicesUtil.validateParameterNotNull(refundResponse, "[refundResponse] can't be null.");
		ServicesUtil.validateParameterNotNull(paymentTransaction, "[paymentTransaction] can't be null.");

		PaymentTransactionEntryModel followOnRefundEntry = modelService.create(PaymentTransactionEntryModel.class);

		followOnRefundEntry.setType(PaymentTransactionType.REFUND_FOLLOW_ON);
		followOnRefundEntry.setTransactionStatus(refundResponse.getStatus());
		followOnRefundEntry.setTransactionStatusDetails(refundResponse.getStatusDetails());
		followOnRefundEntry.setPaymentTransaction(paymentTransaction);
		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction,
				PaymentTransactionType.REFUND_FOLLOW_ON);
		followOnRefundEntry.setCode(newEntryCode);
		followOnRefundEntry.setAmount(refundResponse.getTotalAmount());
		followOnRefundEntry.setTime(refundResponse.getTransactionCreatedTime());
		if (refundResponse.getCurrency() != null)
		{
			followOnRefundEntry.setCurrency(commonI18NService.getCurrency(refundResponse.getCurrency().getCurrencyCode()));
		}
		followOnRefundEntry.setRequestId(refundResponse.getRequestId());
		modelService.saveAll(paymentTransaction, followOnRefundEntry);
		return followOnRefundEntry;
	}

	@Override
	public PaymentTransactionModel createPaymentTransaction(@Nonnull final AbstractOrderModel cart)
	{
		ServicesUtil.validateParameterNotNull(cart, "[cart] can't be null.");
		PaymentTransactionModel transaction = modelService.create(PaymentTransactionModel.class);
		transaction.setOrder(cart);
		transaction.setPaymentProvider(wileyMPGSPaymentProviderService.generatePaymentProvider());
		transaction.setCode(String.valueOf(UUID.randomUUID()));
		transaction.setInfo(cart.getPaymentInfo());
		transaction.setPlannedAmount(amountToPayCalculationStrategy.getOrderAmountToPay(cart));
		modelService.save(transaction);
		modelService.refresh(cart);

		return transaction;
	}
}
