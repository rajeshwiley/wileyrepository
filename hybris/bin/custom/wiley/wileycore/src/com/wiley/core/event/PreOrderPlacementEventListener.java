package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.wiley.core.constants.WileyCoreConstants;


/**
 * Listener for pre-order placement events
 */
public class PreOrderPlacementEventListener extends AbstractWileySiteEventListener<PreOrderPlacementEvent>
{
	/**
	 * On site event.
	 *
	 * @param preOrderPlacementEvent
	 * 		the preOrder placed event
	 */
	@Override
	protected void onSiteEvent(final PreOrderPlacementEvent preOrderPlacementEvent)
	{
		final OrderModel orderModel = preOrderPlacementEvent.getProcess().getOrder();
		final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
				WileyCoreConstants.PRE_ORDER_PLACEMENT_EMAIL_PROCESS + "-" + orderModel.getCode() + "-"
						+ System.currentTimeMillis(), WileyCoreConstants.PRE_ORDER_PLACEMENT_EMAIL_PROCESS);
		orderProcessModel.setOrder(orderModel);
		getModelService().save(orderProcessModel);
		getBusinessProcessService().startProcess(orderProcessModel);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final PreOrderPlacementEvent event)
	{
		final OrderModel order = event.getProcess().getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return true;
	}

}
