package com.wiley.core.order.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;


/**
 * Created by Raman_Hancharou on 3/30/2016.
 */
public interface WileyOrderEntryDao
{
	List<OrderEntryModel> findOrderEntriesByProductAndOwnerExcludingStatuses(String productCode, String ownerId,
			OrderStatus... orderStatuses);

	List<OrderEntryModel> findOrderEntryByGuid(String guid);

	SearchPageData<AbstractOrderEntryModel> findOrderEntriesByBusinessValues(BaseSiteModel baseSite,
		 UserModel user, String businessItemId, String businessKey,
		 PageableData pagination, List<OrderStatus> statuses);
}
