package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartRestorationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Bean for B2C speicific usage only
 */
public class Wileyb2cCommerceCartRestorationStrategy extends DefaultCommerceCartRestorationStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cCommerceCartRestorationStrategy.class);

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public CommerceCartRestoration restoreCart(final CommerceCartParameter parameter) throws CommerceCartRestorationException
	{
		final CartModel cartModel = parameter.getCart();
		final CommerceCartRestoration restoration = new CommerceCartRestoration();
		final List<CommerceCartModification> modifications = new ArrayList<>();
		if (cartModel != null)
		{
			if (baseStoreService.getCurrentBaseStore().equals(cartModel.getStore()))
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Restoring from cart " + cartModel.getCode() + ".");
				}
				if (isCartInValidityPeriod(cartModel))
				{
					cartModel.setCalculated(Boolean.FALSE);
					if (!cartModel.getPaymentTransactions().isEmpty())
					{
						// clear payment transactions
						clearPaymentTransactionsOnCart(cartModel);
						// reset guid since its used as a merchantId for payment subscriptions
						// and is a base id for generating PaymentTransaction.code
						// see de.hybris.platform.payment.impl.DefaultPaymentServiceImpl.authorize()
						cartModel.setGuid(getGuidKeyGenerator().generate().toString());
					}

					final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
					if (!currentBaseSite.equals(cartModel.getSite()))
					{
						cartModel.setSite(currentBaseSite); // actualize site.
					}

					getModelService().save(cartModel);
					try
					{
						getCommerceCartCalculationStrategy().recalculateCart(parameter);
					}
					catch (final IllegalStateException ex)
					{
						LOG.error("Failed to recalculate order [" + cartModel.getCode() + "]", ex);
					}

					getCartService().setSessionCart(cartModel);

					if (LOG.isDebugEnabled())
					{
						LOG.debug("Cart " + cartModel.getCode() + " was found to be valid and was restored to the session.");
					}
				}
				else
				{
					try
					{
						modifications.addAll(rebuildSessionCart(parameter));
					}
					catch (final CommerceCartModificationException e)
					{
						throw new CommerceCartRestorationException(e.getMessage(), e);
					}
				}
			}
			else
			{
				LOG.warn(String.format("Current Store %s does not equal to cart %s Store %s",
						baseStoreService.getCurrentBaseStore(), cartModel, cartModel.getStore()));
			}
		}
		restoration.setModifications(modifications);
		return restoration;
	}

}
