/**
 *
 */
package com.wiley.core.wiley.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.util.TaxValue;

import java.util.Collection;
import java.util.Collections;


/**
 * {@link FindTaxValuesStrategy} implementation that return empty taxes. Sgould be used in case external tax calculation
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileyVoidFindTaxValuesStrategy implements FindTaxValuesStrategy
{

	@Override
	public Collection<TaxValue> findTaxValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		// This is to avoid embedded tax system
		return Collections.<TaxValue> emptyList();
	}

}
