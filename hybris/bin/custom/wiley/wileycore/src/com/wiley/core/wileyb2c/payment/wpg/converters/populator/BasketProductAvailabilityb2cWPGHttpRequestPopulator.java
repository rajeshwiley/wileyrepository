package com.wiley.core.wileyb2c.payment.wpg.converters.populator;

import com.wiley.core.event.facade.payment.BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent;
import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

public class BasketProductAvailabilityb2cWPGHttpRequestPopulator
		extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
	implements ApplicationEventPublisherAware
{
	private ApplicationEventPublisher applicationEventPublisher;

	@Override
	public void populate(final CreateSubscriptionRequest source, final PaymentData target)
			throws ConversionException
	{
		BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent event =
				new BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent(source, target);
		applicationEventPublisher.publishEvent(event);
	}

	@Override
	public void setApplicationEventPublisher(final ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}
