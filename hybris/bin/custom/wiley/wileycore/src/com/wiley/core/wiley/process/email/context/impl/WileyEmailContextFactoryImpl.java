package com.wiley.core.wiley.process.email.context.impl;

import de.hybris.platform.acceleratorservices.process.email.context.impl.DefaultEmailContextFactory;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.store.BaseStoreModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.wiley.process.strategies.WileyProcessContextStoreResolutionStrategy;


/**
 * Extension of {@link com.wiley.core.wiley.process.email.context.impl.WileyEmailContextFactoryImpl} to resolve render template
 * by {@link de.hybris.platform.store.BaseStoreModel}
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileyEmailContextFactoryImpl extends DefaultEmailContextFactory
{
	static final String SEPARATOR = "-";
	static final String TEMPLATE_POSTFIX = "-template";
	@Resource
	private WileyProcessContextStoreResolutionStrategy wileyProcessContextStoreResolutionStrategy;

	@Override
	protected String resolveRendererTemplateForComponent(@Nonnull final AbstractCMSComponentModel component,
			@Nonnull final BusinessProcessModel businessProcessModel)
	{
		final BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcessModel);

		if (store == null)
		{
			final String message = "Cannot get BaseStore for business process code: "
					+ businessProcessModel.getCode() + ". " + businessProcessModel;
			throw new IllegalArgumentException(message);
		}

		final ComposedTypeModel componentType = getTypeService().getComposedTypeForClass(component.getClass());

		return store.getUid() + SEPARATOR + componentType.getCode() + TEMPLATE_POSTFIX;
	}
}
