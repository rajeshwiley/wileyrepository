package com.wiley.core.util.savedvalues.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemModelContext;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelContextUtils;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.wiley.core.util.savedvalues.ItemModificationHistoryService;
import com.wiley.core.util.savedvalues.ItemModificationInfo;


/**
 * Implementation is similar to ootb one. (removed typeFacade usages)
 *
 * @see com.hybris.backoffice.cockpitng.dataaccess.facades.object.savedvalues.DefaultItemModificationHistoryService
 */
public class WileyItemModificationHistoryServiceImpl implements ItemModificationHistoryService
{
	@Resource
	private ModelService modelService;
	@Resource
	private I18NService i18NService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public void logModifications(final ItemModificationInfo modificationInfo)
	{
		logModifications(modificationInfo.getModel(), modificationInfo);
	}

	@Override
	public ItemModificationInfo createModificationInfo(final ItemModel itemModel)
	{
		final ItemModificationInfo modificationInfo = new ItemModificationInfo(itemModel);
		final ModelValueHistory modelHistory = this.getModelValueHistory(itemModel);
		if (modelHistory != null)
		{
			boolean isNew = modelService.isNew(itemModel);
			if (modelHistory.isDirty() || isNew)
			{
				modificationInfo.setNew(isNew);

				// handle non-localized attributes
				Set<String> dirtyAttributes = modelHistory.getDirtyAttributes();
				for (String attribute : dirtyAttributes)
				{
					modificationInfo.addEntry(attribute, false, getOriginalValue(itemModel, attribute, modelHistory),
							modelService.getAttributeValue(itemModel, attribute));
				}

				// handle localized attributes
				Map<Locale, Set<String>> localizedDirtyAttributes = modelHistory.getDirtyLocalizedAttributes();
				Map<String, Set<Locale>> reversed = new HashMap();
				for (Map.Entry<Locale, Set<String>> localeEntry : localizedDirtyAttributes.entrySet())
				{
					for (String attribute : localeEntry.getValue())
					{
						Set<Locale> locales = reversed.get(attribute);
						if (locales == null)
						{
							locales = new HashSet(1);
						}
						locales.add(localeEntry.getKey());
						reversed.put(attribute, locales);
					}
				}

				for (Map.Entry<String, Set<Locale>> attributeEntry : reversed.entrySet())
				{
					final String attribute = attributeEntry.getKey();
					final Map<Locale, Object> originalValue = new HashMap((attributeEntry.getValue()).size());
					final Map<Locale, Object> modifiedValue = new HashMap((attributeEntry.getValue()).size());

					for (Locale locale : attributeEntry.getValue())
					{
						originalValue.put(locale, getOriginalValue(itemModel, attribute, locale, modelHistory));
						modifiedValue.put(locale, modelService.getAttributeValue(itemModel, attribute, locale));
					}
					modificationInfo.addEntry(attribute, true, originalValue, modifiedValue);
				}

			}
		}

		return modificationInfo;
	}

	private void logModifications(final ItemModel model, final ItemModificationInfo modificationInfo)
	{
		final Map<String, Object> originalValues = new HashMap();
		final Map<String, Object> modifiedValues = new HashMap();

		for (String attribute : modificationInfo.getModifiedAttributes())
		{
			originalValues.put(attribute,
					toPersistenceLayer(modificationInfo.getOriginalValue(attribute), modificationInfo.isLocalized(attribute)));
			modifiedValues.put(attribute,
					toPersistenceLayer(modificationInfo.getModifiedValue(attribute), modificationInfo.isLocalized(attribute)));
		}

		if (!modifiedValues.isEmpty())
		{
			if (modificationInfo.isNew())
			{
				JaloConnection.getInstance().logItemCreation(model.getPk(), modifiedValues);
			}
			else if (modelService.isRemoved(model))
			{
				JaloConnection.getInstance().logItemRemoval(model.getPk(), false);
			}
			else
			{
				JaloConnection.getInstance().logItemModification(model.getPk(), modifiedValues, originalValues, false);
			}
		}

	}

	private Object toPersistenceLayer(final Object value, boolean localized)
	{
		Object result = null;
		if (localized)
		{
			if (value instanceof Map)
			{
				Map<Locale, Object> localeBasedMap = (Map) value;
				Map<Language, Object> languageBasedMap = new HashMap();

				for (Map.Entry<Locale, Object> localeEntry : localeBasedMap.entrySet())
				{
					final Locale locale = localeEntry.getKey();
					final Object localeValue = localeEntry.getValue();
					final LanguageModel language = getLanguageForLocale(locale);
					languageBasedMap.put(modelService.getSource(language), modelService.toPersistenceLayer(localeValue));
				}

				result = languageBasedMap;
			}
		}
		else
		{
			result = modelService.toPersistenceLayer(value);
		}

		return result;
	}

	private LanguageModel getLanguageForLocale(final Locale locale)
	{
		LanguageModel language;
		try
		{
			language = commonI18NService.getLanguage(i18NService.getBestMatchingLocale(locale).getLanguage());
		}
		catch (UnknownIdentifierException var13)
		{
			language = commonI18NService.getLanguage(locale.toString());
		}
		return language;
	}


	private ModelValueHistory getModelValueHistory(final ItemModel itemModel)
	{
		ModelValueHistory modelHistory = null;
		final ItemModelContext itemModelContext = ModelContextUtils.getItemModelContext(itemModel);
		if (itemModelContext != null)
		{
			modelHistory = ((ItemModelContextImpl) itemModelContext).getValueHistory();
		}

		return modelHistory;
	}


	private Object getOriginalValue(final ItemModel model, final String attribute, final ModelValueHistory history)
	{
		Object value = null;
		if (!modelService.isNew(model) && history.isValueLoaded(attribute))
		{
			value = history.getOriginalValue(attribute);
		}

		return value;
	}

	private Object getOriginalValue(final ItemModel model, final String attribute, final Locale locale,
			final ModelValueHistory history)
	{
		Object value = null;
		if (!modelService.isNew(model) && history.isValueLoaded(attribute, locale))
		{
			value = history.getOriginalValue(attribute, locale);
		}

		return value;
	}
}
