package com.wiley.core.healthcheck.checks;

import de.hybris.platform.cluster.DefaultBroadcastService;
import de.hybris.platform.cluster.PingBroadcastHandler;
import de.hybris.platform.hac.data.dto.cluster.ClusterData;
import de.hybris.platform.hac.data.dto.cluster.NodeData;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;

import com.codahale.metrics.health.HealthCheck;


public class ClusterHealthCheck extends HealthCheck
{
	@Override
	protected Result check()
	{
		ClusterData nodesInfo = getNodesInfo();

		ResultBuilder builder = Result.builder();
		builder.healthy().withMessage("Cluster health report has been generated")
				.withDetail("cluster", nodesInfo)
				.withDetail("info", nodesInfo);
		return builder.build();
	}

	private ClusterData getNodesInfo()
	{
		PingBroadcastHandler pingHandler = PingBroadcastHandler.getInstance();
		DefaultBroadcastService broadcastService = DefaultBroadcastService.getInstance();
		ClusterData clusterData = new ClusterData();
		clusterData.setClusterEnabled(broadcastService.isClusteringEnabled());
		clusterData.setClusterIslandId(broadcastService.getClusterIslandPK());
		clusterData.setClusterNodeId((long) broadcastService.getClusterNodeID());
		clusterData.setDynamicClusterNodeId(broadcastService.getDynamicClusterNodeID());
		clusterData.setNodes(getNodesData(pingHandler.getNodes()));

		return clusterData;
	}

	private List<NodeData> getNodesData(final Collection<PingBroadcastHandler.NodeInfo> nodes)
	{
		return nodes.stream().map(nodeInfo -> {
			NodeData nodeData = new NodeData();
			nodeData.setNodeIP(nodeInfo.getIP());
			nodeData.setNodeID(nodeInfo.getNodeID());
			nodeData.setDynamicNodeID(nodeInfo.getDynamicNodeID());
			nodeData.setMethodName(StringEscapeUtils.escapeHtml(nodeInfo.getMethodName()));
			return nodeData;
		}).collect(Collectors.toList());
	}
}