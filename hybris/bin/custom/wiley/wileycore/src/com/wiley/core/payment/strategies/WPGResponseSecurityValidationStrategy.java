package com.wiley.core.payment.strategies;

import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;


public interface WPGResponseSecurityValidationStrategy
{
	boolean validate(WPGHttpValidateResultData resultData);
}
