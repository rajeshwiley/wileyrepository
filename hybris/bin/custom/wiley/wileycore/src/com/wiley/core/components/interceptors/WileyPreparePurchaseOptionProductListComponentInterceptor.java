package com.wiley.core.components.interceptors;

import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyPreparePurchaseOptionProductListComponentInterceptor
		implements PrepareInterceptor<WileyPurchaseOptionProductListComponentModel>
{
	@Resource
	private ModelService modelService;

	@Override
	public void onPrepare(final WileyPurchaseOptionProductListComponentModel wileyPurchaseOptionProductListComponentModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{
		if (wileyPurchaseOptionProductListComponentModel.getUg() == null)
		{
			UserDiscountGroup userDiscountGroup = UserDiscountGroup.valueOf(UUID.randomUUID().toString());
			modelService.save(userDiscountGroup);
			wileyPurchaseOptionProductListComponentModel.setUg(userDiscountGroup);
		}
		if (interceptorContext.isModified(wileyPurchaseOptionProductListComponentModel,
				WileyPurchaseOptionProductListComponentModel.ITEMS))
		{
			final List<WileyPurchaseOptionProductModel> items = wileyPurchaseOptionProductListComponentModel.getItems();
			final List<DiscountRowModel> discountRows = wileyPurchaseOptionProductListComponentModel.getDiscountRows();
			Map<String, DiscountRowModel> discountsByProductCode = discountRows.stream().collect(Collectors.toMap(
					discountRow -> discountRow.getProduct().getCode(), discountRow -> discountRow));
			if (CollectionUtils.isNotEmpty(items))
			{
				for (final WileyPurchaseOptionProductModel productModel : items)
				{
					final DiscountRowModel discountRowModel = discountsByProductCode.remove(productModel.getCode());
					if (discountRowModel == null)
					{
						DiscountRowModel newDiscountRow = modelService.create(DiscountRowModel.class);
						newDiscountRow.setDiscount(wileyPurchaseOptionProductListComponentModel.getDiscount());
						newDiscountRow.setUg(wileyPurchaseOptionProductListComponentModel.getUg());
						newDiscountRow.setProduct(productModel);
						modelService.save(newDiscountRow);
					}
				}
			}
			for (final DiscountRowModel discountRowModel : discountsByProductCode.values())
			{
				modelService.remove(discountRowModel);
			}
		}
		if (interceptorContext.isModified(wileyPurchaseOptionProductListComponentModel,
				WileyPurchaseOptionProductListComponentModel.DISCOUNT))
		{
			final List<DiscountRowModel> currentDiscountRowModels =
					wileyPurchaseOptionProductListComponentModel.getDiscountRows();
			final Collection<DiscountRowModel> newDiscountRowModelCollection = new ArrayList<>();
			for (final DiscountRowModel discountRowModel : currentDiscountRowModels)
			{
				DiscountRowModel newDiscountRow = modelService.create(DiscountRowModel.class);
				newDiscountRow.setDiscount(wileyPurchaseOptionProductListComponentModel.getDiscount());
				newDiscountRow.setUg(discountRowModel.getUg());
				newDiscountRow.setProduct(discountRowModel.getProduct());
				newDiscountRowModelCollection.add(newDiscountRow);
			}
			modelService.removeAll(currentDiscountRowModels);
			modelService.saveAll(newDiscountRowModelCollection);

		}
	}
}
