package com.wiley.core.wileyb2c.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.internal.service.AbstractBusinessService;
import de.hybris.platform.subscriptionservices.jalo.SubscriptionTerm;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.wileyb2c.jalo.Wileyb2cEurope1PriceFactory;


/**
 * This price service retrieves country specific prices for b2c
 */
public class Wileyb2cPriceServiceImpl extends AbstractBusinessService implements Wileyb2cPriceService
{
	@Resource
	private Wileyb2cEurope1PriceFactory wileyb2cEurope1PriceFactory;

	@Override
	public List<PriceInformation> getPriceInformationsForProduct(final ProductModel productModel)
	{
		SessionContext ctx = JaloSession.getCurrentSession().getSessionContext();
		final boolean isNetUser = wileyb2cEurope1PriceFactory.isNetUser(JaloSession.getCurrentSession().getUser());
		final Product productItem = getModelService().getSource(productModel);
		try
		{
			return wileyb2cEurope1PriceFactory.getProductPriceInformations(ctx, productItem, new Date(), isNetUser);
		}
		catch (JaloPriceFactoryException e)
		{
			throw new SystemException(e.getMessage(), e);
		}
	}

	@Override
	public List<PriceInformation> findPricesByProductAndSubscriptionTerm(final ProductModel productModel,
			final SubscriptionTermModel subscriptionTerm)
	{
		final List<PriceInformation> priceInfos = getPriceInformationsForProduct(productModel);
		final SubscriptionTerm subscriptionTermItem = getModelService().getSource(subscriptionTerm);
		return wileyb2cEurope1PriceFactory.filterPriceInfoBySubscriptionTerm(priceInfos, subscriptionTermItem);
	}
}
