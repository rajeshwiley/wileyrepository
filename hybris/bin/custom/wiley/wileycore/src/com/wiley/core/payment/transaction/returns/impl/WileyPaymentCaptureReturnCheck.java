package com.wiley.core.payment.transaction.returns.impl;


import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.payment.transaction.PaymentTransactionService;


public class WileyPaymentCaptureReturnCheck implements ReturnableCheck
{
	@Autowired
	private PaymentTransactionService paymentTransactionService;

	@Override
	public boolean perform(final OrderModel orderModel, final AbstractOrderEntryModel abstractOrderEntryModel,
			final long qty)
	{
		return paymentTransactionService.getAcceptedTransactionEntry(orderModel, PaymentTransactionType.CAPTURE) != null;
	}
}
