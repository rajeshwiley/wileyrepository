package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;


/**
 * Wileyb2b specific cart calculation.
 */
public class Wileyb2bCartCalculationService implements CalculationService
{

	@Resource
	private EsbCartCalculationGateway esbCartCalculationGateway;

	@Resource
	private ModelService modelService;

	@Resource
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;


	@Override
	@Transactional(rollbackFor = CalculationException.class)
	public void calculate(final AbstractOrderModel order) throws CalculationException
	{
		if (!(order instanceof CartModel))
		{
			throw new IllegalArgumentException("Method is implemented to handle only Cart.");
		}

		if (this.requiresCalculation(order))
		{
			recalculateInternal((CartModel) order);
		}
	}

	@Override
	public boolean requiresCalculation(final AbstractOrderModel order)
	{
		return orderRequiresCalculationStrategy.requiresCalculation(order);
	}

	@Override
	@Transactional(rollbackFor = CalculationException.class)
	public void calculate(final AbstractOrderModel order, final Date date) throws CalculationException
	{
		this.calculate(order);
	}

	@Override
	@Transactional(rollbackFor = CalculationException.class)
	public void calculateTotals(final AbstractOrderModel order, final boolean recalculate) throws CalculationException
	{
		if (recalculate)
		{
			this.recalculate(order);
		}
		else
		{
			this.calculate(order);
		}
	}

	@Override
	@Transactional(rollbackFor = CalculationException.class)
	public void recalculate(final AbstractOrderModel order) throws CalculationException
	{
		if (!(order instanceof CartModel))
		{
			throw new IllegalArgumentException("Method is implemented to handle only Cart.");
		}

		this.recalculateInternal((CartModel) order);
	}

	@Override
	@Transactional(rollbackFor = CalculationException.class)
	public void recalculate(final AbstractOrderModel order, final Date date) throws CalculationException
	{
		this.recalculate(order);
	}

	@Override
	public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate)
	{
		throw new UnsupportedOperationException("Method is not supported for this implementation.");
	}

	@Override
	public void recalculate(final AbstractOrderEntryModel entry) throws CalculationException
	{
		throw new UnsupportedOperationException("Method is not supported for this implementation.");
	}

	private void recalculateInternal(final CartModel cart) throws CalculationException
	{
		List<ExternalCartModification> cartModifications;
		try
		{
			cartModifications = esbCartCalculationGateway.verifyAndCalculateCart(cart);
		}
		catch (ExternalSystemException e)
		{
			throw new CalculationException(e.getMessage(), e);
		}

		/* we need to save the cart and entries twice, because interceptors recognize modified attributes and
		 * reset calculated flag.
		 * At first attempt, calculated flag is reset to false. Then we change calculated flag to true and save the cart again.
		 */
		saveAbstractOrderAndEntries(cart);
		setCalculatedStatus(cart);
		saveAbstractOrderAndEntries(cart);

		refreshAbstractOrderAndEntries(cart);

		externalCartModificationsStorageService.pushAll(cart, cartModifications);
	}

	private void refreshAbstractOrderAndEntries(final AbstractOrderModel abstractOrder)
	{
		modelService.refresh(abstractOrder);

		final List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			for (AbstractOrderEntryModel entry : entries)
			{
				modelService.refresh(entry);
			}
		}
	}

	private void setCalculatedStatus(final AbstractOrderModel abstractOrder)
	{
		final List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			entries.forEach(entry -> entry.setCalculated(true));
		}
		abstractOrder.setCalculated(true);
	}

	private void saveAbstractOrderAndEntries(final AbstractOrderModel abstractOrder)
	{
		final List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			/*
			 * in this case saveAll doesn't work, because ModelService use batch update and validate all entries before they
			 * are saved. So, An UniqueIndetiferException can occure when we have to entry in db (not updated yet):
			 * firstEntry with entryNumber = 1
			 * secondEntry with entryNumber = 2
			 *
			 * And two entries with normalized entryNumbers:
			 * firstEntry with entryNumber = 0
			 * secondEntry with entryNumber = 1
			 *
			 * And in this case, the validation exception occurs on entry with entryNumber = 1
			 */
			entries.forEach(entry -> modelService.save(entry));
		}
		modelService.save(abstractOrder);
	}
}
