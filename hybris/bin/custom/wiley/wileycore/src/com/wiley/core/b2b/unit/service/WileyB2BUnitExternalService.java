package com.wiley.core.b2b.unit.service;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.Collection;
import java.util.List;


public interface WileyB2BUnitExternalService
{
	/**
	 * @param sapAccountNumber
	 * 		Unique identifier of the customer's B2B unit (company).
	 * 		It is also known as Business Partner Number.
	 * @return B2BUnitModel or null if it's not found. The instance of B2BUnitModel is not attached to persistence context
	 */
	B2BUnitModel find(String sapAccountNumber, BaseSiteModel site);

	/**
	 * @param sapAccountNumber
	 * 		Unique identifier of the customer's B2B unit (company).
	 * 		It is also known as Business Partner Number.
	 * @param poNumbers collections of Purchase Order Numbers to be validated
	 * @return list of invalid PO numbers. In case ExternalSystemException returns empty list
	 */
	List<String> validatePONumbers(String sapAccountNumber, Collection<String> poNumbers);
}
