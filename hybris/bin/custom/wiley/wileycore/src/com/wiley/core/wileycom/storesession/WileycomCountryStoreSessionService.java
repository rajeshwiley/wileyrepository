package com.wiley.core.wileycom.storesession;

import de.hybris.platform.core.model.c2l.CountryModel;

import java.util.Optional;


/**
 * Wileycom store session service
 */
public interface WileycomCountryStoreSessionService
{
	//TODO: all method usages should be changed to WileycomStoreSessionService.setCurrentCountry. Current interface removed

	/**
	 * Update current country if valid ISO code provided and it doesn't match with current value.
	 * In case country change, cart country and currency are changed then cart is recalculated
	 * by state of shouldCartBeRecalculated param.
	 *
	 * @param isocode
	 * 		Country isocode.
	 * @param shouldCartBeRecalculated
	 * 		Control cart recalculation (true to force cart recalculation, false to stay cart not recalculated)
	 * @return Current country that is set to session. In case country was not updated, {@link Optional#EMPTY} is returned.
	 */
	Optional<CountryModel> updateCurrentCountryIfRequired(Optional<String> isocode, boolean shouldCartBeRecalculated);
}
