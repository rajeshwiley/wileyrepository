package com.wiley.core.wel.resolver.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;


/**
 * Changes url of the variant product to the base product url
 */
public class WelProductModelUrlResolver extends DefaultProductModelUrlResolver
{
	private static final String CATEGORY_PATH = "{category-path}";
	private static final String SUBCATEGORY_PATH = "{subcategory-path}";
	private static final String PRODUCT_CODE = "{product-code}";
	private static final String LEVEL = "LEVEL";
	private static final String PART = "PART";
	private static final String SUPPLEMENTS = "SUPPLEMENTS";
	private static final String SLASH = "/";


	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Override
	protected String resolveInternal(final ProductModel product)
	{
		String patternToUrl = getPattern();
		final CategoryModel primaryCategory = getProductLineCategory(product);
		patternToUrl = resolveCategoryPath(primaryCategory, patternToUrl);
		patternToUrl = resolveSubCategoryPath(product, primaryCategory, patternToUrl);
		patternToUrl = resolveProductName(product, patternToUrl);

		return patternToUrl.toLowerCase();
	}

	private CategoryModel getProductLineCategory(final ProductModel product)
	{
		CategoryModel primaryCategory = wileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		if (primaryCategory == null)
		{
			List<CategoryModel> categoryPaths = getCategoryPath(product);
			if (CollectionUtils.isNotEmpty(categoryPaths))
			{
				primaryCategory = categoryPaths.get(0);
			}
		}
		return primaryCategory;
	}

	private CategoryModel getPrimaryCategoryForCFAProduct(final ProductModel product)
	{
		CategoryModel primaryCategory = null;
		for (CategoryModel superCategory : product.getSupercategories()) {
			//if category belongs to CFA Level we need to consider to have highest priority among other to
			//support properly structured SEO url for CFA level products
			boolean isCFALevel = superCategory.getSupercategories()
					.stream()
					.anyMatch(wileyCategoryService::isCFALevelCategory);
			if (isCFALevel) {
				primaryCategory = superCategory;
			}
		}

		return primaryCategory != null ? primaryCategory : super.getPrimaryCategoryForProduct(product);
	}

	private String resolveCategoryPath(final CategoryModel primaryCategory, final String url)
	{
		String categoryPath = StringUtils.EMPTY;

		if (primaryCategory != null)
		{
			categoryPath = primaryCategory.getSeoLabel() != null ?
					primaryCategory.getSeoLabel() : primaryCategory.getName();
		}

		return url.replace(CATEGORY_PATH, urlSafe(categoryPath));
	}

	private String resolveSubCategoryPath(final ProductModel product, final CategoryModel primaryCategory, final String url)
	{
		String subcategoryPath = StringUtils.EMPTY;
		List<CategoryModel> categoryPath = getCategoryPath(primaryCategory, product);
		if (categoryPath.size() > 1 && wileyCategoryService.isCFACategory(primaryCategory))
		{
			CategoryModel lastSubCategory = categoryPath.get(categoryPath.size() - 1);
			if (isValidSubcategory(lastSubCategory))
			{
				subcategoryPath =
						lastSubCategory.getSeoLabel() != null ? lastSubCategory.getSeoLabel() : lastSubCategory.getName();
				subcategoryPath = urlSafe(subcategoryPath) + SLASH;
			}
		}
		return url.replace(SUBCATEGORY_PATH, subcategoryPath);
	}

	private List<CategoryModel> getCategoryPath(final CategoryModel primaryCategory, final ProductModel product)
	{
		List<CategoryModel> path;
		if (wileyCategoryService.isCFACategory(primaryCategory)) {
			path =  getCategoryPath(getPrimaryCategoryForCFAProduct(product));
		} else {
			path =  getCategoryPath(product);
		}
		return path;
	}

	private String resolveProductName(final ProductModel product, final String url)
	{
		String productName = product.getSeoLabel() != null ? product.getSeoLabel() : product.getName();
		return url.replace(PRODUCT_CODE, urlSafe(productName));
	}

	private boolean isValidSubcategory(final CategoryModel subcategory)
	{
		String code = subcategory.getCode();
		return code.contains(LEVEL) || code.contains(PART) || code.contains(SUPPLEMENTS);
	}
}