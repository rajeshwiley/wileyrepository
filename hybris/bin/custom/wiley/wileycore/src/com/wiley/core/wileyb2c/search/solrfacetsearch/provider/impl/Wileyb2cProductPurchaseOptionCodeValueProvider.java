package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 */
public class Wileyb2cProductPurchaseOptionCodeValueProvider extends AbstractWileycomValueProvider<String>
{
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		return Collections.singletonList(
				wileyb2cClassificationService.resolveClassificationClass((ProductModel) model).getCode());
	}

	@Required
	public void setWileyb2cClassificationService(
			final Wileyb2cClassificationService wileyb2cClassificationService)
	{
		this.wileyb2cClassificationService = wileyb2cClassificationService;
	}
}
