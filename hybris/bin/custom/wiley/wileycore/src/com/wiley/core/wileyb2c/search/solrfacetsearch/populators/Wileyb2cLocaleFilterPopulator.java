package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.locale.WileyLocaleService;

import static com.wiley.core.constants.WileyCoreConstants.SLASH;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cLocaleFilterPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_SORT_TYPE> implements
		Populator<SearchQueryPageableData<SolrSearchQueryData>,
				SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, IndexedType, IndexedProperty, SearchQuery, INDEXED_TYPE_SORT_TYPE>>
{
	static final String URL_INDEXED_PROPERTY_NAME = "url";
	private FieldNameProvider fieldNameProvider;

	@Resource
	private WileyLocaleService wileyLocaleService;

	@Override
	public void populate(
			final SearchQueryPageableData<SolrSearchQueryData> source,
			final SolrSearchRequest<FACET_SEARCH_CONFIG_TYPE, IndexedType, IndexedProperty, SearchQuery,
					INDEXED_TYPE_SORT_TYPE> target)
	{
		final IndexedType indexedType = target.getSearchQuery().getIndexedType();
		final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(URL_INDEXED_PROPERTY_NAME);
		final String translatedField = fieldNameProvider.getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);
		final QueryField localeQuery = new QueryField(translatedField,
				SLASH + wileyLocaleService.getCurrentEncodedLocale());
		localeQuery.setQueryOperator(SearchQuery.QueryOperator.CONTAINS);
		target.getSearchQuery().addFilterQuery(localeQuery);
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}
}
