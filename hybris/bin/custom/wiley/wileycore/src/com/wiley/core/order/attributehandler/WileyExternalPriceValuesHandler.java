package com.wiley.core.order.attributehandler;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.util.PriceValue;

import java.util.Collections;
import java.util.List;

/**
 * Handler for {@link AbstractOrderEntryModel#EXTERNALPRICE} attribute
 */
public class WileyExternalPriceValuesHandler
		implements DynamicAttributeHandler<List<PriceValue>, AbstractOrderEntryModel>
{

	@Override
	public List<PriceValue> get(final AbstractOrderEntryModel model)
	{
		final String values = model.getExternalPriceValuesInternal();
		final List<PriceValue> priceValuesList = (List<PriceValue>) PriceValue.parsePriceValueCollection(values);
		return priceValuesList != null ? priceValuesList : Collections.EMPTY_LIST;
	}

	@Override
	public void set(final AbstractOrderEntryModel model, final List<PriceValue> priceValues)
	{
		final String newValue = PriceValue.toString(priceValues);
		model.setExternalPriceValuesInternal(newValue);
		model.setCalculated(Boolean.FALSE);
		if (model.getOrder() != null)
		{
			model.getOrder().setCalculated(Boolean.FALSE);
		}
	}
}
