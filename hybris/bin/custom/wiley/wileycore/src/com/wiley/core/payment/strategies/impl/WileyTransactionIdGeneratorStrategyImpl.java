package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import javax.annotation.Resource;

import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;


public class WileyTransactionIdGeneratorStrategyImpl implements WileyTransactionIdGeneratorStrategy
{

	@Resource(name = "wpgTransactionIdGenerator")
	private KeyGenerator keyGenerator;

	@Override
	public String generateTransactionId()
	{
		final Object generatedValue = keyGenerator.generate();
		return String.valueOf(generatedValue);
	}

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	public void setKeyGenerator(final KeyGenerator keyGenerator)
	{
		this.keyGenerator = keyGenerator;
	}
}
