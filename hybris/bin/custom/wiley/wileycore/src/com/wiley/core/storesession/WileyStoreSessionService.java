package com.wiley.core.storesession;

import de.hybris.platform.commerceservices.storesession.StoreSessionService;
import de.hybris.platform.core.model.order.CartModel;


/**
 * Wiley store session service extends default store session service by additional logic during currency change
 */
public interface WileyStoreSessionService extends StoreSessionService
{

	/**
	 * Set currenct currency to session and optionally calculate cart.
	 *
	 * @param isocode
	 * 		Currency isocode
	 * @param recalculateCart
	 * 		If true then cart calculation is executed at the end, otherwise {@link CartModel#CALCULATED} is set to false
	 */
	void setCurrentCurrency(String isocode, boolean recalculateCart);


}
