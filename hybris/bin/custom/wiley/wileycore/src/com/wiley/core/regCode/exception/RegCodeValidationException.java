package com.wiley.core.regCode.exception;

public class RegCodeValidationException extends AbstractRegCodeOperationException
{
	public RegCodeValidationException(final String message)
	{
		super(message);
	}

	public RegCodeValidationException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

}
