package com.wiley.core.mpgs.dto.verify;


import com.wiley.core.mpgs.dto.json.Billing;
import com.wiley.core.mpgs.dto.json.Order;
import com.wiley.core.mpgs.dto.json.Session;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;


public class MPGSVerifyRequestDTO
{
	private String apiOperation;
	private SourceOfFunds sourceOfFunds;
	private Session session;
	private Billing billing;
	private Order order;

	public Order getOrder()
	{
		return order;
	}

	public void setOrder(final Order order)
	{
		this.order = order;
	}

	public String getApiOperation()
	{
		return apiOperation;
	}

	public void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public Session getSession()
	{
		return session;
	}

	public void setSession(final Session session)
	{
		this.session = session;
	}

	public Billing getBilling()
	{
		return billing;
	}

	public void setBilling(final Billing billing)
	{
		this.billing = billing;
	}
}
