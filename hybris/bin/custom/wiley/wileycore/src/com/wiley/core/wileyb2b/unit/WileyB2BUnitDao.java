package com.wiley.core.wileyb2b.unit;

import de.hybris.platform.b2b.model.B2BUnitModel;

import java.util.List;


public interface WileyB2BUnitDao
{
	List<B2BUnitModel> findB2BUnitBySapAccountNumber(String sapAccountNumber);
}
