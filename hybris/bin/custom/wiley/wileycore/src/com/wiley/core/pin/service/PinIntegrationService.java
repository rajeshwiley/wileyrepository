package com.wiley.core.pin.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;



/**
 * Integration Service to communicate with Wiley Core external system for Pin activation.
 */
public interface PinIntegrationService
{
	/**
	 * Check if Order was placed by PIN activation flow
	 * @param orderModel
	 * 		the Order in subject
	 * @return
	 * 		true if it was placed by activating a PIN
	 * 		false otherwise
	 */
	boolean isPinUsedForOrderPlacement(AbstractOrderModel orderModel);

	/**
	 * Validate the order's PIN in Wiley Core external system
	 * @param orderModel
	 * 		the order
	 * @return
	 * 		true if validation was successful
	 * 		false otherwise
	 */
	boolean validatePinForOrder(AbstractOrderModel orderModel);

	/**
	 * Activate the order's PIN in Wiley Core external system
	 * @param orderModel
	 * 		the order
	 * @return
	 * 		true if it could be activated
	 * 		false otherwise
	 */
	boolean activatePinForOrder(AbstractOrderModel orderModel);
}
