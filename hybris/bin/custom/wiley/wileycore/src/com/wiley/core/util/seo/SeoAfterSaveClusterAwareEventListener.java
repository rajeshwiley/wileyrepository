package com.wiley.core.util.seo;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class SeoAfterSaveClusterAwareEventListener extends AbstractEventListener<SeoAfterSaveClusterAwareEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(SeoAfterSaveClusterAwareEventListener.class);

	@Autowired
	private SeoUrlMappingManager seoUrlMappingManager;

	@Override
	protected void onEvent(final SeoAfterSaveClusterAwareEvent seoAfterSaveClusterAwareEvent)
	{
		LOG.debug("Setting reinitialization SEO information flag to invalidate seo urls on page load.");
		seoUrlMappingManager.reinitializeMapping();
	}
}
