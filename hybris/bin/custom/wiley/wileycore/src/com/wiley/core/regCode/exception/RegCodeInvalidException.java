package com.wiley.core.regCode.exception;

/**
 * RegCode operation exception.
 */
public class RegCodeInvalidException extends AbstractRegCodeOperationException
{
	public RegCodeInvalidException(final String message)
	{
		super(message);
	}

	public RegCodeInvalidException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
