package com.wiley.core.product.dao;

import java.util.List;

import com.wiley.core.model.WileyFreeTrialProductModel;


/**
 * Created by Aliaksei_Zlobich on 3/16/2016.
 */
public interface WileyFreeTrialProductDao
{

	List<WileyFreeTrialProductModel> findFreeTrialProductsByCode(String code);

}
