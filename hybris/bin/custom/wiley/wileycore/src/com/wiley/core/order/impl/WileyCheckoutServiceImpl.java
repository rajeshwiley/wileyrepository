package com.wiley.core.order.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.paypal.hybris.constants.PaypalConstants;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.dao.WileyOrderEntryDao;
import com.wiley.core.product.WileyProductEditionFormatService;

import static de.hybris.platform.core.enums.OrderStatus.CANCELLED;
import static de.hybris.platform.core.enums.OrderStatus.FAILED;


/**
 * Created by Uladzimir_Barouski on 3/2/2016.
 */
public class WileyCheckoutServiceImpl implements WileyCheckoutService
{
	@Resource
	private WileyOrderEntryDao wileyOrderEntryDao;
	@Resource
	private UserService userService;
	@Resource
	private CartService cartService;
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	@Nonnull
	public boolean isStudentOrder(@Nonnull final AbstractOrderModel abstractOrderModel)
	{
		Assert.notNull(abstractOrderModel);
		return UserDiscountGroup.STUDENT.equals(abstractOrderModel.getDiscountGroup());
	}

	@Override
	@Nonnull
	public boolean isCartHasStudentVerification(@Nonnull final AbstractOrderModel abstractOrderModel)
	{
		Assert.notNull(abstractOrderModel);
		return !StringUtils.isEmpty(abstractOrderModel.getUniversity());
	}

	@Override
	@Nonnull
	public boolean isOrderWithoutShipping(@Nonnull final AbstractOrderModel abstractOrderModel)
	{
		Assert.notNull(abstractOrderModel);
		//digital cart has no products to be shipped
		return wileyProductEditionFormatService.isDigitalCart(abstractOrderModel);
	}

	@Override
	@Nonnull
	public boolean isNonZeroOrder(@Nonnull final AbstractOrderModel orderModel)
	{
		Assert.notNull(orderModel);
		return orderModel.getTotalPrice() != null && orderModel.getTotalPrice() > 0.0D;
	}

	@Override
	public boolean isDigitalCart(@Nonnull final AbstractOrderModel abstractOrderModel)
	{
		Assert.notNull(abstractOrderModel);
		return wileyProductEditionFormatService.isDigitalCart(abstractOrderModel);
	}

	/**
	 * Checks if current user has already have order with passed free trial product code
	 *
	 * @param freeTrialCode
	 * @return boolean
	 */
	@Override
	public boolean isFreeTrialWasOrderedByCurrentUser(@Nonnull final String freeTrialCode)
	{
		Assert.notNull(freeTrialCode);
		UserModel userModel = userService.getCurrentUser();
		Assert.notNull(userModel);
		final OrderStatus[] orderStatusesToBeExcluded = { CANCELLED, FAILED };
		List<OrderEntryModel> orderEntries = wileyOrderEntryDao.findOrderEntriesByProductAndOwnerExcludingStatuses(freeTrialCode,
				userModel.getUid(), orderStatusesToBeExcluded);
		return !orderEntries.isEmpty();
	}


	@Override
	public boolean isPaypalCheckout()
	{
		final PaymentInfoModel paymentInfo = cartService.getSessionCart().getPaymentInfo();
		return paymentInfo != null && PaypalConstants.PAYPAL_PAYMENT_INFO_CODE.equals(paymentInfo.getCode());
	}

	@Override
	public boolean isTaxAvailable(final AbstractOrderModel abstractOrderModel)
	{
		return abstractOrderModel.getTaxCalculated() || StringUtils.isNotBlank(abstractOrderModel.getTaxNumber())
				|| !isNonZeroOrder(abstractOrderModel);
	}
}
