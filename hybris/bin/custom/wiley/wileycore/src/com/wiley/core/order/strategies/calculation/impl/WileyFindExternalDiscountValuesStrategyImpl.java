package com.wiley.core.order.strategies.calculation.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.converter.WileyExternalDiscountConverter;
import com.wiley.core.model.WileyExternalDiscountModel;

import static org.apache.commons.lang.BooleanUtils.isFalse;


public class WileyFindExternalDiscountValuesStrategyImpl implements FindDiscountValuesStrategy
{

	@Resource
	private WileyExternalDiscountConverter wileyExternalDiscountConverter;

	@Override
	public List<DiscountValue> findDiscountValues(final AbstractOrderEntryModel entry)
	{
		return filterExternalDiscounts(entry.getExternalDiscounts(), entry.getOrder().getCurrency().getIsocode())
				.stream()
				.map(wileyExternalDiscountConverter::convert)
				.collect(Collectors.toList());
	}

	@Override
	public List<DiscountValue> findDiscountValues(final AbstractOrderModel order)
	{
		return filterExternalDiscounts(order.getExternalDiscounts(), order.getCurrency().getIsocode())
				.stream()
				.map(wileyExternalDiscountConverter::convert)
				.collect(Collectors.toList());
	}

	private List<WileyExternalDiscountModel> filterExternalDiscounts(final List<WileyExternalDiscountModel> externalDiscounts,
			final String currency)
	{
		final List<WileyExternalDiscountModel> result = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			List<WileyExternalDiscountModel> externalDiscountsInCartCurrency = externalDiscounts.stream()
					.filter(d -> isFalse(d.getAbsolute()) || Objects.equals(getCurrencyIsoCode(d), currency))
					.collect(Collectors.toList());
			result.addAll(externalDiscountsInCartCurrency);
		}
		return result;
	}

	private String getCurrencyIsoCode(final WileyExternalDiscountModel externalDiscount)
	{
		String currencyIsoCode = null;
		if (externalDiscount.getCurrency() != null)
		{
			currencyIsoCode = externalDiscount.getCurrency().getIsocode();
		}
		return currencyIsoCode;
	}
}
