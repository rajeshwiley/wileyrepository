package com.wiley.core.wileyas.job;

import com.wiley.core.cart.WileyCommerceCartDao;
import de.hybris.platform.acceleratorservices.cronjob.CartRemovalJob;
import de.hybris.platform.acceleratorservices.model.CartRemovalCronJobModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.event.WileyOrderEntryEvent;


public class WileyasCartRemovalJob extends CartRemovalJob
{
	private static final Logger LOG = Logger.getLogger(WileyasCartRemovalJob.class);

	private static final String FREQUENCY_AGE = "as.abandoned.cronjob.frequency.removal.carts.in.seconds";
    private static final String FREQUENCY_ANONYMOUS_AGE =
            "as.abandoned.cronjob.frequency.removal.anonymous.carts.in.seconds";

	@Resource
	private EventService eventService;

	@Resource
	private TransactionTemplate transactionTemplate;

	private WileyCommerceCartDao wileyCommerceCartDao;

	@Override
	public PerformResult perform(final CartRemovalCronJobModel job)
	{
		//OOTB implementation with 2 alterations
		try
		{
			for (final BaseSiteModel site : job.getSites())
			{
				int age = getConfigurationService().getConfiguration().getInt(FREQUENCY_AGE);

				if (site.getCartRemovalAge() != null)
				{
					age = site.getCartRemovalAge();
				}
				for (final CartModel oldCart : getWileyCommerceCartDao().getAbandonCarts(
						new DateTime(getTimeService().getCurrentTime())
								.minusSeconds(age).toDate(), site))
				{
					//Publish a event for each cart entry then remove the cart
					removeCartAndPublishCartEntryRemovalEvent(oldCart);
				}

				age = getConfigurationService().getConfiguration().getInt(FREQUENCY_ANONYMOUS_AGE);

				if (site.getAnonymousCartRemovalAge() != null)
				{
					age = site.getAnonymousCartRemovalAge();
				}

				for (final CartModel oldCart : getCommerceCartDao().getCartsForRemovalForSiteAndUser(
						new DateTime(getTimeService().getCurrentTime()).minusSeconds(age).toDate(), site,
						getUserService().getAnonymousUser()))
				{
					//Same here. Publish a event for each cart entry then remove the cart
					removeCartAndPublishCartEntryRemovalEvent(oldCart);
				}

				for (final CartModel cartToFlag : getSaveCartDao().getSavedCartsForRemovalForSite(site))
				{
					final CommerceSaveCartParameter parameters = new CommerceSaveCartParameter();
					parameters.setCart(cartToFlag);
					parameters.setEnableHooks(getConfigurationService().getConfiguration().getBoolean(
							CommerceServicesConstants.FLAGFORDELETIONHOOK_ENABLED, true));

					getCommerceSaveCartService().flagForDeletion(parameters);
				}
			}

			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during cart cleanup", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}

	private void removeCartAndPublishCartEntryRemovalEvent(final CartModel oldCart)
	{
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				oldCart.getEntries().forEach(entry -> {
					WileyOrderEntryEvent wileyOrderEntryEvent = new WileyOrderEntryEvent("DELETED", entry);
					eventService.publishEvent(wileyOrderEntryEvent);
				});

				getModelService().remove(oldCart);
			}
		});
	}

	public WileyCommerceCartDao getWileyCommerceCartDao()
	{
		return wileyCommerceCartDao;
	}

	@Required
	public void setWileyCommerceCartDao(final WileyCommerceCartDao wileyCommerceCartDao)
	{
		this.wileyCommerceCartDao = wileyCommerceCartDao;
	}
}
