package com.wiley.core.integration.address;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.service.WileyasAddressVerificationService;



/**
 * Don't use gateway directly.
 * Use {@link WileyasAddressVerificationService} instead of it
 *
 * @author Yury Bich
 */
public interface WileyAddressesGateway
{
	@Nonnull
	List<AddressDto> getSuggestions(@Nonnull AddressDto addressData);
}
