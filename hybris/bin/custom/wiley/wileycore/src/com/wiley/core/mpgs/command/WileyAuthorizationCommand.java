package com.wiley.core.mpgs.command;


import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;


public interface WileyAuthorizationCommand extends WileyCommand<WileyAuthorizationRequest, WileyAuthorizationResponse>
{
}
