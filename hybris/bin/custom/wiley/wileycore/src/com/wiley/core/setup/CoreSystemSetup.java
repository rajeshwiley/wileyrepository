/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = WileyCoreConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup
{
	/**
	 * The constant IMPORT_ACCESS_RIGHTS.
	 */
	@Resource
	private ValidationService validationService;

	/**
	 * This method will be called by system creator during initialization and system update. Be sure that this method can
	 * be called repeatedly.
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData()
	{
		// please keep "itemtype.impex" first in list, as it corrects data-model
		importImpexFile("/wileycore/import/common/itemtype.impex");
		importImpexFile("/wileycore/import/common/languages.impex");
		importImpexFile("/wileycore/import/common/essential-data.impex");
		importImpexFile("/wileycore/import/common/worldregion.impex");
		importImpexFile("/wileycore/import/common/countries.impex");
		importImpexFile("/wileycore/import/common/constraints.impex");
		importImpexFile("/wileycore/import/common/b2c-constraints.impex");
		importImpexFile("/wileycore/import/common/script-templates.impex");
		importImpexFile("/wileycore/import/cockpits/user-groups.impex");
		validationService.reloadValidationEngine();
	}

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 *
	 * @return the initialization options
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		return Collections.emptyList();
	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 * 		the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.INIT)
	public void createProjectData(final SystemSetupContext context)
	{
		importImpexFile("/wileycore/import/cockpits/cmscockpit/cmscockpit-users.impex");
		importImpexFile("/wileycore/import/cockpits/cmscockpit/cmscockpit-access-rights.impex");
		importImpexFile("/wileycore/import/cockpits/productcockpit/productcockpit-access-rights.impex");
		importImpexFile("/wileycore/import/cockpits/productcockpit/productcockpit-constraints.impex");
		importImpexFile("/wileycore/import/cockpits/reportcockpit/reportcockpit-users.impex");
		importImpexFile("/wileycore/import/cockpits/reportcockpit/reportcockpit-access-rights.impex");
		importImpexFile("/wileycore/import/common/promotionenginesetup.impex");
		importImpexFile("/wileycore/import/common/solr.impex");
	}

	/**
	 * Gets extension names.
	 *
	 * @return the extension names
	 */
	private List<String> getExtensionNames()
	{
		return Registry.getCurrentTenant().getTenantSpecificExtensionNames();
	}

	private void importImpexFile(final String file)
	{
		getSetupImpexService().importImpexFile(file, true);
	}
}
