package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.impl.DefaultIndexerService;
import de.hybris.platform.solrfacetsearch.indexer.strategies.IndexerStrategy;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.WileyIndexerService;
import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.strategies.WileyIndexerStrategy;
import com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl.Wileyb2cCountryAndCurrencyQualifierProvider;


/**
 * This class was customized due to it's mandatory to run delete and update together in our system because both
 * them check last modification time in indexer queries.
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexerServiceImpl extends DefaultIndexerService implements WileyIndexerService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyIndexerServiceImpl.class);

	@Resource
	private SessionService sessionService;

	@Override
	public void updateIndex(final FacetSearchConfig facetSearchConfig, final Map<String, String> indexerHints)
			throws IndexerException
	{
		super.deleteFromIndex(facetSearchConfig, indexerHints);
		super.updateIndex(facetSearchConfig, indexerHints);
	}

	@Override
	public void updatePartialTypeIndex(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
			final Collection<IndexedProperty> indexedProperties, final List<PK> pks, final Set<CountryModel> countries)
			throws IndexerException
	{
		IndexerStrategy indexerStrategy = this.createIndexerStrategy(facetSearchConfig);
		indexerStrategy.setIndexOperation(IndexOperation.PARTIAL_UPDATE);
		indexerStrategy.setFacetSearchConfig(facetSearchConfig);
		indexerStrategy.setIndexedType(indexedType);
		indexerStrategy.setIndexedProperties(indexedProperties);
		indexerStrategy.setPks(pks);
		indexerStrategy.setIndexerHints(Collections.EMPTY_MAP);
		WileyIndexerStrategy wileyIndexerStrategy = (WileyIndexerStrategy) indexerStrategy;
		sessionService.setAttribute(Wileyb2cCountryAndCurrencyQualifierProvider.COUNTRIES, countries);
		wileyIndexerStrategy.execute();
	}


	@Override
	public void restoreIndex(final FacetSearchConfig facetSearchConfig, final Map<String, String> indexerHints)
			throws IndexerException
	{
		IndexConfig indexConfig = facetSearchConfig.getIndexConfig();

		for (IndexedType indexedType : indexConfig.getIndexedTypes().values())
		{
			LOG.info("Running solr index for IndexOperation RESTORE_DELETE, indexedType: [{}]", indexedType.getCode());
			createIndexerStrategy(facetSearchConfig, indexerHints, indexedType, IndexOperation.RESTORE_DELETE);
			LOG.info("Running solr index for IndexOperation RESTORE, indexedType: [{}]", indexedType.getCode());
			createIndexerStrategy(facetSearchConfig, indexerHints, indexedType, IndexOperation.RESTORE);
		}

	}

	private void createIndexerStrategy(final FacetSearchConfig facetSearchConfig, final Map<String, String> indexerHints,
			final IndexedType indexedType, final IndexOperation indexOperation) throws IndexerException
	{
		IndexerStrategy indexerStrategy = createIndexerStrategy(facetSearchConfig);
		indexerStrategy.setIndexOperation(indexOperation);
		indexerStrategy.setFacetSearchConfig(facetSearchConfig);
		indexerStrategy.setIndexedType(indexedType);
		indexerStrategy.setIndexerHints(indexerHints);
		indexerStrategy.execute();
	}
}
