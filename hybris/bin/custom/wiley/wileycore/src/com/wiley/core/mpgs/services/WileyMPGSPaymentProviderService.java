package com.wiley.core.mpgs.services;



public interface WileyMPGSPaymentProviderService
{
	String generatePaymentProvider();

	boolean isMPGSProviderGroup(String provider);
}
