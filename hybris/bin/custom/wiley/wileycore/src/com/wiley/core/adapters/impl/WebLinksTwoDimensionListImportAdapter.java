package com.wiley.core.adapters.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.adapters.AbstractLinksTwoDimensionListImportAdapter;
import com.wiley.core.model.WileyWebLinkModel;


/**
 * Adapter implementation that import impex value to {@link ProductModel#setWebLinks(Collection)}.
 */
public class WebLinksTwoDimensionListImportAdapter extends AbstractLinksTwoDimensionListImportAdapter
{

	@Autowired
	private ModelService modelService;

	@Override
	public void saveModels(final ProductModel product, final List<WileyWebLinkModel> webLinks)
	{
		product.setWebLinks(webLinks);
		modelService.save(product);
	}
}
