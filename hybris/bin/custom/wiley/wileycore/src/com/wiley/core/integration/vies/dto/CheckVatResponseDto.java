package com.wiley.core.integration.vies.dto;

/**
 * Created by Sergiy_Mishkovets on 2/22/2018.
 */
public class CheckVatResponseDto
{
	private String countryCode;
	private String vatNumber;
	private String requestDate;
	private Boolean valid;
	private String name;
	private String address;

	public String getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(final String countryCode)
	{
		this.countryCode = countryCode;
	}

	public String getVatNumber()
	{
		return vatNumber;
	}

	public void setVatNumber(final String vatNumber)
	{
		this.vatNumber = vatNumber;
	}

	public String getRequestDate()
	{
		return requestDate;
	}

	public void setRequestDate(final String requestDate)
	{
		this.requestDate = requestDate;
	}

	public Boolean getValid()
	{
		return valid;
	}

	public void setValid(final Boolean valid)
	{
		this.valid = valid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(final String address)
	{
		this.address = address;
	}
}
