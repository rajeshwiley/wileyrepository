package com.wiley.core.refund;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.refund.OrderRefundEntry;

import java.math.BigDecimal;
import java.util.List;

public class OrderRefundRequest {

    private final BigDecimal refundAmount;
    private final OrderModel order;
    private final List<OrderRefundEntry> entriesToRefund;


    public OrderRefundRequest(
            final OrderModel order, final List<OrderRefundEntry> entriesToRefund, final BigDecimal refundAmount) {
        this.order = order;
        this.entriesToRefund = entriesToRefund;
        this.refundAmount = refundAmount;
    }

    public OrderModel getOrder() {
        return this.order;
    }

    public List<OrderRefundEntry> getEntriesToRefund() {
        return this.entriesToRefund;
    }

    public BigDecimal getRefundAmount() {
        return refundAmount;
    }
}
