package com.wiley.core.wileyb2c.product;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.util.PriceValue;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.price.impl.WileyCommercePriceServiceImpl;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class Wileyb2cCommercePriceServiceImpl extends WileyCommercePriceServiceImpl implements Wileyb2cCommercePriceService
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cCommercePriceServiceImpl.class);
	public static final boolean ACTIVE_ONLY = true;

	private Wileyb2cPriceService wileyb2cPriceService;

	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;

	@Resource
	private ProductReferenceService productReferenceService;

	@Override
	public List<PriceInformation> findPricesByProductAndSubscriptionTerm(final ProductModel productModel,
			final SubscriptionTermModel subscriptionTerm)
	{
		return wileyb2cPriceService.findPricesByProductAndSubscriptionTerm(productModel, subscriptionTerm);
	}

	@Override
	public boolean validateProductSubscriptionTermPrice(final WileyProductModel productModel,
			final SubscriptionTermModel subscriptionTerm)
	{
		final Collection<SubscriptionTermModel> productTerms = productModel.getSubscriptionTerms();
		if (CollectionUtils.isEmpty(productTerms))
		{
			return false;
		}

		boolean isValidTermForProduct =
				wileyb2cSubscriptionService.isValidSubscriptionTermForProduct(productModel, subscriptionTerm);
		if (!isValidTermForProduct)
		{
			return false;
		}

		LOG.debug("Getting price for product [{}] and subscriptionTerm [{}].", productModel.getCode(), subscriptionTerm.getId());
		final List<PriceInformation> priceInformations = findPricesByProductAndSubscriptionTerm(productModel, subscriptionTerm);

		return CollectionUtils.isNotEmpty(priceInformations);
	}

	@Override
	public PriceInformation getComponentsOriginalPrice(final ProductModel productModel)
	{
		PriceInformation sumPriceInfo = null;
		Collection<ProductReferenceModel> components = productReferenceService.getProductReferencesForSourceProduct(productModel,
				ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, ACTIVE_ONLY);
		String currencyIso = null;
		boolean isNetPrice = false;
		Double sumPrice = 0.0;
		for (ProductReferenceModel component : components)
		{
			PriceInformation componentPrice = getWebPriceForProduct(component.getTarget());
			if (componentPrice != null)
			{
				if (currencyIso == null)
				{
					currencyIso = componentPrice.getPriceValue().getCurrencyIso();
					isNetPrice = componentPrice.getPriceValue().isNet();
				}
				sumPrice += componentPrice.getPriceValue().getValue();
			}
		}
		if (sumPrice > 0)
		{
			sumPriceInfo = new PriceInformation(new PriceValue(currencyIso, sumPrice, isNetPrice));
		}
		return sumPriceInfo;
	}

	@Required
	public void setWileyb2cPriceService(final Wileyb2cPriceService wileyb2cPriceService)
	{
		this.wileyb2cPriceService = wileyb2cPriceService;
	}

	@Required
	public void setWileyb2cSubscriptionService(
			final Wileyb2cSubscriptionService wileyb2cSubscriptionService)
	{
		this.wileyb2cSubscriptionService = wileyb2cSubscriptionService;
	}
}
