package com.wiley.core.order.strategies.calculation;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.PriceValue;

import javax.annotation.Nullable;


/**
 * Strategy that focuses on resolving external {@link PriceValue} for the given {@link AbstractOrderEntryModel}.
 */
public interface WileyFindExternalPriceStrategy
{
	/**
	 * Resolves external subtotal price value for the given {@link AbstractOrderEntryModel}.
	 *
	 * @param entry
	 * 		{@link AbstractOrderEntryModel}
	 * @return {@link PriceValue} instance or null if no external price found
	 */
	@Nullable
	PriceValue findExternalPrice(AbstractOrderEntryModel entry);
}
