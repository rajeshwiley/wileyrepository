package com.wiley.core.payment.strategies.impl;


import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.ArrayUtils;

import com.google.common.base.Preconditions;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.store.WileyBaseStoreService;


public class WPGSecurityHashGeneratorStrategyImpl implements SecurityHashGeneratorStrategy
{
	private Map<String, char[]> vendorAccountPasswords;

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	@Override
	public String generateSecurityHash(final String siteId, final String counterMeasureBase)
	{
		char[] password = vendorAccountPasswords.get(wileyBaseStoreService.getBaseStoreUidForSite(siteId));
		Preconditions.checkArgument(password != null, "Account password is not configured for site '%s'", siteId);

		final char[] counterMeasureWithPassword = ArrayUtils.addAll(counterMeasureBase.toCharArray(), password);
		return DigestUtils.md5Hex(String.valueOf(counterMeasureWithPassword));
	}

	public void setVendorAccountPasswords(final Map<String, char[]> vendorAccountPasswords)
	{
		this.vendorAccountPasswords = vendorAccountPasswords;
	}
}
