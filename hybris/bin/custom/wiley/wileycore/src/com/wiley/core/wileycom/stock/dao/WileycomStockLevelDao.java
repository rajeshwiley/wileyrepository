package com.wiley.core.wileycom.stock.dao;

import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.impl.StockLevelDao;

import javax.validation.constraints.NotNull;


/**
 * Created by Georgii_Gavrysh on 7/27/2016.
 */
public interface WileycomStockLevelDao extends StockLevelDao
{
	/**
	 * remove all stock level records with sequenceId less that parameter
	 *
	 * @param warehouseCode
	 * 		filter parameter
	 * @param sequenceId
	 * @return number of removed records
	 */
	int removeObsoleteStockLevels(@NotNull String warehouseCode, @NotNull Long sequenceId);

	/**
	 * Find stockLevel by code.
	 *
	 * @param stockLevelCode
	 * 		the stock level code
	 * @return the stock level model
	 */
	StockLevelModel findStockLevelByCode(@NotNull String stockLevelCode);

}
