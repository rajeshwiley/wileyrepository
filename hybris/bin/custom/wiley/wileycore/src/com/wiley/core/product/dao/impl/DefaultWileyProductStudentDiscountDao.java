package com.wiley.core.product.dao.impl;

import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import com.wiley.core.product.dao.WileyProductStudentDiscountDao;

import javax.annotation.Resource;

public class DefaultWileyProductStudentDiscountDao implements WileyProductStudentDiscountDao
{
    static final String DISCOUNT_QUERY = "SELECT {d:pk} FROM {Discount AS d} WHERE {d:code} LIKE '20_percent'";

    @Resource
    private FlexibleSearchService flexibleSearchService;

    public void getStudentDiscount(final DiscountRowModel discountRowModel)
    {
        final SearchResult<DiscountModel> result = flexibleSearchService.search(DISCOUNT_QUERY);
        if (!result.getResult().isEmpty())
        {
            discountRowModel.setDiscount((DiscountModel) result.getResult().get(0));
        }
    }
}
