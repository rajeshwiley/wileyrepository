package com.wiley.core.cart.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.impl.CommerceCartFactory;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.cart.WileyCartFactory;
import com.wiley.core.enums.OrderType;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * B2C, B2B cart FACTORY
 * This class overridden to set b2c, b2b session country of the user to cart.
 */
public class WileyCartFactoryImpl extends CommerceCartFactory implements WileyCartFactory
{
    public static final Logger LOG = LoggerFactory.getLogger(WileyCartFactoryImpl.class);

    @Resource
    private WileycomI18NService wileycomI18NService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private WileyCommonI18NService wileyCommonI18NService;

    @Resource
    private UserService userService;

    @Resource
    private ModelService modelService;

    @Override
    public CartModel createCartWithType(final OrderType orderType) {
        final CartModel cart = createCartInternal();

        Assert.notNull(cart);

        cart.setStatus(OrderStatus.CREATED);
        cart.setOrderType(orderType);
        modelService.save(cart);
        return cart;
    }

    @Override
    public CartModel createCartInternal() {
        final CartModel cart = super.createCartInternal();
        setCartUnit(cart);
        final Optional<CountryModel> sessionCountry = wileycomI18NService.getCurrentCountry();
        if (sessionCountry.isPresent()) {
            cart.setCountry(sessionCountry.get());
            final CurrencyModel defaultCurrency = wileyCommonI18NService.getDefaultCurrency(sessionCountry.get());

            commonI18NService.setCurrentCurrency(defaultCurrency);
            LOG.debug("Set session country [{}] to cart for on cart creation and currency to [{}].",
                    sessionCountry.get().getIsocode(),
                    defaultCurrency.getIsocode());
        } else {
            final BaseSiteModel site = cart.getSite();
            LOG.error("Found empty session country on cart creation. Cart GUID: {}, base site: {}.", cart.getGuid(),
                    Objects.nonNull(site) ? site.getUid() : "null");
        }
        return cart;
    }

    private void setCartUnit(final CartModel cart) {
        final UserModel customer = userService.getCurrentUser();
        if (customer instanceof B2BCustomerModel) {
            cart.setUnit(((B2BCustomerModel) customer).getDefaultB2BUnit());
        }
    }

    @Override
    @Nonnull
    public CartModel createCart() {
        return createCartWithType(OrderType.GENERAL);
    }
}
