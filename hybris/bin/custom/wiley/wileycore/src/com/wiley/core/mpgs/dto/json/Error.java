package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Error
{
	private String cause;
	private String explanation;
	private String field;
	private String supportCode;
	private String validationType;

	public String getCause()
	{
		return cause;
	}

	public void setCause(final String cause)
	{
		this.cause = cause;
	}

	public String getExplanation()
	{
		return explanation;
	}

	public void setExplanation(final String explanation)
	{
		this.explanation = explanation;
	}

	public String getField()
	{
		return field;
	}

	public void setField(final String field)
	{
		this.field = field;
	}

	public String getSupportCode()
	{
		return supportCode;
	}

	public void setSupportCode(final String supportCode)
	{
		this.supportCode = supportCode;
	}

	public String getValidationType()
	{
		return validationType;
	}

	public void setValidationType(final String validationType)
	{
		this.validationType = validationType;
	}
}
