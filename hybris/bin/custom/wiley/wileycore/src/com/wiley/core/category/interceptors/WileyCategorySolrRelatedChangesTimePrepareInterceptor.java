package com.wiley.core.category.interceptors;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;


/**
 * Created by Maksim_Kozich on 8/22/2017.
 */
public class WileyCategorySolrRelatedChangesTimePrepareInterceptor implements PrepareInterceptor<CategoryModel>
{
	private Collection<String> solrCategoryRelatedProperties;
	private Collection<String> solrClassificationClassRelatedProperties;

	/**
	 * Marks all descendant products as changed to let them be picked up by index update job.
	 * For classification classes marks only one level child products, as only fields from classification class itself
	 * are stored in Solr, no hierarchical fields like allCategories and categoryPath.
	 * Otherwise updating some solr related field in root classification class will lead to
	 * reindex of bulk number of products even though no index field will be actually changed.
	 */
	@Override
	public void onPrepare(@Nonnull final CategoryModel categoryModel, @Nonnull final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("categoryModel", categoryModel);
		ServicesUtil.validateParameterNotNullStandardMessage("interceptorContext", interceptorContext);

		if (isAnyCategorySolrRelatedPropertiesModified(categoryModel, interceptorContext)
				|| isAnyClassificationClassSolrRelatedPropertiesModified(categoryModel, interceptorContext))
		{
			if (categoryModel instanceof ClassificationClassModel)
			{
				markAllCategoryChildProductsRequireReindex(categoryModel, interceptorContext);
			}
			else
			{
				markAllCategoryDescendantProductsRequireReindex(categoryModel, interceptorContext);
			}
		}
	}

	private void markAllCategoryDescendantProductsRequireReindex(final CategoryModel categoryModel,
			final InterceptorContext interceptorContext)
	{
		markAllCategoryChildProductsRequireReindex(categoryModel, interceptorContext);
		Collection<CategoryModel> allSubcategories = categoryModel.getAllSubcategories();
		if (CollectionUtils.isNotEmpty(allSubcategories))
		{
			allSubcategories.forEach(
					subCategoryModel -> markAllCategoryDescendantProductsRequireReindex(subCategoryModel, interceptorContext));
		}
	}

	private void markAllCategoryChildProductsRequireReindex(final CategoryModel categoryModel,
			final InterceptorContext interceptorContext)
	{
		List<ProductModel> products = categoryModel.getProducts();
		if (CollectionUtils.isNotEmpty(products))
		{
			products.forEach(
					productModel ->
					{
						productModel.setModifiedtime(getCurrentTime());
						interceptorContext.registerElementFor(productModel, PersistenceOperation.SAVE);
					}
			);
		}
	}

	protected Date getCurrentTime()
	{
		return new Date();
	}

	private boolean isAnyCategorySolrRelatedPropertiesModified(final CategoryModel categoryModel,
			final InterceptorContext interceptorContext)
	{
		return getSolrCategoryRelatedProperties().stream().anyMatch(
				solrRelatedProperty -> interceptorContext.isModified(categoryModel, solrRelatedProperty));
	}

	private boolean isAnyClassificationClassSolrRelatedPropertiesModified(final CategoryModel categoryModel,
			final InterceptorContext interceptorContext)
	{
		return (categoryModel instanceof ClassificationClassModel && getSolrClassificationClassRelatedProperties().stream()
				.anyMatch(
						solrRelatedProperty -> interceptorContext.isModified(categoryModel, solrRelatedProperty)));
	}

	public Collection<String> getSolrCategoryRelatedProperties()
	{
		return solrCategoryRelatedProperties;
	}

	public void setSolrCategoryRelatedProperties(final Collection<String> solrCategoryRelatedProperties)
	{
		this.solrCategoryRelatedProperties = solrCategoryRelatedProperties;
	}

	public Collection<String> getSolrClassificationClassRelatedProperties()
	{
		return solrClassificationClassRelatedProperties;
	}

	public void setSolrClassificationClassRelatedProperties(final Collection<String> solrClassificationClassRelatedProperties)
	{
		this.solrClassificationClassRelatedProperties = solrClassificationClassRelatedProperties;
	}
}

