package com.wiley.core.wileyas.subscription;

import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.subscriptionservices.subscription.impl.FindSubscriptionPricingWithCurrentPriceFactoryStrategy;

import org.springframework.beans.factory.annotation.Required;


public class WileyasOrderEditFindSubscriptionPricingWithCurrentPriceFactoryStrategy extends
		FindSubscriptionPricingWithCurrentPriceFactoryStrategy
{

	private PriceFactory priceFactory;

	@Override
	public PriceFactory getCurrentPriceFactory()
	{
		return priceFactory;
	}

	@Required
	public void setPriceFactory(final PriceFactory priceFactory)
	{
		this.priceFactory = priceFactory;
	}
}
