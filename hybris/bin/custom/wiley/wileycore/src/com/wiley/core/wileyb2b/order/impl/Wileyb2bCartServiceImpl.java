package com.wiley.core.wileyb2b.order.impl;

import com.wiley.core.event.facade.orderinfo.Wileyb2bPopulateOrderInfoDataEvent;
import com.wiley.core.order.impl.WileyCartServiceImpl;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public class Wileyb2bCartServiceImpl extends WileyCartServiceImpl {
    @Override
    public void populateFromCurrentCart(final OrderInfoData orderInfoData) {
        Wileyb2bPopulateOrderInfoDataEvent event = new Wileyb2bPopulateOrderInfoDataEvent(orderInfoData);
        getApplicationEventPublisher().publishEvent(event);
    }
}
