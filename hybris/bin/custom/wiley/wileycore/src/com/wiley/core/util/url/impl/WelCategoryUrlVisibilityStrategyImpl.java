package com.wiley.core.util.url.impl;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.util.url.UrlVisibilityStrategy;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public class WelCategoryUrlVisibilityStrategyImpl implements UrlVisibilityStrategy<CategoryModel>
{
	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Override
	public boolean isUrlForItemVisible(final CategoryModel category)
	{
		ServicesUtil.validateParameterNotNull(category, "Category couldn't be null");

		if (wileyCategoryService.isFreeTrialCategory(category)
				|| wileyCategoryService.isGiftCardsCategory(category)
				|| wileyCategoryService.isSupplementsCategory(category)
				|| wileyCategoryService.isPartnersCategory(category)
				|| category instanceof VariantCategoryModel
				|| category instanceof VariantValueCategoryModel ^ wileyCategoryService.isCFALevelVariantCategory(category)
				|| !isTopLevelCategory(category) && !isCFASubCategory(category))
		{
			return false;
		}
		return true;
	}
	private boolean isCFASubCategory(final CategoryModel categoryModel) {
		return wileyCategoryService.isCFALevelCategory(categoryModel)
				|| wileyCategoryService.isCFALevelVariantCategory(categoryModel);
	}
	private boolean isTopLevelCategory(final CategoryModel categoryModel) {
		List<CategoryModel> parents =
				wileyCategoryService.getTopLevelCategoriesForCategory(categoryModel);
		return parents.size() == 1 && categoryModel.getCode().equals(parents.get(0).getCode());
	}
}
