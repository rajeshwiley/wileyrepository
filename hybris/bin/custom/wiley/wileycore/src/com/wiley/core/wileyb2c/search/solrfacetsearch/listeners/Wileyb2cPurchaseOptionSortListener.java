package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchListener;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cPurchaseOptionSortListener implements FacetSearchListener
{
	static final String POSTFIX = "_int asc";
	private String purchaseOptionSequenceProperty;

	@Override
	public void beforeSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{
		facetSearchContext.getSearchQuery().addRawParam("group.sort", purchaseOptionSequenceProperty + POSTFIX);
	}

	public void setPurchaseOptionSequenceProperty(final String purchaseOptionSequenceProperty)
	{
		this.purchaseOptionSequenceProperty = purchaseOptionSequenceProperty;
	}

	@Override
	public void afterSearch(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}

	@Override
	public void afterSearchError(final FacetSearchContext facetSearchContext) throws FacetSearchException
	{

	}
}
