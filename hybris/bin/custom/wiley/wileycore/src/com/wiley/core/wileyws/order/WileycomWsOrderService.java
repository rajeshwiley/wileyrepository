package com.wiley.core.wileyws.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;

import java.util.Optional;

import javax.validation.constraints.NotNull;



/**
 * The interface Wileycom ws order service.
 */
public interface WileycomWsOrderService extends OrderService
{

	/**
	 * Update status and add history entry.
	 *
	 * @param orderModel
	 * 		the order model
	 * @param orderStatus
	 * 		the order status
	 * @param description
	 * 		the description
	 */
	void updateOrderStatusAndAddHistoryEntry(OrderModel orderModel, OrderStatus orderStatus, String description);

	/**
	 * Find order by code order model.
	 *
	 * @param orderCode
	 * 		the order code
	 * @return the order model
	 */
	OrderModel findOrderByCode(String orderCode);

	/**
	 * finds an order entry by isbn of the entry product
	 * @param abstractOrderEntryModel
	 * @param isbn
	 * @return
	 */
	Optional<AbstractOrderEntryModel> getOrderEntryModelByISBN(@NotNull AbstractOrderModel abstractOrderEntryModel,
			@NotNull String isbn);


	/**
	 * Save orderModel and create history entry
	 *  @param orderModel orderModel
	 * @param snapshot snapshot of original order
	 * @param message message
	 */
	void updateOrderAndAddHistoryEntry(OrderModel orderModel, OrderModel snapshot, String message);
}
