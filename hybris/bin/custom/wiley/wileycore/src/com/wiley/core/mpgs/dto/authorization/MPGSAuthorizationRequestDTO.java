package com.wiley.core.mpgs.dto.authorization;

import com.wiley.core.mpgs.dto.json.Billing;
import com.wiley.core.mpgs.dto.json.Order;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.json.authorization.SourceOfFunds;


public class MPGSAuthorizationRequestDTO
{
	private String apiOperation;
	private SourceOfFunds sourceOfFunds;
	private Order order;
	private Billing billing;
	private Transaction transaction;

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public Order getOrder()
	{
		return order;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public void setOrder(final Order order)
	{
		this.order = order;
	}

	public String getApiOperation()
	{
		return apiOperation;
	}

	public void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}

	public Billing getBilling()
	{
		return billing;
	}

	public void setBilling(final Billing billing)
	{
		this.billing = billing;
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}
}
