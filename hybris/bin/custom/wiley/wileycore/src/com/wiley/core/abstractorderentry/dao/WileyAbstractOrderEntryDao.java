package com.wiley.core.abstractorderentry.dao;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.List;


public interface WileyAbstractOrderEntryDao
{
	List<AbstractOrderEntryModel> findAbstractOrderEntriesByBusinessKey(String businessKey);

	List<AbstractOrderEntryModel> findAbstractOrderEntryByGuid(String siteUid, String guid);
}
