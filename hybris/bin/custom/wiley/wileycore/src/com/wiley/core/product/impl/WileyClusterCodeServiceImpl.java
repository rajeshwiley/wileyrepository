package com.wiley.core.product.impl;

import com.wiley.core.model.ClusterCodeModel;
import com.wiley.core.product.WileyClusterCodeService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public class WileyClusterCodeServiceImpl implements WileyClusterCodeService {

    private List<String> useIsbnForCurrencies;
    
    @Override
    public Optional<ClusterCodeModel> getClusterCodeForCurrency(@NotNull final ProductModel product,
                                                                @NotNull final CurrencyModel currency)
    {
        Assert.notNull(product);
        Assert.notNull(currency);
        
        return product.getClusterCodes().stream().filter(
            clusterCode -> currency.equals(clusterCode.getCurrency())
        ).findFirst();
    }

    @Override
    public boolean shouldClusterCodeBeDefinedForThisCurrency(@NotNull final CurrencyModel currency)
    {
        Assert.notNull(currency);
        Assert.notNull(useIsbnForCurrencies,
                "List of currencies that don't have cluster code should be set up correctly");
        for (String excludedCurrency : useIsbnForCurrencies) {
            if (excludedCurrency.equalsIgnoreCase(currency.getIsocode()))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isClusterCodeMissingForCurrency(@NotNull final ProductModel product, @NotNull final CurrencyModel currency)
    {
        return shouldClusterCodeBeDefinedForThisCurrency(currency) && !getClusterCodeForCurrency(product, currency).isPresent();
    }

    public void setUseIsbnForCurrencies(final List<String> useIsbnForCurrencies) {
        this.useIsbnForCurrencies = useIsbnForCurrencies;
    }
}
