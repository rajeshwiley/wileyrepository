package com.wiley.core.externaltax.xml.parsing;

import java.util.ArrayList;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.helpers.DefaultValidationEventHandler;

import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;

import com.wiley.core.exceptions.WileyTaxServiceMessageException;


/**
 * Dedicated converter for Tax Service. Customized in two favours - registered for "application/octet-stream" media type and
 * amended error processing
 */
public class TaxServiceJaxb2RootElementHttpMessageConverter extends Jaxb2RootElementHttpMessageConverter
{
	public TaxServiceJaxb2RootElementHttpMessageConverter()
	{
		super();

		// Tax Service response doesn't contain Content-Type header.
		// org.springframework.web.client.HttpMessageConverterExtractor.getContentType() returns
		// MediaType.APPLICATION_OCTET_STREAM by default
		ArrayList<MediaType> mediaTypes = new ArrayList<>(getSupportedMediaTypes());
		mediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
		mediaTypes.add(MediaType.TEXT_PLAIN);
		setSupportedMediaTypes(mediaTypes);
	}

	@Override
	protected void customizeUnmarshaller(final Unmarshaller unmarshaller)
	{
		super.customizeUnmarshaller(unmarshaller);

		try
		{
			// Event handler is required to report an error. Default Unmarshaller implementation swallow any errors except of
			// javax.xml.bind.ValidationEvent.FATAL_ERROR. DefaultValidationEventHandler in contrast to it, terminate processing
			// straight after it meet Messages.ERROR.
			unmarshaller.setEventHandler(new DefaultValidationEventHandler());
		}
		catch (JAXBException e)
		{
			throw new WileyTaxServiceMessageException(
					"An error occured while setting validation event handler to tax service message converter", e);
		}
	}
}
