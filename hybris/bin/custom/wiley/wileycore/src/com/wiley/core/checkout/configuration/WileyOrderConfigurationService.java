package com.wiley.core.checkout.configuration;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Optional;

import com.wiley.core.model.WileyOrderConfigurationModel;


public interface WileyOrderConfigurationService
{
	Optional<WileyOrderConfigurationModel> getOrderConfiguration(AbstractOrderModel orderModel);
}
