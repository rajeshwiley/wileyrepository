package com.wiley.core.category.attributehandler;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.variants.model.VariantCategoryModel;
import javax.validation.constraints.NotNull;
import org.apache.log4j.Logger;
import com.wiley.core.enums.VariantCategoryType;


public class TypeVariantCategoryAttributeHandler
		extends AbstractDynamicAttributeHandler<VariantCategoryType, VariantCategoryModel>
{
	private static final Logger LOGGER = Logger.getLogger(TypeVariantCategoryAttributeHandler.class);

	public static final String TYPE_CATEGORY_CODE_ENDS_WITH = "TYPE";
	public static final String PART_CATEGORY_CODE_ENDS_WITH = "PART";
	public static final String LEVEL_CATEGORY_CODE_ENDS_WITH = "LEVEL";

	@Override
	public VariantCategoryType get(@NotNull final VariantCategoryModel category)
	{
		if (category == null) //NOSONAR
		{
			throw new IllegalArgumentException("VariantCategoryModel is required");
		}
		if (category.getCode().endsWith(TYPE_CATEGORY_CODE_ENDS_WITH))
		{
			return VariantCategoryType.TYPE;
		}
		else if (category.getCode().endsWith(PART_CATEGORY_CODE_ENDS_WITH))
		{
			return VariantCategoryType.PART;
		}
		else if (category.getCode().endsWith(LEVEL_CATEGORY_CODE_ENDS_WITH))
		{
			return VariantCategoryType.LEVEL;
		}
		return null;
	}

	@Override
	public void set(@NotNull final VariantCategoryModel category, final VariantCategoryType value) {
		LOGGER.trace("Silently ignoring TypeVariantCategoryAttributeHandler setter invocation..");
	}
}
