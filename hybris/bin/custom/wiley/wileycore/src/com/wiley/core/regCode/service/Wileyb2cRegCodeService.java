package com.wiley.core.regCode.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.regCode.exception.RegCodeInvalidException;


/**
 * Registration Code Service.
 */
public interface Wileyb2cRegCodeService
{
	/**
	 * Validate the Registration Code via extarnal system
	 *
	 * @param regCode
	 * 		the registration code
	 * @throws RegCodeInvalidException
	 */
	CartModel validateRegCode(@Nonnull String regCode);

	/**
	 * Validate the Registration Code via extarnal system
	 *
	 * @param regCode
	 * 		the registration code
	 * @throws RegCodeInvalidException
	 */
	CartModel validateRegCodeForProduct(@Nonnull String regCode, @Nonnull String productCode);

	/**
	 * Get RegCode, if it's applied to the order
	 * @param abstractOrderModel order
	 * @return String value of the code
	 */
	Optional<String> getRegCodeForOrder(AbstractOrderModel abstractOrderModel);
}
