package com.wiley.core.wileycom.product.valuetranslator.adapter;

import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomProductRestrictionImportAdapter
{
	/**
	 * Import {@link de.hybris.platform.voucher.model.ProductRestrictionModel} to specified voucher
	 *
	 * @param cellValue
	 * 		String representation of {@link de.hybris.platform.voucher.model.ProductRestrictionModel}.
	 * 		For example: 222222222221|222222222222, where 222222222221 and 222222222222 are product ISBN
	 * @param promotionVoucher
	 * 		promotion voucher to import
	 * @param catalogId
	 * 		the catalogId for product lookup
	 * @param catalogVersion
	 * 		the catalogVersion for product lookup
	 * @return the code of product if found
	 * @throws UnresolvedValueException
	 * 		when valueExpr is in invalid format
	 */
	void performImport(String cellValue, Item promotionVoucher, String catalogId, String catalogVersion)
			throws UnresolvedValueException;
}
