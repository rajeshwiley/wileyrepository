package com.wiley.core.search.solrfacetsearch;

import java.io.Serializable;
import java.util.Objects;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyQueryFacet implements Serializable
{
	private String field;
	private String value;

	public WileyQueryFacet(final String field, final String query)
	{
		this.field = field;
		this.value = query;
	}

	public String getField()
	{
		return field;
	}

	public void setField(final String field)
	{
		this.field = field;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final WileyQueryFacet that = (WileyQueryFacet) o;

		return Objects.equals(this.field, that.field)
				&& Objects.equals(this.value, that.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(field, value);
	}
}
