package com.wiley.core.jobs;


import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;

import java.time.Instant;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.cart.WileyCommerceCartDao;
import com.wiley.core.event.AbandonCartEvent;
import com.wiley.core.model.AbandonCartEmailCronJobModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * Job for abandon cart email.
 */
public class AbandonCartEmailJobPerformable extends AbstractJobPerformable<AbandonCartEmailCronJobModel>
{

	@Autowired
	@Qualifier(value = "wileyCommerceCartDao")
	private WileyCommerceCartDao commerceCartDao;

	@Autowired
	private EventService eventService;

	private static final Logger LOG = Logger.getLogger(AbandonCartEmailJobPerformable.class);

	@Override
	public PerformResult perform(final AbandonCartEmailCronJobModel job)
	{
		LOG.info("Starting job for cron job " + job.getCode());

		try
		{
			if (job.getSites() == null || job.getSites().isEmpty())
			{
				LOG.warn("There is no sites defined for " + job.getCode());
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			}



			final int cartAge = job.getAbandonCartEmailAge().intValue();

			for (final BaseSiteModel site : job.getSites())
			{

				final Instant instant = Instant.now().minusSeconds(cartAge);
				Date modifiedBefore = Date.from(instant);

				for (final CartModel selectedCart : getCommerceCartDao().getAbandonCarts(modifiedBefore, site))
				{
					if (CollectionUtils.isNotEmpty(selectedCart.getEntries()))
					{
						LOG.info("Sending email " + job.getCode());
						eventService.publishEvent(new AbandonCartEvent(selectedCart));
					}

				}
			}

			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during abandon cart email cron job execution", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

	}

	public WileyCommerceCartDao getCommerceCartDao()
	{
		return commerceCartDao;
	}

	public void setCommerceCartDao(final WileyCommerceCartDao commerceCartDao)
	{
		this.commerceCartDao = commerceCartDao;
	}

}
