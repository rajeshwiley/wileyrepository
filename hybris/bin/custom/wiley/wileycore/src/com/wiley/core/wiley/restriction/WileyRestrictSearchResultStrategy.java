package com.wiley.core.wiley.restriction;

import com.wiley.core.wileyb2c.serialization.json.Wileyb2cVariantDto;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileyRestrictSearchResultStrategy
{
	boolean isRestricted(Wileyb2cVariantDto product);
}
