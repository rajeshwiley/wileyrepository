package com.wiley.core.abstractorderentry.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.abstractorderentry.dao.WileyAbstractOrderEntryDao;
import com.wiley.core.abstractorderentry.service.WileyAbstractOrderEntryService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.lang.String.format;


public class WileyAbstractOrderEntryServiceImpl implements WileyAbstractOrderEntryService
{
	private static final String ENTRY_NOT_FOUND = "Entry with guid [%s] for site [%s] not found";
	private static final String ENTRY_NOT_UNIQUE = "Entry with guid [%s] for site [%s] isn't unique";

	@Resource
	private WileyAbstractOrderEntryDao wileyAbstractOrderEntryDao;

	@Override
	public Optional<AbstractOrderEntryModel> getAbstractOrderEntryForBusinessKey(final String businessKey)
	{
		final List<AbstractOrderEntryModel> abstractOrderEntries =
				wileyAbstractOrderEntryDao.findAbstractOrderEntriesByBusinessKey(businessKey);
		return Optional.ofNullable(CollectionUtils.isNotEmpty(abstractOrderEntries) ? abstractOrderEntries.get(0) : null);
	}

	@Override
	public AbstractOrderEntryModel getAbstractOrderEntryForGuid(final String siteId, final String guid)
	{
		List<AbstractOrderEntryModel> entries = wileyAbstractOrderEntryDao.findAbstractOrderEntryByGuid(siteId, guid);
		validateIfSingleResult(entries, format(ENTRY_NOT_FOUND, guid, siteId), format(ENTRY_NOT_UNIQUE, guid, siteId));
		return entries.get(0);
	}
}
