package com.wiley.core.wileyb2c.subscription.services;

import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;


public interface Wileyb2cFreeTrialSubscriptionTermCheckingStrategy
{

	/**
	 * Checks if current subscription term is free trial
	 * @param subscriptionTerm - subscription term
	 * @return - true if current subscription is free trial
	 */
	boolean isFreeTrial(SubscriptionTermModel subscriptionTerm);

}
