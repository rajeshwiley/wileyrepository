package com.wiley.core.adapters;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrMatcher;
import org.apache.commons.lang3.text.StrTokenizer;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;


/**
 * Abstract adapter converts two-dimension lists like "<code>value1|subvalue1, value2|subvalue2</code>".
 * First dimension is comma-separate list, that has ordinary CSV format.
 * Second dimension is pipe-separated list.
 */
public abstract class AbstractTwoDimensionListImportAdapter<T extends ItemModel>
{
	private static final String DEFAULT_SECOND_DIMENSION_DELIMITER = "|";
	private static final int DEFAULT_SECOND_DIMENSION_MIN_SIZE_EXPECTED = 2;

	@Autowired
	private ModelService modelService;
	@Autowired
	private SessionService sessionService;

	public void performImport(final String cellValue, final Item processedItem, final Map<String, Object> sessionParams)
			throws ImpExException
	{
		final ProductModel product = modelService.get(processedItem);
		final StrTokenizer csvTokenizer = createCSVTokenizer(cellValue);
		final List<T> links = covertTokens(product, csvTokenizer);
		saveModelsInLocalView(sessionParams, product, links);
	}

	private StrTokenizer createCSVTokenizer(final String cellValue)
	{
		final StrTokenizer csvTokenizer = StrTokenizer.getCSVInstance();
		csvTokenizer.setIgnoreEmptyTokens(true);
		csvTokenizer.reset(cellValue);
		return csvTokenizer;
	}

	private List<T> covertTokens(final ProductModel product, final StrTokenizer csvTokenizer) throws ImpExException
	{
		final List<T> links = new LinkedList<T>();
		for (final String token : csvTokenizer.getTokenList())
		{
			links.add(convertToModel(token, product));
		}
		return links;
	}

	private void saveModelsInLocalView(final Map<String, Object> sessionParams, final ProductModel product, final List<T> links)
	{
		sessionService.executeInLocalViewWithParams(sessionParams, new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				saveModels(product, links);
			}
		});
	}

	/**
	 * Method of converting token from the first dimension.
	 *
	 * @param token
	 * 		Token
	 * @param product
	 * 		Product
	 * @return not saved model item
	 * @throws ImpExException
	 */
	public abstract T convertToModel(@Nonnull String token, @Nonnull ProductModel product) throws ImpExException;

	/**
	 * Method to be used to save list of converted models under the product
	 *
	 * @param product
	 * 		Product
	 * @param models
	 * 		List of models to be saved
	 * @throws ImpExException
	 */
	public abstract void saveModels(ProductModel product, List<T> models);


	/**
	 * Create tokenizer for the second dimension.
	 * Util to be used under {@link #convertToModel(String, ProductModel)} method.
	 *
	 * @param token
	 * 		Token to be passed to tokenizer
	 * @return Configured tokenizer
	 * @throws ImpExException
	 * 		If token contains less items then expected by {@link #getSecondDimensionTokenizer(String)} amount
	 */
	protected StrTokenizer getSecondDimensionTokenizer(@Nonnull final String token) throws ImpExException
	{
		Preconditions.checkNotNull(token, "token parameter should not be null");

		final StrTokenizer tokenizer = new StrTokenizer(token, getSecondDimensionDelimiter());
		tokenizer.setIgnoreEmptyTokens(false);
		tokenizer.setTrimmerMatcher(StrMatcher.trimMatcher());

		if (tokenizer.size() < getSecondDimensionMinSizeExpected())
		{
			throw new ImpExException(
					String.format(
							"Impex value should have more items in the list. Impex tokenized value is [%s]."
									+ " Expected [%s] items  with [%s] delimiter.", token,
							getSecondDimensionMinSizeExpected(), getSecondDimensionDelimiter()));
		}
		return tokenizer;
	}

	/**
	 * Util method to get value from tokenizer for the second dimension
	 *
	 * @param tokenizer
	 * 		{@link StrTokenizer} tokenizer (for token from the second dimension)
	 * @param isMandatory
	 * 		If particular value is mandatory in the token and should not be empty
	 * @param fieldName
	 * 		Field name that will be used during exception handling
	 * @return String value from tokenizer
	 * @throws ImpExException
	 * 		If value is mandatory and empty
	 */
	protected String getNextValue(final StrTokenizer tokenizer, final boolean isMandatory, final String fieldName)
			throws ImpExException
	{

		final String value = tokenizer.hasNext() ? tokenizer.next() : null;
		if (isMandatory && StringUtils.isEmpty(value))
		{
			throw new ImpExException(
					String.format("[%s]: [%s] field should not be empty. Elemenent value: [%s]", this.getClass().getSimpleName(),
							fieldName, value));
		}
		else
		{
			return value;
		}
	}

	public String getSecondDimensionDelimiter()
	{
		return DEFAULT_SECOND_DIMENSION_DELIMITER;
	}

	public int getSecondDimensionMinSizeExpected()
	{
		return DEFAULT_SECOND_DIMENSION_MIN_SIZE_EXPECTED;
	}

}
