package com.wiley.core.externaltax.strategies;

import de.hybris.platform.commerceservices.externaltax.CalculateExternalTaxesStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.core.externaltax.WileyExternalTaxDocument;


/**
 * Service for calculating taxes using Wiley tax service.
 */
public interface WileyCalculateExternalTaxesStrategy extends CalculateExternalTaxesStrategy
{
	/**
	 * Calculate taxes for an order.
	 *
	 * @param abstractOrder
	 * 		order to calculate taxes for
	 * @return an WileyExternalTaxDocument that represents the taxes among with statuses of tax calculation for each order item
	 */
	WileyExternalTaxDocument calculateExternalTaxes(AbstractOrderModel abstractOrder);

}
