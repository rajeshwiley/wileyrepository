/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.commerceservices.externaltax.DecideExternalTaxesStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;
import com.wiley.core.order.WileyCheckoutService;


public class WileyDetermineExternalTaxStrategy implements
		DecideExternalTaxesStrategy
{
	private WileyTaxAddressStrategy taxAddressStrategy;

	private WileyCheckoutService wileyCheckoutService;

	private UserService userService;

	@Override
	public boolean shouldCalculateExternalTaxes(
			final AbstractOrderModel abstractOrder)
	{
		if (abstractOrder == null)
		{
			throw new IllegalStateException(
					"Order is null. Cannot apply external tax to it.");
		}

		boolean isNonZeroOrder = wileyCheckoutService.isNonZeroOrder(abstractOrder);

		return Boolean.TRUE.equals(abstractOrder.getNet())
				&& isAddressForTaxPresent(abstractOrder)
				&& isTaxNumberNotPresent(abstractOrder)
				&& isNonZeroOrder
				&& !isAnonymousUser();
	}

	private boolean isAnonymousUser()
	{
		return userService.isAnonymousUser(userService.getCurrentUser());
	}

	private boolean isAddressForTaxPresent(final AbstractOrderModel abstractOrder)
	{
		return taxAddressStrategy.resolvePaymentAddress(abstractOrder) != null;
	}
	
	private boolean isTaxNumberNotPresent(final AbstractOrderModel abstractOrder)
	{
		return StringUtils.isBlank(abstractOrder.getTaxNumber());
	}

	@Required
	public void setTaxAddressStrategy(final WileyTaxAddressStrategy taxAddressStrategy)
	{
		this.taxAddressStrategy = taxAddressStrategy;
	}

	public void setWileyCheckoutService(final WileyCheckoutService wileyCheckoutService)
	{
		this.wileyCheckoutService = wileyCheckoutService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
