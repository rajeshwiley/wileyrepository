package com.wiley.core.payment.populators;

import com.wiley.core.order.WileyCartService;
import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;


/**
 * Populator to fill payment info
 */
public class DeliveryMethodWPGHttpRequestPopulator extends
		AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	private static final String WPG_CUSTOM_DELIVERY_METHOD_NAME = "WPG_CUSTOM_delivery_method_name";
	private static final String WPG_CUSTOM_DELIVERY_METHOD_DESCRIPTION = "WPG_CUSTOM_delivery_method_description";

	@Resource
	private WileyCartService wileyCartService;

	@Override
	public void populate(final CreateSubscriptionRequest createSubscriptionRequest, final PaymentData paymentData)
			throws ConversionException
	{
		final CartModel cartModel = getWileyCartService().getSessionCart();
		final DeliveryModeModel deliveryModeModel = cartModel.getDeliveryMode();
		if (deliveryModeModel != null)
		{
			addRequestQueryParam(paymentData, WPG_CUSTOM_DELIVERY_METHOD_NAME, deliveryModeModel.getName());
			addRequestQueryParam(paymentData,
					WPG_CUSTOM_DELIVERY_METHOD_DESCRIPTION, deliveryModeModel.getDescription());
		}
	}

	public WileyCartService getWileyCartService() {
		return wileyCartService;
	}

	public void setWileyCartService(final WileyCartService wileyCartService) {
		this.wileyCartService = wileyCartService;
	}
}
