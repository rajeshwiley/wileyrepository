package com.wiley.core.wileycom.customer.strategy.impl;

import com.wiley.core.wileycom.customer.service.WileycomCustomerService;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Resource;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;


public class WileycomCustomerRegistrationStrategyImpl implements WileyCustomerRegistrationStrategy
{
	@Resource
	private WileycomCustomerService wileycomCustomerService;

	@Override
	public boolean register(final CustomerModel customerModel, final String password)
	{
		return wileycomCustomerService.register(customerModel, password);
	}
}
