package com.wiley.core.mpgs.command.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyRetrieveSessionCommand;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;


public class WileyMPGSRetrieveSessionCommandImpl implements WileyRetrieveSessionCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSRetrieveSessionCommandImpl.class);

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyRetrieveSessionResponse perform(final WileyRetrieveSessionRequest request)
	{
		WileyRetrieveSessionResponse response = null;
		try
		{
			response = wileyMpgsPaymentGateway.retrieveSession(request);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to retrieve session due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyRetrieveSessionResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to retrieve session due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyRetrieveSessionResponse::new);
		}
		return response;
	}

}