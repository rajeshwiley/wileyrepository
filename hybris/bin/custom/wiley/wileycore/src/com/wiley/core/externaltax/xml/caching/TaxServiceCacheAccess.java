package com.wiley.core.externaltax.xml.caching;

import java.util.Optional;

import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;


/**
 * Provides an access to cached values of tax amounts for particular request
 */
public interface TaxServiceCacheAccess
{
	Optional<Double> get(TaxServiceRequest taxServiceRequest);

	void put(TaxServiceRequest taxServiceRequest, Double taxAmount);
}
