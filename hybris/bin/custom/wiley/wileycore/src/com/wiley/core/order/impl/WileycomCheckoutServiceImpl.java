package com.wiley.core.order.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.order.WileycomCheckoutService;
import com.wiley.core.wileycom.order.dao.WileycomOrderDao;


public class WileycomCheckoutServiceImpl extends WileyCheckoutServiceImpl implements WileycomCheckoutService
{
	@Resource
	private UserService userService;

	@Resource
	private WileycomOrderDao wileycomOrderDao;

	@Override
	public Optional<OrderModel> getLastOrderForCustomer(final String customerUid)
	{
		Optional<OrderModel> resultOrder = Optional.empty();
		CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customerUid);

		if (customerModel != null)
		{
			resultOrder = wileycomOrderDao.getLastOrderForCustomer(customerModel);
		}

		return resultOrder;
	}
}
