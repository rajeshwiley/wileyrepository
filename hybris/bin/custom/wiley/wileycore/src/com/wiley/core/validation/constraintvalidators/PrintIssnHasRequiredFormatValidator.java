package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.PrintIssnHasRequiredFormat;


public class PrintIssnHasRequiredFormatValidator
		extends AbstractWileyProductConstraintValidator<PrintIssnHasRequiredFormat>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validatePrintIssnHasRequiredFormat(productModel.getFeatures());
	}
}
