package com.wiley.core.wileybundle.service;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyBundleModel;


public interface WileyBundlesFilterService
{
	/**
	 * Perform filtering based on user session country with its fallbacks and products visibility rules
	 * @param bundles - List of bundles that could contain incorrect entries
	 * @return - filtered, visible and valid list of product bundles
	 */
	List<WileyBundleModel> filterBundles(@Nonnull List<WileyBundleModel> bundles);
}
