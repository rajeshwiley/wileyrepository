package com.wiley.core.wel.seo;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.event.AfterCronJobFinishedEvent;
import com.wiley.core.util.seo.SeoAfterSaveClusterAwareEvent;


public class WelAfterSyncListener extends AbstractEventListener<AfterCronJobFinishedEvent>
{
	private static final String SYNC_JOB = "sync welProductCatalog:Staged->Online";
	@Autowired
	private EventService eventService;

	@Override
	protected void onEvent(final AfterCronJobFinishedEvent event)
	{
		if (event.getCronJobResult().equals(CronJobResult.SUCCESS) && event.getJobCode().equals(SYNC_JOB))
		{
			sendSeoEvent();
		}
	}

	private void sendSeoEvent()
	{
		final SeoAfterSaveClusterAwareEvent afterSaveClusterAwareEvent = new SeoAfterSaveClusterAwareEvent(StringUtils.EMPTY);
		eventService.publishEvent(afterSaveClusterAwareEvent);
	}

}
