package com.wiley.core.order;

import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * Created by Uladzimir_Barouski on 3/2/2016.
 */
public interface WileyCheckoutService
{
	/**
	 * Check if student's checkout flow
	 *
	 * @return boolean
	 */
	boolean isStudentOrder(AbstractOrderModel abstractOrderModel);

	/**
	 * Check if student's AbstractOrder has not empty university name
	 *
	 * @return boolean
	 */
	boolean isCartHasStudentVerification(AbstractOrderModel abstractOrderModel);

	/**
	 * Check if order doesn't have products to be shipped
	 *
	 * @return boolean
	 */
	boolean isOrderWithoutShipping(AbstractOrderModel abstractOrderModel);

	/**
	 * Checks if order is non Zero Cost
	 *
	 * @return boolean
	 */
	boolean isNonZeroOrder(AbstractOrderModel abstractOrderModel);

	/**
	 * Checks if order is Zero Cost
	 * @param order
	 * @return - true if order should not be paid
	 */
	default boolean isZeroOrder(AbstractOrderModel order) {
		return !isNonZeroOrder(order);
	}

	/**
	 * Check if order contains only digital products
	 *
	 * @return boolean
	 */
	boolean isDigitalCart(AbstractOrderModel abstractOrderModel);

	/**
	 * Checks if current user has already have order with passed free trial product code
	 *
	 * @return boolean
	 */
	boolean isFreeTrialWasOrderedByCurrentUser(String freeTrialCode);

	/**
	 * Detects whether current checkout flow is operated by PayPal
	 * @return true for PayPal flow
	 */
	boolean isPaypalCheckout();
	
	boolean isTaxAvailable(AbstractOrderModel abstractOrderModel);
}
