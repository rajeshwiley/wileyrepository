package com.wiley.core.servicelayer.processdefinition.impl;

import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.servicelayer.dao.processdefinition.WileyProcessDefinitionDao;
import com.wiley.core.servicelayer.processdefinition.WileyProcessDefinitionService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyProcessDefinitionServiceImpl implements WileyProcessDefinitionService
{
	@Resource
	private WileyProcessDefinitionDao wileyProcessDefinitionDao;

	@Override
	public List<DynamicProcessDefinitionModel> getAllProcessDefinitions()
	{
		return wileyProcessDefinitionDao.findAllProcessDefinitions();
	}

	@Override
	public List<DynamicProcessDefinitionModel> getProcessDefinitionsByCode(final String code)
	{
		return wileyProcessDefinitionDao.findProcessDefinitionsByCode(code);
	}
}
