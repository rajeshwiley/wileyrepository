package com.wiley.core.payment.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.commands.request.SubscriptionAuthorizationRequest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.payment.strategies.CreateSubscriptionAuthorizeRequestStrategy;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;
import com.wiley.core.payment.response.WileyCaptureResult;


public class WileyWPGPaymentServiceImpl implements WileyPaymentService
{
	@Autowired
	private ModelService modelService;
	@Autowired
	private CardPaymentService cardPaymentService;
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private CreateSubscriptionAuthorizeRequestStrategy subscriptionAuthorizeRequestStrategy;

	@Override
	public Optional<PaymentTransactionEntryModel> capture(final PaymentTransactionModel transaction)
	{
		Optional<PaymentTransactionEntryModel> authEntry = findAuthorizationEntry(transaction);
		PaymentTransactionEntryModel captureEntry = null;

		if (authEntry.isPresent())
		{

			PaymentTransactionType transactionType = PaymentTransactionType.CAPTURE;
			String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, transactionType);

			WileyCaptureResult result = performCapture(authEntry.get(), newEntryCode);

			captureEntry = modelService.create(PaymentTransactionEntryModel.class);

			captureEntry.setType(transactionType);
			captureEntry.setCurrency(authEntry.get().getCurrency());
			captureEntry.setPaymentTransaction(transaction);
			captureEntry.setCode(newEntryCode);

			captureEntry.setAmount(result.getTotalAmount());
			captureEntry.setRequestId(result.getRequestId());
			captureEntry.setRequestToken(result.getRequestToken());
			captureEntry.setTime(result.getRequestTime());

			captureEntry.setTransactionStatus(result.getTransactionStatus().toString());
			captureEntry.setTransactionStatusDetails(result.getStatus().getDescription());
			captureEntry.setWpgMerchantResponse(result.getMerchantResponse());
			captureEntry.setWpgResponseCode(result.getStatus().getCode());

			modelService.save(captureEntry);
		}
		return Optional.ofNullable(captureEntry);
	}

	@Override
	public PaymentTransactionEntryModel authorize(final AbstractOrderModel order)
	{
		SubscriptionAuthorizationRequest request = subscriptionAuthorizeRequestStrategy.createRequest(order);

		WileyAuthorizeResult result = (WileyAuthorizeResult) cardPaymentService.authorize(request);

		PaymentTransactionModel transaction = modelService.create(PaymentTransactionModel.class);
		transaction.setOrder(order);
		transaction.setPaymentProvider(order.getStore().getPaymentProvider());
		transaction.setCode(order.getUser().getUid() + "_" + UUID.randomUUID());

		PaymentTransactionType transactionType = PaymentTransactionType.AUTHORIZATION;

		String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(transaction, transactionType);

		PaymentTransactionEntryModel authEntry = modelService.create(PaymentTransactionEntryModel.class);

		authEntry.setType(transactionType);
		authEntry.setCurrency(order.getCurrency());
		authEntry.setPaymentTransaction(transaction);
		authEntry.setCode(newEntryCode);

		authEntry.setAmount(result.getTotalAmount());
		authEntry.setRequestId(result.getRequestId());
		authEntry.setRequestToken(result.getRequestToken());
		authEntry.setTime(result.getAuthorizationTime());

		authEntry.setTransactionStatus(result.getTransactionStatus().toString());
		authEntry.setTransactionStatusDetails(result.getStatus().getDescription());

		authEntry.setWpgMerchantResponse(result.getMerchantResponse());
		authEntry.setWpgResponseCode(result.getStatus().getCode());
		authEntry.setWpgAuthCode(result.getAuthorizationCode());

		List<PaymentTransactionModel> paymentTransactions = new ArrayList<>(order.getPaymentTransactions());
		paymentTransactions.add(transaction);
		order.setPaymentTransactions(paymentTransactions);

		modelService.saveAll(order, authEntry);

		return authEntry;
	}

	private WileyCaptureResult performCapture(final PaymentTransactionEntryModel authEntry, final String newEntryCode)
	{
		PaymentTransactionModel transaction = authEntry.getPaymentTransaction();
		AbstractOrderModel order = transaction.getOrder();
		return (WileyCaptureResult) cardPaymentService.capture(
				new WileyCaptureRequest(newEntryCode, transaction
						.getRequestId(), authEntry.getRequestToken(),
						Currency.getInstance(authEntry.getCurrency().getIsocode()),
						authEntry.getAmount(), transaction.getPaymentProvider(), authEntry.getWpgAuthCode(),
						order.getSite().getUid()));
	}

	private Optional<PaymentTransactionEntryModel> findAuthorizationEntry(final PaymentTransactionModel transaction)
	{
		return transaction.getEntries().stream()
				.filter(entry ->
						PaymentTransactionType.AUTHORIZATION.equals(entry.getType())
								&& TransactionStatus.ACCEPTED.toString().equals(entry.getTransactionStatus()))
				.findFirst();
	}
}
