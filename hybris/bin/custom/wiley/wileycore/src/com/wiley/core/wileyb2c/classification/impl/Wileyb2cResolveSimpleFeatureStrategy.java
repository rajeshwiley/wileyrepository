package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;

import java.util.Set;

import com.google.common.collect.Sets;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cResolveSimpleFeatureStrategy extends AbstractWileyb2cResolveClassificationAttributeStrategy
{

	static final com.google.common.collect.ImmutableSet<Wileyb2cClassificationAttributes>
			CLASSIFICATION_ATTRIBUTES = Sets.immutableEnumSet(Wileyb2cClassificationAttributes.VOLUME_AND_ISSUE,
			Wileyb2cClassificationAttributes.NUMBER_OF_PAGES);

	@Override
	protected String processFeature(final Feature feature)
	{
		return feature.getValue().getValue().toString();
	}

	@Override
	protected Set<Wileyb2cClassificationAttributes> getAttributes()
	{
		return CLASSIFICATION_ATTRIBUTES;
	}
}