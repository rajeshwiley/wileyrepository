package com.wiley.core.pin.valuetranslator;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import javax.annotation.Resource;

import com.wiley.core.product.WileyProductService;


/**
 * Default Adapter implementation.
 */
public class DefaultProductCodeValueTranslatorAdapter implements ProductCodeValueTranslatorAdapter
{

	@Resource
	private WileyProductService productService;

	@Override
	public Object importValue(final String valueExpr, final Item toItem, final String catalogId, final String catalogVersion)
			throws UnresolvedValueException
	{
		String[] isbnAndTerm = valueExpr.split(":");
		if (isbnAndTerm.length > 2)
		{
			throw new UnresolvedValueException("Can't resolve given product by isbn and term as input is in wrong format.");
		}
		String isbn = isbnAndTerm[0];
		String term = isbnAndTerm.length == 2 ? isbnAndTerm[1] : null;

		ProductModel productModel = productService.getProductForIsbnAndTerm(isbn, term, catalogId, catalogVersion);

		return productModel.getCode();
	}

	@Override
	public String exportValue(final Object objectToExport) throws JaloInvalidParameterException
	{
		return objectToExport.toString();
	}
}
