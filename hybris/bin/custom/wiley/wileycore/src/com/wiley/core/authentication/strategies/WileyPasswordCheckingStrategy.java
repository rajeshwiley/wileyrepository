package com.wiley.core.authentication.strategies;

import de.hybris.platform.jalo.user.PasswordCheckingStrategy;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.util.Assert;



/**
 * Created by Uladzimir_Barouski on 5/4/2016.
 */
public class WileyPasswordCheckingStrategy implements PasswordCheckingStrategy
{
	private static final String DEFAULT = "default";

	private Map<String, PasswordCheckingStrategy> checkingStrategyMap;

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public boolean checkPassword(final User user, final String plainPassword)
	{
		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (currentBaseStore != null)
		{
			final String currentBaseStoreUid = currentBaseStore.getUid();
			final PasswordCheckingStrategy strategy = checkingStrategyMap.get(currentBaseStoreUid);
			Assert.notNull(strategy);
			return strategy.checkPassword(user, plainPassword);
		}
		return checkingStrategyMap.get(DEFAULT).checkPassword(user, plainPassword);
	}

	public void setCheckingStrategyMap(
			final Map<String, PasswordCheckingStrategy> checkingStrategyMap)
	{
		this.checkingStrategyMap = checkingStrategyMap;
	}
}

