package com.wiley.core.wileycom.product.valuetranslator;

import com.wiley.core.wileycom.valuetranslator.AbstractDefaultWileycomSpecialValueTranslator;


/**
 * Implementation of {@link AbstractDefaultWileycomSpecialValueTranslator} to add
 * {@link de.hybris.platform.voucher.model.DateRestrictionModel} to
 * {@link de.hybris.platform.voucher.model.PromotionVoucherModel}. Import logic is delegated to
 * {@link com.wiley.core.wileycom.product.valuetranslator.adapter.impl.WileycomTemporaryRestrictionImportAdapterImpl}
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileycomTemporaryRestrictionTranslator extends AbstractDefaultWileycomSpecialValueTranslator
{
	private static final String DEFAULT_IMPORT_ADAPTER_NAME = "wileycomTemporaryRestrictionImportAdapter";

	@Override
	protected String getDefaultImportAdapterName()
	{
		return DEFAULT_IMPORT_ADAPTER_NAME;
	}

}
