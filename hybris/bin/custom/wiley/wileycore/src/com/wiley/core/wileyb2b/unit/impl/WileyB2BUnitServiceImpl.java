package com.wiley.core.wileyb2b.unit.impl;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;
import com.wiley.core.wileyb2b.unit.WileyB2BUnitDao;
import com.wiley.core.wileyb2b.unit.WileyB2BUnitService;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.List;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.lang.String.format;


public class WileyB2BUnitServiceImpl implements WileyB2BUnitService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyB2BUnitServiceImpl.class);

	@Resource
	private WileyB2BUnitDao wileyB2BUnitDao;

	@Resource
	private B2BUnitService<CompanyModel, B2BCustomerModel> b2bUnitService;

	@Resource
	private WileyCustomerGateway wileyCustomerGateway;


	@Override
	public B2BUnitModel getB2BUnitForSapAccountNumber(final String sapAccountNumber)
	{
		final List<B2BUnitModel> b2BUnits = wileyB2BUnitDao.findB2BUnitBySapAccountNumber(sapAccountNumber);
		validateIfSingleResult(b2BUnits, format("B2BUnit with sapAccountNumber '%s' not found!", sapAccountNumber),
				format("B2BUnit with sapAccountNumber '%s' is not unique!", sapAccountNumber));
		return b2BUnits.get(0);
	}

	@Override
	public void handleB2BUnitActiveStatusUpdated(final String sapAccountNumber)
	{
		final B2BUnitModel b2BUnitModel = getB2BUnitForSapAccountNumber(sapAccountNumber);
		boolean active = b2BUnitModel.getActive();
		if (active)
		{
			b2bUnitService.enableUnit(b2BUnitModel);
		}
		else
		{
			this.b2bUnitService.disableUnit(b2BUnitModel);
		}
		b2bUnitService.getB2BCustomers(b2BUnitModel).forEach(b2BCustomerModel -> updateCdmCustomer(b2BCustomerModel));
	}

	private void updateCdmCustomer(final B2BCustomerModel b2BCustomerModel)
	{
		try
		{
			LOG.debug("Sending request to CDM for customer {} with updated activeStatus={}", b2BCustomerModel.getCustomerID(),
					b2BCustomerModel.getActive());
			wileyCustomerGateway.updateCustomer(b2BCustomerModel);
		}
		catch (ExternalSystemException e)
		{
			LOG.error("CDM update request for customer {} failed.", b2BCustomerModel.getCustomerID(), e);
		}
	}
}
