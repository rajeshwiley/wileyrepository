package com.wiley.core.jalo.translators;

import static com.wiley.core.jalo.translators.util.CellValueUtil.isIgnoredValue;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.Map;

import com.wiley.core.adapters.AbstractTwoDimensionListImportAdapter;


public abstract class AbstractWileySpecialValueTranslator extends AbstractSpecialValueTranslator
{
	protected static final String PRODUCT_TYPE_CODE = "Product";
	protected SpecialColumnDescriptor columnDescriptor;
	protected AbstractTwoDimensionListImportAdapter twoDimensionListImportAdapter;
	protected TypeService typeService;

	@Override
	public void validate(final String expr) throws HeaderValidationException
	{
		final String headerTypeCode = columnDescriptor.getHeader().getConfiguredComposedTypeCode();
		if (!getTypeService().isAssignableFrom(getProductTypeCode(), headerTypeCode))
		{
			throw new HeaderValidationException(String.format("Illegal composed type %s for %s."
					+ " Must be %s or any of its subtypes",
					headerTypeCode, this.getClass().getSimpleName(), getProductTypeCode()), 0);
		}
	}

	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		if (!isIgnoredValue(cellValue))
		{
			final Map<String, Object> headerInterceptorParams = columnDescriptor.getHeader().getInterceptorRelatedParameters();
			getTwoDimensionListImportAdapter().performImport(cellValue, processedItem, headerInterceptorParams);
		}
	}

	@Override
	public String performExport(final Item item) throws ImpExException
	{
		throw new UnsupportedOperationException("Export operation is not supported");
	}


	public TypeService getTypeService()
	{
		return typeService;
	}

	/**
	 * @return the twoDimensionListImportAdapter
	 */
	public AbstractTwoDimensionListImportAdapter getTwoDimensionListImportAdapter()
	{
		return twoDimensionListImportAdapter;
	}

	public String getProductTypeCode()
	{
		return PRODUCT_TYPE_CODE;
	}

	@Override
	public abstract void init(SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException;

}
