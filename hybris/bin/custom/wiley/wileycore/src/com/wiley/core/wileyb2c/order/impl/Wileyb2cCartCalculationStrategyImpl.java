package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;

import com.wiley.core.enums.OrderType;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * This strategy is intended to distinguish different types of order/cart and skip promotions is required
 */
public class Wileyb2cCartCalculationStrategyImpl extends DefaultCommerceCartCalculationStrategy
{
	@Override
	public boolean calculateCart(final CommerceCartParameter parameter)
	{
		if (shouldSkipPromotions(parameter))
		{
			return calculateWithoutPromotions(parameter);
		}
		else
		{
			return super.calculateCart(parameter);
		}
	}

	@Override
	public boolean recalculateCart(final CommerceCartParameter parameter)
	{
		if (shouldSkipPromotions(parameter))
		{
			return recalculateWithoutPromotions(parameter);
		}
		else
		{
			return super.recalculateCart(parameter);
		}
	}

	/**
	 * This method repeats everything from super.calculte() except calling updatePromotion()
	 */
	private boolean calculateWithoutPromotions(final CommerceCartParameter parameter)
	{
		final CartModel cartModel = parameter.getCart();

		validateParameterNotNull(cartModel, "Cart model cannot be null");

		final CalculationService calcService = getCalculationService();
		if (calcService.requiresCalculation(cartModel))
		{
			try
			{
				parameter.setRecalculate(false);
				beforeCalculate(parameter);
				calcService.calculate(cartModel);
			}
			catch (final CalculationException calculationException)
			{
				throw new IllegalStateException("Cart model " + cartModel.getCode() + " was not calculated due to: "
						+ calculationException.getMessage(), calculationException);
			}
			finally
			{
				afterCalculate(parameter);
			}
			return true;
		}
		return false;
	}

	/**
	 * This method repeats everything from super.recalculte() except calling updatePromotion()
	 */
	private boolean recalculateWithoutPromotions(final CommerceCartParameter parameter)
	{
		final CartModel cartModel = parameter.getCart();
		try
		{
			parameter.setRecalculate(true);
			beforeCalculate(parameter);
			getCalculationService().recalculate(cartModel);
		}
		catch (final CalculationException calculationException)
		{
			throw new IllegalStateException(String.format("Cart model %s was not calculated due to: %s ", cartModel.getCode(),
					calculationException.getMessage()), calculationException);
		}
		finally
		{
			afterCalculate(parameter);
		}
		return true;
	}

	private boolean shouldSkipPromotions(final CommerceCartParameter parameter)
	{
		if (parameter.getCart() != null)
		{
			if (OrderType.GRACE_PERIOD.equals(parameter.getCart().getOrderType()) || OrderType.REGISTRATION_CODE_ACIVATION.equals(
					parameter.getCart().getOrderType()))
			{
				return true;
			}
			return false;
		}
		return false;
	}
}
