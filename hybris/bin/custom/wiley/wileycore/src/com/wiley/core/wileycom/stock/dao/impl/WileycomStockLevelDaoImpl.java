package com.wiley.core.wileycom.stock.dao.impl;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.stock.impl.DefaultStockLevelDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.wiley.core.wileycom.stock.dao.WileycomStockLevelDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Created by Georgii_Gavrysh on 7/27/2016.
 */
public class WileycomStockLevelDaoImpl extends DefaultStockLevelDao implements WileycomStockLevelDao
{
	private static final Logger LOG = Logger.getLogger(WileycomStockLevelDaoImpl.class);

	private static final String FIND_STOCK_LEVELS_BY_CODE =
			"SELECT {" + StockLevelModel.PK + "} FROM {" + StockLevelModel._TYPECODE
					+ "} WHERE {" + StockLevelModel.CODE + "} = ?code";

	private JdbcTemplate jdbcTemplate;
	private TypeService typeService;
	private StockLevelDBMetaData stockLevelDBMetaData;
	private WarehouseDBMetaData warehouseDBMetaData;

	@Override
	public int removeObsoleteStockLevels(@NotNull final String warehouseCode, @NotNull final Long sequenceId)
	{
		prepareDBMetaData();
		LOG.debug(String.format("#removeObsoleteStockLevels. warehouseCode = %s, sequenceId = %d", warehouseCode, sequenceId));
		final String removeStatement = assembleRemoveStatement();
		final int removedRecordsNumber = getJdbcTemplate().update(removeStatement, new Object[] { sequenceId, warehouseCode });
		LOG.debug(String.format("#removeObsoleteStockLevels. %d stock level record(s) have been removed.", removedRecordsNumber));
		return removedRecordsNumber;
	}

	@Override
	public StockLevelModel findStockLevelByCode(@NotNull final String stockLevelCode)
	{
		validateParameterNotNull(stockLevelCode, "StockLevelCode cannot be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("code", stockLevelCode);

		final List<StockLevelModel> result = getFlexibleSearchService().<StockLevelModel> search(
				new FlexibleSearchQuery(FIND_STOCK_LEVELS_BY_CODE, queryParams)).getResult();

		ServicesUtil.validateIfSingleResult(result, String.format("Could not find StockLevel for code [%s]", stockLevelCode),
				String.format("There are more then one StockLevel with code [%s]", stockLevelCode));

		return result.get(0);
	}

	private String assembleRemoveStatement()
	{
		final StringBuilder stringBuilder = new StringBuilder("DELETE FROM " + stockLevelDBMetaData.tableName);
		stringBuilder.append(" WHERE (" + stockLevelDBMetaData.sequenceIdCol + " < ? ");
		stringBuilder.append("  or " + stockLevelDBMetaData.sequenceIdCol + " is null)");
		stringBuilder.append("  and " + stockLevelDBMetaData.warehouseCol + " = ");
		stringBuilder.append("    (SELECT w." + warehouseDBMetaData.pkCol + " FROM " + warehouseDBMetaData.tableName + " as w ");
		stringBuilder.append("     WHERE w." + warehouseDBMetaData.codeCol + " = ?");
		stringBuilder.append("    )");
		return stringBuilder.toString();
	}

	public JdbcTemplate getJdbcTemplate()
	{
		return jdbcTemplate;
	}

	@Override
	public void setJdbcTemplate(final JdbcTemplate jdbcTemplate)
	{
		this.jdbcTemplate = jdbcTemplate;
	}

	public TypeService getTypeService()
	{
		return typeService;
	}

	@Override
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected static class StockLevelDBMetaData
	{
		private final String tableName;
		private final String pkCol;
		private final String warehouseCol;
		private final String sequenceIdCol;

		protected StockLevelDBMetaData(final TypeService typeService)
		{
			ComposedTypeModel composedTypeModel = typeService.getComposedTypeForClass(StockLevelModel.class);
			this.tableName = composedTypeModel.getTable();
			this.pkCol = typeService.getAttributeDescriptor(composedTypeModel, "pk").getDatabaseColumn();
			this.warehouseCol = typeService.getAttributeDescriptor(composedTypeModel, "warehouse").getDatabaseColumn();
			this.sequenceIdCol = typeService.getAttributeDescriptor(composedTypeModel, "sequenceId").getDatabaseColumn();
		}
	}

	protected static class WarehouseDBMetaData
	{
		private final String tableName;
		private final String pkCol;
		private final String codeCol;

		protected WarehouseDBMetaData(final TypeService typeService)
		{
			ComposedTypeModel composedTypeModel = typeService.getComposedTypeForClass(WarehouseModel.class);
			this.tableName = composedTypeModel.getTable();
			this.pkCol = typeService.getAttributeDescriptor(composedTypeModel, "pk").getDatabaseColumn();
			this.codeCol = typeService.getAttributeDescriptor(composedTypeModel, "code").getDatabaseColumn();
		}
	}

	private void prepareDBMetaData()
	{
		if (stockLevelDBMetaData == null)
		{
			stockLevelDBMetaData = new StockLevelDBMetaData(typeService);
		}
		if (warehouseDBMetaData == null)
		{
			warehouseDBMetaData = new WarehouseDBMetaData(typeService);
		}
	}

}
