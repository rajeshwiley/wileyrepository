package com.wiley.core.healthcheck.checks;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.health.HealthCheck;


public class WileyTnsHealthCheck extends HealthCheck
{
	private static final Logger LOG = Logger.getLogger(WileyTnsHealthCheck.class);

	private static final String TNS_CHECK_GATEWAY = "/api/rest/version/47/information";
	private static final String TNS_HTTP_PROTOCOL = "https://";
	private static final String SUCCESSFUL_RESPONSE_STATUS = "OPERATING";

	@Autowired
	private RestTemplate restTemplate;

	private String tnsUrl;

	@Override
	protected Result check() throws Exception
	{
		try
		{
			final ResponseEntity<LinkedHashMap> response = restTemplate.getForEntity(getComposedUrl(), LinkedHashMap.class);

			if (response.getStatusCode().equals(HttpStatus.OK))
			{
				LinkedHashMap<String, String> responseMap = response.getBody();

				if (responseMap.get("status").equals(SUCCESSFUL_RESPONSE_STATUS))
				{
					return Result.healthy("TNS gateway is available!");
				}
			}
		}
		catch (Exception e)
		{
			LOG.debug("During TNS healthCheck has ben occurred error!", e);
		}

		return Result.unhealthy("TNS gateway is not available!");
	}

	private String getComposedUrl()
	{
		return TNS_HTTP_PROTOCOL + getTnsUrl() + TNS_CHECK_GATEWAY;
	}

	public String getTnsUrl()
	{
		return tnsUrl;
	}

	@Required
	public void setTnsUrl(final String tnsUrl)
	{
		this.tnsUrl = tnsUrl;
	}
}