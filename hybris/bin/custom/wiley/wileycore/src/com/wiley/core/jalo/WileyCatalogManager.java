/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.jalo;

import de.hybris.platform.catalog.constants.GeneratedCatalogConstants;
import de.hybris.platform.catalog.jalo.CatalogItem;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.europe1.jalo.DiscountRow;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.jalo.type.AttributeDescriptor;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.Utilities;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * Extended implementation of ootb CatalogManagor Jalo class with following customization :
 *
 * setSupercategories/getSupercategories : To make CategoryProductRelation target ordered.
 */
public class WileyCatalogManager extends CatalogManager
{
	@Override
	public void setSupercategories(final SessionContext ctx, final Product item,
			final Collection<Category> value)
	{
		item.setLinkedItems(ctx, false, GeneratedCatalogConstants.Relations.CATEGORYPRODUCTRELATION,
				null, value,
				Utilities.getRelationOrderingOverride(CATEGORYPRODUCTRELATION_SRC_ORDERED, true),
				Utilities.getRelationOrderingOverride(CATEGORYPRODUCTRELATION_TGT_ORDERED, true),
				Utilities.getMarkModifiedOverride(CATEGORYPRODUCTRELATION_MARKMODIFIED));
	}

	@Override
	public Collection<Category> getSupercategories(final SessionContext ctx, final Product item)
	{
		final List items = item.getLinkedItems(ctx, false,
				GeneratedCatalogConstants.Relations.CATEGORYPRODUCTRELATION, "Category", null,
				Utilities.getRelationOrderingOverride(CATEGORYPRODUCTRELATION_SRC_ORDERED, true),
				Utilities.getRelationOrderingOverride(CATEGORYPRODUCTRELATION_TGT_ORDERED, true));

		return items;
	}

	/**
	 * Adjusted OOTB. Counterpart target discount row is searched by corresponding product from target catalog version.
	 */
	@Override
	public Item getCounterpartItem(final SessionContext ctx, final Item sourceItem, final CatalogVersion targetVersion)
			throws JaloInvalidParameterException
	{
		ComposedType comptyp = sourceItem.getComposedType();
		if (!this.isCatalogItem(comptyp))
		{
			throw new JaloInvalidParameterException(
					"source item " + sourceItem + " is no instance of a catalog item type but of " + comptyp.getCode(), 0);
		}
		else if (sourceItem instanceof CatalogItem)
		{
			return ((CatalogItem) sourceItem).getCounterpartItem(targetVersion);
		}
		else
		{
			AttributeDescriptor versionAD = this.getCatalogVersionAttribute(ctx, comptyp);
			Collection keyAttributes = this.getUniqueKeyAttributes(ctx, comptyp);
			if (keyAttributes != null && !keyAttributes.isEmpty())
			{
				StringBuilder strbuil = new StringBuilder();
				strbuil.append("SELECT {").append(Item.PK).append("} FROM {").append(comptyp.getCode()).append("} ");
				strbuil.append("WHERE {").append(versionAD.getQualifier()).append("}=?tgtVer ");
				HashMap params = new HashMap();
				params.put("tgtVer", targetVersion);
				Iterator var10 = keyAttributes.iterator();

				while (var10.hasNext())
				{
					AttributeDescriptor items = (AttributeDescriptor) var10.next();
					if (!items.isSearchable())
					{
						throw new JaloInvalidParameterException(
								"key attribute " + items.getQualifier() + " out of " + keyAttributes + " of catalog item type "
										+ comptyp.getCode() + " is not searchable", 0);
					}

					try
					{
						Object e = getSourceItemAttributeValue(ctx, sourceItem, items.getQualifier(), targetVersion);
						strbuil.append(" AND {").append(items.getQualifier()).append("}");
						if (e == null)
						{
							strbuil.append(" IS NULL ");
						}
						else
						{
							String token = items.getQualifier() + "KeyValue";
							strbuil.append("=?").append(token).append(" ");
							params.put(token, e);
						}
					}
					catch (Exception var13)
					{
						throw new JaloInvalidParameterException(var13, 0);
					}
				}

				strbuil.append("ORDER BY {").append(Item.CREATION_TIME).append("} DESC");
				List items1 = FlexibleSearch.getInstance().search(strbuil.toString(), params, comptyp.getJaloClass()).getResult();
				return items1.isEmpty() ? null : (Item) items1.get(0);
			}
			else
			{
				throw new JaloInvalidParameterException("no key attribute(s) defined for catalog item type " + comptyp.getCode(),
						0);
			}
		}
	}

	/**
	 * Counterpart target discount row is searched by corresponding product from target catalog version.
	 */
	protected Object getSourceItemAttributeValue(final SessionContext ctx, final Item sourceItem, final String qualifier,
			final CatalogVersion targetVersion)
			throws JaloSecurityException
	{
		Object result = sourceItem.getAttribute(ctx, qualifier);
		if (sourceItem instanceof DiscountRow && DiscountRowModel.PRODUCT.equals(qualifier) && result instanceof Item)
		{
			result = getCounterpartItem(ctx, (Item) result, targetVersion);
		}
		return result;
	}
}
