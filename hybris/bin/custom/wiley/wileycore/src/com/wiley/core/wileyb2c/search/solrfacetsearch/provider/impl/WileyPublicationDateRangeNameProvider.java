package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.provider.impl.DefaultRangeNameProvider;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyPublicationDateRangeNameProvider extends DefaultRangeNameProvider
{
	@Override
	public List<ValueRange> getValueRanges(final IndexedProperty property, final String qualifier)
	{

		final List<ValueRange> valueRanges = super.getValueRanges(property, qualifier);
		final List<ValueRange> convertedValueRanges = new ArrayList<>();
		for (final ValueRange valueRange : valueRanges)
		{
			convertedValueRanges.add(convertValueRange(valueRange));
		}
		return convertedValueRanges;
	}

	private ValueRange convertValueRange(final ValueRange valueRange)
	{
		final ValueRange convertedValueRange = new ValueRange();
		convertedValueRange.setFrom(convertToDays(valueRange.getFrom()));
		convertedValueRange.setTo(convertToDays(valueRange.getTo()));
		convertedValueRange.setName(valueRange.getName());
		return convertedValueRange;
	}

	private String convertToDays(final Comparable value)
	{
		if (value == null)
		{
			return null;
		}
		final String to = (String) value;
		final String[] parts = to.split(" ");
		if (parts.length != 2)
		{
			throw new IllegalStateException("Illegal format " + value + " for publication date range");
		}


		return String.valueOf(ChronoUnit.DAYS.between(getNow(),
				getNow().plusYears(Integer.parseInt(parts[0])).plusDays(Integer.parseInt(parts[1]))));
	}

	protected LocalDate getNow()
	{
		return LocalDate.now();
	}
}
