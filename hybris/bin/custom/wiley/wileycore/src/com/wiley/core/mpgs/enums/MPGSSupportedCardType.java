package com.wiley.core.mpgs.enums;

import java.util.HashMap;
import java.util.Map;

public enum MPGSSupportedCardType
{
	VISA("VISA"),
	MASTER("MASTERCARD"),
	AMEX("AMEX"),
	DISCOVER("DISCOVER");

	private final String stringValue;

	private static final Map<String, MPGSSupportedCardType> LOOKUP = new HashMap<>();

	static
	{
		for (final MPGSSupportedCardType cardType : MPGSSupportedCardType.values())
		{
			LOOKUP.put(cardType.getStringValue(), cardType);
		}
	}

	MPGSSupportedCardType(final String stringValue)
	{
		this.stringValue = stringValue;
	}

	public String getStringValue()
	{
		return this.stringValue;
	}

	public static MPGSSupportedCardType get(final String stringValue)
	{
		return LOOKUP.get(stringValue);
	}
}
