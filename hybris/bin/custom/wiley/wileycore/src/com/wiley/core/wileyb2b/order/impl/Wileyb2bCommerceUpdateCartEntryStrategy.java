package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;


/**
 * Wileyb2b specific update cart entry strategy.
 */
public class Wileyb2bCommerceUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCommerceUpdateCartEntryStrategy.class);

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	public CommerceCartModification updateQuantityForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		return super.updateQuantityForCartEntry(parameters);
	}

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	public CommerceCartModification updatePointOfServiceForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		return super.updatePointOfServiceForCartEntry(parameters);
	}

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	public CommerceCartModification updateToShippingModeForCartEntry(final CommerceCartParameter parameters)
			throws CommerceCartModificationException
	{
		return super.updateToShippingModeForCartEntry(parameters);
	}

	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return quantityToAdd;
	}

	@Override
	protected CommerceCartModification modifyEntry(final CartModel cartModel, final AbstractOrderEntryModel entryToUpdate,
			final long actualAllowedQuantityChange, final long newQuantity, final Integer maxOrderQuantity)
	{
		// Now work out how many that leaves us with on this entry
		final long entryQuantity = entryToUpdate.getQuantity().longValue();
		final long entryNewQuantity = entryQuantity + actualAllowedQuantityChange;

		final ModelService modelService = getModelService();

		if (entryNewQuantity <= 0)
		{
			final CartEntryModel entry = new CartEntryModel();
			entry.setProduct(entryToUpdate.getProduct());

			// The allowed new entry quantity is zero or negative
			// just remove the entry
			modelService.remove(entryToUpdate);
			modelService.refresh(cartModel);
			normalizeEntryNumbers(cartModel);
			getCommerceCartCalculationStrategy().calculateCart(createCommerceCartParameter(cartModel));

			// Return an empty modification
			final CommerceCartModification modification = new CommerceCartModification();
			modification.setEntry(entry);
			modification.setQuantity(0);
			// We removed all the quantity from this row
			modification.setQuantityAdded(-entryQuantity);

			if (newQuantity == 0)
			{
				modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			}
			else
			{
				modification.setStatusCode(CommerceCartModificationStatus.LOW_STOCK);
			}

			return modification;
		}
		else
		{
			// Adjust the entry quantity to the new value
			entryToUpdate.setQuantity(Long.valueOf(entryNewQuantity));
			entryToUpdate.setCalculated(false);
			cartModel.setCalculated(false);

			final ProductModel originalEntryProduct = entryToUpdate.getProduct();

			final CartEntryModel entryBeforeRecalculation = createCartEntryStub(entryToUpdate.getEntryNumber(),
					entryToUpdate.getProduct());

			final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModel);
			getCommerceCartCalculationStrategy().calculateCart(commerceCartParameter);

			final Optional<ExternalCartModification> externalModificationForEntry =
					externalCartModificationsStorageService.popLastModificationForCartAndEntry(cartModel,
							entryBeforeRecalculation);

			// Return the modification data
			final CommerceCartModification modification = new CommerceCartModification();
			if (externalModificationForEntry.isPresent())
			{
				final CartEntryModel entry = createCartEntryStub(null, originalEntryProduct);
				modification.setEntry(entry);
				final ExternalCartModification externalModification = externalModificationForEntry.get();
				modification.setQuantityAdded(externalModification.getQuantity() - entryQuantity);
				modification.setQuantity(externalModification.getQuantity());
				modification.setStatusCode(externalModification.getStatusCode());
				modification.setMessage(externalModificationForEntry.get().getMessage());
				modification.setMessageType(externalModificationForEntry.get().getMessageType());
			}
			else
			{
				modification.setQuantityAdded(actualAllowedQuantityChange);
				modification.setEntry(entryToUpdate);
				modification.setQuantity(entryNewQuantity);
				modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			}

			return modification;
		}
	}

	private CommerceCartParameter createCommerceCartParameter(final CartModel cartModel)
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		return parameter;
	}

	private CartEntryModel createCartEntryStub(final Integer entryNumber, final ProductModel productModel)
	{
		final CartEntryModel stub = new CartEntryModel();
		stub.setEntryNumber(entryNumber);
		stub.setProduct(productModel);
		return stub;
	}
}
