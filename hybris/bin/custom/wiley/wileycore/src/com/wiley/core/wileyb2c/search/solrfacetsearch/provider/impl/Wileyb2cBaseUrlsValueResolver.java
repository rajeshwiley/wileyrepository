package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductUrlsValueResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.variants.model.VariantProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cBaseUrlsValueResolver extends ProductUrlsValueResolver
{

	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final ProductModel product,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		if (product instanceof VariantProductModel)
		{
			super.addFieldValues(document, batchContext, indexedProperty, ((VariantProductModel) product).getBaseProduct(),
					resolverContext);
		}
		else
		{
			throw new FieldValueProviderException("Cannot resolve value for non variant product");
		}
	}
}
