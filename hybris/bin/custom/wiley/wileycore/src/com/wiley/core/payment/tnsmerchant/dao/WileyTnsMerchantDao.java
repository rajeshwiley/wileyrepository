package com.wiley.core.payment.tnsmerchant.dao;

import java.util.List;

import com.wiley.core.model.WileyTnsMerchantModel;


public interface WileyTnsMerchantDao
{
	List<WileyTnsMerchantModel> findTnsMerchant(String countryIsoCode, String currencyIsoCode);
}
