package com.wiley.core.payment.response.validators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.validators.PayPalExpressCheckoutResponseValidator;
import com.wiley.core.i18n.WileyCommonI18NService;


public class PayPalExpressCheckoutResponseCurrencyValidator implements PayPalExpressCheckoutResponseValidator
{

	private static final String CURRENCY_ERROR_LONG_MESSAGE =
			"The billing country in PayPal must match the billing country on your order. "
					+ "Please update your billing country before returning to PayPal.";
	private static final String CURRENCY_ERROR_SHORT_MESSAGE = "Invalid Currency selected on PayPal";
	@Resource(name = "wileyCommonI18NService")

	private WileyCommonI18NService wileyCommonI18NService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Override
	public boolean validateResponse(final GetExpressCheckoutDetailsResultData responseData)
	{
		AddressData billingAddress = responseData.getBillingAddress();
		Preconditions.checkNotNull(billingAddress, "Billing address from PayPal can't be null");

		CountryModel paypalCountry = commonI18NService.getCountry(billingAddress.getCountry().getIsocode());
		final CartModel sessionCart = cartService.getSessionCart();
		CurrencyModel cartCurrency = sessionCart.getCurrency();
		CurrencyModel paypalCountryCurrency = wileyCommonI18NService.getDefaultCurrency(paypalCountry);
		boolean currencyMatch = cartCurrency.equals(paypalCountryCurrency);
		if (!currencyMatch) {
			responseData.setAck(PaypalConstants.STATUS_ERROR);
			if (responseData.getErrors() == null) {
				responseData.setErrors(new ArrayList<>());
			}
			ResultErrorData error = new ResultErrorData();
			error.setLongMessage(
					CURRENCY_ERROR_LONG_MESSAGE);
			error.setShortMessage(CURRENCY_ERROR_SHORT_MESSAGE);
			error.setErrorCode(PayPalExpressCheckoutResponseCurrencyValidator.class.getName());
			responseData.getErrors().add(error);
		}

		return currencyMatch;
	}
}
