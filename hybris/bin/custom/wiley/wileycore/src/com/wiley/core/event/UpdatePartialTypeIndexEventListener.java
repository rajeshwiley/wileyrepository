package com.wiley.core.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.WileyIndexerService;


/**
 * Created by Raman_Hancharou on 4/11/2017.
 */
public class UpdatePartialTypeIndexEventListener extends AbstractEventListener<UpdatePartialTypeIndexEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdatePartialTypeIndexEventListener.class);

	@Resource
	private WileyIndexerService wileyIndexerService;

	@Override
	protected void onEvent(final UpdatePartialTypeIndexEvent event)
	{
		try
		{
			wileyIndexerService.updatePartialTypeIndex(event.getFacetSearchConfig(), event.getIndexedType(),
					event.getIndexedProperties(), event.getPks(), event.getCountries());
		}
		catch (IndexerException e)
		{
			LOG.error("Error while updating partial type: " + e.getMessage());
		}
	}
}
