package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.BackgroundImageHasRequiredFormats;


public class BackgroundImageHasRequiredFormatsValidator
		extends AbstractWileyProductConstraintValidator<BackgroundImageHasRequiredFormats>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validateBackgroundImageContainerContainsRequiredFormats(productModel.getBackgroundImage());
	}
}
