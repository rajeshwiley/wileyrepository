package com.wiley.core.promotions.actions.conditions;

import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;

public class WileyPartnerOrderConditionTranslator implements RuleConditionTranslator
{

	public static final String PARTNER_ID = "partnerId";

	@Override
	public RuleIrCondition translate(final RuleCompilerContext context,
									 final RuleConditionData ruleConditionData,
									 final RuleConditionDefinitionData ruleConditionDefinitionData)
			throws RuleCompilerException
	{

		String cartRaoVariable = context.generateVariable(CartRAO.class);

		RuleIrAttributeCondition irPartnerCondition = new RuleIrAttributeCondition();
		irPartnerCondition.setVariable(cartRaoVariable);
		irPartnerCondition.setAttribute(PARTNER_ID);
		irPartnerCondition.setOperator(RuleIrAttributeOperator.NOT_EQUAL);
		irPartnerCondition.setValue(null);
		return irPartnerCondition;
	}
}
