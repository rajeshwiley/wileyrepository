package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Set;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cResolvePublicationDateStrategy extends AbstractWileyb2cResolveClassificationAttributeStrategy
{
	static final Set<Wileyb2cClassificationAttributes> CLASSIFICATION_ATTRIBUTES =
			Collections.singleton(Wileyb2cClassificationAttributes.PUBLICATION_DATE);

	@Override
	protected String processFeature(final Feature feature)
	{
		return new SimpleDateFormat("MMM yyyy").format(feature.getValue().getValue());
	}

	@Override
	protected Set<Wileyb2cClassificationAttributes> getAttributes()
	{
		return CLASSIFICATION_ATTRIBUTES;
	}

}