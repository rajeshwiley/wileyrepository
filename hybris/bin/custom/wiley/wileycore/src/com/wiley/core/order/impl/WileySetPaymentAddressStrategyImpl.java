package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.order.WileySetPaymentAddressStrategy;


public class WileySetPaymentAddressStrategyImpl implements WileySetPaymentAddressStrategy
{
	@Resource
	private ModelService modelService;
	@Resource
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Override
	public void setPaymentAddress(@Nonnull final CommerceCheckoutParameter commerceCheckoutParameter)
	{
		final CartModel cartModel = commerceCheckoutParameter.getCart();
		final CustomerModel currentUser = checkoutCustomerStrategy.getCurrentUserForCheckout();
		final AddressModel addressModel = commerceCheckoutParameter.getAddress();
		modelService.refresh(cartModel);
		cartModel.setPaymentAddress(addressModel);
		currentUser.setDefaultPaymentAddress(addressModel);
		modelService.save(cartModel);
	}
}
