package com.wiley.core.discount.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.discount.WileyExternalDiscountService;
import com.wiley.core.model.WileyExternalDiscountModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;


public class WileyExternalDiscountServiceImpl implements WileyExternalDiscountService
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public WileyExternalDiscountModel getWileyExternalDiscountForGuid(final String guid)
	{
		validateParameterNotNullStandardMessage("guid", guid);

		List<WileyExternalDiscountModel> externalDiscounts = findWileyExternalDiscountsForGuid(guid);

		validateIfSingleResult(externalDiscounts,
				format("There is no wileyExternalDiscount with guid [%s]", guid),
				format("More then one wileyExternalDiscount found for guid [%s]", guid));

		return externalDiscounts.get(0);
	}

	@Override
	public boolean isWileyExternalDiscountExistedForGuid(final String guid)
	{
		validateParameterNotNullStandardMessage("guid", guid);

		List<WileyExternalDiscountModel> externalDiscounts = findWileyExternalDiscountsForGuid(guid);

		return CollectionUtils.isNotEmpty(externalDiscounts);
	}

	List<WileyExternalDiscountModel> findWileyExternalDiscountsForGuid(final String guid)
	{
		final WileyExternalDiscountModel example = new WileyExternalDiscountModel();
		example.setGuid(guid);
		return flexibleSearchService.getModelsByExample(example);
	}
}
