package com.wiley.core.wileycom.order;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * Interface extends {@link DeliveryService} with b2c specific methods
 */
public interface WileycomDeliveryService extends DeliveryService
{
	/**
	 * Get the supported external delivery modes for the abstract order.
	 *
	 * @param abstractOrder
	 * @return the list of supported externaldelivery modes, by default sorted by cost
	 */
	List<ExternalDeliveryModeModel> getSupportedExternalDeliveryModeListForOrder(
			@Nonnull AbstractOrderModel abstractOrder);


	/**
	 * finds external delivery mode by passed code from passed deliveryModes
	 *
	 * @param currentDeliveryModeCode
	 * @param externalDeliveryModes
	 * @return found external delivery mode
	 */
	Optional<ExternalDeliveryModeModel> findDeliveryModeByExternalCode(@Nonnull String currentDeliveryModeCode,
			@Nonnull List<ExternalDeliveryModeModel> externalDeliveryModes);

	/**
	 * Gets first delivery mode from passed deliveryMode list
	 *
	 * @param externalDeliveryModes
	 * 		list of available modes
	 * @return first dilivery mode from passed list
	 */
	@Nonnull
	ExternalDeliveryModeModel getDefaultDeliveryMode(@Nonnull List<ExternalDeliveryModeModel> externalDeliveryModes);
}
