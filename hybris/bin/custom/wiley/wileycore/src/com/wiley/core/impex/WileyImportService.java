package com.wiley.core.impex;

import de.hybris.platform.impex.model.cronjob.ImpExImportCronJobModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.impl.DefaultImportService;
import de.hybris.platform.servicelayer.impex.impl.ImportCronJobResult;

import javax.annotation.Resource;


public class WileyImportService extends DefaultImportService
{
	public static final String IMPORT_JOB_CODE = "WileyHotFolder-ImpEx-Import";
	public static final String IMPORT_CRON_JOB = "ImpExImportCronJob";

	@Resource
	private CronJobService cronJobService;

	/**
	 * OOTB_CODE
	 * Overrides OOTB importData to create ImpExImportCronJob with WileyHotFolder-ImpEx-Import job.
	 */
	@Override
	public ImportResult importData(final ImportConfig config)
	{
		ImpExImportCronJobModel cronJob = (ImpExImportCronJobModel) this.getModelService().create(IMPORT_CRON_JOB);
		cronJob.setJob(cronJobService.getJob(IMPORT_JOB_CODE));

		this.configureCronJob(cronJob, config);
		this.getModelService().saveAll(new Object[] { cronJob.getJob(), cronJob });
		this.importData(cronJob, config.isSynchronous(), config.isRemoveOnSuccess());
		return ((Item) this.getModelService().getSource(cronJob)).isAlive() ?
				new ImportCronJobResult(cronJob) :
				new ImportCronJobResult((ImpExImportCronJobModel) null);
	}

	//Overrides OOTB to mock in test
	@Override
	protected void importData(final ImpExImportCronJobModel cronJob, final boolean synchronous, final boolean removeOnSuccess)
	{
		super.importData(cronJob, synchronous, removeOnSuccess);
	}

	//Overrides OOTB to mock in test
	@Override
	protected void configureCronJob(final ImpExImportCronJobModel cronJob, final ImportConfig config)
	{
		super.configureCronJob(cronJob, config);
	}

	@Override
	public void setCronJobService(final CronJobService cronJobService)
	{
		super.setCronJobService(cronJobService);
		this.cronJobService = cronJobService;
	}
}
