package com.wiley.core.address;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.user.AddressService;

import java.util.Optional;


/**
 * The interface Wiley address service.
 */
public interface WileyAddressService extends AddressService
{
	/**
	 * Returns address based on it's id
	 *
	 * @param addressId
	 * @return 
	 */
	Optional<AddressModel> getAddressById(String addressId);

	/**
	 * Returns address based on it's externalId
	 *
	 * @param addressExternalId
	 * @return
	 */
	Optional<AddressModel> getAddressByExternalIdAndOwner(String addressExternalId, String ownerId);


	/**
	 * Update address with transaction id.
	 *
	 * @param address
	 * 		the address
	 * @param transactionId
	 * 		the transaction id
	 */
	void updateAddressWithTransactionId(AddressModel address, String transactionId);
}
