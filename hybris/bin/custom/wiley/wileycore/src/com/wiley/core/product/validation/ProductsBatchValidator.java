package com.wiley.core.product.validation;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.jobs.ProductsValidationJobPerformable;


/**
 * Validates batch of products.
 * For more details please see {@link ProductsValidationJobPerformable}
 */
public interface ProductsBatchValidator
{
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	CronJobResult validateProducts(List<ProductModel> idsBatch, CronJobModel cronJob,
			List<ConstraintGroupModel> constraintGroups);
}
