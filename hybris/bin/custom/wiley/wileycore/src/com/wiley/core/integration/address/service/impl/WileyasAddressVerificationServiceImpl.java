package com.wiley.core.integration.address.service.impl;

import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.integration.address.WileyAddressesGateway;
import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.dto.AddressVerificationResultDto;
import com.wiley.core.integration.address.service.WileyasAddressVerificationService;


public class WileyasAddressVerificationServiceImpl implements WileyasAddressVerificationService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasAddressVerificationServiceImpl.class);

	private WileyCountryService wileyCountryService;

	private WileyCommonI18NService wileyCommonI18NService;

	private WileyAddressesGateway wileyAddressesGateway;

	private ValidationService validationService;

	private Set<String> acceptCodes;

	private Set<String> rejectCodes;

	private Set<String> knownCodes;

	private int suggestedAddressesLimit;

	private static final Comparator<String> FIELD_COMPARATOR = Comparator.comparing(StringUtils::trimToEmpty);

	private static final Comparator<AddressDto> ADDRESS_DTO_COMPARATOR = Comparator.comparing(
			AddressDto::getLine1, FIELD_COMPARATOR)
			.thenComparing(AddressDto::getLine2, FIELD_COMPARATOR)
			.thenComparing(AddressDto::getTown, FIELD_COMPARATOR)
			.thenComparing(AddressDto::getRegionIso2, FIELD_COMPARATOR)
			.thenComparing(AddressDto::getPostalCode, FIELD_COMPARATOR)
			.thenComparing(AddressDto::getCountryIso2, FIELD_COMPARATOR);

	@PostConstruct
	public void createKnownCodesSet()
	{
		this.knownCodes = new HashSet<>();
		knownCodes.addAll(acceptCodes);
		knownCodes.addAll(rejectCodes);
	}

	@Override
	public AddressVerificationResultDto verifyAddress(final AddressDto userAddress)
	{
		final List<AddressDto> suggestions = wileyAddressesGateway.getSuggestions(userAddress);

		final AddressVerificationResultDto verificationResult = new AddressVerificationResultDto();

		final List<AddressDto> sortedSuggestions = sortSuggestions(suggestions);

		final AddressVerificationDecision decision = getAddressVerificationDecision(sortedSuggestions);

		final List<AddressDto> filteredSuggestions = filterSuggestions(sortedSuggestions, userAddress);
		verificationResult.setSuggestedAddresses(filteredSuggestions);
		verificationResult.setDecision(decision);

		if (decision == AddressVerificationDecision.REJECT)
		{
			verificationResult.setSuggestedAddresses(Collections.singletonList(userAddress));
		}
		return verificationResult;
	}

	private boolean isValidAddressRegion(final AddressDto address)
	{
		final String countryIso2 = address.getCountryIso2();
		final String regionIso2 = address.getRegionIso2();
		if (StringUtils.isNotBlank(countryIso2) && StringUtils.isBlank(regionIso2))
		{
			return true;
		}

		final CountryModel countryModel = wileyCountryService.findCountryByCode(countryIso2);
		try
		{
			wileyCommonI18NService.getRegionForShortCode(countryModel, regionIso2);
			return true;
		}
		catch (final UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			if (CollectionUtils.isNotEmpty(countryModel.getRegions()))
			{
				// We support regions for this country. Absent region is error case
				LOG.error(e.getMessage(), e);
				return false;
			}
			else
			{
				// We don't support regions for this country. Just log issue with region and continue without it
				LOG.debug(e.getMessage());
				address.setRegionIso2(null);
				return true;
			}
		}
	}

	private boolean isValidAddressDto(final AddressDto address)
	{
		final Set<HybrisConstraintViolation> violations = validationService.validate(address);
		if (CollectionUtils.isNotEmpty(violations))
		{
			for (HybrisConstraintViolation violation : violations)
			{
				final ConstraintViolation constraintViolation = violation.getConstraintViolation();
				LOG.debug(violation.getLocalizedMessage(), constraintViolation.getPropertyPath(),
						constraintViolation.getInvalidValue());
			}
			return false;
		}
		return true;
	}

	private AddressVerificationDecision getAddressVerificationDecision(final List<AddressDto> sortedSuggestions)
	{
		Set<String> decisions = getDecisionCodes(sortedSuggestions);
		AddressVerificationDecision decision;
		if (acceptCodes.containsAll(decisions))
		{
			decision = AddressVerificationDecision.REVIEW;
		}
		else if (!Collections.disjoint(rejectCodes, decisions))
		{
			decision = AddressVerificationDecision.REJECT;
		}
		else
		{
			decision = AddressVerificationDecision.UNKNOWN;
		}
		return decision;
	}

	private Set<String> getDecisionCodes(final List<AddressDto> sortedSuggestions)
	{
		Set<String> decisions = new HashSet<>();
		for (AddressDto addr : sortedSuggestions)
		{
			String decisionCode = addr.getDecision();
			decisions.add(decisionCode);

			if (!knownCodes.contains(decisionCode))
			{
				LOG.warn("Unknown decision code for address: {}", addr);
			}
		}
		return decisions;
	}

	private List<AddressDto> sortSuggestions(final List<AddressDto> suggestions)
	{
		return suggestions.stream().sorted(
				Comparator.comparing(AddressDto::getRelevance, Comparator.nullsLast(Comparator.reverseOrder()))
						.thenComparing(AddressDto::getLine1, Comparator.nullsLast(Comparator.naturalOrder())))
				.limit(suggestedAddressesLimit)
				.collect(Collectors.toList());
	}

	public static <T> Predicate<T> distinct(final Function<? super T, AddressDto> keyExtractor)
	{
		final Set<AddressDto> seen = new TreeSet<>(ADDRESS_DTO_COMPARATOR);
		return t -> seen.add(keyExtractor.apply(t));
	}

	private List<AddressDto> filterSuggestions(final List<AddressDto> suggestions,
			final AddressDto userAddress)
	{
		return suggestions.stream().sequential()
				.filter(this::isValidAddressRegion)
				.filter(this::isValidAddressDto)
				.filter(suggestedAddress -> ADDRESS_DTO_COMPARATOR.compare(userAddress, suggestedAddress) != 0)
				.filter(distinct(suggestedAddress -> suggestedAddress))
				.collect(Collectors.toList());
	}

	@Required
	public void setWileyAddressesGateway(final WileyAddressesGateway wileyAddressesGateway)
	{
		this.wileyAddressesGateway = wileyAddressesGateway;
	}

	@Required
	public void setSuggestedAddressesLimit(final int suggestedAddressesLimit)
	{
		this.suggestedAddressesLimit = suggestedAddressesLimit;
	}

	public void setAcceptCodes(final Set<String> acceptCodes)
	{
		this.acceptCodes = acceptCodes;
	}

	public void setRejectCodes(final Set<String> rejectCodes)
	{
		this.rejectCodes = rejectCodes;
	}

	@Required
	public void setWileyCountryService(final WileyCountryService wileyCountryService)
	{
		this.wileyCountryService = wileyCountryService;
	}

	@Required
	public void setWileyCommonI18NService(final WileyCommonI18NService wileyCommonI18NService)
	{
		this.wileyCommonI18NService = wileyCommonI18NService;
	}

	@Required
	public void setValidationService(final ValidationService validationService)
	{
		this.validationService = validationService;
	}
}
