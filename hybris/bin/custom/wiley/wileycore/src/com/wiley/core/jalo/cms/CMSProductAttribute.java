package com.wiley.core.jalo.cms;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;


public class CMSProductAttribute extends GeneratedCMSProductAttribute
{
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		return super.createItem(ctx, type, allAttributes);
	}

}
