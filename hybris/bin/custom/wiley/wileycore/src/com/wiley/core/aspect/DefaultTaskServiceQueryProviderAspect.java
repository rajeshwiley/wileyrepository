package com.wiley.core.aspect;

import de.hybris.platform.task.impl.DefaultWileyTaskServiceQueryProvider;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

@Aspect
public class DefaultTaskServiceQueryProviderAspect
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultTaskServiceQueryProviderAspect.class);

    private final DefaultWileyTaskServiceQueryProvider wileyTaskServiceQueryProvider = new DefaultWileyTaskServiceQueryProvider();

    @Around("execution(* de.hybris.platform.task.impl.DefaultTaskServiceQueryProvider.getValidTasksToExecuteQuery(..))")
    public String aroundCreateDefaultWileyTaskServiceQueryProvider(final ProceedingJoinPoint proceedingJoinPoint)
    {
        LOG.debug("Starting intercept DefaultTaskServiceQueryProvider.getValidTasksToExecuteQuery");
        return wileyTaskServiceQueryProvider.getValidTasksToExecuteQuery(
                (Collection) proceedingJoinPoint.getArgs()[0],
                (boolean) proceedingJoinPoint.getArgs()[1],
                (boolean) proceedingJoinPoint.getArgs()[2],
                (boolean) proceedingJoinPoint.getArgs()[3]);
    }

    @Around("execution(* de.hybris.platform.task.impl.DefaultTaskServiceQueryProvider.setQueryParameters(..))")
    public Map<String, Object> aroundSetQueryParameters(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable
    {
        LOG.debug("Starting intercept DefaultTaskServiceQueryProvider.setQueryParameters");
        return wileyTaskServiceQueryProvider.setQueryParameters(
                (Collection) proceedingJoinPoint.getArgs()[0],
                (boolean) proceedingJoinPoint.getArgs()[1],
                (Integer) proceedingJoinPoint.getArgs()[2],
                (Integer) proceedingJoinPoint.getArgs()[3],
                (Set) proceedingJoinPoint.getArgs()[4],
                (Duration) proceedingJoinPoint.getArgs()[5]);
    }
}
