package com.wiley.core.mpgs.dto.capture;


import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.wiley.core.mpgs.dto.json.Error;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MPGSCaptureResponseDTO
{
	private Transaction transaction;
	private Response response;
	private String result;
	private Error error;
	private String timeOfRecord;

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}

	public Response getResponse()
	{
		return response;
	}

	public void setResponse(final Response response)
	{
		this.response = response;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}

	public Error getError()
	{
		return error;
	}

	public void setError(final Error error)
	{
		this.error = error;
	}

	public String getTimeOfRecord()
	{
		return timeOfRecord;
	}

	public void setTimeOfRecord(final String timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}
}