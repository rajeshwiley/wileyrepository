package com.wiley.core.integration.ebp.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Created by Uladzimir_Barouski on 2/18/2016.
 */
@XmlRootElement(namespace = "www.efficientlearning.com/wsdl", name = "addProductsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addProductsResponse")
public class EbpAddProductsResponse
{
	@XmlElement(required = true, name = "return")
	private MessageWSResponse response;

	public MessageWSResponse getResponse()
	{
		return response;
	}

	public void setResponse(final MessageWSResponse response)
	{
		this.response = response;
	}
}