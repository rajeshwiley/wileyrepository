package com.wiley.core.integration.edi.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.strategy.EdiOrderProvisionStrategy;
import com.wiley.core.model.EdiExportCronJobModel;


public abstract class WileyAbstractExportJob extends AbstractJobPerformable<EdiExportCronJobModel>
{
	@Resource(name = "ediOrderProvisionStrategy")
	protected EdiOrderProvisionStrategy orderProvisionStrategy;
	@Resource
	protected BaseStoreService baseStoreService;
	@Resource
	protected ConfigurationService configurationService;

	@Override
	public PerformResult perform(final EdiExportCronJobModel cronJobModel)
	{
		final String paymentType = cronJobModel.getPaymentType();
		getSupportedStores(paymentType).forEach(storeCode -> {
			final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(storeCode);
			handleExport(paymentType, baseStore);
		});
		return success();
	}

	protected abstract void handleExport(String paymentType, BaseStoreModel baseStore);

	private Set<String> getSupportedStores(final String paymentType)
	{
		final String ediSupportedStores = String.format(EDIConstants.EDI_SUPPORTED_STORES_PROPERTY_PATTERN,
				paymentType.toLowerCase());
		final String supportedStoresString = configurationService.getConfiguration().getString(ediSupportedStores,
				EDIConstants.EDI_SUPPORTED_STORES_DEFAULT);
		return Stream.of(StringUtils.split(supportedStoresString, ",")).filter(StringUtils::isNotBlank).map(String::trim).collect(
				Collectors.toSet());
	}

	private PerformResult success()
	{
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}