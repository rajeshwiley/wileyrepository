package com.wiley.core.wileyb2c.basesite.theme.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteTheme;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.wiley.core.wileyb2c.basesite.theme.Wileyb2cThemeNameResolutionStrategy;


/**
 * B2C strategy implementation
 */
public class Wileyb2cThemeNameResolutionStrategyImpl implements Wileyb2cThemeNameResolutionStrategy
{

	private String defaultThemeName;

	@Override
	public String resolveThemeName(final BaseSiteModel baseSite)
	{
		Preconditions.checkNotNull(baseSite, "Parameter baseSite should not be null");

		final SiteTheme theme = baseSite.getTheme();
		if (theme != null)
		{
			return theme.getCode();
		}
		return getDefaultThemeName();
	}

	public String getDefaultThemeName()
	{
		return defaultThemeName;
	}

	@Required
	public void setDefaultThemeName(final String defaultThemeName)
	{
		this.defaultThemeName = defaultThemeName;
	}
}
