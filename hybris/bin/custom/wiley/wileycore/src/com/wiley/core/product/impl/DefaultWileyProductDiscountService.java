package com.wiley.core.product.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.AbstractDiscountRowModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.DiscountValue;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.WileyPriceFactory;
import com.wiley.core.order.data.ProductDiscountParameter;
import com.wiley.core.product.WileyProductDiscountService;


/**
 * Default implementation for {@link WileyProductDiscountService}.
 */
public class DefaultWileyProductDiscountService implements WileyProductDiscountService
{
	private WileyPriceFactory wileyPriceFactory;

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private UserService userService;
	
	@Override
	@Nonnull
	public List<DiscountValue> getPotentialDiscountForCurrentCustomer(@Nonnull final ProductModel product,
				@Nonnull final UserDiscountGroup userDiscountGroup)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);

		final CurrencyModel currentCurrency = commerceCommonI18NService.getCurrentCurrency();
		final UserModel currentUser = userService.getCurrentUser();

		return getPotentialDiscountsforUserGroup(product, currentUser, currentCurrency, userDiscountGroup);
	}

	private List<DiscountValue> getPotentialDiscounts(final ProductModel product, final UserDiscountGroup userDiscountGroup,
			final UserModel user, final CurrencyModel currency,
			final Optional<Predicate<AbstractDiscountRowModel>> additionalFilter)
	{

		try
		{
			Date currentTime = new Date();
			boolean isNet = false;

			ProductDiscountParameter parameter = new ProductDiscountParameter();
			parameter.setProduct(product);
			parameter.setUserDiscountGroup(userDiscountGroup);
			parameter.setUser(user);
			parameter.setCurrency(currency);
			parameter.setDate(currentTime);
			parameter.setNet(isNet);
			parameter.setAdditionalFilter(additionalFilter);

			return wileyPriceFactory.getProductDiscountValues(parameter);
		}
		catch (JaloPriceFactoryException e)
		{
			throw new SystemException(e.getMessage(), e);
		}
	}

	private List<DiscountValue> getPotentialDiscountsforUserGroup(final ProductModel product, final UserModel user,
			final CurrencyModel currency, final UserDiscountGroup userDiscountGroup)
	{
		// As far as OOTB returns discounts which are assigned to appropriate
		// discount group and discounts which are not assigned to any discount group, we need to
		// filter discounts without discount group.
		Predicate<AbstractDiscountRowModel> additionalFilter = discountRow -> userDiscountGroup.equals(discountRow.getUg());
		final List<DiscountValue> potentialDiscounts = getPotentialDiscounts(product, userDiscountGroup, user, currency,
				Optional.of(additionalFilter));
		return potentialDiscounts;
	}

	@Required
	public void setWileyPriceFactory(final WileyPriceFactory wileyPriceFactory)
	{
		this.wileyPriceFactory = wileyPriceFactory;
	}
}
