package com.wiley.core.integration.alm.authentication.dto;

import org.springframework.http.HttpStatus;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class AlmAuthenticationResponseDto
{
	private String userId;

	private String imitateeId;

	private HttpStatus httpStatus;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public String getImitateeId()
	{
		return imitateeId;
	}

	public void setImitateeId(final String imitateeId)
	{
		this.imitateeId = imitateeId;
	}

	public HttpStatus getHttpStatus()
	{
		return httpStatus;
	}

	public void setHttpStatus(final HttpStatus httpStatus)
	{
		this.httpStatus = httpStatus;
	}
}
