package com.wiley.core.integration.esb.service;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Contains util methods for working with cart entries.
 */
public interface Wileyb2bCartEntryService
{

	/**
	 * Searches cart entry by product code in cart directly.
	 *
	 * @param cart
	 * @param isbn
	 * @return optional object.
	 */
	@Nonnull
	Optional<CartEntryModel> findCartEntryByIsbn(@Nonnull CartModel cart, @Nonnull String isbn);

}
