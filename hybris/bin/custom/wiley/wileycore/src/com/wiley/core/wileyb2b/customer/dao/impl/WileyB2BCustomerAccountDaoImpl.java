package com.wiley.core.wileyb2b.customer.dao.impl;

import com.wiley.core.wileyb2b.customer.dao.WileyB2BCustomerAccountDao;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.subscriptionservices.daos.impl.DefaultSubscriptionCustomerAccountDao;
import org.apache.commons.lang.ArrayUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WileyB2BCustomerAccountDaoImpl extends DefaultSubscriptionCustomerAccountDao implements WileyB2BCustomerAccountDao
{

	private static final String QUERY_B2B_UNIT_ORDERS =
			"select {o.pk} from {" + B2BUnitModel._TYPECODE + " as u "
					+ "join " + B2BCustomerModel._TYPECODE + " as c on {c." + B2BCustomerModel.DEFAULTB2BUNIT + "} = {u.pk} "
					+ "join " + OrderModel._TYPECODE + " as o on {o." + OrderModel.USER + "} = {c.pk}} "
					+ "WHERE {u.pk} = ?unit AND {o." + OrderModel.VERSIONID + "} IS NULL AND {o."
					+ OrderModel.STORE + "} = ?store "
					+ "AND {o." + OrderModel.PARENT + "} IS NULL ";

	private static final String AND_STATUSES = "AND  {o." + OrderModel.STATUS + "} in {?statusList} ";
	private static final String ORDER_BY_DATE = "ORDER BY {o." + OrderModel.DATE + "} DESC, {pk}";
	private static final String ORDER_BY_CODE = "ORDER BY  {o." + OrderModel.CODE + "} DESC";
	private static final String BY_DATE = "byDate";
	private static final String BY_ORDER_NUMBER = "byOrderNumber";

	@Override
	public SearchPageData<OrderModel> findOrdersByUnitAndStore(final B2BUnitModel b2bUnit,
															   final BaseStoreModel store,
															   final OrderStatus[] statuses,
															   final PageableData pageableData)
	{
		ServicesUtil.validateParameterNotNull(b2bUnit, "B2BUnit must not be null");
		ServicesUtil.validateParameterNotNull(store, "Store must not be null");
		Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("unit", b2bUnit);
		queryParams.put("store", store);
		List<SortQueryData> sortQueries;
		if (ArrayUtils.isNotEmpty(statuses))
		{
			queryParams.put("statusList", Arrays.asList(statuses));
			sortQueries = Arrays.asList(
					createSortQueryData(BY_DATE, QUERY_B2B_UNIT_ORDERS + AND_STATUSES + ORDER_BY_DATE),
					createSortQueryData(BY_ORDER_NUMBER, QUERY_B2B_UNIT_ORDERS + AND_STATUSES + ORDER_BY_CODE)
			);
		} else {
			sortQueries = Arrays.asList(
					createSortQueryData(BY_DATE, QUERY_B2B_UNIT_ORDERS + ORDER_BY_DATE),
					createSortQueryData(BY_ORDER_NUMBER, QUERY_B2B_UNIT_ORDERS + ORDER_BY_CODE)
			);
		}
		return this.getPagedFlexibleSearchService().search(sortQueries, BY_DATE, queryParams, pageableData);
	}
}
