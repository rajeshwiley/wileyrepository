package com.wiley.core.search.flexiblesearch.impl;

import de.hybris.platform.commerceservices.search.flexiblesearch.impl.DefaultPagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Map;

import org.springframework.util.Assert;

import com.wiley.core.search.flexiblesearch.CustomPagedFlexibleSearchService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Created by Mikhail_Asadchy on 06.10.2016.
 */
public class CustomPagedFlexibleSearchServiceImpl extends DefaultPagedFlexibleSearchService implements
		CustomPagedFlexibleSearchService
{

	@Override
	public <T> SearchPageData<T> search(final String query, final Map<String, ?> queryParams,
			final String countQuery, final Map<String, ?> countQueryParams, final PageableData pageableData)
	{
		validateParameterNotNull(query, "query cannot be null");
		validateParameterNotNull(countQuery, "countQuery cannot be null");
		validateParameterNotNull(pageableData, "pageableData cannot be null");
		Assert.isTrue(pageableData.getCurrentPage() >= 0, "pageableData current page must be zero or greater");
		Assert.isTrue(pageableData.getPageSize() > 0, "pageableData page size must be greater than zero");

		FlexibleSearchQuery searchQuery = getFlexibleSearchQuery(query, queryParams);
		searchQuery.setNeedTotal(false);
		searchQuery.setStart(pageableData.getCurrentPage() * pageableData.getPageSize());
		searchQuery.setCount(pageableData.getPageSize());

		final SearchResult<T> searchResult = getFlexibleSearchService().search(searchQuery);

		// Create the paged search result
		final SearchPageData<T> result = createSearchPageData();
		result.setResults(searchResult.getResult());
		result.setPagination(createPagination(pageableData, searchResult));
		// Note: does not set sorts

		searchQuery = getFlexibleSearchQuery(countQuery, countQueryParams);
		searchQuery.setResultClassList(Collections.singletonList(Long.class));
		final SearchResult<Long> countSearchResult = getFlexibleSearchService().search(searchQuery);
		populatePagination(result, countSearchResult);

		return result;
	}

	private <T> void populatePagination(final SearchPageData<T> result, final SearchResult<Long> countSearchResult)
	{
		final PaginationData paginationData = result.getPagination();

		paginationData.setTotalNumberOfResults(countSearchResult.getResult().get(0));
		paginationData.setNumberOfPages((int) Math.ceil(((double) paginationData.getTotalNumberOfResults())
				/ paginationData.getPageSize())); // copypasted row from DefaultPagedFlexibleSearchService.createPagination
	}

	private FlexibleSearchQuery getFlexibleSearchQuery(final String query, final Map<String, ?> queryParams)
	{
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		if (queryParams != null && !queryParams.isEmpty())
		{
			searchQuery.addQueryParameters(queryParams);
		}
		return searchQuery;
	}

}
