/**
 * 
 */
package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collections;
import java.util.Set;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;

public class Wileyb2cResolveMultivaluedFeatureStrategy extends AbstractWileyb2cResolveClassificationAttributeStrategy
{

  protected static final Set<Wileyb2cClassificationAttributes> CLASSIFICATION_ATTRIBUTES =
      Collections.singleton(Wileyb2cClassificationAttributes.SOCIETY_LINK);

  @Override
  protected String processFeature(final Feature feature)
  {
    return feature.getValues().toString();
  }
  
  @Override
  public <T> T resolveAttributeValue(final ProductModel product, final Wileyb2cClassificationAttributes attribute)
  {
    final Feature feature = getFeatureByAttribute(product, attribute);
    if (feature != null)
    {
      return (T) feature.getValues();
    }
    return null;
  }

  @Override
  protected Set<Wileyb2cClassificationAttributes> getAttributes()
  {
    return CLASSIFICATION_ATTRIBUTES;
  }
}
