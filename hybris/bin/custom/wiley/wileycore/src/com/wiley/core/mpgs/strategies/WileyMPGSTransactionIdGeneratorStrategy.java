package com.wiley.core.mpgs.strategies;


public interface WileyMPGSTransactionIdGeneratorStrategy
{
	String generateTransactionId();
}
