package com.wiley.core.wiley.session.storage.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFailedCartModificationsStorageServiceImpl implements WileyFailedCartModificationsStorageService
{
	static final String CART_MODIFICATION_REMOVED_ENTRIES_KEY = "showRemovedEntries";
	@Resource
	private SessionService sessionService;
	@Resource
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;
	@Resource
	private Converter<CartModificationData, CommerceCartModification> wileyCartModificationReverseConverter;

	@Override
	public void pushAll(@Nonnull final List<CommerceCartModification> failedCartModifications)
	{
		final List<CommerceCartModification> newModifications = new ArrayList<>(failedCartModifications);
		List<CartModificationData> newModificationDatas = new ArrayList<>();
		newModificationDatas.addAll(cartModificationConverter.convertAll(newModifications));
		final List<CartModificationData> previousModificationDatas = sessionService
				.getAttribute(CART_MODIFICATION_REMOVED_ENTRIES_KEY);
		if (previousModificationDatas != null)
		{
			newModificationDatas.addAll(previousModificationDatas);
		}
		sessionService.getCurrentSession().setAttribute(CART_MODIFICATION_REMOVED_ENTRIES_KEY,
				newModificationDatas);
	}

	@Nonnull
	@Override
	public List<CommerceCartModification> popAll()
	{
		return wileyCartModificationReverseConverter.convertAll(popAllData());
	}

	@Nonnull
	@Override
	public List<CartModificationData> popAllData()
	{
		List<CartModificationData> failedCartModificationDatas = sessionService
				.getAttribute(CART_MODIFICATION_REMOVED_ENTRIES_KEY);
		if (failedCartModificationDatas != null)
		{
			sessionService.removeAttribute(CART_MODIFICATION_REMOVED_ENTRIES_KEY);
		}
		else
		{
			failedCartModificationDatas = Collections.emptyList();
		}
		return failedCartModificationDatas;
	}

	@Override
	public boolean isCartCalculationFailed()
	{
		return CollectionUtils.isNotEmpty(sessionService.getAttribute(CART_MODIFICATION_REMOVED_ENTRIES_KEY));
	}
}
