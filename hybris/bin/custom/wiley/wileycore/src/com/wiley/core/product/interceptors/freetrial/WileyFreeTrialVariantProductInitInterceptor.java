package com.wiley.core.product.interceptors.freetrial;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;


/**
 * Sets defaults fields for {@link WileyFreeTrialVariantProductModel}
 */
public class WileyFreeTrialVariantProductInitInterceptor implements InitDefaultsInterceptor<WileyFreeTrialVariantProductModel>
{
	@Override
	public void onInitDefaults(final WileyFreeTrialVariantProductModel wileyFreeTrialVariantProductModel,
			final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		wileyFreeTrialVariantProductModel.setEditionFormat(ProductEditionFormat.DIGITAL);
	}
}
