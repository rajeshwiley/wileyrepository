package com.wiley.core.price.interceptor;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.springframework.util.Assert;

import com.wiley.core.product.WileyClusterCodeService;
import com.wiley.core.util.interceptor.AbstractCatalogAwareInterceptor;

/**
 * Validates if for selected currency Cluster code is set up 
 */
public class PriceRowIsClusterCodeDefinedValidateInterceptor extends AbstractCatalogAwareInterceptor
        implements ValidateInterceptor<PriceRowModel>
{
    private WileyClusterCodeService clusterCodeService;
    
    @Override
    public void onValidate(final PriceRowModel priceRow, final InterceptorContext interceptorContext)
            throws InterceptorException
    {
        final ProductModel product = priceRow.getProduct();
        if (product == null || !isProductWithRestriction(product))
        {
            return;
        }
        final CurrencyModel currency = priceRow.getCurrency();
        Assert.notNull(currency);

        if (clusterCodeService.isClusterCodeMissingForCurrency(product, currency))
        {
            throw new InterceptorException("Cluster code is not defined for product " + product.getCode()
                + " in currency " + currency.getIsocode());
        }
    }

    public void setClusterCodeService(final WileyClusterCodeService clusterCodeService) {
        this.clusterCodeService = clusterCodeService;
    }
}
