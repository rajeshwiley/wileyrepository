package com.wiley.core.wileycom.impex;

import de.hybris.platform.impex.jalo.imp.DefaultImportProcessor;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;


/**
 * This processor don't set null language to session during impex import
 */
public class WileycomLanguageImportProcessor extends DefaultImportProcessor
{
	@Override
	protected void adjustSessionSettings() {
		SessionContext ctx = JaloSession.getCurrentSession().createLocalSessionContext();
		ctx.setAttribute("disableRestrictions", Boolean.TRUE);
		ctx.setAttribute("disableRestrictionGroupInheritance", Boolean.TRUE);
		ctx.setAttribute("use.fast.algorithms", Boolean.TRUE);
		ctx.setAttribute("import.mode", Boolean.TRUE);
		ctx.setAttribute("disable.attribute.check", Boolean.TRUE);
	}
}
