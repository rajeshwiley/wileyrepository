package com.wiley.core.integration.edi;

/**
 * Constants with common header names
 */
public final class Headers
{
	private Headers() { }

	public static final String CONVERTED_PROCESSES = "convertedProcesses";
}
