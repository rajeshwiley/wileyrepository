package com.wiley.core.product.dao;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyProductVariantSetModel;


/**
 * Contains methods for searching {@link com.wiley.core.model.WileyProductVariantSetModel} models.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyProductVariantSetDao
{

	/**
	 * Find product sets.
	 *
	 * @return the list ordered by modified time
	 */
	@Nonnull
	List<WileyProductVariantSetModel> findProductSets();

}
