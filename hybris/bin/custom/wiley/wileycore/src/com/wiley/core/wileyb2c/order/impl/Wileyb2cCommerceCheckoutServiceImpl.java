package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

import com.wiley.core.order.impl.WileyCommerceCheckoutServiceImpl;


/**
 * Extends {@link WileyCommerceCheckoutServiceImpl} for setting payment address on set payment info.
 * We need payment address for external taxes calculation. Possible case(when payment address is not defined, but
 * paymentInfo is defined) is using existing credit card
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cCommerceCheckoutServiceImpl extends WileyCommerceCheckoutServiceImpl
{

	@Override
	public boolean setPaymentInfo(final CommerceCheckoutParameter parameter)
	{
		CommerceCheckoutParameter setPaymentAddressCheckoutParameter = new CommerceCheckoutParameter();
		setPaymentAddressCheckoutParameter.setAddress(parameter.getPaymentInfo().getBillingAddress());
		setPaymentAddressCheckoutParameter.setCart(parameter.getCart());
		setPaymentAddress(setPaymentAddressCheckoutParameter);
		return super.setPaymentInfo(parameter);
	}

}
