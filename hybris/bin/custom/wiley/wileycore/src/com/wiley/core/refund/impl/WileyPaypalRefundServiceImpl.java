package com.wiley.core.refund.impl;

import de.hybris.platform.payment.commands.result.RefundResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Date;

import javax.annotation.Resource;

import com.ebay.api.RefundTransactionRequestType;
import com.ebay.api.RefundTransactionResponseType;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.converters.impl.PayPalRequestDataConverter;
import com.paypal.hybris.converters.impl.PayPalResponseConverter;
import com.paypal.hybris.data.RefundTransactionRequestData;
import com.paypal.hybris.data.RefundTransactionResultData;
import com.paypal.hybris.service.PaypalPaymentService;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.response.WileyRefundResult;
import com.wiley.core.payment.transaction.impl.WileyPaymentTransactionService;
import com.wiley.core.refund.WileyPaypalRefundService;


public class WileyPaypalRefundServiceImpl implements WileyPaypalRefundService
{
	@Resource
	private PaypalPaymentService paypalPaymentService;
	@Resource
	private PayPalRequestDataConverter refundTransReqDataConverter;
	@Resource
	private PayPalResponseConverter refundTransResConverter;
	@Resource
	private WileyPaymentTransactionService paymentTransactionService;

	@Override
	public RefundResult performPayPalRefund(final WileyFollowOnRefundRequest refundRequest)
	{
		final RefundTransactionRequestData requestData = new RefundTransactionRequestData();
		requestData.setTransactionId(refundRequest.getRequestId());
		requestData.setAmount(refundRequest.getTotalAmount());
		requestData.setCurrencyIsoCode(refundRequest.getCurrency().getCurrencyCode());

		RefundTransactionRequestType request = (RefundTransactionRequestType) refundTransReqDataConverter.convert(requestData);
		RefundTransactionResponseType response = paypalPaymentService.refundTransaction(request);
		RefundTransactionResultData resultData = (RefundTransactionResultData) refundTransResConverter.convert(response);


		return createResult(refundRequest, resultData);
	}

	private RefundResult createResult(final WileyFollowOnRefundRequest refundRequest,
			final RefundTransactionResultData resultData)
	{
		WileyRefundResult refundResult = new WileyRefundResult();
		refundResult.setCurrency(refundRequest.getCurrency());
		refundResult.setPaymentProvider(PaypalConstants.PAYMENT_PROVIDER_NAME);
		refundResult.setRequestTime(new Date());
		refundResult.setTotalAmount(refundRequest.getTotalAmount());
		refundResult.setRequestId(resultData.getRefundTransactionId());

		if (PaypalConstants.STATUS_SUCCESS.equals(resultData.getAck()))
		{
			refundResult.setTransactionStatus(TransactionStatus.ACCEPTED);
			refundResult.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
		} else {
			refundResult.setTransactionStatus(TransactionStatus.ERROR);
			refundResult.setPayPalTransactionErrorDetails(
					paymentTransactionService.createPayPalErrorDetails(resultData.getErrors()));

		}
		return refundResult;
	}
}
