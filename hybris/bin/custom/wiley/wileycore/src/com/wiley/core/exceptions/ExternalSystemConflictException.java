package com.wiley.core.exceptions;

/**
 * Created by Sergiy_Mishkovets on 7/14/2016.
 */
public class ExternalSystemConflictException extends ExternalSystemClientErrorException
{
	public ExternalSystemConflictException(final String message)
	{
		super(message);
	}

	public ExternalSystemConflictException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemConflictException(final Throwable cause)
	{
		super(cause);
	}
}
