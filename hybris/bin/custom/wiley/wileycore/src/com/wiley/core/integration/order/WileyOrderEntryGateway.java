package com.wiley.core.integration.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * Gateway interface to publish changes in cart
 */
public interface WileyOrderEntryGateway
{
	@Transactional(propagation = Propagation.MANDATORY)
	void publish(@Nonnull @Payload AbstractOrderEntryModel orderEntryModel,
			@Header("hybrisOrderEntryEventType") String hybrisOrderEntryEventType);
}
