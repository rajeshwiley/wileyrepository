package com.wiley.core.wiley.caching.impl;

import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.region.CacheRegion;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;

import com.wiley.core.wiley.caching.WileyCacheKeyMapper;


public class WileyRegionCacheAdapterImpl implements Cache
{
	private CacheRegion cacheRegion;
	private WileyCacheKeyMapper cacheKeyMapper;

	@Override
	public String getName()
	{
		return cacheRegion.getName();
	}

	@Override
	public Object getNativeCache()
	{
		return cacheRegion;
	}

	@Override
	public ValueWrapper get(final Object keyObj)
	{
		CacheKey key = cacheKeyMapper.map(keyObj);
		return toWrapper(cacheRegion.get(key));
	}

	@Override
	public <T> T get(final Object keyObj, final Class<T> type)
	{
		T value = lookup(keyObj);

		if (value != null && type != null && !type.isInstance(value))
		{
			throw new IllegalStateException("Cached value is not of required type [" + type.getName() + "]: " + value);
		}
		else
		{
			return value;
		}
	}

	protected <T> T lookup(final Object keyObj)
	{
		CacheKey key = cacheKeyMapper.map(keyObj);
		return (T) cacheRegion.get(key);
	}

	@Override
	public <T> T get(final Object keyObj, final Callable<T> valueLoader)
	{
		T value = lookup(keyObj);

		if (value == null)
		{
			try
			{
				value = valueLoader.call();
			}
			catch (Throwable e)
			{
				throw new ValueRetrievalException("Unable to resolve a value for cache for key '" + keyObj, valueLoader, e);
			}

			put(keyObj, value);
		}

		return value;
	}

	@Override
	public void put(final Object keyObj, final Object value)
	{
		CacheKey key = cacheKeyMapper.map(keyObj);
		cacheRegion.getWithLoader(key, new WileyDefaultCacheValueLoader(value));
	}

	@Override
	public ValueWrapper putIfAbsent(final Object keyObj, final Object value)
	{
		CacheKey key = cacheKeyMapper.map(keyObj);
		final WileyDefaultCacheValueLoader cacheValueLoader = new WileyDefaultCacheValueLoader(value);
		final Object existingValue = cacheRegion.getWithLoader(key, cacheValueLoader);

		return toWrapper(existingValue);
	}

	@Override
	public void evict(final Object keyObj)
	{
		CacheKey key = cacheKeyMapper.map(keyObj);
		// The second argument is 'shouldFireEvents'
		cacheRegion.remove(key, false);
	}

	@Override
	public void clear()
	{
		cacheRegion.clearCache();
	}

	private ValueWrapper toWrapper(final Object value)
	{
		return value != null ? new SimpleValueWrapper(value) : null;
	}

	@Required
	public void setCacheRegion(final CacheRegion cacheRegion)
	{
		this.cacheRegion = cacheRegion;
	}

	@Required
	public void setCacheKeyMapper(final WileyCacheKeyMapper cacheKeyMapper)
	{
		this.cacheKeyMapper = cacheKeyMapper;
	}
}