package com.wiley.core.subscription.exception;

/** This exception is intended to be thrown when WileySubscription is being updated */
public class SubscriptionUpdateException extends RuntimeException
{
	public SubscriptionUpdateException(final String message)
	{
		super(message);
	}

	public SubscriptionUpdateException(final Throwable cause)
	{
		super(cause);
	}

	public SubscriptionUpdateException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

}
