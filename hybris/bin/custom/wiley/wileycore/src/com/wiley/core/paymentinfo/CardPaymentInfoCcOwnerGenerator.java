package com.wiley.core.paymentinfo;

import de.hybris.platform.core.model.user.AddressModel;

import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Generates CC owner for card payment info.
 */
public interface CardPaymentInfoCcOwnerGenerator
{

	/**
	 * Generates CC owner base on billing address.<br/>
	 * Format: <code>"FirstName LastName"</code>
	 *
	 * @param billingAddress
	 * @return optional CC owner.
	 */
	@Nonnull
	Optional<String> getCCOwnerFromBillingAddress(@Nonnull AddressModel billingAddress);

}
