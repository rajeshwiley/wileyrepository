package com.wiley.core.cms.interceptors;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;
import com.wiley.core.wiley.classification.WileyClassificationService;


/**
 * Interceptor merge list of CMSProductAttributes with products' ClassAttributeAssignments
 */
public class VariantProductComparisonTableComponentPrepareInterceptor
		implements PrepareInterceptor<VariantProductComparisonTableComponentModel>
{
	@Autowired
	private ModelService modelService;

	@Autowired
	private WileyClassificationService wileyClassificationService;

	@Override
	public void onPrepare(final VariantProductComparisonTableComponentModel variantProductComparisonTableComponentModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{

		if (!interceptorContext.isModified(variantProductComparisonTableComponentModel,
				VariantProductComparisonTableComponentModel.PRODUCTS))
		{
			return;
		}

		List<WileyVariantProductModel> products = variantProductComparisonTableComponentModel.getProducts();
		if (CollectionUtils.isEmpty(products))
		{
			if (CollectionUtils.isNotEmpty(variantProductComparisonTableComponentModel.getCmsProductAttributes()))
			{
				modelService.removeAll(variantProductComparisonTableComponentModel.getCmsProductAttributes());
			}
		}
		else
		{
			Set<ClassAttributeAssignmentModel> filteredAssignments = wileyClassificationService.getFilteredAssignmentForProducts(
					products);

			wileyClassificationService.mergeAssigmentsWithCMSProductAttributes(filteredAssignments,
					variantProductComparisonTableComponentModel);
		}

	}
}
