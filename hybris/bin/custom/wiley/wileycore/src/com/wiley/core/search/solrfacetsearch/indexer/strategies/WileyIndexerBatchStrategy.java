package com.wiley.core.search.solrfacetsearch.indexer.strategies;

import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.strategies.impl.DefaultIndexerBatchStrategy;


public class WileyIndexerBatchStrategy extends DefaultIndexerBatchStrategy
{
	protected void executeIndexerOperation(final IndexerBatchContext batchContext) throws IndexerException, InterruptedException
	{
		switch (batchContext.getIndexOperation())
		{
			case RESTORE:
			case RESTORE_DELETE:
				getIndexer().indexItems(batchContext.getItems(), batchContext.getFacetSearchConfig(),
						batchContext.getIndexedType());
				break;
			default:
				super.executeIndexerOperation(batchContext);
		}
	}
}
