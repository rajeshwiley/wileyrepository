package com.wiley.core.integration.impl;

import com.google.common.collect.ImmutableMap;
import com.wiley.core.integration.WileyIntegrationService;
import com.wiley.core.security.WileyMessageEncoder;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.time.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


/**
 * Holds logic required for Wiley integration
 */
public class WileyIntegrationServiceImpl implements WileyIntegrationService {
    private String authUrl;

    @Autowired
    private TimeService timeService;

    private WileyMessageEncoder wileyMessageEncoder;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    /**
     * Builds "magic" link for Wiley integration.
     * <p>
     * Please take a look at corresponding <a href="https://confluence.wiley.ru/display/ECSC/IDD%3A+Link-with-
     * token+authentication+mechanism">IDD: Link-with-token authentication mechanism</a> for more details
     *
     * @param customerData customer data
     * @return String URL with query parameters required for user authentication at AGS
     */
    @Override
    public String getMagicLink(final CustomerModel customerData) {
        ImmutableMap<String, String> uriVariables = ImmutableMap.<String, String>builder()
                .put("uid", customerData.getUid())
                .put("fname", customerData.getFirstName())
                .put("timestamp", getTimestamp())
                .put("token", getToken(customerData.getUid(), getTimestamp()))
                .build();

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(authUrl)
                .query("uid={uid}")
                .query("fname={fname}")
                .query("timestamp={timestamp}")
                .query("token={token}");

        return uriComponentsBuilder.buildAndExpand(uriVariables).encode().toUriString();
    }

    private String getToken(final String uid, final String timestamp) {
        return wileyMessageEncoder.encode(uid + timestamp);
    }

    private String getTimestamp() {
        Date currentTime = timeService.getCurrentTime();
        return ZonedDateTime.ofInstant(currentTime.toInstant(), ZoneId.ofOffset("UTC", ZoneOffset.UTC)).format(formatter);
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(final String authUrl) {
        this.authUrl = authUrl;
    }

    public WileyMessageEncoder getWileyMessageEncoder() {
        return wileyMessageEncoder;
    }

    public void setWileyMessageEncoder(final WileyMessageEncoder wileyMessageEncoder) {
        this.wileyMessageEncoder = wileyMessageEncoder;
    }
}
