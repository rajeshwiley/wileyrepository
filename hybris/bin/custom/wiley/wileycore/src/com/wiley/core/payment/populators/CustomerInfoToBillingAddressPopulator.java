package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;


public class CustomerInfoToBillingAddressPopulator extends AbstractRequestPopulator<CustomerInfoData, AddressModel>
{
    private CommonI18NService commonI18NService;
    private CustomerEmailResolutionService customerEmailResolutionService;
    private UserService userService;

    @Override
    public void populate(final CustomerInfoData source, final AddressModel target)
            throws ConversionException
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        target.setFirstname(source.getBillToFirstName());
        target.setLastname(source.getBillToLastName());
        target.setLine1(source.getBillToStreet1());
        target.setLine2(source.getBillToStreet2());
        target.setTown(source.getBillToCity());
        target.setPostalcode(source.getBillToPostalCode());
        target.setExternalId(source.getBillToExternalId());

        if (StringUtils.isNotBlank(source.getBillToTitleCode()))
        {
            target.setTitle(getUserService().getTitleForCode(source.getBillToTitleCode()));
        }

        final CountryModel country = getCommonI18NService().getCountry(source.getBillToCountry());
        target.setCountry(country);
        if (StringUtils.isNotEmpty(source.getBillToState()))
        {
            target.setRegion(getCommonI18NService().getRegion(country,
                    country.getIsocode() + "-" + source.getBillToState()));
        }

        // Phone1 is required for sending it to Eloqua.
        target.setPhone1(source.getBillToPhoneNumber());
        // mark address as billing
        target.setBillingAddress(true);
        target.setShippingAddress(false);
        target.setVisibleInAddressBook(true);
    }

    protected CommonI18NService getCommonI18NService()
    {
        return commonI18NService;
    }

    protected CustomerEmailResolutionService getCustomerEmailResolutionService()
    {
        return customerEmailResolutionService;
    }

    protected UserService getUserService()
    {
        return userService;
    }

    public void setCommonI18NService(final CommonI18NService commonI18NService)
    {
        this.commonI18NService = commonI18NService;
    }

    public void setCustomerEmailResolutionService(
            final CustomerEmailResolutionService customerEmailResolutionService)
    {
        this.customerEmailResolutionService = customerEmailResolutionService;
    }

    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }
}
