package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbRealTimeInventoryCheckGateway;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.wileyb2c.order.Wileyb2cExternalInventoryService;


/**
 * Created by Mikhail_Asadchy on 7/29/2016.
 */
public class Wileyb2cExternalInventoryServiceImpl implements Wileyb2cExternalInventoryService
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cExternalInventoryServiceImpl.class);

	@Resource
	private EsbRealTimeInventoryCheckGateway esbRealTimeInventoryCheckGateway;

	@Resource
	private CartService cartService;

	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public Map<String, ExternalInventoryStatus> doRealTimeInventoryCheck()
	{
		final CartModel cartModel = cartService.getSessionCart();

		if (cartModel.getDeliveryAddress() != null && cartModel.getDeliveryMode() != null && !wileyProductEditionFormatService
				.isDigitalCart(cartModel))
		{
			try
			{
				return esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);
			}
			catch (ExternalSystemException e)
			{
				LOG.error("ExternalSystemException during real-time inventory check call", e);
			}
			catch (Exception e)
			{
				LOG.error("Some error during real-time inventory check call", e);
			}
		}

		return new HashMap<>();
	}
}
