package com.wiley.core.classification.features;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.internal.converter.impl.DefaultLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections4.CollectionUtils;

import com.google.common.base.Preconditions;


/**
 * Feature with support of language fallback mechanism
 */
public class FallbackLocalizedFeature extends LocalizedFeature
{
	private LocaleProvider localeProvider;
	private boolean fallbackEnabled;

	public FallbackLocalizedFeature(final ClassAttributeAssignmentModel assignment, final Map<Locale, List<FeatureValue>> values,
			final Locale currentLocale, final I18NService i18nService)
	{
		super(assignment, values, currentLocale);
		initializeFallbackFields(i18nService);
	}

	public FallbackLocalizedFeature(final String code, final Map<Locale, List<FeatureValue>> values, final Locale currentLocale,
			final I18NService i18nService)
	{
		super(code, values, currentLocale);
		initializeFallbackFields(i18nService);
	}

	private void initializeFallbackFields(final I18NService i18nService)
	{
		localeProvider = new DefaultLocaleProvider(i18nService);
		fallbackEnabled = localeProvider.isFallbackEnabled();
	}

	@Override
	public List<FeatureValue> getValues(final Locale locale)
	{
		Preconditions.checkNotNull(locale, "locale can not be null");

		final List<FeatureValue> featureValues = getValueWithFallback(locale, super::getValues,
				CollectionUtils::isEmpty);

		if (featureValues == null)
		{
			return Collections.emptyList();
		}
		else
		{
			return featureValues;
		}
	}

	@Override
	public FeatureValue getValue(final Locale locale)
	{
		Preconditions.checkNotNull(locale, "locale can not be null");

		return getValueWithFallback(locale, super::getValue, value -> value == null);
	}

	@Nullable
	private <R> R getValueWithFallback(@Nonnull final Locale locale, @Nonnull final Function<Locale, R> getValueFunction,
			@Nonnull final Predicate<R> emptyValuePredicate)
	{
		Preconditions.checkNotNull(locale, "locale can not be null");
		Preconditions.checkNotNull(getValueFunction, "getValueFunction can not be null");
		Preconditions.checkNotNull(emptyValuePredicate, "emptyValuePredicate can not be null");

		R value = getValueFunction.apply(locale);
		if (emptyValuePredicate.test(value) && isFallbackEnabled())
		{
			final List<Locale> fallbacks = getLocaleProvider().getFallbacks(locale);
			if (fallbacks != null)
			{
				for (final Locale fallback : fallbacks)
				{
					value = getValueFunction.apply(fallback);
					if (!emptyValuePredicate.test(value))
					{
						break;
					}
				}
			}
		}
		return value;
	}

	public boolean isFallbackEnabled()
	{
		return fallbackEnabled;
	}

	public LocaleProvider getLocaleProvider()
	{
		return localeProvider;
	}
}
