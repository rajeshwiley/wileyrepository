package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.solrfacetsearch.config.FacetType;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.FacetValueField;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.util.ClientUtils;

import com.wiley.core.search.client.solrj.WileySolrQuery;
import com.wiley.core.search.client.solrj.facet.DomainSwg;
import com.wiley.core.search.client.solrj.facet.TermFacetSwg;
import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


public class WileyFacetSearchQueryFacetsPopulator extends WileyAbstractFacetSearchQueryPopulator
{
	private static final Logger LOG = Logger.getLogger(WileyFacetSearchQueryFacetsPopulator.class);
	public static final String DEFAULT_FACET_SORT = "count";
	public static final String GROUP_FACET = "group";
	public static final int MINIMUM_COUNT = 1;
	public static final int DEFAULT_LIMIT = -1;
	private String defaultFacetSort;
	private Integer defaultLimit;

	@Override
	public void populate(final SearchQueryConverterData source, final WileySolrQuery target)
	{
		WileySearchQuery searchQuery = (WileySearchQuery) source.getSearchQuery();
		Map<String, FacetInfo> facets = this.prepareFacets(searchQuery);
		int index = 0;

		String groupFacet = getGroupFacet(searchQuery);
		int facetLimit = resolveFacetLimit(searchQuery);
		int termFacetLimit = resolveTermFacetLimit(facetLimit);
		searchQuery.setFacetLimit(facetLimit);

		for (Iterator<FacetInfo> iterator = facets.values().iterator(); iterator.hasNext(); ++index)
		{
			FacetInfo facet = iterator.next();
			String translatedField = this.getFieldNameTranslator().translate(searchQuery, facet.getField(),
					FieldNameProvider.FieldType.INDEX);
			FacetType facetType = facet.getFacetType();

			TermFacetSwg termFacet = new TermFacetSwg(translatedField, MINIMUM_COUNT, termFacetLimit);
			if (groupFacet != null)
			{
				termFacet.getFacet().put(GROUP_FACET, groupFacet);
				termFacet.getSort().put(GROUP_FACET, TermFacetSwg.DESC);
			}

			final IndexedProperty indexedProperty = searchQuery.getIndexedType().getIndexedProperties().get(facet.getField());
			//
			if (indexedProperty.isQueryFacet() && CollectionUtils.isEmpty(facet.getValues()))
			{
				continue;
			}

			if (CollectionUtils.isEmpty(facet.getValues()))
			{
				target.getJsonParam().getFacet().addFacet(termFacet);
			}
			else
			{
				QueryField facetFilterQuery;
				String filterQuery;
				if (FacetType.REFINE.equals(facetType))
				{
					if (facet.getValues().size() > 1)
					{
						LOG.warn("Multiple values found for facet with type REFINE [field: " + facet.getField() + "]");
					}

					facetFilterQuery = new QueryField(facet.getField(), SearchQuery.Operator.AND, facet.getValues());
					filterQuery = this.convertQueryField(searchQuery, facetFilterQuery);
					target.addFilterQuery(new String[] { filterQuery });
					target.getJsonParam().getFacet().addFacet(termFacet);
				}
				else if (FacetType.MULTISELECTAND.equals(facetType))
				{
					facetFilterQuery = new QueryField(facet.getField(), SearchQuery.Operator.AND, facet.getValues());
					filterQuery = this.convertQueryField(searchQuery, facetFilterQuery);
					String tag = "fk" + index;
					target.addFilterQuery(new String[] { "{!tag=" + tag + "}" + filterQuery });
					DomainSwg domain = new DomainSwg(tag);
					termFacet.setDomain(domain);
					target.getJsonParam().getFacet().addFacet(termFacet);
				}
				else if (FacetType.MULTISELECTOR.equals(facetType))
				{
					facetFilterQuery = new QueryField(facet.getField(), SearchQuery.Operator.OR, facet.getValues());
					filterQuery = this.convertQueryField(searchQuery, facetFilterQuery);
					String tag = "fk" + index;
					target.addFilterQuery(new String[] { "{!tag=" + tag + "}" + filterQuery });
					DomainSwg domain = new DomainSwg(tag);
					termFacet.setDomain(domain);
					target.getJsonParam().getFacet().addFacet(termFacet);
				}
				else
				{
					LOG.warn("Unknown facet type [field: " + facet.getField() + "]");
				}
			}
		}

		target.setFacetSort(this.resolveFacetSort());
	}

	/**
	 * OOTB_CODE Based {@link AbstractFacetSearchQueryPopulator#convertQueryField(SearchQuery, QueryField)}.
	 * Customization is placed on {@link #escapeIfNeeded(String, IndexedProperty)}
	 *
	 * @param searchQuery
	 * @param queryField
	 * @return
	 */
	@Override
	protected String convertQueryField(final SearchQuery searchQuery, final QueryField queryField)
	{
		String convertedField = this.getFieldNameTranslator().translate(searchQuery, queryField.getField(),
				FieldNameProvider.FieldType.INDEX);
		StringBuilder query = new StringBuilder();
		query.append(convertedField);
		query.append(':');
		String operator;
		String separator;
		final IndexedProperty indexedProperty = searchQuery.getIndexedType().getIndexedProperties().get(queryField.getField());
		if (queryField.getValues().size() == 1)
		{
			String convertedValues = (String) queryField.getValues().iterator().next();
			operator = escapeIfNeeded(convertedValues, indexedProperty);
			separator = this.getFacetSearchQueryOperatorTranslator().translate(operator, queryField.getQueryOperator());
			query.append(separator);
		}
		else
		{
			ArrayList convertedValues1 = new ArrayList(queryField.getValues().size());
			Iterator separator1 = queryField.getValues().iterator();

			String convertedValue;
			while (separator1.hasNext())
			{
				operator = (String) separator1.next();
				convertedValue = escapeIfNeeded(operator, indexedProperty);
				String convertedValue1 = this.getFacetSearchQueryOperatorTranslator().translate(convertedValue,
						queryField.getQueryOperator());
				convertedValues1.add(convertedValue1);
			}

			SearchQuery.Operator operator1 = this.resolveQueryFieldOperator(searchQuery, queryField);
			separator = " " + operator1 + " ";
			convertedValue = StringUtils.join(convertedValues1, separator);
			query.append('(');
			query.append(convertedValue);
			query.append(')');
		}

		return query.toString();
	}

	String escapeIfNeeded(final String operator, final IndexedProperty indexedProperty)
	{
		return indexedProperty.isQueryFacet() ? operator : ClientUtils.escapeQueryChars(operator);
	}

	protected Map<String, FacetInfo> prepareFacets(final WileySearchQuery searchQuery)
	{
		HashMap<String, FacetInfo> facets = new HashMap();

		List<FacetField> queryFacets = getFilteredQueryFacets(searchQuery);
		for (FacetField facetValue : queryFacets)
		{
			FacetInfo facetInfo = new FacetInfo(facetValue.getField(), facetValue.getFacetType());
			facets.put(facetValue.getField(), facetInfo);
		}

		for (FacetValueField facetValue : searchQuery.getFacetValues())
		{
			FacetInfo facetInfo = facets.get(facetValue.getField());
			if (facetInfo != null)
			{
				if (CollectionUtils.isNotEmpty(facetValue.getValues()))
				{
					facetInfo.getValues().addAll(facetValue.getValues());
				}
			}
			else
			{
				LOG.warn("Search query contains value for facet but facet was not added [field: " + facetValue.getField() + "]");
			}
		}

		return facets;
	}

	protected List<FacetField> getFilteredQueryFacets(final WileySearchQuery searchQuery)
	{
		List<FacetField> result = searchQuery.getFacets();
		if (searchQuery.getFacetCode() != null)
		{
			result = result.stream()
					.filter(f -> isFilteredFacet(searchQuery, f) || isFacetValueFacet(searchQuery, f))
					.collect(Collectors.toList());
		}
		return result;
	}

	protected boolean isFacetValueFacet(final WileySearchQuery searchQuery, final FacetField facetField)
	{
		return searchQuery.getFacetValues().stream().anyMatch(
				facetValueField -> facetField.getField().equals(facetValueField.getField()));
	}

	protected boolean isFilteredFacet(final WileySearchQuery searchQuery, final FacetField facetField)
	{
		return searchQuery.getFacetCode().equals(facetField.getField());
	}

	public void setDefaultFacetSort(final String defaultFacetSort)
	{
		this.defaultFacetSort = defaultFacetSort;
	}

	public String getDefaultFacetSort()
	{
		return defaultFacetSort;
	}

	@Nonnull
	protected String resolveFacetSort()
	{
		return this.defaultFacetSort != null ? this.defaultFacetSort : DEFAULT_FACET_SORT;
	}

	protected int resolveFacetLimit(final WileySearchQuery searchQuery)
	{
		int result = DEFAULT_LIMIT;

		if (Boolean.TRUE.equals(searchQuery.getUseFacetViewMoreLimit()))
		{
			Integer facetSearchConfigFacetViewMoreLimit = searchQuery.getFacetSearchConfig().getFacetViewMoreLimit();
			if (facetSearchConfigFacetViewMoreLimit != null)
			{
				result = facetSearchConfigFacetViewMoreLimit;
			}
		}
		else
		{
			final Integer facetSearchConfigFacetLimit = searchQuery.getFacetSearchConfig().getFacetLimit();
			if (facetSearchConfigFacetLimit != null)
			{
				result = facetSearchConfigFacetLimit;
			}
		}
		return result;
	}

	protected int resolveTermFacetLimit(int facetLimit)
	{
		return facetLimit == DEFAULT_LIMIT ? facetLimit : facetLimit + 1;
	}

	protected static class FacetInfo
	{
		private final String field;
		private final FacetType facetType;
		private final Set<String> values;

		public FacetInfo(final String field, final FacetType facetType)
		{
			this.field = field;
			this.facetType = facetType;
			this.values = new LinkedHashSet();
		}

		public String getField()
		{
			return this.field;
		}

		public FacetType getFacetType()
		{
			return this.facetType;
		}

		public Set<String> getValues()
		{
			return this.values;
		}
	}
}