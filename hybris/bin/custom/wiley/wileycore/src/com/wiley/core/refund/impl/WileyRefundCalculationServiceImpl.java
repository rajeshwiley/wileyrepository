package com.wiley.core.refund.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.refund.WileyRefundCalculationService;


public class WileyRefundCalculationServiceImpl implements WileyRefundCalculationService
{

	@Autowired
	private PaymentTransactionService transactionService;

	@Override
	public BigDecimal calculateMaximumAvailableRefundAmountForOrder(final OrderModel orderModel)
	{
		List<PaymentTransactionEntryModel> captureTransactionEntries = transactionService.getAcceptedTransactionEntries(
				orderModel, PaymentTransactionType.CAPTURE);
		BigDecimal maximumAvailableRefundAmount = BigDecimal.ZERO;
		for (PaymentTransactionEntryModel captureTransactionEntry : captureTransactionEntries)
		{
			PaymentTransactionModel transactionWithCaptureEntry = captureTransactionEntry.getPaymentTransaction();
			maximumAvailableRefundAmount = maximumAvailableRefundAmount.add(captureTransactionEntry.getAmount());

			BigDecimal alreadyRefundedTotal = transactionService.getAcceptedTransactionEntries(transactionWithCaptureEntry,
					PaymentTransactionType.REFUND_FOLLOW_ON)
					.stream()
					.filter(transactionEntry -> transactionEntry.getAmount() != null)
					.map(PaymentTransactionEntryModel::getAmount)
					.reduce(BigDecimal.ZERO, BigDecimal::add);

			maximumAvailableRefundAmount = maximumAvailableRefundAmount
					.subtract(alreadyRefundedTotal)
					.subtract(calculateGiftCardTotalPrice(orderModel));
		}
		return maximumAvailableRefundAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
	}

	private BigDecimal calculateGiftCardTotalPrice(final OrderModel orderModel)
	{
		return orderModel.getEntries().stream()
				.filter(entry -> entry.getProduct() instanceof WileyGiftCardProductModel)
				.map(entry -> BigDecimal.valueOf(entry.getBasePrice() * entry.getQuantity()))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}