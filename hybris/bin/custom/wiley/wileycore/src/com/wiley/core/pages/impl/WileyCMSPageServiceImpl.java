package com.wiley.core.pages.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSPageService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.pages.WileyCMSPageService;
import com.wiley.core.pages.dao.WileyCMSPageDao;


public class WileyCMSPageServiceImpl extends DefaultCMSPageService implements WileyCMSPageService
{
	@Resource
	private WileyCMSPageDao wileyCMSPageDao;

	@Override
	public WileyPartnerPageModel getPartnerPageByCategoryId(final String categoryCode)
	{
		return (WileyPartnerPageModel) getPartnerPageForIdAndCategoryCode(categoryCode);
	}

	private AbstractPageModel getPartnerPageForIdAndCategoryCode(final String categoryCode)
	{
		final Collection<CatalogVersionModel> versions = getCatalogVersionService()
				.getSessionCatalogVersions();
		final Collection<AbstractPageModel> pages = wileyCMSPageDao
				.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE, versions, categoryCode);

		if (pages.size() == 1)
		{
			final AbstractPageModel page = pages.iterator().next();
			final Collection<AbstractRestrictionModel> restrictions = page.getRestrictions();

			if (restrictions.isEmpty())
			{
				return page;
			}
			else
			{
				String restrictionsString = restrictions
						.stream()
						.map(AbstractRestrictionModel::getName)
						.collect(Collectors.joining(", "));

				LOG.error("A category page with name `" + page.getName() + "` for category with code `" + categoryCode
						+ "` is inaccessible because of restrictions `"
						+ restrictionsString + "`");
			}
		}
		else
		{
			String pagesString = !pages.isEmpty() ?
					pages.stream().map(AbstractPageModel::getName).collect(Collectors.joining(", ")) :
					"";

			String catalogVersionsString = !versions.isEmpty() ?
					versions.stream().map(CatalogVersionModel::getVersion).collect(Collectors.joining(", ")) :
					"";

			LOG.error("Inconsistent number of pages for category with code `" + categoryCode + "` (actual: " + pages.size()
					+ ", expected: 1); catalog versions: `" + catalogVersionsString + "`"
					+ "; pages: `" + pagesString + "`");
		}

		return null;
	}

	@Override
	public List<AbstractPageModel> findPagesByComponent(final AbstractCMSComponentModel componentModel)
	{
		return wileyCMSPageDao.findPagesByComponent(componentModel);
	}

	@Override
	public Collection<AbstractPageModel> findAllPagesByCategoryCode(final String typeCode,
			final Collection<CatalogVersionModel> catalogVersions, final String categoryCode)
	{
		return wileyCMSPageDao.findAllPagesByCategoryCode(typeCode, catalogVersions, categoryCode);
	}
}
