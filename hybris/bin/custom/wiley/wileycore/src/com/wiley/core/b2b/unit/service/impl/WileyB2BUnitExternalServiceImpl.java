package com.wiley.core.b2b.unit.service.impl;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;
import com.wiley.core.b2b.unit.service.exception.B2BUnitNotFoundException;
import com.wiley.core.b2b.unit.service.exception.FindB2BUnitExternalSystemException;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.integration.b2bunit.B2BUnitGateway;


public class WileyB2BUnitExternalServiceImpl implements WileyB2BUnitExternalService
{
	private static final Logger LOG = Logger.getLogger(WileyB2BUnitExternalServiceImpl.class);

	@Resource
	private B2BUnitGateway b2bUnitGateway;

	@Override
	public B2BUnitModel find(final String sapAccountNumber, final BaseSiteModel site)
	{
		B2BUnitModel b2BUnit = null;
		try
		{
			b2BUnit = b2bUnitGateway.find(sapAccountNumber, site);
		}
		catch (ExternalSystemNotFoundException notFoundException)
		{
			final String errorMessage = "B2B Unit with SAP Account Number " + sapAccountNumber
					+ " does not exist in SAP ERP";
			if (LOG.isDebugEnabled())
			{
				LOG.debug(errorMessage, notFoundException);
				LOG.debug(ExceptionUtils.getRootCause(notFoundException));
			}

			throw new B2BUnitNotFoundException(errorMessage, notFoundException);
		}
		catch (ExternalSystemException exception)
		{
			final String errorMessage = "Failed to find B2B Unit with SAP Account Number " + sapAccountNumber
					+ " due to external system exception";
			LOG.error(errorMessage, exception);
			LOG.error(ExceptionUtils.getRootCause(exception));
			throw new FindB2BUnitExternalSystemException(errorMessage, exception);
		}
		return b2BUnit;
	}

	@Override
	public List<String> validatePONumbers(final String sapAccountNumber, final Collection<String> poNumbers)
	{
		List<String> invalidPoNumbers = new ArrayList<>();
		try
		{
			invalidPoNumbers = b2bUnitGateway.validatePONumbers(sapAccountNumber, poNumbers);
		}
		catch (ExternalSystemException exception)
		{
			// According to the AC2 from https://confluence.wiley.ru/pages/viewpage.action?pageId=36963599
			// When there's no successful response (due to error or timeout)
			// The system doesn't show any messages to end user
			LOG.error(exception);
			LOG.error(ExceptionUtils.getRootCause(exception));
		}
		return invalidPoNumbers;
	}
}
