package com.wiley.core.payment.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.order.impl.DefaultPaymentModeService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.checkout.configuration.WileyOrderConfigurationService;
import com.wiley.core.model.WileyOrderConfigurationModel;
import com.wiley.core.payment.WileyPaymentModeService;


public class WileyPaymentModeServiceImpl extends DefaultPaymentModeService implements WileyPaymentModeService
{

	@Resource
	private WileyOrderConfigurationService wileyOrderConfigurationService;

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public List<PaymentModeModel> getSupportedPaymentModes(final AbstractOrderModel orderModel)
	{
		List<PaymentModeModel> supportedPaymentModes = new ArrayList<>();

		wileyOrderConfigurationService
				.getOrderConfiguration(orderModel)
				.ifPresent(orderConfiguration ->
						supportedPaymentModes.addAll(filterByPaymentModesAvailableForBaseStore(orderConfiguration)));

		return supportedPaymentModes;
	}

	private List<PaymentModeModel> filterByPaymentModesAvailableForBaseStore(
			final WileyOrderConfigurationModel orderConfigurationModel)
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return orderConfigurationModel.getPermittedPaymentModes()
				.stream()
				.filter(currentBaseStore.getPaymentModes()::contains)
				.collect(Collectors.toList());
	}
}
