package com.wiley.core.integration.esb;

import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.model.ExternalDeliveryModeModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import javax.annotation.Nonnull;
import java.util.List;


/**
 * Service to delegate cart calculation for external service (ESB).
 */
public interface EsbCartCalculationGateway
{
	String SAP_ACCOUNT_NUMBER = "sapAccountNumber";
	String BUSINESS_PRICE_CURRENCY = "header-currency";
	String BUSINESS_PRICE_COUNTRY = "header-country";

	/**
	 * Execute external cart calculation.<br/>
	 * Gateway's chain can modify next entities:
	 * <ul>
	 * <li>Cart</li>
	 * <li>All cart entries</li>
	 * <li>ExternalDiscountValues of cart and cart entries</li>
	 * </ul>
	 *
	 * Depending on received data from ESB, new cart entries can be added and existing can be updated or removed.
	 *
	 * @param cartModel
	 * 		cart for calculation
	 * @return cart modifications which have been applied on the cart
	 * @throws de.hybris.platform.servicelayer.exceptions.SystemException
	 * 		if something went wrong.
	 * @throws ExternalSystemInternalErrorException
	 * 		if system has internal error.
	 * @throws ExternalSystemAccessException
	 * 		if system is not available or response time is exceed.
	 */
	@Nonnull
	List<ExternalCartModification> verifyAndCalculateCart(@Nonnull CartModel cartModel);

	/**
	 * Calculate list of external delivery modes with costs for order
	 * orderModel should contain not empty delivery address
	 *
	 * @param orderModel
	 * 		contains objects which are qualified as payload for external call.
	 * @return List of ExternalDeliveryModeModel
	 */
	List<ExternalDeliveryModeModel> getExternalDeliveryModes(@Nonnull AbstractOrderModel orderModel);

	/**
	 * External product price calculation gateway for B2B websites.
	 *
	 * @param productModel
	 * 		Payload for product model.
	 * @param sapAccountNumber
	 * 		Header for sapAccountNumber
	 * @param sessionCountry
	 * 		Header for sessionCountry
	 * @param sessionCurrency
	 * 		Header for sessionCurrency
	 * @return List of PriceInformation for given product.
	 */
	List<PriceInformation> getProductPriceInformations(@Nonnull @Payload ProductModel productModel,
			@Nonnull @Header(SAP_ACCOUNT_NUMBER) String sapAccountNumber,
			@Nonnull @Header(BUSINESS_PRICE_COUNTRY) String sessionCountry,
			@Nonnull @Header(BUSINESS_PRICE_CURRENCY) String sessionCurrency);
}
