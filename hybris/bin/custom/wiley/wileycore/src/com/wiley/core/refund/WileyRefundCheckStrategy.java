package com.wiley.core.refund;

import de.hybris.platform.core.model.order.OrderModel;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface WileyRefundCheckStrategy
{
	/**
	 * Check if order is fully refunded
	 * @param order - Order
	 * @return true if order is fully refunded
	 * @throws IllegalStateException  - if order does not have Capture transaction
	 *	or refund transaction amount is bigger than original capture amount
	 * @throws NullPointerException - if order is null
	 */
	boolean isFullyRefundedOrder(OrderModel order);

	boolean isOrderRefundedByOneTransaction(WileyExportProcessModel process);
}
