package com.wiley.core.order.impl;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.coupon.WileyCouponService;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.order.WileyOrderDiscountGroupService;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.core.product.WileyProductDiscountService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.DiscountValue;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.util.List;


public class WileyOrderDiscountGroupServiceImpl implements WileyOrderDiscountGroupService
{

	@Resource
	private ModelService modelService;

	@Resource
	private WileyProductDiscountService wileyProductDiscountService;

	@Resource
	private WileyCouponService wileyCouponService;

	@Resource
	private WileyPartnerService wileyPartnerService;

	@Override
	public UserDiscountGroup resolveDiscountGroupByAbstractOrder(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder", abstractOrder);
		return abstractOrder.getDiscountGroup();
	}

	@Override
	public void decideIfDiscountGroupOrVoucherShouldBeApplied(@Nonnull final CommerceCartParameter commerceCartParameter)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("commerceCartParameter", commerceCartParameter);

		final CartModel cartModel = commerceCartParameter.getCart();
		final WileyPartnerCompanyModel partnerCompany = wileyPartnerService.getPartnerById(commerceCartParameter.getPartnerId());

		final boolean hasStudentDiscountProducts = hasStudentDiscountProducts(cartModel);
		final boolean hasPartnerProducts = hasPartnerProducts(cartModel, partnerCompany);

		if (hasPartnerOrStudentInfo(cartModel))
		{
			final boolean shouldResetPartner = !hasPartnerProducts;
			final boolean shouldResetStudent = !hasStudentDiscountProducts;

			resetStudentOrPartner(cartModel, shouldResetPartner, shouldResetStudent);
		}

		if (partnerCompany != null && commerceCartParameter.getPartnerId() != null && hasPartnerProducts)
		{
			cartModel.setWileyPartner(partnerCompany);
			modelService.save(cartModel);
		}
		if (commerceCartParameter.isApplyStudentDiscount() && hasStudentDiscountProducts)
		{
			wileyCouponService.redeemCoupon(WileyCoreConstants.STUDENT_COUPON_CODE, cartModel);
		}
	}


	private void resetStudentOrPartner(final AbstractOrderModel abstractOrder, boolean shouldResetPartner,
			boolean shouldResetStudent)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder", abstractOrder);
		if (shouldResetStudent)
		{
			abstractOrder.setUniversity(null);
			wileyCouponService.releaseCouponCode(WileyCoreConstants.STUDENT_COUPON_CODE, abstractOrder);
		}
		if (shouldResetPartner)
		{
			abstractOrder.setWileyPartner(null);
		}
		if (shouldResetPartner || shouldResetStudent)
		{
			getModelService().save(abstractOrder);
		}
	}

	private boolean hasPartnerOrStudentInfo(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrder", abstractOrder);
		return abstractOrder.getDiscountGroup() != null
				|| abstractOrder.getUniversity() != null
				|| abstractOrder.getWileyPartner() != null;
	}

	@Override
	public boolean hasStudentDiscountProducts(@Nonnull final AbstractOrderModel abstractOrder)
	{
		boolean result = false;
		final UserDiscountGroup studentDiscountGroup = UserDiscountGroup.STUDENT;
		for (AbstractOrderEntryModel abstractOrderEntryModel : abstractOrder.getEntries())
		{
			final List<DiscountValue> discountValues =
					wileyProductDiscountService.getPotentialDiscountForCurrentCustomer(
							abstractOrderEntryModel.getProduct(), studentDiscountGroup);
			if (!discountValues.isEmpty())
			{
				result = true;
				break;
			}
		}
		return result;
	}

	private boolean hasPartnerProducts(final CartModel cartModel, final WileyPartnerCompanyModel companyFromParameters)
	{
		WileyPartnerCompanyModel partnerCompany = companyFromParameters;
		if (partnerCompany == null)
		{
			partnerCompany = cartModel.getWileyPartner();
		}
		return wileyPartnerService.orderContainsPartnerProduct(partnerCompany, cartModel);
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
