package com.wiley.core.product.interceptors.freetrial;

import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialProductModel;


/**
 * Sets defaults fields for {@link WileyFreeTrialProductModel}
 */
public class WileyFreeTrialProductInitInterceptor implements InitDefaultsInterceptor<WileyFreeTrialProductModel>
{
	@Override
	public void onInitDefaults(final WileyFreeTrialProductModel wileyFreeTrialProductModel,
			final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		wileyFreeTrialProductModel.setEditionFormat(ProductEditionFormat.DIGITAL);
	}
}
