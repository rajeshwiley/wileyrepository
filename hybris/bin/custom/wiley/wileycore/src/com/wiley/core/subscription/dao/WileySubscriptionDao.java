/**
 *
 */
package com.wiley.core.subscription.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Dao to retrieve subscription related data
 */
public interface WileySubscriptionDao
{

	/**
	 * Retrieves subscription page for the current user
	 *
	 *
	 * @param catalogVersions
	 * @param pageableData
	 * @return
	 */
	SearchPageData<WileySubscriptionModel> getSubscriptions(UserModel currentUser,
			Collection<CatalogVersionModel> catalogVersions, PageableData pageableData);

	/**
	 * Get subscription by code
	 */
	List<WileySubscriptionModel> getSubscription(String code);

	/**
	 * Dao to get subscription by customer and status
	 */
	List<WileySubscriptionModel> getSubscription(UserModel customer, SubscriptionStatus status);

	/**
	 * Looks for WileySubscription whose expiration date is before than given moment.
	 * This method could be used for getting items to update expiration status
	 *
	 * @param status
	 * 		The status of subscription
	 * @param moment
	 * 		An edge date-time value
	 * @return the list of {@link WileySubscriptionModel} with corresponding status and experation date.
	 */
	List<WileySubscriptionModel> findSubscriptionsWithExpirationDateBefore(@Nonnull SubscriptionStatus status,
			@Nonnull ZonedDateTime moment);

	/**
	 * Find subscriptions with expiration date between start and end date.<br/>
	 *
	 * @param status
	 * 		it is used to find subscriptions with necessary subscription status.
	 * @param start
	 * 		the start. Included.
	 * @param end
	 * 		the end. Excluded.
	 * @return the list of {@link WileySubscriptionModel} with corresponding status and experation date.
	 */
	@Nonnull
	List<WileySubscriptionModel> findSubscriptionsWithExpirationDateBetween(@Nonnull SubscriptionStatus status,
			@Nonnull ZonedDateTime start, @Nonnull ZonedDateTime end);

	/**
	 * Looks for Renewal processes working/worked with certain WileySubscription by given state
	 * In future we may have not only renewal, but some other business process having WileySubscription in context
	 *
	 * @param subscriptionCode
	 * 		WileySubscription.code value of subscription assigned to process
	 * @param states
	 * 		Interesting ProcessState. Combined by OR-clause in query
	 * @return List of WileySubscriptionProcess connected to given WileySubscription being in given states at the moment of query
	 */
	List<WileySubscriptionProcessModel> findRenewalProcessesForSubscription(@Nonnull String subscriptionCode,
			ProcessState... states);

	/**
	 * Retrieves payment transactions for a subscription
	 *
	 * @param currentUser
	 * @param pageableData
	 * @return
	 */
	SearchPageData<WileySubscriptionPaymentTransactionModel> getSubscriptionBillingActivities(String subscriptionCode,
			UserModel currentUser, PageableData pageableData);

	/**
	 * Search for subscriptions by its external code (identifier).
	 *
	 * @param externalCode
	 * 		The external universally unique identifier of the subscription. Expected to be provided by SAP ERP.
	 * @return internal codes of all found subscriptions.
	 */
	List<WileySubscriptionModel> getInternalCodesByExternalCode(@Nonnull String externalCode);
}
