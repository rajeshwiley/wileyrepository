package com.wiley.core.wileyb2c.order;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


public interface Wileyb2cCommerceCartService extends CommerceCartService
{

	/**
	 * Saves continueUrl for cart.
	 *
	 * @param continueUrl
	 * 		continueUrl is used on cart page in 'Continue shopping' button
	 * @param cartModel
	 * 		target cart
	 */
	void saveContinueUrlForCart(@Nullable String continueUrl, @Nonnull CartModel cartModel);

}
