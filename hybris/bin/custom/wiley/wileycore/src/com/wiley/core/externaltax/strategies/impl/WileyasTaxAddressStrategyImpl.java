package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;


public class WileyasTaxAddressStrategyImpl implements WileyTaxAddressStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasTaxAddressStrategyImpl.class);

	@Override
	public AddressModel resolvePaymentAddress(final AbstractOrderModel abstractOrder)
	{
		if (abstractOrder.getPaymentAddress() == null)
		{
			LOG.warn("Payment Address is null for order code", abstractOrder.getCode());
		}
		return abstractOrder.getPaymentAddress();
	}

	@Override
	public AddressModel resolveDeliveryAddress(final AbstractOrderModel abstractOrder)
	{
		return null;
	}
}