package com.wiley.core.integration.esb;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * Created by Aliaksei_Zlobich on 8/1/2016.
 */
public interface EsbOrderGateway
{

	void sendOrder(OrderModel orderModel);

}
