package com.wiley.core.product.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;



import javax.annotation.Resource;

import java.util.Optional;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.dao.WileyVariantProductDao;


/**
 * Default implementation of {@link WileyVariantProductDao}
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyVariantProductDao implements WileyVariantProductDao
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<WileyVariantProductModel> findProductByISBNIfExists(final String isbn,
			final CatalogVersionModel catalogVersion)
	{
		WileyVariantProductModel example = new WileyVariantProductModel();
		example.setIsbn(isbn);
		example.setCatalogVersion(catalogVersion);
		WileyVariantProductModel modelByExample = null;
		try
		{
			modelByExample = getFlexibleSearchService().getModelByExample(example);
		}
		catch (ModelNotFoundException e)
		{
			return Optional.empty();
		}
		return Optional.of(modelByExample);
	}

	/**
	 * Gets flexible search service.
	 *
	 * @return the flexible search service
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * Sets flexible search service.
	 *
	 * @param flexibleSearchService
	 * 		the flexible search service
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
