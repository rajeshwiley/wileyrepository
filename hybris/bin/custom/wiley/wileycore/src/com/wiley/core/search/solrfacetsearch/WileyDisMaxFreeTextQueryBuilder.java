package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DisMaxFreeTextQueryBuilder;
import org.apache.commons.lang.StringUtils;

public class WileyDisMaxFreeTextQueryBuilder extends DisMaxFreeTextQueryBuilder
{

	@Override
	public String buildQuery(final SearchQuery searchQuery) {
		if (StringUtils.isBlank(searchQuery.getUserQuery())) {
			return "";
		} else {
			String userQuery = searchQuery.getUserQuery();
			String result = userQuery.replaceAll("\"", "");
			return "{!edismax}" + result;
		}
	}
}
