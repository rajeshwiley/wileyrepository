package com.wiley.core.promotions.interceptors;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.promotionengineservices.interceptors.PromotionRulePrepareInterceptor;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;

import javax.validation.constraints.NotNull;
import java.util.Locale;


/**
 * Wiley promotion rule interceptor
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyPromotionRulePrepareInterceptor extends PromotionRulePrepareInterceptor
{
	/**
	 * OOTB_CODE copy paste of PromotionRulePrepareInterceptor to add localized product banner
	 */
	@Override
	protected void doOnPrepare(@NotNull final AbstractRuleEngineRuleModel model, @NotNull final InterceptorContext context)
	{
		RuleBasedPromotionModel ruleBasedPromotion = model.getPromotion();
		if (ruleBasedPromotion == null || !model.getVersion().equals(ruleBasedPromotion.getRuleVersion()))
		{
			ruleBasedPromotion = createNewPromotionAndAddToRuleModel(model);
		}

		AbstractRuleModel rule = getRuleDao().findRuleByCode(model.getCode());
		if (rule != null)
		{
			setLocalizedName(rule, ruleBasedPromotion);
			ruleBasedPromotion.setPriority(rule.getPriority());
			context.registerElementFor(ruleBasedPromotion, PersistenceOperation.SAVE);

			// Custom code for localized product banner
			if (rule instanceof PromotionSourceRuleModel)
			{
				PromotionSourceRuleModel promotionSourceRuleModel = (PromotionSourceRuleModel) rule;
				setLocalizedDescription(promotionSourceRuleModel, ruleBasedPromotion);
				setLocalizedProductBanner(promotionSourceRuleModel, ruleBasedPromotion);
			}
		}

		if (context.isModified(model, AbstractRuleEngineRuleModel.MESSAGEFIRED) && setLocalizedMessageFired(model,
				ruleBasedPromotion))
		{
			context.registerElementFor(ruleBasedPromotion, PersistenceOperation.SAVE);
		}

	}

	protected void setLocalizedDescription(final PromotionSourceRuleModel rule, final RuleBasedPromotionModel promotion)
	{
		copyLocalized(rule, PromotionSourceRuleModel.LONGDESCRIPTION, promotion,
				RuleBasedPromotionModel.PROMOTIONDESCRIPTION);
	}

	private void setLocalizedProductBanner(final PromotionSourceRuleModel rule, final RuleBasedPromotionModel promotion)
	{
		copyLocalized(rule, PromotionSourceRuleModel.PRODUCTBANNER, promotion, RuleBasedPromotionModel.PRODUCTBANNER);
	}

	private void setLocalizedName(final AbstractRuleModel rule, final RuleBasedPromotionModel promotion)
	{
		copyLocalized(rule, AbstractRuleModel.NAME, promotion, RuleBasedPromotionModel.NAME);
	}

	private void copyLocalized(final AbstractRuleModel rule, final String rulePropertyToGet,
							   final RuleBasedPromotionModel promotion, final String promoPropertyToSet)
	{
		for (LanguageModel language : getCommonI18NService().getAllLanguages())
		{
			Locale locale = getCommonI18NService().getLocaleForLanguage(language);
			promotion.setProperty(promoPropertyToSet, locale, rule.getProperty(rulePropertyToGet, locale));
		}
	}
}
