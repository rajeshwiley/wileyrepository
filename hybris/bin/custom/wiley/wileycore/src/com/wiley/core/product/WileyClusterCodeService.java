package com.wiley.core.product;

import com.wiley.core.model.ClusterCodeModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface WileyClusterCodeService {
    Optional<ClusterCodeModel> getClusterCodeForCurrency(@NotNull ProductModel product, @NotNull CurrencyModel currency);
    
    boolean shouldClusterCodeBeDefinedForThisCurrency(@NotNull CurrencyModel currency);
    
    boolean isClusterCodeMissingForCurrency(@NotNull ProductModel product, @NotNull CurrencyModel currency);
}
