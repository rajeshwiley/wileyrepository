package com.wiley.core.jalo.translators.util;

/**
 * Util to check impex cell value
 */
public final class CellValueUtil
{
	private static final String IMPEX_IGNORE_VALUE = "<ignore>";

	private CellValueUtil()
	{
		//non public constructor for util
	}

	/**
	 * Checks if cell value is ignored
	 *
	 * @param cellValue
	 * 		Cell Value
	 * @return Return true if <code>cellValue</code> is not null and starts with '&lt;gnore&gt;' value
	 */
	public static boolean isIgnoredValue(final String cellValue)
	{
		return cellValue != null && cellValue.startsWith(IMPEX_IGNORE_VALUE);
	}
}
