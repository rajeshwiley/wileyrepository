package com.wiley.core.wileybundle.service.impl;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileybundle.dao.WileyBundleDao;
import com.wiley.core.wileybundle.service.WileyBundleService;
import com.wiley.core.wileybundle.service.WileyBundlesFilterService;


public class WileyBundleServiceImpl implements WileyBundleService
{

	private final WileyBundleDao wileyBundleDao;
	private final WileyProductRestrictionService wileyProductRestrictionService;
	private final WileyBundlesFilterService wileyBundlesFilterService;

	@Autowired
	public WileyBundleServiceImpl(
			final WileyBundleDao wileyBundleDao,
			final WileyProductRestrictionService wileyProductRestrictionService,
			final WileyBundlesFilterService wileyBundlesFilterService)
	{
		this.wileyBundleDao = wileyBundleDao;
		this.wileyProductRestrictionService = wileyProductRestrictionService;
		this.wileyBundlesFilterService = wileyBundlesFilterService;
	}

	@Override
	public List<WileyBundleModel> getBundlesForProduct(@Nonnull final ProductModel productModel)
	{
		Preconditions.checkNotNull(productModel, "Product cant be null");

		if (!wileyProductRestrictionService.isVisible(productModel) || !wileyProductRestrictionService.isAvailable(productModel))
		{
			return Collections.emptyList();
		}

		List<WileyBundleModel> bundles = wileyBundleDao.getBundlesForProduct(productModel);
		return wileyBundlesFilterService.filterBundles(bundles);
	}
}
