package com.wiley.core.wileycom.restriction.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import org.apache.commons.collections4.CollectionUtils;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomRestrictVisibilityByAssortmentStrategy implements WileyRestrictProductStrategy
{
	@Resource
	private WileyBaseStoreService wileyBaseStoreService;
	@Resource
	private BaseSiteService baseSiteService;


	@Override
	public boolean isRestricted(final ProductModel product)
	{
		final BaseSiteModel site = baseSiteService.getCurrentBaseSite();
		final WileyAssortment assortment = wileyBaseStoreService.acquireBaseStoreByBaseSite(site).getAssortment();

		return assortment != null && (CollectionUtils.isEmpty(product.getAssortments())
				|| !product.getAssortments().contains(assortment));
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available due to assortment check.",
						parameter.getProduct().getCode()), null);
	}
}
