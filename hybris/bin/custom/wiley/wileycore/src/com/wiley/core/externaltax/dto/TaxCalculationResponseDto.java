package com.wiley.core.externaltax.dto;

import java.util.List;


public class TaxCalculationResponseDto
{
	private List<TaxCalculationResponseEntryDto> entries;
	private List<TaxValueDto> handlingTaxes;
	private List<TaxValueDto> shippingTaxes;

	public List<TaxCalculationResponseEntryDto> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationResponseEntryDto> entries)
	{
		this.entries = entries;
	}

	public List<TaxValueDto> getHandlingTaxes()
	{
		return handlingTaxes;
	}

	public void setHandlingTaxes(final List<TaxValueDto> handlingTaxes)
	{
		this.handlingTaxes = handlingTaxes;
	}

	public List<TaxValueDto> getShippingTaxes()
	{
		return shippingTaxes;
	}

	public void setShippingTaxes(final List<TaxValueDto> shippingTaxes)
	{
		this.shippingTaxes = shippingTaxes;
	}
}
