package com.wiley.core.promotions.actions.rao.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.converters.populator.ProductRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.CategoryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;
import org.apache.commons.collections.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WileyProductRAOPopulator extends ProductRaoPopulator
{
	@Override
	public void populate(final ProductModel source, final ProductRAO target) throws ConversionException
	{
		super.populate(source, target);
		if (source instanceof VariantProductModel) {
			ProductModel baseProduct = ((VariantProductModel) source).getBaseProduct();
			//add base product categories as well
			Collection<CategoryModel> supercategories = baseProduct.getSupercategories();

			if (CollectionUtils.isNotEmpty(supercategories)) {
				List<CategoryModel> allSuperCategories = new LinkedList<>(supercategories);
				for (CategoryModel category : supercategories) {
					allSuperCategories.addAll(getCategoryService().getAllSupercategoriesForCategory(category));
				}
				List<CategoryRAO> categoryRAOS = getCategoryConverter().convertAll(allSuperCategories);
				Set<CategoryRAO> allCategories = new HashSet<>(target.getCategories());

				allCategories.addAll(categoryRAOS);

				target.setCategories(allCategories);
			}

		}
	}
}
