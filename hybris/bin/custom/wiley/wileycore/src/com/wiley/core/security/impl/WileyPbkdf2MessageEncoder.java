package com.wiley.core.security.impl;

import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.crypto.codec.Hex;

import com.wiley.core.security.WileyMessageEncoder;


/**
 * Calculates the hash using the PBKDF2 algorithm.
 *
 * @author Mikhail_Kondratyev
 */
public class WileyPbkdf2MessageEncoder implements WileyMessageEncoder, InitializingBean
{
	private String secretKey;
	private int iterations = 5000;
	private int keyLength = 512;
	private String keyAlgorithm = "PBKDF2WithHmacSHA512";
	private int saltLength = 128;
	private String saltAlgorithm = "SHA1PRNG";

	private SecureRandom saltGenerator;
	private SecretKeyFactory keyFactory;

	@Override
	public void afterPropertiesSet() throws Exception
	{
		saltGenerator = SecureRandom.getInstance(saltAlgorithm);
		keyFactory = SecretKeyFactory.getInstance(keyAlgorithm);
	}

	@Override
	public String encode(final String message)
	{
		final String messageToEncode = message + secretKey;

		final byte[] salt = new byte[saltLength / 8];
		saltGenerator.nextBytes(salt);

		final byte[] encodedHash = calculateHash(messageToEncode, salt);

		return String.valueOf(Hex.encode(salt)) + String.valueOf(Hex.encode(encodedHash));
	}

	@Override
	public boolean check(final String message, final String hash)
	{
		final String messageToEncode = message + secretKey;

		final byte[] salt = Hex.decode(hash.substring(0, saltLength / 4));
		final byte[] encodedHash = Hex.decode(hash.substring(saltLength / 4));

		final byte[] calculatedHash = calculateHash(messageToEncode, salt);

		return Arrays.equals(encodedHash, calculatedHash);
	}

	private byte[] calculateHash(final String message, final byte[] salt)
	{
		try
		{
			final PBEKeySpec specification = new PBEKeySpec(message.toCharArray(), salt, iterations, keyLength);
			return keyFactory.generateSecret(specification).getEncoded();
		}
		catch (InvalidKeySpecException e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	@Required
	public void setSecretKey(final String secretKey)
	{
		this.secretKey = secretKey;
	}

	public void setIterations(final int iterations)
	{
		this.iterations = iterations;
	}

	public void setKeyLength(final int keyLength)
	{
		this.keyLength = keyLength;
	}

	public void setKeyAlgorithm(final String keyAlgorithm)
	{
		this.keyAlgorithm = keyAlgorithm;
	}

	public void setSaltLength(final int saltLength)
	{
		this.saltLength = saltLength;
	}

	public void setSaltAlgorithm(final String saltAlgorithm)
	{
		this.saltAlgorithm = saltAlgorithm;
	}
}
