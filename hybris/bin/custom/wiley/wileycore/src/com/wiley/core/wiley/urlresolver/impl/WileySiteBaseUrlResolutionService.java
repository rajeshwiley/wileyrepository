package com.wiley.core.wiley.urlresolver.impl;

import de.hybris.platform.acceleratorservices.urlresolver.impl.DefaultSiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.web.util.UrlUtils;

import com.wiley.core.store.WileyBaseStoreService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Extension of {@link DefaultSiteBaseUrlResolutionService} to use base store instead of base site
 * for resolving media and site URLs. It's done to provide possibility to create new legacy
 * site without adding new properties.
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileySiteBaseUrlResolutionService extends DefaultSiteBaseUrlResolutionService
{
	private static final String CMS_SITE_MODEL_CANNOT_BE_NULL_MSG = "CMS site model cannot be null";

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	/**
	 * Added checking if path in media is an absolute url.
	 *
	 * @param site
	 * 		the base site
	 * @param secure
	 * 		flag to indicate is HTTPS url is required
	 * @param path
	 * 		path for the media
	 * @return The URL for the media
	 */
	@Override
	public String getMediaUrlForSite(final BaseSiteModel site, final boolean secure, final String path)
	{
		validateParameterNotNull(site, CMS_SITE_MODEL_CANNOT_BE_NULL_MSG);

		if (path != null && UrlUtils.isAbsoluteUrl(path))
		{
			return path;
		}

		return super.getMediaUrlForSite(site, secure, path);
	}

	/**
	 * OOTB_CODE
	 * Resolves media base url for the given site.
	 * Based on {@link DefaultSiteBaseUrlResolutionService#getMediaUrlForSite(BaseSiteModel, boolean)}. Mostly custom logic is
	 * implemented in {@link #getBaseStoreUid(BaseSiteModel)}
	 *
	 * @param site
	 * 		the base site
	 * @param secure
	 * 		flag to indicate is HTTPS url is required
	 * @return The URL for the media root
	 */
	@Override
	public String getMediaUrlForSite(final BaseSiteModel site, final boolean secure)
	{
		validateParameterNotNull(site, "CMS site model cannot be null");

		final String url = cleanupUrl(lookupConfig("media." + getBaseStoreUid(site) + (secure ? ".https" : ".http")));
		if (url != null)
		{
			return url;
		}
		return getDefaultMediaUrlForSite(site, secure);
	}

	/**
	 * OOTB_CODE
	 * Resolves website base url for the given site.
	 * Based on {@link DefaultSiteBaseUrlResolutionService#getWebsiteUrlForSite(BaseSiteModel, String, boolean, String)}. Mostly
	 * custom logic is implemented in {@link #getBaseStoreUid(BaseSiteModel)}
	 *
	 * @param site
	 * 		the base site
	 * @param secure
	 * 		flag to indicate is HTTPS url is required
	 * @param path
	 * 		the path to include in the url
	 * @return The URL for the website
	 */
	@Override
	public String getWebsiteUrlForSite(final BaseSiteModel site, final String encodingAttributes, final boolean secure,
			final String path)
	{
		validateParameterNotNull(site, "CMS site model cannot be null");

		final String url = cleanupUrl(lookupConfig("website." + getBaseStoreUid(site) + (secure ? ".https" : ".http")));
		if (url != null)
		{
			// if url contains ? remove everything after ? then add path then add back the query string
			// this is so website urls in config files can have query strings and urls in emails will be
			// formatted correctly
			if (url.contains("?"))
			{
				final String queryString = url.substring(url.indexOf('?'));
				final String tmpUrl = url.substring(0, url.indexOf('?'));
				return cleanupUrl(tmpUrl) + (StringUtils.isNotBlank(encodingAttributes) ? encodingAttributes : "") + (
						path == null ? "" : path) + "/" + queryString;
			}

			return url + (StringUtils.isNotBlank(encodingAttributes) ? encodingAttributes : "") + (path == null ? "" : path);
		}
		return getDefaultWebsiteUrlForSite(site, secure, path);
	}

	private String getBaseStoreUid(final BaseSiteModel site)
	{
		Optional<BaseStoreModel> optionalStore = wileyBaseStoreService.getBaseStoreForBaseSite(site);
		return optionalStore.map(BaseStoreModel::getUid).orElseThrow(
				() -> new IllegalStateException("Can not resolve store for site " + site.getUid()));
	}
}
