package com.wiley.core.servicelayer;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import java.util.Collection;

import javax.annotation.Nonnull;


/**
 * Service to get hybris items
 */
public interface WileyItemService
{
	/**
	 * Find catalog-aware items including subtypes
	 *
	 * @param type
	 * 		Root composed type of items, should be catalog-aware composed type
	 * @param catalogVersions
	 * 		Non-empty catalog version list
	 * @return Collection of {@link ItemModel}
	 */
	@Nonnull
	Collection<ItemModel> getCatalogAwareItems(@Nonnull ComposedTypeModel type,
			@Nonnull Collection<CatalogVersionModel> catalogVersions);
}
