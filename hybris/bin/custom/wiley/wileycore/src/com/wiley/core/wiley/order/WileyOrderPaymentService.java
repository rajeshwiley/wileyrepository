package com.wiley.core.wiley.order;

import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;
import java.util.Collection;

public interface WileyOrderPaymentService
{
	Collection<PendingPaymentActon> getPendingPaymentActions(OrderModel model);
	Collection<PendingPaymentActon> getPendingPaymentActions(OrderModel model, PaymentActonType paymentActonType);

	boolean isCurrencyChanged(OrderModel model);
	
	BigDecimal calculateAmountChanged(OrderModel model);
}
