package com.wiley.core.product.impl;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileyProductVariantSetInformation;
import com.wiley.core.product.WileyProductVariantSetService;
import com.wiley.core.product.dao.WileyProductVariantSetDao;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Default implementation of {@link WileyProductVariantSetService}
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyProductVariantSetService implements WileyProductVariantSetService
{

	private static final Logger LOG = Logger.getLogger(DefaultWileyProductVariantSetService.class);

	@Resource
	private WileyProductVariantSetDao wileyProductVariantSetDao;

	@Resource(name = "productService")
	private WileyProductService wileyProductService;

	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	@Nonnull
	public List<WileyProductVariantSetInformation> getWileyProductSetsByProducts(@Nonnull final Set<ProductModel> products)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("entries", products);

		// Trying to find product sets which contain the entries
		final List<WileyProductVariantSetInformation> results = findSets(products);

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Found [count:%s] product sets.", results.size()));
		}

		return results;
	}

	@Nonnull
	@Override
	public List<WileyProductVariantSetInformation> getWileyProductSetsByBaseProduct(@Nonnull final ProductModel product)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);
		return product.getVariants()
				.stream()
				.filter(variant -> variant instanceof WileyProductVariantSetModel
						&& wileyProductRestrictionService.isVisible(variant))
				.map(WileyProductVariantSetModel.class::cast)
				.map(set ->
				{
					WileyProductVariantSetInformation information = new WileyProductVariantSetInformation();
					information.setIncludedProducts(getSetEntries(set));
					information.setProductSet(set);
					return information;
				}).collect(Collectors.toList());
	}

	@Override
	@Nonnull
	public List<GenericVariantProductModel> getSetEntries(@Nonnull final WileyProductVariantSetModel set)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("set", set);

		// Get variantValueCategory to find appropriate GenericVariantProductS from base product.
		final Set<VariantValueCategoryModel> variantValueCategories = getVariantValueCategories(set);

		if (CollectionUtils.isNotEmpty(variantValueCategories))
		{
			final ProductModel baseProduct = set.getBaseProduct();
			return getWileyProductVariants(baseProduct, variantValueCategories);
		}
		else
		{
			LOG.warn(String.format("VariantValue categories ware not found for ProductVariantSet [%s]", set));
		}

		return Collections.emptyList();
	}

	private List<WileyProductVariantSetInformation> findSets(final Set<ProductModel> abstractOrderProducts)
	{
		final List<WileyProductVariantSetModel> sets = getWileyProductVariantSetDao().findProductSets()
				.stream()
				.filter(variantProductModel -> wileyProductRestrictionService.isVisible(variantProductModel))
				.collect(Collectors.toList());
		List<WileyProductVariantSetInformation> result = new ArrayList<>();

		for (WileyProductVariantSetModel set : sets)
		{
			final List<GenericVariantProductModel> setEntries = getSetEntries(set);

			if (CollectionUtils.isEmpty(abstractOrderProducts) && CollectionUtils.isEmpty(setEntries)
					|| CollectionUtils.isNotEmpty(setEntries) && abstractOrderProducts.containsAll(setEntries))
			{
				WileyProductVariantSetInformation information = new WileyProductVariantSetInformation();
				information.setIncludedProducts(setEntries);
				information.setProductSet(set);
				result.add(information);
			}
		}
		return result;
	}

	private List<GenericVariantProductModel> getWileyProductVariants(final ProductModel baseProduct,
			final Set<VariantValueCategoryModel> variantValueCategories)
	{
		final Collection<VariantProductModel> variants = baseProduct.getVariants();
		if (CollectionUtils.isNotEmpty(variants))
		{
			return variants.stream()
					.filter(variantProductModel -> getWileyProductService().canBePartOfProductSet(variantProductModel)
							&& wileyProductRestrictionService.isVisible(variantProductModel))
					.map(variantProductModel -> (WileyVariantProductModel) variantProductModel)
					.filter(variantProductModel -> variantProductModel.getSupercategories().containsAll(variantValueCategories))
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private Set<VariantValueCategoryModel> getVariantValueCategories(final WileyProductVariantSetModel set)
	{
		final Collection<CategoryModel> categories = set.getSupercategories();
		if (CollectionUtils.isNotEmpty(categories))
		{
			return categories.stream()
					.filter(categoryModel -> categoryModel instanceof VariantValueCategoryModel)
					.map(categoryModel -> (VariantValueCategoryModel) categoryModel)
					.filter(categoryModel -> Boolean.FALSE.equals(categoryModel.getIsSetCategory()))
					.collect(Collectors.toSet());

		}
		return Collections.emptySet();
	}

	public WileyProductVariantSetDao getWileyProductVariantSetDao()
	{
		return wileyProductVariantSetDao;
	}

	public void setWileyProductVariantSetDao(final WileyProductVariantSetDao wileyProductVariantSetDao)
	{
		this.wileyProductVariantSetDao = wileyProductVariantSetDao;
	}

	public WileyProductService getWileyProductService()
	{
		return wileyProductService;
	}

	public void setWileyProductService(final WileyProductService wileyProductService)
	{
		this.wileyProductService = wileyProductService;
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
