package com.wiley.core.subscription.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;


public class WileySubscriptionCodeGenerator
{
	public String generateCode(final AbstractOrderEntryModel entry)
	{
		return entry.getOrder().getCode();
	}
}
