package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.strategies.calculation.impl.DefaultOrderRequiresCalculationStrategy;

import org.apache.commons.collections.CollectionUtils;


/**
 * This strategy is used only for clean up entry taxes if order entries were changed in cart.
 * Created by Uladzimir_Barouski on 6/29/2016.
 */
public class Wileyb2cCartRequiresCalculationStrategy extends DefaultOrderRequiresCalculationStrategy
{
	@Override
	public boolean requiresCalculation(final AbstractOrderEntryModel orderEntry)
	{
		//We have to check if entry has taxes to clean up them if cart was changed
		return Boolean.FALSE.equals(orderEntry.getCalculated()) || CollectionUtils.isNotEmpty(orderEntry.getTaxValues());
	}
}
