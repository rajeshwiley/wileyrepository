package com.wiley.core.integration.b2bunit;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.Collection;
import java.util.List;

import com.wiley.core.exceptions.ExternalSystemException;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface B2BUnitGateway
{
	String SAP_ACCOUNT_NUMBER = "sap-account-number";
	String SITE = "site";

	/**
	 * @param sapAccountNumber
	 * 		Unique identifier of the customer's B2B unit (company).
	 * 		It is also known as Business Partner Number.
	 * @return B2BUnitModel or null if it's not found. The instance of B2BUnitModel is not attached to persistence context
	 * @throws ExternalSystemException
	 * 		ExternalSystemNotFoundException in case customer with sapAccountNumber not found throws
	 */
	B2BUnitModel find(@Header(SAP_ACCOUNT_NUMBER) String sapAccountNumber, @Header(SITE) BaseSiteModel site);

	/**
	 * @param sapAccountNumber
	 * 		Unique identifier of the customer's B2B unit (company).
	 * 		It is also known as Business Partner Number.
	 * @param poNumbers
	 * 		list of Purchase Order Numbers to be validated
	 * @return list of invalid PO Numbers
	 * @throws ExternalSystemException
	 */
	List<String> validatePONumbers(@Header(SAP_ACCOUNT_NUMBER) String sapAccountNumber, @Payload Collection<String> poNumbers);
}
