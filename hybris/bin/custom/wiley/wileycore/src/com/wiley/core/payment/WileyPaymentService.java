package com.wiley.core.payment;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Optional;

public interface WileyPaymentService {
    /**
     * Attempts to perform payment capture for given PaymentTransaction. If this transaction contains entry
     * with transaction type Authorize and status Accepted then capture operation will be invoked.
     *
     * In case of any communication or other not expected errors returned entry will have
     * status ERROR
     * In case of successful response returned entry will
     * have status ACCEPTED
     *
     * @param transaction transaction to perform capture for
     * @return {@link java.util.Optional} containing entry created for capture operation (for successful or failed).
     * Also returns empty optional if given transaction does not have any successful authorization entries
     */
    Optional<PaymentTransactionEntryModel> capture(PaymentTransactionModel transaction);

    /**
     * Performs authorization of payment for given order. Order should be calculated and customer should have valid
     * card token stored in default payment info.
     * In case of any communication or other not expected errors returned entry will
     * have status ERROR
     * In case of successful response returned entry will
     * have status ACCEPTED
     *
     * @param order to authorize payment for
     * @return entry created for given authorization request. It is persisted in DB before return.
     * @throws java.lang.IllegalStateException when order hasn't customer  or customer has no billing address or payment
     * information
     */
    PaymentTransactionEntryModel authorize(AbstractOrderModel order);
}
