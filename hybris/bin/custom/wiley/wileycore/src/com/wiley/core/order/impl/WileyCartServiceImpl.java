package com.wiley.core.order.impl;

import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import com.wiley.core.event.facade.orderinfo.WileyPopulateOrderInfoDataEvent;
import com.wiley.core.order.WileyCartService;
import com.wiley.core.product.WileyProductService;


/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class WileyCartServiceImpl extends WileyCartServiceForAccelerator
		implements WileyCartService, ApplicationEventPublisherAware
{

	private ApplicationEventPublisher applicationEventPublisher;

	@Resource(name = "productService")
	private WileyProductService wileyProductService;

	@Override
	public void setApplicationEventPublisher(final ApplicationEventPublisher applicationEventPublisher)
	{
		this.applicationEventPublisher = applicationEventPublisher;
	}

	@Override
	public void populateFromCurrentCart(final OrderInfoData orderInfoData)
	{
		final WileyPopulateOrderInfoDataEvent wileyPopulateOrderInfoDataEvent = new WileyPopulateOrderInfoDataEvent(
				orderInfoData);
		getApplicationEventPublisher().publishEvent(wileyPopulateOrderInfoDataEvent);
	}

	@Override
	public boolean isCartWithProductsFromCategory(@Nonnull final String categoryCode)
	{
		CartModel cart = hasSessionCart() ? getSessionCart() : null;
		if (cart != null)
		{
			for (AbstractOrderEntryModel entry : cart.getEntries())
			{
				ProductModel product = entry.getProduct();
				if (wileyProductService.isBaseProductFromCategory(product, categoryCode))
				{
					return true;
				}
			}
		}
		return false;
	}

	public ApplicationEventPublisher getApplicationEventPublisher()
	{
		return applicationEventPublisher;
	}
}
