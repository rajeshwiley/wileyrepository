package com.wiley.core.order.service;

import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Nonnull;


public interface WileyOrderStateSerializationService
{
	/**
	 * Serialize OrderModel. It is expected that implementation will convert OrderModel -> OrderWsDTO -> String.
	 */
	String serializeToOrderState(@Nonnull OrderModel order);
}
