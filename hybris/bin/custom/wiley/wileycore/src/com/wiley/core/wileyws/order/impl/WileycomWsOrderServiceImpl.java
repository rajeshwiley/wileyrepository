package com.wiley.core.wileyws.order.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.impl.DefaultOrderService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.wileycom.order.dao.WileycomOrderDao;
import com.wiley.core.wileyws.order.WileycomWsOrderHistoryService;
import com.wiley.core.wileyws.order.WileycomWsOrderService;


/**
 * Created by Anton_Lukyanau on 8/2/2016.
 */
public class WileycomWsOrderServiceImpl extends DefaultOrderService implements WileycomWsOrderService
{
	@Resource
	private WileycomOrderDao wileycomOrderDao;

	@Resource
	private WileycomWsOrderHistoryService wileycomWsOrderHistoryService;


	@Override
	@Transactional
	public void updateOrderStatusAndAddHistoryEntry(final OrderModel orderModel, final OrderStatus orderStatus,
			final String description)
	{
		updateOrderStatus(orderModel, orderStatus);
		wileycomWsOrderHistoryService.createHistoryEntry(orderModel, description);
	}

	@Override
	@Transactional
	public void updateOrderAndAddHistoryEntry(final OrderModel orderModel, final OrderModel snapshot, final String message)
	{
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			getModelService().saveAll(entries);
		}
		getModelService().save(orderModel);
		saveAddress(orderModel.getDeliveryAddress());
		saveAddress(orderModel.getPaymentAddress());
		wileycomWsOrderHistoryService.createHistoryEntry(orderModel, snapshot, message);
	}

	private void saveAddress(final AddressModel addressModel)
	{
		if (addressModel != null)
		{
			getModelService().save(addressModel);
		}
	}

	protected void updateOrderStatus(final OrderModel orderModel, final OrderStatus orderStatus)
	{
		orderModel.setStatus(orderStatus);
		getModelService().save(orderModel);
	}

	@Override
	public OrderModel findOrderByCode(final String orderCode)
	{
		return wileycomOrderDao.findOrdersByCode(orderCode);
	}


	@Override
	public Optional<AbstractOrderEntryModel> getOrderEntryModelByISBN(@NotNull final AbstractOrderModel abstractOrderModel,
			@NotNull final String isbn)
	{
		if (abstractOrderModel.getEntries() == null)
		{
			return Optional.empty();
		}
		return abstractOrderModel.getEntries().stream().filter(orderEntryModel ->
				orderEntryModel.getProduct().getIsbn().equals(isbn)).findAny();
	}
}
