package com.wiley.core.validation.service.impl;

import de.hybris.platform.validation.model.constraints.AbstractConstraintModel;
import de.hybris.platform.validation.model.constraints.AttributeConstraintModel;
import de.hybris.platform.validation.model.constraints.DynamicConstraintModel;
import de.hybris.platform.validation.services.impl.DefaultConstraintService;


public class WileyConstraintService extends DefaultConstraintService
{
	@Override
	public boolean isConstraintDuplicated(final AbstractConstraintModel constraint)
	{
		if (constraint instanceof DynamicConstraintModel || constraint instanceof AttributeConstraintModel)
		{
			//allow multiple constraints
			return false;
		}
		else
		{
			return super.isConstraintDuplicated(constraint);
		}
	}
}
