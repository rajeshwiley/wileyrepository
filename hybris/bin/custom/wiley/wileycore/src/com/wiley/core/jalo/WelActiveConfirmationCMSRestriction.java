package com.wiley.core.jalo;


import de.hybris.platform.jalo.SessionContext;


public class WelActiveConfirmationCMSRestriction extends GeneratedWelActiveConfirmationCMSRestriction
{
	@Override
	public String getDescription(final SessionContext sessionContext)
	{
		return "Active confirmation message is restricted!";
	}
}
