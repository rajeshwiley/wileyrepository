package com.wiley.core.wileyas.order.dao.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.wiley.core.order.dao.impl.DefaultWileyOrderEntryDao;
import com.wiley.core.wileyas.order.dao.WileyasOrderEntryDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Implementation of {@link WileyasOrderEntryDao}
 */
public class WileyasOrderEntryDaoImpl extends DefaultWileyOrderEntryDao implements WileyasOrderEntryDao
{
	private static final String QUERY_FIND_ORDER_ENTRIES_BY_BUSINESS_KEY_EXCLUDING_STATUSES =
			"SELECT {e." + AbstractOrderEntryModel.PK + "} FROM"
					+ " {" + AbstractOrderEntryModel._TYPECODE + " AS e }"
					+ " WHERE {e." + AbstractOrderEntryModel.BUSINESSKEY + "} = ?businessKey"
					+ " AND ({e." + AbstractOrderEntryModel.STATUS + "} IS NULL"
					+ " OR {e." + AbstractOrderEntryModel.STATUS + "} NOT IN (?orderStatuses))";


	@Override
	public List<AbstractOrderEntryModel> findOrderEntriesByBusinessKey(@Nonnull final String businessKey,
			@Nonnull final List<OrderStatus> excludedOrderEntryStatuses)
	{
		validateParameterNotNullStandardMessage("businessKey", businessKey);
		validateParameterNotNullStandardMessage("excludedOrderEntryStatuses", excludedOrderEntryStatuses);

		final Map<String, Object> params = new HashMap<>();
		params.put("businessKey", businessKey);
		params.put("orderStatuses", excludedOrderEntryStatuses);
		SearchResult<AbstractOrderEntryModel> searchResult = getFlexibleSearchService().search(
				QUERY_FIND_ORDER_ENTRIES_BY_BUSINESS_KEY_EXCLUDING_STATUSES, params);
		return searchResult.getResult();
	}
}
