package com.wiley.core.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.exception.FullfilmentProcessStaringException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Optional;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.service.WileyOrderStateSerializationService;


public interface WileyOrderFulfilmentProcessService
{

	/**
	 * Shortcut for {@link #startFulfilmentProcessForOrder(OrderModel, boolean, String, boolean)} without order state.
	 * Method validates order status and should be used only for order in status
	 * {@link de.hybris.platform.core.enums.OrderStatus#CREATED}.
	 *
	 * @throws IllegalArgumentException
	 * 		if order is null or has not CREATED status
	 */
	OrderProcessModel startFulfilmentProcessForOrder(OrderModel order, boolean shouldStartProcess)
			throws FullfilmentProcessStaringException;


	/**
	 * Creates business process that belongs to BaseStore of passed order and optionally starts it.
	 *
	 * <p>If business process is type of {@link WileyOrderProcessModel}, then
	 * field {@link WileyOrderProcessModel#ACTIVEFOR} is set with {@code order}.
	 *
	 * <p>{@link WileyOrderProcessModel#ACTIVEFOR} has unique index and quarantees
	 * that hybris won't have two active/failed business processes for one order.
	 * It is expected, that {@link WileyOrderProcessModel} process erases
	 * {@link WileyOrderProcessModel#ACTIVEFOR} field on successful finish.
	 *
	 * @param order
	 * 		is required order for which need to start process
	 * @param shouldStartProcess
	 * 		defines whether process should be started by service method or not
	 * @param previousOrderState
	 * 		Serialized {@code order} before any changes in String representation using
	 * 		{@link WileyOrderStateSerializationService} bean
	 * @param processPayment
	 * 		defines if payment should be processed
	 * @return instance of process
	 * @throws FullfilmentProcessStaringException
	 * 		when order has null store or store has empty fulfillment process definition name
	 * @throws IllegalArgumentException
	 * 		if order is null or previousOrderState is null or empty
	 */
	OrderProcessModel startFulfilmentProcessForOrder(OrderModel order, boolean shouldStartProcess,
			String previousOrderState, boolean processPayment) throws FullfilmentProcessStaringException;

	/**
	 * Check whether {@link WileyOrderProcessModel} exists with assigned {@code abstractOrder}
	 * to {@link WileyOrderProcessModel#ACTIVEFOR}.<br/>
	 * See {@link #startFulfilmentProcessForOrder(OrderModel, boolean, String, boolean)} for details.
	 *
	 * @param order
	 * 		Searchable order
	 * @return true if business process was found, false - otherwise
	 */
	boolean existsProcessActiveFor(OrderModel order);

	/**
	 * Get order fulfillment process which is assigned to order
	 *
	 * @param order
	 * 		Searchable order
	 * @return Optional with business process item
	 */
	Optional<WileyOrderProcessModel> getOrderProcessActiveFor(OrderModel order);
	

	/**
	 * Search for business process which {@link OrderProcessModel#ORDER} is equal to <code>orderModel<code/>
	 * and {@link OrderProcessModel#PROCESSSTATE} is equal to to <code>processState</code>
	 *
	 * @param orderModel
	 * 		order model
	 * @param processState
	 * 		process state
	 * @return Optional with business process item
	 */
	Optional<OrderProcessModel> getOrderProcessInStateForOrder(OrderModel orderModel, ProcessState processState);

}
