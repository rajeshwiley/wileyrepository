package com.wiley.core.event;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;


/**
 * Type of event when customer was registered
 */
public class NewCustomerEvent extends AbstractEvent
{
	private CustomerModel customerModel;
	private String oldEmailAddress;

	public NewCustomerEvent(@Nonnull final CustomerModel customerModel)
	{
		Assert.notNull(customerModel);

		this.customerModel = customerModel;
	}

	public NewCustomerEvent(final CustomerModel customerModel, final String oldEmailAddress)
	{
		this.oldEmailAddress = oldEmailAddress;
		this.customerModel = customerModel;
	}

	public CustomerModel getCustomerModel()
	{
		return customerModel;
	}

	public String getOldEmailAddress()
	{
		return oldEmailAddress;
	}

	public void setOldEmailAddress(final String oldEmailAddress)
	{
		this.oldEmailAddress = oldEmailAddress;
	}
}
