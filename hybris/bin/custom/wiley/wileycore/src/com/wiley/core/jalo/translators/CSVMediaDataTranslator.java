package com.wiley.core.jalo.translators;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.media.MediaDataTranslator;
import de.hybris.platform.jalo.Item;


/**
 * Created by Maksim_Kozich on 26.11.2015.
 */
public class CSVMediaDataTranslator extends MediaDataTranslator
{
	private static final String MEDIA_PREFIX = "mediaPrefix";
	private String mediaPrefix;

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		super.init(columnDescriptor);
		this.mediaPrefix = columnDescriptor.getDescriptorData().getModifier(MEDIA_PREFIX);
	}

	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		super.performImport(mediaPrefix != null ? mediaPrefix + cellValue : cellValue, processedItem);
	}
}
