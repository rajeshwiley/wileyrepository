package com.wiley.core.wileyb2c.savedcart;

import de.hybris.platform.core.model.order.CartModel;


public interface WileyB2CExternalCartService
{
	/**
	 * Find cart details by id in external system and maps it to Hybris cart
	 *
	 * @param cartId Unique identifier of the cart
	 * @return
	 */
	CartModel getExternalCart(String cartId);
}
