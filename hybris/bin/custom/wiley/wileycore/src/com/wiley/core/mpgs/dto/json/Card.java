package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Card
{
	private String brand;
	private String fundingMethod;
	private String expiry;
	private String issuer;
	private String number;
	private String scheme;

	public String getBrand()
	{
		return brand;
	}

	public void setBrand(final String brand)
	{
		this.brand = brand;
	}

	public String getFundingMethod()
	{
		return fundingMethod;
	}

	public void setFundingMethod(final String fundingMethod)
	{
		this.fundingMethod = fundingMethod;
	}

	public String getExpiry()
	{
		return expiry;
	}

	public void setExpiry(final String expiry)
	{
		this.expiry = expiry;
	}

	public String getIssuer()
	{
		return issuer;
	}

	public void setIssuer(final String issuer)
	{
		this.issuer = issuer;
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(final String number)
	{
		this.number = number;
	}

	public String getScheme()
	{
		return scheme;
	}

	public void setScheme(final String scheme)
	{
		this.scheme = scheme;
	}
}
