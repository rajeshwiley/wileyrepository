package com.wiley.core.exceptions;

/**
 * Created by Sergiy_Mishkovets on 7/14/2016.
 */
public class ExternalSystemNotFoundException extends ExternalSystemClientErrorException
{
	public ExternalSystemNotFoundException(final String message)
	{
		super(message);
	}

	public ExternalSystemNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemNotFoundException(final Throwable cause)
	{
		super(cause);
	}
}
