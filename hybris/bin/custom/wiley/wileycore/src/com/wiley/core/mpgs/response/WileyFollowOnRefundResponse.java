package com.wiley.core.mpgs.response;


import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;


public class WileyFollowOnRefundResponse extends WileyResponse
{

	private String requestId;
	private Currency currency;
	private BigDecimal totalAmount;
	private Date transactionCreatedTime;

	public Currency getCurrency()
	{
		return currency;
	}


	public void setCurrency(final Currency currency)
	{
		this.currency = currency;
	}


	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}


	public void setTotalAmount(final BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	public String getRequestId()
	{
		return requestId;
	}

	public void setRequestId(final String requestId)
	{
		this.requestId = requestId;
	}

	public Date getTransactionCreatedTime()
	{
		return transactionCreatedTime;
	}

	public void setTransactionCreatedTime(final Date transactionCreatedTime)
	{
		this.transactionCreatedTime = transactionCreatedTime;
	}
}
