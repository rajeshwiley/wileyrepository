package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.listeners;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.enums.IndexMode;
import de.hybris.platform.solrfacetsearch.indexer.IndexerContext;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.listeners.IndexerOperationListener;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.strategies.WileyIndexerStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexerOperationListener extends IndexerOperationListener
{


	/**
	 * OOTB_CODE. Line this.getSolrIndexOperationService().endOperation(context.getIndexOperationId(), false); was moved
	 * to {@link WileyIndexerStrategy#endIndexing()} to prevent update last index date after index for each country
	 *
	 * @param context
	 * @throws IndexerException
	 */
	@Override
	public void afterIndex(final IndexerContext context) throws IndexerException
	{
		try
		{
			FacetSearchConfig e = context.getFacetSearchConfig();
			IndexedType indexedType = context.getIndexedType();
			IndexConfig indexConfig = e.getIndexConfig();
			Index index = context.getIndex();
			IndexOperation indexOperation = context.getIndexOperation();
			if (indexOperation == IndexOperation.FULL && indexConfig.getIndexMode() == IndexMode.DIRECT)
			{
				SolrSearchProvider solrSearchProvider = this.getSolrSearchProviderFactory().getSearchProvider(e, indexedType);
				solrSearchProvider.deleteOldDocuments(index, context.getIndexOperationId());
			}

			this.getSolrIndexService().activateIndex(e.getName(), indexedType.getIdentifier(), index.getQualifier());
		}
		catch (SolrServiceException var8)
		{
			throw new IndexerException(var8);
		}
	}

}
