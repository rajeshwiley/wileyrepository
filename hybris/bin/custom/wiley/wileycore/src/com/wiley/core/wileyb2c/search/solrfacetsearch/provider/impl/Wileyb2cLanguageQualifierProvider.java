package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import de.hybris.platform.solrfacetsearch.provider.impl.LanguageQualifierProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.impl.WileyIndexerServiceImpl;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cLanguageQualifierProvider extends LanguageQualifierProvider
{

	static final String DELIMITER = "_";

	/**
	 * This customization was done due to we run update and delete together
	 * See {@link WileyIndexerServiceImpl#updateIndex(FacetSearchConfig, Map)}
	 */
	@Override
	public Collection<Qualifier> getAvailableQualifiers(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
	{
		Objects.requireNonNull(facetSearchConfig, "facetSearchConfig is null");
		Objects.requireNonNull(indexedType, "indexedType is null");
		final List<LanguageQualifier> qualifiers = new ArrayList<>();

		for (final LanguageModel indexLanguage : facetSearchConfig.getIndexConfig().getLanguages())
		{
			for (final CountryModel indexCountry : facetSearchConfig.getIndexConfig().getCountries())
			{
				final LanguageModel language = getCommonI18NService().getLanguage(
						indexLanguage.getIsocode() + DELIMITER + indexCountry.getIsocode());
				Locale locale = this.getCommonI18NService().getLocaleForLanguage(language);
				qualifiers.add(new LanguageQualifier(language, locale));
			}
		}

		return Collections.unmodifiableList(qualifiers);
	}

}
