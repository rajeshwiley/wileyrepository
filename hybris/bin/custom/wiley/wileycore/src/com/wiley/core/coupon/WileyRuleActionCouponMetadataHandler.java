package com.wiley.core.coupon;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.couponservices.model.AbstractCouponModel;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.promotionengineservices.model.AbstractRuleBasedPromotionActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengine.RuleActionMetadataHandler;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * OOTB code from Hybris 6.6
 * need to fix OOTB bug in Hybris 6.2 that mentioned in
 * https://jira.wiley.com/browse/ECSC-23822
 * Hybris doesn't set applied coupons in action if it works with multi coupons
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
//TODO-1808 remove fix for OOTB bug
public class WileyRuleActionCouponMetadataHandler
		implements RuleActionMetadataHandler<AbstractRuleBasedPromotionActionModel>
{
	private ModelService modelService;
	private String metadataId;
	private CouponService couponService;
	private CartService cartService;

	protected Set<String> getUsedCouponCodes(final AbstractOrderModel order)
	{
		return order.getAllPromotionResults().stream().flatMap((p) -> p.getActions().stream())
				.filter((a) -> a instanceof AbstractRuleBasedPromotionActionModel)
				.map((a) -> (AbstractRuleBasedPromotionActionModel) a)
				.filter((a) -> CollectionUtils.isNotEmpty(a.getUsedCouponCodes()))
				.flatMap((a) -> a.getUsedCouponCodes().stream()).collect(Collectors.toSet());
	}

	@Override
	public void handle(final AbstractRuleBasedPromotionActionModel actionModel,
			final String metadataValue)
	{
		ServicesUtil.validateParameterNotNull(actionModel, "ActionModel can't be null");
		ServicesUtil.validateParameterNotNull(actionModel.getPromotionResult(),
				"PromotionResult of ActionModel can't be null");
		final AbstractOrderModel order = getOrder(actionModel.getPromotionResult());
		ServicesUtil.validateParameterNotNull(order, "Order of ActionModel can't be null");
		if (CollectionUtils.isNotEmpty(order.getAppliedCouponCodes()))
		{
			final Set<String> couponIdsFromActionMetadata = Arrays.stream(metadataValue.split(","))
					.map((s) -> s.replaceAll("\"", "").trim()).collect(Collectors.toSet());
			final Set<String> usedCouponCodes = getUsedCouponCodes(order);
			final List<String> orderCouponCodesToUse = order.getAppliedCouponCodes()
					.stream().filter((cc) -> !usedCouponCodes.contains(cc)
							&& isCouponPresentInActionMetadata(couponIdsFromActionMetadata, cc))
					.collect(Collectors.toList());
			usedCouponCodes.addAll(orderCouponCodesToUse);
			actionModel.setUsedCouponCodes(orderCouponCodesToUse);
			final List<String> actionMetadataHandlers = Objects.nonNull(actionModel.getMetadataHandlers()) ?
					new ArrayList<>(actionModel.getMetadataHandlers()) : new ArrayList<>();
			if (!actionMetadataHandlers.contains(getMetadataId()))
			{
				actionMetadataHandlers.add(getMetadataId());
				actionModel.setMetadataHandlers(actionMetadataHandlers);
			}
		}

	}

	private boolean isCouponPresentInActionMetadata(final Set<String> couponIdsFromActionMetadata,
			final String couponCode)
	{
		final String couponId = getCouponIdByCouponCode(couponCode);
		return Objects.nonNull(couponId) && couponIdsFromActionMetadata.contains(couponId);
	}

	private String getCouponIdByCouponCode(final String couponCode)
	{
		return getCouponService().getCouponForCode(couponCode)
				.map(AbstractCouponModel::getCouponId).orElse(null);
	}

	@Override
	public void undoHandle(final AbstractRuleBasedPromotionActionModel actionModel)
	{
		ServicesUtil.validateParameterNotNull(actionModel, "ActionModel can't be null");
		if (CollectionUtils.isNotEmpty(actionModel.getMetadataHandlers()))
		{
			final List<String> actionMetadataHandlers = actionModel.getMetadataHandlers()
					.stream().filter((mdid) -> !mdid.equals(getMetadataId()))
					.collect(Collectors.toList());
			actionModel.setMetadataHandlers(actionMetadataHandlers);
		}

	}

	public AbstractOrderModel getOrder(final PromotionResultModel promotionResult)
	{
		if (getCartService().hasSessionCart()
				&& getCartService().getSessionCart().getCode()
				.equals(promotionResult.getOrder().getCode()))
		{
			return getCartService().getSessionCart();
		}
		else
		{
			return promotionResult.getOrder();
		}
	}


	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public String getMetadataId()
	{
		return metadataId;
	}

	@Required
	public void setMetadataId(final String metadataId)
	{
		this.metadataId = metadataId;
	}

	public CouponService getCouponService()
	{
		return couponService;
	}

	@Required
	public void setCouponService(final CouponService couponService)
	{
		this.couponService = couponService;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
