package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


public class Wileyb2cProductAvailabilityValueProvider extends AbstractWileycomValueProvider<String>
{

	private WileyProductRestrictionService wileyb2cProductRestrictionService;

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			final ProductModel productModel = (ProductModel) model;
			return Collections.singletonList(String.valueOf(wileyb2cProductRestrictionService.isAvailable(productModel)));
		}

		return Collections.emptyList();
	}

	@Required
	public void setWileyb2cProductRestrictionService(
			final WileyProductRestrictionService wileyb2cProductRestrictionService)
	{
		this.wileyb2cProductRestrictionService = wileyb2cProductRestrictionService;
	}

}
