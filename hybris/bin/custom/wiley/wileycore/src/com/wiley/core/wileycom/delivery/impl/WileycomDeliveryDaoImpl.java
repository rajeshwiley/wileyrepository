package com.wiley.core.wileycom.delivery.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.delivery.WileycomDeliveryDao;


/**
 * Created by Mikhail_Asadchy on 8/29/2016.
 */
public class WileycomDeliveryDaoImpl implements WileycomDeliveryDao
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public List<ExternalDeliveryModeModel> getSupportedExternalDeliveryModes(final List<String> externalCodes)
	{
		final String query = "select {edm:pk} from "
				+ "{ExternalDeliveryMode as edm "
				+ "JOIN BaseStore2DeliveryModeRel as rel on {rel:target}={edm:pk} "
				+ "JOIN BaseStore as bs on {rel:source}={bs:pk}} "
				+ "where {edm:externalCode} in (?externalCodes) AND {rel:source} = ?store";
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
		fsq.addQueryParameter("externalCodes", externalCodes);
		fsq.addQueryParameter("store", baseStoreService.getCurrentBaseStore());
		final SearchResult<ExternalDeliveryModeModel> search = flexibleSearchService.search(fsq);
		return search.getResult();
	}
}
