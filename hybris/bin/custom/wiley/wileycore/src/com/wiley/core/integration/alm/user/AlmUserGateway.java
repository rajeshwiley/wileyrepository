package com.wiley.core.integration.alm.user;

import javax.annotation.Nonnull;

import com.wiley.core.integration.alm.user.dto.UserDto;


public interface AlmUserGateway
{
	UserDto getUserData(@Nonnull String userId);
}
