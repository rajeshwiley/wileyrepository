package com.wiley.core.coupon;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.couponservices.services.CouponService;


/**
 * Created by Sergiy_Mishkovets on 10/11/2017.
 */
public interface WileyCouponService extends CouponService
{
	/**
	 * Releases all coupons from the order
	 * @param order
	 */
	void releaseAllCoupons(AbstractOrderModel order);
}
