package com.wiley.core.externaltax.services.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.LocaleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Stopwatch;
import com.wiley.core.externaltax.services.TaxSystemRequestCommand;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceResponse;


/**
 * Encapsulates a request to external tax system as an object
 */
public class TaxSystemRequestCommandImpl implements TaxSystemRequestCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(TaxSystemRequestCommandImpl.class);
	private static final String RESPONSE_STATUS_ERROR = "ERROR";

	@Value("${tax.service.uri}")
	private String taxServiceUri;

	@Value("${tax.service.locale}")
	private String taxServiceLocaleProperty;

	@Autowired
	private RestTemplate restTemplate;

	private NumberFormat taxValueFormat;

	/**
	 * Set properties for @HystrixCommand
	 * All other properties are left default. Please find values here https://github.com/Netflix/Hystrix/wiki/Configuration
	 */
	@PostConstruct
	public void postConstruct()
	{
		Locale locale = LocaleUtils.toLocale(taxServiceLocaleProperty);
		taxValueFormat = DecimalFormat.getInstance(locale);
	}

	@Override
	@Nonnull
	public Double execute(@Nonnull final TaxServiceRequest taxServiceRequest, @Nonnull final String orderCode,
			final int entryNumber)
	{
		Assert.notNull(taxServiceRequest);
		Assert.notNull(orderCode);

		final ResponseEntity<TaxServiceResponse> responseEntity;

		LOG.debug("Getting taxes for for order {} entry {}. \n", orderCode, entryNumber);

		final Stopwatch sw = Stopwatch.createStarted();

		try
		{
			final HttpEntity<TaxServiceRequest> httpEntity = new HttpEntity<>(taxServiceRequest, createHttpHeader());
			responseEntity = restTemplate.exchange(taxServiceUri, HttpMethod.POST, httpEntity, TaxServiceResponse.class);
		}
		catch (Exception e)
		{
			// ResourceAccessException - in case if any IO error occurs
			// RestClientException -  whenever it encounters client-side HTTP errors
			// any other?
			throw new TaxSystemException(String.format(
					"The error occurs during call to external tax calculation service for order [%1$s] entry [%2$s]",
					orderCode, entryNumber), e);
		}
		finally
		{
			sw.stop();
		}

		if (!responseEntity.getStatusCode().is2xxSuccessful())
		{
			throw new TaxSystemException(String.format(
					"Tax Service responded with not successful http status code [%1$s] for order [%2$s] entry [%3$s]",
					responseEntity.getStatusCode(), orderCode, entryNumber));
		}

		final TaxServiceResponse taxServiceResponse = responseEntity.getBody();

		if (RESPONSE_STATUS_ERROR.equals(taxServiceResponse.getTaxAmountOrStatus()))
		{
			throw new TaxSystemResponseError(String.format(
					"Tax Service responded with ERROR status for order [%1$s] entry [%2$s]", orderCode, entryNumber));
		}

		LOG.debug(
				String.format("Tax Service Response [%1$s] for order [%2$s] entry [%3$s] with response time %4$s milliseconds. "
								+ "\n %5$s",
						taxServiceResponse.getTaxAmountOrStatus(), orderCode, entryNumber, sw.elapsed(TimeUnit.MILLISECONDS),
						responseEntity.toString()));

		try
		{
			return unmarshalDouble(taxServiceResponse.getTaxAmountOrStatus());
		}
		catch (ParseException e)
		{
			throw new TaxSystemException(String.format(
					"Tax Service responded with invalid tax amount [%1$s] for order [%2$s] entry [%3$s]",
					taxServiceResponse.getTaxAmountOrStatus(), orderCode, entryNumber));
		}
	}

	private HttpHeaders createHttpHeader()
	{
		final HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_XML);
		return header;
	}

	private double unmarshalDouble(final String taxAmount) throws ParseException
	{
		return taxValueFormat.parse(taxAmount).doubleValue();
	}
}