package com.wiley.core.wileyas.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartEntryModel;

import com.wiley.core.wiley.order.impl.WileyCommerceAddToCartStrategy;


/**
 * Wileyas specific CommerceAddToCartStrategy
 */
public class WileyasCommerceAddToCartStrategyImpl extends WileyCommerceAddToCartStrategy
{

	@Override
	protected void validateAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		super.validateAddToCart(parameter);
	}

	@Override
	protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
			throws CommerceCartModificationException
	{
		CartEntryModel result = super.addCartEntry(parameter, actualAllowedQuantityChange);

		result.setBusinessKey(parameter.getBusinessKey());
		result.setOriginalBusinessKey(parameter.getBusinessKey());
		result.setBusinessItemId(parameter.getBusinessItemId());

		result.setExternalPriceValues(parameter.getExternalPriceValues());
		result.setExternalDiscounts(parameter.getExternalDiscounts());
		result.setNamedDeliveryDate(parameter.getNamedDeliveryDate());
		result.setAdditionalInfo(parameter.getAdditionalInfo());
		result.setStatus(OrderStatus.DRAFT);

		getModelService().save(result);
		return result;
	}

	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final CommerceCartModification modification = doAddToCart(parameter);
		// OOTB modification
		// We need full recalculation when adding a product to cart
		getCommerceCartCalculationStrategy().recalculateCart(parameter);
		afterAddToCart(parameter, modification);
		mergeEntry(modification, parameter);
		return modification;
	}
}
