package com.wiley.core.wileyb2b.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratorservices.company.impl.DefaultB2BCommerceUserService;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;
import com.wiley.core.users.strategy.WileycomChangeUserUidStrategy;
import com.wiley.core.wileyb2b.service.WileyB2BCommerceUserService;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;

public class WileyB2BCommerceUserServiceImpl extends DefaultB2BCommerceUserService
		implements WileyB2BCommerceUserService
{
	private static final int RANDOM_PASSWORD_LENGTH = 16;

	@Resource
	private WileyCustomerGateway wileyCustomerGateway;

	@Resource(name = "wileycomCustomerRegistrationStrategy")
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategy;

	@Resource(name = "wileycomChangeUserUidStrategy")
	private WileycomChangeUserUidStrategy wileycomChangeUserUidStrategy;

	@Override
	public void disableCustomer(final String uid)
	{
		final B2BCustomerModel customerModel = getCustomerForUid(uid);
		validateParameterNotNullStandardMessage("B2BCustomer", uid);
		customerModel.setActive(Boolean.FALSE);
		this.saveModel(customerModel);
		updateCustomerInExternalSystem(customerModel);
	}

	@Override
	public void enableCustomer(final String uid)
	{
		final B2BCustomerModel customerModel = getCustomerForUid(uid);
		validateParameterNotNullStandardMessage("B2BCustomer", uid);
		customerModel.setActive(Boolean.TRUE);
		this.saveModel(customerModel);
		updateCustomerInExternalSystem(customerModel);
	}

	@Override
	public B2BCustomerModel removeUserRole(final String user, final String role)
	{
		final B2BCustomerModel customerModel = super.removeUserRole(user, role);
		updateCustomerInExternalSystem(customerModel);
		return customerModel;
	}

	@Override
	public B2BCustomerModel addUserRole(final String user, final String role)
	{
		final B2BCustomerModel customerModel = super.addUserRole(user, role);
		updateCustomerInExternalSystem(customerModel);
		return customerModel;
	}

	private void updateCustomerInExternalSystem(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("B2BCustomer", customerModel);
		wileyCustomerGateway.updateCustomer(customerModel);
	}

	@Override
	public B2BCustomerModel createCustomer(final B2BCustomerModel b2BCustomerModel, final boolean createUser)
	{
		B2BCustomerModel customerModel;
		if (createUser)
		{
			customerModel = this.getModelService().create(B2BCustomerModel.class);
		}
		else
		{
			customerModel = getCustomerForUid(b2BCustomerModel.getUid());
		}

		return customerModel;
	}

	@Override
	public void saveCustomer(@Nonnull final CustomerModel customerModel, final boolean createUser)
	{
		if (createUser)
		{
			saveModel(customerModel);
			try
			{
				wileyCustomerRegistrationStrategy.register(customerModel, randomAlphanumeric(RANDOM_PASSWORD_LENGTH));
			}
			catch (ExternalSystemException | WileycomCustomerRegistrationFailureException e)
			{
				getModelService().remove(customerModel);
				throw e;
			}
		}
		else
		{
			updateCustomerInExternalSystem(customerModel);
			saveModel(customerModel);
		}
	}

}
