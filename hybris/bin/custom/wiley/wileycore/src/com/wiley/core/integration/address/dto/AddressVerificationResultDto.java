package com.wiley.core.integration.address.dto;

import de.hybris.platform.commerceservices.address.AddressVerificationDecision;

import java.util.List;


public class AddressVerificationResultDto
{
	private AddressVerificationDecision decision;

	private List<AddressDto> suggestedAddresses;

	public AddressVerificationDecision getDecision()
	{
		return decision;
	}

	public void setDecision(final AddressVerificationDecision decision)
	{
		this.decision = decision;
	}

	public List<AddressDto> getSuggestedAddresses()
	{
		return suggestedAddresses;
	}

	public void setSuggestedAddresses(final List<AddressDto> suggestedAddresses)
	{
		this.suggestedAddresses = suggestedAddresses;
	}
}
