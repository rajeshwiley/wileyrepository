package com.wiley.core.exceptions;

/**
 * This exception is thrown if malformed request was sent.
 */
public class ExternalSystemBadRequestException extends ExternalSystemClientErrorException
{
	public ExternalSystemBadRequestException()
	{
		super();
	}

	public ExternalSystemBadRequestException(final String message)
	{
		super(message);
	}

	public ExternalSystemBadRequestException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemBadRequestException(final Throwable cause)
	{
		super(cause);
	}
}
