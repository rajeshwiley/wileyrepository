package com.wiley.core.processengine.process;

import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.processengine.process.ProcessengineTaskRunner;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.logging.ProcessEngineLoggingCtx;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.processengine.log.WileyProcessEngineLoggingCtx;


public class WileyProcessengineTaskRunner extends ProcessengineTaskRunner
{
	private static final int LOG_MESSAGE_LENGTH = 65535;
	private ModelService modelService;
	private ConfigurationService configurationService;

	@Override
	public ProcessEngineLoggingCtx initLoggingCtx(final ProcessTaskModel task)
	{
		return new WileyProcessEngineLoggingCtx(task, modelService,
				configurationService.getConfiguration().getInt("bp.db.logmessage.length", LOG_MESSAGE_LENGTH));
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		super.setModelService(modelService);
		this.modelService = modelService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
