package com.wiley.core.converters;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyModelServiceAwarePopulatingConverter<SOURCE, TARGET> extends AbstractPopulatingConverter<SOURCE, TARGET>
{
    @Resource
    private ModelService modelService;

    private Class<TARGET> targetClass;

    @Override
    public TARGET convert(final SOURCE source) throws ConversionException
    {
        final TARGET target = modelService.create(getTargetClass());
        super.populate(source, target);
        return target;
    }

    public Class<TARGET> getTargetClass()
    {
        return targetClass;
    }

    @Override
    @Required
    public void setTargetClass(final Class<TARGET> targetClass)
    {
        super.setTargetClass(targetClass);
        this.targetClass = targetClass;
    }
}
