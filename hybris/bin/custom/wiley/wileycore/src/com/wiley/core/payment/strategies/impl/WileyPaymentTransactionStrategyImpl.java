package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultPaymentTransactionStrategy;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.strategies.WileyPaymentTransactionStrategy;


public class WileyPaymentTransactionStrategyImpl extends DefaultPaymentTransactionStrategy implements
		WileyPaymentTransactionStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyPaymentTransactionStrategyImpl.class);

	@Resource
	private WileyCreditCardPaymentInfoCreateStrategy wileyCreditCardPaymentInfoCreateStrategy;
	@Resource
	private Converter<CustomerInfoData, AddressModel> customerInfoToBillingAddressConverter;

	@Override
	public PaymentTransactionEntryModel savePaymentTransactionEntry(final CustomerModel customerModel,
			final CurrencyModel currencyModel, final CreateSubscriptionResult subscriptionResult,
			final PaymentTransactionType transactionType)
	{
		PaymentTransactionEntryModel transactionEntry = savePaymentTransactionEntryInternal(customerModel, currencyModel,
				subscriptionResult, transactionType);
		return transactionEntry;
	}

	private PaymentTransactionEntryModel savePaymentTransactionEntryInternal(final CustomerModel customerModel,
			final CurrencyModel currencyModel, final CreateSubscriptionResult subscriptionResult,
			final PaymentTransactionType transactionType)
	{
		PaymentTransactionEntryModel transactionEntry = super.savePaymentTransactionEntry(customerModel,
				subscriptionResult.getRequestId(), subscriptionResult.getOrderInfoData());
		PaymentTransactionModel paymentTransaction = transactionEntry.getPaymentTransaction();
		getModelService().refresh(paymentTransaction);
		WPGHttpValidateResultData wpgResultInfoData = subscriptionResult.getWpgResultInfoData();
		transactionEntry.setRequestId(wpgResultInfoData.getTransID());
		transactionEntry.setRequestToken(wpgResultInfoData.getToken());

		WileyTransactionStatusEnum status = determineTransactionStatus(wpgResultInfoData);
		transactionEntry.setWpgResponseCode(status.getCode());
		transactionEntry.setWpgMerchantResponse(wpgResultInfoData.getMerchantResponse());
		if (PaymentTransactionType.AUTHORIZATION.equals(transactionType))
		{
			transactionEntry.setType(PaymentTransactionType.AUTHORIZATION);
			String value = wpgResultInfoData.getValue();

			if (StringUtils.isNotEmpty(value))
			{
				transactionEntry.setAmount(new BigDecimal(value));
				paymentTransaction.setPlannedAmount(new BigDecimal(value));
			}
			transactionEntry.setWpgAuthCode(wpgResultInfoData.getAuthCode());
			transactionEntry.setCurrency(currencyModel);
			paymentTransaction.setCurrency(currencyModel);
			if (WileyTransactionStatusEnum.SUCCESS == status)
			{
				AddressModel billingAddress = customerInfoToBillingAddressConverter.convert(
						subscriptionResult.getCustomerInfoData());
				CreditCardPaymentInfoModel creditCardPaymentInfo =
						wileyCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(
								subscriptionResult.getSubscriptionInfoData(), subscriptionResult.getPaymentInfoData(),
								billingAddress,
								customerModel, false);
				billingAddress.setOwner(creditCardPaymentInfo);
				paymentTransaction.setInfo(creditCardPaymentInfo);
			}
			getModelService().save(paymentTransaction);
		}


		if (WileyTransactionStatusEnum.SUCCESS != status)
		{
			transactionEntry.setTransactionStatus(status.name());
			transactionEntry.setTransactionStatusDetails(status.getDescription());
		}
		getModelService().save(transactionEntry);
		return transactionEntry;
	}

	private WileyTransactionStatusEnum determineTransactionStatus(final WPGHttpValidateResultData wpgResult)
	{
		String returnCode = wpgResult.getReturnCode();
		WileyTransactionStatusEnum status = WileyTransactionStatusEnum.findByCode(returnCode);
		if (status == null)
		{
			status = WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG;
			LOG.error("Unexpected return code '{}' from WPG. '{}'-'{}' will be saved for transaction",
					returnCode,
					status.getCode(),
					status.name());
		}
		return status;
	}
}
