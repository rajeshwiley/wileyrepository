package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.FirstGalleryContainerIsNotVideo;

public class FirstGalleryContainerIsNotVideoValidator
		extends AbstractWileyProductConstraintValidator<FirstGalleryContainerIsNotVideo>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validateFirstGalleryContainerIsNotVideo(productModel.getGalleryImages());
	}
}
