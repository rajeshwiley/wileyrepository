package com.wiley.core.b2b.unit.service.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalAddressService;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbB2BUnitAddressGateway;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WileyB2BUnitExternalAddressServiceImpl implements WileyB2BUnitExternalAddressService
{

	private static final String CREATE_ADDRESS_LOG_MSG = "Creating shipping address for unit [name:[%s] uid:[%s]]";
	private static final String UPDATE_ADDRESS_LOG_MSG = "Updating shipping address with id [%s] from unit [name:[%s] uid:[%s]]";
	private static final String DELETE_ADDRESS_LOG_MSG = "Deleting shipping address with id [%s] from unit [name:[%s] uid:[%s]]";

	private static final Logger LOG = LoggerFactory.getLogger(WileyB2BUnitExternalAddressServiceImpl.class);
	@Resource
	private EsbB2BUnitAddressGateway esbB2BUnitAddressGateway;
	@Override
	public String addShippingAddress(final B2BUnitModel unit, final AddressModel addressModel)
	{
		Preconditions.checkNotNull(unit, "B2BUnit cannot be null");
		Preconditions.checkNotNull(addressModel, "Address cannot be null");

		LOG.info(String.format(CREATE_ADDRESS_LOG_MSG, unit.getName(), unit.getUid()));

		return esbB2BUnitAddressGateway.addShippingAddress(addressModel, unit);
	}
	
	@Override
	public Boolean deleteShippingAddress(final B2BUnitModel unit, final String addressId)
	{
	    Preconditions.checkNotNull(unit, "B2BUnit cannot be null");
	    Preconditions.checkNotNull(addressId, "Address id cannot be null");

		LOG.info(String.format(DELETE_ADDRESS_LOG_MSG, addressId, unit.getName(), unit.getUid()));

		esbB2BUnitAddressGateway.deleteShippingAddress(addressId, unit);

		return Boolean.TRUE;
	}

	@Override
	public boolean updateShippingAddress(final B2BUnitModel unit, final AddressModel addressModel)
			throws ExternalSystemException
	{
		Preconditions.checkNotNull(unit, "B2BUnit cannot be null");
		Preconditions.checkNotNull(addressModel, "Address cannot be null");
		Preconditions.checkArgument(StringUtils.isNotEmpty(addressModel.getExternalId()),
				"Address should have 'externalId'");

		LOG.info(String.format(UPDATE_ADDRESS_LOG_MSG, addressModel.getExternalId(), unit.getName(), unit.getUid()));
		esbB2BUnitAddressGateway.updateShippingAddress(addressModel, unit);

		return true;
	}
}
