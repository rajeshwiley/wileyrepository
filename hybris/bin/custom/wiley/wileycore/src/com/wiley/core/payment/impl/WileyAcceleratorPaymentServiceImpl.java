package com.wiley.core.payment.impl;

import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentErrorField;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.impl.DefaultAcceleratorPaymentService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.exceptions.ExternalSystemClientErrorException;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.model.PendingBillingAddressModel;
import com.wiley.core.payment.WileyAcceleratorPaymentService;
import com.wiley.core.payment.strategies.WPGResponseSecurityValidationStrategy;
import com.wiley.core.payment.strategies.WileyPaymentTransactionStrategy;
import com.wiley.core.wileycom.customer.WileycomExternalAddressService;

import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_AUTH;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_VALIDATE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyAcceleratorPaymentServiceImpl extends DefaultAcceleratorPaymentService implements WileyAcceleratorPaymentService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyAcceleratorPaymentServiceImpl.class);

	private WPGResponseSecurityValidationStrategy responseSecurityValidationStrategy;

	@Autowired
	private WileycomExternalAddressService wileycomExternalAddressService;
	@Autowired
	private CustomerAccountService customerAccountService;
	@Autowired
	private Converter<CustomerInfoData, AddressModel> customerInfoToBillingAddressConverter;
	@Autowired
	private CustomerEmailResolutionService customerEmailResolutionService;

	@Override
	public PaymentSubscriptionResultItem completeHopCreatePaymentSubscription(final CustomerModel customerModel,
			final boolean saveInAccount, final Map<String, String> parameters) throws IllegalArgumentException
	{
		return completeHopCreatePaymentSubscription(customerModel, saveInAccount, false, parameters);
	}


	@Override
	public PaymentSubscriptionResultItem completeHopCreatePaymentSubscriptionWithoutCheckout(final CustomerModel customerModel,
			final Map<String, String> parameters)
	{
		return completeHopCreatePaymentSubscription(customerModel, true, true, parameters);
	}

	private PaymentSubscriptionResultItem completeHopCreatePaymentSubscription(final CustomerModel customerModel,
			final boolean saveInAccount,
			final boolean validateWithoutCheckout,
			final Map<String, String> parameters)
	{
		final Map<String, PaymentErrorField> errors = new HashMap<>();
		final CreateSubscriptionResult response = getHopPaymentResponseInterpretation().interpretResponse(parameters, null,
				errors);

		validateParameterNotNull(response, "CreateSubscriptionResult cannot be null");
		Assert.notNull(response.getDecision(), "Decision cannot be null");
		boolean accepted = DecisionsEnum.ACCEPT.name().equalsIgnoreCase(response.getDecision());
		final PaymentSubscriptionResultItem paymentSubscriptionResult = new PaymentSubscriptionResultItem();
		paymentSubscriptionResult.setDecision(String.valueOf(response.getDecision()));
		paymentSubscriptionResult.setSuccess(accepted);
		paymentSubscriptionResult.setResultCode(String.valueOf(response.getReasonCode()));
		if (accepted)
		{
			completeSuccessfulHopSubscription(customerModel, saveInAccount, validateWithoutCheckout,
					response, paymentSubscriptionResult);
		}
		else
		{
			LOG.error("Cannot create subscription. Decision: '{}', Reason code '{}'",
					response.getDecision(), response.getReasonCode());
			if (validateWithoutCheckout)
			{
				LOG.error("Failed subscription response: {}", parameters);
			}
			else
			{
				CartModel cart = getCartService().getSessionCart();
				updateCartWithSubscriptionTransactionEntry(customerModel, response, cart,
						getPaymentTransactionType(response.getWpgResultInfoData()));
				getModelService().saveAll(cart);
			}
			paymentSubscriptionResult.setErrors(errors);
		}
		return paymentSubscriptionResult;
	}

	private PaymentTransactionType getPaymentTransactionType(final WPGHttpValidateResultData wpgResultInfoData)
	{
		Assert.notNull(wpgResultInfoData, "WpgResultInfoData cannot be null");
		final String operation = wpgResultInfoData.getOperation();
		if (OPERATION_AUTH.equalsIgnoreCase(operation))
		{
			return PaymentTransactionType.AUTHORIZATION;
		}
		else if (OPERATION_VALIDATE.equalsIgnoreCase(operation))
		{
			return PaymentTransactionType.CREATE_SUBSCRIPTION;
		}
		return null;
	}

	private void completeSuccessfulHopSubscription(final CustomerModel customerModel, final boolean saveInAccount,
			final boolean validateWithoutCheckout, final CreateSubscriptionResult response,
			final PaymentSubscriptionResultItem paymentSubscriptionResult)
	{
		WPGHttpValidateResultData wpgResultInfoData = response.getWpgResultInfoData();
		Assert.notNull(wpgResultInfoData, "WpgResultInfoData cannot be null");
		if (getResponseSecurityValidationStrategy().validate(wpgResultInfoData))
		{
			final String operation = wpgResultInfoData.getOperation();
			if (OPERATION_AUTH.equalsIgnoreCase(operation))
			{
				completeSuccessfulHopSubscriptionForAuth(customerModel, saveInAccount, response, paymentSubscriptionResult);
			}
			else if (OPERATION_VALIDATE.equalsIgnoreCase(operation))
			{
				completeSuccessfulHopSubscriptionForValidate(customerModel, saveInAccount, validateWithoutCheckout,
						response, paymentSubscriptionResult);
			}
		}
		else
		{
			LOG.error("Cannot create subscription. Subscription signature does not match.");
		}
	}

	private void completeSuccessfulHopSubscriptionForAuth(final CustomerModel customerModel, final boolean saveInAccount,
			final CreateSubscriptionResult response, final PaymentSubscriptionResultItem paymentSubscriptionResult)
	{

		Assert.notNull(response.getAuthReplyData(),
				String.format("AuthReplyData cannot be null for the request  [%1$s]", response.getRequestId()));
		Assert.notNull(response.getCustomerInfoData(),
				String.format("CustomerInfoData cannot be null for the request  [%1$s] ", response.getRequestId()));
		Assert.notNull(response.getOrderInfoData(),
				String.format("OrderInfoData cannot be null for the request  [%1$s] ", response.getRequestId()));
		Assert.notNull(response.getPaymentInfoData(),
				String.format("PaymentInfoData cannot be null for the request  [%1$s] ", response.getRequestId()));
		Assert.notNull(response.getSignatureData(),
				String.format("SignatureData cannot be null for the request  [%1$s] order ", response.getRequestId()));
		Assert.notNull(response.getSubscriptionInfoData(),
				String.format("SubscriptionInfoData cannot be null for the request  [%1$s] ", response.getRequestId()));
		Assert.notNull(
				response.getSubscriptionSignatureData(),
				String.format("SubscriptionSignatureData cannot be null for the request  [%1$s]",
						response.getRequestId()));


		final CartModel cart = getCartService().getSessionCart();

		updateCartWithSubscriptionTransactionEntry(customerModel, response, cart, PaymentTransactionType.AUTHORIZATION);
		cart.setStatus(OrderStatus.PAYMENT_AUTHORIZED);
		getModelService().saveAll(cart);

		final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy().saveSubscription(
				customerModel, response.getCustomerInfoData(), response.getSubscriptionInfoData(),
				response.getPaymentInfoData(), saveInAccount);
		paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

		// Check if the subscription has already been validated
		final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
				.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
		if (subscriptionValidation != null)
		{
			cardPaymentInfoModel.setSubscriptionValidated(true);
			getModelService().save(cardPaymentInfoModel);
			getModelService().remove(subscriptionValidation);
		}

	}

	protected void completeSuccessfulHopSubscriptionForValidate(final CustomerModel customerModel, final boolean saveInAccount,
			final boolean validateWithoutCheckout, final CreateSubscriptionResult response,
			final PaymentSubscriptionResultItem paymentSubscriptionResult)
	{
		Assert.notNull(response.getCustomerInfoData(), "CustomerInfoData cannot be null");
		Assert.notNull(response.getPaymentInfoData(), "PaymentInfoData cannot be null");
		Assert.notNull(response.getSignatureData(), "SignatureData cannot be null");
		Assert.notNull(response.getSubscriptionInfoData(), "SubscriptionInfoData cannot be null");
		Assert.notNull(response.getSubscriptionSignatureData(), "SubscriptionSignatureData cannot be null");

		final AddressModel billingAddress = customerInfoToBillingAddressConverter.convert(response.getCustomerInfoData(),
				getModelService().create(AddressModel.class));
		final CreditCardPaymentInfoModel cardPaymentInfoModel = getCreditCardPaymentInfoCreateStrategy()
				.createCreditCardPaymentInfo(response.getSubscriptionInfoData(), response.getPaymentInfoData(), billingAddress,
						customerModel, saveInAccount);
		billingAddress.setOwner(cardPaymentInfoModel);
		billingAddress.setEmail(getCustomerEmailResolutionService().getEmailForCustomer(customerModel));

		if (validateWithoutCheckout)
		{
			LOG.debug("Successfully completed validate operation: {}",
					ToStringBuilder.reflectionToString(response.getWpgResultInfoData(), ToStringStyle.MULTI_LINE_STYLE));

			//export address first, do not store payment details if export has failed
			try
			{
				wileycomExternalAddressService.addBillingAddress(customerModel, billingAddress, true);
			}
			catch (ExternalSystemException ex)
			{
				LOG.warn("External system could not save billing address", ex);
				throw new ExternalSystemClientErrorException(ex);
			}
			catch (IllegalArgumentException ex)
			{
				LOG.error("Failed to export billing address due to ", ex);
				throw new ExternalSystemClientErrorException(ex);
			}
		}
		else
		{
			final CartModel cart = getCartService().getSessionCart();
			updateCartWithSubscriptionTransactionEntry(customerModel, response, cart, PaymentTransactionType.CREATE_SUBSCRIPTION);
			getModelService().saveAll(cart);
		}

		saveSubscriptionForBillingAddress(customerModel, response.getCustomerInfoData(), cardPaymentInfoModel,
				response.getPaymentInfoData(), saveInAccount, billingAddress);
		paymentSubscriptionResult.setStoredCard(cardPaymentInfoModel);

		if (validateWithoutCheckout)
		{
			PendingBillingAddressModel pendingBillingAddress = customerModel.getPendingBillingAddress();

			if (Boolean.TRUE.equals(pendingBillingAddress.getDefault()))
			{
				customerAccountService.setDefaultPaymentInfo(customerModel, cardPaymentInfoModel);
			}

			getModelService().remove(pendingBillingAddress);
			getModelService().save(customerModel);
		}

		// Check if the subscription has already been validated
		final CCPaySubValidationModel subscriptionValidation = getCreditCardPaymentSubscriptionDao()
				.findSubscriptionValidationBySubscription(cardPaymentInfoModel.getSubscriptionId());
		if (subscriptionValidation != null)
		{
			cardPaymentInfoModel.setSubscriptionValidated(true);
			getModelService().save(cardPaymentInfoModel);
			getModelService().remove(subscriptionValidation);
		}
	}

	protected void saveSubscriptionForBillingAddress(final CustomerModel customerModel, final CustomerInfoData customerInfoData,
			final CreditCardPaymentInfoModel cardPaymentInfoModel, final PaymentInfoData paymentInfoData,
			final boolean saveInAccount, final AddressModel billingAddress)
	{
		validateParameterNotNull(customerInfoData, "customerInfoData cannot be null");
		validateParameterNotNull(cardPaymentInfoModel, "cardPaymentInfoModel cannot be null");
		validateParameterNotNull(paymentInfoData, "paymentInfoData cannot be null");

		if (CustomerType.GUEST.equals(customerModel.getType()))
		{
			final StringBuilder name = new StringBuilder();
			if (!StringUtils.isBlank(customerInfoData.getBillToFirstName()))
			{
				name.append(customerInfoData.getBillToFirstName());
				name.append(' ');
			}
			if (!StringUtils.isBlank(customerInfoData.getBillToLastName()))
			{
				name.append(customerInfoData.getBillToLastName());
			}
			customerModel.setName(name.toString());
			getModelService().save(customerModel);
		}

		getModelService().saveAll(cardPaymentInfoModel, billingAddress);
		getModelService().refresh(customerModel);

		final List<PaymentInfoModel> paymentInfoModels = new ArrayList<PaymentInfoModel>(customerModel.getPaymentInfos());
		if (!paymentInfoModels.contains(cardPaymentInfoModel))
		{
			paymentInfoModels.add(cardPaymentInfoModel);
			if (saveInAccount)
			{
				customerModel.setPaymentInfos(paymentInfoModels);
				getModelService().save(customerModel);
			}

			getModelService().save(cardPaymentInfoModel);
			getModelService().refresh(customerModel);
		}
	}

	private void updateCartWithSubscriptionTransactionEntry(final CustomerModel customerModel,
			final CreateSubscriptionResult response, final CartModel cart, final PaymentTransactionType transactionType)
	{
		Assert.notNull(transactionType, "Transaction Type cannot be null");
		PaymentTransactionEntryModel transactionEntry =
				getWileyPaymentTransactionStrategy().savePaymentTransactionEntry(customerModel,
						cart.getCurrency(), response, transactionType);
		List<PaymentTransactionModel> paymentTransactions = new ArrayList<>(cart.getPaymentTransactions());
		paymentTransactions.add(transactionEntry.getPaymentTransaction());
		cart.setPaymentTransactions(paymentTransactions);
	}

	private WileyPaymentTransactionStrategy getWileyPaymentTransactionStrategy()
	{
		return (WileyPaymentTransactionStrategy) getPaymentTransactionStrategy();
	}

	public WPGResponseSecurityValidationStrategy getResponseSecurityValidationStrategy()
	{
		return responseSecurityValidationStrategy;
	}

	public void setResponseSecurityValidationStrategy(
			final WPGResponseSecurityValidationStrategy responseSecurityValidationStrategy)
	{
		this.responseSecurityValidationStrategy = responseSecurityValidationStrategy;
	}

	protected CustomerEmailResolutionService getCustomerEmailResolutionService()
	{
		return customerEmailResolutionService;
	}
}