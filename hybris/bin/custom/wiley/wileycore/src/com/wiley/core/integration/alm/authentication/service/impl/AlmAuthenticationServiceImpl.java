package com.wiley.core.integration.alm.authentication.service.impl;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.integration.alm.authentication.AlmAuthenticationGateway;
import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;
import com.wiley.core.integration.alm.authentication.service.AlmAuthenticationService;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class AlmAuthenticationServiceImpl implements AlmAuthenticationService
{
	@Autowired
	private AlmAuthenticationGateway almAuthenticationGateway;

	@Nonnull
	@Override
	public AlmAuthenticationResponseDto validateSessionToken(@Nonnull final String sessionToken)
	{
		try
		{
			return almAuthenticationGateway.validateSessionToken(sessionToken);
		}
		catch (ExternalSystemNotFoundException ex)
		{
			return createErrorDto(NOT_FOUND);
		}
		catch (ExternalSystemException ex)
		{
			return createErrorDto(INTERNAL_SERVER_ERROR);
		}
	}

	private AlmAuthenticationResponseDto createErrorDto(final HttpStatus httpStatus)
	{
		final AlmAuthenticationResponseDto dto = new AlmAuthenticationResponseDto();
		dto.setHttpStatus(httpStatus);
		return dto;
	}
}
