package com.wiley.core.payment.populators;

import com.wiley.core.media.WileyMediaService;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSContentSlotService;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.core.wileyb2c.basesite.theme.Wileyb2cThemeNameResolutionStrategy;

import static com.wiley.core.payment.WileyHttpRequestParams.WPG_ADDRESS;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_ALLOW_AVS_FAIL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_COUNTRY_CODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_ADOBE_ANALYTICS_SCRIPT_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_BACK_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_CURRENT_YEAR;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_CUSTOMER_FIRST_NAME;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_GRAPHICSTANDARDS_LINK;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_INITIATOR;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_LOGO_IMAGE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_LOGO_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_OPERATION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_RESPONSIVE_LOGO_DATA;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_SITE_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_THEME;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_WILEY_LINK;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_DESCRIPTION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_METHOD;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_OPERATION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_POSTCODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_REGION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TIMESTAMP;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TRANSACTION_ID;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_VENDOR_ID;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public abstract class WPGHttpRequestPopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	private static final Logger LOG = Logger.getLogger(WPGHttpRequestPopulator.class);


	private static final String SITE_URL_CONFIG_GRAPHICSTANDARDS = "ags.graphicstandards.url.https";
	private static final String SITE_URL_CONFIG_WILEY = "wiley.com.url";
	private static final int MAX_POSTCODE_LENGTH = 8;
	private static final String SESSION_WPG_VALIDATION_INITIATOR_KEY = "wpg_validation_initiator";
	private static final String EMPTY_ENCODING_ATTRIBUTES = "";
	private static final String EMPTY_PATH = "";
	static final String SITE_LOGO_SLOT = "SiteLogoSlot";

	private String wpgOperation;
	private Map<String, String> wpgBackUrlBySite;
	private String wpgMethod;
	private String wpgDescription;
	private String wpgAllowAvsFail;
	private String adobeAnalyticsScriptUrlConfigKey;
	static final String DEFAULT_IMAGE_PATH_TEMPLATE = "/_ui/responsive/theme-%s/images/logo.png";
	static final String DEFAULT_IMAGE_HREF = "javascript:void(0);";

	@Resource
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	@Autowired
	private WileyTransactionIdGeneratorStrategy transactionIdGeneratorStrategy;
	@Autowired
	private WPGRegionStrategy wpgRegionStrategy;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private ConfigurationService configurationService;
	@Resource(name = "wpgHttpVendorIdStrategy")
	private WPGVendorIdStrategy httpVendorIdStrategy;
	@Resource
	private DefaultCMSSiteService cmsSiteService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private Wileyb2cThemeNameResolutionStrategy wileyb2cThemeNameResolutionStrategy;
	@Autowired
	private UserService userService;
	@Autowired
	private CMSContentSlotService cmsContentSlotService;
	@Resource
	private SiteConfigService siteConfigService;

	@Resource
	private WileyMediaService wileyMediaService;

	@Override
	public void populate(final CreateSubscriptionRequest source, final PaymentData target) throws ConversionException
	{
		validateParameterNotNull(source, "Parameter [CreateSubscriptionRequest] source cannot be null");
		validateParameterNotNull(target, "Parameter [PaymentData] target cannot be null");
		validateParameterNotNull(source.getCustomerBillToData(),
				"createSubscriptionRequest.getCustomerBillToData()  cannot be null");
		validateParameterNotNull(source.getOrderInfoData(), "createSubscriptionRequest.getOrderInfoData()  cannot be null");

		addRequestQueryParam(target, WPG_OPERATION, wpgOperation);

		final String wpgTimestamp = createWpgTimestamp();
		addRequestQueryParam(target, WPG_TIMESTAMP, wpgTimestamp);
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		final String vendorId = String.valueOf(httpVendorIdStrategy.getVendorId(currentSite.getUid()));
		addRequestQueryParam(target, WPG_VENDOR_ID, vendorId);

		final String transactionId = transactionIdGeneratorStrategy.generateTransactionId();
		addRequestQueryParam(target, WPG_TRANSACTION_ID, transactionId);
		addRequestQueryParam(target, WPG_METHOD, wpgMethod);
		addRequestQueryParam(target, WPG_DESCRIPTION, wpgDescription);

		final String currency = source.getOrderInfoData().getCurrency();
		final String region = wpgRegionStrategy.getRegionByCurrency(currency);
		addRequestQueryParam(target, WPG_REGION, region);


		final CustomerBillToData billingAddress = source.getCustomerBillToData();

		String address;
		String postcode;
		String countryCode;

		if (billingAddress != null)
		{
			address = billingAddress.getBillToStreet1();
			postcode = billingAddress.getBillToPostalCode();
			countryCode = billingAddress.getCountryNumericIsocode();
		}
		else
		{
			//TODO: this should be removed when wel checkout will have billing details
			final CustomerShipToData customerShipToData = source.getCustomerShipToData();
			address = customerShipToData.getShipToStreet1();
			postcode = customerShipToData.getShipToPostalCode();
			countryCode = customerShipToData.getCountryNumericIsocode();
		}

		addRequestQueryParam(target, WPG_ADDRESS, address);
		addRequestQueryParam(target, WPG_POSTCODE, StringUtils.substring(postcode, 0, MAX_POSTCODE_LENGTH));
		addRequestQueryParam(target, WPG_COUNTRY_CODE, countryCode);
		addRequestQueryParam(target, WPG_ALLOW_AVS_FAIL, wpgAllowAvsFail);

		populateSiteThemeAndLogo(target, currentSite);

		final String siteUrl = siteBaseUrlResolutionService.getWebsiteUrlForSite(currentSite, EMPTY_ENCODING_ATTRIBUTES, true,
				EMPTY_PATH);
		addRequestQueryParam(target, WPG_CUSTOM_SITE_URL, siteUrl);
		addRequestQueryParam(target, WPG_CUSTOM_BACK_URL, getWpgBackUrlBySite().get(currentSite.getUid()));
		addRequestQueryParam(target, WPG_CUSTOM_OPERATION, wpgOperation);

		addRequestQueryParam(target, WPG_CUSTOM_ADOBE_ANALYTICS_SCRIPT_URL, siteConfigService.getProperty(
				adobeAnalyticsScriptUrlConfigKey));
		final String initiator = sessionService.getAttribute(SESSION_WPG_VALIDATION_INITIATOR_KEY);
		addRequestQueryParam(target, WPG_CUSTOM_INITIATOR, initiator);

		final UserModel user = userService.getCurrentUser();
		if (user instanceof CustomerModel)
		{
			addRequestQueryParam(target, WPG_CUSTOM_CUSTOMER_FIRST_NAME, ((CustomerModel) user).getFirstName());
		}


		LocalDateTime timePoint = LocalDateTime.now();
		addRequestQueryParam(target, WPG_CUSTOM_CURRENT_YEAR, String.valueOf(timePoint.getYear()));
		addRequestQueryParam(target, WPG_CUSTOM_GRAPHICSTANDARDS_LINK, getGraphicStandardsSiteUrl());
		addRequestQueryParam(target, WPG_CUSTOM_WILEY_LINK, getWileySiteUrl());
		target.setPostUrl(source.getRequestUrl());
	}

	/**
	 * Populate WPG_CUSTOM_THEME, WPG_CUSTOM_LOGO_IMAGE, WPG_CUSTOM_RESPONSIVE_LOGO_DATA and WPG_CUSTOM_LOGO_URL properties
	 *
	 * @param target
	 * 		Target PaymentData
	 * @param currentSite
	 * 		current cms site
	 */
	public void populateSiteThemeAndLogo(final PaymentData target, final CMSSiteModel currentSite)
	{
		final String siteTheme = wileyb2cThemeNameResolutionStrategy.resolveThemeName(currentSite);
		addRequestQueryParam(target, WPG_CUSTOM_THEME, siteTheme);

		final MediaContainerModel logoImagesForSite = currentSite.getResponsiveLogoImages();
		if (logoImagesForSite != null)
		//if logos collection is explicitly configured for currentSite - use it (Legacy sites)
		{
			addRequestQueryParam(target, WPG_CUSTOM_RESPONSIVE_LOGO_DATA,
					convertMediaContainerToMediaData(currentSite, logoImagesForSite));
		}
		else
		{
			final MediaContainerModel logoImagesForSlot = getLogoImagesForSlot();
			if (logoImagesForSlot != null)
			//if logos collection is configured for SiteLogoSlot - use it (Wileycom sites)
			{
				addRequestQueryParam(target, WPG_CUSTOM_RESPONSIVE_LOGO_DATA,
						convertMediaContainerToMediaData(currentSite, logoImagesForSlot));
			}
			else
			//in other cases - use default logo image for current theme
			{
				String logoImageUrl = siteBaseUrlResolutionService.getWebsiteUrlForSite(currentSite, EMPTY_ENCODING_ATTRIBUTES,
						true, String.format(DEFAULT_IMAGE_PATH_TEMPLATE, siteTheme));
				addRequestQueryParam(target, WPG_CUSTOM_LOGO_IMAGE, logoImageUrl);
			}
		}

		String redirectURL = currentSite.getRedirectURL();
		addRequestQueryParam(target, WPG_CUSTOM_LOGO_URL, redirectURL != null ? redirectURL : DEFAULT_IMAGE_HREF);
	}

	private MediaContainerModel getLogoImagesForSlot()
	{
		Optional<ContentSlotModel> contentSlotForId = Optional.empty();
		try
		{
			contentSlotForId = Optional.of(cmsContentSlotService.getContentSlotForId(SITE_LOGO_SLOT));
		}
		catch (UnknownIdentifierException ex)
		{
			LOG.warn(SITE_LOGO_SLOT + " slot not found.", ex);
		}
		final Optional<AbstractCMSComponentModel> logoComponent = contentSlotForId.isPresent() ?
				contentSlotForId.get().getCmsComponents().stream().filter(x -> x instanceof SimpleResponsiveBannerComponentModel)
						.findFirst() : Optional.empty();
		final Optional<SimpleResponsiveBannerComponentModel> logo = logoComponent.isPresent() ?
				Optional.of((SimpleResponsiveBannerComponentModel) logoComponent.get()) : Optional.empty();
		return logo.isPresent() ? logo.get().getMedia() : null;
	}

	private String convertMediaContainerToMediaData(final CMSSiteModel currentSite,
			final MediaContainerModel mediaContainer)
	{
		return getWileyMediaService().convertMediaContainerToMediaData(currentSite, mediaContainer);
	}

	protected abstract String createSecurityHashCode(Map<String, String> parameters);

	public abstract SecurityHashGeneratorStrategy getSecurityHashGeneratorStrategy();

	protected String getSiteId()
	{
		final BaseSiteModel currentSite = baseSiteService.getCurrentBaseSite();
		return currentSite.getUid();
	}

	private String getGraphicStandardsSiteUrl()
	{
		final String siteUrl = configurationService.getConfiguration()
				.getString(SITE_URL_CONFIG_GRAPHICSTANDARDS, null);
		return siteUrl;
	}


	private String getWileySiteUrl()
	{
		final String siteUrl = configurationService.getConfiguration()
				.getString(SITE_URL_CONFIG_WILEY, null);
		return siteUrl;
	}

	protected String createWpgTimestamp()
	{
		return String.valueOf(System.currentTimeMillis());
	}

	public String getWpgOperation()
	{
		return wpgOperation;
	}

	public void setWpgOperation(final String wpgOperation)
	{
		this.wpgOperation = wpgOperation;
	}

	public String getWpgMethod()
	{
		return wpgMethod;
	}

	public void setWpgMethod(final String wpgMethod)
	{
		this.wpgMethod = wpgMethod;
	}

	public String getWpgDescription()
	{
		return wpgDescription;
	}

	public void setWpgDescription(final String wpgDescription)
	{
		this.wpgDescription = wpgDescription;
	}

	public String getWpgAllowAvsFail()
	{
		return wpgAllowAvsFail;
	}

	public void setWpgAllowAvsFail(final String wpgAllowAvsFail)
	{
		this.wpgAllowAvsFail = wpgAllowAvsFail;
	}

	public WPGRegionStrategy getWpgRegionStrategy()
	{
		return wpgRegionStrategy;
	}

	public void setWpgRegionStrategy(final WPGRegionStrategy wpgRegionStrategy)
	{
		this.wpgRegionStrategy = wpgRegionStrategy;
	}

	public Map<String, String> getWpgBackUrlBySite()
	{
		return wpgBackUrlBySite;
	}

	public void setWpgBackUrlBySite(final Map<String, String> wpgBackUrlBySite)
	{
		this.wpgBackUrlBySite = wpgBackUrlBySite;
	}

	public String getAdobeAnalyticsScriptUrlConfigKey()
	{
		return adobeAnalyticsScriptUrlConfigKey;
	}

	public void setAdobeAnalyticsScriptUrlConfigKey(final String adobeAnalyticsScriptUrlConfigKey)
	{
		this.adobeAnalyticsScriptUrlConfigKey = adobeAnalyticsScriptUrlConfigKey;
	}

	public WileyMediaService getWileyMediaService() {
		return wileyMediaService;
	}

	public void setWileyMediaService(final WileyMediaService wileyMediaService) {
		this.wileyMediaService = wileyMediaService;
	}
}
