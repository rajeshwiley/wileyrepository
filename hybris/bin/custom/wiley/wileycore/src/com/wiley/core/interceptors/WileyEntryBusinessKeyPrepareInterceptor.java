package com.wiley.core.interceptors;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;


/**
 * This interceptor nullify business key in case of cancelled entry model
 */
public class WileyEntryBusinessKeyPrepareInterceptor implements PrepareInterceptor<AbstractOrderEntryModel>
{
	@Override
	public void onPrepare(final AbstractOrderEntryModel abstractOrderEntryModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (abstractOrderEntryModel.getStatus() == OrderStatus.CANCELLED)
		{
			abstractOrderEntryModel.setBusinessKey(null);
		}
	}
}
