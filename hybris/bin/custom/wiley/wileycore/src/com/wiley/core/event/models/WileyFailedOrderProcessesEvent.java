package com.wiley.core.event.models;

import com.wiley.core.model.WileyOrderProcessModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.time.LocalDateTime;
import java.util.Collection;

public class WileyFailedOrderProcessesEvent extends AbstractEvent {

    private Collection<WileyOrderProcessModel> processes;

    private LocalDateTime dateFrom;

    private LocalDateTime dateTo;

    public WileyFailedOrderProcessesEvent(final Collection<WileyOrderProcessModel> processes,
                                          final LocalDateTime dateFrom,
                                          final LocalDateTime dateTo) {
        this.processes = processes;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public Collection<WileyOrderProcessModel> getProcesses() {
        return processes;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }
}
