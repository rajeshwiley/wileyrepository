package com.wiley.core.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.util.DiscountValue;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * Service is used to get product discounts. The service contains util methods to work with {@link DiscountValue}.
 */
public interface WileyProductDiscountService
{
	/**
	 * Returns discounts for current user and current currency.
	 *
	 * @param product
	 * 		product which is used to search student discounts assigned to it.
	 * @return returns list of discounts or empty list otherwise.
	 */
	@Nonnull
	List<DiscountValue> getPotentialDiscountForCurrentCustomer(@Nonnull ProductModel product, 
			@Nonnull UserDiscountGroup userDiscountGroup);
}
