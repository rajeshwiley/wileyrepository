/**
 *
 */
package com.wiley.core.subscription.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.core.GenericSearchField;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.genericsearch.GenericSearchService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.fest.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.core.search.flexiblesearch.CustomPagedFlexibleSearchService;
import com.wiley.core.subscription.dao.WileySubscriptionDao;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation of {@link WileySubscriptionDao}..
 */
public class WileySubscriptionDaoImpl extends DefaultGenericDao<WileySubscriptionModel> implements WileySubscriptionDao
{
	@Resource
	private CustomPagedFlexibleSearchService customPagedFlexibleSearchService;

	@Resource
	private GenericSearchService genericSearchService;

	private static final Logger LOG = LoggerFactory.getLogger(WileySubscriptionDaoImpl.class);

	private static final String QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BETWEEN_DATES;
	private static final String QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BEFORE_DATE;
	private static final String QUERY_FIND_SUBSCRIPTIONS;
	private static final String QUERY_FIND_SUBSCRIPTION_COUNT;
	private static final String QUERY_GET_SUBSCRIPTION_BILLING_ACTIVITIES;

	static
	{
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT {" + WileySubscriptionModel.PK + "} ")
				.append("FROM {" + WileySubscriptionModel._TYPECODE + "} ")
				.append("WHERE {" + WileySubscriptionModel.STATUS + "} = ?status ")
				.append("AND {" + WileySubscriptionModel.EXPIRATIONDATE + "} >= ?start ")
				.append("AND {" + WileySubscriptionModel.EXPIRATIONDATE + "} < ?end");
		QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BETWEEN_DATES = sbQuery.toString();

		sbQuery = new StringBuilder();
		sbQuery.append("SELECT {" + WileySubscriptionModel.PK + "} ")
				.append("FROM {" + WileySubscriptionModel._TYPECODE + "} ")
				.append("WHERE {" + WileySubscriptionModel.STATUS + "} = ?status ")
				.append("AND {" + WileySubscriptionModel.EXPIRATIONDATE + "} < ?moment ");
		QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BEFORE_DATE = sbQuery.toString();

		sbQuery = new StringBuilder();
		QUERY_FIND_SUBSCRIPTIONS = sbQuery.append("SELECT {ws.").append(WileySubscriptionModel.PK).append("} FROM {")
				.append(WileySubscriptionModel._TYPECODE).append(" as ws join ").append(ProductModel._TYPECODE)
				.append(" as p on {ws.").append(WileySubscriptionModel.PRODUCT).append("} = {p.").append(ProductModel.PK)
				.append("}} where {ws.").append(WileySubscriptionModel.CUSTOMER).append("} = ?customer")
				.append(" and {p.").append(ProductModel.CATALOGVERSION).append("} IN (?catalogVersion)")
				.append(" order by ").append("CASE {").append(WileySubscriptionModel.STATUS)
				.append("} WHEN ?firstStatus THEN 1 WHEN ?secondStatus THEN 2 WHEN ?thirdStatus THEN 3 ELSE 4 END, ").append(
						"{ws.")
				.append(WileySubscriptionModel.STARTDATE).append("} desc").toString();

		sbQuery = new StringBuilder();
		QUERY_FIND_SUBSCRIPTION_COUNT = sbQuery.append("SELECT COUNT(*) FROM {").append(WileySubscriptionModel._TYPECODE).append(
				"} where {").append(WileySubscriptionModel.CUSTOMER).append("} = ?").append(WileySubscriptionModel.CUSTOMER)
				.toString();

		sbQuery = new StringBuilder();
		sbQuery.append("SELECT {t.").append(WileySubscriptionPaymentTransactionModel.PK)
				.append("} FROM {WileySubscriptionPaymentTransaction AS t JOIN WileySubscription AS s").append(" ON {t.")
				.append(WileySubscriptionPaymentTransactionModel.SUBSCRIPTION)
				.append("}={s.")
				.append(UserModel.PK)
				.append("}}")
				.append(" WHERE {s:")
				.append(WileySubscriptionModel.CUSTOMER)
				.append("} = ?")
				.append(WileySubscriptionModel.CUSTOMER)
				.append(" AND {s:")
				.append(WileySubscriptionModel.CODE)
				.append("} = ?")
				.append(WileySubscriptionModel.CODE)
				.append(" ORDER BY {t.")
				.append(WileySubscriptionPaymentTransactionModel.TIME)
				.append("} DESC");
		QUERY_GET_SUBSCRIPTION_BILLING_ACTIVITIES = sbQuery.toString();
	}


	public WileySubscriptionDaoImpl(final String typecode)
	{
		super(typecode);
	}

	@Override
	public SearchPageData<WileySubscriptionModel> getSubscriptions(final UserModel currentUser,
			final Collection<CatalogVersionModel> catalogVersions, final PageableData pageableData)
	{
		validateParameterNotNullStandardMessage("currentUser", currentUser);
		validateParameterNotNullStandardMessage("catalogVersions", catalogVersions);

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("customer", currentUser);
		queryParams.put("catalogVersion", catalogVersions);
		queryParams.put("firstStatus", SubscriptionStatus.ACTIVE);
		queryParams.put("secondStatus", SubscriptionStatus.CANCELLED);
		queryParams.put("thirdStatus", SubscriptionStatus.EXPIRED);

		final Map<String, Object> countQueryParams = new HashMap<>();
		countQueryParams.put(WileySubscriptionModel.CUSTOMER, currentUser);

		SearchPageData<WileySubscriptionModel> search =
				customPagedFlexibleSearchService.search(QUERY_FIND_SUBSCRIPTIONS, queryParams,
						QUERY_FIND_SUBSCRIPTION_COUNT, countQueryParams, pageableData);
		return search;
	}

	@Override
	public List<WileySubscriptionModel> getSubscription(final String code)
	{
		validateParameterNotNull(code, "WileySubscription code must not be null!");
		return find(Collections.singletonMap(WileySubscriptionModel.CODE, (Object) code));
	}

	@Override
	public List<WileySubscriptionModel> getSubscription(final UserModel customer,
			final SubscriptionStatus status)
	{
		validateParameterNotNull(customer, "Customer must not be null!");
		validateParameterNotNull(status, "Status must not be null!");

		final Map parameters = new HashMap();
		parameters.put(WileySubscriptionModel.CUSTOMER, customer);
		parameters.put(WileySubscriptionModel.STATUS, status);

		return find(parameters);

	}

	@Override
	public List<WileySubscriptionModel> findSubscriptionsWithExpirationDateBefore(@Nonnull final SubscriptionStatus status,
			@Nonnull final ZonedDateTime moment)
	{
		LOG.debug("Trying to find subscriptions with status [{}] and with expiration date before [{}] .", status, moment);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BEFORE_DATE);

		query.addQueryParameter("status", status);
		query.addQueryParameter("moment", Date.from(moment.toInstant()));

		return getFlexibleSearchService().<WileySubscriptionModel> search(query).getResult();
	}

	@Override
	@Nonnull
	public List<WileySubscriptionModel> findSubscriptionsWithExpirationDateBetween(@Nonnull final SubscriptionStatus status,
			@Nonnull final ZonedDateTime start, @Nonnull final ZonedDateTime end)
	{

		LOG.debug("Trying to find subscriptions with status [{}] and with expiration date between [{}] and [{}].", status,
				start, end);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_ACTIVE_SUBSCRIPTIONS_WITH_EXPIRATION_BETWEEN_DATES);

		query.addQueryParameter("status", status);
		query.addQueryParameter("start", Date.from(start.toInstant()));
		query.addQueryParameter("end", Date.from(end.toInstant()));

		return getFlexibleSearchService().<WileySubscriptionModel> search(query).getResult();
	}

	@Override
	public List<WileySubscriptionProcessModel> findRenewalProcessesForSubscription(@Nonnull final String subscriptionCode,
			final ProcessState... states)
	{
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put(WileySubscriptionModel.CODE, subscriptionCode);

		final StringBuilder qBuilder = new StringBuilder();
		qBuilder.append("SELECT {" + WileySubscriptionProcessModel.PK + "} ")
				.append("FROM {" + WileySubscriptionProcessModel._TYPECODE + " AS proc JOIN " + WileySubscriptionModel._TYPECODE
						+ " AS sub ")
				.append("ON {sub." + WileySubscriptionModel.PK + "}={proc." + WileySubscriptionProcessModel.WILEYSUBSCRIPTION)
				.append("}}")
				.append("WHERE {sub." + WileySubscriptionModel.CODE + "}=?" + WileySubscriptionModel.CODE);

		if (!Arrays.isEmpty(states))
		{
			qBuilder.append(" AND (");
			int cnt = 0;
			for (final ProcessState state : states)
			{
				if (cnt > 0)
				{
					qBuilder.append(" OR ");
				}
				final String parameterName = WileySubscriptionProcessModel.STATE + (cnt++);
				qBuilder.append("{" + WileySubscriptionProcessModel.STATE + "}=?" + parameterName);
				parameters.put(parameterName, state);
			}
			qBuilder.append(")");
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(qBuilder.toString(), parameters);

		return getFlexibleSearchService().<WileySubscriptionProcessModel> search(query).getResult();
	}

	@Override
	public SearchPageData<WileySubscriptionPaymentTransactionModel> getSubscriptionBillingActivities(
			final String subscriptionCode, final UserModel currentUser, final PageableData pageableData)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(WileySubscriptionModel.CUSTOMER, currentUser);
		queryParams.put(WileySubscriptionModel.CODE, subscriptionCode);

		return customPagedFlexibleSearchService.<WileySubscriptionPaymentTransactionModel> search(
				QUERY_GET_SUBSCRIPTION_BILLING_ACTIVITIES, queryParams, pageableData);
	}

	@Override
	public List<WileySubscriptionModel> getInternalCodesByExternalCode(@Nonnull final String externalCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("externalCode", externalCode);
		final GenericQuery genericQuery = new GenericQuery(WileySubscriptionModel._TYPECODE);

		final GenericSearchField externalCodeField = new GenericSearchField(WileySubscriptionModel._TYPECODE,
				WileySubscriptionModel.EXTERNALCODE);
		final GenericCondition equalsExternalCode = GenericCondition.equals(externalCodeField, externalCode);
		genericQuery.addCondition(equalsExternalCode);

		return genericSearchService.<WileySubscriptionModel> search(genericQuery).getResult();
	}
}
