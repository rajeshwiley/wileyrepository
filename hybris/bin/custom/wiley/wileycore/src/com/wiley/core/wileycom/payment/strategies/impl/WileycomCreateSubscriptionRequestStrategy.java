package com.wiley.core.wileycom.payment.strategies.impl;

import com.wiley.core.payment.strategies.impl.WileyCreateSubscriptionRequestStrategy;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * B2C Strategy set payment info data from CartModel.paymentInfo
 */
public class WileycomCreateSubscriptionRequestStrategy extends WileyCreateSubscriptionRequestStrategy
{

	@Override
	public CreateSubscriptionRequest createSubscriptionRequest(final String siteName, final String requestUrl,
			final String responseUrl, final String merchantCallbackUrl, final CustomerModel customerModel,
			final CreditCardPaymentInfoModel cardInfo, final AddressModel paymentAddress) throws IllegalArgumentException
	{

		final CreateSubscriptionRequest request = super.createSubscriptionRequest(siteName, requestUrl,
				responseUrl,
				merchantCallbackUrl, customerModel, cardInfo, paymentAddress);
		final CartModel cart = getCartService().getSessionCart();

		if (cart.getPaymentInfo() != null && cart.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			request.setPaymentInfoData(getPaymentInfoDataConverter().convert((CreditCardPaymentInfoModel) cart.getPaymentInfo()));
			request.setCustomerBillToData(getCustomerBillToDataConverter().convert(cart.getPaymentInfo().getBillingAddress()));
		}

		request.getOrderInfoData().setDesiredShippingDate(cart.getDesiredShippingDate());
		request.getOrderInfoData().setTaxCalculated(cart.getTaxCalculated());
		return request;
	}

}
