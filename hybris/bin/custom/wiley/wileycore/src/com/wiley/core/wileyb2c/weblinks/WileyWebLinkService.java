package com.wiley.core.wileyb2c.weblinks;

import com.wiley.core.model.WileyWebLinkModel;


public interface WileyWebLinkService
{
	String getWebLinkUrl(WileyWebLinkModel webLink);
}
