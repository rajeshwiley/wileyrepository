package com.wiley.core.externaltax.xml.caching.impl;

import de.hybris.platform.regioncache.CacheValueLoadException;
import de.hybris.platform.regioncache.CacheValueLoader;
import de.hybris.platform.regioncache.key.CacheKey;

public class TaxServiceSimpleCacheValueLoader implements CacheValueLoader<Double>
{

	private final Double cachedValue;

	public TaxServiceSimpleCacheValueLoader(final Double cachedValue)
	{
		this.cachedValue = cachedValue;
	}

	@Override
	public Double load(final CacheKey cacheKey) throws CacheValueLoadException
	{
		if (cacheKey == null) {
			return null;
		}

		return cachedValue;
	}
}
