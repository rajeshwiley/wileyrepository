package com.wiley.core.product.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.product.WileyFreeTrialVariantProductService;
import com.wiley.core.product.dao.WileyFreeTrialVariantProductDao;

import static java.lang.String.format;


/**
 * Created by Raman_Hancharou on 3/30/2016.
 */
public class DefaultWileyFreeTrialVariantProductService implements WileyFreeTrialVariantProductService
{
	@Resource
	private WileyFreeTrialVariantProductDao wileyFreeTrialVariantProductDao;

	@Override
	@Nonnull
	public WileyFreeTrialVariantProductModel getFreeTrialVariantProductByCode(@Nonnull final String code)
	{
		final List<WileyFreeTrialVariantProductModel> freeTrialVariants =
				wileyFreeTrialVariantProductDao.findFreeTrialVariantProductsByCode(code);

		ServicesUtil.validateIfSingleResult(freeTrialVariants,
				format("%s with code '%s' not found!", WileyFreeTrialVariantProductModel._TYPECODE, code),
				format("%s code '%s' is not unique! Found %d items.", WileyFreeTrialVariantProductModel._TYPECODE, code,
						freeTrialVariants.size()));

		return freeTrialVariants.get(0);
	}
}
