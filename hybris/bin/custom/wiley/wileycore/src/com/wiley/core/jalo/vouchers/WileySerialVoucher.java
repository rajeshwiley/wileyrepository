package com.wiley.core.jalo.vouchers;

import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.voucher.jalo.SerialVoucher;
import de.hybris.platform.voucher.jalo.util.VoucherValue;

import org.springframework.beans.factory.annotation.Required;


public class WileySerialVoucher extends SerialVoucher
{
	private WileyVoucherApplicableValueStrategy applicableValueStrategy;

	@Override
	protected VoucherValue getApplicableValue(final AbstractOrder anOrder)
	{
		return applicableValueStrategy.getApplicableValue(anOrder, super.getApplicableValue(anOrder));
	}

	@Required
	public void setApplicableValueStrategy(final WileyVoucherApplicableValueStrategy applicableValueStrategy)
	{
		this.applicableValueStrategy = applicableValueStrategy;
	}
}
