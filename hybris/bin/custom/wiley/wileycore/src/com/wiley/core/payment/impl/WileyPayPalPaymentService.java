package com.wiley.core.payment.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Optional;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.data.DoCaptureRequestData;
import com.paypal.hybris.data.DoCaptureResultData;
import com.paypal.hybris.data.PaymentStatus;
import com.paypal.hybris.facade.impl.PayPalPaymentFacade;
import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.payment.transaction.impl.WileyPaymentTransactionService;


public class WileyPayPalPaymentService implements WileyPaymentService
{

	@Autowired
	private WileyPaymentTransactionService wileyPaymentTransactionService;
	@Autowired
	private PayPalPaymentFacade payPalPaymentFacade;

	@Override
	public Optional<PaymentTransactionEntryModel> capture(final PaymentTransactionModel txn)
	{
		final DoCaptureRequestData captureReqData = prepareCaptureRequestData(txn);
		final DoCaptureResultData captureResult = payPalPaymentFacade.doCapture(captureReqData);
		Optional<PaymentTransactionEntryModel> transactionEntryOptional;
		if (captureResult.getPaymentStatus() == PaymentStatus.COMPLETED) {
			final PaymentTransactionEntryModel transactionEntryModel =
				wileyPaymentTransactionService.createTransactionEntry(
						PaymentTransactionType.CAPTURE, TransactionStatus.ACCEPTED.name(),
						TransactionStatusDetails.SUCCESFULL.name(), captureResult.getTransactionId(), txn.getOrder(),
						txn.getOrder().getCurrency(), captureResult.getAmount(), captureResult.getDateTime().getTime(), txn);
			transactionEntryOptional = Optional.of(transactionEntryModel);
		}
		else {
			final String errorDetails = wileyPaymentTransactionService.createPayPalErrorDetails(captureResult.getErrors());
			final PaymentTransactionEntryModel errorEntry =
					wileyPaymentTransactionService.createTransactionEntry(
							PaymentTransactionType.CAPTURE, captureResult.getPaymentStatus().name(),
							errorDetails, captureResult.getTransactionId(), txn.getOrder(), txn.getOrder().getCurrency(),
							captureResult.getAmount(), captureResult.getDateTime().getTime(), txn);
			transactionEntryOptional = Optional.of(errorEntry);
		}
		return transactionEntryOptional;
	}

	private DoCaptureRequestData prepareCaptureRequestData(final PaymentTransactionModel txn)
	{
		final DoCaptureRequestData captureReqData = new DoCaptureRequestData();
		final double authorizedAmount =
				wileyPaymentTransactionService.calculateTotalAuthorized((OrderModel) txn.getOrder());
		captureReqData.setAmount(authorizedAmount);
		captureReqData.setAuthorizationId(txn.getRequestId());
		captureReqData.setCurrencyIsoCode(txn.getCurrency().getIsocode());
		captureReqData.setComplete(true);
		captureReqData.setInvoiceId(txn.getOrder().getCode());
		return captureReqData;
	}

	@Override
	public PaymentTransactionEntryModel authorize(final AbstractOrderModel order)
	{
		throw new NotImplementedException("This is not implemented yet here");
	}
}
