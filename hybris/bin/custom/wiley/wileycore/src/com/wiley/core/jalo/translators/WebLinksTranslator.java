package com.wiley.core.jalo.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.Collection;

import com.wiley.core.adapters.impl.WebLinksTwoDimensionListImportAdapter;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;


/**
 * Transator for import two dimension data (comma separated list where each item is pipe '|' separated list)
 * into {@link ProductModel#setWebLinks(Collection)}.
 *
 * For example, "{@link WileyWebLinkTypeEnum#WOL}|http://wol_url/,{@link WileyWebLinkTypeEnum#WOL_CP}|http://wol_cp_url/,"
 * translates to two {@link WileyWebLinkModel} with {@link WileyWebLinkModel#TYPE}
 * and url in {@link WileyWebLinkModel#PARAMETERS}.
 */
public class WebLinksTranslator extends AbstractWileySpecialValueTranslator
{

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		this.columnDescriptor = columnDescriptor;
		this.typeService = Registry.getApplicationContext().getBean(TypeService.class);
		this.twoDimensionListImportAdapter = Registry.getApplicationContext().getBean(
				WebLinksTwoDimensionListImportAdapter.class);
	}

}
