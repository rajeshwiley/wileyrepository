package com.wiley.core.integration.edi;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface WileyShippableExcelExportGateway
{
	void exportOrders(
			@Nullable List<WileyExportProcessModel> processes,
			@Nonnull BaseStoreModel baseStore,
			@Nonnull LocalDateTime localDateTime,
			@Nonnull String paymentType,
			@Nonnull String mailTo
	);
}
