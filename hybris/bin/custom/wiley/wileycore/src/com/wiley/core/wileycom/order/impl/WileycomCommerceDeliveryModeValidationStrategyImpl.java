package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceDeliveryModeValidationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.fest.util.Collections;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.order.WileycomDeliveryService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Strategy checks that passed supported delivery modes contain current delivery mode
 */
public class WileycomCommerceDeliveryModeValidationStrategyImpl extends DefaultCommerceDeliveryModeValidationStrategy
		implements WileycomCommerceDeliveryModeValidationStrategy
{
	@Resource
	private WileycomDeliveryService wileycomDeliveryService;

	@Override
	public void validateDeliveryMode(final CommerceCheckoutParameter parameter)
	{
		final CartModel cartModel = parameter.getCart();
		validateParameterNotNull(cartModel, "Cart model cannot be null");

		final ExternalDeliveryModeModel currentDeliveryMode = (ExternalDeliveryModeModel) cartModel.getDeliveryMode();
		if (currentDeliveryMode != null)
		{
			final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
			commerceCartParameter.setEnableHooks(true);
			commerceCartParameter.setCart(cartModel);

			List<ExternalDeliveryModeModel> supportedDeliveryModes = parameter.getSupportedDeliveryModes();

			// in case of change delivery address supportedDeliveryModes won't be set
			if (supportedDeliveryModes == null)
			{
				supportedDeliveryModes = wileycomDeliveryService.getSupportedExternalDeliveryModeListForOrder(cartModel);
			}
			invalidateCartDeliveryMode(cartModel, supportedDeliveryModes);

			// Calculate cart after removing delivery mode
			getCommerceCartCalculationStrategy().calculateCart(commerceCartParameter);

		}
	}

	@Override
	public void invalidateCartDeliveryMode(@Nonnull final AbstractOrderModel orderModel,
			@Nonnull final List<ExternalDeliveryModeModel> supportedDeliveryModes)
	{
		validateParameterNotNullStandardMessage("orderModel", orderModel);
		validateParameterNotNullStandardMessage("supportedDeliveryMode", supportedDeliveryModes);

		final ExternalDeliveryModeModel currentDeliveryMode = (ExternalDeliveryModeModel) orderModel.getDeliveryMode();
		if (Collections.isEmpty(supportedDeliveryModes)) {
			orderModel.setDeliveryMode(null);
		} else if (currentDeliveryMode == null) {
			orderModel.setDeliveryMode(getDefaultDeliveryMode(supportedDeliveryModes));
		} else {
			ExternalDeliveryModeModel newDeliveryMode = wileycomDeliveryService
					.findDeliveryModeByExternalCode(currentDeliveryMode.getExternalCode(), supportedDeliveryModes)
					.orElseGet(() -> getDefaultDeliveryMode(supportedDeliveryModes));
			orderModel.setDeliveryMode(newDeliveryMode);
		}

		getModelService().save(orderModel);
		getModelService().refresh(orderModel);
	}

	private ExternalDeliveryModeModel getDefaultDeliveryMode(final List<ExternalDeliveryModeModel> supportedDeliveryModes)
	{
		return wileycomDeliveryService.getDefaultDeliveryMode(
				supportedDeliveryModes);
	}
}
