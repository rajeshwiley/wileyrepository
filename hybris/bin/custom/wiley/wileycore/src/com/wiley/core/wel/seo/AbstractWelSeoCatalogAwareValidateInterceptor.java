package com.wiley.core.wel.seo;

import de.hybris.platform.servicelayer.event.EventService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.util.interceptor.AbstractCatalogAwareInterceptor;
import com.wiley.core.util.seo.SeoAfterSaveClusterAwareEvent;


public class AbstractWelSeoCatalogAwareValidateInterceptor
		extends AbstractCatalogAwareInterceptor
{
	protected static final String CATALOG_VERSION = "Online";

	private final EventService eventService;

	@Autowired
	public AbstractWelSeoCatalogAwareValidateInterceptor(final EventService eventService)
	{
		this.eventService = eventService;
	}


	protected void publishClusterAwareEvent()
	{
		SeoAfterSaveClusterAwareEvent seoAfterSaveEvent = new SeoAfterSaveClusterAwareEvent(StringUtils.EMPTY);
		eventService.publishEvent(seoAfterSaveEvent);
	}
}
