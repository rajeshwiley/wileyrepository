package com.wiley.core.mpgs.response;

import java.util.Date;


public class WileyVerifyResponse extends WileyResponse
{
	private String transactionId;
	private String currency;
	private Date timeOfRecord;

	public Date getTimeOfRecord()
	{
		return timeOfRecord;
	}

	public void setTimeOfRecord(final Date timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

}
