package com.wiley.core.wileyws.exceptions;

public class WileyWsValidationException extends RuntimeException
{
	private String path;
	private String type;

	public WileyWsValidationException(final String message, final String path, final String type)
	{
		super(message);
		this.path = path;
		this.type = type;
	}

	public String getPath()
	{
		return this.path;
	}

	public String getType()
	{
		return this.type;
	}
}