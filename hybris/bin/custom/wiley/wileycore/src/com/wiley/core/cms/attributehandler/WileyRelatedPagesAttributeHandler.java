package com.wiley.core.cms.attributehandler;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.pages.WileyCMSPageService;


/**
 * Created by Uladzimir_Barouski on 3/21/2017.
 */
public class WileyRelatedPagesAttributeHandler extends
		AbstractDynamicAttributeHandler<List<AbstractPageModel>, AbstractCMSComponentModel>
{
	@Resource
	private WileyCMSPageService wileyCMSPageService;

	@Override
	public List<AbstractPageModel> get(final AbstractCMSComponentModel component)
	{
		return wileyCMSPageService.findPagesByComponent(component)
				.stream()
				.distinct()
				.collect(Collectors.toList());
	}
}
