package com.wiley.core.b2b.unit.service.exception;

public class FindB2BUnitExternalSystemException extends RuntimeException
{
	public FindB2BUnitExternalSystemException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
