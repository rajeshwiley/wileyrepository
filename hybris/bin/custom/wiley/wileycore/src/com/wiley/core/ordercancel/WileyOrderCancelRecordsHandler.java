package com.wiley.core.ordercancel;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.exceptions.OrderCancelDaoException;
import de.hybris.platform.ordercancel.exceptions.OrderCancelRecordsHandlerException;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelRecordsHandler;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;

import java.time.Instant;
import java.util.Date;
import java.util.Map;


public class WileyOrderCancelRecordsHandler extends DefaultOrderCancelRecordsHandler
{
	/**
	 * OOTB replaced version id with timestamp
	 */
	@Override
	protected String generateEntryCode(final OrderHistoryEntryModel snapshot)
	{
		return snapshot.getOrder().getCode() + "_v" + Instant.now().toString() + "_t";
	}

	/**
	 * OOTB removed creation of history snapshot
	 */
	@Override
	public OrderCancelRecordEntryModel createRecordEntry(final OrderCancelRequest request, final PrincipalModel requestor)
			throws OrderCancelRecordsHandlerException
	{
		if (request == null)
		{
			throw new OrderCancelRecordsHandlerException(null, "Cancel request cannot be null");
		}
		else if (request.getOrder() == null)
		{
			throw new OrderCancelRecordsHandlerException(null, "Cancel request contains no order reference");
		}
		else
		{
			OrderModel order = request.getOrder();
			Map<Integer, AbstractOrderEntryModel> originalOrderEntriesMapping = storeOriginalOrderEntriesMapping(order);
			String description =
					(!request.isPartialCancel() ? "Full c" : "Partial c") + "ancel request for order: " + order.getCode();
			OrderHistoryEntryModel snapshot = createSnaphot(order, null, description, requestor);

			try
			{
				OrderCancelRecordModel cancelRecord = getOrCreateCancelRecord(order);
				if (cancelRecord.isInProgress())
				{
					throw new IllegalStateException("Cannot create new Order cancel request - the order cancel record indicates: "
							+ "Cancel already in progress");
				}
				else
				{
					cancelRecord.setInProgress(true);
					getModelService().save(cancelRecord);
					return createCancelRecordEntry(request, order, cancelRecord, snapshot, originalOrderEntriesMapping);
				}
			}
			catch (OrderCancelDaoException ex)
			{
				throw new OrderCancelRecordsHandlerException(order.getCode(), ex);
			}
		}
	}

	/**
	 * OOTB removed saving history snapshot
	 */
	@Override
	protected OrderHistoryEntryModel createSnaphot(final OrderModel order, final OrderModel version, final String description,
			final PrincipalModel requestor)
	{
		OrderHistoryEntryModel historyEntry = getModelService().create(
				OrderHistoryEntryModel.class);
		historyEntry.setOrder(order);
		historyEntry.setDescription(description);
		historyEntry.setTimestamp(new Date());
		if (requestor instanceof EmployeeModel)
		{
			historyEntry.setEmployee((EmployeeModel) requestor);
		}

		getModelService().save(historyEntry);
		return historyEntry;
	}
}
