package com.wiley.core.wileyas.mpgs.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;


public class WileyasMPGSPaymentServiceImpl extends WileyMPGSPaymentServiceImpl
{
	@Override
	public PaymentTransactionEntryModel authorize(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNull(abstractOrder, "[abstractOrder] can't be null.");

		PaymentTransactionEntryModel authorizationEntry;
		if (!wileyMPGSMerchantService.isMerchantValid(abstractOrder))
		{
			authorizationEntry = new PaymentTransactionEntryModel();
			authorizationEntry.setTransactionStatus(TransactionStatus.ERROR.name());
		}
		else
		{
			authorizationEntry = super.authorize(abstractOrder);
		}

		return authorizationEntry;
	}
}
