package com.wiley.core.servicelayer.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.servicelayer.dao.WileyItemDao;


public class WileyItemDaoImpl implements WileyItemDao
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Nonnull
	public Collection<ItemModel> findItems(@Nonnull final ComposedTypeModel type,
			@Nonnull final Collection<CatalogVersionModel> versions)
	{
		Preconditions.checkNotNull(type, "parameter type should not be null");
		Preconditions.checkNotNull(versions, "parameter versions should not be null");

		String searchQuery = "SELECT {pk} FROM {" + type.getCode() + "} WHERE {catalogversion} in (?versions)";
		SearchResult<ItemModel> items = flexibleSearchService.search(searchQuery, Collections.singletonMap("versions", versions));
		return items.getCount() > 0 ? items.getResult() : Collections.emptyList();
	}
}
