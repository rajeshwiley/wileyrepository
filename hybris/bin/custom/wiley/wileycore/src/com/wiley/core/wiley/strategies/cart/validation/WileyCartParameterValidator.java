package com.wiley.core.wiley.strategies.cart.validation;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;


public interface WileyCartParameterValidator
{
	void validateAddToCart(CommerceCartParameter parameter) throws CommerceCartModificationException;
}
