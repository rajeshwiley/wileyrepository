package com.wiley.core.integration.ebp.enums;

import java.util.HashMap;
import java.util.Map;


public enum WileyShipMethod
{
	INTL("international"),
	STND("standard"),
	NXTD("next-day"),
	EXPR("express"),
	DONT("dont");

	private final String value;
	private static final Map<String, WileyShipMethod> SHIP_METHOD_MAP = new HashMap<String, WileyShipMethod>();

	static
	{
		for (WileyShipMethod shipMethod : values())
		{
			SHIP_METHOD_MAP.put(shipMethod.getValue(), shipMethod);
		}
	}

	WileyShipMethod(final String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}

	public static WileyShipMethod getByValue(final String value)
	{
		return SHIP_METHOD_MAP.get(value);
	}

}
