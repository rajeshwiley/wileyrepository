package com.wiley.core.store.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.store.WileyBaseStoreService;


/**
 * Default implementation of {@link WileyBaseStoreService}.
 */
public class WileyBaseStoreServiceImpl implements WileyBaseStoreService
{

	@Resource
	private BaseSiteService baseSiteService;

	@Override
	@Nonnull
	public String getBaseStoreUidForSite(@Nonnull final String siteUid)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("siteUid", siteUid);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(siteUid);

		if (baseSite == null)
		{
			throw new UnknownIdentifierException(String.format("Could not find base site for uid [%s].", siteUid));
		}

		final Optional<BaseStoreModel> optionalBaseStore = getBaseStoreForBaseSite(baseSite);

		if (!optionalBaseStore.isPresent())
		{
			throw new IllegalStateException(String.format("Could not find base store for base site [%s].", siteUid));
		}

		final BaseStoreModel baseStoreModel = optionalBaseStore.get();
		return baseStoreModel.getUid();
	}

	@Override
	@Nonnull
	public Optional<BaseStoreModel> getBaseStoreForBaseSite(@Nonnull final BaseSiteModel baseSiteModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("baseSiteModel", baseSiteModel);

		Optional<BaseStoreModel> result = Optional.empty();
		final List<BaseStoreModel> stores = baseSiteModel.getStores();
		if (CollectionUtils.isNotEmpty(stores))
		{
			result = Optional.of(stores.get(0));
		}
		return result;
	}

	@Nonnull
	@Override
	public BaseStoreModel acquireBaseStoreByBaseSite(@Nonnull final BaseSiteModel baseSiteModel)
	{
		final Optional<BaseStoreModel> optionalBaseStore = getBaseStoreForBaseSite(baseSiteModel);

		if (!optionalBaseStore.isPresent())
		{
			throw new IllegalStateException(
					String.format("Could not find base store for base site [%s].", baseSiteModel.getUid()));
		}

		return optionalBaseStore.get();
	}
}
