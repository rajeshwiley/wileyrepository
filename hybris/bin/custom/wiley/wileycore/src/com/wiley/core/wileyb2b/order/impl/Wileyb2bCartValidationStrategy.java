package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartCleanStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;
import com.wiley.core.wiley.order.impl.WileyCartValidationStrategy;


/**
 * Wileyb2b specific CartValidationStrategy
 */
public class Wileyb2bCartValidationStrategy extends WileyCartValidationStrategy
{

	@Resource(name = "defaultB2BCartValidationStrategy")
	private CartCleanStrategy cartCleanStrategy;


	@Resource(name = "wileyb2bCommerceCartService")
	private CommerceCartService commerceCartService;

	@Resource
	private ProductService productService;

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Override
	@Deprecated
	@Nonnull
	public List<CommerceCartModification> validateCart(@Nonnull final CartModel cartModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartModel", cartModel);

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		return this.validateCart(parameter);
	}

	@Override
	@Nonnull
	public List<CommerceCartModification> validateCart(@Nonnull final CommerceCartParameter parameter)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);

		final CartModel cartModel = parameter.getCart();
		ServicesUtil.validateParameterNotNullStandardMessage("cartModel", cartModel);


		cartCleanStrategy.cleanCart(cartModel);
		try
		{
			commerceCartService.recalculateCart(parameter);
		}
		catch (CalculationException e)
		{
			// this exception should never be thrown. Only hybris API problem.
			throw new IllegalStateException(e.getMessage(), e);
		}

		final List<CommerceCartModification> modifications = new ArrayList<>(wileyFailedCartModificationsStorageService.popAll());
		final List<ExternalCartModification> externalCartModifications = externalCartModificationsStorageService.popAll(
				cartModel);
		modifications.addAll(getCommerceCartModifications(externalCartModifications));

		return modifications;
	}

	private List<CommerceCartModification> getCommerceCartModifications(
			final List<ExternalCartModification> externalCartModifications)
	{
		List<CommerceCartModification> resultList = Collections.emptyList();

		if (!externalCartModifications.isEmpty())
		{
			resultList = externalCartModifications.stream()
					.filter(this::shouldBeDisplayed)
					.map(external -> {
						final CommerceCartModification internal = new CommerceCartModification();
						internal.setQuantity(external.getQuantity());
						internal.setStatusCode(external.getStatusCode());
						internal.setMessage(external.getMessage());
						internal.setMessageType(external.getMessageType());

						final CartEntryModel cartEntryModel = new CartEntryModel();
						cartEntryModel.setEntryNumber(external.getEntryNumber());
						cartEntryModel.setProduct(productService.getProductForCode(external.getProductCode()));
						internal.setEntry(cartEntryModel);

						return internal;
					})
					.collect(Collectors.toList());
		}

		return resultList;
	}

	private boolean shouldBeDisplayed(final ExternalCartModification external)
	{
		return StringUtils.isNotEmpty(external.getMessage());
	}
}
