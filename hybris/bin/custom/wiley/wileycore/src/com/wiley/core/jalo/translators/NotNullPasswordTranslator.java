package com.wiley.core.jalo.translators;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.ConvertPlaintextToEncodedUserPasswordTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.user.User;


/**
 * Created by Maksim_Kozich on 21.01.2016.
 * Special translator to be used during live customer import to prevent existing user passwords to be changed.
 */
public class NotNullPasswordTranslator extends ConvertPlaintextToEncodedUserPasswordTranslator
{
	/**
	 * parameter name to be used in impex to provide required password encoding.
	 */
	public static final String ENCODING = "encoding";
	private String encoding;

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		performSuperInit(columnDescriptor);
		this.encoding = columnDescriptor.getDescriptorData().getModifier(ENCODING);
	}

	protected void performSuperInit(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		super.init(columnDescriptor);
	}

	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		if (processedItem instanceof User)
		{
			User user = (User) processedItem;
			String userPassword = user.getEncodedPassword();
			if (userPassword == null)
			{
				performSuperImport(this.encoding != null ? (this.encoding + ":" + cellValue) : cellValue, processedItem);
			}
		}
	}

	protected void performSuperImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		super.performImport(cellValue, processedItem);
	}

}
