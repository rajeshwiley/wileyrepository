package com.wiley.core.jalo.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.servicelayer.type.TypeService;

import com.wiley.core.adapters.impl.ExternalStoresTwoDimensionListImportAdapter;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.ExternalCompanyModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;


/**
 * Transator for import two dimension data (comma separated list where each item is pipe '|' separated list)
 * into {@link WileyPurchaseOptionProductModel#setExternalCompany(ExternalCompanyModel)}.
 *
 * For example,
 * "{@link WileyWebLinkTypeEnum#WOL WOL}|http://wol_url/,{@link WileyWebLinkTypeEnum#WOL_CP WOL_CP}|http://wol_cp_url/,"
 * translates to two {@link WileyWebLinkModel} with {@link WileyWebLinkModel#TYPE}
 * and url in {@link WileyWebLinkModel#PARAMETERS}.
 */
public class ExternalStoresTranslator extends AbstractWileySpecialValueTranslator
{

	private static final String PRODUCT_TYPE_CODE = "WileyPurchaseOptionProduct";
	
	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		this.columnDescriptor = columnDescriptor;
		this.typeService = Registry.getApplicationContext().getBean(TypeService.class);
		this.twoDimensionListImportAdapter = Registry.getApplicationContext().getBean(
				ExternalStoresTwoDimensionListImportAdapter.class);
	}
	
	@Override
	public String getProductTypeCode()
	{
		return PRODUCT_TYPE_CODE;
	}

}
