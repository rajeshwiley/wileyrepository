package com.wiley.core.mpgs.command;

import de.hybris.platform.payment.commands.Command;

import java.util.function.Supplier;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.response.WileyResponse;


public interface WileyCommand<Request, Result extends WileyResponse> extends Command<Request, Result>
{

	default Result createHybrisExceptionResponse(Supplier<Result> supplier)
	{
		Result result = supplier.get();
		result.setStatus(WileyMPGSConstants.HYBRIS_EXCEPTION);
		return result;
	}

	default Result createErrorResponse(Supplier<Result> supplier)
	{
		Result result = supplier.get();
		result.setStatus(WileyMPGSConstants.RETURN_STATUS_ERROR);
		return result;
	}

}
