package com.wiley.core.integration.article.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.USE_DEFAULTS;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(USE_DEFAULTS)
public class ArticleDto
{
	@JsonProperty(value = "articleName")
	private String articleName;

	@JsonProperty(value = "doi")
	private String doi;

	@JsonProperty(value = "journalId")
	private String journalId;

	@JsonProperty(value = "journalName")
	private String journalName;

	@JsonProperty(value = "pictureUrl")
	private String pictureUrl;

	public String getArticleName()
	{
		return articleName;
	}

	public void setArticleName(final String articleName)
	{
		this.articleName = articleName;
	}

	public String getDoi()
	{
		return doi;
	}

	public void setDoi(final String doi)
	{
		this.doi = doi;
	}

	public String getJournalId()
	{
		return journalId;
	}

	public void setJournalId(final String journalId)
	{
		this.journalId = journalId;
	}

	public String getJournalName()
	{
		return journalName;
	}

	public void setJournalName(final String journalName)
	{
		this.journalName = journalName;
	}

	public String getPictureUrl()
	{
		return pictureUrl;
	}

	public void setPictureUrl(final String pictureUrl)
	{
		this.pictureUrl = pictureUrl;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("articleName", articleName)
				.add("doi", doi)
				.add("journalId", journalId)
				.add("journalName", journalName)
				.add("pictureUrl", pictureUrl)
				.toString();
	}
}
