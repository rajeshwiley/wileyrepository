package com.wiley.storefront.web.payment.forms;


import java.io.Serializable;


public class WPGResponseForm implements Serializable
{
	private static final long serialVersionUID = 320633571924163589L;

	private String operation;
	private String returnCode;
	private String returnMessage;
	private String vendorID;
	private String transID;
	private String value;
	private String merchantResponse;
	private String merchantID;
	private String avsAddrResult;
	private String avsPostResult;
	private String token;
	private String authCode;
	private String acquirerID;
	private String acquirerName;
	private String bankID;
	private String bankName;
	private String maskedCardNumber;
	private String cardExpiry;
	private String timestamp;
	private String security;
	private String cscResult;
	private String currency;
	private String siteId;

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(final String operation)
	{
		this.operation = operation;
	}

	public String getReturnCode()
	{
		return returnCode;
	}

	public void setReturnCode(final String returnCode)
	{
		this.returnCode = returnCode;
	}

	public String getReturnMessage()
	{
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage)
	{
		this.returnMessage = returnMessage;
	}

	public String getVendorID()
	{
		return vendorID;
	}

	public void setVendorID(final String vendorID)
	{
		this.vendorID = vendorID;
	}

	public String getTransID()
	{
		return transID;
	}

	public void setTransID(final String transID)
	{
		this.transID = transID;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	public String getMerchantResponse()
	{
		return merchantResponse;
	}

	public void setMerchantResponse(final String merchantResponse)
	{
		this.merchantResponse = merchantResponse;
	}

	public String getMerchantID()
	{
		return merchantID;
	}

	public void setMerchantID(final String merchantID)
	{
		this.merchantID = merchantID;
	}

	public String getAvsAddrResult()
	{
		return avsAddrResult;
	}

	public void setAvsAddrResult(final String avsAddrResult)
	{
		this.avsAddrResult = avsAddrResult;
	}

	public String getAvsPostResult()
	{
		return avsPostResult;
	}

	public void setAvsPostResult(final String avsPostResult)
	{
		this.avsPostResult = avsPostResult;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getAuthCode()
	{
		return authCode;
	}

	public void setAuthCode(final String authCode)
	{
		this.authCode = authCode;
	}

	public String getAcquirerID()
	{
		return acquirerID;
	}

	public void setAcquirerID(final String acquirerID)
	{
		this.acquirerID = acquirerID;
	}

	public String getAcquirerName()
	{
		return acquirerName;
	}

	public void setAcquirerName(final String acquirerName)
	{
		this.acquirerName = acquirerName;
	}

	public String getBankID()
	{
		return bankID;
	}

	public void setBankID(final String bankID)
	{
		this.bankID = bankID;
	}

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(final String bankName)
	{
		this.bankName = bankName;
	}

	public String getMaskedCardNumber()
	{
		return maskedCardNumber;
	}

	public void setMaskedCardNumber(final String maskedCardNumber)
	{
		this.maskedCardNumber = maskedCardNumber;
	}

	public String getCardExpiry()
	{
		return cardExpiry;
	}

	public void setCardExpiry(final String cardExpiry)
	{
		this.cardExpiry = cardExpiry;
	}

	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(final String timestamp)
	{
		this.timestamp = timestamp;
	}

	public String getSecurity()
	{
		return security;
	}

	public void setSecurity(final String security)
	{
		this.security = security;
	}

	public String getCscResult()
	{
		return cscResult;
	}

	public void setCscResult(final String cscResult)
	{
		this.cscResult = cscResult;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getSiteId()
	{
		return siteId;
	}

	public void setSiteId(final String siteId)
	{
		this.siteId = siteId;
	}
}
