package com.wiley.storefront.web.payment.forms;


import java.io.Serializable;


public class WPGRequestForm implements Serializable
{
	private static final long serialVersionUID = 5585354248022849724L;

	private String wpgValue;
	private String wpgCurrency;
	private String wpgRegion;
	private String wpgBasketTax;
	private String wpgBasket2qty;
	private String wpgAddress;
	private String wpgBasket2price;
	private String wpgTransId;
	private String wpgVendorId;
	private String wpgDescription;
	private String wpgBasket1name;
	private String wpgBasketShipping;
	private String wpgPostcode;
	private String wpgAllowAvsFail;
	private String wpgBasket2Total;
	private String wpgBasket1Price;
	private String wpgOperation;
	private String wpgTimestamp;
	private String wpgBasketCount;
	private String wpgBasket1Total;
	private String wpgBasket2Name;
	private String wpgCountryCode;
	private String wpgMethod;
	private String wpgBasketTotal;
	private String wpgBasket1Qty;
	private String wpgSecurity;
	private String csrfToken;

	public String getWpgValue()
	{
		return wpgValue;
	}

	public void setWpgValue(final String wpgValue)
	{
		this.wpgValue = wpgValue;
	}

	public String getWpgCurrency()
	{
		return wpgCurrency;
	}

	public void setWpgCurrency(final String wpgCurrency)
	{
		this.wpgCurrency = wpgCurrency;
	}

	public String getWpgRegion()
	{
		return wpgRegion;
	}

	public void setWpgRegion(final String wpgRegion)
	{
		this.wpgRegion = wpgRegion;
	}

	public String getWpgBasketTax()
	{
		return wpgBasketTax;
	}

	public void setWpgBasketTax(final String wpgBasketTax)
	{
		this.wpgBasketTax = wpgBasketTax;
	}

	public String getWpgBasket2qty()
	{
		return wpgBasket2qty;
	}

	public void setWpgBasket2qty(final String wpgBasket2qty)
	{
		this.wpgBasket2qty = wpgBasket2qty;
	}

	public String getWpgAddress()
	{
		return wpgAddress;
	}

	public void setWpgAddress(final String wpgAddress)
	{
		this.wpgAddress = wpgAddress;
	}

	public String getWpgBasket2price()
	{
		return wpgBasket2price;
	}

	public void setWpgBasket2price(final String wpgBasket2price)
	{
		this.wpgBasket2price = wpgBasket2price;
	}

	public String getWpgTransId()
	{
		return wpgTransId;
	}

	public void setWpgTransId(final String wpgTransId)
	{
		this.wpgTransId = wpgTransId;
	}

	public String getWpgVendorId()
	{
		return wpgVendorId;
	}

	public void setWpgVendorId(final String wpgVendorId)
	{
		this.wpgVendorId = wpgVendorId;
	}

	public String getWpgDescription()
	{
		return wpgDescription;
	}

	public void setWpgDescription(final String wpgDescription)
	{
		this.wpgDescription = wpgDescription;
	}

	public String getWpgBasket1name()
	{
		return wpgBasket1name;
	}

	public void setWpgBasket1name(final String wpgBasket1name)
	{
		this.wpgBasket1name = wpgBasket1name;
	}

	public String getWpgBasketShipping()
	{
		return wpgBasketShipping;
	}

	public void setWpgBasketShipping(final String wpgBasketShipping)
	{
		this.wpgBasketShipping = wpgBasketShipping;
	}

	public String getWpgPostcode()
	{
		return wpgPostcode;
	}

	public void setWpgPostcode(final String wpgPostcode)
	{
		this.wpgPostcode = wpgPostcode;
	}

	public String getWpgAllowAvsFail()
	{
		return wpgAllowAvsFail;
	}

	public void setWpgAllowAvsFail(final String wpgAllowAvsFail)
	{
		this.wpgAllowAvsFail = wpgAllowAvsFail;
	}

	public String getWpgBasket2Total()
	{
		return wpgBasket2Total;
	}

	public void setWpgBasket2Total(final String wpgBasket2Total)
	{
		this.wpgBasket2Total = wpgBasket2Total;
	}

	public String getWpgBasket1Price()
	{
		return wpgBasket1Price;
	}

	public void setWpgBasket1Price(final String wpgBasket1Price)
	{
		this.wpgBasket1Price = wpgBasket1Price;
	}

	public String getWpgOperation()
	{
		return wpgOperation;
	}

	public void setWpgOperation(final String wpgOperation)
	{
		this.wpgOperation = wpgOperation;
	}

	public String getWpgTimestamp()
	{
		return wpgTimestamp;
	}

	public void setWpgTimestamp(final String wpgTimestamp)
	{
		this.wpgTimestamp = wpgTimestamp;
	}

	public String getWpgBasketCount()
	{
		return wpgBasketCount;
	}

	public void setWpgBasketCount(final String wpgBasketCount)
	{
		this.wpgBasketCount = wpgBasketCount;
	}

	public String getWpgBasket1Total()
	{
		return wpgBasket1Total;
	}

	public void setWpgBasket1Total(final String wpgBasket1Total)
	{
		this.wpgBasket1Total = wpgBasket1Total;
	}

	public String getWpgBasket2Name()
	{
		return wpgBasket2Name;
	}

	public void setWpgBasket2Name(final String wpgBasket2Name)
	{
		this.wpgBasket2Name = wpgBasket2Name;
	}

	public String getWpgCountryCode()
	{
		return wpgCountryCode;
	}

	public void setWpgCountryCode(final String wpgCountryCode)
	{
		this.wpgCountryCode = wpgCountryCode;
	}

	public String getWpgMethod()
	{
		return wpgMethod;
	}

	public void setWpgMethod(final String wpgMethod)
	{
		this.wpgMethod = wpgMethod;
	}

	public String getWpgBasketTotal()
	{
		return wpgBasketTotal;
	}

	public void setWpgBasketTotal(final String wpgBasketTotal)
	{
		this.wpgBasketTotal = wpgBasketTotal;
	}

	public String getWpgBasket1Qty()
	{
		return wpgBasket1Qty;
	}

	public void setWpgBasket1Qty(final String wpgBasket1Qty)
	{
		this.wpgBasket1Qty = wpgBasket1Qty;
	}

	public String getWpgSecurity()
	{
		return wpgSecurity;
	}

	public void setWpgSecurity(final String wpgSecurity)
	{
		this.wpgSecurity = wpgSecurity;
	}

	public String getCsrfToken()
	{
		return csrfToken;
	}

	public void setCsrfToken(final String csrfToken)
	{
		this.csrfToken = csrfToken;
	}
}
