package com.wiley.test.gateway.transformer;

import java.util.function.Function;

import org.springframework.messaging.Message;


/**
 * Transformer for testing purpose.
 */
public class TestTransformer
{

	private Function<Message<?>, Message<?>> transformationFunction;

	public Message<?> transform(final Message<?> message)
	{
		return transformationFunction.apply(message);
	}

	public Function<Message<?>, Message<?>> getTransformationFunction()
	{
		return transformationFunction;
	}

	public void setTransformationFunction(
			final Function<Message<?>, Message<?>> transformationFunction)
	{
		this.transformationFunction = transformationFunction;
	}
}
