package com.wiley.storefront.controllers;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class AuthorServiceStubNotificationController
{
	private static final Logger LOG = Logger.getLogger(AuthorServiceStubNotificationController.class);

	@CrossOrigin
	@RequestMapping(value = "/api/commonOrder/orderPlaced/{otderId}", method = RequestMethod.POST)
	public ResponseEntity orderPlacedNotification(@PathVariable final String otderId)
	{
		LOG.debug("Notification has been sent");
		return new ResponseEntity(HttpStatus.OK);
	}
}
