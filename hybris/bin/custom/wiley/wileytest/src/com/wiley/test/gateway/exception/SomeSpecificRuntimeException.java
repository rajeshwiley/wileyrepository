package com.wiley.test.gateway.exception;

/**
 * Exception for testing purpose.
 */
public class SomeSpecificRuntimeException extends RuntimeException
{
	public SomeSpecificRuntimeException(final String message)
	{
		super(message);
	}

	public SomeSpecificRuntimeException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public SomeSpecificRuntimeException()
	{
		super();
	}
}
