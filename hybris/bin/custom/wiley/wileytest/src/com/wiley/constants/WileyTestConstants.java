package com.wiley.constants;

@SuppressWarnings("PMD")
public final class WileyTestConstants extends GeneratedWileyTestConstants
{
	public static final String EXTENSIONNAME = "wileytest";
 	public static final String AS_SITE_ID = "asSite";
	
	private WileyTestConstants()
	{
		//empty
	}
}
