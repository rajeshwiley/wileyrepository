package com.wiley.integration;

/**
 * This Gateway is created to test caching with cache:advice in Spring Integration outbound gateway
 */
public interface WileyTestCachingGateway
{
	String send(String testMessage);
}
