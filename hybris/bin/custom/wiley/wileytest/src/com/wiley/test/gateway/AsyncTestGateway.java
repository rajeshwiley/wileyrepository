package com.wiley.test.gateway;

import java.util.concurrent.Future;


/**
 * Gateway for testing purpose.
 */
public interface AsyncTestGateway
{

	Future<Object> asyncMethod();

}
