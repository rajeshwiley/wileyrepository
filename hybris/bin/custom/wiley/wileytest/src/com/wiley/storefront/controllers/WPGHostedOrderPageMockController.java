package com.wiley.storefront.controllers;


import de.hybris.platform.commerceservices.util.AbstractComparator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.storefront.web.payment.forms.AddressForm;
import com.wiley.storefront.web.payment.forms.PaymentDetailsForm;
import com.wiley.storefront.web.payment.forms.WPGRequestForm;
import com.wiley.storefront.web.payment.forms.WPGResponseForm;


@Controller
@RequestMapping("/wpghop-mock")
public class WPGHostedOrderPageMockController
{
	protected static final String HOP_PAYMENT_FORM_PAGE = "hopMock/paymentForm";
	protected static final String HOP_REDIRECT_POST_PAGE = "hopMock/redirectPost";

	protected static final String RETURN_URL_AGS = "/agsstorefront/checkout/multi/hop/response";
	protected static final String SITE_ID_AGS = "ags";

	protected static final char SEPARATOR_CHAR = '\u00AC'; // Mathematical not sign
	protected static final String SEPARATOR_STR = Character.toString(SEPARATOR_CHAR);
	protected static final String OPERATION_AUTH = "auth";
	protected static final String OPERATION_VALIDATE = "validate";

	@Autowired
	private SecurityHashGeneratorStrategy wpgHttpSecurityHashGeneratorStrategy;

	private String vendorIdAgs;

	public SecurityHashGeneratorStrategy getWpgHttpSecurityHashGeneratorStrategy()
	{
		return wpgHttpSecurityHashGeneratorStrategy;
	}

	public void setWpgHttpSecurityHashGeneratorStrategy(final SecurityHashGeneratorStrategy wpgHttpSecurityHashGeneratorStrategy)
	{
		this.wpgHttpSecurityHashGeneratorStrategy = wpgHttpSecurityHashGeneratorStrategy;
	}

	public String getVendorIdAgs()
	{
		return vendorIdAgs;
	}

	public void setVendorIdAgs(final String vendorIdAgs)
	{
		this.vendorIdAgs = vendorIdAgs;
	}

	// find return url by vendor id
	private Map<String, String> mapVendorToUrl = new HashMap<>();
	// find siteId by vendorId
	private Map<String, String> mapVendorToSiteId = new HashMap<>();

	private void init()
	{
		mapVendorToUrl.put(getVendorIdAgs(), RETURN_URL_AGS);
		mapVendorToSiteId.put(getVendorIdAgs(), SITE_ID_AGS);
	}

	private Map<String, String> supportedCardTypes = new HashMap<String, String>();

	public WPGHostedOrderPageMockController() {
		supportedCardTypes.put("001", "Visa");
		supportedCardTypes.put("002", "MasterCard");
		supportedCardTypes.put("003", "American Express");
		supportedCardTypes.put("005", "Diners Club");
		supportedCardTypes.put("024", "Maestro (UK Domestic)");
	}
	private PaymentDetailsForm paymentDetailsForm;

	@ModelAttribute("cardTypes")
	public Map<String, String> getSupportedCardTypes()
	{
		return supportedCardTypes;
	}

	@ModelAttribute("currentLanguageIso")
	public String getCurrentLanguageIso()
	{
		final Locale currentLocale = Locale.getDefault();
		return currentLocale.getCountry().toLowerCase(Locale.getDefault());
	}

	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<>();

		months.add(new SelectOption("1", "01"));
		months.add(new SelectOption("2", "02"));
		months.add(new SelectOption("3", "03"));
		months.add(new SelectOption("4", "04"));
		months.add(new SelectOption("5", "05"));
		months.add(new SelectOption("6", "06"));
		months.add(new SelectOption("7", "07"));
		months.add(new SelectOption("8", "08"));
		months.add(new SelectOption("9", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	@ModelAttribute("billingCountries")
	public List<SelectOption> getBillingCountries()
	{
		final List<SelectOption> countries = new ArrayList<>();
		final Locale[] locales = Locale.getAvailableLocales();
		final Map<String, String> countryMap = new HashMap<>();

		for (final Locale locale : locales)
		{
			final String code = locale.getCountry();
			final String name = locale.getDisplayCountry();

			// putting in a map to remove duplicates
			if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(name))
			{
				countryMap.put(code, name);
			}
		}

		for (final Map.Entry<String, String> entry : countryMap.entrySet())
		{
			countries.add(new SelectOption(entry.getKey(), entry.getValue()));
		}

		Collections.sort(countries, CountryComparator.INSTANCE);

		return countries;
	}

	@ModelAttribute("mockErrorResponses")
	public List<SelectOption> getMockErrorResponses()
	{
		final List<SelectOption> errorResponses = new ArrayList<>();
		errorResponses.add(new SelectOption("150", "General system failure"));
		errorResponses.add(new SelectOption("151", "Server time-out"));
		errorResponses.add(new SelectOption("152", "Server time-out"));

		return errorResponses;
	}

	@ModelAttribute("wpgPaymentDetailsForm")
	public PaymentDetailsForm getPaymentDetailsForm()
	{
		return paymentDetailsForm;
	}

	public void setPaymentDetailsForm(final PaymentDetailsForm paymentDetailsForm)
	{
		this.paymentDetailsForm = paymentDetailsForm;
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
	public String getHopPaymentForm(final HttpServletRequest request, final Model model)
	{
		//Dummy Credit Card Information
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		paymentDetailsForm.setCardTypeCode("001");
		paymentDetailsForm.setCardNumber("4111111111111111");
		paymentDetailsForm.setVerificationNumber("123");
		paymentDetailsForm.setIssueNumber("01");
		paymentDetailsForm.setStartMonth("1");
		paymentDetailsForm.setStartYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - 1));
		paymentDetailsForm.setExpiryMonth("12");
		paymentDetailsForm.setExpiryYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR) + 1));

		// Override DefaultPaymentDetailsForm values with existing customer payment info data if available.
		if (StringUtils.isNotBlank(getParameter("card_accountNumber", request)))
		{
			paymentDetailsForm.setCardTypeCode(getParameter("card_cardType", request));

			//Force entry of card number as this is dependent on the card type.
			paymentDetailsForm.setCardNumber(StringUtils.EMPTY);

			final String verificationNumber = getParameter("card_cvNumber", request);
			if (StringUtils.isNotBlank(verificationNumber))
			{
				paymentDetailsForm.setVerificationNumber(verificationNumber);
			}

			final String issueNumber = getParameter("card_issueNumber", request);
			if (StringUtils.isNotBlank(issueNumber))
			{
				paymentDetailsForm.setIssueNumber(issueNumber);
			}

			final String startMonth = getParameter("card_startMonth", request);
			if (StringUtils.isNotBlank(startMonth))
			{
				paymentDetailsForm.setStartMonth(startMonth);
			}

			final String startYear = getParameter("card_startYear", request);
			if (StringUtils.isNotBlank(startYear))
			{
				paymentDetailsForm.setStartYear(startYear);
			}

			paymentDetailsForm.setExpiryMonth(getParameter("card_expirationMonth", request));
			paymentDetailsForm.setExpiryYear(getParameter("card_expirationYear", request));
		}

		// Store the original parameters sent by the caller
		paymentDetailsForm.setOriginalParameters(serializeRequestParameters(request));

		//Actual Customer Billing Address
		final AddressForm addressForm = new AddressForm();
		if (request != null)
		{
			addressForm.setTitleCode(getParameter("billTo_titleCode", request));
			addressForm.setFirstName(getParameter("billTo_firstName", request));
			addressForm.setLastName(getParameter("billTo_lastName", request));
			addressForm.setLine1(getParameter("billTo_street1", request));
			addressForm.setLine2(getParameter("billTo_street2", request));
			addressForm.setTownCity(getParameter("billTo_city", request));
			addressForm.setState(getParameter("billTo_state", request));
			addressForm.setPostcode(getParameter("billTo_postalCode", request));
			addressForm.setCountryIso(getParameter("billTo_country", request));
			addressForm.setPhoneNumber(getParameter("billTo_phoneNumber", request));
			addressForm.setEmailAddress(getParameter("billTo_email", request));
		}

		paymentDetailsForm.setBillingAddress(addressForm);

		if (null == getPaymentDetailsForm())
		{
			setPaymentDetailsForm(new PaymentDetailsForm());
		}
		BeanUtils.copyProperties(paymentDetailsForm, getPaymentDetailsForm());

		model.addAttribute("paymentDetailsForm", paymentDetailsForm);

		if (null != request)
		{
			final WPGResponseForm responseForm = new WPGResponseForm();
			final WPGRequestForm requestForm = new WPGRequestForm();
			fillInRequestForm(request, requestForm);

			String siteId = mapVendorToSiteId.get(requestForm.getWpgVendorId());
			if (null == siteId)
			{
				siteId = mapVendorToSiteId.get(vendorIdAgs);
			}
			responseForm.setSiteId(siteId);

			if (null != requestForm.getWpgOperation())
			{
				switch (requestForm.getWpgOperation())
				{
					case "WPG_validate":
						fillInHopMockResponse(requestForm, responseForm, OPERATION_VALIDATE);
						break;
					case "WPG_auth":
						fillInHopMockResponse(requestForm, responseForm, OPERATION_AUTH);
						break;
					default:
						break;
				}
			}

			request.getSession().setAttribute("WPGResponseForm", responseForm);
		}

		return HOP_PAYMENT_FORM_PAGE;
	}

	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public String doValidateAndPost(final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult,
			final HttpServletRequest request, final Model model)
	{
		if (bindingResult.hasErrors())
		{
			return HOP_PAYMENT_FORM_PAGE;
		}

		final WPGResponseForm responseForm = (WPGResponseForm) request.getSession().getAttribute("WPGResponseForm");
		String redirectUrl = mapVendorToUrl.get(responseForm.getVendorID());
		if (null == redirectUrl)
		{
			redirectUrl = RETURN_URL_AGS;
		}

		if (null == paymentDetailsForm.getCardNumber()) //NOSONAR
		{
			model.addAttribute("postUrl", redirectUrl);
			Map<String, String> params = new HashMap<>();
			fillInParamsForResponse(responseForm, params);
			model.addAttribute("postParams", params);
			return HOP_REDIRECT_POST_PAGE;
		}

		BeanUtils.copyProperties(getPaymentDetailsForm(), paymentDetailsForm);
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);

		final Map<String, String> params = deserializeParameterMap(paymentDetailsForm.getOriginalParameters());

		final AddressForm billingAddress = paymentDetailsForm.getBillingAddress();
		if (params != null)
		{
			final String reason = paymentDetailsForm.getMockReasonCode();
			processTransactionDecision(request, reason, params);

			//Card Details
			final String endPortion = paymentDetailsForm.getCardNumber().trim().substring(
					paymentDetailsForm.getCardNumber().length() - 4);
			final String maskedCardNumber = "************" + endPortion;

			params.put("card_accountNumber", maskedCardNumber);
			params.put("card_cardType", paymentDetailsForm.getCardTypeCode());
			params.put("card_expirationMonth", paymentDetailsForm.getExpiryMonth());
			params.put("card_expirationYear", paymentDetailsForm.getExpiryYear());
			params.put("card_startMonth", paymentDetailsForm.getStartMonth());
			params.put("card_startYear", paymentDetailsForm.getStartYear());

			final String subscriptionId = UUID.randomUUID().toString();
			params.put("paySubscriptionCreateReply_subscriptionID", subscriptionId);
			params.put("paySubscriptionCreateReply_subscriptionIDPublicSignature",
					getMockedPublicDigest());

			if (billingAddress != null)
			{
				params.put("billTo_titleCode", billingAddress.getTitleCode());
				params.put("billTo_firstName", billingAddress.getFirstName());
				params.put("billTo_lastName", billingAddress.getLastName());
				params.put("billTo_street1", billingAddress.getLine1());
				params.put("billTo_street2", billingAddress.getLine2());
				params.put("billTo_city", billingAddress.getTownCity());
				params.put("billTo_state", billingAddress.getState());
				params.put("billTo_postalCode", billingAddress.getPostcode());
				params.put("billTo_country", billingAddress.getCountryIso());
				params.put("billTo_phoneNumber", billingAddress.getPhoneNumber());
				params.put("billTo_email", billingAddress.getEmailAddress());
			}
			model.addAttribute("postUrl", params.get("orderPage_receiptResponseURL"));
			Boolean showDebugPage = paymentDetailsForm.getShowDebugPage();
			if (showDebugPage == null)
			{
				showDebugPage = Boolean.FALSE;
			}
			model.addAttribute("showDebugPage", Boolean.TRUE);
			model.addAttribute("postParams", params);

			return HOP_REDIRECT_POST_PAGE;
		}

		return HOP_PAYMENT_FORM_PAGE;
	}

	/**
	 * Set parameters for request form
	 *
	 * @param request
	 * 		- http request
	 * @param requestForm
	 * 		- form to show on the jsp
	 */
	private void fillInRequestForm(final HttpServletRequest request, final WPGRequestForm requestForm)
	{
		requestForm.setWpgValue(request.getParameter("WPG_value"));
		requestForm.setWpgCurrency(request.getParameter("WPG_currency"));
		requestForm.setWpgRegion(request.getParameter("WPG_region"));
		requestForm.setWpgAddress(request.getParameter("WPG_address"));
		requestForm.setWpgTransId(request.getParameter("WPG_transID"));
		requestForm.setWpgVendorId(request.getParameter("WPG_vendorID"));
		requestForm.setWpgDescription(request.getParameter("WPG_description"));
		requestForm.setWpgPostcode(request.getParameter("WPG_postcode"));
		requestForm.setWpgAllowAvsFail(request.getParameter("WPG_allowAVSfail"));
		requestForm.setWpgOperation(request.getParameter("WPG_operation"));
		requestForm.setWpgTimestamp(request.getParameter("WPG_timestamp"));
		requestForm.setWpgCountryCode(request.getParameter("WPG_countryCode"));
		requestForm.setWpgMethod(request.getParameter("WPG_method"));

		requestForm.setWpgBasketTax(request.getParameter("WPG_BASKET_tax"));
		requestForm.setWpgBasketShipping(request.getParameter("WPG_BASKET_shipping"));
		requestForm.setWpgBasketTotal(request.getParameter("WPG_BASKET_total"));
		requestForm.setWpgBasketCount(request.getParameter("WPG_BASKET_count"));
		requestForm.setWpgBasket1name(request.getParameter("WPG_BASKET_1_name"));
		requestForm.setWpgBasket1Qty(request.getParameter("WPG_BASKET_1_qty"));
		requestForm.setWpgBasket1Price(request.getParameter("WPG_BASKET_1_price"));
		requestForm.setWpgBasket1Total(request.getParameter("WPG_BASKET_1_total"));
		requestForm.setWpgBasket2Name(request.getParameter("WPG_BASKET_2_name"));
		requestForm.setWpgBasket2qty(request.getParameter("WPG_BASKET_2_qty"));
		requestForm.setWpgBasket2price(request.getParameter("WPG_BASKET_2_price"));
		requestForm.setWpgBasket2Total(request.getParameter("WPG_BASKET_2_total"));

		requestForm.setWpgSecurity(request.getParameter("WPG_security"));
		requestForm.setCsrfToken(request.getParameter("CSRFToken"));
	}

	private void fillInHopMockResponse(final WPGRequestForm requestForm,
			final WPGResponseForm responseForm,
			final String operation)
	{
		if (null == requestForm || null == responseForm || null == operation)
		{
			return;
		}

		if (OPERATION_AUTH.equals(operation))
		{
			responseForm.setValue(requestForm.getWpgValue());
			responseForm.setCurrency("USD");
			responseForm.setAuthCode("010461");
		}

		responseForm.setReturnCode("0");

		responseForm.setOperation(operation);

		responseForm.setReturnMessage("SUCCESS");
		responseForm.setVendorID(requestForm.getWpgVendorId());
		responseForm.setTransID(requestForm.getWpgTransId());
		responseForm.setMerchantResponse(requestForm.getWpgDescription());
		responseForm.setMerchantID("2292388873");
		responseForm.setCscResult("Y");
		responseForm.setAvsAddrResult("Y");
		responseForm.setAvsPostResult("N");
		responseForm.setToken("T344058642196266");
		responseForm.setAcquirerID("A");
		responseForm.setAcquirerName("AMEX");
		responseForm.setBankID("U");
		responseForm.setBankName("UNKNOWN");
		responseForm.setMaskedCardNumber("411111xxxxxx1111");
		final String expirationYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) + 3);
		responseForm.setCardExpiry("01" + expirationYear.substring(2));
		responseForm.setTimestamp(requestForm.getWpgTimestamp());
		responseForm.setSecurity(getHash(responseForm));
	}

	private void fillInParamsForResponse(final WPGResponseForm form, final Map<String, String> params)
	{
		params.put("operation", form.getOperation());
		params.put("returnCode", form.getReturnCode());
		params.put("returnMessage", form.getReturnMessage());
		params.put("vendorID", form.getVendorID());
		params.put("transID", form.getTransID());
		params.put("value", form.getValue());
		params.put("currency", form.getCurrency());
		params.put("merchantResponse", form.getMerchantResponse());
		params.put("merchantID", form.getMerchantID());
		params.put("CSCResult", "Y");
		params.put("AVSAddrResult", form.getAvsAddrResult());
		params.put("AVSPostResult", form.getAvsPostResult());
		params.put("token", form.getToken());
		params.put("authCode", form.getAuthCode());
		params.put("acquirerID", form.getAcquirerID());
		params.put("acquirerName", form.getAcquirerName());
		params.put("bankID", form.getBankID());
		params.put("bankName", form.getBankName());
		params.put("maskedCardNumber", form.getMaskedCardNumber());
		params.put("cardExpiry", form.getCardExpiry());
		params.put("timestamp", form.getTimestamp());
		params.put("security", getHash(form));
	}

	/**
	 * Calculate md5 hash based on parameters
	 *
	 * @param form
	 * 		- response parameters
	 * @return md5 hash
	 */
	private String getHash(final WPGResponseForm form)
	{
		StringBuilder md5Base = new StringBuilder();

		md5Base.append(form.getOperation());
		md5Base.append(form.getReturnCode());
		md5Base.append(form.getReturnMessage());
		md5Base.append(form.getVendorID());
		md5Base.append(form.getTransID());

		if (null != form.getValue())
		{
			md5Base.append(form.getValue());
		}
		if (null != form.getCurrency())
		{
			md5Base.append(form.getCurrency());
		}

		md5Base.append(form.getMerchantResponse());
		md5Base.append(form.getMerchantID());
		md5Base.append(form.getCscResult());
		md5Base.append(form.getAvsAddrResult());
		md5Base.append(form.getAvsPostResult());
		md5Base.append(form.getToken());

		if (null != form.getAuthCode())
		{
			md5Base.append(form.getAuthCode());
		}

		md5Base.append(form.getAcquirerID());
		md5Base.append(form.getAcquirerName());
		md5Base.append(form.getBankID());
		md5Base.append(form.getBankName());
		md5Base.append(form.getMaskedCardNumber());
		md5Base.append(form.getCardExpiry());
		md5Base.append(form.getTimestamp());

		return getWpgHttpSecurityHashGeneratorStrategy().generateSecurityHash(form.getSiteId(), md5Base.toString());
	}

	protected void processTransactionDecision(final HttpServletRequest request, final String reasonCode,
			final Map<String, String> params)
	{
		if (params == null || request == null)
		{
			return;
		}

		String decision = "ACCEPT";
		if (request.getParameter("button.fail") != null)
		{
			decision = "ERROR";
		}

		String modReasonCode = reasonCode;
		if ("ACCEPT".equalsIgnoreCase(decision) && StringUtils.isBlank(reasonCode))
		{
			modReasonCode = "100";
		}
		else if (StringUtils.isBlank(reasonCode))
		{
			//General error
			modReasonCode = "150";
		}

		//Default decision to ACCEPT 100
		params.put("decision", decision);
		params.put("reasonCode", modReasonCode);
		params.put("decision_publicSignature", getMockedPublicDigest());
	}

	protected String getMockedPublicDigest()
	{
		return "BzW+Xn0ZgZHeQRcFB6ri";
	}

	protected String getParameter(final String parameterName, final HttpServletRequest request)
	{
		final String result = request.getParameter(parameterName);
		if ("null".equalsIgnoreCase(result))
		{
			return StringUtils.EMPTY;
		}
		return StringUtils.isNotBlank(result) ? result : StringUtils.EMPTY;
	}

	protected String serializeRequestParameters(final HttpServletRequest request)
	{
		final StringBuilder result = new StringBuilder();

		final Enumeration myEnum = request.getParameterNames();
		while (myEnum.hasMoreElements())
		{
			final String paramName = (String) myEnum.nextElement();
			result.append(paramName).append(SEPARATOR_STR).append(request.getParameter(paramName));
			if (myEnum.hasMoreElements())
			{
				result.append(SEPARATOR_STR);
			}
		}

		return result.toString();
	}

	protected Map<String, String> deserializeParameterMap(final String paramString)
	{
		final Map<String, String> results = new HashMap<>();
		if (StringUtils.isNotBlank(paramString))
		{
			final String[] params = paramString.split(SEPARATOR_STR);
			for (int i = 0; i < params.length; i++)
			{
				results.put(params[i], (i + 1 >= params.length ? "" : params[++i]));
			}
		}
		return results;
	}

	/**
	 * Data class used to hold a drop down select option value. Holds the code identifier as well as the display name.
	 */
	public static class SelectOption
	{
		private final String code;
		private final String name;

		public SelectOption(final String code, final String name)
		{
			this.code = code;
			this.name = name;
		}

		public String getCode()
		{
			return code;
		}

		public String getName()
		{
			return name;
		}
	}

	/**
	 * Comparator class used to sort countries.
	 */
	public static class CountryComparator extends AbstractComparator<SelectOption>
	{
		public static final CountryComparator INSTANCE = new CountryComparator();

		@Override
		protected int compareInstances(final SelectOption option1, final SelectOption option2)
		{
			int result = compareValues(option1.getName(), option2.getName(), false);
			if (EQUAL == result)
			{
				result = compareValues(option1.getCode(), option2.getCode(), false);
			}
			return result;
		}
	}

}
