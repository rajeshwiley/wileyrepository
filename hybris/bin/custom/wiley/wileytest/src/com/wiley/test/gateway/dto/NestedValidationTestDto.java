package com.wiley.test.gateway.dto;

import javax.validation.constraints.NotNull;


/**
 * DTO for testing purpose.
 */
public class NestedValidationTestDto
{

	@NotNull
	private String regNumber;

	public String getRegNumber()
	{
		return regNumber;
	}

	public void setRegNumber(final String regNumber)
	{
		this.regNumber = regNumber;
	}
}
