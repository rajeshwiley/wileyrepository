package com.wiley.test.gateway.dto;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * DTO for testing purpose.
 */
public class ValidationTestDto
{

	@NotNull
	private String name;

	@Min(18)
	@Max(65)
	private Integer age;

	@Valid
	private NestedValidationTestDto nestedValidationTestDto;

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public Integer getAge()
	{
		return age;
	}

	public void setAge(final Integer age)
	{
		this.age = age;
	}

	public NestedValidationTestDto getNestedValidationTestDto()
	{
		return nestedValidationTestDto;
	}

	public void setNestedValidationTestDto(final NestedValidationTestDto nestedValidationTestDto)
	{
		this.nestedValidationTestDto = nestedValidationTestDto;
	}
}
