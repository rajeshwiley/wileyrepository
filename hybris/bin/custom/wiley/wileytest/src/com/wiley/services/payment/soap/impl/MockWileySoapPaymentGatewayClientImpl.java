package com.wiley.services.payment.soap.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.integrations.wpg.client.WileySoapPaymentGatewayClient;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentRequest;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentResponse;


public class MockWileySoapPaymentGatewayClientImpl extends WileySoapPaymentGatewayClient
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySoapPaymentGatewayClient.class);
	private static final String USE_MOCK_KEY = "payment.wpg.soap.useMock";

	private String operation;

	@Autowired
	private ConfigurationService configurationService;

	@Override
	public TokenRefundPaymentResponse tokenRefund(final TokenRefundPaymentRequest request)
	{
		TokenRefundPaymentResponse response = null;

		if (shouldUseMock())
		{
			response = new TokenRefundPaymentResponse();
			response.setReturnCode(Integer.valueOf(WileyTransactionStatusEnum.SUCCESS.getCode()));
			response.setReturnMessage(WileyTransactionStatusEnum.SUCCESS.getDescription());
			response.setTransId(request.getTransId());
			response.setMerchantResponse("Mock transaction");
			response.setMerchantId("1234567");
			response.setValue(request.getValue());
			response.setAuthCode("01234");
			response.setAcquirerId("V");
			response.setAcquirerName("VISA");
			response.setBankId("U");
			response.setBankName("UNKNOWN");
			response.setMaskedCardNumber("************1111");
			response.setCardExpiry("0117");
			response.setOperation(operation);
			response.setVendorId(request.getVendorId());
			response.setTimestamp(String.valueOf(System.currentTimeMillis()));

			LOG.debug("Mock Soap Response ='{}'",
					ToStringBuilder.reflectionToString(response, ToStringStyle.MULTI_LINE_STYLE));
		}
		else
		{
			response = super.tokenRefund(request);
		}

		return response;
	}

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(final String operation)
	{
		this.operation = operation;
	}

	private boolean shouldUseMock()
	{
		return configurationService.getConfiguration().getBoolean(USE_MOCK_KEY, false);
	}
}
