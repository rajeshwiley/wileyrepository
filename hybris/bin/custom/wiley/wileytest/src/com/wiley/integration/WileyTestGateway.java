package com.wiley.integration;

/**
 * This Gateway is created to test WileyCircuitBreakerRequestHandlerAdvice.
 */
public interface WileyTestGateway
{
	String send(String testMessage);
}
