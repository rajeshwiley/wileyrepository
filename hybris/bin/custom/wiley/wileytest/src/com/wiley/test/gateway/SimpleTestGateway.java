package com.wiley.test.gateway;

/**
 * Gateway for testign purpose.
 */
public interface SimpleTestGateway
{

	void voidMethod();

	Object returnableMethod();

	Object returnableMethodWithParam(Object object);

}
