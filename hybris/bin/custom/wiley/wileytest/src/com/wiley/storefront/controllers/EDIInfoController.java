package com.wiley.storefront.controllers;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.strategy.EdiOrderProvisionStrategy;


@Controller
public class EDIInfoController
{

	@Autowired
	private EdiOrderProvisionStrategy orderProvisionStrategy;

	@RequestMapping("ediinfo")
	public String showEdiManagementPage(final Model model,
			@RequestParam final String store, @RequestParam final String paymentType) {

		BaseStoreModel storeModel = new BaseStoreModel();
		storeModel.setUid(store);
		List<WileyExportProcessModel> exportProcesses =
				orderProvisionStrategy.getExportProcessesReadyForExport(storeModel, paymentType);

		model.addAttribute("processes", exportProcesses);
		return "edi/info";
	}
}
