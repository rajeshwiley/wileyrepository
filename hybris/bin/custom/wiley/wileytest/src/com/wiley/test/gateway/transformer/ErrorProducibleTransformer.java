package com.wiley.test.gateway.transformer;

import com.wiley.test.gateway.exception.SomeSpecificRuntimeException;

import org.springframework.messaging.Message;


/**
 * Transformer for testing purpose. Throws exception.
 */
public class ErrorProducibleTransformer
{

	public Message<Object> throwRuntimeException(final Message<Object> message)
	{
		throw new SomeSpecificRuntimeException("Test runtime exception.");
	}

}
