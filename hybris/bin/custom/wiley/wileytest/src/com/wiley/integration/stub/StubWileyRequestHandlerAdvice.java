package com.wiley.integration.stub;

import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;


/**
 * Created by Georgii_Gavrysh on 7/7/2016.
 */
public class StubWileyRequestHandlerAdvice extends AbstractRequestHandlerAdvice
{
	@Override
	protected Object doInvoke(final ExecutionCallback executionCallback, final Object o, final Message<?> message)
			throws Exception
	{
		return executionCallback.execute();
	}
}
