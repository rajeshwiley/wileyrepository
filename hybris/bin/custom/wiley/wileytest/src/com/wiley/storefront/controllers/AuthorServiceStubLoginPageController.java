package com.wiley.storefront.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AuthorServiceStubLoginPageController
{
	@RequestMapping("/aslogin")
	public String showAuthorServiceStubLoginPage(final Model model)
	{
		return "authorService/login";
	}
}
