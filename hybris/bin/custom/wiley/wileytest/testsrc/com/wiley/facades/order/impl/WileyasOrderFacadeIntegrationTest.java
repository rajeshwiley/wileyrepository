package com.wiley.facades.order.impl;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.wileyas.order.impl.WileyasCalculationService;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;


@IntegrationTest
public class WileyasOrderFacadeIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String IMPEX_BASE_PATH = "/wileytest/import/order/WileyasOrderFacadeIntegrationTest";
	private static final String COMMON_DATA_PATH = IMPEX_BASE_PATH + "/common.impex";
	private static final String ORDER_ENTRY_DATA_PATH = IMPEX_BASE_PATH + "/cancel-order-entry.impex";
	private static final String ORDER_ENTRY_GUID_DATA_PATH = IMPEX_BASE_PATH + "/order-entry-guid.impex";
	private static final String ORDER_DATA_PATH = IMPEX_BASE_PATH + "/cancel-order.impex";
	private static final String CHANGE_PRICE_PATH = IMPEX_BASE_PATH + "/change-price.impex";
	private static final String ORDER_DATA_BP_PATH = IMPEX_BASE_PATH + "/cancel-order-active-bp.impex";
	private static final String ORDER_CANCELLED_ENTRIES_DATA_PATH = IMPEX_BASE_PATH + "/order-cancelled-entries.impex";
	private static final String CUSTOMER_ID = "alm_id1";
	private static final String AGENT_ID = "asagent1";
	private static final String AGENT_PASSWORD = "1234";
	private static final String BASE_SITE_ID = "asSite";
	private static final String BASE_STORE_ID = "asStore";
	private static final String ORDER_CODE = "asCancellableTestOrder";
	private static final String EDIT_ORDER_CODE = "asEditTestOrder";
	private static final String EDIT_ORDER_DATA_PATH = IMPEX_BASE_PATH + "/edit-order.impex";
	private static final String EDIT_ORDER_WRON_SOURCE_SYSTEM_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-wrong-source-system.impex";
	private static final String EDIT_ZERO_ORDER_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-zero-order.impex";
	private static final String EDIT_ORDER_NOT_EDITABLE_ENTRIES_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-not-editable-entry.impex";
	private static final String EDIT_ORDER_ACTIVE_BP_PATH = IMPEX_BASE_PATH + "/edit-order-active-bp.impex";
	private static final String EDIT_ORDER_FAILED_BP_PATH = IMPEX_BASE_PATH + "/edit-order-failed-bp.impex";
	private static final String ORDER_CODE1 = "test-order-code";
	private static final Integer ENTRY_NUMBER = 1;
	private static final Double ENTRY_TOTAL_PRICE_AFTER_CANCEL = 0.0;
	private static final Double ENTRY_SUBTOTAL_PRICE_AFTER_CANCEL = 0.0;
	private static final Long ENTRY_QUANTITY_AFTER_CANCEL = 0L;
	private static final Double ORDER_TOTAL_PRICE_AFTER_CANCEL_ENTRY = 13.0;
	private static final Double ORDER_SUBTOTAL_PRICE_WITHOUT_DISCOUNT_AFTER_CANCEL_ENTRY = 15.0;
	public static final String TEST_ORDER_ENTRY_GUID = "test-order-entry-guid";
	public static final String TEST_ORDER_GUID = "test-order-guid";

	@Resource
	private WileyasOrderFacade wileyasOrderFacade;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource(name = "wileyasOrderEditCalculationService")
	private WileyasCalculationService wileyasCalculationService;

	@Resource(name = "wileyasExternalTaxesService")
	private ExternalTaxesService externalTaxesService;

	@Resource
	private WileyAssistedServiceFacade wileyAssistedServiceFacade;

	@Before
	public void importData() throws Exception
	{
		baseSiteService.setCurrentBaseSite(BASE_SITE_ID, false);
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void testGetOrderGuidByOrderEntryGuid() throws Exception
	{
		importCsv(ORDER_ENTRY_GUID_DATA_PATH, DEFAULT_ENCODING);

		String orderGuid = wileyasOrderFacade.getOrderGuidByOrderEntryGuid(TEST_ORDER_ENTRY_GUID);

		assertEquals(TEST_ORDER_GUID, orderGuid);
	}
}
