package com.wiley.integration.vies;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import de.hybris.bootstrap.annotations.IntegrationTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Resource;
import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.vies.ViesCheckVatGateway;
import com.wiley.core.integration.vies.dto.CheckVatResponseDto;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


@IntegrationTest
public class ViesCheckVatGatewayIntegrationTest extends WileyAbstractWireMockTest {
    private static final String COUNTRY_CODE = "GB";
    private static final String COUNTRY_CODE_UA = "UA";
    private static final String VAT_NUMBER = "858540495";
    private static final String INVALID_VAT_NUMBER = "0058540495";
    private static final String VAT_NUMBER_SERVICE_UNAVAILABLE = "987654321";
    private static final String RESOURCE_RESPONSE_PREFIX = "/wileytest/integration/ViesCheckVatGatewayIntegrationTest/responses";
    private static final String RESOURCE_REQUEST_PREFIX = "/wileytest/integration/ViesCheckVatGatewayIntegrationTest/mappings";


    @Resource
    private ViesCheckVatGateway viesCheckVatGateway;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testCheckVatValidNumber() throws Exception {
        //given
        final StubMapping stubMapping = setUpStubMapping(
                RESOURCE_REQUEST_PREFIX + "/mapping_200_valid.json", //mapping GB, 858540495
                RESOURCE_RESPONSE_PREFIX + "/response_valid.xml"
        );
        wireMock.addStubMapping(stubMapping);

        //when
        CheckVatResponseDto checkVatResponseDto;
        checkVatResponseDto = viesCheckVatGateway.checkVat(COUNTRY_CODE, VAT_NUMBER);

        //then
        assertNotNull(checkVatResponseDto);
        assertTrue("Vat number should be valid!", checkVatResponseDto.getValid());
    }

    @Test
    public void testCheckVatInvalidNumber() throws Exception {
        //given
        final StubMapping stubMapping = setUpStubMapping(
                RESOURCE_REQUEST_PREFIX + "/mapping_200_invalidTaxNumber.json", //mapping GB, 0058540495
                RESOURCE_RESPONSE_PREFIX + "/response_invalidTaxNumber.xml"
        );
        wireMock.addStubMapping(stubMapping);

        //when
        final CheckVatResponseDto checkVatResponseDto = viesCheckVatGateway.checkVat(COUNTRY_CODE, INVALID_VAT_NUMBER);

        //then
        assertNotNull(checkVatResponseDto);
        assertFalse("Vat number should be invalid!", checkVatResponseDto.getValid());
    }

    @Test
    public void testViesServiceUnavailable() throws Exception, ExternalSystemInternalErrorException {
        //given
        final StubMapping stubMapping = setUpStubMapping(
                RESOURCE_REQUEST_PREFIX + "/mapping_200_maxNoOfConcurrentReqGlobal.json", //mapping UA, 987654321
                RESOURCE_RESPONSE_PREFIX + "/response_maximumNumberOfConcurrentRequestHasBeenReached.xml"
        );
        wireMock.addStubMapping(stubMapping);
        //when

        //then
        exception.expect(ExternalSystemInternalErrorException.class);
        viesCheckVatGateway.checkVat(COUNTRY_CODE_UA, VAT_NUMBER_SERVICE_UNAVAILABLE);
    }
}


