package com.wiley.integration.alm.authentication;

import de.hybris.bootstrap.annotations.IntegrationTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.alm.authentication.AlmAuthenticationGateway;
import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;

import static org.springframework.http.HttpStatus.OK;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@IntegrationTest
public class AlmAuthenticationGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(AlmAuthenticationGatewayIntegrationTest.class);
	private static final String RESOURCE_PREFIX = "/wileytest/integration/AlmAuthenticationGatewayIntegrationTest";
	private static final String CUSTOMER = "test_alm_id";
	private static final String AGENT = "test_asagent";
	private static final String NOT_VALID = "test_alm_not_valid";
	private static final String SERVER_ERROR = "test_alm_server_error";
	private static final String TIMEOUT = "test_alm_timeout";
	private static final String CODE_400 = "test_alm_400";
	private static final String CODE_401 = "test_alm_401";

	@Resource(name = "almAuthenticationGateway")
	private AlmAuthenticationGateway gateway;

	@Resource
	private RestTemplate authentificationApigeeMgOAuth2RestTemplate;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void verifyCustomerTokenTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_200_customer.json",
				RESOURCE_PREFIX + "/response/response_validate_session_token_200_customer.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		final AlmAuthenticationResponseDto dto = gateway.validateSessionToken(CUSTOMER);

		// then
		assertNotNull(dto);
		assertEquals(CUSTOMER, dto.getUserId());
		assertNull(dto.getImitateeId());
		assertEquals(OK, dto.getHttpStatus());
	}

	@Test
	public void verifyAgentTokenTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_200_agent.json",
				RESOURCE_PREFIX + "/response/response_validate_session_token_200_agent.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		final AlmAuthenticationResponseDto dto = gateway.validateSessionToken(AGENT);

		// then
		assertNotNull(dto);
		assertEquals(AGENT, dto.getUserId());
		assertEquals(CUSTOMER, dto.getImitateeId());
		assertEquals(OK, dto.getHttpStatus());
	}

	@Test(expected = ExternalSystemNotFoundException.class)
	public void verifyNotFoundTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_404.json",
				RESOURCE_PREFIX + "/response/response_validate_session_token_404.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.validateSessionToken(NOT_VALID);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyServerErrorTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_500.json",
				500
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.validateSessionToken(SERVER_ERROR);
	}

	@Test(expected = ExternalSystemAccessException.class)
	public void verifyTimeoutTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_timeout.json",
				200
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.validateSessionToken(TIMEOUT);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void verify400Test() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_400.json",
				400
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.validateSessionToken(CODE_400);
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void verify401Test() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_validate_session_token_401.json",
				401
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.validateSessionToken(CODE_401);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) authentificationApigeeMgOAuth2RestTemplate.getRequestFactory();
	}
}