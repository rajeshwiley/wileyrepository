package com.wiley.core.wileycom.users;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemClientErrorException;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.wileycom.users.service.impl.WileycomUsersServiceImpl;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertTrue;


@IntegrationTest
public class WileycomUsersUpdateUserIdIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileycomUsersUpdateUserIdIntegrationTest";

	private static final String USER_NAME = "tepatel@wiley.com";
	private static final String USER_PASSWORD = "12341234";
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");
	private static final List<String> DISABLED_SITES = asList("ags", "wel");

	@Resource(name = "wileycomUsersService")
	private WileycomUsersServiceImpl wileycomUsersService;

	@Resource
	private HttpComponentsClientHttpRequestFactory usersRequestFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
	}

	@Test
	public void successCase() throws Exception
	{
		// Given
		final String newUserUid = "newtepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_Success.json",
				204
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);

		// When
		final boolean isValid = wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);

		// Then
		// assert expected and actual response objects
		assertTrue(isValid);
	}


	@Test(expected = ExternalSystemException.class)
	public void verify500case() throws Exception
	{
		// Given
		final String newUserUid = "invalid500_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_500.json",
				500
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void verify401case() throws Exception
	{
		// Given
		final String newUserUid = "invalid401_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_401.json",
				401
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
		// Then expect exception
	}

	@Test(expected = ExternalSystemClientErrorException.class)
	public void verify403case() throws Exception
	{
		// Given
		final String newUserUid = "invalid403_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_403.json",
				403
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
		// Then expect exception
	}

	@Test(expected = ExternalSystemException.class)
	public void verify409case() throws Exception
	{
		// Given
		final String newUserUid = "invalid409_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_409.json",
				409
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
		// Then expect exception
	}

	@Test(expected = ExternalSystemException.class)
	public void verify404case() throws Exception
	{
		// Given
		final String newUserUid = "invalid404_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_404.json",
				404
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
		// Then expect exception
	}

	@Test(expected = ExternalSystemException.class)
	public void verify400case() throws Exception
	{
		// Given
		final String newUserUid = "invalid400_tepatel@wiley.com";

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/updateUid_400.json",
				400
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel userModel = new UserModel();
		userModel.setUid(USER_NAME);
		// When
		wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
		// Then expect exception
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return usersRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("updatePassword", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final String newUserUid = "newtepatel@wiley.com";

				final StubMapping stubMapping = setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/updateUid_Success.json",
						204
				);
				wireMock.addStubMapping(stubMapping);

				final UserModel userModel = new UserModel();
				userModel.setUid(USER_NAME);

				wileycomUsersService.updateUserId(userModel, newUserUid, USER_PASSWORD);
				return null;
			}
		});
		return funcMap;
	}
}
