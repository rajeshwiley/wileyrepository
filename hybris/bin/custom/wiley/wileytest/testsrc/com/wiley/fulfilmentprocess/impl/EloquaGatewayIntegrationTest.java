package com.wiley.fulfilmentprocess.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;

import com.wiley.core.common.WileyAbstractStoreAwareChainTest;
import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.converters.populator.CustomerToContactPopulator;
import com.wiley.integrations.eloqua.converters.populator.SubscriptionStatusContactPopulator;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static java.util.Arrays.asList;


/**
 * Default integration test for {@link EloquaGateway}.
 */
@IntegrationTest
public class EloquaGatewayIntegrationTest extends WileyAbstractStoreAwareChainTest
{
	public static final int SUBSCRIPTION_STATUS_FIELD_ID = 100206;
	public static final String TEST_COUPON_CODE = "test_coupon_code";
	public static final int ORDER_CODE_FIELD_ID = 5263;
	public static final int PROMOTION_CODE_FIELD_ID = 5268;

	@Resource
	private EloquaGateway eloquaGateway;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CouponService couponService;

	private static final String WEL_SITE_UID = "wel";
	private static final String TEST_HYBRIS_ORDER_CODE = "test_hybris_order";
	private static final String TEST_HYBRIS_ORDER_CODE_2 = "test_hybris_order_2";

	private static final String SEARCH_CONTACT_URI = "/api/rest/1.0/data/contacts";
	private static final String CREATE_CONTACT_URI = "/api/rest/1.0/data/contact";
	private static final String UPDATE_CONTACT_MATCHED_URI = "/api/rest/1.0/data/contact/[0-9]+";
	private static final String CREATE_TRANSACTIONAL_RECORD_MATCHED_URI = "/api/rest/1.0/data/customObject/[0-9]+";

	private static final Integer TEST_ELOQUA_ID = 1001;

	private static final List<String> ENABLED_SITES = asList("ags", "wel");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource(name = "eloquaHttpClient")
	private HttpClient httpClient;

	@Before
	public void setUp() throws Exception
	{
		final RequestConfig config = RequestConfig.custom().setStaleConnectionCheckEnabled(true).build();
		ReflectionTestUtils.setField(httpClient, "defaultConfig", config);
		importCsv(
				"/wileyfulfilmentprocess/test/integration/EloquaGatewayIntegrationTest/testCreateEloquaContact.impex",
				DEFAULT_ENCODING);
	}

	@Test
	public void testCheckPromotionCodeForRequest() throws Exception
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE, baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();

		couponService.redeemCoupon(TEST_COUPON_CODE, order);

		customer.setEloquaId(TEST_ELOQUA_ID);
		final OrderEntryModel orderEntry = (OrderEntryModel) order.getEntries().get(0);

		wireMock.stubFor(
			post(urlMatching(CREATE_TRANSACTIONAL_RECORD_MATCHED_URI))
				.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withRequestBody(
					matching(".*id\":" + ORDER_CODE_FIELD_ID + ",\"value\":\"" + TEST_HYBRIS_ORDER_CODE
							+ ".*\"id\":" + PROMOTION_CODE_FIELD_ID + ",\"value\":\"" + TEST_COUPON_CODE + "\"}.*"))
				.willReturn(aResponse()
					.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
					.withStatus(200).withBody(
						"{\"type\":\"CustomObjectData\",\"contactId\":2079,\"fieldValues\":[ "
							+ "{\"type\":\"FieldValue\",\"id\":" + ORDER_CODE_FIELD_ID + ","
							+ "\"value\":\"00126159\"}]}")));

		// When
		final CustomObjectDataDto dto = eloquaGateway.createEloquaTransactionalRecord(orderEntry);

		// Then
		assertNotNull(dto);
	}

	@Test
	public void testSearchForEloquaContactWhenContactIsNotPresent() throws Exception
	{
		wireMock
				.stubFor(
						get(urlPathEqualTo(SEARCH_CONTACT_URI))
								.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.willReturn(aResponse()
										.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
										.withStatus(200).withBody(createSearchResponseBodyForNoContact())));

		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
				baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();

		// When
		final Optional<ContactDto> optional = eloquaGateway.searchForEloquaContact(customer, order.getSite());
		final ContactDto contactDto = optional.orElse(null);

		// Then
		assertFalse("Should not return ContactDto.", optional.isPresent());
	}

	@Test
	public void testSearchForEloquaContactWhenContactIsPresent() throws Exception
	{
		wireMock
				.stubFor(
						get(urlPathEqualTo(SEARCH_CONTACT_URI))
								.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.willReturn(aResponse()
										.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
										.withStatus(200).withBody(createSearchResponseBodyWithContact())));

		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE_2,
				baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();

		// When
		final Optional<ContactDto> optional = eloquaGateway.searchForEloquaContact(customer, order.getSite());

		// Then
		assertTrue("Should return ContactDto.", optional.isPresent());
	}

	@Test
	public void testCreateEloquaContactWithSubscription() throws Exception
	{
		wireMock.stubFor(post(urlPathEqualTo(CREATE_CONTACT_URI))
				.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withRequestBody(
						matching(".*" + SUBSCRIPTION_STATUS_FIELD_ID + ".*"))
				.willReturn(
						aResponse().withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
								.withStatus(200).withBody(createResponseBodyWithSubscription())));

		final boolean subscribe = true;
		createAndCheckEloquaContact(subscribe);
	}

	@Test
	public void testCreateEloquaContactWithoutSubscription() throws Exception
	{
		wireMock
				.stubFor(
						post(urlPathEqualTo(CREATE_CONTACT_URI))
								.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.willReturn(aResponse()
										.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
										.withStatus(200).withBody(createResponseBodyWithoutSubscription())));

		final boolean subscribe = false;
		createAndCheckEloquaContact(subscribe);
	}

	@Test
	public void testUpdateEloquaContactWithSubscription() throws Exception
	{
		wireMock.stubFor(put(urlMatching(UPDATE_CONTACT_MATCHED_URI))
				.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
				.withRequestBody(
						matching(".*" + SUBSCRIPTION_STATUS_FIELD_ID + ".*"))
				.willReturn(
						aResponse().withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
								.withStatus(200).withBody(createResponseBodyWithSubscription())));

		final boolean subscribe = true;
		updateAndCheckEloquaContact(subscribe);
	}

	@Test
	public void testUpdateEloquaContactWithoutSubscription() throws Exception
	{
		wireMock
				.stubFor(
						put(urlMatching(UPDATE_CONTACT_MATCHED_URI))
								.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
								.willReturn(aResponse()
										.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
										.withStatus(200).withBody(createResponseBodyWithoutSubscription())));

		final boolean subscribe = false;
		updateAndCheckEloquaContact(subscribe);
	}

	private void updateAndCheckEloquaContact(final boolean subscribe)
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
				baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();
		customer.setEloquaId(TEST_ELOQUA_ID);
		delay();
		// When
		final ContactDto contact = eloquaGateway.updateEloquaContact(customer,
				order.getPaymentAddress(), subscribe, order.getSite());

		// Then
		assertNotNull("Should return ContactDto.", contact);
		checkRequiredFieldsInContactDto(customer, contact);
		checkOptionalFieldsInContactDto(order, customer, contact, subscribe);
	}

	private void delay()
	{
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			Assert.fail("Cannot sleep and wait for WireMock");
		}
	}

	private void createAndCheckEloquaContact(final boolean subscribe) throws ImpExException
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
				baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();
		delay();
		// When
		final ContactDto contact = eloquaGateway.createEloquaContact(customer,
				order.getPaymentAddress(), subscribe, order.getSite());

		assertNotNull("Should return ContactDto.", contact);
		checkRequiredFieldsInContactDto(customer, contact);
		checkOptionalFieldsInContactDto(order, customer, contact, subscribe);
	}

	private void checkRequiredFieldsInContactDto(final CustomerModel customer,
			final ContactDto contact)
	{
		assertNotNull("Expected filled ContactDto.type.", contact.getType());
		assertNotNull("Expected filled ContactDto.id", contact.getId());
		assertEquals("Customer.uid should equal Contact.emailAddress.", customer.getUid(),
				contact.getEmailAddress());
	}

	private void checkOptionalFieldsInContactDto(final OrderModel order, final CustomerModel customer,
			final ContactDto contact, final Boolean subscribe)
	{
		assertEquals("Customer.firstName should equal Contact.firstName.", customer.getFirstName(),
				contact.getFirstName());
		assertEquals("Customer.lastName should equal Contact.lastName.", customer.getLastName(),
				contact.getLastName());
		final AddressModel paymentAddress = order.getPaymentAddress();
		assertEquals("Address.country.isocode should equal Contact.country.",
				paymentAddress.getCountry().getIsocode3(), contact.getCountry());
		assertEquals("Address.phone1 should equal Contact.businessPhone.", paymentAddress.getPhone1(),
				contact.getBusinessPhone());
		assertEquals("Address.line1 should equal Contact.address1.", paymentAddress.getLine1(),
				contact.getAddress1());
		assertEquals("Address.line2 should equal Contact.address2.", paymentAddress.getLine2(),
				contact.getAddress2());
		assertEquals("Address.town should equal Contact.city.", paymentAddress.getTown(),
				contact.getCity());
		assertEquals("Address.region.isocode should equal Contact.province.",
				paymentAddress.getRegion().getIsocode(), contact.getProvince());
		assertEquals("Address.postalCode should equal Contact.postalCode.",
				paymentAddress.getPostalcode(), contact.getPostalCode());
		final List<FieldValueDto> fieldValues = contact.getFieldValues();
		assertEquals("Contact.fieldValuses should contain expected 'Explicit Consent'.", subscribe,
				isSubscribed(fieldValues));
		assertTrue("Contact.fieldValuses should contain expected 'Hybris'.",
				isHybrisSource(fieldValues));
	}

	private boolean isSubscribed(final List<FieldValueDto> fieldValues)
	{
		return checkExistingInFieldValues(fieldValues,
				SUBSCRIPTION_STATUS_FIELD_ID,
				SubscriptionStatusContactPopulator.SUBSCRIPTION_STATUS_FIELD_EXPLICIT_CONSENT_VALUE);
	}

	private Boolean isHybrisSource(final List<FieldValueDto> fieldValues)
	{
		return checkExistingInFieldValues(fieldValues,
				CustomerToContactPopulator.LEAD_SOURCE_MOST_RECENT_FIELD_ID,
				CustomerToContactPopulator.LEAD_SOURCE_MOST_RECENT_FIELD_HYBRIS_VALUE);
	}

	private boolean checkExistingInFieldValues(final List<FieldValueDto> fieldValues,
			final int fieldId, final String expectedValue)
	{
		return fieldValues.stream()
				.anyMatch(fieldValueDto -> Integer.valueOf(fieldId).equals(fieldValueDto.getId())
						&& "FieldValue".equals(fieldValueDto.getType())
						&& expectedValue.equals(fieldValueDto.getValue()));
	}

	private String createSearchResponseBodyForNoContact() throws Exception
	{
		return Files.lines(Paths.get(getClass()
				.getResource(
						"/wileytest/integration/EloquaGatewayIntegrationTest/search_response_no_contact.json")
				.toURI())).collect(Collectors.joining());
	}

	private String createSearchResponseBodyWithContact() throws Exception
	{
		return Files.lines(Paths.get(getClass()
				.getResource(
						"/wileytest/integration/EloquaGatewayIntegrationTest/search_response_contact.json")
				.toURI())).collect(Collectors.joining());
	}

	private String createResponseBodyWithSubscription() throws Exception
	{
		return Files.lines(Paths.get(getClass()
				.getResource(
						"/wileytest/integration/EloquaGatewayIntegrationTest/response_subscription.json")
				.toURI())).collect(Collectors.joining());
	}

	private String createResponseBodyWithoutSubscription() throws Exception
	{
		return Files.lines(Paths.get(getClass()
				.getResource(
						"/wileytest/integration/EloquaGatewayIntegrationTest/response_no_subscription.json")
				.toURI())).collect(Collectors.joining());
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		Map<String, Callable<T>> funcMap = new HashMap<>();

		// Given
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(site);
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

		final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
				baseStore);
		final CustomerModel customer = (CustomerModel) order.getUser();

		funcMap.put("searchForEloquaContact", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				//verify search chain
				wireMock
						.stubFor(
								get(urlPathEqualTo(SEARCH_CONTACT_URI))
										.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.willReturn(aResponse()
												.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
												.withStatus(200).withBody(createSearchResponseBodyForNoContact())));
				eloquaGateway.searchForEloquaContact(customer, baseSite);
				return null;
			}
		});


		funcMap.put("createEloquaContact", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				//verify create chain
				wireMock
						.stubFor(
								post(urlPathEqualTo(CREATE_CONTACT_URI))
										.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.willReturn(aResponse()
												.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
												.withStatus(200).withBody(createResponseBodyWithoutSubscription())));
				eloquaGateway.createEloquaContact(customer,
						order.getPaymentAddress(), false, baseSite);
				return null;
			}
		});

		funcMap.put("updateEloquaContact", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				//verify update chain
				wireMock
						.stubFor(
								put(urlMatching(UPDATE_CONTACT_MATCHED_URI))
										.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.willReturn(aResponse()
												.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
												.withStatus(200).withBody(createResponseBodyWithoutSubscription())));
				customer.setEloquaId(TEST_ELOQUA_ID);
				delay();
				eloquaGateway.updateEloquaContact(customer,
						order.getPaymentAddress(), false, baseSite);
				return null;
			}
		});



		funcMap.put("createEloquaTransactionalRecord", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				//verify create transactional record chain
				wireMock
						.stubFor(
								post(urlMatching(CREATE_TRANSACTIONAL_RECORD_MATCHED_URI))
										.withHeader(HttpHeaders.CONTENT_TYPE, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.withHeader(HttpHeaders.ACCEPT, equalTo(MediaType.APPLICATION_JSON_VALUE))
										.withRequestBody(
												matching(".*"))
										.willReturn(aResponse()
												.withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
												.withStatus(200).withBody(
														"{\"type\":\"CustomObjectData\",\"contactId\":2079,\"fieldValues\":[ "
																+ "{\"type\":\"FieldValue\",\"id\":5263,"
																+ "\"value\":\"00126159\"}]}")));
				customer.setEloquaId(TEST_ELOQUA_ID);
				order.setSite(baseSite);
				eloquaGateway.createEloquaTransactionalRecord((OrderEntryModel) order.getEntries().get(0));
				return null;
			}
		});

		return funcMap;
	}
}
