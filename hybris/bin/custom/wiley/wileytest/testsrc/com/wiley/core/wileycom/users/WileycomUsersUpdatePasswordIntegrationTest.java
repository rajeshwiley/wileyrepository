package com.wiley.core.wileycom.users;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.integration.users.gateway.WileyUsersGateway;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.core.wileycom.users.service.WileycomUsersService;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertTrue;


@IntegrationTest
public class WileycomUsersUpdatePasswordIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileycomUsersUpdatePasswordIntegrationTest";

	private static final String USER_UID = "test@mail.com";

	private static final String PASSWORD_OLD = "oldPassword";
	private static final String PASSWORD_VALID = "test123";
	private static final String PASSWORD_ERROR = "passwordError500";
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");
	private static final List<String> DISABLED_SITES = asList("ags", "wel");

	@Resource
	private WileycomUsersService wileycomUsersService;

	@Resource
	private HttpComponentsClientHttpRequestFactory usersRequestFactory;

	private final UserModel userModel = new UserModel();

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileyUsersGateway wileyUsersGateway;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		userModel.setUid(USER_UID);
	}

	@Test
	public void shouldReturnTrueIfPasswordIsValid() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_Success.json",
				204
		);
		wireMock.addStubMapping(stubMapping);

		final boolean updated = wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, PASSWORD_VALID);
		assertTrue(updated);
	}


	@Test(expected = PasswordUpdateException.class)
	public void shouldThrowAnExceptionIfPasswordIsInValid() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_400.json",
				RESOURCE_PREFIX + "/response/response_passwordCheck_400.json"
		);
		wireMock.addStubMapping(stubMapping);
		wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, "passwordError400");
	}

	@Test(expected = PasswordUpdateException.class)
	public void shouldThrowAnExceptionIfUnauthorized() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_401.json",
				401
		);
		wireMock.addStubMapping(stubMapping);
		wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, "passwordError401");
	}

	@Test(expected = PasswordUpdateException.class)
	public void verify404case() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_404.json",
				404
		);
		wireMock.addStubMapping(stubMapping);
		wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, "passwordError404");
	}

	@Test(expected = PasswordUpdateException.class)
	public void shouldThrowAnExceptionIfForbidden() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_403.json",
				403
		);
		wireMock.addStubMapping(stubMapping);
		wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, "passwordError403");
	}

	@Test(expected = PasswordUpdateException.class)
	public void shouldThrowAnExceptionIfErrorOccurs() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/passwordCheck_500.json",
				RESOURCE_PREFIX + "/response/response_passwordCheck_500.json"
		);
		wireMock.addStubMapping(stubMapping);
		wileycomUsersService.updatePassword(userModel, PASSWORD_OLD, PASSWORD_ERROR);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return usersRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("updatePassword", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final StubMapping stubMapping = setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/passwordCheck_Success.json",
						204
				);
				wireMock.addStubMapping(stubMapping);
				wileyUsersGateway.updatePassword(userModel, PASSWORD_OLD, PASSWORD_VALID);
				return null;
			}
		});
		return funcMap;
	}
}
