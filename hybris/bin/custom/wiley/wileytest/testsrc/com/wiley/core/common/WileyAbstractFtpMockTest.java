package com.wiley.core.common;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.WatchService;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.nio.file.spi.FileSystemProvider;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang.SystemUtils;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.UserAuth;
import org.apache.sshd.server.auth.UserAuthNoneFactory;
import org.apache.sshd.server.auth.password.PasswordAuthenticator;
import org.apache.sshd.server.auth.password.PasswordChangeRequiredException;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.keyprovider.AbstractGeneratorHostKeyProvider;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.session.ServerSession;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import static com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder.newLinux;
import static com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder.newMacOs;
import static com.github.marschall.memoryfilesystem.MemoryFileSystemBuilder.newWindows;


/**
 * Created by Uladzimir_Barouski on 1/26/2017.
 */
@IntegrationTest
public class WileyAbstractFtpMockTest  extends AbstractWileyServicelayerTransactionalTest
{
	@Value("${sftp.key.passphrase}")
	private String sftpKeyPassword;

	private SshServer sftpServer;
	private SshServer coreSftpServer;

	protected java.nio.file.FileSystem sftpFileSystem;
	protected java.nio.file.FileSystem coreSftpFileSystem;

	@Before
	public void setUp() throws Exception
	{
		startCoreSftp();
		startSftp();
	}

	private void startCoreSftp() throws IOException
	{
		coreSftpServer = SshServer.setUpDefaultServer();
		coreSftpServer.setPort(Integer.valueOf(Config.getParameter("sftp.core.port")));

		byte[] serverKey = Config.getParameter("edi.core.sftp.public.key").getBytes();
		Path serverKeyFile = Paths.get("sftp.cer");
		Files.write(serverKeyFile, serverKey);
		AbstractGeneratorHostKeyProvider hostKeyProvider = new SimpleGeneratorHostKeyProvider(serverKeyFile);
		hostKeyProvider.setAlgorithm("RSA");
		coreSftpServer.setKeyPairProvider(hostKeyProvider);

		coreSftpServer.setPasswordAuthenticator(
				new DummyPasswordAuthenticator(Config.getParameter("ftp.username"), Config.getParameter("ftp.password")));

		List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<NamedFactory<UserAuth>>();
		userAuthFactories.add(new UserAuthNoneFactory());
		coreSftpServer.setUserAuthFactories(userAuthFactories);

		coreSftpServer.setCommandFactory(new ScpCommandFactory());

		List<NamedFactory<Command>> namedFactoryList = new ArrayList<NamedFactory<Command>>();
		namedFactoryList.add(new SftpSubsystemFactory());
		coreSftpServer.setSubsystemFactories(namedFactoryList);

		coreSftpFileSystem = createSftpFileSystem();
		coreSftpServer.setFileSystemFactory(session -> new DoNotClose(coreSftpFileSystem));
		try
		{
			coreSftpServer.start();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void startSftp() throws IOException
	{
		sftpServer = SshServer.setUpDefaultServer();
		sftpServer.setPort(Integer.valueOf(Config.getParameter("sftp.port")));

		File serverKey =  new File(Config.getParameter("HYBRIS_TEMP_DIR") + "/sftp/sftp_cer.ser");
		AbstractGeneratorHostKeyProvider hostKeyProvider =
				new SimpleGeneratorHostKeyProvider(serverKey);
		hostKeyProvider.setAlgorithm("RSA");
		sftpServer.setKeyPairProvider(hostKeyProvider);

		sftpServer.setPublickeyAuthenticator(new DummyPublickeyAuthenticator(Config.getParameter("ftp.username")));

		List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<NamedFactory<UserAuth>>();
		userAuthFactories.add(new UserAuthNoneFactory());
		sftpServer.setUserAuthFactories(userAuthFactories);

		sftpServer.setCommandFactory(new ScpCommandFactory());

		List<NamedFactory<Command>> namedFactoryList = new ArrayList<NamedFactory<Command>>();
		namedFactoryList.add(new SftpSubsystemFactory());
		sftpServer.setSubsystemFactories(namedFactoryList);

		sftpFileSystem = createSftpFileSystem();
		sftpServer.setFileSystemFactory(session -> new DoNotClose(sftpFileSystem));
		try
		{
			sftpServer.start();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private java.nio.file.FileSystem createSftpFileSystem() throws IOException {
		java.nio.file.FileSystem fileSystem = null;
		if (SystemUtils.IS_OS_LINUX)
		{
			fileSystem = newLinux().build("sftpServerRule@" + hashCode());
		} else if (SystemUtils.IS_OS_WINDOWS) {
			fileSystem = newWindows().build("sftpServerRule@" + hashCode());
		} else {
			fileSystem = newMacOs().build("sftpServerRule@" + hashCode());
		}
		return fileSystem;
	}

	@After
	public void after() throws Exception
	{
		sftpServer.stop();
		coreSftpServer.stop();
	}

	public class DummyPublickeyAuthenticator implements PublickeyAuthenticator
	{

		private String expectedUser;

		public DummyPublickeyAuthenticator(final String expectedUser)
		{
			this.expectedUser = expectedUser;
		}

		@Override
		public boolean authenticate(final String user, final PublicKey publicKey, final ServerSession serverSession) {
			return expectedUser.equals(user);
		}
	}

	public class DummyPasswordAuthenticator implements PasswordAuthenticator
	{

		private String expectedUser;
		private String expectedPassword;

		public DummyPasswordAuthenticator(final String expectedUser, final String expectedPassword)
		{
			this.expectedUser = expectedUser;
			this.expectedPassword = expectedPassword;
		}

		@Override
		public boolean authenticate(final String user, final String password, final ServerSession serverSession)
				throws PasswordChangeRequiredException
		{
			return Objects.equals(expectedUser, user) && Objects.equals(expectedPassword, password);
		}
	}


	private static class DoNotClose extends java.nio.file.FileSystem {
		final java.nio.file.FileSystem fileSystem;

		DoNotClose(final java.nio.file.FileSystem fileSystem) {
			this.fileSystem = fileSystem;
		}

		@Override
		public FileSystemProvider provider() {
			return fileSystem.provider();
		}

		@Override
		public void close() throws IOException {
			//will not be closed
		}

		@Override
		public boolean isOpen() {
			return fileSystem.isOpen();
		}

		@Override
		public boolean isReadOnly() {
			return fileSystem.isReadOnly();
		}

		@Override
		public String getSeparator() {
			return fileSystem.getSeparator();
		}

		@Override
		public Iterable<Path> getRootDirectories() {
			return fileSystem.getRootDirectories();
		}

		@Override
		public Iterable<FileStore> getFileStores() {
			return fileSystem.getFileStores();
		}

		@Override
		public Set<String> supportedFileAttributeViews() {
			return fileSystem.supportedFileAttributeViews();
		}

		@Override
		public Path getPath(final String first, final String... more) {
			return fileSystem.getPath(first, more);
		}

		@Override
		public PathMatcher getPathMatcher(final String syntaxAndPattern) {
			return fileSystem.getPathMatcher(syntaxAndPattern);
		}

		@Override
		public UserPrincipalLookupService getUserPrincipalLookupService() {
			return fileSystem.getUserPrincipalLookupService();
		}

		@Override
		public WatchService newWatchService() throws IOException {
			return fileSystem.newWatchService();
		}
	}
}
