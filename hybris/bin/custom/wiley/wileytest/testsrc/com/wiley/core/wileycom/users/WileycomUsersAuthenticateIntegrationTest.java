package com.wiley.core.wileycom.users;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.wileycom.users.service.WileycomUsersService;
import com.wiley.core.wileycom.users.service.exception.WileyUserBadRequestException;
import com.wiley.core.wileycom.users.service.exception.WileyUserUnreachableException;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;


/**
 * Created by Mikhail_Asadchy on 14.06.2016.
 */
@IntegrationTest
public class WileycomUsersAuthenticateIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final String TEST_FOLDER_PREFIX = "/wileytest/integration/WileycomUsersAuthenticateIntegrationTest/";
	private static final String MAPPINGS_PREFIX = TEST_FOLDER_PREFIX + "mappings/";
	private static final String RESPONSE_PREFIX = TEST_FOLDER_PREFIX + "response/";
	private static final String ANY_PASSWORD = "anyPassword";
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");
	private static final List<String> DISABLED_SITES = asList("ags", "wel");

	@Resource(name = "wileycomUsersService")
	private WileycomUsersService wileycomUsersService;

	@Resource
	private HttpComponentsClientHttpRequestFactory usersRequestFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
	}

	@Test
	public void successTest() throws Exception
	{
		// Given
		final String userName = "tepatel@wiley.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "/users_validUserPattern.json",
				RESPONSE_PREFIX + "/validUser_response.json");

		// When
		final boolean isValid = wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then
		// assert expected and actual response objects
		assertEquals(true, isValid);
	}

	@Test
	public void userWithWrongPasswordTest() throws Exception
	{
		// Given
		final String userName = "invalidUser@wiley.com";

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "/users_invalidUserPattern.json",
				RESPONSE_PREFIX + "/response_badCredentials.json");

		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// When
		final boolean isValid = wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then
		// assert expected and actual response objects
		assertFalse(isValid);
	}

	@Test(expected = WileyUserBadRequestException.class)
	public void malformedRequestTest() throws Exception
	{
		// Given
		final String userName = "malformedrequest@user.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "/users_malformedRequest.json",
				RESPONSE_PREFIX + "/response_malformedRequest.json");

		// When
		wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then expect exception
	}

	@Test(expected = WileyUserUnreachableException.class)
	public void notFoundTest() throws Exception
	{
		// Given
		final String userName = "notfound@user.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "/users_notfound.json",
				RESPONSE_PREFIX + "/response_notFound.json");

		// When
		wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then expect exception
	}

	@Test(expected = WileyUserUnreachableException.class)
	public void notAuthorizedRequestTest() throws Exception
	{
		final String userName = "unauthorized@user.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "users_unauthorized.json",
				RESPONSE_PREFIX + "response_notAuthorizedRequest.json");

		// When
		wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then
	}

	@Test(expected = WileyUserUnreachableException.class)
	public void forbiddenRequestTest() throws Exception
	{
		final String userName = "forbidden@user.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		setupWireMockPostErrorResponse("/users/authenticate", HttpStatus.SC_FORBIDDEN);

		// When
		wileycomUsersService.authenticate(userModel, ANY_PASSWORD);
	}

	@Test(expected = WileyUserUnreachableException.class)
	public void internalErrorRequestTest() throws Exception
	{
		// Given
		final String userName = "internalerror@user.com";
		final UserModel userModel = new UserModel();
		userModel.setUid(userName);

		// preparations
		addRequestAndResponse(
				MAPPINGS_PREFIX + "users_internalError.json",
				RESPONSE_PREFIX + "response_internalError.json");

		// When
		wileycomUsersService.authenticate(userModel, ANY_PASSWORD);

		// Then expect exception
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return usersRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("authenticate", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				// Given
				final String userName = "tepatel@wiley.com";
				final UserModel userModel = new UserModel();
				userModel.setUid(userName);

				// preparations
				addRequestAndResponse(
						MAPPINGS_PREFIX + "/users_validUserPattern.json",
						RESPONSE_PREFIX + "/validUser_response.json");


				wileycomUsersService.authenticate(userModel, ANY_PASSWORD);
				return null;
			}
		});
		return funcMap;
	}
}
