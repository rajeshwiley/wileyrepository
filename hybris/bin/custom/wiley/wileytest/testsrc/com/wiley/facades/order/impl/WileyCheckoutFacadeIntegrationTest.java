package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.common.BaseProcessIntegrationTest;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wiley.order.WileyCartFacade;


/**
 * Integration test for WileyCheckoutFacade
 */
@IntegrationTest
public class WileyCheckoutFacadeIntegrationTest extends BaseProcessIntegrationTest
{
	private static final String IMPEX_PATH = "/wileytest/import/order/WileyCheckoutFacadeIntegrationTest";

	private static final String COMMON_DATA_PATH = IMPEX_PATH + "/common.impex";
	private static final String ZERO_ORDER_DATA_PATH = IMPEX_PATH + "/zero-order.impex";
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String CART_GUID = "test-usd-cart1-guid";
	private static final String VOUCHER_100_PERCENT_DISCOUNT_CODE = "test_100_discount";
	private static final double DELTA = 0.0001;
	private static final double ENTRY_PRICE = 11.19d;
	private static final double ZERO_PRICE = 0.0d;

	private static final String CUSTOMER_ID = "alm_id1";
	private static final String BASE_SITE_ID = "asSite";

	@Resource(name = "wileyasCheckoutFacade")
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "wileyasCouponFacade")
	private WileyCouponFacade couponFacade;

	@Resource(name = "wileyasCartFacade")
	private WileyCartFacade wileyasCartFacade;

	@Resource
	private ModelService modelService;

	@Resource
	private WileyOrderService wileyOrderService;

	@Resource
	private CartService cartService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;

	@Before
	public void setUp() throws Exception
	{
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(BASE_SITE_ID, false);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void testPlaceZeroOrderWith100PercentDiscount() throws InvalidCartException, ImpExException, VoucherOperationException
	{
		importCsv(ZERO_ORDER_DATA_PATH, DEFAULT_ENCODING);
		wileyasCartFacade.switchSessionCart(CART_GUID);
		// apply 100 percent coupon code to trigger zero order flow
		couponFacade.applyVoucher(VOUCHER_100_PERCENT_DISCOUNT_CODE);

		final CartModel cart = cartService.getSessionCart();

		assertEquals(ENTRY_PRICE, cart.getSubtotal(), DELTA);
		assertEquals(ENTRY_PRICE, cart.getTotalDiscounts(), DELTA);
		assertEquals(ZERO_PRICE, cart.getTotalPrice(), DELTA);

		final OrderData orderData = wileyCheckoutFacade.placeOrder();

		OrderModel orderModel = wileyOrderService.getOrderForGUID(orderData.getGuid(),
				baseSiteService.getCurrentBaseSite());

		assertNotNull(orderModel);

		final Optional<WileyOrderProcessModel> orderProcess = wileyOrderFulfilmentProcessService.getOrderProcessActiveFor(
				orderModel);
		final WileyOrderProcessModel wileyOrderProcessModel = orderProcess.get();

		assertNotNull(wileyOrderProcessModel);

		waitForProcessCompletion(wileyOrderProcessModel);
		assertProcessState(wileyOrderProcessModel, ProcessState.SUCCEEDED);

		modelService.refresh(orderModel);
		modelService.refresh(orderModel.getEntries().get(0));

		assertEquals(1, orderModel.getEntries().size());
		assertEquals(OrderStatus.PAYMENT_WAIVED, orderModel.getEntries().get(0).getStatus());
	}
}
