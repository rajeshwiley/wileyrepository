package com.wiley.integration;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.externaltax.services.impl.TaxSystemResponseError;
import com.wiley.core.integration.sabrix.SabrixTaxGateway;
import com.wiley.core.integration.sabrix.dto.Wileyb2cTaxResponse;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToXml;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class SabrixTaxGatewayIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final int WIRE_MOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/SabrixTaxGatewayIntegrationTest/";
	private static final String B2C_STORE_ID = "wileyb2c";
	private static final String B2C_SITE_ID = "wileyb2c";
	private static final String ONE_INVOICES_ORDER_CODE = "oneInvoiceOrder";
	private static final String SEVERAL_INVOICES_ORDER_CODE = "severalInvoiceOrder";
	@Rule
	public final WireMockRule wireMockRule = new WireMockRule(wireMockConfig().port(WIRE_MOCK_PORT));
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	@Resource
	private SabrixTaxGateway sabrixTaxGateway;
	@Resource
	private CustomerAccountService customerAccountService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private SetupImpexService setupImpexService;
	@Resource
	private WileycomI18NService wileycomI18NService;
	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void before()
	{
		setupImpexService.importImpexFile("/wileytest/import/order/SabrixGatewayIntegrationTest/orders.impex", true);
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(B2C_SITE_ID), false);
		wileycomI18NService.setDefaultCurrentCountry();
	}

	@Test
	public void testSuccessOneInvoice() throws Exception
	{
		final OrderModel order = prepareOrderModelAndStubs(ONE_INVOICES_ORDER_CODE, "request_for_one_invoice.xml",
				"response_for_one_invoice.xml");

		final Wileyb2cTaxResponse wileyb2cTaxResponse = sabrixTaxGateway.calculateTaxForOrder(order);

		Assert.assertEquals(wileyb2cTaxResponse.getOrderTotalTax(), Double.valueOf(14.4));
	}

	private OrderModel prepareOrderModelAndStubs(final String oneInvoicesOrderCode,
			final String requestFile, final String responseFile) throws Exception
	{
		wireMockRule.resetMappings();
		wireMockRule.stubFor(post(
				urlMatching("/TaxCompliance/TaxComplianceService.serviceagent/EndpointURL"))
				.withRequestBody(
						equalToXml(loadFileAsString(TEST_REQUESTS_PATH + requestFile))
				).willReturn(
						aResponse()
								.withStatus(200)
								.withHeader("Content-Type", "text/xml; charset=utf-8")
								.withBody(loadFileAsString(TEST_REQUESTS_PATH + responseFile))
				));
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(B2C_STORE_ID);
		return customerAccountService.getOrderForCode(oneInvoicesOrderCode, baseStore);
	}

	@Test
	public void testSuccessSeveralInvoices() throws Exception
	{
		final OrderModel order = prepareOrderModelAndStubs(SEVERAL_INVOICES_ORDER_CODE, "request_for_several_invoices.xml",
				"response_for_several_invoices.xml");

		final Wileyb2cTaxResponse wileyb2cTaxResponse = sabrixTaxGateway.calculateTaxForOrder(order);

		Assert.assertEquals(wileyb2cTaxResponse.getOrderTotalTax(), Double.valueOf(21.57));
	}

	@Test
	public void testServiceError() throws Exception
	{
		final OrderModel order = prepareOrderModelAndStubs(ONE_INVOICES_ORDER_CODE, "request_for_one_invoice.xml",
				"response_service_error.xml");
		expectedException.expect(TaxSystemResponseError.class);
		expectedException.expectMessage("Sabrix tax calculation failed. The reason is REQUEST PROCESSED WITH ERROR RESPONSE."
				+ " The detailed error is: ErrorCode: ESB_MDM_DATA_ERROR ErrorDescription: Error in 'MDM SYSTEM' . Reason :- "
				+ "'OneSource_ProductCode is not present in MDM_DB'");

		sabrixTaxGateway.calculateTaxForOrder(order);
	}

	@Test
	public void testTaxCalculationError() throws Exception
	{
		final OrderModel order = prepareOrderModelAndStubs(ONE_INVOICES_ORDER_CODE, "request_for_one_invoice.xml",
				"response_tax_calculation_error.xml");
		expectedException.expect(TaxSystemResponseError.class);
		expectedException.expectMessage("Sabrix tax calculation failed.The detailed error is: Error code: FAILED_TO_CALCULATE_TAX"
				+ " Error description: Per calculation of the input data, no tax result is returned.");

		sabrixTaxGateway.calculateTaxForOrder(order);
	}

}
