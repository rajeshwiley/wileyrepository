package com.wiley.utils;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.Configurable;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.util.ReflectionTestUtils;


/**
 * Created by Mikhail_Asadchy on 11.07.2016.
 */
public final class HandshakeUtils
{

	private HandshakeUtils()
	{
	}

	public static void adjustRequestFactoryForTests(final HttpComponentsClientHttpRequestFactory requestFactory)
	{
		final RequestConfig config = RequestConfig.custom().setStaleConnectionCheckEnabled(true).build();
		//based on logic in HttpComponentsClientHttpRequestFactory#createConfig
		if (requestFactory.getHttpClient() instanceof Configurable)
		{
			ReflectionTestUtils.setField(requestFactory.getHttpClient(), "defaultConfig", config);
		}
		else
		{
			ReflectionTestUtils.setField(requestFactory, "requestConfig", config);
		}
	}

}
