package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.common.BaseProcessIntegrationTest;
import com.wiley.core.order.service.WileyOrderService;


/**
 * Integration test for {@link WileyOrderFulfilmentProcessServiceImpl}
 */

@IntegrationTest
@Ignore
public class WileyasOrderFulfilmentProcessServiceImplIntegrationTest extends BaseProcessIntegrationTest
{
	private static final String IMPEX_PATH = "/wileytest/import/order/WileyasOrderFulfilmentProcessServiceImplIntegrationTest";

	private static final String COMMON_DATA_PATH = IMPEX_PATH + "/common.impex";
	private static final String EDIT_ORDER_DATA_PATH = IMPEX_PATH + "/testOrdersForFulfillmentProcess.impex";

	private static final String BASE_SITE_ID = "asSite";

	private static final String DEFAULT_ENCODING = "UTF-8";
	public static final String ORDER_GUID_VALID = "test_order_for_fulfilment_valid";
	public static final String ORDER_GUID_EXTERNAL = "test_order_for_fulfilment_external";
	public static final String ORDER_GUID_NOT_CALCULATED = "test_order_for_fulfilment_not_calculated";
	public static final String ORDER_GUID_NO_ENTRIES = "test_order_for_fulfilment_no_entries";
	public static final String ORDER_GUID_INCORRECT_AMOUNT_AUTHORIZED = "test_order_for_fulfilment_incorrect_amount_authorized";
	public static final String ORDER_GUID_CC = "test_order_for_fulfilment_cc";
	public static final String ORDER_GUID_CC_AU = "test_order_for_fulfilment_cc_AU";
	public static final String PREVIOUS_ORDER_STATE_EMPTY_JSON = "{}";
	public static final String TRANSACTION_STATUS_ACCEPTED = "ACCEPTED";
	public static final String TRANSACTION_STATUS_HYBRIS_EXCEPTION = "HYBRIS_EXCEPTION";
	public static final String CHECK_ORDER_ACTION_ID = "checkOrder";
	public static final String TAKE_PAYMENT_ACTION_ID = "takePayment";
	public static final String EXPORT_ORDER_ACTION_ID = "exportOrder";
	public static final String ORDER_FAILED_NOTIFICATION_ACTION_ID = "orderFailedNotification";
	public static final String DEACTIVATE_PROCESS_ACTION_ID = "deactivateProcess";
	public static final String NOK = "NOK";
	public static final String OK = "OK";
	public static final String SKIP_REFUND = "SKIP_REFUND";
	public static final String SKIP_PAYMENT = "SKIP_PAYMENT";
	public static final String REFUND_PAYMENT_ACTION_ID = "refundPayment";

	@Resource
	private WileyOrderFulfilmentProcessServiceImpl wileyOrderFulfilmentProcessService;

	@Resource
	private WileyOrderService wileyOrderService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private ModelService modelService;

	private BaseSiteModel asSite;

	@Before
	public void setUp() throws Exception
	{
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		asSite = baseSiteService.getBaseSiteForUID(BASE_SITE_ID);
	}

	@Test
	public void testProcessShouldSkipAllStepsAndExportToErpIfOrderIsExternal()
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_EXTERNAL, asSite);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifySkippedAllStepsAndExportedToErp(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 0);
	}

	@Test
	public void testProcessShouldSkipAllStepsAndExportToErpIfProcessPaymentFalse()
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_VALID, asSite);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, false);

		waitForProcessCompletion(orderProcessModel);
		verifySkippedAllStepsAndExportedToErp(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 0);
	}

	@Test
	public void testProcessShouldSkipAllStepsAndFailIfOrderNotCalculated()
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_NOT_CALCULATED, asSite);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessFailedAfterCheckAction(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 0);
	}

	@Test
	public void testProcessShouldSkipAllStepsAndFailIfOrderNoEntries()
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_NO_ENTRIES, asSite);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessFailedAfterCheckAction(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 0);
	}

	@Test
	public void testProcessShouldSkipAllStepsAndFailIfIncorrectPaymentAmountAuthorized()
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_INCORRECT_AMOUNT_AUTHORIZED, asSite);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessFailedAfterCheckAction(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 1);
	}

	@Test
	public void testProcessShouldSkipRefundStepAndFailIfTakePaymentFailed() throws Exception
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_CC, asSite);
		// need to set calculated flag in java code for orders with paymentAddress
		orderModel.setCalculated(true);
		modelService.save(orderModel);
		// use 200 status for error response to avoid retry policy
		wireMock.addRequestAndResponse(CAPTURE_MAPPING_PATH, CAPTURE_ERROR_RESPONSE_PATH);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessFailedAfterTakePaymentAction(orderModel, orderProcessModel);
		verifyNoNewPaymentTransactions(orderModel, 1);

	}

	@Test
	public void testProcessShouldSkipRefundStepForNewOrder() throws Exception
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_CC, asSite);
		// need to set calculated flag in java code for orders with paymentAddress
		orderModel.setCalculated(true);
		modelService.save(orderModel);
		wireMock.addRequestAndResponse(CAPTURE_MAPPING_PATH, US_RESPONSE_PATH + CAPTURE_RESPONSE_PATH);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessSkippedRefundActionAfterSuccessTakePaymentAction(orderModel, orderProcessModel);
		// new CAPTURE transaction entry
		List<PaymentTransactionModel> paymentTransactions = orderModel.getPaymentTransactions();
		assertNotNull(paymentTransactions);
		assertEquals(1, paymentTransactions.size());

		PaymentTransactionModel paymentTransactionModel = paymentTransactions.get(0);

		verifyPaymentTransactionEntryAdded(paymentTransactionModel, 1, PaymentTransactionType.CAPTURE,
				TRANSACTION_STATUS_ACCEPTED);
	}

	@Test
	public void testProcessShouldTakePaymentAndRefundIfCountryChanged() throws Exception
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_CC_AU, asSite);
		// need to set calculated flag in java code for orders with paymentAddress
		orderModel.setCalculated(true);
		modelService.save(orderModel);
		wireMock.addRequestAndResponse(CAPTURE_MAPPING_PATH, AUJW_RESPONSE_PATH + CAPTURE_RESPONSE_PATH);
		wireMock.addRequestAndResponse(REFUND_MAPPING_PATH, US_RESPONSE_PATH + REFUND_RESPONSE_PATH);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessSuccessTakePaymentAndRefundActions(orderModel, orderProcessModel);
		// new CAPTURE AND REFUND transaction entries
		assertTrue(CollectionUtils.isNotEmpty(orderModel.getPaymentTransactions()));
		assertEquals(2, orderModel.getPaymentTransactions().size());

		PaymentTransactionModel audPaymentTransactionModel = orderModel.getPaymentTransactions().get(1);
		verifyPaymentTransactionEntryAdded(audPaymentTransactionModel, 1, PaymentTransactionType.CAPTURE,
				TRANSACTION_STATUS_ACCEPTED);

		PaymentTransactionModel usdPaymentTransactionModel = orderModel.getPaymentTransactions().get(0);
		verifyPaymentTransactionEntryAdded(usdPaymentTransactionModel, 2, PaymentTransactionType.REFUND_FOLLOW_ON,
				TRANSACTION_STATUS_ACCEPTED);
	}

	@Test
	public void testProcessShouldSkipExportIfRefundFailed() throws Exception
	{
		final OrderModel orderModel = wileyOrderService.getOrderForGUID(ORDER_GUID_CC_AU, asSite);
		// need to set calculated flag in java code for orders with paymentAddress
		orderModel.setCalculated(true);
		modelService.save(orderModel);
		wireMock.addRequestAndResponse(CAPTURE_MAPPING_PATH, AUJW_RESPONSE_PATH + CAPTURE_RESPONSE_PATH);
		wireMock.addRequestAndResponse(REFUND_MAPPING_PATH, US_RESPONSE_PATH + REFUND_ERROR_RESPONSE_PATH);

		BusinessProcessModel orderProcessModel = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel,
				true,
				PREVIOUS_ORDER_STATE_EMPTY_JSON, true);

		waitForProcessCompletion(orderProcessModel);
		verifyProcessFailedAndSkippedExportActionAfterFailedRefundAction(orderModel, orderProcessModel);
		// new CAPTURE but no REFUND transaction entries
		assertTrue(CollectionUtils.isNotEmpty(orderModel.getPaymentTransactions()));
		assertEquals(2, orderModel.getPaymentTransactions().size());

		PaymentTransactionModel audPaymentTransactionModel = orderModel.getPaymentTransactions().get(1);
		verifyPaymentTransactionEntryAdded(audPaymentTransactionModel, 1, PaymentTransactionType.CAPTURE,
				TRANSACTION_STATUS_ACCEPTED);

		PaymentTransactionModel usdPaymentTransactionModel = orderModel.getPaymentTransactions().get(0);
		verifyPaymentTransactionEntryAdded(usdPaymentTransactionModel, 2, PaymentTransactionType.REFUND_FOLLOW_ON,
				TRANSACTION_STATUS_HYBRIS_EXCEPTION);
	}

	protected void verifyProcessFailedAfterCheckAction(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.CHECKED_INVALID,
				orderProcessModel, ProcessState.FAILED,
				Pair.of(CHECK_ORDER_ACTION_ID, NOK),
				Pair.of(ORDER_FAILED_NOTIFICATION_ACTION_ID, OK)
		);
	}

	protected void verifyProcessFailedAfterTakePaymentAction(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.PAYMENT_FAILED,
				orderProcessModel, ProcessState.FAILED,
				Pair.of(CHECK_ORDER_ACTION_ID, OK),
				Pair.of(TAKE_PAYMENT_ACTION_ID, NOK),
				Pair.of(ORDER_FAILED_NOTIFICATION_ACTION_ID, OK)
		);
	}

	protected void verifyProcessSkippedRefundActionAfterSuccessTakePaymentAction(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.EXPORTED_TO_ERP,
				orderProcessModel, ProcessState.SUCCEEDED,
				Pair.of(CHECK_ORDER_ACTION_ID, OK),
				Pair.of(TAKE_PAYMENT_ACTION_ID, SKIP_REFUND),
				Pair.of(EXPORT_ORDER_ACTION_ID, OK),
				Pair.of(DEACTIVATE_PROCESS_ACTION_ID, OK)
		);
	}

	protected void verifyProcessSuccessTakePaymentAndRefundActions(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.EXPORTED_TO_ERP,
				orderProcessModel, ProcessState.SUCCEEDED,
				Pair.of(CHECK_ORDER_ACTION_ID, OK),
				Pair.of(TAKE_PAYMENT_ACTION_ID, OK),
				Pair.of(REFUND_PAYMENT_ACTION_ID, OK),
				Pair.of(EXPORT_ORDER_ACTION_ID, OK),
				Pair.of(DEACTIVATE_PROCESS_ACTION_ID, OK)
		);
	}

	protected void verifyProcessFailedAndSkippedExportActionAfterFailedRefundAction(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.PAYMENT_FAILED,
				orderProcessModel, ProcessState.FAILED,
				Pair.of(CHECK_ORDER_ACTION_ID, OK),
				Pair.of(TAKE_PAYMENT_ACTION_ID, OK),
				Pair.of(REFUND_PAYMENT_ACTION_ID, NOK),
				Pair.of(ORDER_FAILED_NOTIFICATION_ACTION_ID, OK)
		);
	}

	protected void verifySkippedAllStepsAndExportedToErp(final OrderModel orderModel,
			final BusinessProcessModel orderProcessModel)
	{
		verifyProcessAndAction(
				orderModel, OrderStatus.EXPORTED_TO_ERP,
				orderProcessModel, ProcessState.SUCCEEDED,
				Pair.of(CHECK_ORDER_ACTION_ID, SKIP_PAYMENT),
				Pair.of(EXPORT_ORDER_ACTION_ID, OK),
				Pair.of(DEACTIVATE_PROCESS_ACTION_ID, OK)
		);
	}

	private void verifyProcessTaskLog(final ProcessTaskLogModel exportOrderActionLog, final String actionId,
			final String returnCode)
	{
		assertEquals(actionId, exportOrderActionLog.getActionId());
		assertEquals(returnCode, exportOrderActionLog.getReturnCode());
	}


	protected void verifyProcessAndAction(final OrderModel orderModel, final OrderStatus orderStatus,
			final BusinessProcessModel orderProcessModel, final ProcessState processState,
			final Pair<String, String>... actionsWithReturnCodes)
	{
		modelService.refresh(orderModel);
		assertEquals(orderStatus, orderModel.getStatus());
		assertProcessState(orderProcessModel, processState);

		Collection<ProcessTaskLogModel> taskLogs = orderProcessModel.getTaskLogs();
		assertNotNull(taskLogs);

		assertEquals(actionsWithReturnCodes.length, taskLogs.size());

		Iterator<ProcessTaskLogModel> taskLogIterator = taskLogs
				.stream()
				.sorted(Comparator.comparing(ProcessTaskLogModel::getEndDate))
				.collect(Collectors.toList())
				.iterator();

		for (Pair<String, String> actionWithReturnCode : actionsWithReturnCodes)
		{
			verifyProcessTaskLog(taskLogIterator.next(), actionWithReturnCode.getLeft(), actionWithReturnCode.getRight());
		}

	}

	private void verifyNoNewPaymentTransactions(final OrderModel orderModel, int paymentTransactionsSize)
	{
		// no new payment transactions
		assertNotNull(orderModel.getPaymentTransactions());
		assertEquals(paymentTransactionsSize, orderModel.getPaymentTransactions().size());
	}

	private void verifyPaymentTransactionEntryAdded(final PaymentTransactionModel paymentTransactionModel, int originalSize,
			final PaymentTransactionType paymentTransactionType, final String transactionStatus)
	{
		List<PaymentTransactionEntryModel> transactionEntries = paymentTransactionModel.getEntries();
		assertTrue(CollectionUtils.isNotEmpty(transactionEntries));
		assertEquals(originalSize + 1, transactionEntries.size());
		PaymentTransactionEntryModel lastTransactionEntry = transactionEntries.get(originalSize);
		assertEquals(paymentTransactionType, lastTransactionEntry.getType());
		assertEquals(transactionStatus, lastTransactionEntry.getTransactionStatus());
	}
}
