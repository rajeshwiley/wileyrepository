package com.wiley.facades.as.address;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;


@IntegrationTest
public class WileyasAddressVerificationFacadeIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasAddressVerificationFacadeIntegrationTest.class);
	private static final String MAPPING_PREFIX = "/mappings/addresses/suggestions/";
	private static final String RESPONSE_PREFIX = "/__files/addresses/suggestions/";
	private static final String REQUEST_FOR_Q0 = "mapping_suggest_FR.json";
	private static final String REQUEST_FOR_Q3_AND_N1 = "mapping_suggest_PL.json";
	private static final String REQUEST_FOR_Q3_AND_Q1 = "mapping_suggest_DE.json";
	private static final String REQUEST_FOR_THIS = "mapping_suggest_200.json";
	private static final String REQUEST_FOR_Q3 = "mapping_suggest_AU.json";
	private static final String RETURN_Q0 = "response_suggest_FR_200.json";
	private static final String RETURN_Q3_AND_N1 = "response_suggest_PL_200.json";
	private static final String RETURN_Q3_AND_Q1 = "response_suggest_DE_200.json";
	private static final String RETURN_Q3 = "response_suggest_AU_200.json";
	private static final String COUNTRY_ISO2 = "US";
	private static final String COUNTRY_Q0 = "FR";
	private static final String COUNTRY_Q3_AND_N1 = "PL";
	private static final String COUNTRY_Q3_AND_Q1 = "DE";
	private static final String COUNTRY_THIS = "CA";
	private static final String COUNTRY_Q3 = "AU";
	private static final String REGION_ISO = "KY";
	private static final String REGION_EMPTY = StringUtils.EMPTY;
	private static final String REGION_THE_SAME = "AB";

	@Resource
	private WileyasAddressVerificationFacade wileyasAddressVerificationFacade;

	@Resource
	private RestTemplate addressDoctorApigeeEdgeOAuth2RestTemplate;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.info("WireMock Request: {}", request);
			LOG.info("WireMock Response: {}", response);
		});
	}

	@Test
	public void testGetCustomerDataSuccessful() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				MAPPING_PREFIX + "mapping_suggest_US_KY.json",
				RESPONSE_PREFIX + "response_suggest_US_KY_200.json"
		);
		wireMock.addStubMapping(stubMapping);

		final AddressData addressData = prepareDefaultAddressData(COUNTRY_ISO2, REGION_ISO);

		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(
						addressData);
		assertNotNull(decision);
		assertEquals(AddressVerificationDecision.REVIEW, decision.getDecision());

		List<AddressData> addresses = decision.getSuggestedAddresses();
		assertNotNull(addresses);
		assertFalse(addresses.isEmpty());

		AddressData address = addresses.get(0);
		assertNotNull(address);
		assertEquals(COUNTRY_ISO2, address.getCountry().getIsocode());
		assertNotNull(address.getCountry().getName());

		assertNotNull(address.getRegion());
		assertNotNull(address.getRegion().getIsocode());
		assertNotNull(address.getRegion().getName());
	}


	@Test
	public void verify503Test() throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(MAPPING_PREFIX + "mapping_suggest_503.json");
		wireMock.addStubMapping(stubMapping);

		AddressData addressData = new AddressData();
		addressData.setLine1(String.valueOf(HttpStatus.SERVICE_UNAVAILABLE.value()));

		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.UNKNOWN, decision.getDecision());
	}

	@Test
	public void verifyServerErrorTest() throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(MAPPING_PREFIX + "mapping_suggest_500.json");
		wireMock.addStubMapping(stubMapping);

		AddressData addressData = new AddressData();
		addressData.setLine1(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));

		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.UNKNOWN, decision.getDecision());
	}

	@Test
	public void verify400Test() throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(MAPPING_PREFIX + "mapping_suggest_400.json");
		wireMock.addStubMapping(stubMapping);

		AddressData addressData = new AddressData();
		addressData.setLine1(String.valueOf(HttpStatus.BAD_REQUEST.value()));

		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.UNKNOWN, decision.getDecision());
	}

	@Test
	public void verify401Test() throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(MAPPING_PREFIX + "mapping_suggest_401.json");
		wireMock.addStubMapping(stubMapping);

		AddressData addressData = new AddressData();
		addressData.setLine1(String.valueOf(HttpStatus.UNAUTHORIZED.value()));

		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.UNKNOWN, decision.getDecision());
	}

	@Test
	public void verifyQ0ResultTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				MAPPING_PREFIX + REQUEST_FOR_Q0,
				RESPONSE_PREFIX + RETURN_Q0
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_Q0, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertFalse(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.REJECT, decision.getDecision());
	}

	@Test
	public void verifyQ3AndN1ResultTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				MAPPING_PREFIX + REQUEST_FOR_Q3_AND_N1,
				RESPONSE_PREFIX + RETURN_Q3_AND_N1
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_Q3_AND_N1, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.UNKNOWN, decision.getDecision());
	}

	@Test
	public void verifyQ3AndQ1ResultTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				MAPPING_PREFIX + REQUEST_FOR_Q3_AND_Q1,
				RESPONSE_PREFIX + RETURN_Q3_AND_Q1
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_Q3_AND_Q1, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertFalse(decision.isSkipAddressDoctor());
		assertEquals(AddressVerificationDecision.REJECT, decision.getDecision());
	}

	@Test
	public void verifyTheSameAddressResultTest() throws Exception
	{
		// given
		final StubMapping stubMapping = createStubMappingFrom(MAPPING_PREFIX + REQUEST_FOR_THIS);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_THIS, REGION_THE_SAME);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertTrue(decision.isSkipAddressDoctor());
		assertFalse(decision.isHasSuggestions());
		assertEquals(AddressVerificationDecision.REVIEW, decision.getDecision());
	}

	@Test
	public void verifyQ3ResultTest() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				MAPPING_PREFIX + REQUEST_FOR_Q3,
				RESPONSE_PREFIX + RETURN_Q3
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_Q3, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertFalse(decision.isSkipAddressDoctor());
		assertTrue(decision.isHasSuggestions());
		assertEquals(AddressVerificationDecision.REVIEW, decision.getDecision());
	}

	protected AddressData prepareDefaultAddressData(final String countryIso, final String regionIso)
	{
		AddressData addressData = new AddressData();
		CountryData countryData = new CountryData();
		countryData.setIsocode(countryIso);
		addressData.setCountry(countryData);
		RegionData regionData = new RegionData();
		regionData.setIsocodeShort(regionIso);
		addressData.setRegion(regionData);
		addressData.setLine1("address line-1");
		addressData.setLine2("address line-2");
		addressData.setTown("Test Town");
		addressData.setPostalCode("122333");
		return addressData;
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) addressDoctorApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}
