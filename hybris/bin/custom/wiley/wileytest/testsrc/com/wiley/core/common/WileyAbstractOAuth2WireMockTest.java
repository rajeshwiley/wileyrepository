package com.wiley.core.common;

import org.junit.Before;


/**
 * Created by Mikhail_Asadchy on 05.07.2016.
 */
public abstract class WileyAbstractOAuth2WireMockTest extends WileyAbstractWireMockTest implements WileyOAuth2SetupMixIn
{

	@Before
	public void setUp() throws Exception
	{
		setUpOauth(wireMock);
	}
}
