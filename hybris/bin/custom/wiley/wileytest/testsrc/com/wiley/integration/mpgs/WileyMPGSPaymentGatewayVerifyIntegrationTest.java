package com.wiley.integration.mpgs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.utils.HandshakeUtils;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


@IntegrationTest
public class WileyMPGSPaymentGatewayVerifyIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String ORDER_ID = "order12345";
	private static final String TRANSACTION_ID = "transaction12345";
	private static final String CURRENCY = "USD";
	private static final String SESSION_ID = "SESSION0002298837250L66683331F8";
	private static final String BILLING_STREET1 = "Test 11-22";
	private static final String BILLING_STREET2 = null;
	private static final String BILLING_CITY = "NY";
	private static final String BILLING_COUNTRY = "USA";
	private static final String BILLING_POSTAL_CODE = "12345678";
	private static final String BILLING_STATE = null;
	private static final String STATUS_DETAILS = "APPROVED";
	private static final String VERIFY_REQUEST_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST/order/" + ORDER_ID + "/transaction/" + TRANSACTION_ID;
	private static final String SUCCESS_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/verify_request_success.json";
	private static final String SUCCCESS_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/verify_response_success.json";

	private static final String ERROR_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/verify_request_error.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/verify_response_error.json";

	WileyVerifyRequest request;

	@Resource
	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Resource
	private RestTemplate mpgsPaymentGatewayClientRestTemplate;

	@Before
	public void setUp() throws Exception
	{
		final HttpComponentsClientHttpRequestFactory requestFactory =
				(HttpComponentsClientHttpRequestFactory) mpgsPaymentGatewayClientRestTemplate.getRequestFactory();

		if (requestFactory != null)
		{
			HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
		}

		request = new WileyVerifyRequest();
		request.setApiOperation(WileyMPGSConstants.API_OPERATAION_VERIFY);
		request.setCurrency(CURRENCY);
		request.setSessionId(SESSION_ID);
		request.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);
		request.setBillingStreet1(BILLING_STREET1);
		request.setBillingStreet2(BILLING_STREET2);
		request.setBillingCity(BILLING_CITY);
		request.setBillingCountry(BILLING_COUNTRY);
		request.setBillingPostalCode(BILLING_POSTAL_CODE);
		request.setStateProvince(BILLING_STATE);
		request.setPaymentProvider(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		request.setUrlPath(WIRE_MOCK_URL + VERIFY_REQUEST_PATH);

	}

	@Test
	public void testVerifyPositive() throws Exception
	{
		// given
		wireMock.stubFor(put(urlMatching(VERIFY_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCCESS_RESPONSE_JSON_FILE_PATH))
				));

		// when
		WileyVerifyResponse response = mpgsPaymentGateway.verify(request);

		// then
		assertEquals(TransactionStatus.ACCEPTED.name(), response.getStatus());
		assertEquals(STATUS_DETAILS, response.getStatusDetails());
		assertEquals(TRANSACTION_ID, response.getTransactionId());
		assertEquals(CURRENCY, response.getCurrency());
		assertNotNull(response.getTimeOfRecord());
	}

	@Test(expected = HttpClientErrorException.class)
	public void testVerifyError() throws Exception
	{
		wireMock.stubFor(put(urlMatching(VERIFY_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(ERROR_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_BAD_REQUEST)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))
				));

		WileyVerifyResponse response = mpgsPaymentGateway.verify(request);
	}

}
