package com.wiley.core.refund;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.common.WileyWireMockRule;
import com.wiley.core.order.service.WileyOrderService;
import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.refund.OrderRefundEntry;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.core.annotation.Order;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@Ignore
public class WileyRefundServiceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
    private static final String BASE_PATH = "/wileytest/import/order/WileyRefundServiceIntegrationTest";
    private static final String IMPEX_WEL_CC_ORDERS = BASE_PATH + "/impex/wel-orders-cc.impex";
    private static final String IMPEX_AGS_CC_ORDERS = BASE_PATH + "/impex/ags-orders-cc.impex";
    private static final String IMPEX_WEL_REFUNDED = BASE_PATH + "/impex/wel-orders-refunded.impex";
    private static final String IMPEX_COMMON_DATA = BASE_PATH + "/impex/common.impex";
    private static final String DEFAULT_ENCODING = "UTF-8";

    private static final String WIREMOCK_MAPPING = BASE_PATH + "/mappings/refund.json";
    private static final String WIREMOCK_RESPONCE_WEL_1 = BASE_PATH + "/responses/response_refund_wel_1.json";
    private static final String WIREMOCK_RESPONCE_WEL_2 = BASE_PATH + "/responses/response_refund_wel_2.json";
    private static final String WIREMOCK_RESPONCE_WEL_3 = BASE_PATH + "/responses/response_refund_wel_3.json";
    private static final String WIREMOCK_RESPONCE_AGS_1 = BASE_PATH + "/responses/response_refund_ags_1.json";
    private static final String WIREMOCK_RESPONCE_AGS_2 = BASE_PATH + "/responses/response_refund_ags_2.json";
    private static final String WIREMOCK_RESPONCE_ERROR = BASE_PATH + "/responses/response_refund_error.json";

    private static final String USER_ID = "testuser";
    private static final String AGS_SITE_ID = "ags";
    private static final String WEL_SITE_ID = "wel";
    private static final String WEL_ORDER_CC_1 = "welCcTestOrderGuid1";
    private static final String WEL_ORDER_CC_2 = "welCcTestOrderGuid2";
    private static final String WEL_ORDER_CC_3 = "welCcTestOrderGuid3";
    private static final String AGS_ORDER_CC_1 = "agsCcTestOrderGuid1";
    private static final String AGS_ORDER_CC_2 = "agsCcTestOrderGuid2";
    private static final String REFUNDED_ORDER = "refundedTestOrderGuid";

    @Rule
    public WileyWireMockRule wireMock = WileyWireMockRule.build();

    @Resource
    private BaseSiteService baseSiteService;

    @Resource
    private WileyRefundService wileyRefundService;

    @Resource
    private WileyOrderService wileyOrderService;

    @Before
    public void prepare() throws Exception
    {
        importCsv(IMPEX_COMMON_DATA, DEFAULT_ENCODING);
        userService.setCurrentUser(userService.getUserForUID(USER_ID));
    }

    @Test
    @Order(1)
    public void checkIsorderRefund() throws ImpExException
    {
        // given
        setDefaultSite(WEL_SITE_ID);
        // when
        importData(IMPEX_WEL_REFUNDED, DEFAULT_ENCODING);
        // then
        final OrderModel orderAfter = getOrderModel(WEL_SITE_ID, REFUNDED_ORDER);
        assertTrue(wileyRefundService.isFullyRefundedOrder(orderAfter));
    }

    @Test
    public void tryRefundCcPaymentCapturedWelOrder() throws Exception
    {
        // given
        setDefaultSite(WEL_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_WEL_1);
        importData(IMPEX_WEL_CC_ORDERS, DEFAULT_ENCODING);
        // when
        // then
        processTest(WEL_SITE_ID, WEL_ORDER_CC_1);
    }

    @Test
    public void tryRefundCcCompletedWelOrder() throws Exception
    {
        // given
        setDefaultSite(WEL_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_WEL_2);
        importData(IMPEX_WEL_CC_ORDERS, DEFAULT_ENCODING);
        // when
        // then
        processTest(WEL_SITE_ID, WEL_ORDER_CC_2);
    }

    @Test(expected = OrderRefundException.class)
    public void tryRefundCcCompletedWelOrderError() throws Exception
    {
        // given
        setDefaultSite(WEL_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_ERROR);
        importData(IMPEX_WEL_CC_ORDERS, DEFAULT_ENCODING);
        final OrderRefundRequest refundRequest = getOrderRefundRequest(WEL_SITE_ID, WEL_ORDER_CC_1);
        final OrderModel orderBefore = getOrderModel(WEL_SITE_ID, WEL_ORDER_CC_1);
        assertTrue(wileyRefundService.isEligibleForRefund(orderBefore));
        // when
        wileyRefundService.processRefund(refundRequest);
        // then
        // Exceptions are expected
    }

    @Test
    public void tryRefundCcPaymentCapturedAgsOrder() throws Exception
    {
        // given
        setDefaultSite(AGS_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_AGS_1);
        importData(IMPEX_AGS_CC_ORDERS, DEFAULT_ENCODING);
        // when
        // then
        processTest(AGS_SITE_ID, AGS_ORDER_CC_1);
    }

    @Test
    public void tryRefundCcCompletedAgsOrder() throws Exception
    {
        // given
        setDefaultSite(AGS_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_AGS_2);
        importData(IMPEX_AGS_CC_ORDERS, DEFAULT_ENCODING);
        // when
        // then
        processTest(AGS_SITE_ID, AGS_ORDER_CC_2);
    }

    @Test(expected = OrderRefundException.class)
    public void tryRefundCcCompletedAgsOrderError() throws Exception
    {
        // given
        setDefaultSite(AGS_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_ERROR);
        importData(IMPEX_AGS_CC_ORDERS, DEFAULT_ENCODING);
        final OrderRefundRequest refundRequest = getOrderRefundRequest(AGS_SITE_ID, AGS_ORDER_CC_1);
        final OrderModel orderBefore = getOrderModel(AGS_SITE_ID, AGS_ORDER_CC_1);
        assertTrue(wileyRefundService.isEligibleForRefund(orderBefore));
        // when
        wileyRefundService.processRefund(refundRequest);
        // then
        // Exceptions are expected
    }

    @Test
    public void tryProcessRefundPaymentTransaction() throws Exception
    {
        // given
        setDefaultSite(WEL_SITE_ID);
        setUpWireMockMapping(WIREMOCK_MAPPING, WIREMOCK_RESPONCE_WEL_3);
        importData(IMPEX_WEL_CC_ORDERS, DEFAULT_ENCODING);
        final OrderModel orderBefore = getOrderModel(WEL_SITE_ID, WEL_ORDER_CC_3);
        assertTrue(wileyRefundService.isEligibleForRefund(orderBefore));
        // when
        wileyRefundService.processRefundPaymentTransaction(orderBefore, new BigDecimal(100));
        // then
        // Do not expect any exceptions.
        final OrderModel orderAfter = getOrderModel(WEL_SITE_ID, WEL_ORDER_CC_3);
        assertTrue(wileyRefundService.isFullyRefundedOrder(orderAfter));
    }

    private void setUpWireMockMapping(final String request, final String response) throws Exception
    {
        wireMock.addRequestAndResponse(request, response);
        Thread.sleep(1000);
    }

    private void processTest(final String baseSiteId, final String orderId) throws OrderRefundException
    {
        // given
        final OrderRefundRequest refundRequest = getOrderRefundRequest(baseSiteId, orderId);

        // when
        final OrderModel orderBefore = getOrderModel(baseSiteId, orderId);
        assertTrue(wileyRefundService.isEligibleForRefund(orderBefore));
        wileyRefundService.processRefund(refundRequest);

        // then
        // Do not expect any exceptions.
        final OrderModel orderAfter = getOrderModel(baseSiteId, orderId);
        assertTrue(wileyRefundService.isFullyRefundedOrder(orderAfter));
    }

    private OrderModel getOrderModel(final String baseSiteId, final String orderId)
    {
        final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(baseSiteId);
        return wileyOrderService.getOrderForGUID(orderId, baseSite);
    }

    private OrderRefundRequest getOrderRefundRequest(final String baseSiteId, final String orderId)
    {
        final OrderModel order = getOrderModel(baseSiteId, orderId);
        final List<OrderRefundEntry> refundEntries = new ArrayList<>();
        order.getEntries().stream().forEach(
            entry ->
            {
                final OrderRefundEntry refEntry =
                        new OrderRefundEntry(entry, entry.getQuantity(), "", RefundReason.LOSTINTRANSIT);
                refundEntries.add(refEntry);
            }
        );

        return new OrderRefundRequest(order, refundEntries, new BigDecimal(order.getTotalPrice()));
    }

    private void setDefaultSite(final String siteId)
    {
        baseSiteService.setCurrentBaseSite(siteId, false);
    }
}
