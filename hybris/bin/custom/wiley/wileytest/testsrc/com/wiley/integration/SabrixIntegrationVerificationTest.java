package com.wiley.integration;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import com.wiley.utils.FileTestUtils;
import com.wiley.utils.SoapTestUtils;
import com.wiley.utils.XmlTestUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



/**
 * This test was implemented for Sabrix web service analysis in scope of external tax calculation POC. It's ignored due
 * to it test external system and should don't run during CI process
 *
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public class SabrixIntegrationVerificationTest
{
	private static final String STATE_MARKER = "STATE_MARKER";
	private static final String CITY_MARKER = "CITY_MARKER";
	private static final String ZIP_MARKER = "ZIP_MARKER";
	private static final String COUNTRY_MARKER = "COUNTRY_MARKER";
	private static final String PRODUCT_CODE_MARKER = "PRODUCT_CODE_MARKER";
	private static final String TRANSACTION_TYPE_MARKER = "TRANSACTION_TYPE_MARKER";
	private static final String NON_PRODUCT_CODE_MARKER = "NON_PRODUCT_CODE_MARKER";
	private static final String FULFILMENT_TYPE_MARKER = "FULFILMENT_TYPE_MARKER";
	private static final String MEDIA_TYPE_MARKER = "MEDIA_TYPE_MARKER";
	private static final String EXTERNAL_COMPANY_ID = "EXTERNAL_COMPANY_ID_MARKER";
	private static final String EXTERNAL_COMPANY_COUNTRY = "EXTERNAL_COMPANY_COUNTRY";
	private static final String EXTERNAL_COMPANY_STATE = "EXTERNAL_COMPANY_STATE";
	private static final String EXTERNAL_COMPANY_CITY = "EXTERNAL_COMPANY_CITY";
	private static final String EXTERNAL_COMPANY_POSTAL_CODE = "EXTERNAL_COMPANY_POSTAL_CODE";
	private static final String URL = "https://sit.gmacintegration.wiley.com/TaxCompliance/TaxComplianceService.serviceagent"
			+ "/EndpointURL ";
	private static final String SOAP_ACTION = "/TaxCompliance/TaxComplianceService.serviceagent/EndpointURL"
			+ "/TaxComplianceOperation";
	private static final String TAX_RESPONSE_PATH = "*[local-name()='Envelope']/*[local-name()='Body']"
			+ "/*[local-name()='TaxCalculationResponseEBM']";
	private static final String SERVICE_STATUS_PATH = "/" + TAX_RESPONSE_PATH + "/*[local-name()='ServiceResponse']"
			+ "/*[local-name()='ServiceResponseStatus']";
	private static final String SERVICE_MSG_PATH = "/" + TAX_RESPONSE_PATH + "/*[local-name()='ServiceResponse']"
			+ "/*[local-name()='ServiceResponseMesg']";
	private static final String OUTDATA_PATH = "/*[local-name()='taxCalculationResponse']/*[local-name()='OUTDATA']";
	private static final String IS_SUCCESS_PATH =
			"/" + TAX_RESPONSE_PATH + OUTDATA_PATH + "/*[local-name()='REQUEST_STATUS']/*[local-name()='IS_SUCCESS']";
	private static final String IS_PARTIAL_SUCCESS_PATH =
			"/" + TAX_RESPONSE_PATH + OUTDATA_PATH + "/*[local-name()='REQUEST_STATUS']/*[local-name()='IS_PARTIAL_SUCCESS']";
	private static final String TOTAL_TAX_AMOUNT_PATH = "sum(/" + TAX_RESPONSE_PATH + OUTDATA_PATH
			+ "/*[local-name()='INVOICE']/*[local-name()='LINE']/*[local-name()='TOTAL_TAX_AMOUNT'])";
	private static final String ERROR_PATH = TAX_RESPONSE_PATH + OUTDATA_PATH
			+ "/*[local-name()='REQUEST_STATUS']/*[local-name()='ERROR']/*[local-name()='DESCRIPTION']";
	private static final String TRUE_LITERAL = "true";
	private static final String SUCCESS_SERVICE_STATUS = "SUCCESS";
	private static final String REQUEST_FOLDER = "/wileytest/integration/SabrixIntegrationVerificationTest/request/";
	private static final String DATA_FOLDER = "/wileytest/integration/SabrixIntegrationVerificationTest/data/";
	private static final String PASSED_TEST_MARKER = "PASSED_TESTS";


	@Test
	public void testUSStates() throws Exception
	{
		List<String> states = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "zip_code_database.csv");
		Map<String, String> zipByState = new HashMap<>();
		Map<String, String> cityByState = new HashMap<>();
		for (String stateLine : states)
		{
			String[] parts = stateLine.split(",");
			String zip = parts[0];
			String city = parts[1];
			String state = parts[2];
			zipByState.put(state, zip);
			cityByState.put(state, city);
		}
		String requestTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_one_invoice_US.xml");
		String request;
		List<String> usStates = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "us_states.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(usStates, reportBuilder, "State");


		for (String usState : usStates)
		{
			String city = cityByState.get(usState);
			String zip = zipByState.get(usState);
			if (zip == null)
			{
				throw new IllegalStateException("Could find zip for state " + usState);
			}
			if (city == null)
			{
				throw new IllegalStateException("Could find city for state " + usState);
			}
			request = requestTemplate.replace(STATE_MARKER, usState).replace(CITY_MARKER, city).replace(ZIP_MARKER, zip);
			failed = sendResponseAndCheckIt(request, failed, usState, reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testUSStates.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString().replace(PASSED_TEST_MARKER, String.valueOf(usStates.size() - failed)));
		Assert.assertEquals(failed, 0);
	}

	private void createHeader(final List<String> usStates, final StringBuilder reportBuilder, final String... parameters)
	{
		reportBuilder.append("<html charset=\"utf-8\"><head><meta http-equiv=\"content-type\" content=\"text/html;")
				.append(" charset=UTF-8\"><title>Test US states</title></head><body>").append(
				"<h2>" + PASSED_TEST_MARKER + " of ").append(usStates.size()).append(" passed</h2>")
				.append("<table border=\"1\"><tr>");
		for (String parameter : parameters)
		{
			reportBuilder.append("<th>").append(parameter).append("</th>");
		}
		reportBuilder.append("<th>Status</th><th>Total Tax Amount</th><th>Details</th><th>Request</th>")
				.append("<th>Response</th></tr>");

	}

	@Test
	public void testCanadaStates() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_one_invoice_CA.xml");
		String request;
		List<String> canadaStates = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "canada_states.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(canadaStates, reportBuilder, "State");

		for (String state : canadaStates)
		{
			request = requestTemplate.replace(STATE_MARKER, state);

			failed = sendResponseAndCheckIt(request, failed, state, reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testCAStates.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString().replace(PASSED_TEST_MARKER, String.valueOf(canadaStates.size() - failed)));
		Assert.assertEquals(failed, 0);
	}

	@Test
	public void testOtherCountries() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_one_invoice_other_countries.xml");
		String request;
		List<String> countries = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "countries.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(countries, reportBuilder, "Country");

		for (String country : countries)
		{
			request = requestTemplate.replace(COUNTRY_MARKER, country);

			failed = sendResponseAndCheckIt(request, failed, country, reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testOtherCountries.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString().replace(PASSED_TEST_MARKER, String.valueOf(countries.size() - failed)));
		Assert.assertEquals(failed, 0);
	}

	@Test
	public void testDifferentProductCodes() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(
				REQUEST_FOLDER + "request_for_one_invoice_one_line_different_product_codes.xml");
		String request;
		List<String> productCodeAndTransactionTypes = FileTestUtils.loadFileAsStrings(
				DATA_FOLDER + "product_codes_and_transaction_types.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(productCodeAndTransactionTypes, reportBuilder, "Esb Mapping", "PDM Product Code", "Transaction Type");

		for (String productCodeAndTransactionType : productCodeAndTransactionTypes)
		{
			String productCode = extractParameter(productCodeAndTransactionType, 1);
			String transactionType = extractParameter(productCodeAndTransactionType, 2);

			request = requestTemplate.replace(PRODUCT_CODE_MARKER, productCode);
			request = request.replace(TRANSACTION_TYPE_MARKER, transactionType);

			failed = sendResponseAndCheckIt(request, failed, removeQuotes(productCodeAndTransactionType), reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testProductCodes.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString()
						.replace(PASSED_TEST_MARKER, String.valueOf(productCodeAndTransactionTypes.size() - failed)));
		Assert.assertEquals(failed, 0);
	}

	private String extractParameter(final String parameters, final int index)
	{
		String parameterWithQuotes = parameters.split(",")[index];
		return removeQuotes(parameterWithQuotes);
	}

	private String removeQuotes(final String parameterWithQuotes)
	{
		return parameterWithQuotes.substring(1, parameterWithQuotes.length() - 1);
	}

	@Test
	public void testDifferentNonProductCodes() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(
				REQUEST_FOLDER + "request_for_one_line_different_non_product_codes.xml");
		String request;
		List<String> nonProductCodeAndTransactionTypes = FileTestUtils.loadFileAsStrings(
				DATA_FOLDER + "non_product_codes_and_transaction_types.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(nonProductCodeAndTransactionTypes, reportBuilder, "Esb Mapping", "PDM Product Code", "Transaction Type");

		for (String nonProductCodeAndTransactionType : nonProductCodeAndTransactionTypes)
		{
			String nonProductCode = extractParameter(nonProductCodeAndTransactionType, 1);
			String transactionType = extractParameter(nonProductCodeAndTransactionType, 2);

			request = requestTemplate.replace(NON_PRODUCT_CODE_MARKER, nonProductCode);
			request = request.replace(TRANSACTION_TYPE_MARKER, transactionType);

			failed = sendResponseAndCheckIt(request, failed, removeQuotes(nonProductCodeAndTransactionType), reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testNonProductCodes.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString()
						.replace(PASSED_TEST_MARKER, String.valueOf(nonProductCodeAndTransactionTypes.size() - failed)));
		Assert.assertEquals(failed, 0);
	}

	@Test
	public void testJournals() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_journal.xml");
		String request;
		List<String> journalInfos = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "journals.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(journalInfos, reportBuilder, "Esb Mapping", "PDM Product Code", "Transaction Type", "Media Type",
				"Fulfilment type");

		for (String journalInfo : journalInfos)
		{
			String productCode = extractParameter(journalInfo, 1);
			String transactionType = extractParameter(journalInfo, 2);
			String mediaType = extractParameter(journalInfo, 3);
			String fulfilmentType = extractParameter(journalInfo, 4);

			request = requestTemplate.replace(PRODUCT_CODE_MARKER, productCode);
			request = request.replace(TRANSACTION_TYPE_MARKER, transactionType);
			request = request.replace(MEDIA_TYPE_MARKER, mediaType);
			request = request.replace(FULFILMENT_TYPE_MARKER, fulfilmentType);

			failed = sendResponseAndCheckIt(request, failed, removeQuotes(journalInfo), reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testJournals.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString()
						.replace(PASSED_TEST_MARKER, String.valueOf(journalInfos.size() - failed)));
		System.out.println("Test variants: " + journalInfos.size());
		Assert.assertEquals(failed, 0);
	}

	@Test
	public void testExternalCompanies() throws Exception
	{
		String requestUSTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_external_companies_US.xml");
		String requestCATemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_external_companies_CA.xml");
		String requestOtherTemplate = FileTestUtils.loadFileAsString(REQUEST_FOLDER + "request_for_external_companies.xml");

		String request;
		List<String> externalCompanies = FileTestUtils.loadFileAsStrings(DATA_FOLDER + "external-companies.csv");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();
		createHeader(externalCompanies, reportBuilder, "External Company Id", "Country", "State", "City", "Postal Code",
				"Ship To State", "Ship To City", "Ship To Zip");

		for (String parameters : externalCompanies)
		{
			String externalCompanyId = extractParameter(parameters, 0);
			String country = extractParameter(parameters, 1);
			String state = extractParameter(parameters, 2);
			String city = extractParameter(parameters, 3);
			String postalCode = extractParameter(parameters, 4);
			String shipToState = extractParameter(parameters, 5);
			String shipToCity = extractParameter(parameters, 6);
			String shipToPostalCode = extractParameter(parameters, 7);

			String requestTemplate;
			switch (country)
			{
				case "US":
					requestTemplate = requestUSTemplate;
					break;
				case "CA":
					requestTemplate = requestCATemplate;
					break;
				default:
					requestTemplate = requestOtherTemplate;
					break;
			}

			request = requestTemplate.replace(EXTERNAL_COMPANY_ID, externalCompanyId);
			request = request.replace(EXTERNAL_COMPANY_COUNTRY, country);
			if (!StringUtils.isEmpty(state))
			{
				request = request.replace(EXTERNAL_COMPANY_STATE, state);
			}
			request = request.replace(EXTERNAL_COMPANY_CITY, city);
			request = request.replace(EXTERNAL_COMPANY_POSTAL_CODE, postalCode);
			request = request.replace(COUNTRY_MARKER, country);
			if (!StringUtils.isEmpty(shipToState))
			{
				request = request.replace(STATE_MARKER, shipToState);
			}
			request = request.replace(CITY_MARKER, shipToCity);
			request = request.replace(ZIP_MARKER, shipToPostalCode);

			failed = sendResponseAndCheckIt(request, failed, removeQuotes(parameters), reportBuilder);
		}

		reportBuilder.append("</body></html>");
		File file = new File("testExternalCompanies.html");
		FileUtils.writeStringToFile(file,
				reportBuilder.toString()
						.replace(PASSED_TEST_MARKER, String.valueOf(externalCompanies.size() - failed)));
		System.out.println("Test variants: " + externalCompanies.size());
		Assert.assertEquals(failed, 0);
	}


	@Test
	public void testSeveralInvoices() throws Exception
	{
		String requestTemplate = FileTestUtils.loadFileAsString(
				REQUEST_FOLDER + "request_for_several_invoices.xml");
		int failed = 0;
		StringBuilder reportBuilder = new StringBuilder();

		failed = sendResponseAndCheckIt(requestTemplate, failed, "", reportBuilder);

		Assert.assertEquals(failed, 0);
	}

	private int sendResponseAndCheckIt(final String requestTemplate, int failed, final String parameters,
			final StringBuilder reportBuilder)
			throws IOException, ParserConfigurationException, SAXException, XPathExpressionException
	{
		String requestPath = "files/" + StringUtils.join(parameters.split("\",\"")) + "request.xml";
		File requestFile = new File(requestPath);
		String responsePath = "files/" + StringUtils.join(parameters.split("\",\"")) + "response.xml";
		File responseFile = new File(responsePath);
		FileUtils.writeStringToFile(requestFile, requestTemplate);
		String htmlParameters = parametersToHtml(parameters);
		reportBuilder.append("<tr>").append(htmlParameters);
		String soapResponse = SoapTestUtils.sendSoapRequest(URL, SOAP_ACTION, requestTemplate);
		FileUtils.writeStringToFile(responseFile, soapResponse);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new StringReader(soapResponse)));
		Node serviceStatus = getXpathAndCheckNotNull(document, SERVICE_STATUS_PATH, parameters);
		if (SUCCESS_SERVICE_STATUS.equals(serviceStatus.getFirstChild().getNodeValue()))
		{
			Node isSuccessNode = getXpathAndCheckNotNull(document, IS_SUCCESS_PATH, parameters);
			Node isPartialSuccessNode = getXpathAndCheckNotNull(document, IS_PARTIAL_SUCCESS_PATH, parameters);
			if (TRUE_LITERAL.equals(isSuccessNode.getFirstChild().getNodeValue()) && TRUE_LITERAL.equals(
					isPartialSuccessNode.getFirstChild().getNodeValue()))
			{
				Double totalTaxAmount = XmlTestUtils.getDoubleValueByXpath(document, TOTAL_TAX_AMOUNT_PATH);
				reportBuilder.append("<td style=\"background-color:green\">Success</td>").append(
						"<td>").append(BigDecimal.valueOf(totalTaxAmount).setScale(2, BigDecimal.ROUND_UP).toString()).append(
						"</td>").append("<td></td>");
			}
			else
			{
				StringBuilder errorBuilder = new StringBuilder();
				NodeList errors = XmlTestUtils.getNodeListByXPath(document, ERROR_PATH);
				for (int i = 0; i < errors.getLength(); i++)
				{
					Node errorNode = errors.item(i);
					errorBuilder.append(errorNode.getFirstChild().getNodeValue());
				}
				failed++;
				reportBuilder.append("<td style=\"background-color:red\">Failed</td>").append(
						"<td></td>").append("<td>").append("Tax calculation error ").append(
						errorBuilder.toString()).append("</td>");
			}
		}
		else
		{
			failed++;
			Node serviceStatusMsg = getXpathAndCheckNotNull(document, SERVICE_MSG_PATH, parameters);
			reportBuilder.append("<td style=\"background-color:red\">Failed</td>").append("<td>")
					.append("<td>").append("Service error ").append(
					serviceStatus.getFirstChild().getNodeValue()).append(", message").append(
					serviceStatusMsg.getFirstChild().getNodeValue()).append("</td>");
		}
		reportBuilder.append("<td>").append("<a href=\"").append(requestPath).append("\">").append("request").append("</a>")
				.append("</td>").append("<td>").append("<a href=\"").append(responsePath).append("\">").append("response").append(
				"</a>").append("</td>").append("</tr>");
		return failed;
	}

	private String parametersToHtml(final String parameters)
	{
		StringBuilder parametersBuilder = new StringBuilder();
		for (String parameter : parameters.split("\",\""))
		{
			parametersBuilder.append("<td>").append(parameter).append("</td>");
		}
		return parametersBuilder.toString();
	}

	private Node getXpathAndCheckNotNull(final Document document, final String totalTaxAmountPath, final String usState)
			throws XPathExpressionException
	{
		Node node = XmlTestUtils.getNodeByXPath(document, totalTaxAmountPath);
		Assert.assertNotNull("For state " + usState, node);
		return node;
	}
}
