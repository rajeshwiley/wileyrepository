package com.wiley.fulfilmentprocess.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Rule;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.common.WileyAbstractStoreAwareChainTest;
import com.wiley.core.integration.wileycore.WileyCoreGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.request;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.github.tomakehurst.wiremock.http.RequestMethod.GET;
import static com.github.tomakehurst.wiremock.http.RequestMethod.POST;
import static java.util.Arrays.asList;


/**
 * Created by Uladzimir_Barouski on 12/9/2016.
 */
@IntegrationTest
public class WileyPinGatewayIntegrationTest extends WileyAbstractStoreAwareChainTest
{
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String MATCHED_URL = "/CGI-BIN/LANSAWEB.*";
	public static final String PIN_CODE = "pinCode";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "last name";

	@Rule
	public final WireMockRule wireMock = new WireMockRule(wireMockConfig().port(WIREMOCK_PORT));

	private static final List<String> ENABLED_SITES = asList("ags", "wel");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource
	private WileyCoreGateway wileyCoreGateway;

	@Resource
	private BaseSiteService baseSiteService;

	private void setupResponse(final RequestMethod method, final String response)
	{
		wireMock.stubFor(request(method.value(), urlMatching(MATCHED_URL)).willReturn(aResponse().withStatus(200)
				.withHeader("Content-Type", "text/html").withBody(response)));
	}

	private String createResponse(final boolean activate, final boolean success) {
		return "\n\n\n\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<message>\n\t<action>"
				+ (activate ? "ACTIVATE" : "VALIDATE")
				+ "</action>\n\t<pins>\n\n\t\t<pinresponse>\n\t\t\t<pin>PININ123456789</pin>\n\t\t\t"
				+ (success ? "<sku>9780470371671</sku>" : "<sku/>")
				+ "\n\t\t\t<responsecode>"
				+ (success ? "0000" : "0001")
				+ "</responsecode>\n\t\t\t"
				+ (success ? "<responsedescription/>" : "<responsedescription>Invalid PIN</responsedescription>\n")
				+ "\n\t\t</pinresponse>\n\n\n\t</pins>\n</message>";
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		BaseSiteModel siteModel = baseSiteService.getBaseSiteForUID(site);

		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("activatePin", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				setupResponse(POST, createResponse(true, true));
				wileyCoreGateway.activatePin(PIN_CODE, FIRST_NAME, LAST_NAME, siteModel);
				return null;
			}
		});

		funcMap.put("validatePin", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				setupResponse(GET, createResponse(false, true));
				wileyCoreGateway.validatePin(PIN_CODE, siteModel);
				return null;
			}
		});
		return funcMap;
	}
}
