package com.wiley.integration.mpgs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.utils.HandshakeUtils;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


@IntegrationTest
public class WileyMPGSPaymentGatewayAuthorizeIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String TEST_ORDER_ID = "TEST_ORDER_ID1";
	private static final String TEST_ORDER_URL_PART = "/order/";
	private static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID1";
	private static final String TEST_TRANSACTION_URL_PART = "/transaction/";
	private static final String TOKEN = "4508753994831019";
	private static final String INVALID_TOKEN = "40120074941600260";
	private static final String TEST_CURRENCY = "USD";
	private static final String TEST_CITY = "New Jersey";
	private static final String TEST_COUNTRY = "USA";
	private static final String TEST_POSTAL_CODE = "07030";
	private static final String TEST_STATE = "US-NJ";
	private static final String TEST_STREET = "111 River St";
	private static final BigDecimal TEST_AMOUNT = new BigDecimal("100000");
	private static final String STATUS_APPROVED = "APPROVED";
	private static final String SUCCESS_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_request_success.json";
	private static final String SUCCCESS_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_response_success.json";
	private static final String ERROR_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_request_error.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_response_error.json";
	private static final String FAILURE_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_request_failure.json";
	private static final String FAILURE_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_response_failure.json";
	private static final String UNKNOWN_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_request_unknown.json";
	private static final String UNKNOWN_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/authorization_response_unknown.json";
	private static final String TEST_AUTHORIZATION_REQUEST_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST"
					+ TEST_ORDER_URL_PART
					+ TEST_ORDER_ID
					+ TEST_TRANSACTION_URL_PART
					+ TEST_TRANSACTION_ID;
	public static final String TIMED_OUT = "TIMED_OUT";
	public static final String TEST_TRANSACTION_REFERENCE = "0257000000000001";

	@Resource
	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Resource
	private RestTemplate mpgsPaymentGatewayClientRestTemplate;

	private WileyAuthorizationRequest request;

	@Before
	public void setUp() throws Exception
	{
		final HttpComponentsClientHttpRequestFactory requestFactory =
				(HttpComponentsClientHttpRequestFactory) mpgsPaymentGatewayClientRestTemplate.getRequestFactory();

		if (requestFactory != null)
		{
			HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
		}

		request = createWileyAuthorizationRequest();
	}

	@Test
	public void testAuthorizeStatusSuccess() throws Exception
	{

		wireMock.stubFor(put(urlMatching(TEST_AUTHORIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCCESS_RESPONSE_JSON_FILE_PATH))
				));

		WileyAuthorizationResponse response = mpgsPaymentGateway.authorize(request);

		assertEquals(TransactionStatus.ACCEPTED.name(), response.getStatus());
		assertEquals(STATUS_APPROVED, response.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, response.getTransactionId());
		assertEquals(TEST_CURRENCY, response.getCurrency());
		assertEquals(TEST_AMOUNT, response.getTotalAmount());
		assertNotNull(response.getTransactionCreatedTime());
		assertEquals(TOKEN, response.getToken());
	}

	@Test(expected = HttpClientErrorException.class)
	public void testAuthorizeStatusError() throws Exception
	{

		wireMock.stubFor(put(urlMatching(TEST_AUTHORIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(ERROR_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_BAD_REQUEST)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))
				));
		request.setToken(INVALID_TOKEN);

		WileyAuthorizationResponse response = mpgsPaymentGateway.authorize(request);
	}

	@Test
	public void testAuthorizeStatusFailure() throws Exception
	{

		wireMock.stubFor(put(urlMatching(TEST_AUTHORIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(FAILURE_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(FAILURE_RESPONSE_JSON_FILE_PATH))
				));

		WileyAuthorizationResponse response = mpgsPaymentGateway.authorize(request);

		assertEquals(WileyMPGSConstants.RETURN_STATUS_FAILURE, response.getStatus());
		assertEquals(TIMED_OUT, response.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, response.getTransactionId());
		assertEquals(TEST_CURRENCY, response.getCurrency());
		assertEquals(TEST_AMOUNT, response.getTotalAmount());
		assertNotNull(response.getTransactionCreatedTime());
		assertEquals(TOKEN, response.getToken());
	}

	@Test
	public void testAuthorizeStatusUnknown() throws Exception
	{

		wireMock.stubFor(put(urlMatching(TEST_AUTHORIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(UNKNOWN_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(UNKNOWN_RESPONSE_JSON_FILE_PATH))
				));

		WileyAuthorizationResponse response = mpgsPaymentGateway.authorize(request);

		assertEquals(WileyMPGSConstants.RETURN_STATUS_UNKNOWN, response.getStatus());
		assertEquals(TIMED_OUT, response.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, response.getTransactionId());
		assertEquals(TEST_CURRENCY, response.getCurrency());
		assertEquals(TEST_AMOUNT, response.getTotalAmount());
		assertNotNull(response.getTransactionCreatedTime());
		assertEquals(TOKEN, response.getToken());
	}

	private WileyAuthorizationRequest createWileyAuthorizationRequest()
	{
		WileyAuthorizationRequest request = new WileyAuthorizationRequest();
		request.setPaymentProvider(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		request.setApiOperation(WileyMPGSConstants.API_OPERATION_AUTHORIZE);
		request.setUrlPath(WIRE_MOCK_URL + TEST_AUTHORIZATION_REQUEST_PATH);
		request.setCurrency(TEST_CURRENCY);
		request.setTotalAmount(TEST_AMOUNT);
		request.setBillingStreet1(TEST_STREET);
		request.setBillingStreet2(TEST_STREET);
		request.setBillingCity(TEST_CITY);
		request.setStateProvince(TEST_STATE);
		request.setBillingPostalCode(TEST_POSTAL_CODE);
		request.setBillingCountry(TEST_COUNTRY);
		request.setToken(TOKEN);
		request.setTransactionReference(TEST_TRANSACTION_REFERENCE);
		return request;
	}
}
