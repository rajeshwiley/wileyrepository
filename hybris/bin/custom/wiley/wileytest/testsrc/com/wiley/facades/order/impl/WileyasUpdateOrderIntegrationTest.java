package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.BaseProcessIntegrationTest;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.as.order.WileyasUpdateOrderFacade;
import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import com.wiley.facades.wiley.order.dto.OrderUpdateRequestDTO;


@IntegrationTest
public class WileyasUpdateOrderIntegrationTest extends BaseProcessIntegrationTest
{
	public static final String EXTERNAL_TAXES_CALCULATE_RESPONSE_MAPPING =
			"/wileytest/integration/common/mappings/taxes-calculate-200.json";
	private static final String IMPEX_PATH = "/wileytest/import/order/WileyasUpdateOrderIntegrationTest";

	private static final String COMMON_DATA_PATH = IMPEX_PATH + "/common.impex";
	private static final String EDIT_ORDER_DATA_PATH = IMPEX_PATH + "/edit-order.impex";

	private static final String BASE_SITE_ID = "asSite";
	private static final String CUSTOMER_ID = "alm_id1";
	private static final String AGENT_ID = "asagent1";
	private static final String AGENT_PASSWORD = "1234";
	private static final String ASM_EDIT_ORDER_CODE = "asAsmEditTestOrder";
	private static final String EDIT_ORDER_MPGS_SESSION_ID = "asEditTestMPGSSessionId";
	private static final String AU_ISO_CODE = "AU";
	private static final String US_ISO_CODE = "US";
	private static final String FIRST_NAME = "First Name";
	private static final String LAST_NAME = "Last Name";
	private static final String LINE1 = "Line 1";
	private static final String CITY = "City";
	private static final String POSTAL_CODE = "223344";
	private static final String PHONE = "2233445";
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String REFUND_PAYMENT = "refundPayment";
	private static final String RETURN_CODE = "OK";
	private static final int PAYMENT_TRANSACTIONS_SIZE_1 = 1;
	private static final int PAYMENT_TRANSACTIONS_SIZE_2 = 2;
	private static final int PAYMENT_TRANSACTIONS_SIZE_3 = 3;
	private static final int PAYMENT_TRANSACTIONS_SIZE_5 = 5;
	private static final int PAYMENT_ENTRIES_SIZE_2 = 2;
	private static final int PAYMENT_ENTRIES_SIZE_3 = 3;
	private static final Double TOTAL_PRICE = 100.0;
	private static final Double TOTAL_PRICE_AFTER_FIRST_EDIT_ORDER = 200.0;
	private static final Double TOTAL_PRICE_AFTER_SECOND_EDIT_ORDER = 100.0;

	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private WileyAssistedServiceFacade wileyAssistedServiceFacade;
	@Resource
	private WileyasUpdateOrderFacade wileyasUpdateOrderFacade;
	@Resource
	private WileyasOrderFacade wileyasOrderFacade;
	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;
	@Resource
	private UserService userService;

	@Before
	public void setUp() throws Exception
	{
		final StubMapping stubMapping = wireMock.createStubMappingFrom(EXTERNAL_TAXES_CALCULATE_RESPONSE_MAPPING);
		wireMock.addStubMapping(stubMapping);
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(BASE_SITE_ID, false);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void testUpdateOrderByAgentIfAddressCurrencyAndCreditCardChanged() throws Exception
	{
		final OrderModel orderModel = wileyasOrderFacade.getOrderModel(ASM_EDIT_ORDER_CODE);
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE);

		setUpWiremockMappings(false, AUJW_RESPONSE_PATH);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_1, orderModel.getPaymentTransactions().size());
		testUpdateOrder(orderModel, AU_ISO_CODE, false);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_3, orderModel.getPaymentTransactions().size());
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE_AFTER_FIRST_EDIT_ORDER);
		assertTokenizationTransaction(orderModel.getPaymentTransactions().get(1));
		assertAutorizationTransaction(orderModel.getPaymentTransactions().get(2));

		setUpWiremockMappings(false, US_RESPONSE_PATH);
		testUpdateOrder(orderModel, US_ISO_CODE, false);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_5, orderModel.getPaymentTransactions().size());
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE_AFTER_SECOND_EDIT_ORDER);
		assertTokenizationTransaction(orderModel.getPaymentTransactions().get(3));
		assertAutorizationTransaction(orderModel.getPaymentTransactions().get(4));
	}

	@Test
	public void testUpdateOrderByAgentIfAddressCurrencyChangedAndKeepCurrentPaymentMethod() throws Exception
	{
		final OrderModel orderModel = wileyasOrderFacade.getOrderModel(ASM_EDIT_ORDER_CODE);
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE);

		setUpWiremockMappings(true, AUJW_RESPONSE_PATH);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_1, orderModel.getPaymentTransactions().size());
		testUpdateOrder(orderModel, AU_ISO_CODE, true);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_2, orderModel.getPaymentTransactions().size());
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE_AFTER_FIRST_EDIT_ORDER);
		assertAutorizationTransaction(orderModel.getPaymentTransactions().get(1));

		setUpWiremockMappings(true, US_RESPONSE_PATH);
		testUpdateOrder(orderModel, US_ISO_CODE, true);
		assertEquals(PAYMENT_TRANSACTIONS_SIZE_3, orderModel.getPaymentTransactions().size());
		assertEquals(orderModel.getTotalPrice(), TOTAL_PRICE_AFTER_SECOND_EDIT_ORDER);
		assertAutorizationTransaction(orderModel.getPaymentTransactions().get(2));
	}

	private void assertTokenizationTransaction(final PaymentTransactionModel transaction)
	{
		final List<PaymentTransactionEntryModel> entries = transaction.getEntries();
		assertEquals(PAYMENT_ENTRIES_SIZE_3, entries.size());
		PaymentTransactionEntryModel retriveSesiion = entries.get(0);
		assertEquals(PaymentTransactionType.RETRIEVE_SESSION, retriveSesiion.getType());
		assertEquals(TransactionStatus.ACCEPTED.name(), retriveSesiion.getTransactionStatus());

		PaymentTransactionEntryModel verify = entries.get(1);
		assertEquals(PaymentTransactionType.VERIFY, verify.getType());
		assertEquals(TransactionStatus.ACCEPTED.name(), verify.getTransactionStatus());

		PaymentTransactionEntryModel tokenization = entries.get(2);
		assertEquals(PaymentTransactionType.TOKENIZATION, tokenization.getType());
		assertEquals(TransactionStatus.ACCEPTED.name(), tokenization.getTransactionStatus());
	}

	private void assertAutorizationTransaction(final PaymentTransactionModel transaction)
	{
		final List<PaymentTransactionEntryModel> entries = transaction.getEntries();
		assertEquals(PAYMENT_ENTRIES_SIZE_2, entries.size());
		PaymentTransactionEntryModel authorization = entries.get(0);
		assertEquals(PaymentTransactionType.AUTHORIZATION, authorization.getType());
		assertEquals(TransactionStatus.ACCEPTED.name(), authorization.getTransactionStatus());

		PaymentTransactionEntryModel capture = entries.get(1);
		assertEquals(PaymentTransactionType.CAPTURE, capture.getType());
		assertEquals(TransactionStatus.ACCEPTED.name(), capture.getTransactionStatus());
	}

	private void testUpdateOrder(final OrderModel orderModel, final String isoCode, boolean keepCurrent)
			throws AssistedServiceException
	{
		loginAsCsAgent();

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(ASM_EDIT_ORDER_CODE, isoCode, keepCurrent);
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		assertResult(result);
		assertBusinessProcess(orderModel);
	}

	private void assertResult(final OrderData result)
	{
		assertNotNull(result);
		assertEquals(result.getCode(), ASM_EDIT_ORDER_CODE);
	}

	private void assertBusinessProcess(final OrderModel orderModel)
	{
		Optional<WileyOrderProcessModel> orderProcess = wileyOrderFulfilmentProcessService.getOrderProcessActiveFor(orderModel);
		final WileyOrderProcessModel wileyOrderProcessModel = orderProcess.get();
		assertNotNull(wileyOrderProcessModel);
		waitForProcessCompletion(wileyOrderProcessModel);
		assertProcessState(wileyOrderProcessModel, ProcessState.SUCCEEDED);
		assertRefundProcess(wileyOrderProcessModel);
	}

	private void assertRefundProcess(final WileyOrderProcessModel wileyOrderProcessModel)
	{
		final ProcessTaskLogModel log = wileyOrderProcessModel.getTaskLogs().stream()
				.filter(l -> l.getActionId().equals(REFUND_PAYMENT))
				.findAny().orElse(null);
		assertNotNull(log);
		assertEquals(RETURN_CODE, log.getReturnCode());
	}

	private void loginAsCsAgent() throws AssistedServiceException
	{
		wileyAssistedServiceFacade.launchAssistedServiceMode();
		wileyAssistedServiceFacade.loginAssistedServiceAgent(AGENT_ID, AGENT_PASSWORD);
		wileyAssistedServiceFacade.emulateCustomer(CUSTOMER_ID, null);
	}

	private OrderUpdateRequestDTO getOrderUpdateRequestDTO(final String orderCode, final String countryIso, boolean keepCurrent)
	{
		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(orderCode);
		AddressData paymentAddressData = setUpAddressData(countryIso);
		updateRequestDTO.setPaymentAddress(paymentAddressData);
		if (!keepCurrent)
		{
			updateRequestDTO.setPaymentMode(PaymentModeEnum.CARD);
		}
		updateRequestDTO.setSessionId(EDIT_ORDER_MPGS_SESSION_ID);
		return updateRequestDTO;
	}

	private AddressData setUpAddressData(final String countryIso)
	{
		AddressData paymentAddressData = new AddressData();
		CountryData countryData = new CountryData();
		countryData.setIsocode(countryIso);
		paymentAddressData.setCountry(countryData);
		paymentAddressData.setFirstName(FIRST_NAME);
		paymentAddressData.setLastName(LAST_NAME);
		paymentAddressData.setLine1(LINE1);
		paymentAddressData.setTown(CITY);
		paymentAddressData.setPostalCode(POSTAL_CODE);
		paymentAddressData.setPhone(PHONE);
		return paymentAddressData;
	}
}
