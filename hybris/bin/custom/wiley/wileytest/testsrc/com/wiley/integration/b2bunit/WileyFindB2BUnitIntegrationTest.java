package com.wiley.integration.b2bunit;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.lang.LocaleUtils;
import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;
import com.wiley.core.b2b.unit.service.exception.B2BUnitNotFoundException;
import com.wiley.core.b2b.unit.service.exception.FindB2BUnitExternalSystemException;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static java.util.Arrays.asList;


@IntegrationTest
public class WileyFindB2BUnitIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyFindB2BUnitIntegrationTest.class);

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/Wileyb2bFindB2BUnitIntegrationTest/";
	private static final String SAP_ACCOUNT_NUMBER = "sapAccountNumber45";

	private static final List<String> ENABLED_SITES = asList("wileyb2b");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "ags", "wel");

	@Resource
	private WileyB2BUnitExternalService wileyB2BUnitExternalService;
	@Resource
	private HttpComponentsClientHttpRequestFactory b2bUnitRequestFactory;
	@Resource
	private BaseSiteService baseSiteService;


	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void shouldReturnB2BUnit() throws Exception
	{
		// Given
		setupWireMockSuccessfulResponse("response_b2bUnitFound.json");
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then
		verifyB2BUnitFields(b2BUnit);
		verifyB2BUnitAddresses(b2BUnit);
	}

	@Test(expected = FindB2BUnitExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus400BadRequest() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_BAD_REQUEST);
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	@Test(expected = FindB2BUnitExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus401Unauthorized() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_UNAUTHORIZED);
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	@Test(expected = FindB2BUnitExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus403Forbidden() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_FORBIDDEN);
		//When
		wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	@Test(expected = B2BUnitNotFoundException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus404NotFound() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_NOT_FOUND);
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	@Test(expected = FindB2BUnitExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus500InternalError() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	@Test(expected = FindB2BUnitExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus503ServiceUnavailable() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_SERVICE_UNAVAILABLE);
		//When
		final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER, getSite());
		//Then FindB2BUnitExternalSystemException is thrown
	}

	private void setupWireMockSuccessfulResponse(final String responseBodyFileName)
			throws Exception
	{
		wireMock.
				stubFor(get(urlMatching("/b2b/accounts/" + SAP_ACCOUNT_NUMBER))
						.withHeader("Content-Type", matching("application/json"))
						.withHeader("Accept", matching("application/json"))
						.withHeader("Src", equalTo("hybris"))
						.withHeader("TransactionId", matching(".*"))

						.willReturn(aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", "application/json")
								.withBody(readFromFile(TEST_REQUESTS_PATH + responseBodyFileName))));
	}

	private void setupWireMockErrorResponse(final int httpStatus)
	{
		final String urlRegex = "/b2b/accounts/" + SAP_ACCOUNT_NUMBER;
		setupWireMockGetErrorResponse(urlRegex, httpStatus);
	}

	private void verifyB2BUnitFields(final B2BUnitModel b2BUnit)
	{
		assertEquals("test_sap_account_number", b2BUnit.getUid());
		assertEquals("test_sap_account_number", b2BUnit.getSapAccountNumber());
		assertEquals("test_account_Ecid_Number", b2BUnit.getAccountEcidNumber());
		assertEquals("test_salesRep_Contact_Name", b2BUnit.getSapSalesRepContactName());
		assertEquals("sales@contact.email", b2BUnit.getSapSalesRepContactEmail());
		assertEquals("test_sales_Rep_Contact_Phone", b2BUnit.getSapSalesRepContactPhone());
		assertEquals("test_b2b_unit_name_en", b2BUnit.getLocName(LocaleUtils.toLocale("en")));
		assertEquals("test_b2b_unit_name_fr", b2BUnit.getLocName(LocaleUtils.toLocale("fr")));
	}

	private void verifyB2BUnitAddresses(final B2BUnitModel b2BUnit)
	{
		verifyB2BUnitAddressFields("test_billing_", b2BUnit.getBillingAddress(), false, true);
		verifyB2BUnitAddressFields("test_shipping1_", b2BUnit.getShippingAddress(), true, false);

		List<AddressModel> addresses = new ArrayList<>(b2BUnit.getAddresses());

		final int expectedNumberOfAddresses = 3;
		assertEquals(expectedNumberOfAddresses, addresses.size());
		verifyB2BUnitAddressFields("test_shipping1_", addresses.get(0), true, false);
		verifyB2BUnitAddressFields("test_shipping2_", addresses.get(1), true, false);
		verifyB2BUnitAddressFields("test_billing_", addresses.get(2), false, true);
	}

	private void verifyB2BUnitAddressFields(final String prefix, final AddressModel addressModel,
			final boolean isShippingAddress, final boolean isBillingAddress)
	{
		assertEquals(prefix + "external_address_id", addressModel.getExternalId());
		assertEquals(prefix + "address_postcode", addressModel.getPostalcode());
		assertEquals(prefix + "address_city", addressModel.getTown());
		assertEquals(prefix + "address_line1", addressModel.getStreetname());
		assertEquals(prefix + "address_line2", addressModel.getStreetnumber());
		assertEquals(prefix + "address_firstName", addressModel.getFirstname());
		assertEquals(prefix + "address_lastName", addressModel.getLastname());
		assertEquals(prefix + "address_phoneNumber", addressModel.getPhone1());
		assertEquals("US", addressModel.getCountry().getIsocode());
		assertEquals("US-CA", addressModel.getRegion().getIsocode());
		assertEquals(isShippingAddress, addressModel.getShippingAddress().booleanValue());
		assertEquals(isBillingAddress, addressModel.getBillingAddress().booleanValue());
	}

	private BaseSiteModel getSite()
	{
		return baseSiteService.getBaseSiteForUID("wileyb2b");
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return b2bUnitRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("updateCustomer", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				setupWireMockSuccessfulResponse("response_b2bUnitFound.json");
				//When
				final B2BUnitModel b2BUnit = wileyB2BUnitExternalService.find(SAP_ACCOUNT_NUMBER,
						baseSiteService.getBaseSiteForUID(site));
				return null;
			}
		});
		return funcMap;
	}
}
