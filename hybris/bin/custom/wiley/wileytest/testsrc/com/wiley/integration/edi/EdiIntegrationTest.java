package com.wiley.integration.edi;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.wiley.core.common.WileyAbstractFtpMockTest;
import com.wiley.core.integration.edi.WileyEdiExportGateway;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;




/**
 * Created by Uladzimir_Barouski on 1/26/2017.
 */
@IntegrationTest
public class EdiIntegrationTest extends WileyAbstractFtpMockTest
{
	public static final String EDI_SUFFIX = ".new";
	private static final String EDI_FILE =
			"<PurchaseOrder>"
					+ "<BillToCustomer/>"
					+ "<ShipToCustomer/>"
					+ "<CurrencyCode>USD</CurrencyCode>"
					+ "<OrderType>CC</OrderType>"
					+ "<ShippingChargeAmount>44.00</ShippingChargeAmount>"
					+ "<ShipMethod>INTL</ShipMethod>"
					+ "<PromotionCode/>"
					+ "<EmailAddress>tepatel@wiley.com</EmailAddress>"
					+ "<SalespersonCode/>"
					+ "<MarketOutlet/>"
					+ "<HybrisAuthorizationCode/>"
					+ "<ItemsStructure>"
					+ "<PurchaseOrderNumber>HBtestEdiOrder</PurchaseOrderNumber>"
					+ "<ProductId>111111111111</ProductId>"
					+ "<Quantity>1</Quantity>"
					+ "<Price>0.00</Price>"
					+ "<Discount>0.00</Discount>"
					+ "<TaxAmount>0.00</TaxAmount>"
					+ "<SalesModel/>"
					+ "</ItemsStructure>"
					+ "</PurchaseOrder>";
	public static final String PAYMENT_TYPE = "PayPal";
	public static final String BASE_STORE_ID = "wel";

	@Resource
	private DefaultSftpSessionFactory sftpSessionFactory;
	@Resource
	private WileyEdiExportGateway wileyEdiExportGateway;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Test
	public void testEdi() throws Exception
	{
		//Given
		importCsv("/wileytest/import/order/EdiGatewayIntegrationTest/testdata.impex", DEFAULT_ENCODING);
		String selectOrdersReadyForExportQuery =
				"SELECT {PK} "
						+ "FROM {WileyExportProcess} WHERE {code}  = 'edi_test_export_process'";
		final FlexibleSearchQuery query = new FlexibleSearchQuery(selectOrdersReadyForExportQuery);
		List<WileyExportProcessModel> exportProcessList = flexibleSearchService.<WileyExportProcessModel> search(query)
				.getResult();
		//When
		wileyEdiExportGateway.exportOrders(exportProcessList, baseStoreService.getBaseStoreForUid(BASE_STORE_ID),
				LocalDateTime.of(2017, 01, 27, 23, 59), PAYMENT_TYPE, "testMailList");

		//Then
		verifySftp();
		verifyCoreSftp();
	}

	private void verifySftp() throws IOException, JSchException, SftpException
	{
		JSch jsch = new JSch();
		Session session = jsch.getSession(Config.getParameter("ftp.username"), Config.getParameter("ftp.host"),
				Integer.valueOf(Config.getParameter("sftp.port")));
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();

		Channel channel = session.openChannel("sftp");
		ChannelSftp sftpChannel = (ChannelSftp) channel;
		sftpChannel.connect();
		String home = sftpFileSystem.getRootDirectories().iterator().next().toString();
		List<Path> files = Files.list(sftpFileSystem.getPath(home)).collect(Collectors.toList());
		List<Path> ediFileList = files.stream()
				.filter(f -> f.getFileName().toString().endsWith(EDI_SUFFIX))
				.collect(Collectors.toList());
		assertNotNull(ediFileList);
		assertEquals(1, ediFileList.size());
		Path ediFile = ediFileList.get(0);
		assertTrue(Files.exists(ediFile));
		assertFalse(Files.isDirectory(ediFile));
		String fileText = IOUtils.toString(Files.readAllBytes(ediFile));
		assertTrue(fileText.contains(EDI_FILE));
		sftpChannel.exit();
		session.disconnect();
		if (channel != null) {
			channel.disconnect();
		}
		if (session != null) {
			session.disconnect();
		}
	}

	private void verifyCoreSftp() throws IOException, JSchException, SftpException
	{
		JSch jsch = new JSch();
		Session session = jsch.getSession(Config.getParameter("ftp.username"), Config.getParameter("ftp.host"),
				Integer.valueOf(Config.getParameter("sftp.core.port")));
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();

		Channel channel = session.openChannel("sftp");
		ChannelSftp sftpChannel = (ChannelSftp) channel;
		sftpChannel.connect();
		String home = coreSftpFileSystem.getRootDirectories().iterator().next().toString();
		List<Path> files = Files.list(coreSftpFileSystem.getPath(home)).collect(Collectors.toList());
		List<Path> ediFileList = files.stream()
				.filter(f -> f.getFileName().toString().endsWith(EDI_SUFFIX))
				.collect(Collectors.toList());
		assertNotNull(ediFileList);
		assertEquals(1, ediFileList.size());
		Path ediFile = ediFileList.get(0);
		assertTrue(Files.exists(ediFile));
		assertFalse(Files.isDirectory(ediFile));
		String fileText = IOUtils.toString(Files.readAllBytes(ediFile));
		assertTrue(fileText.contains(EDI_FILE));
		sftpChannel.exit();
		session.disconnect();
		if (channel != null) {
			channel.disconnect();
		}
		if (session != null) {
			session.disconnect();
		}
	}
}
