package com.wiley.facades.as.address.validation;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.facades.as.address.WileyasAddressVerificationFacade;


@IntegrationTest
public class WileyasAddressVerificationServiceImplIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final String COUNTRY_AU = "AU";
	private static final String REGION_EMPTY = StringUtils.EMPTY;
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileyasAddressVerificationServiceImplIntegrationTest";
	private static final String REQUEST = "/mapping_suggest_AU.json";
	private static final String RESPONSE = "/response_suggest_AU_200.json";
	private static final String RESPONSE_WITH_NOT_SUPPORTED_UNICODE_CHARACTERS =
			"/response_suggest_AU_200_with_not_supported_unicode.json";
	private static final int SUGGESTED_ADDRESS_COUNT_1 = 1;
	private static final int SUGGESTED_ADDRESS_COUNT_3 = 3;

	@Resource
	private WileyasAddressVerificationFacade wileyasAddressVerificationFacade;

	@Resource
	private RestTemplate addressDoctorApigeeEdgeOAuth2RestTemplate;

	@Test
	public void shouldGetSuggestedAddress() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + REQUEST,
				RESOURCE_PREFIX + RESPONSE
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_AU, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertFalse(decision.isSkipAddressDoctor());
		assertTrue(decision.isHasSuggestions());
		assertEquals(SUGGESTED_ADDRESS_COUNT_3, decision.getSuggestedAddresses().size());
	}

	@Test
	public void shouldNotGetSuggestedAddressWithNotSupportedUnicodeCharacters() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + REQUEST,
				RESOURCE_PREFIX + RESPONSE_WITH_NOT_SUPPORTED_UNICODE_CHARACTERS
		);
		wireMock.addStubMapping(stubMapping);
		final AddressData addressData = prepareDefaultAddressData(COUNTRY_AU, REGION_EMPTY);

		// when
		final AddressVerificationResult<AddressVerificationDecision> decision =
				wileyasAddressVerificationFacade.verifyAddressData(addressData);

		//then
		assertNotNull(decision);
		assertFalse(decision.isSkipAddressDoctor());
		assertTrue(decision.isHasSuggestions());
		assertEquals(SUGGESTED_ADDRESS_COUNT_1, decision.getSuggestedAddresses().size());
	}

	protected AddressData prepareDefaultAddressData(final String countryIso, final String regionIso)
	{
		AddressData addressData = new AddressData();
		CountryData countryData = new CountryData();
		countryData.setIsocode(countryIso);
		addressData.setCountry(countryData);
		RegionData regionData = new RegionData();
		regionData.setIsocodeShort(regionIso);
		addressData.setRegion(regionData);
		addressData.setLine1("address line-1");
		addressData.setLine2("address line-2");
		addressData.setTown("Test Town");
		addressData.setPostalCode("122333");
		return addressData;
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) addressDoctorApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}
