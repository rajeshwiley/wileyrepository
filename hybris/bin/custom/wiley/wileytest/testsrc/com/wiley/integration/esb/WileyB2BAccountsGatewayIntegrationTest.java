package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.b2baccounts.WileyB2BAccountsGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;
import static java.util.Arrays.asList;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;


/**
 * Created by Raman_Hancharou on 8/11/2016.
 */
@IntegrationTest
public class WileyB2BAccountsGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyB2BAccountsGatewayIntegrationTest.class);

	private static final String WCOM_PRODUCT = "WCOM_PRODUCT";
	private static final String SAP_ACCOUNT_NUMBER = "testSapAccountNumber";

	// test outbound gateway uri is overwritten via configs/common/local_tenant_junit.properties
	private static final String CHECK_LICENSE_PATH = "/b2b/accounts/testSapAccountNumber/ownership\\?isbn={isbn}";

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/CheckLicenseGatewayIntegrationTest/";

	private static final List<String> ENABLED_SITES = asList("wileyb2b");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "ags", "wel");

	@Resource
	private WileyB2BAccountsGateway wileyB2BAccountsGateway;

	@Resource
	private ProductService productService;

	private ProductModel productModel;

	private B2BCustomerModel customer;
	private B2BUnitModel b2bUnit;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Before
	public void before() throws Exception
	{
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

		setUpOauth(wireMock);
		baseSiteService.setCurrentBaseSite("wileyb2b", true);
		productModel = productService.getProductForCode(WCOM_PRODUCT);
		customer = new B2BCustomerModel();
		b2bUnit = new B2BUnitModel();
		b2bUnit.setSapAccountNumber(SAP_ACCOUNT_NUMBER);
		customer.setDefaultB2BUnit(b2bUnit);
	}

	@Test
	public void testCheckLicenseSuccessTrue() throws Exception
	{
		// Given
		mockCheckLicense(productModel, "response_check_license_success_true.json", HttpStatus.SC_OK);

		// When
		Boolean response = wileyB2BAccountsGateway.checkLicense(productModel, customer);

		// Then
		assertEquals(Boolean.TRUE, response);
	}

	@Test
	public void testCheckLicenseSuccessFalse() throws Exception
	{
		// Given
		mockCheckLicense(productModel, "response_check_license_success_false.json", HttpStatus.SC_OK);

		// When
		Boolean response = wileyB2BAccountsGateway.checkLicense(productModel, customer);

		// Then
		assertEquals(Boolean.FALSE, response);
	}

	@Test
	public void testCheckLicenseError404() throws Exception
	{
		// Given
		mockCheckLicense(productModel, "response_check_license_error_404.json", HttpStatus.SC_NOT_FOUND);

		// When
		try
		{
			wileyB2BAccountsGateway.checkLicense(productModel, customer);
			fail("Expected ExternalSystemException");
		}
		catch (ExternalSystemException e)
		{
			//success
		}
		catch (Throwable t)
		{
			fail("Unexpected exception type. Expected ExternalSystemException");
		}
	}

	private void mockCheckLicense(final ProductModel productModel, final String responseFileName,
			final int expectedResponseStatus) throws Exception
	{
		wireMock.
				stubFor(get(urlMatching(CHECK_LICENSE_PATH.replace("{isbn}", productModel.getIsbn())))
						.withHeader("Authorization", WireMock.equalTo("bearer bd55e2ce-92b0-46a2-a8f4-82a69440b586"))
						.willReturn(
								aResponse()
										.withStatus(expectedResponseStatus)
										.withHeader("Content-Type", "application/json")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + responseFileName))
						));
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("checkLicense", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				mockCheckLicense(productModel, "response_check_license_success_true.json", HttpStatus.SC_OK);
				wileyB2BAccountsGateway.checkLicense(productModel, customer);
				return null;
			}
		});
		return funcMap;
	}
}
