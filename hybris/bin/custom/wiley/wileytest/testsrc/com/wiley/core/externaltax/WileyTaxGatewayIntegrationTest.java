package com.wiley.core.externaltax;

import de.hybris.bootstrap.annotations.IntegrationTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.externaltax.dto.TaxAddressDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseEntryDto;
import com.wiley.core.externaltax.dto.TaxValueDto;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


/**
 * Created by Maksim_Kozich on 09.08.2018.
 */
@IntegrationTest
public class WileyTaxGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileyTaxGatewayIntegrationTest";
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String SUCCESS_REQUEST_BODY_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/calculate_tax_request_body_success.json";
	private static final String SUCCESS_REQUEST_BODY_MISSING_OPTIONAL_FIELDS_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/calculate_tax_request_body_missing_optional_fields_success.json";
	private static final String SUCCESS_RESPONSE_BODY_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/calculate_tax_response_body_success.json";
	private static final String CALCULATE_TAXES_PATH = "/es/v1/taxes/calculate";

	@Resource
	private WileyTaxGateway wileyTaxGateway;

	@Resource
	private RestTemplate taxApigeeEdgeOAuth2RestTemplate;

	@Test
	public void testCalculateTax() throws Exception
	{
		// given
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_BODY_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCESS_RESPONSE_BODY_JSON_FILE_PATH))
				));

		final TaxCalculationRequestDto taxCalculationRequestDto = createTaxCalculationRequestDto();

		// when
		final TaxCalculationResponseDto dto = wileyTaxGateway.calculateTax(taxCalculationRequestDto);

		// then
		verifyExpectedResponse(dto);
	}

	private void verifyExpectedResponse(final TaxCalculationResponseDto dto)
	{
		verifyResponseDto(dto);
	}

	@Test
	public void testCalculateTaxMissingOptionalFieldsShouldNotBePresentInRequest() throws Exception
	{
		// given
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_BODY_MISSING_OPTIONAL_FIELDS_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCESS_RESPONSE_BODY_JSON_FILE_PATH))
				));

		final TaxCalculationRequestDto taxCalculationRequestDto = createTaxCalculationRequestDto();
		taxCalculationRequestDto.setDeliveryAddress(null);
		taxCalculationRequestDto.getPaymentAddress().setPostcode(null);
		taxCalculationRequestDto.getPaymentAddress().setCountry(null);
		taxCalculationRequestDto.getPaymentAddress().setState(null);
		taxCalculationRequestDto.getPaymentAddress().setCity(null);

		// when
		final TaxCalculationResponseDto dto = wileyTaxGateway.calculateTax(taxCalculationRequestDto);

		// then
		verifyResponseDto(dto);
	}

	private void verifyResponseDto(final TaxCalculationResponseDto dto)
	{
		assertNotNull(dto);
		assertNotNull(dto.getEntries());
		assertEquals(2, dto.getEntries().size());

		TaxCalculationResponseEntryDto firstEntry = dto.getEntries().get(0);
		assertNotNull(firstEntry);
		assertEquals(Integer.valueOf(0), firstEntry.getId());
		assertNotNull(firstEntry.getTaxes());

		assertEquals(1, firstEntry.getTaxes().size());

		TaxValueDto firstEntryTax = firstEntry.getTaxes().get(0);
		assertEquals("XXX", firstEntryTax.getCode());
		assertEquals(Double.valueOf(1.0), firstEntryTax.getValue());

		TaxCalculationResponseEntryDto secondEntry = dto.getEntries().get(1);
		assertNotNull(secondEntry);
		assertEquals(Integer.valueOf(1), secondEntry.getId());
		assertNotNull(secondEntry.getTaxes());

		assertEquals(2, secondEntry.getTaxes().size());

		TaxValueDto secondEntryFirstTax = secondEntry.getTaxes().get(0);
		assertEquals("YYY", secondEntryFirstTax.getCode());
		assertEquals(Double.valueOf(2.1), secondEntryFirstTax.getValue());

		TaxValueDto secondEntrySecondTax = secondEntry.getTaxes().get(1);
		assertEquals("ZZZ", secondEntrySecondTax.getCode());
		assertEquals(Double.valueOf(2.2), secondEntrySecondTax.getValue());
	}

	private TaxCalculationRequestDto createTaxCalculationRequestDto()
	{
		final TaxCalculationRequestDto result = new TaxCalculationRequestDto();
		result.setSiteId("test site id");

		final ZonedDateTime zonedDateTime = ZonedDateTime.parse("2018-09-08T04:14:00Z");
		result.setDate(Date.from(zonedDateTime.toInstant()));

		result.setCurrency("test currency");

		final TaxAddressDto deliveryAddressDto = getTaxAddressDto(
				"delivery country",
				"delivery state",
				"delivery city",
				"delivery postcode"
		);
		result.setDeliveryAddress(deliveryAddressDto);

		final TaxAddressDto paymentAddressDto = getTaxAddressDto(
				"payment country",
				"payment state",
				"payment city",
				"payment postcode"
		);
		result.setPaymentAddress(paymentAddressDto);

		final List<TaxCalculationRequestEntryDto> entries = new ArrayList<>();

		TaxCalculationRequestEntryDto firstEntry = getTaxCalculationRequestEntryDto(
				"first code",
				1.0,
				"first code type",
				"request first entry id",
				"Physical"
		);
		entries.add(firstEntry);
		TaxCalculationRequestEntryDto secondEntry = getTaxCalculationRequestEntryDto(
				"second code",
				2.0,
				"second code type",
				"request second entry id",
				"Digital"
		);
		entries.add(secondEntry);
		result.setEntries(entries);

		result.setHandlingCost(3.0);
		result.setShippingCost(4.0);

		return result;
	}

	private TaxCalculationRequestEntryDto getTaxCalculationRequestEntryDto(final String code, final double amount,
			final String codeType, final String id, final String productType)
	{
		TaxCalculationRequestEntryDto result = new TaxCalculationRequestEntryDto();
		result.setCode(code);
		result.setAmount(amount);
		result.setCodeType(codeType);
		result.setId(id);
		result.setProductType(productType);
		return result;
	}

	private TaxAddressDto getTaxAddressDto(final String country, final String state, final String city, final String postcode)
	{
		final TaxAddressDto result = new TaxAddressDto();
		result.setCountry(country);
		result.setState(state);
		result.setCity(city);
		result.setPostcode(postcode);
		return result;
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) taxApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}