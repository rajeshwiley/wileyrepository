package com.wiley.core.integration.order.email;

import de.hybris.bootstrap.annotations.IntegrationTest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemClientErrorException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;


/**
 * Created by Tamas_szebeni on 2016-08-10.
 */
@IntegrationTest
public class Wileyb2bSendEmailWithOrderDetailsGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final String RESOURCE_PREFIX =
			"/wileytest/integration/Wileyb2bSendEmailWithOrderDetailsGatewayIntegrationTest";

	@Resource
	private Wileyb2bSendEmailWithOrderDetailsGateway wileyb2bSendEmailWithOrderDetailsGateway;

	@Resource
	private HttpComponentsClientHttpRequestFactory wileyb2bSendEmailWithOrderDetailsRequestFactory;

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return wileyb2bSendEmailWithOrderDetailsRequestFactory;
	}

	@Test
	public void shouldTriggerEmailSendingWithOrderDetailsForExplicitEmail() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_wileyb2b_order_email_success.json",
				RESOURCE_PREFIX + "/response/response_wileyb2b_order_email_success.json"
		);
		wireMock.addStubMapping(stubMapping);

		String userId = "test";
		String orderId = "testOrder";
		String email = "b2bcustomer@wiley.com";
		String sapAccountNumber = "sapAccount3";
		boolean actual = wileyb2bSendEmailWithOrderDetailsGateway
				.sendEmailWithOrderDetails(userId, email, orderId, sapAccountNumber);
		assertTrue("Trigger send email action was successful.", actual);
		wireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/b2b/accounts/sapAccount3/emailOrderDetails"))
				.withRequestBody(WireMock.containing(email)));
	}

	@Test
	public void shouldTriggerEmailSendingWithOrderDetailsReturnWithNotProcessedFalse() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_wileyb2b_order_email_notprocessed.json",
				RESOURCE_PREFIX + "/response/response_wileyb2b_order_email_notprocessed.json"
		);
		wireMock.addStubMapping(stubMapping);

		String userId = "test";
		String email = "test@rustic-hw.com";
		String orderId = "testOrder";
		String sapAccountNumber = "notprocessed";
		boolean actual = wileyb2bSendEmailWithOrderDetailsGateway
				.sendEmailWithOrderDetails(userId, email, orderId, sapAccountNumber);
		assertFalse("Trigger send email request was successful but not processed.", actual);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void shouldTriggerEmailSendingWithOrderDetailsThrowExceptionWhen400() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_wileyb2b_order_email_400.json",
				RESOURCE_PREFIX + "/response/response_wileyb2b_order_email_400.json"
		);
		wireMock.addStubMapping(stubMapping);

		String userId = "test";
		String email = "test@rustic-hw.com";
		String orderId = "testOrder";
		String sapAccountNumber = "httperror400";
		wileyb2bSendEmailWithOrderDetailsGateway.sendEmailWithOrderDetails(userId, email, orderId, sapAccountNumber);
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void verify401Case() throws Exception
	{
		// given
		String userId = "test";
		String email = "test@rustic-hw.com";
		String orderId = "testOrder";
		String sapAccountNumber = "httperror401";
		setupWireMockPostErrorResponse("/b2b/accounts/.*/emailOrderDetails", HttpStatus.SC_UNAUTHORIZED);

		//when
		wileyb2bSendEmailWithOrderDetailsGateway.sendEmailWithOrderDetails(userId, email, orderId, sapAccountNumber);
	}

	@Test(expected = ExternalSystemClientErrorException.class)
	public void verify403Case() throws Exception
	{
		// given
		String userId = "test";
		String email = "test@rustic-hw.com";
		String orderId = "testOrder";
		String sapAccountNumber = "httperror403";
		setupWireMockPostErrorResponse("/b2b/accounts/.*/emailOrderDetails", HttpStatus.SC_FORBIDDEN);

		//when
		wileyb2bSendEmailWithOrderDetailsGateway.sendEmailWithOrderDetails(userId, email, orderId, sapAccountNumber);
	}
}
