package com.wiley.integration.customer;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;

import static java.util.Arrays.asList;


@IntegrationTest
public class WileyCustomerGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCustomerGatewayIntegrationTest.class);

	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileyCustomerGatewayIntegrationTest";

	private static final String USERID = "WileyCustomerGatewayIntegrationTest-user-id";
	private static final String PASSWORD = "WileyCustomerGatewayIntegrationTest-password";

	private static final String FORCE_RESET_PASSWORD_URL_PATTERN = "/users/" + USERID + "/password";
	private static final String FORCE_RESET_PASSWORD_REQUEST_JSON = "/mappings/force_reset_password.json";
	private static final String FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON = "/responses/force_reset_password_error_response.json";

	private static final String CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN = "/customers";
	private static final String CUSTOMER_REGISTRATION_REQUEST_JSON = "/mappings/register-customer-request.json";
	private static final String CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON = "/responses/register-customer-error-response.json";

	private static final List<String> DISABLED_SITES = asList("ags", "wel");
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource
	private WileyCustomerGateway wileyCustomerGateway;

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	private CustomerModel customerModel;

	@Override
	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		userService.setCurrentUser(userService.getAdminUser()); // Needed for creating B2BUnit
		importCsv(RESOURCE_PREFIX + "/impex/testCustomer.impex", DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getAnonymousUser());
		baseSiteService.setCurrentBaseSite("wileyb2c", true);

		customerModel = (CustomerModel) userService.getUserForUID("customer@test.com");

		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void verifyForceResetPasswordSuccessCase() throws Exception
	{
		// Given
		final StubMapping stubMapping = setUpStubMapping(RESOURCE_PREFIX + FORCE_RESET_PASSWORD_REQUEST_JSON, 204);
		wireMock.addStubMapping(stubMapping);

		// When
		wileyCustomerGateway.forceResetPassword(USERID, PASSWORD);

		// Then
		wireMock.verify(getRequestPatternBuilder(FORCE_RESET_PASSWORD_URL_PATTERN, RequestMethod.PUT));
	}

	@Test
	public void verifyForceResetPasswordWhen400StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				400,
				ExternalSystemBadRequestException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyForceResetPasswordWhen401StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				401,
				ExternalSystemUnauthorizedException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyForceResetPasswordWhen403StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				403,
				ExternalSystemUnauthorizedException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyForceResetPasswordWhen404StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				404,
				ExternalSystemNotFoundException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyForceResetPasswordWhen500StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				500,
				ExternalSystemInternalErrorException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyForceResetPasswordWhen503StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.forceResetPassword(USERID, PASSWORD),
				FORCE_RESET_PASSWORD_REQUEST_JSON,
				FORCE_RESET_PASSWORD_ERROR_RESPONSE_JSON,
				503,
				ExternalSystemInternalErrorException.class,
				FORCE_RESET_PASSWORD_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyRegisterCustomerSuccessCase() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(RESOURCE_PREFIX + CUSTOMER_REGISTRATION_REQUEST_JSON, 201);
		wireMock.addStubMapping(stubMapping);

		// when
		boolean result = wileyCustomerGateway.registerCustomer(customerModel, PASSWORD);

		// then
		wireMock.verify(getRequestPatternBuilder(CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN, RequestMethod.POST));
		Assert.assertTrue("In case of success the gateway should return with true.", result);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen400StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				400,
				ExternalSystemBadRequestException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen401StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				401,
				ExternalSystemUnauthorizedException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen403StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				403,
				ExternalSystemUnauthorizedException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen404StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				404,
				ExternalSystemNotFoundException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen500StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				500,
				ExternalSystemInternalErrorException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Test
	public void verifyRegisterCustomerCaseWhen503StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyCustomerGateway.registerCustomer(customerModel, PASSWORD),
				CUSTOMER_REGISTRATION_REQUEST_JSON,
				CUSTOMER_REGISTRATION_ERROR_RESPONSE_JSON,
				503,
				ExternalSystemInternalErrorException.class,
				CUSTOMER_REGISTRATION_EXPECTED_URL_PATTERN,
				RequestMethod.POST
		);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("forceResetPassword", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + FORCE_RESET_PASSWORD_REQUEST_JSON, 204));
				wileyCustomerGateway.forceResetPassword(USERID, PASSWORD);
				return null;
			}
		});

		funcMap.put("registerCustomer", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + CUSTOMER_REGISTRATION_REQUEST_JSON, 201));
				wileyCustomerGateway.registerCustomer(customerModel, PASSWORD);
				return null;
			}
		});
		return funcMap;
	}
}
