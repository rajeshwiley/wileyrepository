package com.wiley.core.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.product.data.ExternalInventoryStatus;

import static java.util.Arrays.asList;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
@IntegrationTest
public class EsbRealTimeInventoryCheckGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("ags", "wel", "wileyb2b");

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private ModelService modelService;

	@Resource
	private EsbRealTimeInventoryCheckGateway esbRealTimeInventoryCheckGateway;

	@Resource
	private BaseSiteService baseSiteService;

	private static final String RESOURCE_PREFIX =
			"/wileytest/integration/EsbInventory";

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
	}

	@Test
	public void successCase() throws Exception
	{
		// given

		// preparations
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/inventoryCheck_Success.json",
				RESOURCE_PREFIX + "/response/response_inventoryCheck_success.json"
		);
		wireMock.addStubMapping(stubMapping);

		final CartModel cartModel = setupCartModel();

		// when
		final Map<String, ExternalInventoryStatus> stringListMap =
				esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);

		// then
		assertTrue(!CollectionUtils.isEmpty(stringListMap));
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void verify400Case() throws Exception
	{
		// given

		// preparations
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/inventoryCheck_400.json",
				400
		);
		wireMock.addStubMapping(stubMapping);

		final CartModel cartModel = setupCartModel();

		cartModel.getDeliveryMode().setCode(null);

		// when
		esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);

		// then
		// expect exception
	}

	// todo (current task) should be ExternalSystemUnauthorizedException
	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify401Case() throws Exception
	{
		// given

		// preparations
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/inventoryCheck_401.json",
				401
		);
		wireMock.addStubMapping(stubMapping);

		final CartModel cartModel = setupCartModel();

		cartModel.getDeliveryMode().setCode("unauthorized401");

		// when
		esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);

		// then
		// expect exception
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify403Case() throws Exception
	{
		// given

		// preparations
		setupWireMockPostErrorResponse("/carts/inventory/check", HttpStatus.SC_FORBIDDEN);

		final CartModel cartModel = setupCartModel();
		cartModel.getDeliveryMode().setCode("unauthorized403");

		// when
		esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);
		// then expect exception
	}

	// todo (current task) should expect ExternalSystemInternalErrorException
	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify500Case() throws Exception
	{
		// given

		// preparations
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/inventoryCheck_500.json",
				500
		);
		wireMock.addStubMapping(stubMapping);

		final CartModel cartModel = setupCartModel();

		cartModel.getDeliveryMode().setCode("internal500");

		// when
		esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);

		// then
		// expect exception
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify503Case() throws Exception
	{
		// given

		// preparations
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/inventoryCheck_503.json",
				503
		);
		wireMock.addStubMapping(stubMapping);

		final CartModel cartModel = setupCartModel();

		cartModel.getDeliveryMode().setCode("unavailable503");

		// when
		esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);

		// then
		// expect exception
	}

	private CartModel setupCartModel() throws ImpExException
	{

		importCsv("/wileycore/test/order/EsbRealTimeInventoryCheckGatewayIntegrationTest/orderWithShippableProduct.impex",
				DEFAULT_ENCODING);

		final CartModel orderModel = new CartModel();
		orderModel.setCode("severalInvoiceOrder");

		return modelService.getByExample(orderModel);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		final CartModel cartModel = setupCartModel();
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("performRealTimeInventoryCheck", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final StubMapping stubMapping = setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/inventoryCheck_Success.json",
						RESOURCE_PREFIX + "/response/response_inventoryCheck_success.json"
				);
				wireMock.addStubMapping(stubMapping);
				esbRealTimeInventoryCheckGateway.performRealTimeInventoryCheck(cartModel);
				return null;
			}
		});
		return funcMap;
	}
}
