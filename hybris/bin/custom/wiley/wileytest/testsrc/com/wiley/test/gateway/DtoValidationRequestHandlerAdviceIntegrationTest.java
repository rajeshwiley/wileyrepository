package com.wiley.test.gateway;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.exceptions.ExternalSystemValidationException;
import com.wiley.test.gateway.dto.NestedValidationTestDto;
import com.wiley.test.gateway.dto.ValidationTestDto;
import com.wiley.test.gateway.transformer.TestTransformer;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.support.MessageBuilder;


/**
 * Integration test for checking {@link com.wiley.integrations.handler.WileycomDtoValidatorRequestHandlerAdvice} behavior.
 */
@IntegrationTest
public class DtoValidationRequestHandlerAdviceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final Logger LOG = LoggerFactory.getLogger(DtoValidationRequestHandlerAdviceIntegrationTest.class);

	private ApplicationContext parentApplicationContext;

	private SimpleTestGateway simpleTestGateway;

	private TestTransformer testTransformer;

	@Before
	public void setUp() throws Exception
	{
		this.parentApplicationContext = Registry.getApplicationContext();

		ApplicationContext applicationContext = initializeTestApplicationContext(parentApplicationContext,
				"/wileytest/gateway/DtoValidationRequestHandlerAdviceIntegrationTest/context/spring-context.xml");

		this.simpleTestGateway = applicationContext.getBean("simpleTestGateway", SimpleTestGateway.class);
		this.testTransformer = applicationContext.getBean("testTransformer", TestTransformer.class);
	}

	@Test
	public void shouldValidateWithoutErrors()
	{
		// Given
		final ValidationTestDto outgoingDto = buildValidationTestDto(
				35,
				"Test name",
				"Test Reg Number34"
		);

		final ValidationTestDto incomingDto = buildValidationTestDto(
				35,
				"Test name",
				"Test Reg Number34"
		);

		testTransformer.setTransformationFunction((incomingMessage) -> MessageBuilder
				.withPayload(outgoingDto)
				.copyHeaders(incomingMessage.getHeaders())
				.build());

		// When
		Object result = simpleTestGateway.returnableMethodWithParam(incomingDto);

		// Then
		assertSame(outgoingDto, result);

	}

	@Test
	public void shouldThrowExceptionOnIncomingDto()
	{
		final ValidationTestDto incomingDto = buildValidationTestDto(
				10,
				null,
				null
		);

		// When
		try
		{
			simpleTestGateway.returnableMethodWithParam(incomingDto);
			fail("Expected ExternalSystemValidationException");
		}
		catch (Exception e)
		{
			// Then
			checkException(e);
		}
	}

	@Test
	public void shouldThrowExceptionOnOutgoingDto()
	{
		// Given
		final ValidationTestDto outgoingDto = buildValidationTestDto(
				35,
				"Test name",
				null
		);

		final ValidationTestDto incomingDto = buildValidationTestDto(
				35,
				"Test name",
				"Test Reg Number34"
		);

		testTransformer.setTransformationFunction((incomingMessage) -> MessageBuilder
				.withPayload(outgoingDto)
				.copyHeaders(incomingMessage.getHeaders())
				.build());

		// When
		try
		{
			simpleTestGateway.returnableMethodWithParam(incomingDto);
			fail("Expected ExternalSystemValidationException");
		}
		catch (Exception e)
		{
			// Then
			checkException(e);
		}
	}

	private void checkException(final Exception e)
	{
		boolean isExpectedException = e instanceof ExternalSystemValidationException;

		if (!isExpectedException)
		{
			LOG.error(e.getMessage(), e);
		}

		assertTrue("Expected ExternalSystemValidationException.", isExpectedException);
	}

	private ValidationTestDto buildValidationTestDto(final Integer age, final String name, final String regNumber)
	{
		final ValidationTestDto validationTestDto = new ValidationTestDto();
		validationTestDto.setAge(age);
		validationTestDto.setName(name);
		NestedValidationTestDto nestedValidationTestDto = new NestedValidationTestDto();
		nestedValidationTestDto.setRegNumber(regNumber);
		validationTestDto.setNestedValidationTestDto(nestedValidationTestDto);
		return validationTestDto;
	}

}
