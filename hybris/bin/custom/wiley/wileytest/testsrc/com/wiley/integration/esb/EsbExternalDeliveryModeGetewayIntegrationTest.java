package com.wiley.integration.esb;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;
import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * Created by Uladzimir_Barouski on 9/1/2016.
 */
public class EsbExternalDeliveryModeGetewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(EsbExternalDeliveryModeGetewayIntegrationTest.class);

	private static final String RESOURCE_PREFIX = "/wileytest/integration/EsbExternalDeliveryModeGatewayIntegrationTest";

	private static final String BASE_SITE = "wileyb2c";

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private EsbCartCalculationGateway realEsbCartCalculationGateway;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		importCsv(RESOURCE_PREFIX + "/impex/cartdata.impex", DEFAULT_ENCODING);

		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
		baseSiteService.setCurrentBaseSite(BASE_SITE, true);
	}

	@Test
	public void deliveryCostSuccess() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/shippingInfoSuccess.json",
				RESOURCE_PREFIX + "/response/shippingInfoSuccess.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		final List<ExternalDeliveryModeModel> externalDeliveryModes = realEsbCartCalculationGateway.getExternalDeliveryModes(
				cart);

		// ---------- Then ----------
		assertNotNull(externalDeliveryModes);
		assertEquals(2, externalDeliveryModes.size());
		assertEquals("A3", externalDeliveryModes.get(0).getExternalCode());
		assertEquals(10.341, externalDeliveryModes.get(0).getCostValue(), 0);
		assertEquals("externalDeliveryModeExternalCode", externalDeliveryModes.get(1).getExternalCode());
		assertEquals(15.99, externalDeliveryModes.get(1).getCostValue(), 0);
	}

	@Test
	public void verify400Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/shippingInfoError400.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.getExternalDeliveryModes(cart);
			fail("Expected ExternalSystemBadRequestException");
		}
		catch (ExternalSystemBadRequestException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	public void verify401Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/shippingInfoError401.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.getExternalDeliveryModes(cart);
			fail("Expected ExternalSystemInternalErrorException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify403Response() throws Exception
	{
		// --------- Given ---------
		setupWireMockPostErrorResponse("/carts/delivery/options", HttpStatus.SC_FORBIDDEN);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------

		realEsbCartCalculationGateway.getExternalDeliveryModes(cart);
		fail("Expected ExternalSystemInternalErrorException");
	}

	@Test
	public void verify404Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/shippingInfoError404.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.getExternalDeliveryModes(cart);
			fail("Expected ExternalSystemInternalErrorException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	public void verify500Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/shippingInfoError500.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.getExternalDeliveryModes(cart);
			fail("Expected ExternalSystemInternalErrorException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}
}
