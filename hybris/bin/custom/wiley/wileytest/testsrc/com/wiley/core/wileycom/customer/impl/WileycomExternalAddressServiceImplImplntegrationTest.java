package com.wiley.core.wileycom.customer.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static java.util.Arrays.asList;


/**
 * Created by Mikhail_Asadchy on 7/12/2016.
 */
@IntegrationTest
public class WileycomExternalAddressServiceImplImplntegrationTest
		extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final String CUSTOMER_ID = "customerId";
	private static final String EXTERNAL_ADDRESS_ID = "externalAddressId";
	private static final String DELETE_BILLING_ADDRESS_URL = "/customers/" + CUSTOMER_ID
			+ "/billingAddresses/" + EXTERNAL_ADDRESS_ID;
	private static final String DELETE_SHIPPING_ADDRESS_URL = "/customers/"
			+ CUSTOMER_ID + "/shippingAddresses/" + EXTERNAL_ADDRESS_ID;

	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");
	private static final List<String> DISABLED_SITES = asList("ags", "wel");

	@Resource
	private WileycomExternalAddressServiceImpl wileycomExternalAddressService;
	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
	}

	@Test
	public void shouldSuccessfullyDeleteShippingAddress() throws Exception
	{
		deleteAddressWithWiremockStub(true, 204);
	}

	@Test
	public void shouldSuccessfullyDeleteBillingAddress() throws Exception
	{
		deleteAddressWithWiremockStub(false, 204);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void verifyBadRequestThrownForDeleteBillingAddressAndResponse400() throws Exception
	{
		deleteAddressWithWiremockStub(false, 400);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void verifyBadRequestThrownForDeleteShippingAddressAndResponse400() throws Exception
	{
		deleteAddressWithWiremockStub(true, 400);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteBillingAddressAndResponse500() throws Exception
	{
		deleteAddressWithWiremockStub(false, 500);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteShippingAddressAndResponse500() throws Exception
	{
		deleteAddressWithWiremockStub(true, 500);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteShippingAddressAndResponse503() throws Exception
	{
		deleteAddressWithWiremockStub(true, 503);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteBillingAddressAndResponse503() throws Exception
	{
		deleteAddressWithWiremockStub(false, 503);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteShippingAddressAndResponse401() throws Exception
	{
		deleteAddressWithWiremockStub(true, 401);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verifyInternalErrorThrownForDeleteBillingAddressAndResponse401() throws Exception
	{
		deleteAddressWithWiremockStub(false, 401);
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("deleteShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				shouldSuccessfullyDeleteShippingAddress();
				return null;
			}
		});
		funcMap.put("deleteBillingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				shouldSuccessfullyDeleteBillingAddress();
				return null;
			}
		});
		return funcMap;
	}

	public void deleteAddressWithWiremockStub(final boolean isShipping, final int wiremockResponseCode)
	{
		wireMock.
				stubFor(delete(urlMatching(isShipping ? DELETE_SHIPPING_ADDRESS_URL : DELETE_BILLING_ADDRESS_URL))
						.withHeader("Content-Type", matching("application/json"))
						.withHeader("Src", equalTo("hybris"))
						.withHeader("transactionId", matching(".*"))
						.willReturn(aResponse().withStatus(wiremockResponseCode)
								.withHeader("Content-Type", "application/json")
								.withBody("")));


		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);

		final AddressModel addressModel = new AddressModel();
		addressModel.setExternalId(EXTERNAL_ADDRESS_ID);
		addressModel.setShippingAddress(isShipping);
		addressModel.setBillingAddress(!isShipping);

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}
}
