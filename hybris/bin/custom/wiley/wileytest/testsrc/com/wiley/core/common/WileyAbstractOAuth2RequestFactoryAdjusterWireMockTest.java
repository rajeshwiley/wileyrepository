package com.wiley.core.common;

import org.junit.Before;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.utils.HandshakeUtils;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.request;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;


/**
 * Created by Mikhail_Asadchy on 12.07.2016.
 */
public abstract class WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest extends WileyAbstractOAuth2WireMockTest
		implements HttpComponentsClientHttpRequestFactoryAdjuster
{
	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		final HttpComponentsClientHttpRequestFactory requestFactory = getFactoryToAdjust();

		if (requestFactory != null)
		{
			HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
		}
	}

	protected void setupWireMockGetErrorResponse(final String requestMappingUrl, final int responseHttpStatus)
	{
		setupWireMockResponse(RequestMethod.GET, requestMappingUrl, responseHttpStatus);
	}

	protected void setupWireMockPostErrorResponse(final String requestMappingUrl, final int responseHttpStatus)
	{
		setupWireMockResponse(RequestMethod.POST, requestMappingUrl, responseHttpStatus);
	}

	protected void setupWireMockPutErrorResponse(final String requestMappingUrl, final int responseHttpStatus)
	{
		setupWireMockResponse(RequestMethod.PUT, requestMappingUrl, responseHttpStatus);
	}

	protected void setupWireMockDeleteErrorResponse(final String requestMappingUrl, final int responseHttpStatus)
	{
		setupWireMockResponse(RequestMethod.DELETE, requestMappingUrl, responseHttpStatus);
	}

	private void setupWireMockResponse(final RequestMethod requestMethod, final String requestMappingUrl,
			final int responseHttpStatus)
	{

		wireMock.
				stubFor(request(requestMethod.name(), urlMatching(requestMappingUrl))
						.willReturn(aResponse()
								.withStatus(responseHttpStatus)
								.withHeader("Content-Type", "application/json")));
	}
}
