package com.wiley.integration.wpg;

import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.Currency;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.response.WileyCaptureResult;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;


/**
 * Created by Raman_Hancharou on 12/7/2016.
 */
public class CaptureGatewayIntegrationTest extends AbstractWpgIntegrationTest
{
	private static final Logger LOG = LoggerFactory.getLogger(AuthorizePaymentGatewayIntegrationTest.class);

	private static final String TEST_REQUEST_ID = "testRequestId";
	private static final String TEST_MERCHANT_TRANSACTION_CODE = "testMerchantTransactionCode";
	private static final String TEST_SUCCESS_REQUEST_TOKEN = "testSuccessRequestToken";
	private static final String TEST_ERROR_REQUEST_TOKEN = "testErrorRequestToken";

	@Before
	public void before() throws Exception
	{
		this.
				wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

	}

	@Test
	public void testCaptureSuccess() throws Exception
	{
		// Given
		final WileyCaptureRequest captureRequest = createCaptureRequest(TEST_SUCCESS_REQUEST_TOKEN);
		mockPaymentMethod(TEST_SUCCESS_REQUEST_TOKEN, "capture_response_success.txt", HttpStatus.SC_OK);
		WileyCaptureResult expexted = createExpectedCaptureResult(captureRequest);

		// When
		WileyCaptureResult actual = wileyPaymentGateway.capture(captureRequest);

		// Then
		assertEqualsCaptureResults(expexted, actual);
	}

	@Test
	public void testCaptureError404() throws Exception
	{
		// Given
		final WileyCaptureRequest captureRequest = createCaptureRequest(TEST_ERROR_REQUEST_TOKEN);
		mockPaymentMethod(TEST_ERROR_REQUEST_TOKEN, "response_error.txt", HttpStatus.SC_NOT_FOUND);

		// When
		try
		{
			wileyPaymentGateway.capture(captureRequest);
			fail("Expected ExternalSystemException");
		}
		catch (ExternalSystemException e)
		{
			//success
		}
		catch (Throwable t)
		{
			fail("Unexpected exception type. Expected ExternalSystemException");
		}
	}

	private WileyCaptureRequest createCaptureRequest(final String requestToken)
	{
		Currency currency = Currency.getInstance(TEST_CURRENCY);
		BigDecimal totalAmount = BigDecimal.valueOf(TEST_TOTAL_AMOUNT);
		WileyCaptureRequest request = new WileyCaptureRequest(TEST_MERCHANT_TRANSACTION_CODE, TEST_REQUEST_ID,
				requestToken, currency, totalAmount, TEST_PAYMENT_PROVIDER, TEST_AUTH_CODE, TEST_SITE_ID);
		return request;
	}

	private WileyCaptureResult createExpectedCaptureResult(final WileyCaptureRequest request)
	{
		WileyCaptureResult result = new WileyCaptureResult();
		result.setMerchantResponse(MERCHANT_RESPONSE);
		result.setRequestId(request.getRequestId());
		result.setTransactionStatus(TransactionStatus.ACCEPTED);
		result.setTotalAmount(request.getTotalAmount());
		result.setStatus(WileyTransactionStatusEnum.SUCCESS);
		return result;
	}

	private void assertEqualsCaptureResults(final WileyCaptureResult expected, final WileyCaptureResult actual)
	{
		assertEquals(expected.getMerchantResponse(), actual.getMerchantResponse());
		assertEquals(expected.getRequestId(), actual.getRequestId());
		assertEquals(expected.getTransactionStatus(), actual.getTransactionStatus());
		assertEquals(expected.getTotalAmount().doubleValue(), actual.getTotalAmount().doubleValue());
		assertEquals(expected.getStatus(), actual.getStatus());
	}
}
