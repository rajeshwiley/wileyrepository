package com.wiley.core.common;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


/**
 * Created by Mikhail_Asadchy on 12.07.2016.
 */
public interface HttpComponentsClientHttpRequestFactoryAdjuster
{

	/**
	 * If you face problem described here (https://jira.wiley.ru/browse/ECSC-11287), you should implement
	 * the method by returning request factory you use.
	 *
	 * @return - request factory to adjust
	 */
	HttpComponentsClientHttpRequestFactory getFactoryToAdjust();

}
