package com.wiley.core.random;

import com.wiley.core.uuid.RandomService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class MockRandomService implements RandomService
{

	private static final String RANDOM_STRING = "randomString";

	@Override
	public String randomString()
	{
		return RANDOM_STRING;
	}

}
