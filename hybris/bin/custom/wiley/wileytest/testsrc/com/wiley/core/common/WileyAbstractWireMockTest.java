package com.wiley.core.common;

import de.hybris.platform.util.Config;

import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Created by Mikhail_Asadchy on 14.06.2016.
 */
public abstract class WileyAbstractWireMockTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyAbstractWireMockTest.class);
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));

	@Rule
	public WileyWireMockRule wireMock = WileyWireMockRule.build();

	protected RequestPatternBuilder getRequestPatternBuilder(final String urlPattern, final RequestMethod requestMethod)
	{
		return wireMock.getRequestPatternBuilder(urlPattern, requestMethod);
	}

	protected StubMapping setUpStubMapping(final String mapping, final String responseBody) throws Exception
	{
		return wireMock.setUpStubMapping(mapping, responseBody);
	}

	protected StubMapping setUpStubMapping(final String mapping, final int responseStatus) throws Exception
	{
		return wireMock.setUpStubMapping(mapping, responseStatus);
	}

	protected void setResponseStatus(final int responseStatus, final StubMapping stubMapping)
	{
		wireMock.setResponseStatus(responseStatus, stubMapping);
	}

	protected void setResponseBody(final StubMapping stubMapping, final String file) throws Exception
	{
		wireMock.setResponseBody(stubMapping, file);
	}

	protected void setResponseBodyBinary(final StubMapping stubMapping, final String binaryFile) throws Exception
	{
		wireMock.setResponseBodyBinary(stubMapping, binaryFile);
	}

	protected void addRequestAndResponse(final String requestJsonPath, final String responseJsonPath) throws Exception
	{
		wireMock.addRequestAndResponse(requestJsonPath, responseJsonPath);
	}

	protected StubMapping createStubMappingFrom(final String jsonFile) throws Exception
	{
		return wireMock.createStubMappingFrom(jsonFile);
	}

	protected void internalVerifyExecutableMethodWithExceptionBehavior(
			final String resourcePrefix, final Executable executableMethod,
			final String stubMappingPath, final String stubResponseBodyPath, final int responseStatus,
			final Class<? extends Exception> expectedException, final String expectedRequestUrl,
			final RequestMethod expectedRequestMethod) throws Exception
	{
		wireMock.internalVerifyExecutableMethodWithExceptionBehavior(resourcePrefix, executableMethod::execute, stubMappingPath,
				stubResponseBodyPath, responseStatus, expectedException, expectedRequestUrl, expectedRequestMethod);
	}

	protected interface Executable
	{
		void execute();
	}
}
