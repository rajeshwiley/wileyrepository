package com.wiley.integrations.price.serviceactivator;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


@IntegrationTest
public class WileyPriceServiceActivatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String RESOURCE_PATH = "/wileytest/integration/WileyPriceServiceActivatorIntegrationTest/";
	private static final String WILEY_AS_PRODUCTS_IMPEX = RESOURCE_PATH + "as_product_import.impex";
	private static final String WILEY_AS_PRODUCTS_EMPTY_START_TIME_IMPEX =
			RESOURCE_PATH + "as_product_import_empty_start_time.impex";
	private static final String WILEY_AS_PRODUCTS_EMPTY_START_TIME_NOT_EMPTY_COUNTRY_IMPEX =
			RESOURCE_PATH + "as_product_import_empty_start_time_not_empty_country.impex";
	private static final String WILEY_AS_PRODUCTS_EXACT_START_TIME_IMPEX =
			RESOURCE_PATH + "as_product_import_exact_start_time.impex";
	private static final String WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_LESSER_GREATER_IMPEX =
			RESOURCE_PATH + "as_product_import_not_exact_start_time_lesser_greater.impex";
	private static final String WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_LESSER_IMPEX =
			RESOURCE_PATH + "as_product_import_not_exact_start_time_lesser.impex";
	private static final String WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_GREATER_IMPEX =
			RESOURCE_PATH + "as_product_import_not_exact_start_time_greater.impex";
	private static final String WILEY_AS_NEW_PRICE = RESOURCE_PATH + "new_price_as.json";
	private static final String WILEY_AS_NEW_PRICE_NO_START_TIME_AND_COUNTRY = RESOURCE_PATH + "new_price_no_start_time_as.json";
	private static final String WILEY_AS_NO_PRODUCT_PRICE = RESOURCE_PATH + "no_product_id_price_as.json";
	private static final String WILEY_AS_NO_PRICE_PRICE = RESOURCE_PATH + "no_price_price_as.json";
	private static final String WILEY_AS_NO_CURRENCY_PRICE = RESOURCE_PATH + "no_currency_price_as.json";
	private static final String WILEY_COM_PRODUCTS_IMPEX = RESOURCE_PATH + "wcom_product_import.impex";
	private static final String WILEY_COM_NEW_PRICE = RESOURCE_PATH + "new_price_wiley_com.json";
	private static final String WILEY_COM_UPDATE_PRICE = RESOURCE_PATH + "update_price_wiley_com.json";
	private static final String CAD_CURRENCY_CODE = "CAD";
	private static final String USD_CURRENCY_CODE = "USD";
	private static final String AS_CATALOG = "asProductCatalog";
	private static final String AS_PRODUCT_CODE = "vp00015";
	private static final String WCOM_CATALOG = "wileyProductCatalog";
	private static final String WCOM_PRODUCT_CODE = "WCOM_VARIANT_PRODUCT";
	private static final String WCOM_PRODUCT_CODE2 = "WCOM_PRODUCT_HARDCOVER";
	private static final String CATALOG_VERSION_NAME = "Online";
	private static final Double EXPECTED_PRICE = 10.5d;
	private static final Date EXPECTED_START_TIME = Date.from(
			ZonedDateTime.of(2018, 6, 27, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant());
	private static final Date EXPECTED_END_TIME = Date.from(
			ZonedDateTime.of(2100, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant());
	private static final Date LESSER_START_TIME = Date.from(
			ZonedDateTime.of(2018, 6, 17, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant());
	private static final Date GREATER_START_TIME = Date.from(
			ZonedDateTime.of(2018, 7, 17, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant());

	@Resource
	private MessageChannel priceRequestChannel;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CommonI18NService commonI18NService;

	@Before
	public void setUp() throws Exception
	{
		// create OOTB SearchRestrictions (Product, etc...)
		CatalogManager.getInstance().createEssentialData(Collections.singletonMap("initmethod", "init"), null);
		// create OOTB SearchRestrictions (PriceRow)
		Europe1PriceFactory.getInstance().createEssentialData(Collections.singletonMap("initmethod", "init"), null);
	}

	@After
	public void cleanupSession()
	{
		catalogVersionService.setSessionCatalogVersions(Collections.emptyList());
	}

	@Test
	public void processAsNewPrice() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(1, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
	}

	@Test
	public void removeAllPricesAndCreateNewPriceIfOnePriceHasEmptyStartTime() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_EMPTY_START_TIME_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(1, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
		assertEquals(EXPECTED_END_TIME, price.getEndTime());
	}

	@Test
	public void dontRemovePricesWithCountryWhenReceivePriceWithoutCountry() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_EMPTY_START_TIME_NOT_EMPTY_COUNTRY_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE_NO_START_TIME_AND_COUNTRY);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, USD_CURRENCY_CODE);
		assertEquals(4, pricesForProduct.size());

		PriceRowModel price = findPriceWithEmptyCountry(pricesForProduct);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertNull(price.getStartTime());
		assertNull(price.getEndTime());
	}

	@Test
	public void updatePriceWithExactStartTime() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_EXACT_START_TIME_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(2, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
		assertEquals(EXPECTED_END_TIME, price.getEndTime());
	}

	@Test
	public void noPricesWithExactStartTimePricesWithLesserAndGreaterStartTimeExists() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_LESSER_GREATER_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(3, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, LESSER_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(LESSER_START_TIME, price.getStartTime());
		assertEquals(EXPECTED_START_TIME, price.getEndTime());

		price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
		assertEquals(GREATER_START_TIME, price.getEndTime());

		price = findPriceForDate(pricesForProduct, GREATER_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(GREATER_START_TIME, price.getStartTime());
	}

	@Test
	public void noPricesWithExactStartTimePricesWithLesserStartTimeExist() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_LESSER_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(2, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, LESSER_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(LESSER_START_TIME, price.getStartTime());
		assertEquals(EXPECTED_START_TIME, price.getEndTime());

		price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
		assertEquals(EXPECTED_END_TIME, price.getEndTime());
	}

	@Test
	public void noPricesWithExactStartTimePriceWithGreaterStartTimeExist() throws Exception
	{
		importCsv(WILEY_AS_PRODUCTS_NOT_EXACT_START_TIME_GREATER_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_AS_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(AS_CATALOG, AS_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(2, pricesForProduct.size());

		PriceRowModel price = findPriceForDate(pricesForProduct, EXPECTED_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
		assertEquals(EXPECTED_START_TIME, price.getStartTime());
		assertEquals(GREATER_START_TIME, price.getEndTime());

		price = findPriceForDate(pricesForProduct, GREATER_START_TIME);
		assertNotNull(price.getProductId());
		assertEquals(GREATER_START_TIME, price.getStartTime());
	}

	@Test
	public void processWcomNewPrice() throws Exception
	{
		importCsv(WILEY_COM_PRODUCTS_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_COM_NEW_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();
		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		final List<PriceRowModel> pricesForProduct = findPrices(WCOM_CATALOG, WCOM_PRODUCT_CODE, CAD_CURRENCY_CODE);
		assertEquals(1, pricesForProduct.size());

		PriceRowModel price = pricesForProduct.get(0);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
	}

	@Test
	public void processWcomUpdatePrice() throws Exception
	{
		importCsv(WILEY_COM_PRODUCTS_IMPEX, DEFAULT_ENCODING);

		String newPriceJson = readFromFile(WILEY_COM_UPDATE_PRICE);
		Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();

		final boolean send = priceRequestChannel.send(priceMessage);
		assertTrue(send);

		// expected that single existing USD price will be updated
		final List<PriceRowModel> pricesForProduct = findPrices(WCOM_CATALOG, WCOM_PRODUCT_CODE2, USD_CURRENCY_CODE);
		assertEquals(1, pricesForProduct.size());

		PriceRowModel price = pricesForProduct.get(0);
		assertNotNull(price.getProductId());
		assertEquals(EXPECTED_PRICE, price.getPrice());
	}

	@Test
	public void testPriceDtoValidation() throws Exception
	{
		String newPriceJson = readFromFile(WILEY_AS_NO_PRODUCT_PRICE);
		checkValidation(newPriceJson);

		newPriceJson = readFromFile(WILEY_AS_NO_PRICE_PRICE);
		checkValidation(newPriceJson);

		newPriceJson = readFromFile(WILEY_AS_NO_CURRENCY_PRICE);
		checkValidation(newPriceJson);
	}

	private void checkValidation(final String newPriceJson)
	{
		try
		{
			Message<String> priceMessage = MessageBuilder.withPayload(newPriceJson).build();

			priceRequestChannel.send(priceMessage);
			fail("Constraint validation not applied");
		}
		catch (Exception e)
		{
			assertThat(e.getCause(), Matchers.instanceOf(ConstraintViolationException.class));
		}
	}

	private List<PriceRowModel> findPrices(final String catalogName, final String productCode, final String currencyCode)
	{
		final CurrencyModel currency = commonI18NService.getCurrency(currencyCode);
		catalogVersionService.setSessionCatalogVersion(catalogName, CATALOG_VERSION_NAME);

		String query = "SELECT {" + PriceRowModel.PK + "} FROM {" + PriceRowModel._TYPECODE + "} "
				+ " WHERE {" + PriceRowModel.PRODUCTID + "} = ?" + PriceRowModel.PRODUCTID
				+ " AND {" + PriceRowModel.CURRENCY + "}=?" + PriceRowModel.CURRENCY
				+ " AND {" + PriceRowModel.MINQTD + "}=?" + PriceRowModel.MINQTD;
		FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
		fsQuery.addQueryParameter(PriceRowModel.PRODUCTID, productCode);
		fsQuery.addQueryParameter(PriceRowModel.CURRENCY, currency);
		fsQuery.addQueryParameter(PriceRowModel.MINQTD, 1L);

		final SearchResult<PriceRowModel> searchResult = flexibleSearchService.search(fsQuery);
		return searchResult.getResult();
	}

	private PriceRowModel findPriceForDate(final List<PriceRowModel> prices, final Date startTime)
	{
		List<PriceRowModel> filteredPrices = prices.stream().filter(
				price -> startTime.equals(price.getStartTime())).collect(
				Collectors.toList());
		assertEquals("Multiple prices with the same startTime found", 1, filteredPrices.size());
		return filteredPrices.get(0);
	}

	private PriceRowModel findPriceWithEmptyCountry(final List<PriceRowModel> prices)
	{
		List<PriceRowModel> filteredPrices = prices.stream().filter(
				price -> price.getCountry() == null).collect(
				Collectors.toList());
		assertEquals("Multiple prices with empty country found", 1, filteredPrices.size());
		return filteredPrices.get(0);
	}

}