package com.wiley.core.externaltax.services.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.externaltax.services.TaxSystemRequestCommand;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToXml;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;


/**
 *
 */
@Ignore
@IntegrationTest
public class TaxSystemRequestCommandImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String TAX_URI_MOCK = "http://localhost:" + WIREMOCK_PORT + "/taxService";
	private static final String MATCHED_URI = "/taxService";

	private TaxServiceRequest request;
	private String bodyRequest;

	@Resource(name = "taxSystemRequestCommand")
	private TaxSystemRequestCommand taxSystemRequestCommand;

	@Rule
	public final WireMockRule wireMock = new WireMockRule(wireMockConfig().port(WIREMOCK_PORT));


	@Before
	public void setUp()
	{
		initRequest();
		setup();
	}

	private void setup()
	{
		ReflectionTestUtils.setField(taxSystemRequestCommand, "taxServiceUri", TAX_URI_MOCK);
		ReflectionTestUtils.setField(taxSystemRequestCommand, "taxServiceLocaleProperty", "en_US");
	}

	private void initRequest()
	{
		request = new TaxServiceRequest();
		request.setCity("New York");
		request.setCountryCode("US");
		request.setNumberOfUnits(5);
		request.setPostageAmount(10.5);
		request.setStateProvince("New York");
		request.setZipCode("01234567890");

		bodyRequest = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
				+ "<TaxServiceRequest>"
				+ "<City>New York</City>"
				+ "<StateProvince>New York</StateProvince>"
				+ "<ZipCode>01234567890</ZipCode>"
				+ "<CountryCode>US</CountryCode>"
				+ "<NumberOfUnits>5</NumberOfUnits>"
				+ "<PostageAmount>10.5</PostageAmount>"
				+ "</TaxServiceRequest>";
	}


	@Test
	public void callSuccessfulFlowTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>15</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		Double taxAmount = taxSystemRequestCommand.execute(request, "123", 12345);

		assertEquals("Expected taxAmount is 15", Double.valueOf(15.0), taxAmount);
	}

	/**
	 * taxserver call fails due timeout
	 *
	 * @throws JAXBException
	 */
	@Test(expected = TaxSystemException.class)
	public void callFailDueTimeoutTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withFixedDelay(15000) //15 seconds
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>15</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		taxSystemRequestCommand.execute(request, "123", 12345);
	}

	/**
	 * response differs from excepted one - has completely wrong structure including root element
	 */
	@Test(expected = TaxSystemException.class)
	public void wrongStructureTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxAmount>15</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		taxSystemRequestCommand.execute(request, "123", 12345);
	}

	/**
	 * response differs from excepted one - has slightly different structure - eg naming of field doesn't match expecting
	 */
	@Test(expected = TaxSystemException.class)
	public void differentStructureTest() throws TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<XXXTaxServiceResponse>"
												+ "<Taxmount>15</TaxAmount>"
												+ "</XXXTaxServiceResponse>")
						));

		taxSystemRequestCommand.execute(request, "123", 12345);
	}

	/**
	 * taxserver is completely unavailable
	 */
	@Test(expected = TaxSystemException.class)
	public void completelyUnavailableTest() throws TaxSystemException
	{
		// do not initialize wiremock at all

		taxSystemRequestCommand.execute(request, "123", 12345);
	}

	/**
	 * returns tax amount incorrectly formatted
	 *
	 * @throws JAXBException
	 */
	@Test(expected = TaxSystemException.class)
	public void taxAmountIncorrectlyFormattedTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>foo</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		taxSystemRequestCommand.execute(request, "123", 12345);
	}

	/**
	 * return 0
	 */
	@Test
	public void returnZeroTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>0</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		Double response = taxSystemRequestCommand.execute(request, "123", 12345);

		assertEquals("Expected taxAmount is 0", Double.valueOf(0.0), response);
	}

	/**
	 * return reasonably big value
	 */
	@Test
	public void returnBigValueTest() throws JAXBException, TaxSystemException
	{
		String taxLength = Double.toString(Double.MAX_VALUE);

		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>" + taxLength + "</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		Double response = taxSystemRequestCommand.execute(request, "123", 12345);

		assertEquals("Expected taxAmount " + taxLength, Double.valueOf(Double.MAX_VALUE), response);
	}

	/**
	 * return ERROR code
	 */
	@Test(expected = TaxSystemException.class)
	public void returnErrorCodeTest() throws JAXBException, TaxSystemException
	{
		wireMock.
				stubFor(post(urlMatching(MATCHED_URI))
						.withHeader("Content-Type", matching("application/xml"))
						.withRequestBody(equalToXml(bodyRequest))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "application/xml")
										.withBody("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
												+ "<TaxServiceResponse>"
												+ "<TaxAmount>ERROR</TaxAmount>"
												+ "</TaxServiceResponse>")
						));

		taxSystemRequestCommand.execute(request, "123", 12345);
	}
}
