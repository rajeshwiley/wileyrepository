package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.address.WileyAddressService;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.esb.EsbB2CSubscriptionGateway;

import static java.util.Arrays.asList;


@IntegrationTest
public class EsbB2CSubscriptionGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(EsbB2CSubscriptionGatewayIntegrationTest.class);

	private static final String EXTERNAL_CODE = "test-external-code";
	private static final String DELIVERY_ADDRESS_ID = "subscription-test-delivery-address-id";
	private static final String CUSTOMER_ID = "subscription-test-customer";
	private static final boolean RENEWABLE_FLAG = true;

	private static final String RESOURCE_PREFIX = "/wileytest/integration/EsbB2CSubscriptionGatewayIntegrationTest";

	private static final String SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN = "/subscriptions/" + EXTERNAL_CODE + "/shippingAddress";
	private static final String MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON = "/mappings/subscription_delivery_address.json";
	private static final String RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON =
			"/responses/subscription_delivery_address_error_response.json";

	private static final String SUBSCRIPTION_AUTORENEW_URL_PATTERN = "/subscriptions/" + EXTERNAL_CODE + "/renewable";
	private static final String MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON = "/mappings/subscription_autorenew.json";
	private static final String RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON =
			"/responses/subscription_autorenew_error_response.json";

	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("wileyb2b", "ags", "wel");

	@Resource
	private EsbB2CSubscriptionGateway esbB2CSubscriptionGateway;

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private WileyAddressService wileyAddressService;

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	private AddressModel subscriptionDeliveryAddress;

	@Override
	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		importCsv(RESOURCE_PREFIX + "/impex/subscription.impex", DEFAULT_ENCODING);
		CustomerModel customer = userService.getUserForUID(CUSTOMER_ID, CustomerModel.class);
		subscriptionDeliveryAddress = wileyAddressService.getAddressByExternalIdAndOwner(DELIVERY_ADDRESS_ID,
				customer.getPk().toString())
				.get();

		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void verifyAutorenewSubscriptionSuccessCase() throws Exception
	{
		final StubMapping stubMapping = setUpStubMapping(RESOURCE_PREFIX + MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // mapping
				204 // response status
		);

		wireMock.addStubMapping(stubMapping);
		// ---------- When ---------
		esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG);

		// ---------- Then ----------
		wireMock.verify(getRequestPatternBuilder(SUBSCRIPTION_AUTORENEW_URL_PATTERN, RequestMethod.PUT));
	}

	@Test
	public void verifyAutorenewSubscriptionWhen400StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG), // executable
				// method
				MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // stub mapping
				RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON, // response
				400, // response status code
				ExternalSystemBadRequestException.class, // expected exception
				SUBSCRIPTION_AUTORENEW_URL_PATTERN, // expected request url
				RequestMethod.PUT // expected request method
		);
	}

	@Test
	public void verifyAutorenewSubscriptionWhen401StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG), // executable
				// method
				MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // stub mapping
				RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON, // response
				401, // response status code
				ExternalSystemUnauthorizedException.class, // expected exception
				SUBSCRIPTION_AUTORENEW_URL_PATTERN, // expected request url
				RequestMethod.PUT // expected request method
		);
	}

	@Test
	public void verifyAutorenewSubscriptionWhen404StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG), // executable
				// method
				MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // stub mapping
				RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON, // response
				404, // response status code
				ExternalSystemNotFoundException.class, // expected exception
				SUBSCRIPTION_AUTORENEW_URL_PATTERN, // expected request url
				RequestMethod.PUT // expected request method
		);
	}

	@Test
	public void verifyAutorenewSubscriptionWhen500StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG), // executable
				// method
				MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // stub mapping
				RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON, // response
				500, // response status code
				ExternalSystemInternalErrorException.class, // expected exception
				SUBSCRIPTION_AUTORENEW_URL_PATTERN, // expected request url
				RequestMethod.PUT // expected request method
		);
	}

	@Test
	public void verifyAutorenewSubscriptionWhen503StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG), // executable
				// method
				MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, // stub mapping
				RESPONSES_SUBSCRIPTION_AUTORENEW_ERROR_RESPONSE_JSON, // response
				503, // response status code
				ExternalSystemInternalErrorException.class, // expected exception
				SUBSCRIPTION_AUTORENEW_URL_PATTERN, // expected request url
				RequestMethod.PUT // expected request method
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressSuccessCase() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(RESOURCE_PREFIX + MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON, 204);
		wireMock.addStubMapping(stubMapping);

		// when
		esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress);

		// then
		wireMock.verify(getRequestPatternBuilder(SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN, RequestMethod.PUT));
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen400StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				400,
				ExternalSystemBadRequestException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen401StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				401,
				ExternalSystemUnauthorizedException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen403StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				403,
				ExternalSystemUnauthorizedException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen404StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				404,
				ExternalSystemNotFoundException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen500StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				500,
				ExternalSystemInternalErrorException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Test
	public void verifyUpdateSubscriptionShippingAddressWhen503StatusCode() throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress),
				MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON,
				RESPONSES_SUBSCRIPTION_SHIPPING_ADDRESS_ERROR_RESPONSE_JSON,
				503,
				ExternalSystemInternalErrorException.class,
				SUBSCRIPTION_SHIPPING_ADDR_URL_PATTERN,
				RequestMethod.PUT
		);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);

		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("autorenewSubscription", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + MAPPINGS_SUBSCRIPTION_AUTORENEW_JSON, 204));
				esbB2CSubscriptionGateway.autorenewSubscription(EXTERNAL_CODE, RENEWABLE_FLAG);
				return null;
			}
		});

		funcMap.put("updateDeliveryAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + MAPPINGS_SUBSCRIPTION_SHIPPING_ADDRESS_JSON, 204));
				esbB2CSubscriptionGateway.updateDeliveryAddress(EXTERNAL_CODE, subscriptionDeliveryAddress);
				return null;
			}
		});
		return funcMap;
	}
}
