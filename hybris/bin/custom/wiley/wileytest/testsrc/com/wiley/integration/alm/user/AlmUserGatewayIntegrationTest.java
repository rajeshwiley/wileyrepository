package com.wiley.integration.alm.user;

import de.hybris.bootstrap.annotations.IntegrationTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.alm.user.AlmUserGateway;
import com.wiley.core.integration.alm.user.dto.UserDto;


@IntegrationTest
public class AlmUserGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(AlmUserGatewayIntegrationTest.class);
	private static final String RESOURCE_PREFIX = "/wileytest/integration/AlmUserGatewayIntegrationTest";
	private static final String CUSTOMER = "testuserid1";
	private static final String FIRST_NAME = "John";
	private static final String LAST_NAME = "Smith";
	private static final String EMAIL = "johnsmith@wiley.com";
	private static final String NOT_VALID = "testuserid400";
	private static final String UNAUTHORIZED = "testuserid401";
	private static final String SERVER_ERROR = "testuserid500";


	@Resource(name = "almUserGateway")
	private AlmUserGateway gateway;

	@Resource
	private RestTemplate almUserApigeeMgOAuth2RestTemplate;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void testGetCustomerDataSuccessful() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_user_data_200.json",
				RESOURCE_PREFIX + "/responses/response_user_200.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		final UserDto dto = gateway.getUserData(CUSTOMER);

		// then
		assertNotNull(dto);
		assertEquals(CUSTOMER, dto.getUserId());
		assertEquals(FIRST_NAME, dto.getFirstName());
		assertEquals(LAST_NAME, dto.getLastName());
		assertEquals(EMAIL, dto.getEmail());
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void testGetCustomerDataWhen400StatusCode() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_user_data_400.json",
				RESOURCE_PREFIX + "/responses/response_user_400.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getUserData(NOT_VALID);
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void testGetCustomerDataWhen401StatusCode() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_user_data_401.json",
				RESOURCE_PREFIX + "/responses/response_user_401.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getUserData(UNAUTHORIZED);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void testGetCustomerDataWhen500StatusCode() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_user_data_500.json",
				RESOURCE_PREFIX + "/responses/response_user_500.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getUserData(SERVER_ERROR);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) almUserApigeeMgOAuth2RestTemplate.getRequestFactory();
	}
}
