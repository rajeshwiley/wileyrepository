package com.wiley.core.wileycom.validation;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.Collections;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;

import com.wiley.core.validation.service.ConstraintGroupService;


public abstract class AbstractIntegrationValidationIntegrationTest<T extends ItemModel> extends AbstractValidationIntegrationTest
{

	private static final String SIZE_VALIDATION_MESSAGE_TEMPLATE =
			"The number of characters in the attribute \"%s\" must between %d and %d (inclusive)";
	
	@Resource
	private ConstraintGroupService constraintGroupService;
	private ConstraintGroupModel integrationConstraintGroup;

	@Before
	public void setup()
	{
		super.setup();
		integrationConstraintGroup = constraintGroupService.getConstraintGroupForId("b2cIntegrationConstraintGroup");
	}

	@Override
	protected void validateTestInstance()
	{
		final Set<HybrisConstraintViolation> violations = getValidationService().validate(getTestItem(),
				Collections.singletonList(integrationConstraintGroup));
		if (CollectionUtils.isNotEmpty(violations))
		{
			final HybrisConstraintViolation violation = (HybrisConstraintViolation) CollectionUtils.get(violations, 0);
			throw new RuntimeException(violation.getLocalizedMessage());
		}
	}

	protected abstract T getTestItem();

	@Override
	protected String getSizeValidationMessageTemplate()
	{
		return SIZE_VALIDATION_MESSAGE_TEMPLATE;
	}
}
