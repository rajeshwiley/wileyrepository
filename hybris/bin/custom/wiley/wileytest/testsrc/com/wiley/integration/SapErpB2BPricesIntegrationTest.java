package com.wiley.integration;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.hamcrest.number.IsCloseTo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;


/**
 * Created by Georgii_Gavrysh on 7/1/2016.
 */
@IntegrationTest
public class SapErpB2BPricesIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG = LoggerFactory.getLogger(SapErpB2BPricesIntegrationTest.class);

	private static final String SAP_ACCOUNT_NUMBER = "testSapAccountNumber";

	// test outbound gateway uri is overwritten via configs/common/local_tenant_junit.properties
	private static final String GET_B2B_PRICES_PATH =
			"/b2b/accounts/testSapAccountNumber/prices\\?isbn={isbn}{sapProductCodeQuery}";

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/SapErpGatewayIntegrationTest/";
	private static final String US_COUNTRY = "US";
	private static final String USD_CURRENCY = "USD";
	private static final String EUR_CURRENCY = "EUR";
	private static final String UK_COUNTRY = "UK";
	private static final String UNKNOWN_COUNTRY = "AU";
	private static final String WCOM_B2B_PHYSICAL_ISBN = "WCOM_B2B_PHYSICAL";
	private static final String WCOM_B2B_PHYSICAL_SAP_CODE = "sapWCOM_B2B_PHYSICAL";
	private static final String WCOM_B2B_DIGITAL_ISBN = "WCOM_B2B_DIGITAL";
	private static final String WCOM_B2B_DIGITAL_SAP_CODE = "sapWCOM_B2B_DIGITAL";
	private static final String UNKNOWN_PRODUCT_ISBN = "NO_PRODUCT";
	private static final List<String> ENABLED_SITES = asList("wileyb2b");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "ags", "wel");

	@Resource
	private EsbCartCalculationGateway realEsbCartCalculationGateway;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Before
	public void setUp() throws Exception
	{
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

		setUpOauth(wireMock);
		baseSiteService.setCurrentBaseSite("wileyb2b", true);
	}

	@Test
	public void successCaseUSCountry() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel(WCOM_B2B_PHYSICAL_ISBN, WCOM_B2B_PHYSICAL_SAP_CODE);
		configureMockWithResponse(productModel, "response_successResponseUS.json", HttpStatus.SC_OK);

		//When
		final List<PriceInformation> priceInformationList = realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, US_COUNTRY, USD_CURRENCY);

		//Than
		Assert.assertThat(priceInformationList.size(), is(1));
		final PriceInformation priceInformation = priceInformationList.get(0);
		Assert.assertThat(priceInformation.getPriceValue().getCurrencyIso(), is(USD_CURRENCY));
		Assert.assertThat(priceInformation.getPriceValue().getValue(), closeTo(11.0, 0.001));
		Assert.assertTrue(priceInformation.getPriceValue().isNet());
	}



	@Test
	public void successCaseUKCountry() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel(WCOM_B2B_DIGITAL_ISBN, WCOM_B2B_DIGITAL_SAP_CODE);
		configureMockWithResponse(productModel, "response_successResponseUK.json", HttpStatus.SC_OK);

		//When
		final List<PriceInformation> priceInformationList = realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, UK_COUNTRY, EUR_CURRENCY);

		//Than
		Assert.assertEquals(priceInformationList.size(), 1);
		final PriceInformation firstPriceInformation = priceInformationList.get(0);
		Assert.assertThat(firstPriceInformation.getPriceValue().getCurrencyIso(), is(EUR_CURRENCY));
		Assert.assertThat(firstPriceInformation.getPriceValue().getCurrencyIso(), is(EUR_CURRENCY));
		Assert.assertThat(firstPriceInformation.getPriceValue().getValue(), closeTo(8.0, 0.001));
		Assert.assertTrue(firstPriceInformation.getPriceValue().isNet());

	}

	@Test
	public void successCaseUnknownCountry() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel(WCOM_B2B_DIGITAL_ISBN, WCOM_B2B_DIGITAL_SAP_CODE);
		configureMockWithResponse(productModel, "response_successDefaultCountry.json", HttpStatus.SC_OK);

		//When
		final List<PriceInformation> priceInformationList = realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, UNKNOWN_COUNTRY, USD_CURRENCY);

		//Than
		Assert.assertThat(priceInformationList.size(), is(1));
		final PriceInformation firstPriceInformation = priceInformationList.get(0);
		Assert.assertThat(firstPriceInformation.getPriceValue().getCurrencyIso(), is(USD_CURRENCY));
		Assert.assertThat(firstPriceInformation.getPriceValue().getValue(), IsCloseTo.closeTo(3.0, 0.001));
		Assert.assertTrue(firstPriceInformation.getPriceValue().isNet());

	}

	@Test
	public void noSuchProductTest() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel(UNKNOWN_PRODUCT_ISBN, null);
		configureMockWithResponse(productModel, "response_NO_SUCH_PRODUCT.json", HttpStatus.SC_OK);

		//When
		final List<PriceInformation> priceInformationList = realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, UK_COUNTRY, EUR_CURRENCY);

		//Than
		Assert.assertThat(priceInformationList.size(), is(0));

	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void badRequestTest() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel("product_code", null);

		configureMockWithResponse(productModel, "response_internalServerError.json", HttpStatus.SC_INTERNAL_SERVER_ERROR);

		//When
		realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, UK_COUNTRY, EUR_CURRENCY);
	}

	@Test(expected = IllegalArgumentException.class)
	public void twoRecordsInResponseException() throws Exception
	{
		//Given
		final ProductModel productModel = createProductModel(WCOM_B2B_DIGITAL_ISBN, WCOM_B2B_DIGITAL_SAP_CODE);
		configureMockWithResponse(productModel, "response_twoPrices.json", HttpStatus.SC_OK);

		//When
		realEsbCartCalculationGateway.getProductPriceInformations(
				productModel, SAP_ACCOUNT_NUMBER, UK_COUNTRY, EUR_CURRENCY);
	}

	private void configureMockWithResponse(final ProductModel productModel, final String jsonFileName,
			final int expectedResponseStatus) throws Exception
	{
		final java.lang.String sapProductCode = productModel.getSapProductCode();
		wireMock.
				stubFor(get(urlMatching(
						GET_B2B_PRICES_PATH
								.replace("{isbn}", productModel.getIsbn())
								.replace("{sapProductCodeQuery}",
										sapProductCode == null ? "" : "\\&sapProductCode=" + sapProductCode)))
						.withHeader("Authorization", WireMock.equalTo("bearer bd55e2ce-92b0-46a2-a8f4-82a69440b586"))
						.withHeader("TransactionId", WireMock.equalTo("randomString"))
						.willReturn(
								aResponse()
										.withStatus(expectedResponseStatus)
										.withHeader("Content-Type", "application/json")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + jsonFileName))
						));
	}

	private ProductModel createProductModel(final String isbn, final String sapProductCode)
	{
		ProductModel productModel = new ProductModel();
		productModel.setIsbn(isbn);
		productModel.setSapProductCode(sapProductCode);
		return productModel;
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		final ProductModel productModel = createProductModel(WCOM_B2B_PHYSICAL_ISBN, WCOM_B2B_PHYSICAL_SAP_CODE);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("getProductPriceInformations", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				configureMockWithResponse(productModel, "response_successResponseUS.json", HttpStatus.SC_OK);
				realEsbCartCalculationGateway.getProductPriceInformations(productModel, SAP_ACCOUNT_NUMBER, US_COUNTRY,
						USD_CURRENCY);
				return null;
			}
		});
		return funcMap;
	}
}
