package com.wiley.facades.product.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.as.product.data.ProductPricesData;


@IntegrationTest
public class DefaultWileyProductFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String IMPEX_BASE_PATH = "/wileytest/import/product/DefaultWileyProductFacadeIntegrationTest";
	private static final String PRICE_DATA_PATH = IMPEX_BASE_PATH + "/product-prices.impex";
	private static final String PRODUCT_DATA_PATH = IMPEX_BASE_PATH + "/product-with-isbn.impex";
	private static final String PRODUCT_CODE = "wstest_vp001";
	private static final String PRODUCT_ISBN = "0123456789";
	private static final String PRODUCT_URL = "/authorservices/c/test-sample-base-product-1/p/wstest_vp001/";
	private static final String NOT_EXISTING_PRODUCT_CODE = "product-does-not-exist";
	private static final String EUR_CURRENCY_CODE = "EUR";
	private static final String NOT_EXISTING_CURRENCY_CODE = "BLR";

	@Resource
	private DefaultWileyProductFacade defaultWileyProductFacade;

	@Test
	public void getPricesForProductAndCurrency() throws ImpExException
	{
		importCsv(PRICE_DATA_PATH, DEFAULT_ENCODING);
		final ProductPricesData productPrices = defaultWileyProductFacade.getProductPrices(PRODUCT_CODE, EUR_CURRENCY_CODE);
		assertNotNull("Prices data is null", productPrices);
		assertFalse("Prices not found", productPrices.getPrices().isEmpty());

		assertEquals("Wrong prices count returned", 2, productPrices.getPrices().size());
	}

	@Test
	public void notExistingProduct()
	{
		try
		{
			defaultWileyProductFacade.getProductPrices(NOT_EXISTING_PRODUCT_CODE, NOT_EXISTING_CURRENCY_CODE);
			fail("Exception was expected but not thrown");
		}
		catch (UnknownIdentifierException uie)
		{
			assertEquals("Wrong exception found",
					String.format("Product with code '%s' not found!", NOT_EXISTING_PRODUCT_CODE),
					uie.getMessage());
		}
	}

	@Test
	public void notExistingCurrency() throws ImpExException
	{
		importCsv(PRICE_DATA_PATH, DEFAULT_ENCODING);
		try
		{
			defaultWileyProductFacade.getProductPrices(PRODUCT_CODE, NOT_EXISTING_CURRENCY_CODE);
			fail("Exception was expected but not thrown");
		}
		catch (UnknownIdentifierException uie)
		{
			assertEquals("Wrong exception found",
					String.format("CurrencyModel with isocode '%s' not found!", NOT_EXISTING_CURRENCY_CODE),
					uie.getMessage());
		}
	}

	@Test
	public void findProductUrlForIsbnOrCodeCorrectIsbn() throws ImpExException
	{
		importCsv(PRODUCT_DATA_PATH, DEFAULT_ENCODING);
		final String productUrl = defaultWileyProductFacade.findProductUrlForIsbnOrCode(PRODUCT_ISBN);
		assertNotNull("Product url is null", productUrl);
		assertEquals(PRODUCT_URL, productUrl);
	}

	@Test
	public void findProductUrlForIsbnOrCodeIncorrectIsbnFallbackToCode() throws ImpExException
	{
		importCsv(PRODUCT_DATA_PATH, DEFAULT_ENCODING);
		final String productUrl = defaultWileyProductFacade.findProductUrlForIsbnOrCode(PRODUCT_CODE);
		assertNotNull("Product url is null", productUrl);
		assertEquals(PRODUCT_URL, productUrl);
	}

}
