package com.wiley.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;


/**
 * Utils that is used for sending SOAP request in tests
 *
 * @author Dzmitryi_Halahayeu
 */
public final class SoapTestUtils
{


	private SoapTestUtils()
	{
	}

	public static String sendSoapRequest(final String soapUrl, final String soapAction, final String request) throws IOException
	{
		URL url = new URL(soapUrl);
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConn = (HttpURLConnection) connection;

		InputStream inputStream = new ByteArrayInputStream(request.getBytes());

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		IOUtils.copy(inputStream, bout);
		inputStream.close();

		byte[] b = bout.toByteArray();

		httpConn.setRequestProperty("Content-Length",
				String.valueOf(b.length));
		httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		httpConn.setRequestProperty("SOAPAction", soapAction);
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);

		OutputStream out = httpConn.getOutputStream();
		out.write(b);
		out.close();

		return IOUtils.toString(httpConn.getInputStream());
	}


}
