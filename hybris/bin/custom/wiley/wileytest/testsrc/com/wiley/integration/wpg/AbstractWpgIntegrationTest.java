package com.wiley.integration.wpg;

import javax.annotation.Resource;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.integration.wpg.WileyPaymentGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


/**
 * Created by Raman_Hancharou on 12/8/2016.
 */
public abstract class AbstractWpgIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final String TEST_RESPONSE_PATH = "/wileytest/integration/WileyPaymentGatewayIntegrationTest/";

	protected static final String TEST_PAYMENT_PROVIDER = "testPaymentProvider";
	protected static final double TEST_TOTAL_AMOUNT = 100.0;
	protected static final String TEST_CURRENCY = "USD";
	protected static final String TEST_SITE_ID = "ags";
	protected static final String MERCHANT_RESPONSE = "APPROVED";
	protected static final String TEST_AUTH_CODE = "testAuthCode";

	// test outbound gateway uri is overwritten via configs/common/local_tenant_junit.properties
	protected static final String AUTHORIZE_PAYMENT_PATH = "/wpg/http/postUrl";

	@Resource
	private RestTemplate wileyPaymentGatewayClientRestTemplate;

	@Resource
	protected WileyPaymentGateway wileyPaymentGateway;

	@Resource
	protected WileyCountryService wileyCountryService;

	protected void mockPaymentMethod(final String bodyMatcher, final String responseFileName, final int expectedResponseStatus)
			throws Exception
	{
		wireMock.
				stubFor(post(urlMatching(AUTHORIZE_PAYMENT_PATH))
						.withHeader("Content-Type", equalTo("application/x-www-form-urlencoded"))
						.withRequestBody(containing(bodyMatcher))
						.willReturn(
								aResponse()
										.withStatus(expectedResponseStatus)
										.withHeader("Content-Type", "text/html")
										.withBody(loadFileAsString(TEST_RESPONSE_PATH + responseFileName))
						));
	}

	/**
	 * If you face problem described here (https://jira.wiley.ru/browse/ECSC-11287), you should implement
	 * the method by returning request factory you use.
	 *
	 * @return - request factory to adjust
	 */
	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) wileyPaymentGatewayClientRestTemplate.getRequestFactory();
	}
}
