package com.wiley.core.common;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.integration.MessageRejectedException;
import org.springframework.messaging.MessagingException;

import static java.util.Arrays.asList;


/**
 * Created by Uladzimir_Barouski on 12/12/2016.
 */
@Ignore
public abstract class WileyAbstractOAuth2StoreAwareChainTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final List<String> ALL_SITES = asList("ags", "wel", "wileyb2c", "wileyb2b");

	@Test
	public void verifyChainEnabledForSpecifiedSites() throws Exception
	{
		List<String> exceptionsList = new ArrayList<>();
		List<String> sites = getEnabledSites();
		for (String site : sites)
		{
			Map<String, Callable<T>> funcs = getChainFunctions(site);
			for (String chainKey : funcs.keySet())
			{
				try
				{
					funcs.get(chainKey).call();
				}
				catch (Exception e)
				{
					exceptionsList.add(
							String.format("Chain: %s failed for site: %s, message: %s", chainKey, site, e.getMessage()));
				}
			}
		}
		verifyExceptions(exceptionsList);
	}

	private void verifyExceptions(final List<String> exceptionsList)
	{
		if (CollectionUtils.isNotEmpty(exceptionsList))
		{
			Assert.fail(exceptionsList.toString());
		}
	}

	@Test
	public void verifyChainDisabledForSpecifiedSitesCollable() throws Exception
	{
		List<String> exceptionsList = new ArrayList<>();
		List<String> sites = getDisabledSites();
		for (String site : sites)
		{
			Map<String, Callable<T>> funcs = getChainFunctions(site);
			for (String chainKey : funcs.keySet())
			{

				try
				{
					funcs.get(chainKey).call();
					exceptionsList.add(
							String.format("Chain: %s must be enabled for site: %s", chainKey, site));
				}
				catch (Exception e)
				{
					if (!(((MessagingException) e).getRootCause() instanceof MessageRejectedException))
					{
						exceptionsList.add(
								String.format("An error occures for chain: %s for site: %s, message: %s", chainKey, site,
										e.getMessage()));
					}
				}
			}
		}
		verifyExceptions(exceptionsList);
	}

	@Test
	public void verifyAllSitesCoveredByTests()
	{
		List<String> allSites = ListUtils.union(getDisabledSites(), getEnabledSites());
		assertTrue(CollectionUtils.isEqualCollection(ALL_SITES, allSites));
	}

	protected abstract List<String> getDisabledSites();

	protected abstract List<String> getEnabledSites();

	protected abstract Map<String, Callable<T>> getChainFunctions(String site) throws Exception;
}
