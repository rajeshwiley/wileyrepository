package com.wiley.integration.address;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.wileycom.customer.impl.WileycomExternalAddressServiceImpl;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.delete;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;


/**
 * Created by Anton_Lukyanau on 7/13/2016.
 */

public class WIleyExternalAddressServiceIntegrationServiceTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	@Resource
	private WileycomExternalAddressServiceImpl wileycomExternalAddressService;

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private BaseSiteService baseSiteService;

	private static final String EXTERNAL_ID  = "sdfsdfdsf-sdfsdf-sdfsd";


	private CustomerModel customerModel;

	private AddressModel addressModel;


	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		customerModel = mock(CustomerModel.class);
		addressModel = mock(AddressModel.class);
		when(addressModel.getExternalId()).thenReturn(EXTERNAL_ID);
		when(addressModel.getShippingAddress()).thenReturn(true);
		when(addressModel.getBillingAddress()).thenReturn(false);
	}

	@Test
	public void successCase() throws Exception
	{

		when(customerModel.getCustomerID()).thenReturn("whatever");
		stubWithRespCode(204, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}


	@Test(expected = ExternalSystemInternalErrorException.class)
	public void notAuthCase() throws Exception
	{
		when(customerModel.getCustomerID()).thenReturn("not_authenticated_request");
		stubWithRespCode(401, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void forbiddenCase() throws Exception
	{
		when(customerModel.getCustomerID()).thenReturn("forbidden_request");
		stubWithRespCode(403, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}


	@Test(expected = ExternalSystemBadRequestException.class)
	public void badRequestErrCase() throws Exception
	{
		when(customerModel.getCustomerID()).thenReturn("whatever");
		stubWithRespCode(400, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}


	@Test(expected = ExternalSystemInternalErrorException.class)
	public void serviceUnavailibleCase() throws Exception
	{
		when(customerModel.getCustomerID()).thenReturn("service_unavailable_request");
		stubWithRespCode(503, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}


	@Test(expected = ExternalSystemInternalErrorException.class)
	public void internalErrCase() throws Exception
	{
		when(customerModel.getCustomerID()).thenReturn("internal_server_error_request");
		stubWithRespCode(500, customerModel.getCustomerID(), addressModel.getExternalId());

		wileycomExternalAddressService.deleteAddress(customerModel, addressModel);
	}


	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	private void stubWithRespCode(int code, final String customerId, final String externalId)
	{
		final String apiUrlPattern = "/customers/" + customerId + "/shippingAddresses/" + externalId;
		wireMock.
				stubFor(delete(urlMatching(apiUrlPattern)).withHeader("Content-Type", matching("application/json"))
						.withHeader("Src", equalTo("hybris"))
						.withHeader("authorization", equalTo("bearer bd55e2ce-92b0-46a2-a8f4-82a69440b586"))
						.withHeader("transactionId", matching(".*"))
						.willReturn(aResponse().withStatus(code)
								.withHeader("Content-Type", "application/json")
								.withBody("")));
	}
}
