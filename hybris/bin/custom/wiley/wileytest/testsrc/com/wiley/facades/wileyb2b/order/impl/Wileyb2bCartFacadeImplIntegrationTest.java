package com.wiley.facades.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;



/**
 * Created by Mikhail_Asadchy on 11/21/2016.
 */
@IntegrationTest
public class Wileyb2bCartFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String BASE_SITE = "wileyb2c";
	@Resource(name = "wileyb2bCartFacade")
	private Wileyb2bCartFacadeImpl wileyb2bCartFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartService")
	private CartService cartService;
	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CommerceCartService commerceCartService;

	private static final String RESOURCE_PREFIX = "/wileytest/import/cart/Wileyb2bCartFacadeImplIntegrationTest/";

	private final String userName = "customer@test.com";

	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getUserForUID(userName));
		baseSiteService.setCurrentBaseSite(BASE_SITE, true);
	}

	@Test
	public void showDeliveryCostFlagShouldBeTrue() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "userCart.impex", DEFAULT_ENCODING);

		// preparations
		cartService.setSessionCart(commerceCartService.getCartForCodeAndUser("cartCode1", userService.getUserForUID(userName)));

		// when
		final CartData sessionCart = wileyb2bCartFacade.getSessionCart();

		// then
		assertTrue("ShowDeliveryCost flag should be true", sessionCart.isShowDeliveryCost());
	}

	@Test
	public void showDeliveryCostFlagShouldBeFalse() throws Exception
	{
		// given

		// preparations

		// when
		final CartData sessionCart = wileyb2bCartFacade.getSessionCart();

		// then
		assertFalse("ShowDeliveryCost flag should be false", sessionCart.isShowDeliveryCost());
	}
}
