/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.integration.welags;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.utils.FileTestUtils;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;


/**
 * Integration test with an mock external tax service.
 */
@IntegrationTest
public class WelAgsCheckoutWithExternalTaxesIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final String COUNTRY = "US";
	private static final String LAST_NAME = "lastName";
	private static final String FIRST_NAME = "firstName";
	private static final String TOWN = "delivery town";
	private static final String POSTAL_CODE = "delivery postcode";
	private static final String STREET_NUMBER = "streetNumber";
	private static final String STREET_NAME = "streetName";
	private static final String TEST_BASESITE_UID = "testSite";
	private static final String TEST_BASESTORE_UID = "testStore";

	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String CALCULATE_TAXES_PATH = "/es/v1/taxes/calculate";
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WelAgsCheckoutWithExternalTaxesIntegrationTest";

	private static final String REQUEST_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/request_body_with_delivery_address.json";
	private static final String RESPONSE_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/response_body_with_delivery_address_success.json";

	private static final String REQUEST_BODY_WITHOUT_DELIVERY_ADDRESS_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/request_body_without_delivery_address.json";
	private static final String RESPONSE_BODY_WITHOUT_DELIVERY_ADDRESS_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/response_body_without_delivery_address_success.json";

	private static final String RESPONSE_BODY_FOR_DISABLED_EXTERNAL_TAX_JSON_FILE_PATH =
			RESOURCE_PREFIX + "/response_body_for_disabled_external_tax_success.json";

	@Resource
	private DefaultCommerceCheckoutService welAgsCommerceCheckoutService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private RestTemplate taxApigeeEdgeOAuth2RestTemplate;

	private BaseStoreModel store;
	private UserModel userWithPhysicalCart;
	private CartModel cartWithPhysicalProducts;
	private UserModel userWithDigitalCart;
	private CartModel cartWithDigitalProducts;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		importCsv("/test/testCheckoutExternalTaxes.impex", "utf-8");

		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(TEST_BASESITE_UID), false);
		store = baseStoreService.getBaseStoreForUid(TEST_BASESTORE_UID);

		userWithPhysicalCart = userService.getUserForUID("abrode");
		cartWithPhysicalProducts = userWithPhysicalCart.getCarts().iterator().next();
		assertValidUserAndCart(userWithPhysicalCart, cartWithPhysicalProducts);

		userWithDigitalCart = userService.getUserForUID("ahertz");
		cartWithDigitalProducts = userWithDigitalCart.getCarts().iterator().next();
		assertValidUserAndCart(userWithDigitalCart, cartWithDigitalProducts);
	}

	/**
	 * Given: externalTaxEnabled and non DigitalCart.
	 * When: checkout and Delivery Address.
	 * Then: Tax should not be zero
	 */
	@Test
	public void shouldCalculateTaxWhenDeliveryAddressIsSet() throws Exception
	{
		userService.setCurrentUser(userWithPhysicalCart);
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(FileTestUtils.loadFileAsString(REQUEST_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH),
								true, true)
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(FileTestUtils.loadFileAsString(RESPONSE_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH))
				));

		final AddressModel addressModel = createNewAddress(userWithPhysicalCart);
		final CommerceCheckoutParameter commerceCheckoutParameter = createCheckoutParameter(addressModel,
				cartWithPhysicalProducts);

		// set delivery address and recalculate cart
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertTrue(cartWithPhysicalProducts.getCalculated());
		Assert.assertEquals(addressModel, cartWithPhysicalProducts.getDeliveryAddress());
		Assert.assertEquals(Double.valueOf(7), cartWithPhysicalProducts.getTotalTax());
	}

	/**
	 * Given: externalTaxEnabled and non DigitalCart.
	 * When: checkout and Delivery Address is not set.
	 * Then: Tax should not be zero.
	 */
	@Test
	public void shouldCalculateTaxWhenDeliveryAddressIsNotSet() throws Exception
	{
		userService.setCurrentUser(userWithPhysicalCart);
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(FileTestUtils.loadFileAsString(REQUEST_BODY_WITHOUT_DELIVERY_ADDRESS_JSON_FILE_PATH),
								true, true)
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(FileTestUtils.loadFileAsString(RESPONSE_BODY_WITHOUT_DELIVERY_ADDRESS_JSON_FILE_PATH))
				));

		final CommerceCheckoutParameter commerceCheckoutParameter = createCheckoutParameter(null,
				cartWithPhysicalProducts);

		// set delivery address and recalculate cart
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertTrue(cartWithPhysicalProducts.getCalculated());
		Assert.assertEquals(Double.valueOf(15), cartWithPhysicalProducts.getTotalTax());
	}

	/**
	 * Given: externalTaxEnabled and Digital Cart.
	 * When: checkout and Payment Address is not set.
	 * Then: Tax should not be zero.
	 */
	@Test
	public void taxShouldBeZeroWhenPaymentAddressIsNotSet()
	{
		userWithDigitalCart.setDefaultPaymentAddress(null);

		modelService.save(userWithDigitalCart);

		userService.setCurrentUser(userWithDigitalCart);

		final CommerceCheckoutParameter commerceCheckoutParameter = createCheckoutParameter(null, cartWithDigitalProducts);

		// set delivery address and recalculate cart
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertTrue(cartWithDigitalProducts.getCalculated());
		Assert.assertEquals(Double.valueOf(0), cartWithDigitalProducts.getTotalTax());
	}

	/**
	 * Given: ExternalTax is enabled and already have calculated cart with tax.
	 * When: disable external tax
	 * Then: Tax should not be zero.
	 */
	@Test
	public void whenExternalTaxIsDisabledWithNoChangesInCartThenTaxShouldNotZero() throws Exception
	{
		userService.setCurrentUser(userWithDigitalCart);
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(FileTestUtils.loadFileAsString(REQUEST_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH),
								true, true)
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(FileTestUtils.loadFileAsString(RESPONSE_BODY_FOR_DISABLED_EXTERNAL_TAX_JSON_FILE_PATH))
				));

		// set delivery address on cart
		final AddressModel addressModel = createNewAddress(userWithDigitalCart);
		final CommerceCheckoutParameter commerceCheckoutParameter = createCheckoutParameter(addressModel,
				cartWithDigitalProducts);

		// set delivery address and recalculate cart
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertTrue(cartWithDigitalProducts.getCalculated());
		Assert.assertEquals(addressModel, cartWithDigitalProducts.getDeliveryAddress());
		Assert.assertEquals(Double.valueOf(2), cartWithDigitalProducts.getTotalTax());

		store.setExternalTaxEnabled(Boolean.FALSE);
		modelService.save(store);

		cartWithDigitalProducts.getStore().setExternalTaxEnabled(Boolean.FALSE);
		modelService.save(cartWithDigitalProducts);

		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);
		Assert.assertEquals(Double.valueOf(1), cartWithDigitalProducts.getTotalTax());
	}

	/**
	 * Given: ExternalTax is enabled and already have calculated cart with tax.
	 * When: disable external tax and change address
	 * Then: Tax should be zero.
	 */
	@Test
	public void whenExternalTaxIsDisabledCartIsChangedThenTaxShouldBeZero() throws Exception
	{
		userService.setCurrentUser(userWithDigitalCart);
		wireMock.stubFor(post(urlMatching(CALCULATE_TAXES_PATH))
				.withRequestBody(
						equalToJson(FileTestUtils.loadFileAsString(REQUEST_BODY_WITH_DELIVERY_ADDRESS_JSON_FILE_PATH),
								true, true)
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(FileTestUtils.loadFileAsString(RESPONSE_BODY_FOR_DISABLED_EXTERNAL_TAX_JSON_FILE_PATH))
				));

		// set delivery address on cart
		final AddressModel addressModel = createNewAddress(userWithDigitalCart);
		final CommerceCheckoutParameter commerceCheckoutParameter = createCheckoutParameter(addressModel,
				cartWithDigitalProducts);

		// set delivery address and recalculate cart
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertTrue(cartWithDigitalProducts.getCalculated());
		Assert.assertEquals(Double.valueOf(2), cartWithDigitalProducts.getTotalTax());

		store.setExternalTaxEnabled(Boolean.FALSE);
		modelService.save(store);

		cartWithDigitalProducts.getStore().setExternalTaxEnabled(Boolean.FALSE);
		modelService.save(cartWithDigitalProducts);

		userWithDigitalCart.setDefaultShipmentAddress(addressModel);
		userWithDigitalCart.setDefaultPaymentAddress(addressModel);
		modelService.save(userWithDigitalCart);

		commerceCheckoutParameter.setAddress(cartWithDigitalProducts.getUser().getDefaultPaymentAddress());
		welAgsCommerceCheckoutService.setDeliveryAddress(commerceCheckoutParameter);

		Assert.assertEquals(Double.valueOf(0), cartWithDigitalProducts.getTotalTax());
	}

	private AddressModel createNewAddress(final UserModel user)
	{
		final AddressModel addressModel = new AddressModel();

		addressModel.setBillingAddress(Boolean.TRUE);
		addressModel.setCountry(commonI18NService.getCountry(COUNTRY));
		addressModel.setStreetname(STREET_NAME);
		addressModel.setStreetnumber(STREET_NUMBER);
		addressModel.setPostalcode(POSTAL_CODE);
		addressModel.setTown(TOWN);
		addressModel.setFirstname(FIRST_NAME);
		addressModel.setLastname(LAST_NAME);
		addressModel.setOwner(user);

		modelService.save(addressModel);
		return addressModel;
	}

	private CommerceCheckoutParameter createCheckoutParameter(final AddressModel addressModel, final CartModel cart)
	{
		final CommerceCheckoutParameter commerceCheckoutParameter = new CommerceCheckoutParameter();

		commerceCheckoutParameter.setCart(cart);
		commerceCheckoutParameter.setAddress(addressModel);

		return commerceCheckoutParameter;
	}

	private void assertValidUserAndCart(final UserModel user, final CartModel cart)
	{
		Assert.assertEquals(user.getCarts().size(), 1);
		Assert.assertFalse(cart.getCalculated());
		Assert.assertEquals(Double.valueOf(0), cart.getTotalTax());
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) taxApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}
