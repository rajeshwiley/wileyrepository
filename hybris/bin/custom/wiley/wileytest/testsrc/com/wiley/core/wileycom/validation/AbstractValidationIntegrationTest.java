package com.wiley.core.wileycom.validation;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.validation.services.ValidationService;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javax.annotation.Resource;

import org.junit.Before;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.Matchers.containsString;


public abstract class AbstractValidationIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	protected static final String SIZE_VALIDATION_LOCALIZED_MESSAGE_TEMPLATE =
			"The number of characters in the attribute \"%s\" must between %d and %d (inclusive) in language: en.";

	protected static final String NOT_NULL_VALIDATION_MESSAGE_TEMPLATE =
			"The attribute \"%s\" must not be null";

	protected static final String THUMBNAIL_IMAGE_NAME = "80Wx109H.png";
	protected static final String VIDEO_THUMBNAIL_IMAGE_NAME = "80Wx109Hv.png";

	protected static final String NORMAL_IMAGE_NAME = "300Wx407H.png";
	protected static final String DESKTOP_IMAGE_NAME = "sample_book_background_desktop.png";
	protected static final String MOBILE_IMAGE_NAME = "sample_book_background_mobile.png";

	@Resource
	private ModelService modelService;
	@Resource
	private ProductService productService;
	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private ValidationService validationService;
	private CatalogVersionModel catalogVersion;

	@Before
	public void setup()
	{

		try
		{
			importCsv(
					"/wileytest/import/WileyDefaultValidationBaseProductIntegrationTest/constraints-and-catalog.impex",
					DEFAULT_ENCODING);
			importCsv(
					"/wileytest/import/WileyDefaultValidationBaseProductIntegrationTest/products-and-categories.impex",
					DEFAULT_ENCODING);
		}
		catch (final ImpExException e)
		{
			throw new RuntimeException("Test setup failed", e);
		}


		userService.setCurrentUser(userService.getAdminUser());
		catalogVersion = catalogVersionService.getCatalogVersion("wileyProductCatalog", "Staged");
	}




	protected void validateSizeConstraint(final Consumer<String> field, final String fieldName,
			final int min, final int max)
	{
		validateSizeConstraint(field, fieldName, min, max, true);
	}

	protected void validateSizeConstraint(final Consumer<String> field, final String fieldName,
			final int min, final int max, final boolean allowNull)
	{
		final String message = String.format(getSizeValidationMessageTemplate(), fieldName, min, max);
		failed(field, randomAlphabetic(min - 1), message);
		failed(field, randomAlphabetic(max + 1), message);
		if (allowNull)
		{
			succeeded(field, null);
		}
		succeeded(field, randomAlphabetic((min + max) / 2));
	}

	protected <T> void validateNotNullConstraint(final Consumer<T> field, final String fieldName, final T sampleNotNullValue)
	{
		final String message = String.format(NOT_NULL_VALIDATION_MESSAGE_TEMPLATE, fieldName);
		failed(field, null, message);
		succeeded(field, sampleNotNullValue);
	}



	protected <T> void failed(final Consumer<T> consumer, final T arg, final String exceptionMessage)
	{
		consumer.accept(arg);
		checkSaveFailed(exceptionMessage);
	}

	protected <T> void succeeded(final Consumer<T> consumer, final T arg)
	{
		consumer.accept(arg);
		validateTestInstance();
	}

	protected void checkSaveFailed(final String exceptionMessage)
	{
		try
		{
			validateTestInstance();
			fail("Validation unexpectedly succeeded");
		}
		catch (Exception e)
		{
			assertThat(e.getMessage(), containsString(exceptionMessage));
		}
	}

	protected abstract void validateTestInstance();

	protected void removeImageWithName(final MediaContainerModel images, final String imageName)
	{
		final List<MediaModel> medias = new ArrayList<>(images.getMedias());

		final MediaModel thumbnail = medias
				.stream()
				.filter(media -> media.getCode().equals(imageName))
				.findFirst()
				.get();

		medias.remove(thumbnail);

		images.setMedias(medias);
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected ProductService getProductService()
	{
		return productService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	protected CatalogVersionService getCatalogVersionService()
	{
		return catalogVersionService;
	}

	protected ValidationService getValidationService()
	{
		return validationService;
	}

	public CatalogVersionModel getCatalogVersion()
	{
		return catalogVersion;
	}

	protected String getSizeValidationMessageTemplate()
	{
		return SIZE_VALIDATION_LOCALIZED_MESSAGE_TEMPLATE;
	}
}
