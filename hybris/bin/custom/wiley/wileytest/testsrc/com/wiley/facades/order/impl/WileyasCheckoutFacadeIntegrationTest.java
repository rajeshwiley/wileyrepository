package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.constants.WileyTestConstants;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.order.WileyCheckoutFacade;

import static de.hybris.platform.order.impl.DefaultCartService.SESSION_CART_PARAMETER_NAME;


/**
 * Integration test for WileyCheckoutFacade
 */
@IntegrationTest
public class WileyasCheckoutFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String IMPEX_PATH = "/wileytest/import/cart/WileyasCheckoutFacadeIntegrationTest";
	private static final String COMMON_DATA_PATH = IMPEX_PATH + "/common.impex";
	private static final String CART_DATA_PATH = IMPEX_PATH + "/cart.impex";
	private static final String CART_GUID = "test-usd-cart-guid";
	private static final String CUSTOMER_ID = "test@test.com";

	@Resource
	private WileyCheckoutFacade wileyasCheckoutFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private SessionService sessionService;

	@Resource
	private CommerceCartService commerceCartService;

	@Before
	public void setUp() throws Exception
	{
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		importCsv(CART_DATA_PATH, DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite(WileyTestConstants.AS_SITE_ID, false);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
		setUpUser(CUSTOMER_ID);
	}

	@Test
	public void wileyasCheckoutCartConverterVerification()
	{
		final CartModel cartForGuidAndSite = commerceCartService.getCartForGuidAndSite(CART_GUID,
				baseSiteService.getCurrentBaseSite());
		assertNotNull("Can't select cart from DB", cartForGuidAndSite);

		sessionService.setAttribute(SESSION_CART_PARAMETER_NAME, cartForGuidAndSite);

		final CartData sessionCart = wileyasCheckoutFacade.getCheckoutCart();
		assertNotNull("Session cart not found", sessionCart);

		assertFalse("Entries expected", sessionCart.getEntries().isEmpty());
		assertNotNull("Subtotal price for entry expected", sessionCart.getEntries().get(0).getSubtotalPrice());
		assertTrue("Digital product expected", sessionCart.isDigitalCart());
		assertNotNull("Hosted session url expected", sessionCart.getHostedSessionScriptUrl());
		assertFalse("Delivery group expected", sessionCart.getDeliveryOrderGroups().isEmpty());
		assertEquals("Wrong Delivery items quantity", Long.valueOf(0), sessionCart.getDeliveryItemsQuantity());
		assertNotNull("Pickup group expected", sessionCart.getPickupOrderGroups());
		assertEquals("Wrong pickup items quantity", Long.valueOf(0), sessionCart.getPickupItemsQuantity());
		assertTrue("Taxes expected", sessionCart.isTaxAvailable());
		assertNotNull("Subtotal without discount is empty", sessionCart.getSubTotalWithoutDiscount());
		assertFalse("Don't show delivery cost for Digital product", sessionCart.isShowDeliveryCost());
		assertNotNull("PO number expected", sessionCart.getPurchaseOrderNumber());
		assertNotNull("Order configuration expected", sessionCart.getOrderConfiguration());
		assertNotNull("Order country expected", sessionCart.getCountry());
		assertNotNull("Payment Address expected", sessionCart.getPaymentAddress());
		assertNotNull("Payment mode expected", sessionCart.getPaymentMode());
		assertNotNull("Tax number expected", sessionCart.getTaxNumber());
		assertNotNull("Tax number validation expected", sessionCart.getTaxNumberValidated());
		assertNotNull("Payment Info expected", sessionCart.getPaymentInfo());
	}
}
