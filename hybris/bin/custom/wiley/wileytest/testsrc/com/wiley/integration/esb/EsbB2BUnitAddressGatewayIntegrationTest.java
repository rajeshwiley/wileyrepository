package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalAddressService;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;

import static java.util.Arrays.asList;


@IntegrationTest
public class EsbB2BUnitAddressGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(EsbB2BUnitAddressGatewayIntegrationTest.class);

	private static final String B2B_UNIT_ID = "Rustic";
	private static final String ADDR_ID = "123";
	private static final String RESOURCE_PREFIX = "/wileytest/integration/EsbB2BUnitAddressGatewayIntegrationTest";
	private static final String ADD_SHIPPING_ADDRESS_URL_PATTERN = "/b2b/accounts/sapAccount1/shippingAddresses";
	private static final RequestMethod ADD_SHIPPING_ADDRESS_REQUEST_METHOD = RequestMethod.POST;
	private static final String UPDATE_DELETE_SHIPPING_ADDRESS_URL_PATTERN =
			"/b2b/accounts/sapAccount1/shippingAddresses/" + ADDR_ID;
	private static final RequestMethod UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD = RequestMethod.PUT;
	private static final RequestMethod DELETE_SHIPPING_ADDRESS_REQUEST_METHOD = RequestMethod.DELETE;

	private static final List<String> ENABLED_SITES = asList("wileyb2b");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "ags", "wel");

	@Resource
	private WileyB2BUnitExternalAddressService wileyB2BUnitExternalAddressService;

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;
	private AddressModel addressModel;
	private B2BUnitModel b2bUnit;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		userService.setCurrentUser(userService.getAdminUser());

		importCsv(RESOURCE_PREFIX + "/impex/b2bUnitsData.impex", DEFAULT_ENCODING);
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		userService.setCurrentUser(userService.getAnonymousUser());

		b2bUnit = b2bUnitService.getUnitForUid(B2B_UNIT_ID);
		addressModel = b2bUnit.getAddresses().iterator().next();

		baseSiteService.setCurrentBaseSite("wileyb2b", true);
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void verifyAddShippingAddressSuccessCase() throws Exception
	{
		final StubMapping stubMapping = getStubMapping("/mappings/add_shipping_address.json", 200);

		wireMock.addStubMapping(stubMapping);


		String addressId = wileyB2BUnitExternalAddressService.addShippingAddress(b2bUnit, addressModel);

		assertEquals(ADDR_ID, addressId);

		wireMock.verify(1, getRequestPatternBuilder(
				ADD_SHIPPING_ADDRESS_URL_PATTERN,
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyUpdateShippingAddressSuccessCase() throws Exception
	{
		final StubMapping stubMapping = getStubMapping("/mappings/update_shipping_address.json", 204);

		wireMock.addStubMapping(stubMapping);
		addressModel.setExternalId(ADDR_ID);
		// ---------- When ---------
		boolean result = wileyB2BUnitExternalAddressService.updateShippingAddress(b2bUnit, addressModel);

		assertTrue(result);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				UPDATE_DELETE_SHIPPING_ADDRESS_URL_PATTERN,
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyDeleteShippingAddressSuccessCase() throws Exception
	{
		final StubMapping stubMapping = getStubMapping("/mappings/delete_shipping_address.json", 204);

		wireMock.addStubMapping(stubMapping);
		addressModel.setExternalId(ADDR_ID);
		// ---------- When ---------
		boolean result = wileyB2BUnitExternalAddressService.deleteShippingAddress(b2bUnit, addressModel.getExternalId());

		assertTrue(result);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				UPDATE_DELETE_SHIPPING_ADDRESS_URL_PATTERN,
				DELETE_SHIPPING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyAddShippingAddressWhen400StatusCode() throws Exception
	{
		verifyAddAddressError(400, ExternalSystemBadRequestException.class);
	}

	@Test
	public void verifyAddShippingAddressWhen401StatusCode() throws Exception
	{
		verifyAddAddressError(401, ExternalSystemInternalErrorException.class);
	}

	@Test
	public void verifyAddShippingAddressWhen403StatusCode() throws Exception
	{
		verifyAddAddressError(403, ExternalSystemInternalErrorException.class);
	}

	@Test
	public void verifyUpdateShippingAddressWhen400StatusCode() throws Exception
	{
		verifyUpdateAddressError(400, ExternalSystemBadRequestException.class);
	}

	@Test
	public void verifyUpdateShippingAddressWhen401StatusCode() throws Exception
	{
		verifyUpdateAddressError(401, ExternalSystemInternalErrorException.class);

	}
	@Test
	public void verifyUpdateShippingAddressWhen403StatusCode() throws Exception
	{
		verifyUpdateAddressError(403, ExternalSystemInternalErrorException.class);

	}

	@Test
	public void verifyDeleteShippingAddressWhen400StatusCode() throws Exception
	{
		verifyDeleteAddressError(400, ExternalSystemBadRequestException.class);
	}

	@Test
	public void verifyDeleteShippingAddressWhen401StatusCode() throws Exception
	{
		verifyDeleteAddressError(401, ExternalSystemInternalErrorException.class);

	}
	@Test
	public void verifyDeleteShippingAddressWhen403StatusCode() throws Exception
	{
		verifyDeleteAddressError(403, ExternalSystemInternalErrorException.class);

	}

	private void verifyAddAddressError(final int statusCode, final Class<? extends Exception> expectedExceptionClass)
			throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyB2BUnitExternalAddressService.addShippingAddress(b2bUnit, addressModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				statusCode, // response status code
				expectedExceptionClass,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	private void verifyUpdateAddressError(final int statusCode, final Class<? extends Exception> expectedExceptionClass)
			throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyB2BUnitExternalAddressService.updateShippingAddress(b2bUnit, addressModel), // executable method
				"/mappings/update_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				statusCode, // response status code
				expectedExceptionClass,  // expected exception
				UPDATE_DELETE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	private void verifyDeleteAddressError(final int statusCode, final Class<? extends Exception> expectedExceptionClass)
			throws Exception
	{
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> wileyB2BUnitExternalAddressService.deleteShippingAddress(b2bUnit, addressModel.getExternalId()),
				"/mappings/delete_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				statusCode, // response status code
				expectedExceptionClass,  // expected exception
				UPDATE_DELETE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				DELETE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);

		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("addShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(getStubMapping("/mappings/add_shipping_address.json", 200));
				wileyB2BUnitExternalAddressService.addShippingAddress(b2bUnit, addressModel);
				return null;
			}
		});

		funcMap.put("updateShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				addressModel.setExternalId(ADDR_ID);
				wireMock.addStubMapping(getStubMapping("/mappings/update_shipping_address.json", 204));
				wileyB2BUnitExternalAddressService.updateShippingAddress(b2bUnit, addressModel);
				return null;
			}
		});

		funcMap.put("deleteShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(getStubMapping("/mappings/delete_shipping_address.json", 204));
				wileyB2BUnitExternalAddressService.deleteShippingAddress(b2bUnit, addressModel.getExternalId());
				return null;
			}
		});
		return funcMap;
	}


	private StubMapping getStubMapping(final String path, int expectedResponseStatus) throws Exception
	{
		return setUpStubMapping(
				RESOURCE_PREFIX + path, // mapping
				expectedResponseStatus // response status
		);
	}
}
