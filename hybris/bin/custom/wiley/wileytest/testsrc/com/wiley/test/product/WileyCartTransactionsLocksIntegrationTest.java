package com.wiley.test.product;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jdbcwrapper.HybrisDataSource;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.HybrisTransactionManager;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.util.TestUtils;

import static de.hybris.platform.util.CSVConstants.HYBRIS_ENCODING;


/**
 * Contains tests for checking behavior of parallel and concurrent transactions with default hybris settings.<br/>
 * The purpose of the test cases are to have predictable behavior with concurrent transactions.
 * <b>Test cases working correctly with default hybris properties and on MySQL DB.
 * Changed DB or properties may make tests failed.</b>
 */
@IntegrationTest
public class WileyCartTransactionsLocksIntegrationTest extends ServicelayerTest
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyCartTransactionsLocksIntegrationTest.class);

	private static final String CART_1_UID = "transaction-test-cart";
	private static final String CART_2_UID = "transaction-test-cart2";
	private static final String USER_UID = "transaction-test-user";

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private CartFactory cartFactory;

	@Resource(name = "txManager")
	private HybrisTransactionManager hybrisTransactionManager;

	private TransactionTemplate template;

	@Before
	public void setUp() throws Exception
	{
		template = new TransactionTemplate(hybrisTransactionManager);

		importCsv("/wileytest/import/order/WileyCartTransactionsLocksIntegrationTest/common.impex", HYBRIS_ENCODING);
	}

	@After
	public void tearDown() throws Exception
	{
		importCsv("/wileytest/import/order/WileyCartTransactionsLocksIntegrationTest/clear_data.impex", HYBRIS_ENCODING);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-2 marked transaction as rollback only. ModelSavingException is thrown when lock wait timeout exceeded.
	 * (It is valid for MySQL. On HsqlDb there is no lock timeout and lock is kept.</li>
	 * <li>thread-2 closed transaction.</li>
	 * <li>thread-1 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingSameCartWithInnerTransaction() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch secondTransactionIsCompletedSignal = createSignal();

		CountDownLatch cart1IsUpdatedSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cart1IsUpdatedSignal.countDown();
					secondTransactionIsCompletedSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setTransactionClosedSignal(secondTransactionIsCompletedSignal)
				.setJob((transactionStatus) ->
				{
					cart1IsUpdatedSignal.await();

					final CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.APPROVED);

					TestUtils.expectException(ModelSavingException.class, () -> modelService.save(testCart));

					// Success, there is no infinite lock.
					// there is a lock timeout
					transactionStatus.setRollbackOnly();
					LOG.debug("{} marked transaction as rollback only.", Thread.currentThread().getName(), testCart.getCode());
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		// make sure that all threads are ready to start.
		startingPoint.await();

		shutdown(executorService, 70);

		// Then
		checkCartStatus(CART_1_UID, USER_UID, OrderStatus.CANCELLING);
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-1 closed transaction.</li>
	 * <li>thread-2 changed cart [transaction-test-master]</li>
	 * <li>thread-2 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingSameCartWithParallelTransactions() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch firstTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsUpdatedSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setTransactionClosedSignal(firstTransactionIsCompletedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cartIsUpdatedSignal.countDown();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					cartIsUpdatedSignal.await();

					final CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.APPROVED);

					firstTransactionIsCompletedSignal.await();

					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		startingPoint.await();

		shutdown(executorService, 30);

		// Then
		checkCartStatus(CART_1_UID, USER_UID, OrderStatus.APPROVED);
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-2 changed cart [transaction2-test-master]</li>
	 * <li>thread-2 closed transaction.</li>
	 * <li>thread-1 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingDifferentCartsWithInnerTransaction() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch secondTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsUpdatedSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cartIsUpdatedSignal.countDown();

					secondTransactionIsCompletedSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionClosedSignal(secondTransactionIsCompletedSignal)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					cartIsUpdatedSignal.await();

					final CartModel testCart = getCartModel(CART_2_UID, USER_UID);
					testCart.setStatus(OrderStatus.APPROVED);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		startingPoint.await();

		shutdown(executorService, 30);

		// Then
		checkCartStatus(CART_1_UID, USER_UID, OrderStatus.CANCELLING);
		checkCartStatus(CART_2_UID, USER_UID, OrderStatus.APPROVED);
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-2 changed cart [transaction2-test-master]</li>
	 * <li>thread-1 closed transaction.</li>
	 * <li>thread-2 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingDifferentCartsWithParallelTransactions() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch firstTransactionIsCompletedSignal = createSignal();

		CountDownLatch firstThreadUpdatedCartSignal = createSignal();
		CountDownLatch secondThreadUpdatedCartSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setTransactionClosedSignal(firstTransactionIsCompletedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());
					firstThreadUpdatedCartSignal.countDown();

					secondThreadUpdatedCartSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					firstThreadUpdatedCartSignal.await();

					final CartModel testCart = getCartModel(CART_2_UID, USER_UID);
					testCart.setStatus(OrderStatus.APPROVED);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());
					secondThreadUpdatedCartSignal.countDown();

					firstTransactionIsCompletedSignal.await();
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		startingPoint.await();

		shutdown(executorService, 30);

		// Then
		checkCartStatus(CART_1_UID, USER_UID, OrderStatus.CANCELLING);
		checkCartStatus(CART_2_UID, USER_UID, OrderStatus.APPROVED);
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-2 created cart [newCartTestCode]</li>
	 * <li>thread-2 closed transaction.</li>
	 * <li>thread-1 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingAndCreatingDifferentCartsWithInnerTransaction() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch secondTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsUpdatedSignal = createSignal();

		String newCartCode = "newCartTestCode";

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cartIsUpdatedSignal.countDown();
					secondTransactionIsCompletedSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionClosedSignal(secondTransactionIsCompletedSignal)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					cartIsUpdatedSignal.await();

					final CartModel testCart = cartFactory.createCart();
					testCart.setCode(newCartCode);
					testCart.setStatus(OrderStatus.APPROVED);
					modelService.save(testCart);
					LOG.debug("{} created cart [{}]", Thread.currentThread().getName(), testCart.getCode());
				});


		try
		{
			executorService.submit(worker1);
			executorService.submit(worker2);

			startingPoint.await();

			shutdown(executorService, 30);

			// Then
			checkCartStatus(CART_1_UID, USER_UID, OrderStatus.CANCELLING);
			checkCartStatus(newCartCode, userService.getCurrentUser().getUid(), OrderStatus.APPROVED);
			checkWorkerState(worker1);
			checkWorkerState(worker2);
		}
		finally
		{
			modelService.remove(getCartModel(newCartCode, userService.getCurrentUser().getUid()));
		}

	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-1 closed transaction.</li>
	 * <li>thread-2 created cart [newCartTestCode]</li>
	 * <li>thread-2 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingAndCreatingDifferentCartsWithParallelTransactions() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch firstTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsUpdatedSignal = createSignal();

		String newCartCode = "newCartTestCode";

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setTransactionClosedSignal(firstTransactionIsCompletedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.CANCELLING);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cartIsUpdatedSignal.countDown();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					cartIsUpdatedSignal.await();

					final CartModel testCart = cartFactory.createCart();
					testCart.setCode(newCartCode);
					testCart.setStatus(OrderStatus.APPROVED);
					modelService.save(testCart);
					LOG.debug("{} created cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					firstTransactionIsCompletedSignal.await();
				});

		try
		{
			executorService.submit(worker1);
			executorService.submit(worker2);

			startingPoint.await();

			shutdown(executorService, 30);

			// Then
			checkCartStatus(CART_1_UID, USER_UID, OrderStatus.CANCELLING);
			checkCartStatus(newCartCode, userService.getCurrentUser().getUid(), OrderStatus.APPROVED);
			checkWorkerState(worker1);
			checkWorkerState(worker2);
		}
		finally
		{
			modelService.remove(getCartModel(newCartCode, userService.getCurrentUser().getUid()));
		}

	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 read cart [transaction-test-master]</li>
	 * <li>thread-2 read cart [transaction-test-master]</li>
	 * <li>thread-1 removed cart [transaction-test-master]</li>
	 * <li>thread-2 marked transaction as rollback only. ModelSavingException is thrown when lock wait timeout exceeded.
	 * (It is valid for MySQL. On HsqlDb there is no lock timeout and lock is kept.</li>
	 * <li>thread-2 closed transaction.</li>
	 * <li>thread-1 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void updatingAndThenRemovingSameCartWithInnerTransaction() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch secondTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsReadyForRemovingSignal = createSignal();
		CountDownLatch cartIsRemovedSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setJob((transactionStatus) ->
				{
					cartIsReadyForRemovingSignal.await();

					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					final String testCartCode = testCart.getCode();
					modelService.remove(testCart);
					LOG.debug("{} removed cart [{}]", Thread.currentThread().getName(), testCartCode);

					cartIsRemovedSignal.countDown();
					secondTransactionIsCompletedSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionClosedSignal(secondTransactionIsCompletedSignal)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					final CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					final String testCartCode = testCart.getCode();

					cartIsReadyForRemovingSignal.countDown();
					cartIsRemovedSignal.await();

					testCart.setStatus(OrderStatus.APPROVED);

					TestUtils.expectException(ModelSavingException.class, () -> modelService.save(testCart));

					// Success
					// the cart should be no longer valid.
					transactionStatus.setRollbackOnly();
					LOG.debug("{} marked transaction as rollback only.", Thread.currentThread().getName(), testCartCode);
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		// make sure that all threads are ready to start.
		startingPoint.await();

		shutdown(executorService, 30);

		// Then
		assertNull(getCartModel(CART_1_UID, USER_UID));
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	/**
	 * Case:
	 * <ol>
	 * <li>thread-1 opened transaction.</li>
	 * <li>thread-2 opened transaction.</li>
	 * <li>thread-1 changed cart [transaction-test-master]</li>
	 * <li>thread-2 marked transaction as rollback only. ModelRemovalException is thrown when lock wait timeout exceeded.
	 * (It is valid for MySQL. On HsqlDb there is no lock timeout and lock is kept.</li>
	 * <li>thread-2 closed transaction.</li>
	 * <li>thread-1 closed transaction.</li>
	 * </ol>
	 */
	@Test
	public void removingAndThenUpdatingSameCartWithInnerTransaction() throws Exception
	{
		Assume.assumeFalse("Skip the test case for HsqlDb.", isHsqlDb());

		// Given
		final int countOfThreads = 2;
		final Tenant currentTenant = Registry.getCurrentTenantNoFallback();
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		CyclicBarrier startingPoint = new CyclicBarrier(countOfThreads + 1); // + 1 parent thread

		CountDownLatch firstTransactionIsStartedSignal = createSignal();
		CountDownLatch secondTransactionIsCompletedSignal = createSignal();

		CountDownLatch cartIsUpdatedSignal = createSignal();

		// When
		final Worker worker1 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionStartedSignal(firstTransactionIsStartedSignal)
				.setJob((transactionStatus) ->
				{
					CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					testCart.setStatus(OrderStatus.APPROVED);
					modelService.save(testCart);
					LOG.debug("{} changed cart [{}]", Thread.currentThread().getName(), testCart.getCode());

					cartIsUpdatedSignal.countDown();
					secondTransactionIsCompletedSignal.await();
				});

		final Worker worker2 = new Worker()
				.setTenant(currentTenant)
				.setTransactionTemplate(template)
				.setStartingPoint(startingPoint)
				.setTransactionClosedSignal(secondTransactionIsCompletedSignal)
				.setBeforeTransactionStart(firstTransactionIsStartedSignal::await)
				.setJob((transactionStatus) ->
				{
					cartIsUpdatedSignal.await();

					final CartModel testCart = getCartModel(CART_1_UID, USER_UID);
					final String testCartCode = testCart.getCode();

					TestUtils.expectException(ModelRemovalException.class, () -> modelService.remove(testCart));

					// Success
					// there is a lock timeout
					transactionStatus.setRollbackOnly();
					LOG.debug("{} marked transaction as rollback only.", Thread.currentThread().getName(),
							testCartCode);
				});

		executorService.submit(worker1);
		executorService.submit(worker2);

		// make sure that all threads are ready to start.
		startingPoint.await();

		shutdown(executorService, 70);

		// Then
		checkCartStatus(CART_1_UID, USER_UID, OrderStatus.APPROVED);
		checkWorkerState(worker1);
		checkWorkerState(worker2);
	}

	private void checkCartStatus(final String cartUid, final String userUid, final OrderStatus expectedStatus)
	{
		final CartModel testCart = getCartModel(cartUid, userUid);
		modelService.refresh(testCart);

		assertEquals(expectedStatus, testCart.getStatus());
	}

	private boolean isHsqlDb()
	{
		final HybrisDataSource dataSource = Registry.getCurrentTenant().getDataSource();
		if ("hsqldb".equals(dataSource.getDatabaseName()))
		{
			return true;
		}
		return false;
	}

	private CartModel getCartModel(final String cartUid, final String userUid)
	{
		return commerceCartService.getCartForCodeAndUser(cartUid,
				userService.getUserForUID(userUid));
	}

	private void shutdown(final ExecutorService executorService, final int timeout) throws InterruptedException, TimeoutException
	{
		executorService.shutdown();
		if (!executorService.awaitTermination(timeout, TimeUnit.SECONDS))
		{
			executorService.shutdownNow();
			throw new TimeoutException("Time is out for child threads.");
		}
	}

	private void checkWorkerState(final Worker worker)
	{
		assertTrue(worker.isCompleted());
		assertTrue(worker.isSuccessfully());
	}

	private CountDownLatch createSignal()
	{
		return new CountDownLatch(1);
	}

	private class Worker implements Runnable
	{

		private Tenant tenant;

		private TransactionTemplate transactionTemplate;

		private CyclicBarrier startingPoint;

		private CountDownLatch transactionStartedSignal;

		private CountDownLatch transactionClosedSignal;

		private Job beforeTransactionStart;

		private TransactionAwareJob job;

		private boolean successfully = false;

		private boolean completed = false;

		@Override
		public void run()
		{
			try
			{
				internalRun();
				successfully = true;
			}
			catch (Exception e)
			{
				LOG.error(e.getMessage(), e);
			}
			finally
			{
				completed = true;
			}
		}

		public void internalRun() throws Exception
		{
			Registry.setCurrentTenant(tenant);

			startingPoint.await();

			if (beforeTransactionStart != null)
			{
				beforeTransactionStart.execute();
			}
			try
			{
				transactionTemplate.execute(new TransactionCallbackWithoutResult()
				{
					@Override
					protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
					{
						try
						{
							if (transactionStartedSignal != null)
							{
								transactionStartedSignal.countDown();
							}
							LOG.debug("{} opened transaction.", Thread.currentThread().getName());
							job.execute(transactionStatus);
						}
						catch (Exception e)
						{
							LOG.debug("{}. Exception: {}", Thread.currentThread().getName(), e.toString());
							throw new RuntimeException(e);
						}
					}
				});
			}
			finally
			{
				LOG.debug("{} closed transaction.", Thread.currentThread().getName());
				if (transactionClosedSignal != null)
				{
					transactionClosedSignal.countDown();
				}
			}
		}

		public Worker setTenant(final Tenant tenant)
		{
			this.tenant = tenant;
			return this;
		}

		public Worker setTransactionTemplate(final TransactionTemplate transactionTemplate)
		{
			this.transactionTemplate = transactionTemplate;
			return this;
		}

		public Worker setStartingPoint(final CyclicBarrier startingPoint)
		{
			this.startingPoint = startingPoint;
			return this;
		}

		public Worker setBeforeTransactionStart(
				final Job beforeTransactionStart)
		{
			this.beforeTransactionStart = beforeTransactionStart;
			return this;
		}

		public Worker setJob(final TransactionAwareJob job)
		{
			this.job = job;
			return this;
		}

		public Worker setTransactionStartedSignal(final CountDownLatch transactionStartedSignal)
		{
			this.transactionStartedSignal = transactionStartedSignal;
			return this;
		}

		public Worker setTransactionClosedSignal(final CountDownLatch transactionClosedSignal)
		{
			this.transactionClosedSignal = transactionClosedSignal;
			return this;
		}

		public boolean isSuccessfully()
		{
			return successfully;
		}

		public boolean isCompleted()
		{
			return completed;
		}
	}

	private interface Job
	{
		void execute() throws Exception;
	}

	private interface TransactionAwareJob
	{
		void execute(TransactionStatus transactionStatus) throws Exception;
	}

}
