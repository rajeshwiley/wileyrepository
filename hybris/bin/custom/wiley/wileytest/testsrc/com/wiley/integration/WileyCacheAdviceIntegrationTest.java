package com.wiley.integration;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.cache.Cache;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.client.HttpServerErrorException;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;


@IntegrationTest
public class WileyCacheAdviceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String REQUEST_MESSAGE = "<root>WileyCacheAdviceIntegrationTest Request Message</root>";
	private static final String RESPONSE_MESSAGE = "WileyCacheAdviceIntegrationTest Response Message";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));

	private static final String TEST_SERVICE_URI = "/cachedTestService";

	@Resource(name = "wileyTestRegionCache")
	private Cache wileyTestRegionCache;

	@Resource
	private WileyTestCachingGateway wileyTestCachingGateway;

	@Rule
	public final WireMockRule wireMock = new WireMockRule(wireMockConfig().port(WIREMOCK_PORT));

	@Before
	public void setUp()
	{
		wileyTestRegionCache.clear();
	}

	@Test
	public void responseShouldNotBeInCache()
	{
		verifyResponseNotStoredInCache();
	}

	@Test
	public void shouldReturnCachedResponse()
	{
		//Given
		setUpWireMockRemoteEndpointWithAnswer(HttpStatus.OK);

		// When send request to integration gateway first time
		final String firstInvocationResponse = wileyTestCachingGateway.send(REQUEST_MESSAGE);

		//Then
		verifyResponseStoredInCache();
		verifyCorrectResponse(firstInvocationResponse);

		// When send request to integration gateway second time
		final String secondInvocationResponse = wileyTestCachingGateway.send(REQUEST_MESSAGE);

		//Then second request should be not sent to remote endpoint
		verifyTotalNumberOfRequestsSentToRemoteEndpoint(1);
		verifyCorrectResponse(secondInvocationResponse);
	}

	@Test
	public void shouldNotStoreErrorResponseInCache()
	{
		//Given
		setUpWireMockRemoteEndpointWithAnswer(HttpStatus.INTERNAL_SERVER_ERROR);

		//When send request to integration gateway and handle error response
		try
		{
			wileyTestCachingGateway.send(REQUEST_MESSAGE);
			fail();
		}
		catch (HttpServerErrorException exception)
		{
		}

		//Then
		verifyResponseNotStoredInCache();

		//Given
		setUpWireMockRemoteEndpointWithAnswer(HttpStatus.OK);

		// When send request to integration gateway second time
		final String secondInvocationResponse = wileyTestCachingGateway.send(REQUEST_MESSAGE);

		//Then
		verifyTotalNumberOfRequestsSentToRemoteEndpoint(2);
		verifyCorrectResponse(secondInvocationResponse);
	}

	private void verifyTotalNumberOfRequestsSentToRemoteEndpoint(final int numberOfInvocations)
	{
		verify(numberOfInvocations, getRequestedFor(urlEqualTo(TEST_SERVICE_URI)));
	}

	private void verifyCorrectResponse(final String firstInvocationResponse)
	{
		assertEquals(RESPONSE_MESSAGE, firstInvocationResponse);
	}

	private void verifyResponseNotStoredInCache()
	{
		assertNull(wileyTestRegionCache.get(REQUEST_MESSAGE));
	}

	private void verifyResponseStoredInCache()
	{
		GenericMessage message = (GenericMessage) wileyTestRegionCache.get(REQUEST_MESSAGE).get();
		assertEquals(RESPONSE_MESSAGE, message.getPayload());
	}

	private void setUpWireMockRemoteEndpointWithAnswer(final HttpStatus httpStatus)
	{
		wireMock.
				stubFor(get(urlMatching(TEST_SERVICE_URI))
						.willReturn(
								aResponse()
										.withStatus(httpStatus.value())
										.withBody(RESPONSE_MESSAGE)
						));
	}
}
