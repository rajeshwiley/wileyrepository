package com.wiley.integration;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;
import de.hybris.platform.voucher.VoucherService;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;

import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.RequestListener;
import com.github.tomakehurst.wiremock.http.Response;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.integration.ebp.EbpGateway;
import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.integration.ebp.dto.EbpAddProductsResponse;
import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.core.integration.ebp.dto.EbpCreateUserResponse;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToXml;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.wiley.utils.FileTestUtils.loadFileAsString;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


/**
 * Created by Maksim_Kozich on 01.04.2016.
 */
@IntegrationTest
public class EbpGatewayIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final Logger LOG = Logger.getLogger(EbpGatewayIntegrationTest.class);

	private static final String CLIENT_ID_PATTERN = "CLIENT_ID";
	private static final String ORDER_DATE_TIME_PATTERN = "ORDER_DATE_TIME";

	private static final String WEL_SITE_UID = "wel";

	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));

	// test outbound gateway uri is overwritten via configs/common/local_tenant_junit.properties
	private static final String EPB_MOCK_PATH = "/ebpv4/jaxws";

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/EbpGatewayIntegrationTest/";

	private static final String TEST_EBP_ORDER_CODE = "ebpOrder";
	private static final String EBP_ORDER_KPMG = "ebpOrderKpmg";
	private static final String EBP_ORDER_STUDENT_US = "ebpOrderStudentUS";
	private static final String EBP_ORDER_STUDENT_NON_US = "ebpOrderStudentNonUS";

	private static final String TEST_VOUCHER_CODE = "xyz-MHE2-B8L5-LPHE";

	// we use value of WileyFulfilmentProcessConstants.WILEY_EBP_DATE_TIME_FORMAT_NAME intentionally to avoid false positive
	private static final String EBP_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";


	@Rule
	public final WireMockRule wireMock = new WireMockRule(wireMockConfig().port(WIREMOCK_PORT));

	@Resource
	private EbpGateway ebpGateway;

	@Resource(name = "retryEbpAdvice")
	private RequestHandlerRetryAdvice retryEbpAdvice;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private VoucherService voucherService;

	@Resource
	private SetupImpexService setupImpexService;

	@Resource
	private UserService userService;

	@Before
	public void before() throws Exception
	{
		userService.setCurrentUser(userService.getAdminUser());

		setupImpexService.importImpexFile("/wileytest/import/order/EbpGatewayIntegrationTest/orders.impex", true);
		wireMock.addMockServiceRequestListener(new RequestListener()
		{
			@Override
			public void requestReceived(final Request request, final Response response)
			{
				LOG.info(request);
				LOG.info(response);
			}
		});
	}

	@Test
	public void testCreateEbpUserAndStudentUS() throws Exception
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(EBP_ORDER_STUDENT_US, baseStore);

		wireMock.
				stubFor(post(urlMatching(EPB_MOCK_PATH))
						.withRequestBody(equalToXml(
								loadFileAsString(TEST_REQUESTS_PATH + "request_create_ebp_user_and_student_us.xml")
										.replaceAll(CLIENT_ID_PATTERN, ((CustomerModel) order.getUser()).getCustomerID())
								)
						).willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "text/xml; charset=utf-8")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + "response_create_user_201.xml"))
						));



		// When
		EbpCreateUserResponse response = ebpGateway.createEbpUser(createEbpCreateUserPayload(order));

		// Then
		assertNotNull(response.getResponse());
		assertEquals("User Created", response.getResponse().getStatusMessage());
		assertEquals(201, response.getResponse().getResponseCode());
	}

	@Test
	public void testUpdateEbpUserAndStudentNonUS() throws Exception
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(EBP_ORDER_STUDENT_NON_US, baseStore);

		wireMock.
				stubFor(post(urlMatching(EPB_MOCK_PATH))
						.withRequestBody(equalToXml(
								loadFileAsString(TEST_REQUESTS_PATH + "request_update_ebp_user_and_srudent_non_us.xml")
										.replaceAll(CLIENT_ID_PATTERN, ((CustomerModel) order.getUser()).getCustomerID())
								)
						).willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "text/xml; charset=utf-8")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + "response_create_user_200.xml"))
						));

		// When
		EbpCreateUserResponse response = ebpGateway.createEbpUser(createEbpCreateUserPayload(order));

		// Then
		assertNotNull(response.getResponse());
		assertEquals(response.getResponse().getStatusMessage(), "User information updated");
		assertEquals(response.getResponse().getResponseCode(), 200);
	}

	@Test
	public void testAddProductsRegularAndManualWithPin() throws Exception
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(TEST_EBP_ORDER_CODE, baseStore);
		SimpleDateFormat ebpFormat = new SimpleDateFormat(EBP_DATE_TIME_FORMAT, Locale.US);

		wireMock.
				stubFor(post(urlMatching(EPB_MOCK_PATH))
						.withRequestBody(equalToXml(
								loadFileAsString(TEST_REQUESTS_PATH + "request_add_products_regular_and_manual.xml")
										.replaceAll(CLIENT_ID_PATTERN, ((CustomerModel) order.getUser()).getCustomerID())
										.replaceAll(ORDER_DATE_TIME_PATTERN, ebpFormat.format(order.getDate()))
						))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "text/xml; charset=utf-8")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + "response_add_products_200.xml"))
						));

		// When
		EbpAddProductsResponse response = ebpGateway.addProducts(createEbpAddProductsPayload(order));

		// Then
		assertNotNull(response.getResponse());
		assertEquals(response.getResponse().getStatusMessage(), "User information updated");
		assertEquals(response.getResponse().getResponseCode(), 200);
	}




	@Test
	public void testAddProductsKpmg() throws Exception
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(EBP_ORDER_KPMG, baseStore);
		SimpleDateFormat ebpFormat = new SimpleDateFormat(EBP_DATE_TIME_FORMAT, Locale.US);

		wireMock.
				stubFor(post(urlMatching(EPB_MOCK_PATH))
						.withRequestBody(equalToXml(
								loadFileAsString(TEST_REQUESTS_PATH + "request_add_products_kpmg.xml")
										.replaceAll(CLIENT_ID_PATTERN, ((CustomerModel) order.getUser()).getCustomerID())
										.replaceAll(ORDER_DATE_TIME_PATTERN, ebpFormat.format(order.getDate()))

						))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "text/xml; charset=utf-8")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + "response_add_products_200.xml"))
						));

		// When
		EbpAddProductsResponse response = ebpGateway.addProducts(createEbpAddProductsPayload(order));

		// Then
		assertNotNull(response.getResponse());
		assertEquals(response.getResponse().getStatusMessage(), "User information updated");
		assertEquals(response.getResponse().getResponseCode(), 200);
	}

	@Test
	public void testTimeoutRetry() throws Exception
	{
		// Given

		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);
		final OrderModel order = customerAccountService.getOrderForCode(EBP_ORDER_STUDENT_NON_US, baseStore);

		wireMock.
				stubFor(post(urlMatching(EPB_MOCK_PATH))
						.withRequestBody(equalToXml(
								loadFileAsString(TEST_REQUESTS_PATH + "request_update_ebp_user_and_srudent_non_us.xml")
										.replaceAll(CLIENT_ID_PATTERN, ((CustomerModel) order.getUser()).getCustomerID())
								)
						).willReturn(
								aResponse()
										.withStatus(200)
										.withFixedDelay(120000)
										.withHeader("Content-Type", "text/xml; charset=utf-8")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + "response_create_user_200.xml"))
						));

		// When
		try
		{
			ebpGateway.createEbpUser(createEbpCreateUserPayload(order));
		}
		catch (Exception e)
		{

		}

		// Then
		verify(3, postRequestedFor(urlEqualTo(EPB_MOCK_PATH)));
	}


	/**
	 * Mimics NotifyEBPAction method
	 *
	 * @param order
	 * @return
	 */
	private EbpCreateUserPayload createEbpCreateUserPayload(final OrderModel order)
	{
		EbpCreateUserPayload ebpCreateUserPayload = new EbpCreateUserPayload();
		ebpCreateUserPayload.setCustomer((CustomerModel) order.getUser());
		ebpCreateUserPayload.setUniversity(order.getUniversity());
		ebpCreateUserPayload.setOrder(order);
		ebpCreateUserPayload.setUniversity(order.getUniversity());
		ebpCreateUserPayload.setUniversityCountry(order.getUniversityCountry());
		ebpCreateUserPayload.setUniversityState(order.getUniversityState());
		return ebpCreateUserPayload;
	}

	private EbpAddProductsPayload createEbpAddProductsPayload(final OrderModel order)
	{
		EbpAddProductsPayload ebpAddProductsPayload = new EbpAddProductsPayload();
		ebpAddProductsPayload.setOrder(order);
		return ebpAddProductsPayload;
	}

}
