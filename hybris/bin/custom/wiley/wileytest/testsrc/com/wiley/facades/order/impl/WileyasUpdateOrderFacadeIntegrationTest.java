package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.exceptions.WileyOrderCreditCardDetailsNotValidException;
import com.wiley.core.exceptions.WileyOrderHasActiveOrFailedFulfilmentProcessException;
import com.wiley.core.exceptions.WileyOrderNonEditableException;
import com.wiley.core.mpgs.services.impl.WileyMPGSMessageServiceImpl;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.as.order.WileyasUpdateOrderFacade;
import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import com.wiley.facades.wiley.order.dto.OrderUpdateRequestDTO;
import com.wiley.integration.apigee.AbstractExternalTaxesRelatedIntegrationTest;

import static com.wiley.facades.constants.WileyFacadesConstants.CURRENT_PAYMENT_OPTION_NAME;


@IntegrationTest
public class WileyasUpdateOrderFacadeIntegrationTest extends AbstractExternalTaxesRelatedIntegrationTest
{
	private static final String IMPEX_BASE_PATH = "/wileytest/import/order/WileyasUpdateOrderFacadeIntegrationTest";
	private static final String MAPPING_BASE_PATH = "/wileytest/integration/WileyasUpdateOrderFacadeIntegrationTest";
	private static final String MAPPING_RETRIEVE_SESSION_MAPPING_PATH =
			MAPPING_BASE_PATH + "/mappings/retrieve_session_mapping.json";
	private static final String MAPPING_RETRIEVE_SESSION_SUCCESS_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/retrieve_session_success.json";
	private static final String MAPPING_RETRIEVE_SESSION_ERROR_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/retrieve_session_error.json";
	private static final String MAPPING_VERIFY_MAPPING_PATH =
			MAPPING_BASE_PATH + "/mappings/verify_mapping.json";
	private static final String MAPPING_VERIFY_SUCCESS_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/verify_success.json";
	private static final String MAPPING_VERIFY_ERROR_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/verify_error.json";
	private static final String MAPPING_TOKENIZATION_MAPPING_PATH =
			MAPPING_BASE_PATH + "/mappings/tokenization_mapping.json";
	private static final String MAPPING_TOKENIZATION_SUCCESS_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/tokenization_success.json";
	private static final String MAPPING_TOKENIZATION_ERROR_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/tokenization_error.json";
	private static final String MAPPING_AUTHORIZATION_MAPPING_PATH =
			MAPPING_BASE_PATH + "/mappings/authorization_mapping.json";
	private static final String MAPPING_AUTHORIZATION_SUCCESS_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/authorization_success.json";
	private static final String MAPPING_AUTHORIZATION_ERROR_RESPONSE_PATH =
			MAPPING_BASE_PATH + "/mappings/response/authorization_error.json";
	private static final String COMMON_DATA_PATH = IMPEX_BASE_PATH + "/common.impex";
	private static final String MAPPING_VAT_PATH = MAPPING_BASE_PATH + "/mappings/vat_mapping.json";
	private static final String MAPPING_VAT_VALID = MAPPING_BASE_PATH + "/mappings/response/vat_response_valid.xml";
	private static final String MAPPING_VAT_INVALID = MAPPING_BASE_PATH + "/mappings/response/vat_response_invalid.xml";

	private static final String CUSTOMER_ID = "alm_id1";
	private static final String BASE_SITE_ID = "asSite";
	private static final String EDIT_ORDER_CODE = "asEditTestOrder";
	private static final String ASM_EDIT_ORDER_CODE = "asAsmEditTestorder";
	private static final String EDIT_ORDER_MPGS_SESSION_ID = "asEditTestMPGSSessionId";
	private static final String EDIT_ORDER_DATA_PATH = IMPEX_BASE_PATH + "/edit-order.impex";
	private static final String EDIT_ORDER_WITH_PAID_ENTRIES = IMPEX_BASE_PATH + "/edit-order-cc-paid.impex";
	private static final String EDIT_ORDER_WITH_ENTRY_WITH_BUSINESSKEY_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-with-entry-with-businesskey.impex";
	private static final String EDIT_ORDER_CC_DATA_PATH = IMPEX_BASE_PATH + "/edit-order-cc.impex";
	private static final String EDIT_ORDER_SET_CALCULATED_DATA_PATH = IMPEX_BASE_PATH + "/edit-order-set-calculated.impex";
	private static final String EDIT_ORDER_CHANGE_PRICE_DATA_PATH = IMPEX_BASE_PATH + "/edit-order-change-price.impex";
	private static final String EDIT_ORDER_CC_SET_CALCULATED_DATA_PATH = IMPEX_BASE_PATH + "/edit-order-cc-set-calculated.impex";
	private static final String EDIT_ORDER_CC_SET_AMOUNT_INCREASED_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-cc-set-amount-increased.impex";
	private static final String EDIT_ORDER_WRON_SOURCE_SYSTEM_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-wrong-source-system.impex";
	private static final String EDIT_ZERO_ORDER_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-zero-order.impex";
	private static final String EDIT_ORDER_NOT_EDITABLE_ENTRIES_DATA_PATH =
			IMPEX_BASE_PATH + "/edit-order-not-editable-entry.impex";
	private static final String EDIT_ORDER_ACTIVE_BP_PATH = IMPEX_BASE_PATH + "/edit-order-active-bp.impex";
	private static final String EDIT_ORDER_FAILED_BP_PATH = IMPEX_BASE_PATH + "/edit-order-failed-bp.impex";
	private static final String EDIT_ORDER_EXTERNAL_PRICES_AND_DISCOUNTS =
			IMPEX_BASE_PATH + "/edit-order-external-prices-and-discounts.impex";
	private static final Double ORDER_ENTRY_EXTERNAL_PRICE_EUR = 21.29;
	private static final Double ORDER_ENTRY_EXTERNAL_DISCOUNT_EUR = 2.9;
	// taxes-calculate-200.json is applied
	private static final Double ORDER_ENTRY_EXTERNAL_TAX_EUR = 3.31;
	private static final Integer ORDER_ENTRIES_NUMBER = 4;
	private static final Integer ORDER_ENTRIES_NOT_CANCELLED_NUMBER = 3;
	private static final Integer ORDER_ENTRIES_QUANTITY = 1;
	private static final Double ORDER_ENTRIES_SUBTOTAL_USD = 20.0;
	private static final Double ORDER_ENTRIES_SUBTOTAL_USD_SUM =
			ORDER_ENTRIES_SUBTOTAL_USD * ORDER_ENTRIES_NUMBER * ORDER_ENTRIES_QUANTITY;
	private static final Double ORDER_ENTRIES_PRODUCT_PRICE_EUR = 50.00;
	private static final Double ORDER_ENTRIES_PRODUCT_PRICE_EUR_SUM =
			ORDER_ENTRIES_PRODUCT_PRICE_EUR * ORDER_ENTRIES_NOT_CANCELLED_NUMBER * ORDER_ENTRIES_QUANTITY;
	private static final String DE_ISO_CODE = "DE";
	private static final String US_ISO_CODE = "US";
	private static final String EUR_ISO_CODE = "EUR";
	private static final String USD_ISO_CODE = "USD";
	private static final String AGENT_ID = "asagent1";
	private static final String AGENT_PASSWORD = "1234";
	private static final String TAX_VAT_NUMBER = "1234567890";
	private static final String ADDITIONAL_INFO_DEPARTMENT = "department_test";
	private static final String ADDITIONAL_INFO_COMPANY = "company_test";

	@Resource
	private WileyasUpdateOrderFacade wileyasUpdateOrderFacade;

	@Resource
	private WileyasOrderFacade wileyasOrderFacade;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private ModelService modelService;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private WileyAssistedServiceFacade wileyAssistedServiceFacade;

	@Before
	public void importData() throws Exception
	{
		baseSiteService.setCurrentBaseSite(BASE_SITE_ID, false);
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void editOrderConfirmShouldFailForZeroOrder() throws ImpExException
	{
		verifyEditOrderFailed(EDIT_ZERO_ORDER_DATA_PATH, EDIT_ORDER_CODE, true,
				WileyOrderNonEditableException.class);
	}

	@Test
	public void editOrderConfirmShouldFailForOrderWithNotEditableEntries() throws ImpExException
	{
		verifyEditOrderFailed(EDIT_ORDER_NOT_EDITABLE_ENTRIES_DATA_PATH, EDIT_ORDER_CODE, true,
				WileyOrderNonEditableException.class);
	}

	@Test
	public void editOrderConfirmShouldFailForActiveBp() throws ImpExException
	{
		verifyEditOrderFailed(EDIT_ORDER_ACTIVE_BP_PATH, EDIT_ORDER_CODE, true,
				WileyOrderHasActiveOrFailedFulfilmentProcessException.class);
	}

	@Test
	public void editOrderConfirmShouldFailForFailedBp() throws ImpExException
	{
		verifyEditOrderFailed(EDIT_ORDER_FAILED_BP_PATH, EDIT_ORDER_CODE, true,
				WileyOrderHasActiveOrFailedFulfilmentProcessException.class);
	}


	@Test
	public void testUpdateOrderDoesNotFailIfRetrieveSessionErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		// then
		// no exception is thrown
	}


	@Test(expected = WileyOrderCreditCardDetailsNotValidException.class)
	public void testUpdateOrderConfirmFailsIfRetrieveSessionErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		// set order calculated to make sure it fails even when total not increased
		importCsv(EDIT_ORDER_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		// then
		// exception is thrown
	}


	@Test
	public void testUpdateOrderDoesNotFailIfTokenizationErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(true);
		setUpTokenizationWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		// then
		// no exception is thrown
	}

	@Test(expected = WileyOrderCreditCardDetailsNotValidException.class)
	public void testUpdateOrderConfirmFailsIfVerifyErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		// set order calculated to make sure it fails even when total not increased
		importCsv(EDIT_ORDER_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		// then
		// exception is thrown
	}

	@Test(expected = WileyOrderCreditCardDetailsNotValidException.class)
	public void testUpdateOrderConfirmFailsIfTokenizationErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		// set order calculated to make sure it fails even when total not increased
		importCsv(EDIT_ORDER_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		// then
		// exception is thrown
	}

	@Test
	public void testUpdateOrderDoesNotFailIfAuthorizationErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(true);
		setUpAuthorizationWiremockMappings(false);
		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		// then
		// no exception is thrown
	}

	@Test(expected = WileyOrderCreditCardDetailsNotValidException.class)
	public void testUpdateOrderConfirmFailsIfAuthorizationErrors() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		// set order calculated to make sure it fails even when total not increased
		importCsv(EDIT_ORDER_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);

		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(true);
		setUpAuthorizationWiremockMappings(false);

		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		// then
		// exception is thrown

	}

	@Test
	public void testUpdateOrderConfirmDoesNotAuthorizeIfAmountNotChangedAndPaymentOptionCurrent() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_CC_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CC_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);

		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(true);
		setUpAuthorizationWiremockMappings(false);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateCurrentPaymentOoptionRequestDTO();

		// when
		wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		// then
		// no exception is thrown
	}

	@Test
	public void testUpdateOrderConfirmFailsIfAmountIncreased() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_CC_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CC_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CC_SET_AMOUNT_INCREASED_DATA_PATH, DEFAULT_ENCODING);

		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(true);
		setUpAuthorizationWiremockMappings(true);

		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();

		try
		{
			// when
			wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);
			fail("IllegalStateException is expected");
		}
		catch (IllegalStateException ex)
		{
			// then
			assertEquals("Unable to create transactions refund list. Not enough available transactions to refund",
					ex.getMessage());
		}
	}

	@Test
	public void testUpdateOrderConfirmDoesAuthorizeIfAmountNotChangedButPaymentModeChangedToCard() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_CC_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CC_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);

		setUpRetrieveSessionWiremockMappings(true);
		setUpVerifyWiremockMappings(true);
		setUpTokenizationWiremockMappings(true);
		setUpAuthorizationWiremockMappings(false);

		OrderUpdateRequestDTO updateRequestDTO = getCreditCardOrderUpdateRequestDTO();
		try
		{
			// when
			wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);
			fail("WileyOrderCreditCardDetailsNotValidException is expected");
		}
		catch (WileyOrderCreditCardDetailsNotValidException ex)
		{
			// then
			assertEquals(WileyMPGSMessageServiceImpl.MESSAGE_KEY_HYBRIS_EXCEPTION, ex.getErrorCode());
		}
	}

	private OrderUpdateRequestDTO getCreditCardOrderUpdateRequestDTO()
	{
		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);
		updateRequestDTO.setPaymentMode(PaymentModeEnum.CARD);
		updateRequestDTO.setSessionId(EDIT_ORDER_MPGS_SESSION_ID);
		return updateRequestDTO;
	}

	private void setUpRetrieveSessionWiremockMappings(boolean retrieveSessionSuccess) throws Exception
	{
		StubMapping retrieveSessionMapping = setUpStubMapping(MAPPING_RETRIEVE_SESSION_MAPPING_PATH,
				retrieveSessionSuccess ?
						MAPPING_RETRIEVE_SESSION_SUCCESS_RESPONSE_PATH :
						MAPPING_RETRIEVE_SESSION_ERROR_RESPONSE_PATH);
		wireMock.addStubMapping(retrieveSessionMapping);
	}

	private void setUpVerifyWiremockMappings(boolean verifySuccess) throws Exception
	{
		StubMapping retrieveSessionMapping = setUpStubMapping(MAPPING_VERIFY_MAPPING_PATH,
				verifySuccess ? MAPPING_VERIFY_SUCCESS_RESPONSE_PATH : MAPPING_VERIFY_ERROR_RESPONSE_PATH);
		wireMock.addStubMapping(retrieveSessionMapping);
	}

	private void setUpTokenizationWiremockMappings(boolean tokenizationSuccess) throws Exception
	{
		StubMapping tokenizationMapping = setUpStubMapping(MAPPING_TOKENIZATION_MAPPING_PATH,
				tokenizationSuccess ?
						MAPPING_TOKENIZATION_SUCCESS_RESPONSE_PATH :
						MAPPING_TOKENIZATION_ERROR_RESPONSE_PATH);
		wireMock.addStubMapping(tokenizationMapping);
	}

	private void setUpAuthorizationWiremockMappings(boolean authorizationSuccess) throws Exception
	{
		StubMapping authorizationMapping = setUpStubMapping(MAPPING_AUTHORIZATION_MAPPING_PATH,
				authorizationSuccess ?
						MAPPING_AUTHORIZATION_SUCCESS_RESPONSE_PATH :
						MAPPING_AUTHORIZATION_ERROR_RESPONSE_PATH);
		wireMock.addStubMapping(authorizationMapping);
	}

	private void setUpViesVatWiremockMappings(boolean authorizationSuccess) throws Exception
	{
		StubMapping authorizationMapping = setUpStubMapping(MAPPING_VAT_PATH,
				authorizationSuccess ?
						MAPPING_VAT_VALID :
						MAPPING_VAT_INVALID);
		wireMock.addStubMapping(authorizationMapping);
	}

	private void verifyEditOrderFailed(final String csvPath, final String orderCode, boolean saveChanges,
			final Class exceptionClass) throws ImpExException
	{
		// given
		importCsv(csvPath, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(orderCode);

		// when
		try
		{
			wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, saveChanges);
			fail("exception should be thrown");
		}
		catch (Exception e)
		{
			// then
			assertEquals("unexpected exception", exceptionClass, e.getClass());
		}
	}


	@Test
	public void testUpdateOrderSuccess() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		// entries statuses are not updated
		List<OrderStatus> expectedEntriesStatuses = Arrays.asList(
				OrderStatus.CREATED,
				OrderStatus.INVOICE_PENDING,
				OrderStatus.INVOICE_OVERDUE,
				OrderStatus.CANCELLED);
		verifyOrderStatuses(result, expectedEntriesStatuses);

		assertNotNull(result.getTotalPrice());
		assertEquals(USD_ISO_CODE, result.getTotalPrice().getCurrencyIso());

		assertNotNull(result.getTotalTax());
		assertEquals(USD_ISO_CODE, result.getTotalTax().getCurrencyIso());
	}


	@Test
	public void testUpdateOrderWithPaidEntriesChangesNotCancelledEntriesStatusToCreated() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_WITH_PAID_ENTRIES, DEFAULT_ENCODING);
		wileyAssistedServiceFacade.launchAssistedServiceMode();
		wileyAssistedServiceFacade.loginAssistedServiceAgent(AGENT_ID, AGENT_PASSWORD);
		wileyAssistedServiceFacade.emulateCustomer(CUSTOMER_ID, null);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);
		updateRequestDTO.setPaymentMode(PaymentModeEnum.INVOICE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		// paid entries statuses changed to CREATED except CANCELLED entries
		List<OrderStatus> expectedEntriesStatuses = Arrays.asList(
				OrderStatus.CREATED,
				OrderStatus.CANCELLED);
		verifyOrderStatuses(result, expectedEntriesStatuses);
	}

	@Test
	public void testUpdateOrderWithEntryWithBusinessKeySuccess() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_WITH_ENTRY_WITH_BUSINESSKEY_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		// entries statuses are not updated
		List<OrderStatus> expectedEntriesStatuses = Collections.singletonList(
				OrderStatus.CREATED);
		verifyOrderStatuses(result, expectedEntriesStatuses);

		assertNotNull(result.getTotalPrice());
		assertEquals(USD_ISO_CODE, result.getTotalPrice().getCurrencyIso());

		assertNotNull(result.getTotalTax());
		assertEquals(USD_ISO_CODE, result.getTotalTax().getCurrencyIso());
	}


	@Test
	public void testUpdateOrderTaxesRecalculatedWhenCurrencyChanged() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(EDIT_ORDER_CODE, DE_ISO_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		assertNotNull(result.getTotalPrice());
		assertEquals(EUR_ISO_CODE, result.getTotalPrice().getCurrencyIso());

		assertNotNull(result.getTotalTax());
		assertEquals(EUR_ISO_CODE, result.getTotalTax().getCurrencyIso());
	}


	@Test
	public void testUpdateOrderExternalPricesAndDiscountsAreAppliedWhenCurrencyChanged() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_EXTERNAL_PRICES_AND_DISCOUNTS, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(EDIT_ORDER_CODE, DE_ISO_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		assertEquals(EUR_ISO_CODE, result.getTotalDiscounts().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRY_EXTERNAL_DISCOUNT_EUR), result.getTotalDiscounts().getValue());
		assertEquals(EUR_ISO_CODE, result.getSubTotalWithoutDiscount().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRY_EXTERNAL_PRICE_EUR), result.getSubTotalWithoutDiscount().getValue());
		assertEquals(EUR_ISO_CODE, result.getSubTotal().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRY_EXTERNAL_PRICE_EUR - ORDER_ENTRY_EXTERNAL_DISCOUNT_EUR),
				result.getTotalPrice().getValue());
		// check external taxes updated
		assertEquals(EUR_ISO_CODE, result.getTotalTax().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRY_EXTERNAL_TAX_EUR), result.getTotalTax().getValue());
	}

	private OrderUpdateRequestDTO getOrderUpdateRequestDTO(final String orderCode, final String countryIso)
	{
		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(orderCode);
		AddressData paymentAddressData = new AddressData();
		CountryData countryData = new CountryData();
		countryData.setIsocode(countryIso);
		paymentAddressData.setCountry(countryData);
		updateRequestDTO.setPaymentAddress(paymentAddressData);
		return updateRequestDTO;
	}

	@Test
	public void updateOrderDoesNotRecalculateTotalsIfNoActualChanges() throws Exception
	{
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_SET_CALCULATED_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);

		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertEquals(USD_ISO_CODE, result.getTotalPrice().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRIES_SUBTOTAL_USD_SUM), result.getTotalPrice().getValue());
	}

	@Test
	public void updateOrderRecalculateTotalsIfAddressCurrencyChanged() throws Exception
	{
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CHANGE_PRICE_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(EDIT_ORDER_CODE, DE_ISO_CODE);

		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		assertEquals(EUR_ISO_CODE, result.getTotalPrice().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRIES_PRODUCT_PRICE_EUR_SUM), result.getTotalPrice().getValue());
	}

	@Test
	public void updateOrderRecalculateTaxChangedTest() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CHANGE_PRICE_DATA_PATH, DEFAULT_ENCODING);
		setUpViesVatWiremockMappings(true);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(EDIT_ORDER_CODE, DE_ISO_CODE);
		updateRequestDTO.setTaxNumber(TAX_VAT_NUMBER);
		updateRequestDTO.setTaxNumberExpirationDate(null);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		// then
		assertEquals(TAX_VAT_NUMBER, result.getTaxNumber());
		assertEquals(EUR_ISO_CODE, result.getTotalPrice().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(ORDER_ENTRIES_PRODUCT_PRICE_EUR_SUM), result.getTotalPrice().getValue());
	}

	@Test
	public void updateOrderRecalculateAdditionalInfoChangedTest() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		importCsv(EDIT_ORDER_CHANGE_PRICE_DATA_PATH, DEFAULT_ENCODING);
		setUpViesVatWiremockMappings(true);

		OrderUpdateRequestDTO updateRequestDTO = getOrderUpdateRequestDTO(EDIT_ORDER_CODE, US_ISO_CODE);
		updateRequestDTO.getPaymentAddress().setDepartment(ADDITIONAL_INFO_DEPARTMENT);
		updateRequestDTO.getPaymentAddress().setCompanyName(ADDITIONAL_INFO_COMPANY);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, false);

		// then
		assertEquals(ADDITIONAL_INFO_COMPANY, result.getPaymentAddress().getCompanyName());
		assertEquals(ADDITIONAL_INFO_DEPARTMENT, result.getPaymentAddress().getDepartment());
	}

	@Test
	public void testUpdateOrderConfirmSuccess() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		assertNotNull(result);
		assertEquals(result.getCode(), EDIT_ORDER_CODE);

		// entries statuses changed to CREATED except CANCELLED entries
		List<OrderStatus> expectedEntriesStatuses = Arrays.asList(
				OrderStatus.CREATED,
				OrderStatus.CREATED,
				OrderStatus.CREATED,
				OrderStatus.CANCELLED);
		verifyOrderStatuses(result, expectedEntriesStatuses);
	}

	@Test
	public void testUpdateOrderConfirmSuccessByAgent() throws Exception
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);
		wileyAssistedServiceFacade.launchAssistedServiceMode();
		wileyAssistedServiceFacade.loginAssistedServiceAgent(AGENT_ID, AGENT_PASSWORD);
		wileyAssistedServiceFacade.emulateCustomer(CUSTOMER_ID, null);

		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(ASM_EDIT_ORDER_CODE);

		// when
		OrderData result = wileyasUpdateOrderFacade.updateOrder(updateRequestDTO, true);

		assertNotNull(result);
		assertEquals(result.getCode(), ASM_EDIT_ORDER_CODE);

		// entries statuses changed to CREATED except CANCELLED entries
		List<OrderStatus> expectedEntriesStatuses = Arrays.asList(
				OrderStatus.CREATED);
		verifyOrderStatuses(result, expectedEntriesStatuses);
	}


	@Test
	public void testEditableOrder() throws ImpExException
	{
		// given
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertTrue(result);
	}

	@Test
	public void testNonHybrisOrderIsNotEditable() throws ImpExException
	{
		// given
		importCsv(EDIT_ORDER_WRON_SOURCE_SYSTEM_DATA_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertFalse(result);
	}

	@Test
	public void testZeroOrderIsNotEditable() throws ImpExException
	{
		// given
		importCsv(EDIT_ZERO_ORDER_DATA_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertFalse(result);
	}

	@Test
	public void testOrderWithNotEditableEntriesIsNotEditable() throws ImpExException
	{
		// given
		importCsv(EDIT_ORDER_NOT_EDITABLE_ENTRIES_DATA_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertFalse(result);
	}

	@Test
	public void testOrderWithActiveBPIsNotEditable() throws ImpExException
	{
		// given
		importCsv(EDIT_ORDER_ACTIVE_BP_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertFalse(result);
	}

	@Test
	public void testOrderWithFailedBPIsNotEditable() throws ImpExException
	{
		// given
		importCsv(EDIT_ORDER_FAILED_BP_PATH, DEFAULT_ENCODING);

		// when
		boolean result = wileyasUpdateOrderFacade.isEditable(EDIT_ORDER_CODE);

		// then
		assertFalse(result);
	}

	@Test //Thats strange but this test actually tests EditOrder related code
	//It checks that you have an option to select CURRENT payment method during edit, but can't select PAYPAL
	public void updateOrderDataContainsCurrentPaymentOptionAndDoesNotContainPaypal() throws Exception
	{
		importCsv(EDIT_ORDER_DATA_PATH, DEFAULT_ENCODING);

		OrderData result = wileyasOrderFacade.getOrderDetailsForCode(EDIT_ORDER_CODE);

		assertNotNull(result.getOrderConfiguration());
		assertTrue(result.getOrderConfiguration().getPermittedPaymentModes().contains(CURRENT_PAYMENT_OPTION_NAME));
		assertFalse(result.getOrderConfiguration().getPermittedPaymentModes().contains(PaymentModeEnum.PAYPAL.getCode()));
	}


	private void verifyOrderStatuses(final OrderData orderAfterUpdate, final List<OrderStatus> entriesStatuses)
	{
		assertNotNull(orderAfterUpdate.getEntries());
		List<OrderStatus> orderStatuses = orderAfterUpdate.getEntries().stream().map(e -> e.getStatus()).collect(
				Collectors.toList());
		assertEquals(entriesStatuses, orderStatuses);

		modelService.detachAll();
		OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderAfterUpdate.getCode());
		assertNotNull(orderModel.getEntries());
		List<OrderStatus> orderModelStatuses = orderModel.getEntries().stream().map(e -> e.getStatus()).collect(
				Collectors.toList());
		assertEquals(entriesStatuses, orderModelStatuses);
	}

	private OrderUpdateRequestDTO getOrderUpdateCurrentPaymentOoptionRequestDTO()
	{
		OrderUpdateRequestDTO updateRequestDTO = new OrderUpdateRequestDTO();
		updateRequestDTO.setOrderCode(EDIT_ORDER_CODE);
		return updateRequestDTO;
	}
}
