package com.wiley.core.wileycom.validation;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;


@IntegrationTest
public class WileyBaseProductDefaultValidationIntegrationTest extends AbstractValidationIntegrationTest
{

	private static final String LANG_EN_ISOCODE = "en";

	protected static final String SIZE_VALIDATION_LOCALIZED_MESSAGE_TEMPLATE =
			"The number of characters in the attribute \"%s\" must between %d and %d (inclusive) in language: en.";

	private static final String FIRST_IMAGE_SHOULD_NOT_BE_VIDEO_MESSAGE =
			"The first item in the attribute \"galleryImages\" cannot be video.";

	private static final String GALLERY_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE =
			"The attribute \"galleryImages\" must contain both the normal and thumbnail formats for each image.";

	private static final String GALLERY_VIDEO_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE =
			"The attribute \"galleryImages\" must contain a thumbnail image for each video.";

	private static final String BACKGROUND_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE
			= "The attribute \"backgroundImage\" must contain both the desktop and mobile formats.";


	private static final int IMAGE_CONTAINER_IDX = 0;
	private static final int VIDEO_CONTAINER_IDX = 1;

	@Resource
	private CommonI18NService commonI18NService;

	private ProductModel testProduct;

	@Before
	public void setup()
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));
		super.setup();
		testProduct = getProductService().getProductForCode(getCatalogVersion(), "TEST_PRODUCT");
	}

	@Test
	public void shouldValidateSizeConstraints()
	{
		validateSizeConstraint(testProduct::setSeoName, "seoName", 3, 60);
		validateSizeConstraint(testProduct::setSeoTitleTag, "seoTitleTag", 3, 60);
		validateSizeConstraint(testProduct::setSeoDescriptionTag, "seoDescriptionTag", 3, 160);
		validateSizeConstraint(testProduct::setBrightcoveVideoId, "brightcoveVideoId", 3, 255);
		validateSizeConstraint(testProduct::setDescriptionEnriched, "descriptionEnriched", 3, 21844);
	}


	@Test
	public void shouldValidateFirstGalleryContainerIsNotBrightcoveVideo()
	{
		final List<MediaContainerModel> galleryImages = new ArrayList<>(testProduct.getGalleryImages());
		//make brightcove to be first in list
		galleryImages.remove(0);
		failed(testProduct::setGalleryImages, galleryImages, FIRST_IMAGE_SHOULD_NOT_BE_VIDEO_MESSAGE);

	}

	@Test
	public void shouldValidateGalleryImagesContainerHasThumbnail()
	{
		final List<MediaContainerModel> galleryImages = new ArrayList<>(testProduct.getGalleryImages());
		final MediaContainerModel imagesGallery = galleryImages.get(IMAGE_CONTAINER_IDX);
		removeImageWithName(imagesGallery, THUMBNAIL_IMAGE_NAME);

		failed(testProduct::setGalleryImages, galleryImages, GALLERY_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);

	}

	@Test
	public void shouldValidateGalleryImagesContainerHasNormalImage()
	{
		final List<MediaContainerModel> galleryImages = new ArrayList<>(testProduct.getGalleryImages());
		final MediaContainerModel imagesGallery = galleryImages.get(IMAGE_CONTAINER_IDX);
		removeImageWithName(imagesGallery, NORMAL_IMAGE_NAME);

		failed(testProduct::setGalleryImages, galleryImages, GALLERY_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);

	}

	@Test
	public void shouldValidateGalleryBrightcoveContainerHasThumbnail()
	{
		final List<MediaContainerModel> galleryImages = new ArrayList<>(testProduct.getGalleryImages());
		final MediaContainerModel imagesGallery = galleryImages.get(VIDEO_CONTAINER_IDX);
		removeImageWithName(imagesGallery, VIDEO_THUMBNAIL_IMAGE_NAME);

		failed(testProduct::setGalleryImages, galleryImages, GALLERY_VIDEO_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);

	}

	@Test
	public void shouldValidateBackgroundImageHasDesktopImage()
	{
		final MediaContainerModel backgroundImage = testProduct.getBackgroundImage();
		removeImageWithName(backgroundImage, DESKTOP_IMAGE_NAME);

		failed(testProduct::setBackgroundImage, backgroundImage, BACKGROUND_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);

	}

	@Test
	public void shouldValidateBackgroundImageHasMobileImage()
	{
		final MediaContainerModel backgroundImage = testProduct.getBackgroundImage();
		removeImageWithName(backgroundImage, MOBILE_IMAGE_NAME);
		failed(testProduct::setBackgroundImage, backgroundImage, BACKGROUND_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);

	}




	protected void validateTestInstance()
	{
		//for default group validation interceptor will do the job
		getModelService().save(testProduct);
	}


	@Override
	protected String getSizeValidationMessageTemplate()
	{
		return SIZE_VALIDATION_LOCALIZED_MESSAGE_TEMPLATE;
	}
}
