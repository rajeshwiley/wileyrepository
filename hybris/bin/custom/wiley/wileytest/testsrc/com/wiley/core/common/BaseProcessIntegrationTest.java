package com.wiley.core.common;

import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.helpers.ProcessParameterHelper;
import de.hybris.platform.processengine.impl.DefaultBusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.testframework.TestModelUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;


@Ignore
public abstract class BaseProcessIntegrationTest extends ServicelayerTest implements WileyOAuth2SetupMixIn
{
	private static final Logger LOG = LogManager.getLogger(BaseProcessIntegrationTest.class);

	protected static final String MAPPINGS_PATH = "/wileytest/integration/BaseProcessIntegrationTest/mappings";
	protected static final String RESPONSE_PATH = "/wileytest/integration/BaseProcessIntegrationTest/response";
	protected static final String RETRIEVE_SESSION_PATH = MAPPINGS_PATH + "/retrieve_session_mapping.json";
	protected static final String AUTHORIZATION_MAPPING_PATH = MAPPINGS_PATH + "/authorization_mapping.json";
	protected static final String VERIFY_MAPPING_PATH = MAPPINGS_PATH + "/verify_mapping.json";
	protected static final String TOKENIZATION_MAPPING_PATH = MAPPINGS_PATH + "/tokenization_mapping.json";
	protected static final String REFUND_MAPPING_PATH = MAPPINGS_PATH + "/refund_mapping.json";
	protected static final String CAPTURE_MAPPING_PATH = MAPPINGS_PATH + "/capture_mapping.json";

	protected static final String AUJW_RESPONSE_PATH = RESPONSE_PATH + "/AUJW";
	protected static final String US_RESPONSE_PATH = RESPONSE_PATH + "/US";
	protected static final String RETRIEVE_SESSION_RESPONSE_PATH = "/retrieve_session.json";
	protected static final String AUTHORIZATION_RESPONSE_PATH = "/authorization.json";
	protected static final String VERIFY_RESPONSE_PATH = "/verify.json";
	protected static final String TOKENIZATION_RESPONSE_PATH = RESPONSE_PATH + "/tokenization.json";
	protected static final String REFUND_RESPONSE_PATH = "/refund_response.json";
	protected static final String REFUND_ERROR_RESPONSE_PATH = "/refund_error.json";
	protected static final String CAPTURE_RESPONSE_PATH = "/capture_response.json";
	protected static final String CAPTURE_ERROR_RESPONSE_PATH = RESPONSE_PATH + "/capture_error.json";

	private static final int PROCESS_EXECUTION_TIMEOUT = 50_000;

	@Resource
	protected DefaultBusinessProcessService businessProcessService;

	@Resource
	protected ProcessParameterHelper processParameterHelper;

	@Rule
	public WileyWireMockRule wireMock = WileyWireMockRule.build();

	@Before
	public void setUpOauth() throws Exception
	{
		setUpOauth(wireMock);
	}

	protected BusinessProcessModel waitForProcessCompletion(final BusinessProcessModel processModel)
	{
		return waitForProcessCompletion(processModel, PROCESS_EXECUTION_TIMEOUT);
	}

	protected BusinessProcessModel waitForProcessCompletion(final BusinessProcessModel processModel, int timeout)
	{
		BusinessProcessModel process = processModel;
		while (ProcessState.RUNNING.equals(process.getState()) && timeout > 0)
		{
			try
			{
				Thread.sleep(1_000L);
				process = TestModelUtils.reReadModel(processModel);
			}
			catch (final InterruptedException e)
			{
				fail(e.getMessage());
			}
			timeout -= 1_000;
		}
		return processModel;
	}

	/**
	 * Check if process has a given state
	 *
	 * @param processModel
	 * @param state
	 */
	protected void assertProcessState(final BusinessProcessModel processModel, final ProcessState state)
	{
		assertEquals("Wrong state for process, current action: " + getCurrentAction(processModel), state,
				TestModelUtils.reReadModel(processModel).getState());
	}

	protected String getCurrentAction(final BusinessProcessModel processModel)
	{
		final BusinessProcessModel reReadProcess = TestModelUtils.reReadModel(processModel);
		final ProcessTaskModel currentTaskModel =
				reReadProcess.getCurrentTasks().iterator().hasNext() ? reReadProcess.getCurrentTasks().iterator().next() : null;
		final String currentTask = currentTaskModel != null ? currentTaskModel.getAction() : "no current action";
		return currentTask;
	}

	protected Object getProcessParameterByName(final String parameterName, final BusinessProcessModel processModel)
	{
		return processParameterHelper.getProcessParameterByName(parameterName,
				TestModelUtils.reReadModel(processModel).getContextParameters()).getValue();
	}

	/**
	 * trigger an event for example "SubprocessEnd" and sends the choice
	 *
	 * @param businessProcessModel
	 * @param eventname
	 * @param choice
	 */
	protected void triggerEvent(final BusinessProcessModel businessProcessModel, final String eventname, final String choice)
	{
		final BusinessProcessEvent businessProcessEvent = BusinessProcessEvent.builder(
				businessProcessModel.getCode() + "_" + eventname).withChoice(choice).build();
		businessProcessService.triggerEvent(businessProcessEvent);
	}

	protected void setUpWiremockMappings(boolean keepCurrent, final String responsePath) throws Exception
	{
		if (!keepCurrent)
		{
			wireMock.addRequestAndResponse(RETRIEVE_SESSION_PATH, responsePath + RETRIEVE_SESSION_RESPONSE_PATH);
			wireMock.addRequestAndResponse(VERIFY_MAPPING_PATH, responsePath + VERIFY_RESPONSE_PATH);
			wireMock.addRequestAndResponse(TOKENIZATION_MAPPING_PATH, TOKENIZATION_RESPONSE_PATH);
		}
		wireMock.addRequestAndResponse(AUTHORIZATION_MAPPING_PATH, responsePath + AUTHORIZATION_RESPONSE_PATH);
		wireMock.addRequestAndResponse(REFUND_MAPPING_PATH, responsePath + REFUND_RESPONSE_PATH);
		wireMock.addRequestAndResponse(CAPTURE_MAPPING_PATH, responsePath + CAPTURE_RESPONSE_PATH);
	}
}
