package com.wiley.integration.savedCart;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.integration.savedCart.SavedCartGateway;
import com.wiley.core.wileyb2c.savedcart.WileyB2CExternalCartService;
import com.wiley.core.wileyb2c.savedcart.exception.GetSavedCartExternalSystemException;
import com.wiley.core.wileyb2c.savedcart.exception.SubscriptionAlreadyRenewedException;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.util.Arrays.asList;


@IntegrationTest
public class WileyB2CExternalCartIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyB2CExternalCartIntegrationTest.class);

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/WileyB2CExternalCartIntegrationTest/";
	private static final String CART_ID = "cartId24234";

	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("wileyb2b", "ags", "wel");

	@Resource
	private WileyB2CExternalCartService wileyB2CExternalCartService;
	@Resource
	private HttpComponentsClientHttpRequestFactory savedCartRequestFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private SavedCartGateway savedCartGateway;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void shouldReturnExternalCart() throws Exception
	{
		// Given
		setupWireMockSuccessfulResponse("response_savedCartFound.json");
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then
		verifyCartFields(cart);
	}

	@Test (expected = SubscriptionAlreadyRenewedException.class)
	public void shouldThrowExceptionIsSubscriptionWasAlreadyRenewed() throws Exception
	{
		// Given
		setupWireMockSuccessfulResponse("response_savedCartSubscriptionAlreadyRenewed.json");
		//When
		wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then SubscriptionAlreadyRenewedException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus400BadRequest() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_BAD_REQUEST);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus401Unauthorized() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_UNAUTHORIZED);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus403Forbidden() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_FORBIDDEN);
		//When
		wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus404NotFound() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_NOT_FOUND);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus500InternalError() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus501ExternalSystemTypeIsNotSupported() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_NOT_IMPLEMENTED);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	@Test(expected = GetSavedCartExternalSystemException.class)
	public void shouldThrowExceptionInCaseOfHttpStatus503ServiceUnavailable() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_SERVICE_UNAVAILABLE);
		//When
		final CartModel cart = wileyB2CExternalCartService.getExternalCart(CART_ID);
		//Then GetSavedCartExternalSystemException is thrown
	}

	private void setupWireMockSuccessfulResponse(final String responseBodyFileName)
			throws Exception
	{
		wireMock.
				stubFor(get(urlEqualTo("/savedCarts?type=olr&cartId=" + CART_ID))
						.withHeader("Content-Type", matching("application/json"))
						.withHeader("Accept", matching("application/json"))
						.withHeader("Src", equalTo("hybris"))
						.withHeader("TransactionId", matching(".*"))

						.willReturn(aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", "application/json")
								.withBody(readFromFile(TEST_REQUESTS_PATH + responseBodyFileName))));
	}

	private void setupWireMockErrorResponse(final int httpStatus)
	{
		wireMock.
				stubFor(get(urlEqualTo("/savedCarts?type=olr&cartId=" + CART_ID))
						.willReturn(aResponse()
								.withStatus(httpStatus)
								.withHeader("Content-Type", "application/json")));
	}

	private void verifyCartFields(final CartModel cart)
	{
		assertEquals("test_cart_code", cart.getCode());
		assertEquals("US", cart.getCountry().getIsocode());
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return savedCartRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("getCart", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				setupWireMockSuccessfulResponse("response_savedCartFound.json");
				savedCartGateway.getCart(CART_ID);
				return null;
			}
		});
		return funcMap;
	}
}
