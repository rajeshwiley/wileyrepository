package com.wiley.core.wileycom.validation;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;


@IntegrationTest
public class WileyVariantProductIntegrationValidationIntegrationTest
		extends AbstractIntegrationValidationIntegrationTest<WileyPurchaseOptionProductModel>
{

	private static final String CODE_FORMAT_WRONG_MESSAGE = "The attribute \"code\" must not contain dashes or hyphens.";
	private static final String ISBN_FORMAT_WRONG_MESSAGE = "The attribute \"isbn\" must be stored without dashes or hyphens.";
	private static final String ISBN_LENGTH_WRONG_MESSAGE = "The attribute \"isbn\" must be exactly 10 characters long.";
	private static final String CLASSIFICATION_CLASSES_WRONG_COUNT_MESSAGE =
			"The attribute \"classificationClasses\" must contain exactly one item.";

	private static final String ISBN13_WRONG_FORMAT_MESSAGE =
			"The attribute \"isbn13\" must be stored without dashes or hyphens, and must be exactly 13 characters long.";
	private static final String PRINT_ISSN_WRONG_FORMAT_MESSAGE =
			"The attribute \"printIssn\" must be stored without dashes or hyphens, and must be exactly 8 characters long.";
	private static final String ONLINE_ISSN_WRONG_FORMAT_MESSAGE =
			"The attribute \"onlineIssn\" must be stored without dashes or hyphens, and must be exactly 8 characters long.";


	@Resource
	private ClassificationSystemService classificationSystemService;

	@Resource
	private ClassificationService classificationService;

	private WileyPurchaseOptionProductModel testProduct;


	@Before
	public void setup()
	{
		super.setup();
		testProduct =
				(WileyPurchaseOptionProductModel) getProductService().getProductForCode(getCatalogVersion(), "TEST_VARIANT");

	}

	@Override
	protected WileyPurchaseOptionProductModel getTestItem()
	{
		return testProduct;
	}


	@Test
	public void shouldValidateNotNullFields()
	{
		validateNotNullConstraint(testProduct::setIsbn, "isbn", randomAlphabetic(10));
		validateNotNullConstraint(testProduct::setLifecycleStatus, "lifecycleStatus", WileyProductLifecycleEnum.ACTIVE);

		validateNotNullConstraint(testProduct::setCustom, "custom", Boolean.FALSE);
		validateNotNullConstraint(testProduct::setEditionFormat, "editionFormat", ProductEditionFormat.DIGITAL);
		validateNotNullConstraint(testProduct::setPrintOnDemand, "printOnDemand", Boolean.FALSE);
	}

	@Test
	public void shouldValidateCode()
	{
		validateSizeConstraint(testProduct::setCode, "code", 1, 255);
		failed(testProduct::setCode, "code-with-dashes", CODE_FORMAT_WRONG_MESSAGE);
		failed(testProduct::setCode, "code\u2013with\u2013hypens", CODE_FORMAT_WRONG_MESSAGE);
	}


	@Test
	public void shouldValidateIsbn()
	{
		failed(testProduct::setIsbn, "12345-7890", ISBN_FORMAT_WRONG_MESSAGE);
		failed(testProduct::setIsbn, "12345\u20137890", ISBN_FORMAT_WRONG_MESSAGE);
		failed(testProduct::setIsbn, randomAlphabetic(11), ISBN_LENGTH_WRONG_MESSAGE);
	}

	@Test
	public void shouldValidateClassificationClasses()
	{
		final ClassificationSystemVersionModel classificationSystem = classificationSystemService.getSystemVersion(
				"testClassificationCatalog", "1");
		final ClassificationClassModel bookClass = classificationSystemService.getClassForCode(classificationSystem, "book");
		final ClassificationClassModel ebookClass = classificationSystemService.getClassForCode(classificationSystem, "ebook");
		//validate for more than one value
		failed(testProduct::setSupercategories, Arrays.asList(bookClass, ebookClass), CLASSIFICATION_CLASSES_WRONG_COUNT_MESSAGE);
		failed(testProduct::setSupercategories, Collections.emptyList(), CLASSIFICATION_CLASSES_WRONG_COUNT_MESSAGE);

	}

	@Test
	public void shouldValidateIsbn13()
	{
		final List<ProductFeatureModel> features = new ArrayList<>(testProduct.getFeatures());
		ProductFeatureModel feature = findFeatureForAttributeCode(features,
				Wileyb2cClassificationAttributes.ISBN13.getCode());
		//too short
		feature.setValue(randomAlphanumeric(3));
		failed(testProduct::setFeatures, features, ISBN13_WRONG_FORMAT_MESSAGE);
		//too long
		feature.setValue(randomAlphanumeric(100));
		failed(testProduct::setFeatures, features, ISBN13_WRONG_FORMAT_MESSAGE);

		//with dash
		feature.setValue("1234567-90123");
		failed(testProduct::setFeatures, features, ISBN13_WRONG_FORMAT_MESSAGE);

		//with hypen
		feature.setValue("1234567\u201390123");
		failed(testProduct::setFeatures, features, ISBN13_WRONG_FORMAT_MESSAGE);

		//valid
		feature.setValue(randomAlphanumeric(13));
		succeeded(testProduct::setFeatures, features);

		//missing attribute should be accepted
		features.remove(feature);
		succeeded(testProduct::setFeatures, features);
	}

	@Test
	public void shouldValidatePrintIssn()
	{
		final List<ProductFeatureModel> features = new ArrayList<>(testProduct.getFeatures());
		ProductFeatureModel feature = findFeatureForAttributeCode(features,
				Wileyb2cClassificationAttributes.PRINT_ISSN.getCode());
		//too short
		feature.setValue(randomAlphanumeric(3));
		failed(testProduct::setFeatures, features, PRINT_ISSN_WRONG_FORMAT_MESSAGE);
		//too long
		feature.setValue(randomAlphanumeric(100));
		failed(testProduct::setFeatures, features, PRINT_ISSN_WRONG_FORMAT_MESSAGE);

		//with dash
		feature.setValue("123-5678");
		failed(testProduct::setFeatures, features, PRINT_ISSN_WRONG_FORMAT_MESSAGE);

		//with hypen
		feature.setValue("123\u20135678");
		failed(testProduct::setFeatures, features, PRINT_ISSN_WRONG_FORMAT_MESSAGE);

		//valid
		feature.setValue(randomAlphanumeric(8));
		succeeded(testProduct::setFeatures, features);
		//missing attribute should be accepted
		features.remove(feature);
		succeeded(testProduct::setFeatures, features);

	}

	@Test
	public void shouldValidateOnlineIssn()
	{
		final List<ProductFeatureModel> features = new ArrayList<>(testProduct.getFeatures());
		ProductFeatureModel feature = findFeatureForAttributeCode(features,
				Wileyb2cClassificationAttributes.ONLINE_ISSN.getCode());
		//too short
		feature.setValue(randomAlphanumeric(3));
		failed(testProduct::setFeatures, features, ONLINE_ISSN_WRONG_FORMAT_MESSAGE);
		//too long
		feature.setValue(randomAlphanumeric(100));
		failed(testProduct::setFeatures, features, ONLINE_ISSN_WRONG_FORMAT_MESSAGE);

		//with dash
		feature.setValue("123-5678");
		failed(testProduct::setFeatures, features, ONLINE_ISSN_WRONG_FORMAT_MESSAGE);

		//with hypen
		feature.setValue("123–5678");
		failed(testProduct::setFeatures, features, ONLINE_ISSN_WRONG_FORMAT_MESSAGE);

		//valid
		feature.setValue(randomAlphanumeric(8));
		succeeded(testProduct::setFeatures, features);

		//missing attribute should be accepted
		features.remove(feature);
		succeeded(testProduct::setFeatures, features);
	}

	private ProductFeatureModel findFeatureForAttributeCode(final List<ProductFeatureModel> features, final String code)
	{
		ProductFeatureModel featureFound = null;
		if (CollectionUtils.isNotEmpty(features))
		{
			featureFound = features.stream()
					.filter(feature -> code.equals(feature.getClassificationAttributeAssignment()
							.getClassificationAttribute().getCode()))
					.findFirst()
					.orElse(null);
		}
		return featureFound;
	}
}
