package com.wiley.integrations.customer.serviceactivator;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.wileyas.user.service.WileyasUserService;


/**
 * @author Maksim Kozich <Maksim_Kozich@epam.com>
 */
@IntegrationTest
public class WileyUserProfileServiceActivatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String RESOURCE_PATH = "/wileytest/integration/WileyUserProfileServiceActivatorIntegrationTest/";
	private static final String CREATE_CUSTOMER_IMPEX = RESOURCE_PATH + "create_customer.impex";
	private static final String UPDATE_CUSTOMER_JSON = RESOURCE_PATH + "update_customer.json";
	private static final String CUSTOMER_UID = "test_customer_sqs";

	private static final String NEW_FIRST_NAME = "New First";
	private static final String NEW_LAST_NAME = StringUtils.EMPTY;
	private static final String NEW_NAME = "New First";
	private static final String NEW_EMAIL = "new_testemail@example.com";

	@Resource
	private MessageChannel userProfileUpdateChannel;

	@Resource
	private WileyasUserService wileyasUserService;

	@Test
	public void testCustomerInfoIsUpdatedIfNotNull() throws Exception
	{
		// given
		importCsv(CREATE_CUSTOMER_IMPEX, DEFAULT_ENCODING);
		String updateCustomerJson = readFromFile(UPDATE_CUSTOMER_JSON);
		Message<String> updateUserMessage = MessageBuilder.withPayload(updateCustomerJson).build();

		// when
		final boolean send = userProfileUpdateChannel.send(updateUserMessage);

		// then
		assertTrue(send);

		final CustomerModel customer = (CustomerModel) wileyasUserService.getUserForUID(CUSTOMER_UID);
		assertEquals(NEW_FIRST_NAME, customer.getFirstName());
		assertEquals(NEW_LAST_NAME, customer.getLastName());
		assertEquals(NEW_NAME, customer.getName());
		assertEquals(NEW_EMAIL, customer.getEmailAddress());
	}

}