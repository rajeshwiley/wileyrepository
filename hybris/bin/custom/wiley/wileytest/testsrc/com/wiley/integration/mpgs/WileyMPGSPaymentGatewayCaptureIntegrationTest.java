package com.wiley.integration.mpgs;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.response.WileyCaptureResponse;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


@IntegrationTest
public class WileyMPGSPaymentGatewayCaptureIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String ORDER_ID = "order12345";
	private static final String TRANSACTION_ID = "transaction12345";
	private static final String CURRENCY = "USD";
	private static final String STATUS_DETAILS = "APPROVED";
	private static final BigDecimal AMOUNT = new BigDecimal(34);
	private static final BigDecimal ERROR_AMOUNT = null;
	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String CAPTURE_REQUEST_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST/order/" + ORDER_ID + "/transaction/" + TRANSACTION_ID;

	private static final String SUCCESS_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/capture_request_success.json";
	private static final String SUCCCESS_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/capture_response_success.json";

	private static final String ERROR_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/capture_request_error.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/capture_response_error.json";
	public static final String TEST_TRANSACTION_REFERENCE = "0257000000000001";


	@Resource
	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Test
	public void testCapturePositive() throws Exception
	{

		wireMock.stubFor(put(urlMatching(CAPTURE_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCCESS_RESPONSE_JSON_FILE_PATH))
				));


		WileyCaptureRequest request = new WileyCaptureRequest(WIRE_MOCK_URL + CAPTURE_REQUEST_PATH, AMOUNT,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, CURRENCY);
		request.setTransactionReference(TEST_TRANSACTION_REFERENCE);

		WileyCaptureResponse response = mpgsPaymentGateway.capture(request);

		assertEquals(TransactionStatus.ACCEPTED.name(), response.getStatus());
		assertEquals(STATUS_DETAILS, response.getStatusDetails());
		assertEquals(AMOUNT, response.getTransactionAmount());
		assertEquals(TRANSACTION_ID, response.getTransactionId());
		assertEquals(CURRENCY, response.getTransactionCurrency());
	}



	@Test(expected = HttpClientErrorException.class)
	public void testCaptureError() throws Exception
	{

		wireMock.stubFor(put(urlMatching(CAPTURE_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(ERROR_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_BAD_REQUEST)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))
				));


		WileyCaptureRequest request = new WileyCaptureRequest(WIRE_MOCK_URL + CAPTURE_REQUEST_PATH, ERROR_AMOUNT,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, CURRENCY);
		request.setTransactionReference(TEST_TRANSACTION_REFERENCE);

		WileyCaptureResponse response = mpgsPaymentGateway.capture(request);

	}
}