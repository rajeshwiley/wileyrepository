package com.wiley.test.product;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.PerformanceTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;


/**
 * The main purpose of this class is to test performance in follow cases<br/>
 * <ul>
 * <li>Case 1: No-Index - lack of check for data integrity</li>
 * <li>Case 2: Dedicated Destination - extra join with Product table for getting Variant data</li>
 * <li>Case 3: Validate Interceptor - extra calls which potentially will slow down Product Data import process</li>
 * </ul>
 *
 * @author Aliaksei_Zlobich
 */
@PerformanceTest
public class WileyVariantProductPerformanceTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String ENCODING = "UTF-8";
	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";
	/**
	 * The constant CATALOG_VERSION_NAME.
	 */
	public static final String CATALOG_VERSION_NAME = "Online";

	@Resource
	private ProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CategoryService categoryService;


	/**
	 * Before.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void before() throws Exception
	{
	}

	/**
	 * Test only update wiley variants products no cache.
	 *
	 * @throws ImpExException
	 * 		the imp ex exception
	 */
	@Test
	public void testOnlyUpdateWileyVariantsProductsNoCache() throws ImpExException
	{
		// Given
		int count = 50;
		// Prepare date for updating
		importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/products.impex", ENCODING);

		// When
		// Import product.impex many times
		disableCache();
		StopWatch stopWatch = new StopWatch("OnlyUpdateWileyVariantsProductsNoCache");
		try
		{
			for (int i = 0; i < count; i++)
			{
				stopWatch.start(String.format("Run %s", i + 1));
				importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/products.impex", ENCODING);
				stopWatch.stop();
			}
		}
		finally
		{
			enableCache();
		}

		// Then
		// Print results
		printResults(stopWatch);
	}

	/**
	 * Test only insert wiley variants products no cache.
	 *
	 * @throws ImpExException
	 * 		the imp ex exception
	 */
	@Test
	public void testOnlyInsertWileyVariantsProductsNoCache() throws ImpExException
	{
		// Given
		int count = 50;

		// When
		// Import product.impex many times
		disableCache();
		StopWatch stopWatch = new StopWatch("OnlyInsertWileyVariantsProductsNoCache");
		try
		{
			for (int i = 0; i < count; i++)
			{
				stopWatch.start(String.format("Run %s", i + 1));
				importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/products.impex", ENCODING);
				stopWatch.stop();
				importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/remove-wiley-wileyvariant-products.impex",
						ENCODING);
			}
		}
		finally
		{
			enableCache();
		}

		// Then
		// Print results
		printResults(stopWatch);
	}

	/**
	 * Test get product for code no cache.
	 *
	 * @throws ImpExException
	 * 		the imp ex exception
	 */
	@Test
	public void testGetProductForCodeNoCache() throws ImpExException
	{
		// Given
		int count = 200;
		importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/products.impex", ENCODING);
		String productCode = "CMA-REVIEW_COURCE-PART_2-PRINTED"; // WileyVariantProduct
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION_NAME);

		// When
		disableCache();
		StopWatch stopWatch = new StopWatch("GetProductForCodeNoCache");
		try
		{
			for (int i = 0; i < count; i++)
			{
				stopWatch.start(String.format("Run %s", i + 1));
				productService.getProductForCode(productCode); // if not found throws UnknownIdentifierException
				stopWatch.stop();
			}
		}
		finally
		{
			enableCache();
		}

		// Then
		printResults(stopWatch);
	}

	/**
	 * Test get products for category no cache.
	 *
	 * @throws ImpExException
	 * 		the imp ex exception
	 */
	@Test
	public void testGetProductsForCategoryNoCache() throws ImpExException
	{
		// Given
		int count = 200;
		importCsv("/wileytest/import/product/WileyVariantProductPerformanceTest/products.impex", ENCODING);
		String categoryCode = "CPA_Category";
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION_NAME);
		final CategoryModel category = categoryService.getCategoryForCode(catalogVersionService.getCatalogVersion(CATALOG_ID,
				CATALOG_VERSION_NAME), categoryCode);

		// When
		disableCache();
		StopWatch stopWatch = new StopWatch("GetProductsForCategoryNoCache");
		try
		{
			for (int i = 0; i < count; i++)
			{
				stopWatch.start(String.format("Run %s", i + 1));
				productService.getProductsForCategory(category);
				stopWatch.stop();
			}
		}
		finally
		{
			enableCache();
		}

		// Then
		printResults(stopWatch);
	}

	private void printResults(final StopWatch stopWatch)
	{
		System.out.println(String.format("%s\nAverage time: %s ms.\n", stopWatch.prettyPrint(),
				(double) stopWatch.getTotalTimeMillis() / (double) stopWatch.getTaskCount()));
	}

	private void disableCache()
	{
		Registry.getCurrentTenant().getCache().setEnabled(false);
	}

	private void enableCache()
	{
		Registry.getCurrentTenant().getCache().setEnabled(true);
	}

}
