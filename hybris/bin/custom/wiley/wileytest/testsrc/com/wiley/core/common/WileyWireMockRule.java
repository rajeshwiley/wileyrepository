package com.wiley.core.common;

import de.hybris.platform.util.Config;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.core.Options;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.http.ResponseDefinition;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.RegexPattern;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.matching.UrlPattern;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.wiremock.transromers.tax.TaxResponseTransformer;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


/**
 * Created by Mikhail_Asadchy on 14.06.2016.
 */
public final class WileyWireMockRule extends WireMockRule
{
	private static final Logger LOG = LogManager.getLogger(WileyWireMockRule.class);
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));

	private WileyWireMockRule(final Options options)
	{
		super(options);
	}

	public RequestPatternBuilder getRequestPatternBuilder(final String urlPattern, final RequestMethod requestMethod)
	{
		final UrlPattern uPattern = new UrlPattern(new RegexPattern(urlPattern), true);
		return RequestPatternBuilder.newRequestPattern(requestMethod, uPattern);
	}

	public StubMapping setUpStubMapping(final String mapping, final String responseBody) throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(mapping);
		setResponseBody(stubMapping, responseBody);
		return stubMapping;
	}

	public StubMapping setUpStubMapping(final String mapping, final int responseStatus) throws Exception
	{
		final StubMapping stubMapping = createStubMappingFrom(mapping);
		setResponseStatus(responseStatus, stubMapping);
		return stubMapping;
	}

	public StubMapping createStubMappingFrom(final String jsonFile) throws Exception
	{
		return StubMapping.buildFrom(readFromFile(jsonFile));
	}

	public void setResponseStatus(final int responseStatus, final StubMapping stubMapping)
	{
		final ResponseDefinition response = stubMapping.getResponse();
		stubMapping.setResponse(ResponseDefinitionBuilder
				.like(response)
				.withStatus(responseStatus)
				.build());
	}

	public void setResponseBody(final StubMapping stubMapping, final String file) throws Exception
	{
		final ResponseDefinition responseDefinition = stubMapping.getResponse();
		stubMapping.setResponse(ResponseDefinitionBuilder
				.like(responseDefinition)
				.withBody(readFromFile(file))
				.build());
	}

	public void setResponseBodyBinary(final StubMapping stubMapping, final String binaryFile) throws Exception
	{
		final ResponseDefinition responseDefinition = stubMapping.getResponse();
		stubMapping.setResponse(ResponseDefinitionBuilder
				.like(responseDefinition)
				.withBody(readBytesFromFile(binaryFile))
				.build());
	}

	public void addRequestAndResponse(final String requestJsonPath, final String responseJsonPath) throws Exception
	{
		addStubMapping(setUpStubMapping(requestJsonPath, responseJsonPath));
	}

	public String readFromFile(final String filePath) throws Exception
	{
		final URI fileUri = this.getClass().getResource(filePath).toURI();
		return Files.lines(Paths.get(fileUri))
				.collect(Collectors.joining());
	}

	public byte[] readBytesFromFile(final String filePathBinary) throws Exception
	{
		final URI fileUri = this.getClass().getResource(filePathBinary).toURI();
		return Files.readAllBytes(Paths.get(fileUri));
	}

	public void internalVerifyExecutableMethodWithExceptionBehavior(
			final String resourcePrefix, final Runnable executableMethod,
			final String stubMappingPath, final String stubResponseBodyPath, final int responseStatus,
			final Class<? extends Exception> expectedException, final String expectedRequestUrl,
			final RequestMethod expectedRequestMethod) throws Exception
	{
		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				resourcePrefix + stubMappingPath,
				resourcePrefix + stubResponseBodyPath
		);
		setResponseStatus(responseStatus, stubMapping);

		addStubMapping(stubMapping);

		// ---------- When ---------
		try
		{
			executableMethod.run();
			fail("Expected " + expectedException.getName());
		}
		catch (final Exception e)
		{
			final boolean isExpectedException = expectedException.isInstance(e);
			if (!isExpectedException)
			{
				LOG.error(e.getMessage(), e); // print cause of the issue to bring more information during fixing of tests.
			}
			assertTrue(String.format("Exception class expected [%s] but was [%s].", expectedException.getName(),
					e.getClass().getName()), isExpectedException);
		}

		// ---------- Then ----------
		verify(1, getRequestPatternBuilder(
				expectedRequestUrl,
				expectedRequestMethod));
	}

	public static WileyWireMockRule build()
	{
		WireMockConfiguration configuration = wireMockConfig().port(WIREMOCK_PORT).extensions(
				new ResponseTemplateTransformer(false),
				new TaxResponseTransformer());
		return new WileyWireMockRule(configuration);
	}
}
