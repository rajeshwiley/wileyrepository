package com.wiley.integration;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.netflix.hystrix.Hystrix;
import com.netflix.hystrix.HystrixCommandProperties;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.util.TestUtils;
import com.wiley.integrations.handler.WileyCircuitBreakerRequestHandlerAdvice;
import com.wiley.test.gateway.transformer.TestTransformer;


/**
 * This test uses test circuitbreackeradvice gateway chain and checks negative scenario.
 */
@IntegrationTest
public class WileyCircuitBreakerRequestHandlerAdviceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String TEST_MESSAGE = "Message";

	private static final String TEST_RESOURCES_PREFIX =
			"/wileytest/integration/WileyCircuitBreakerAdviceRequestHandlerIntegrationTest";

	private static final int MAIN_THREAD_SLEEP_WINDOW_AFTER_FIRST_INVOCATION_IN_MILLISECONDS = 1000;
	private static final int HYSTRIX_RESET_TIMEOUT_IN_SECONDS = 30;
	private static final int THREAD_AWAIT_TERMINATION_TIMEOUT_IN_SECONDS = 30;

	// circuit breaker configuration
	private static final int REQUEST_VOLUME_THRESHOLD = 1;
	private static final int ERROR_THRESHOLD_PERCENTAGE = 100;
	private static final int CIRCUIT_BREAKER_SLEEP_WINDOW_IN_MILLISECONDS = 20 * 1000;

	private WileyTestGateway wileyTestGateway;

	private TestTransformer testTransformer;

	private WileyCircuitBreakerRequestHandlerAdvice wileyCircuitBreakerRequestHandlerAdvice;


	private AtomicInteger countOfInvocations;

	@Before
	public void setUp() throws Exception
	{
		// Initialize local properties
		countOfInvocations = new AtomicInteger();
		ApplicationContext parentApplicationContext = Registry.getApplicationContext();

		// Prepare test application context
		ApplicationContext testApplicationContext = initializeTestApplicationContext(parentApplicationContext,
				TEST_RESOURCES_PREFIX + "/circuitbreakeradvice-integration-spring.xml");

		// Get dependencies from test application context
		wileyTestGateway = testApplicationContext.getBean("wileyTestGateway", WileyTestGateway.class);
		testTransformer = testApplicationContext.getBean("testTransformer", TestTransformer.class);
		wileyCircuitBreakerRequestHandlerAdvice = testApplicationContext.getBean("testWileyCircuitBreakerRequestHandlerAdvice",
				WileyCircuitBreakerRequestHandlerAdvice.class);

		final HystrixCommandProperties.Setter propertiesSetter = wileyCircuitBreakerRequestHandlerAdvice.getPropertiesSetter();
		propertiesSetter.withCircuitBreakerRequestVolumeThreshold(REQUEST_VOLUME_THRESHOLD);
		propertiesSetter.withCircuitBreakerErrorThresholdPercentage(ERROR_THRESHOLD_PERCENTAGE);
		propertiesSetter.withCircuitBreakerSleepWindowInMilliseconds(CIRCUIT_BREAKER_SLEEP_WINDOW_IN_MILLISECONDS);

		Hystrix.reset(HYSTRIX_RESET_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

		// Test transformer throws exception and increase count of invocations
		testTransformer.setTransformationFunction(message -> {
			countOfInvocations.getAndIncrement();
			throw new RuntimeException("Some test Exception");
		});
	}

	@After
	public void tearDown() throws Exception
	{
		Hystrix.reset(HYSTRIX_RESET_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
	}

	@Test
	public void circuitBreakerShouldOpenIfEnabled() throws Exception
	{
		// ------------- Given -------------
		// CircuitBreaker is enabled and fails after first exception.
		final HystrixCommandProperties.Setter propertiesSetter = wileyCircuitBreakerRequestHandlerAdvice.getPropertiesSetter();
		propertiesSetter.withCircuitBreakerEnabled(true);

		// ------------- When -------------
		// First invocation: CircuitBreaker should switch to open state after exception
		// and then all other requests should be skipped
		final int times = REQUEST_VOLUME_THRESHOLD + 10;
		invokeMethodWrappedByCircuitBreaker(times);

		// ------------- Then -------------
		assertEquals(1, countOfInvocations.get());
	}

	@Test
	public void circuitBreakerShouldNotOpenIfDisabled() throws Exception
	{
		// ------------- When -------------
		// First invocation: CircuitBreaker should not be opened if some exception is thrown
		final int times = REQUEST_VOLUME_THRESHOLD + 10;
		invokeMethodWrappedByCircuitBreaker(times);

		// ------------- Then -------------
		assertEquals(times, countOfInvocations.get());
	}

	@Test
	public void circuitBreakerShouldOpenIfEnabledWithMultiThreading() throws Exception
	{
		// ------------- Given -------------
		// CircuitBreaker is disabled but fails after first exception.
		final HystrixCommandProperties.Setter propertiesSetter = wileyCircuitBreakerRequestHandlerAdvice.getPropertiesSetter();
		propertiesSetter.withCircuitBreakerEnabled(true);

		// ------------- When -------------
		// First invocation: CircuitBreaker should switch to open state when first exception is thrown
		// and then all other requests should be skipped
		final int times = REQUEST_VOLUME_THRESHOLD + 10;
		final int countOfThreads = 3;
		invokeMethodWrappedByCircuitBreakerInThreads(times, countOfThreads);

		// ------------- Then -------------
		assertTrue(times > countOfInvocations.get());
	}

	@Test
	public void circuitBreakerShouldNotOpenIfDisabledWithMultiThreading() throws Exception
	{

		// ------------- When -------------
		// First invocation: CircuitBreaker should not be opened if some exception is thrown
		// and then all other requests should not be skipped
		final int times = 10;
		final int countOfThreads = 3;
		invokeMethodWrappedByCircuitBreakerInThreads(times, countOfThreads);

		// ------------- Then -------------
		assertEquals(times, countOfInvocations.get());
	}

	private void invokeMethodWrappedByCircuitBreakerInThreads(final int times, final int countOfThreads)
			throws InterruptedException
	{
		final ExecutorService executorService = Executors.newFixedThreadPool(countOfThreads);

		int timesLeft = times;
		boolean firstInvocation = true;

		while (timesLeft > 0)
		{
			int timesToInvokeInLoop = Math.min(countOfThreads, timesLeft);
			timesLeft = timesLeft - timesToInvokeInLoop;

			CyclicBarrier finish = new CyclicBarrier(timesToInvokeInLoop);

			Runnable invokeTestMethod = () -> wileyTestGateway.send(TEST_MESSAGE);
			Runnable runnableGatewayInvocation = () -> {
				TestUtils.expectException(ExternalSystemInternalErrorException.class, invokeTestMethod);

				// keep the thread until all other threads from ThreadPool are not invoked.
				await(finish);
			};

			if (firstInvocation)
			{
				firstInvocation = false;
				timesToInvokeInLoop--;

				executorService.submit(runnableGatewayInvocation);
				// allows CircuitBreaker to report metrics ant calculate circuit health
				Thread.sleep(MAIN_THREAD_SLEEP_WINDOW_AFTER_FIRST_INVOCATION_IN_MILLISECONDS);
			}

			IntStream.range(0, timesToInvokeInLoop)
					.forEach((index) -> executorService.submit(runnableGatewayInvocation));
		}

		// Waiting for all threads are finished
		executorService.shutdown();
		if (!executorService.awaitTermination(THREAD_AWAIT_TERMINATION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS))
		{
			throw new IllegalStateException("Await termination time is exceeded.");
		}
	}

	private void await(final CyclicBarrier finish)
	{
		try
		{
			finish.await();
		}
		catch (Exception e)
		{
			// Do nothing
		}
	}

	private void invokeMethodWrappedByCircuitBreaker(final int times) throws InterruptedException
	{
		TestUtils.expectException(ExternalSystemInternalErrorException.class,
				() -> wileyTestGateway.send(TEST_MESSAGE));

		// allows CircuitBreaker to report metrics ant calculate circuit health
		Thread.sleep(MAIN_THREAD_SLEEP_WINDOW_AFTER_FIRST_INVOCATION_IN_MILLISECONDS);

		IntStream.range(0, times - 1).forEach((index) ->
				TestUtils.expectException(ExternalSystemInternalErrorException.class,
						() -> wileyTestGateway.send(TEST_MESSAGE)));
	}

}
