package com.wiley.integration.invoice;

import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.integration.invoice.InvoiceGateway;
import com.wiley.core.integration.invoice.dto.InvoiceDto;

public class InvoiceGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(InvoiceGatewayIntegrationTest.class);
	private static final String RESOURCE_PREFIX = "/wileytest/integration/InvoiceGatewayIntegrationTest";
	private static final String ORDER_ENTRY_GUID_200 = "orderEntryGuid200";
	private static final String ORDER_ENTRY_GUID_MISSING_CONTENT_TYPE = "orderEntryGuidMissingContentType";
	private static final String ORDER_ENTRY_GUID_404 = "orderEntryGuid404";
	private static final String ORDER_ENTRY_GUID_500 = "orderEntryGuid500";

	@Resource(name = "invoiceGateway")
	private InvoiceGateway gateway;

	@Resource
	private RestTemplate invoiceApigeeEdgeOAuth2RestTemplate;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void testGetInvoiceFileSuccessful() throws Exception
	{
		// given
		final StubMapping stubMapping = createStubMappingFrom(RESOURCE_PREFIX + "/mappings/mapping_get_invoice_file_200.json");
		setResponseBodyBinary(stubMapping, RESOURCE_PREFIX + "/responses/sample.pdf");
		wireMock.addStubMapping(stubMapping);

		// when
		final InvoiceDto invoiceDto = gateway.getInvoice(ORDER_ENTRY_GUID_200);

		// then
		assertNotNull(invoiceDto);
		assertNotNull(invoiceDto.getBinaryFile());
		assertNotNull(invoiceDto.getMimeType());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetInvoiceFileWhenContentTypeHeaderMissing() throws Exception
	{
		// given
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/mapping_get_invoice_file_missing_content-type.json");
		setResponseBodyBinary(stubMapping, RESOURCE_PREFIX + "/responses/sample.pdf");
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getInvoice(ORDER_ENTRY_GUID_MISSING_CONTENT_TYPE);
	}

	@Test(expected = ExternalSystemNotFoundException.class)
	public void testGetInvoiceFileWhen404StatusCode() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_invoice_file_404.json",
				RESOURCE_PREFIX + "/responses/response_invoice_404.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getInvoice(ORDER_ENTRY_GUID_404);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void testGetInvoiceFileWhen500StatusCode() throws Exception
	{
		// given
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_invoice_file_500.json",
				RESOURCE_PREFIX + "/responses/response_invoice_500.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		gateway.getInvoice(ORDER_ENTRY_GUID_500);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) invoiceApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}
