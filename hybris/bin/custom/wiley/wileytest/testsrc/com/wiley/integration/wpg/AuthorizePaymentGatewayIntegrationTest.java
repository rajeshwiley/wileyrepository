package com.wiley.integration.wpg;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.BillingInfo;

import java.math.BigDecimal;
import java.util.Currency;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;




/**
 * Created by Raman_Hancharou on 8/11/2016.
 */
@IntegrationTest
public class AuthorizePaymentGatewayIntegrationTest extends AbstractWpgIntegrationTest
{

	private static final Logger LOG = LoggerFactory.getLogger(AuthorizePaymentGatewayIntegrationTest.class);

	private static final String TEST_SUCCESS_SUBSCRIPTION_ID = "testSuccessSubscriptionId";
	private static final String TEST_ERROR_SUBSCRIPTION_ID = "testErrorSubscriptionId";
	private static final String TEST_POSTAL_CODE = "testPostalCode";
	private static final String TEST_STREET_1 = "testStreet1";
	private static final String TEST_COUNTRY_ISOCODE = "US";

	@Before
	public void before() throws Exception
	{
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void testAuthorizePaymentSuccess() throws Exception
	{
		// Given
		final WileySubscriptionAuthorizeRequest authorizeRequest = createAuthorizeRequest(TEST_SUCCESS_SUBSCRIPTION_ID);
		mockPaymentMethod(TEST_SUCCESS_SUBSCRIPTION_ID, "authorize_response_success.txt", HttpStatus.SC_OK);
		WileyAuthorizeResult expexted = createExpectedAuthorizeResult(authorizeRequest);

		// When
		WileyAuthorizeResult actual = wileyPaymentGateway.authorizePayment(authorizeRequest);

		// Then
		assertEqualsAuthorizeResults(expexted, actual);
	}

	@Test
	public void testAuthorizePaymentError404() throws Exception
	{
		// Given
		final WileySubscriptionAuthorizeRequest authorizeRequest = createAuthorizeRequest(TEST_ERROR_SUBSCRIPTION_ID);
		mockPaymentMethod(TEST_ERROR_SUBSCRIPTION_ID, "response_error.txt", HttpStatus.SC_NOT_FOUND);

		// When
		try
		{
			wileyPaymentGateway.authorizePayment(authorizeRequest);
			fail("Expected ExternalSystemException");
		}
		catch (ExternalSystemException e)
		{
			//success
		}
		catch (Throwable t)
		{
			fail("Unexpected exception type. Expected ExternalSystemException");
		}
	}

	private WileySubscriptionAuthorizeRequest createAuthorizeRequest(final String subscriptionId)
	{
		Currency currency = Currency.getInstance(TEST_CURRENCY);
		BigDecimal totalAmount = BigDecimal.valueOf(TEST_TOTAL_AMOUNT);
		BillingInfo billingInfo = new BillingInfo();
		billingInfo.setCountry(wileyCountryService.findCountryByCode(TEST_COUNTRY_ISOCODE).getNumeric());
		billingInfo.setPostalCode(TEST_POSTAL_CODE);
		billingInfo.setStreet1(TEST_STREET_1);
		WileySubscriptionAuthorizeRequest request = new WileySubscriptionAuthorizeRequest(TEST_SITE_ID,
				subscriptionId, currency, totalAmount, billingInfo, TEST_PAYMENT_PROVIDER);
		return request;
	}

	private WileyAuthorizeResult createExpectedAuthorizeResult(final WileySubscriptionAuthorizeRequest request)
	{
		BigDecimal totalAmount = BigDecimal.valueOf(TEST_TOTAL_AMOUNT);
		WileyAuthorizeResult result = new WileyAuthorizeResult();
		result.setAuthorizationCode(TEST_AUTH_CODE);
		result.setMerchantResponse(MERCHANT_RESPONSE);
		result.setTotalAmount(totalAmount);
		result.setStatus(WileyTransactionStatusEnum.SUCCESS);
		return result;
	}

	private void assertEqualsAuthorizeResults(final WileyAuthorizeResult expexted, final WileyAuthorizeResult actual)
	{
		assertEquals(expexted.getTotalAmount(), actual.getTotalAmount());
		assertEquals(expexted.getStatus(), actual.getStatus());
		assertEquals(expexted.getAuthorizationCode(), actual.getAuthorizationCode());
		assertEquals(expexted.getMerchantResponse(), actual.getMerchantResponse());
	}
}
