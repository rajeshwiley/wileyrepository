package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.testframework.TestModelUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.wileyas.order.impl.WileyasCalculationService;
import com.wiley.facades.as.order.CancelResult;
import com.wiley.facades.as.order.WileyasCancelOrderFacade;
import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import com.wiley.integration.apigee.AbstractExternalTaxesRelatedIntegrationTest;

import static de.hybris.platform.core.enums.OrderStatus.CANCELLED;


@IntegrationTest
public class WileyasCancelOrderFacadeIntegrationTest extends AbstractExternalTaxesRelatedIntegrationTest
{
	private static final String IMPEX_BASE_PATH = "/wileytest/import/order/WileyasOrderFacadeIntegrationTest";
	private static final String COMMON_DATA_PATH = IMPEX_BASE_PATH + "/common.impex";
	private static final String ORDER_ENTRY_DATA_PATH = IMPEX_BASE_PATH + "/cancel-order-entry.impex";
	private static final String ORDER_DATA_PATH = IMPEX_BASE_PATH + "/cancel-order.impex";
	private static final String CHANGE_PRICE_PATH = IMPEX_BASE_PATH + "/change-price.impex";
	private static final String ORDER_DATA_BP_PATH = IMPEX_BASE_PATH + "/cancel-order-active-bp.impex";
	private static final String ORDER_CANCELLED_ENTRIES_DATA_PATH = IMPEX_BASE_PATH + "/order-cancelled-entries.impex";
	private static final String CUSTOMER_ID = "alm_id1";
	private static final String AGENT_ID = "asagent1";
	private static final String AGENT_PASSWORD = "1234";
	private static final String BASE_SITE_ID = "asSite";
	private static final String BASE_STORE_ID = "asStore";
	private static final String ORDER_CODE = "asCancellableTestOrder";
	private static final String ORDER_CODE1 = "test-order-code";
	private static final Integer ENTRY_NUMBER = 1;
	private static final Double ENTRY_TOTAL_PRICE_AFTER_CANCEL = 0.0;
	private static final Double ENTRY_SUBTOTAL_PRICE_AFTER_CANCEL = 0.0;
	private static final Long ENTRY_QUANTITY_AFTER_CANCEL = 0L;
	private static final Double ORDER_TOTAL_PRICE_AFTER_CANCEL_ENTRY = 13.0;
	private static final Double ORDER_SUBTOTAL_PRICE_WITHOUT_DISCOUNT_AFTER_CANCEL_ENTRY = 15.0;

	@Resource
	private WileyasCancelOrderFacade wileyasCancelOrderFacade;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource(name = "wileyasOrderEditCalculationService")
	private WileyasCalculationService wileyasCalculationService;

	@Resource(name = "wileyasExternalTaxesService")
	private ExternalTaxesService externalTaxesService;

	@Resource
	private WileyAssistedServiceFacade wileyAssistedServiceFacade;

	@Before
	public void importData() throws Exception
	{
		baseSiteService.setCurrentBaseSite(BASE_SITE_ID, false);
		importCsv(COMMON_DATA_PATH, DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void cancelOrderByAgent() throws OrderCancelException, ImpExException, AssistedServiceException
	{
		wileyAssistedServiceFacade.launchAssistedServiceMode();
		wileyAssistedServiceFacade.loginAssistedServiceAgent(AGENT_ID, AGENT_PASSWORD);
		wileyAssistedServiceFacade.emulateCustomer(CUSTOMER_ID, null);

		final OrderModel orderModel = cancelOrder(CancelReason.AUTHOR_REQUEST);
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();

		assertEquals(CancelReason.AUTHOR_REQUEST, entries.get(0).getCancelReason());
		assertEquals(CancelReason.AUTHOR_REQUEST, entries.get(1).getCancelReason());
		assertEquals(CancelReason.CANCEL_REBILL, entries.get(2).getCancelReason());
	}

	@Test
	public void cancelOrderByCustomer() throws OrderCancelException, ImpExException
	{
		final OrderModel orderModel = cancelOrder(null);
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		assertNull("Cancel reason isn't null", entries.get(0).getCancelReason());
		assertNull("Cancel reason isn't null", entries.get(1).getCancelReason());
		assertNotNull("Cancel reason is null", entries.get(2).getCancelReason());
	}

	private OrderModel cancelOrder(final CancelReason cancelReason) throws OrderCancelException, ImpExException
	{
		importCsv(ORDER_DATA_PATH, DEFAULT_ENCODING);

		CancelResult result = wileyasCancelOrderFacade.cancelOrder(ORDER_CODE, cancelReason);
		assertEquals("Wrong result", CancelResult.CANCELLED, result);

		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(BASE_STORE_ID);
		final OrderModel order = customerAccountService.getOrderForCode(ORDER_CODE, baseStore);
		assertNotNull("Can't find order", order);
		assertEquals("Wrong Order status", CANCELLED, order.getStatus());
		assertEquals("Wrong Order subtotal", 0d, order.getSubtotal(), 0.001);
		assertEquals("Wrong Order total", 0d, order.getTotalPrice(), 0.001);

		for (AbstractOrderEntryModel entry : order.getEntries())
		{
			assertEquals("Wrong Order entry status", CANCELLED, entry.getStatus());
			assertEquals("Wrong Order entry subtotal", 0d, entry.getSubtotalPrice(), 0.001);
			assertEquals("Wrong Order entry total", 0d, entry.getTotalPrice(), 0.001);
		}

		return order;
	}

	@Test
	public void cancelOrderActiveBp() throws OrderCancelException, ImpExException
	{
		importCsv(ORDER_DATA_BP_PATH, DEFAULT_ENCODING);

		CancelResult result = wileyasCancelOrderFacade.cancelOrder(ORDER_CODE, null);
		assertEquals("Wrong result", CancelResult.BP_ACTIVE_ERROR, result);
	}

	@Test
	public void testCancelOrderEntryByAgent() throws Exception
	{
		wileyAssistedServiceFacade.launchAssistedServiceMode();
		wileyAssistedServiceFacade.loginAssistedServiceAgent(AGENT_ID, AGENT_PASSWORD);
		wileyAssistedServiceFacade.emulateCustomer(CUSTOMER_ID, null);

		final AbstractOrderEntryModel entry = testCancelOrderEntry(CancelReason.AUTHOR_REQUEST);
		assertEquals(CancelReason.AUTHOR_REQUEST, entry.getCancelReason());
	}

	@Test
	public void testCancelOrderEntryByCustomer() throws Exception
	{
		final AbstractOrderEntryModel entry = testCancelOrderEntry(null);
		assertNull(entry.getCancelReason());
	}

	private AbstractOrderEntryModel testCancelOrderEntry(final CancelReason cancelReason) throws Exception
	{
		importCsv(ORDER_ENTRY_DATA_PATH, DEFAULT_ENCODING);

		OrderModel order = getOrderForCode(ORDER_CODE1);
		wileyasCalculationService.calculate(order);
		int entriesSize = order.getEntries().size();

		wileyasCancelOrderFacade.cancelOrderEntry(ORDER_CODE1, ENTRY_NUMBER, cancelReason);
		AbstractOrderEntryModel entry = order.getEntries().get(1);

		assertEquals(entriesSize, order.getEntries().size());
		assertEquals(ENTRY_TOTAL_PRICE_AFTER_CANCEL, entry.getTotalPrice());
		assertEquals(ENTRY_SUBTOTAL_PRICE_AFTER_CANCEL, entry.getSubtotalPrice());
		assertEquals(ENTRY_QUANTITY_AFTER_CANCEL, entry.getQuantity());
		assertEquals(OrderStatus.CANCELLED, entry.getStatus());
		assertTrue(CollectionUtils.isNotEmpty(entry.getTaxValues()));
		assertEquals(1, entry.getTaxValues().size());
		assertEquals(0, entry.getTaxValues().stream().findFirst().get().getValue(), 0.001);

		return entry;
	}

	@Test
	public void testTaxCalculationAfterEntryCancel() throws ImpExException, CalculationException, OrderCancelException
	{
		importCsv(ORDER_ENTRY_DATA_PATH, DEFAULT_ENCODING);
		OrderModel order = getOrderForCode(ORDER_CODE1);
		assertEquals(2, order.getEntries().size());
		wileyasCalculationService.calculate(order);
		externalTaxesService.calculateExternalTaxes(order);

		Double orderTotalTaxBeforeEntryCancel = order.getTotalTax();
		Double secondEntryTaxBeforeEntryCancel = order.getEntries().get(1).getTaxValues().stream().findFirst().get().getValue();
		assertNotNull(orderTotalTaxBeforeEntryCancel);
		assertNotNull(secondEntryTaxBeforeEntryCancel);

		importCsv(CHANGE_PRICE_PATH, DEFAULT_ENCODING);

		wileyasCancelOrderFacade.cancelOrderEntry(ORDER_CODE1, 0, CancelReason.AUTHOR_REQUEST);
		order = TestModelUtils.reReadModel(order);
		assertNotEquals(orderTotalTaxBeforeEntryCancel, order.getTotalTax());
		Double secondEntryTaxAfterEntryCancel = order.getEntries().get(1).getTaxValues().stream().findFirst().get().getValue();
		assertNotNull(secondEntryTaxAfterEntryCancel);
		assertNotEquals(secondEntryTaxBeforeEntryCancel, secondEntryTaxAfterEntryCancel);
		assertEquals(order.getTotalTax(), secondEntryTaxAfterEntryCancel);
	}

	@Test
	public void testOrderTotalsAfterEntryCancel() throws Exception
	{
		importCsv(ORDER_ENTRY_DATA_PATH, DEFAULT_ENCODING);

		wileyasCancelOrderFacade.cancelOrderEntry(ORDER_CODE1, ENTRY_NUMBER, null);

		OrderModel order = getOrderForCode(ORDER_CODE1);
		assertEquals(ORDER_TOTAL_PRICE_AFTER_CANCEL_ENTRY, order.getTotalPrice());
		assertEquals(ORDER_SUBTOTAL_PRICE_WITHOUT_DISCOUNT_AFTER_CANCEL_ENTRY, order.getSubTotalWithoutDiscount());
	}

	@Test
	public void testRollbackChanges() throws Exception
	{
		importCsv(ORDER_ENTRY_DATA_PATH, DEFAULT_ENCODING);
		OrderModel order = getOrderForCode(ORDER_CODE1);
		wileyasCalculationService.calculate(order);

		OrderData data = wileyasCancelOrderFacade.getOrderDetailsAfterEntryCancelled(order.getCode(), ENTRY_NUMBER);

		assertNotEquals(data.getTotalPrice(), order.getTotalPrice());
		assertNotEquals(data.getSubTotalWithoutDiscount(), order.getSubTotalWithoutDiscount());
	}


	@Test
	public void testCanCancelOrder() throws Exception
	{
		importCsv(ORDER_CANCELLED_ENTRIES_DATA_PATH, DEFAULT_ENCODING);

		final boolean cancellable = wileyasCancelOrderFacade.isCancellable(ORDER_CODE);
		assertFalse("Order is cancellable", cancellable);
	}


	private OrderModel getOrderForCode(final String code)
	{
		return customerAccountService.getOrderForCode((CustomerModel) getUserService().getCurrentUser(),
				code,
				baseStoreService.getCurrentBaseStore());
	}
}
