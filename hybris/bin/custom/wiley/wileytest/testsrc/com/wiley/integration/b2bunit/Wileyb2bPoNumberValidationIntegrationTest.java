package com.wiley.integration.b2bunit;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static java.util.Arrays.asList;


@IntegrationTest
public class Wileyb2bPoNumberValidationIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bPoNumberValidationIntegrationTest.class);

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/Wileyb2bPoNumberValidationIntegrationTest/";
	private static final String VALID_PO1 = "validPO1";
	private static final String VALID_PO2 = "validPO2";
	private static final String INVALID_PO1 = "invalidPO1";
	private static final String INVALID_PO2 = "invalidPO2";
	private static final String SAP_ACCOUNT_NUMBER = "sapAccountNumber45";

	private static final List<String> VALID_PO_NUMBERS = Arrays.asList(VALID_PO1, VALID_PO2);

	private static final List<String> ENABLED_SITES = asList("wileyb2b");
	private static final List<String> DISABLED_SITES = asList("wileyb2c", "ags", "wel");

	@Resource
	private WileyB2BUnitExternalService wileyB2BUnitExternalService;
	@Resource
	private HttpComponentsClientHttpRequestFactory b2bUnitRequestFactory;
	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		baseSiteService.setCurrentBaseSite("wileyb2b", true);
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void shouldReturnListOfInvalidPONumbers() throws Exception
	{
		// Given
		final List<String> poNumbers = Arrays.asList(VALID_PO1, INVALID_PO1, INVALID_PO2);
		setupWireMockSuccessfulResponse(
				"{\"poNumbers\":[\"" + VALID_PO1 + "\",\"" + INVALID_PO1 + "\",\"" + INVALID_PO2 + "\"]}",
				"response_successWithInvalidPONumbers.json");
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, poNumbers);
		//Then
		assertEquals(Arrays.asList(INVALID_PO1, INVALID_PO2), invalidPONumbers);
	}

	@Test
	public void shouldReturnEmptyListIfAllPONumbersAreValid() throws Exception
	{
		// Given
		setupWireMockSuccessfulResponse("{\"poNumbers\":[\"" + VALID_PO1 + "\",\"" + VALID_PO2 + "\"]}",
				"response_successWithValidPONumbers.json");
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus400BadRequest() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_BAD_REQUEST);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus401Unauthorized() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_UNAUTHORIZED);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus403Forbidden() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_FORBIDDEN);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus404NotFound() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_NOT_FOUND);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus500InternalError() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	@Test
	public void shouldReturnEmptyListInCaseOfHttpStatus503ServiceUnavailable() throws Exception
	{
		// Given
		setupWireMockErrorResponse(HttpStatus.SC_SERVICE_UNAVAILABLE);
		//When
		final List<String> invalidPONumbers = wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, VALID_PO_NUMBERS);
		//Then
		assertTrue(invalidPONumbers.isEmpty());
	}

	private void setupWireMockSuccessfulResponse(final String requestBodyContains, final String responseBodyFileName)
			throws Exception
	{
		wireMock.
				stubFor(post(urlMatching("/b2b/accounts/" + SAP_ACCOUNT_NUMBER + "/purchaseOrderNumbers"))
						.withHeader("Content-Type", matching("application/json"))
						.withHeader("Accept", matching("application/json"))
						.withHeader("Src", equalTo("hybris"))
						.withHeader("TransactionId", matching(".*"))
						.withRequestBody(
								containing(requestBodyContains))

						.willReturn(aResponse()
								.withStatus(HttpStatus.SC_OK)
								.withHeader("Content-Type", "application/json")
								.withBody(readFromFile(TEST_REQUESTS_PATH + responseBodyFileName))));
	}

	private void setupWireMockErrorResponse(final int httpStatus)
	{
		final String requestMappingUrl = "/b2b/accounts/" + SAP_ACCOUNT_NUMBER + "/purchaseOrderNumbers";
		setupWireMockPostErrorResponse(requestMappingUrl, httpStatus);
	}


	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return b2bUnitRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("validatePONumbers", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final List<String> poNumbers = Arrays.asList(VALID_PO1, INVALID_PO1, INVALID_PO2);
				setupWireMockSuccessfulResponse(
						"{\"poNumbers\":[\"" + VALID_PO1 + "\",\"" + INVALID_PO1 + "\",\"" + INVALID_PO2 + "\"]}",
						"response_successWithInvalidPONumbers.json");
				//When
				wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, poNumbers);
				return null;
			}
		});
		return funcMap;
	}
}
