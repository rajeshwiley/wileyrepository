package com.wiley.integration.users;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.users.gateway.WileyUsersGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.util.Arrays.asList;


@IntegrationTest
public class WileyUsersGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyUsersGatewayIntegrationTest.class);

	private static final String TEST_RESPONSE_PATH =
			"/wileytest/integration/WileyUsersGatewayIntegrationTest/response/";
	private static final String TEST_SUCCESS_CUSTOMER_ID = "valid_a@a.com";
	private static final String TEST_400_CUSTOMER_ID = "malformed_a@a.com";
	private static final String TEST_401_CUSTOMER_ID = "not_authenticate_a@a.com";
	private static final String TEST_404_CUSTOMER_ID = "not_found_a@a.com";
	private static final String TEST_500_CUSTOMER_ID = "server_error_a@a.com";
	private static final String USER_FIRST_NAME = "test_first_name";
	private static final String USER_LAST_NAME = "test_last_name";

	private static final List<String> DISABLED_SITES = asList("ags", "wel");
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource
	private HttpComponentsClientHttpRequestFactory usersRequestFactory;

	@Resource
	private WileyUsersGateway wileyUsersGateway;

	@Resource
	private BaseSiteService baseSiteService;

	@Override
	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

		setUpOauth(wireMock);
	}

	@Test
	public void searchCustomerSuccessCaseTest() throws Exception
	{

		setupWireMockSuccessfulResponse(TEST_SUCCESS_CUSTOMER_ID, HttpStatus.SC_OK,
				"users_searchCustomer_valid_response.json");

		// When
		final CustomerModel customer = wileyUsersGateway
				.searchCustomerByEmail(TEST_SUCCESS_CUSTOMER_ID);

		// Then
		Assert.assertEquals(customer.getFirstName(), USER_FIRST_NAME);
		Assert.assertEquals(customer.getLastName(), USER_LAST_NAME);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void searchCustomer400RequestTest() throws Exception
	{
		setupWireMockErrorResponse(TEST_400_CUSTOMER_ID, HttpStatus.SC_BAD_REQUEST);
		wileyUsersGateway.searchCustomerByEmail(TEST_400_CUSTOMER_ID);
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void searchCustomer401RequestTest() throws Exception
	{
		setupWireMockErrorResponse(TEST_401_CUSTOMER_ID, HttpStatus.SC_UNAUTHORIZED);
		wileyUsersGateway.searchCustomerByEmail(TEST_401_CUSTOMER_ID);
	}

	@Test(expected = ExternalSystemNotFoundException.class)
	public void searchCustomer404RequestTest() throws Exception
	{
		setupWireMockErrorResponse(TEST_404_CUSTOMER_ID, HttpStatus.SC_NOT_FOUND);
		wileyUsersGateway.searchCustomerByEmail(TEST_404_CUSTOMER_ID);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void searchCustomer500RequestTest() throws Exception
	{
		setupWireMockErrorResponse(TEST_500_CUSTOMER_ID, HttpStatus.SC_INTERNAL_SERVER_ERROR);
		wileyUsersGateway.searchCustomerByEmail(TEST_500_CUSTOMER_ID);
	}

	private void setupWireMockSuccessfulResponse(final String userId, final int statusCode,
			final String responseBodyFileName) throws Exception
	{
		wireMock.stubFor(get(urlEqualTo("/users/" + userId)).withHeader("Content-Type", matching("application/json"))
				.withHeader("Accept", matching("application/json")).withHeader("Src", equalTo("hybris"))
				.withHeader("TransactionId", matching(".*"))

				.willReturn(aResponse().withStatus(statusCode).withHeader("Content-Type", "application/json")
						.withBody(readFromFile(TEST_RESPONSE_PATH + responseBodyFileName))));
	}

	private void setupWireMockErrorResponse(final String userId, final int httpStatus)
	{
		wireMock.stubFor(get(urlEqualTo("/users/" + userId))
				.willReturn(aResponse().withStatus(httpStatus).withHeader("Content-Type", "application/json")));
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return usersRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("searchCustomerByEmail", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				setupWireMockSuccessfulResponse(TEST_SUCCESS_CUSTOMER_ID, HttpStatus.SC_OK,
						"users_searchCustomer_valid_response.json");
				wileyUsersGateway.searchCustomerByEmail(TEST_SUCCESS_CUSTOMER_ID);
				return null;
			}
		});
		return funcMap;
	}
}
