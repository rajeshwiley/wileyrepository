package com.wiley.integration.mpgs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;


import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.utils.HandshakeUtils;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;



@IntegrationTest
public class WileyMPGSPaymentGatewayTokenizeIntegrationTest extends WileyAbstractWireMockTest
{

	private  static final String JSON_CONTENT_TYPE = "application/json";

	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String TOKENIZATION_REQUEST_PATH = "/api/rest/version/44/merchant/TESTWILEYABATEST/token/";


	private static final String SUCCESS_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/tokenization_request_success.json";
	private static final String SUCCCESS_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/tokenization_response_success.json";

	private static final String ERROR_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/tokenization_request_error.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/tokenization_response_error.json";
	private static final String EMPTY_SESSION_ID = null;
	private static final String SESSION_ID = "SESSION0002893994386G920688978J3";
	private static final String SUCCESS_STATUS_DETAILS = "BASIC_VERIFICATION_SUCCESSFUL";
	private static final String TOKEN = "4508753994831019";
	private static final String CARD_EXPIRY_MONTH = "05";
	private static final String CARD_EXPIRY_YEAR = "21";
	private static final String CARD_NUMBER = "450875xxxxxx1019";
	private static final String CARD_SCHEME = "VISA";

	@Resource
	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Resource
	private RestTemplate mpgsPaymentGatewayClientRestTemplate;

	@Before
	public void setUp() throws Exception
	{
		final HttpComponentsClientHttpRequestFactory requestFactory =
				(HttpComponentsClientHttpRequestFactory) mpgsPaymentGatewayClientRestTemplate.getRequestFactory();

		if (requestFactory != null)
		{
			HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
		}
	}


	@Test
	public void testTokenizationPositive() throws Exception
	{

		wireMock.stubFor(post(urlMatching(TOKENIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCCESS_RESPONSE_JSON_FILE_PATH))
				));

		WileyTokenizationRequest request = new WileyTokenizationRequest(WIRE_MOCK_URL + TOKENIZATION_REQUEST_PATH,
				SESSION_ID, WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		request.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);

		WileyTokenizationResponse response =  mpgsPaymentGateway.tokenize(request);

		assertEquals(TransactionStatus.ACCEPTED.name(), response.getStatus());
		assertEquals(SUCCESS_STATUS_DETAILS, response.getStatusDetails());
		assertEquals(TOKEN, response.getToken());
		assertEquals(CARD_EXPIRY_MONTH, response.getCardExpiryMonth());
		assertEquals(CARD_EXPIRY_YEAR, response.getCardExpiryYear());
		assertEquals(CARD_NUMBER, response.getCardNumber());
		assertEquals(CARD_SCHEME, response.getCardScheme());

	}

	@Test(expected = HttpClientErrorException.class)
	public void testTokenizationError() throws Exception
	{

		wireMock.stubFor(post(urlMatching(TOKENIZATION_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(ERROR_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_BAD_REQUEST)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))
				));

		WileyTokenizationRequest request = new WileyTokenizationRequest(WIRE_MOCK_URL + TOKENIZATION_REQUEST_PATH,
				EMPTY_SESSION_ID, WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		request.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);

		WileyTokenizationResponse response =  mpgsPaymentGateway.tokenize(request);
	}
}