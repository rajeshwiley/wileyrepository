package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.esb.EsbB2CSubscriptionGateway;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;

import static java.util.Arrays.asList;


/**
 * Created by Oleg_Dolgopolov on 9/22/2016.
 */
@IntegrationTest
public class WileyB2CChangeSubscriptionPaymentMethodGatewayIntegrationTest extends
		WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG =
			LoggerFactory.getLogger(WileyB2CChangeSubscriptionPaymentMethodGatewayIntegrationTest.class);

	private static final String RESOURCE_PREFIX =
			"/wileytest/integration/WileyB2CChangeSubscriptionPaymentMethodGatewayIntegrationTest";

	private static final String TEST_SUBSCRIPTION_CODE = "SUBSCRIPTION_CODE";
	private static final String TEST_EXTERNAL_SUBSCRIPTION_CODE = "SUBSCRIPTION_EXTERNAL_CODE";

	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("wileyb2b", "ags", "wel");

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private EsbB2CSubscriptionGateway esbB2CSubscriptionGateway;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	WileySubscriptionService subscriptionService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		importCsv(RESOURCE_PREFIX + "/impex/subscription.impex", DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite("wileyb2c", true);

		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void changeSubscriptionPaymentMethodSuccessTest() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/changeSubscriptionPaymentMethodSuccess.json",
				200
		);

		wireMock.addStubMapping(stubMapping);

		WileySubscriptionModel wileySubscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		final PaymentInfoModel paymentInfoModel = wileySubscriptionModel.getOrderEntry().getOrder().getPaymentInfo();

		// ---------- When ---------
		esbB2CSubscriptionGateway.changePaymentMethod(paymentInfoModel, TEST_EXTERNAL_SUBSCRIPTION_CODE);

		// ---------- Then ----------
		// success
	}

	@Test
	public void changeSubscriptionPaymentMethod400Test() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/changeSubscriptionPaymentMethod400.json"
		);
		wireMock.addStubMapping(stubMapping);

		WileySubscriptionModel wileySubscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		final PaymentInfoModel paymentInfoModel = wileySubscriptionModel.getOrderEntry().getOrder().getPaymentInfo();
		paymentInfoModel.getBillingAddress().setPostalcode("400400");

		// ---------- When ---------
		try
		{
			esbB2CSubscriptionGateway.changePaymentMethod(paymentInfoModel, TEST_EXTERNAL_SUBSCRIPTION_CODE);
			fail("Expected ExternalSystemBadRequestException");
		}
		catch (ExternalSystemBadRequestException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	public void changeSubscriptionPaymentMethod401Test() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/changeSubscriptionPaymentMethod401.json"
		);
		wireMock.addStubMapping(stubMapping);

		WileySubscriptionModel wileySubscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		final PaymentInfoModel paymentInfoModel = wileySubscriptionModel.getOrderEntry().getOrder().getPaymentInfo();
		paymentInfoModel.getBillingAddress().setPostalcode("401401");

		// ---------- When ---------
		try
		{
			esbB2CSubscriptionGateway.changePaymentMethod(paymentInfoModel, TEST_EXTERNAL_SUBSCRIPTION_CODE);
			fail("Expected ExternalSystemBadRequestException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	public void changeSubscriptionPaymentMethod404Test() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/changeSubscriptionPaymentMethod404.json"
		);
		wireMock.addStubMapping(stubMapping);

		WileySubscriptionModel wileySubscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		final PaymentInfoModel paymentInfoModel = wileySubscriptionModel.getOrderEntry().getOrder().getPaymentInfo();
		paymentInfoModel.getBillingAddress().setPostalcode("404404");

		// ---------- When ---------
		try
		{
			esbB2CSubscriptionGateway.changePaymentMethod(paymentInfoModel, TEST_EXTERNAL_SUBSCRIPTION_CODE);
			fail("Expected ExternalSystemBadRequestException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("changePaymentMethod", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/changeSubscriptionPaymentMethodSuccess.json",
						200
				));

				WileySubscriptionModel wileySubscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
				final PaymentInfoModel paymentInfoModel = wileySubscriptionModel.getOrderEntry().getOrder().getPaymentInfo();

				esbB2CSubscriptionGateway.changePaymentMethod(paymentInfoModel, TEST_EXTERNAL_SUBSCRIPTION_CODE);
				return null;
			}
		});
		return funcMap;
	}
}
