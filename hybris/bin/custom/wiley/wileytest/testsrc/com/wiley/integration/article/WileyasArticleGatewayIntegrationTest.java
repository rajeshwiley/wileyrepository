package com.wiley.integration.article;

import de.hybris.bootstrap.annotations.IntegrationTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.article.ArticleGateway;
import com.wiley.core.integration.article.dto.ArticleDto;


@IntegrationTest
public class WileyasArticleGatewayIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasArticleGatewayIntegrationTest.class);

	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileyasGetArticleDataGatewayIntegrationTest";

	private static final String RESPONSE_ARTICLE_NAME = "Test article";
	private static final String RESPONSE_DOI = "10.1111/medu.12601";
	private static final String RESPONSE_JOURNAL_ID = "121212121";
	private static final String RESPONSE_JOURNAL_NAME = "Test journal";
	private static final String RESPONSE_PICTURE_URL =
			"https://static1.squarespace.com/static/54dd763ce4b01f6b05bab7db/t/"
					+ "5a19e2e9085229dccc8fdfaa/1511645937570/logos-014__2_.png";

	@Resource(name = "articleGateway")
	private ArticleGateway articleGateway;

	@Resource
	private RestTemplate articleApigeeEdgeOAuth2RestTemplate;

	private ArticleDto responseDto;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		responseDto = null;
		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void callSuccessfulTest() throws Exception
	{
		// given
		StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_article_data_200.json",
				RESOURCE_PREFIX + "/response/response_get_article_data_success.json"
		);
		wireMock.addStubMapping(stubMapping);

		// when
		responseDto = articleGateway.getArticleData("100");

		// then
		checkSuccessfulDtoFromResponse(responseDto);
	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void callFailed400Test() throws Exception
	{
		callFailedTest("400");
	}

	@Test(expected = ExternalSystemUnauthorizedException.class)
	public void callFailed401Test() throws Exception
	{
		callFailedTest("401");
	}

	@Test(expected = ExternalSystemNotFoundException.class)
	public void callFailed404Test() throws Exception
	{
		callFailedTest("404");
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void callFailed500Test() throws Exception
	{
		callFailedTest("500");
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void callFailed503Test() throws Exception
	{
		callFailedTest("503");
	}

	private void callFailedTest(final String code) throws Exception
	{
		// given
		StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/mapping_get_article_data_" + code + ".json",
				Integer.parseInt(code));
		wireMock.addStubMapping(stubMapping);

		// when
		responseDto = articleGateway.getArticleData(code);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) articleApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}

	private void checkSuccessfulDtoFromResponse(final ArticleDto dto)
	{
		assertNotNull(dto);
		assertEquals(RESPONSE_ARTICLE_NAME, dto.getArticleName());
		assertEquals(RESPONSE_DOI, dto.getDoi());
		assertEquals(RESPONSE_JOURNAL_ID, dto.getJournalId());
		assertEquals(RESPONSE_JOURNAL_NAME, dto.getJournalName());
		assertEquals(RESPONSE_PICTURE_URL, dto.getPictureUrl());
	}
}
