package com.wiley.integration.mpgs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Currency;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.utils.HandshakeUtils;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.put;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;


@IntegrationTest
public class WileyMPGSPaymentGatewayRefundIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String JSON_CONTENT_TYPE = "application/json";
	private static final String TEST_ORDER_ID = "TEST_ORDER_ID1";
	private static final String TEST_ORDER_URL_PART = "/order/";
	private static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID1";
	private static final String TEST_TRANSACTION_URL_PART = "/transaction/";
	private static final String TEST_CURRENCY_CODE = "USD";
	private static final Currency TEST_CURRENCY = Currency.getInstance(TEST_CURRENCY_CODE);
	private static final BigDecimal TEST_AMOUNT = BigDecimal.ONE;
	private static final BigDecimal ERROR_AMOUNT = null;
	private static final String STATUS_APPROVED = "APPROVED";
	private static final String SUCCESS_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/refund_request_success.json";
	private static final String SUCCCESS_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/refund_response_success.json";
	private static final String ERROR_REQUEST_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/refund_request_error.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/refund_response_error.json";
	private static final String TEST_REFUND_REQUEST_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST"
					+ TEST_ORDER_URL_PART
					+ TEST_ORDER_ID
					+ TEST_TRANSACTION_URL_PART
					+ TEST_TRANSACTION_ID;
	public static final String TEST_TRANSACTION_REFERENCE = "0257000000000001";

	@Resource
	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Resource
	private RestTemplate mpgsPaymentGatewayClientRestTemplate;

	@Before
	public void setUp() throws Exception
	{
		final HttpComponentsClientHttpRequestFactory requestFactory =
				(HttpComponentsClientHttpRequestFactory) mpgsPaymentGatewayClientRestTemplate.getRequestFactory();

		if (requestFactory != null)
		{
			HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
		}
	}

	@Test
	public void testRefundSuccess() throws Exception
	{
		wireMock.stubFor(put(urlMatching(TEST_REFUND_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(SUCCESS_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_CREATED)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(SUCCCESS_RESPONSE_JSON_FILE_PATH))
				));


		WileyFollowOnRefundRequest request = new WileyFollowOnRefundRequest(WIRE_MOCK_URL + TEST_REFUND_REQUEST_PATH,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, TEST_CURRENCY, TEST_AMOUNT);
		request.setTransactionReference(TEST_TRANSACTION_REFERENCE);

		WileyFollowOnRefundResponse response = mpgsPaymentGateway.refundFollowOn(request);

		assertEquals(TransactionStatus.ACCEPTED.name(), response.getStatus());
		assertEquals(STATUS_APPROVED, response.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, response.getRequestId());
		assertEquals(TEST_CURRENCY, response.getCurrency());
		assertEquals(TEST_AMOUNT, response.getTotalAmount());
	}

	@Test(expected = HttpClientErrorException.class)
	public void testRefundError() throws Exception
	{
		wireMock.stubFor(put(urlMatching(TEST_REFUND_REQUEST_PATH))
				.withRequestBody(
						equalToJson(loadFileAsString(ERROR_REQUEST_JSON_FILE_PATH))
				).willReturn(
						aResponse()
								.withStatus(HttpStatus.SC_BAD_REQUEST)
								.withHeader("Content-Type", JSON_CONTENT_TYPE)
								.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))
				));


		WileyFollowOnRefundRequest request = new WileyFollowOnRefundRequest(WIRE_MOCK_URL + TEST_REFUND_REQUEST_PATH,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, TEST_CURRENCY, ERROR_AMOUNT);
		request.setTransactionReference(TEST_TRANSACTION_REFERENCE);

		mpgsPaymentGateway.refundFollowOn(request);
	}
}
