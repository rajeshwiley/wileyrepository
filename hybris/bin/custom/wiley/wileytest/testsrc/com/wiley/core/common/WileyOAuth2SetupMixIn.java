package com.wiley.core.common;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;


public interface WileyOAuth2SetupMixIn
{

	String RESOURCE_PREFIX = "/wileytest/integration/common";

	default void setUpOauth(final WileyWireMockRule wireMock) throws Exception
	{
		final StubMapping stubMapping = wireMock.setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/oauth2_success.json",
				RESOURCE_PREFIX + "/responses/response_oauthSuccess.json"
		);
		wireMock.addStubMapping(stubMapping);
	}
}
