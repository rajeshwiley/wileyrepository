package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.order.dao.WileycomOrderDao;
import com.wiley.facades.wileyb2c.order.WileyRegCodeCheckoutFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;
import static java.util.Arrays.asList;


@IntegrationTest
public class WileyRegCodeCheckoutFacadeIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyRegCodeCheckoutFacadeIntegrationTest.class);

	private static final String VALIDATE_REGCODE_PATH = "/regcodes/{regcode}";
	private static final String VALID_REG_CODE_PREFIX = "rc-";

	private static final String TEST_REQUESTS_PATH = "/wileytest/integration/WileyRegCodeCheckoutFacadeIntegrationTest/";

	private static final String WILEY_CATALOG_ID = "wileyProductCatalog";

	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("ags", "wel", "wileyb2b");

	@Resource(name = "wileyRegCodeCheckoutFacade")
	private WileyRegCodeCheckoutFacade wileyPlusCheckoutFacade;

	@Resource(name = "wileycomOrderDao")
	private WileycomOrderDao wileycomOrderDao;

	@Resource
	private SessionService sessionService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private HttpComponentsClientHttpRequestFactory sapErpRequestFactory;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileytest/integration/WileyRegCodeCheckoutFacadeIntegrationTest/updateStoreToDisableFulfilment.impex",
				DEFAULT_ENCODING);
		importCsv("/wileytest/integration/WileyRegCodeCheckoutFacadeIntegrationTest/addBasePriceForTestProduct.impex",
				DEFAULT_ENCODING);
		importCsv("/wileytest/integration/WileyRegCodeCheckoutFacadeIntegrationTest/addStockForTestProduct.impex",
				DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		getUserService().setCurrentUser(getUserService().getAnonymousUser());
		wileycomI18NService.setDefaultCurrentCountry();
		catalogVersionService.getCatalogVersion(WILEY_CATALOG_ID, CatalogManager.ONLINE_VERSION);

		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

		setUpOauth(wireMock);


	}

	@Test
	public void shouldPlaceOrderForRegcodeWithoutProduct() throws Exception
	{
		final String testedRegCode = "rc-111111111111";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_111111111111.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);

			String orderCode = wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);

			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);

			assertNotNull(orderCode);
			OrderModel order = wileycomOrderDao.findOrdersByCode(orderCode);
			assertNotNull(order);
			assertEquals(1, order.getEntries().size());
			assertEquals("WCOM_B2C_PHYSICAL", order.getEntries().get(0).getProduct().getCode());
			assertEquals(getUserService().getAnonymousUser(), order.getUser());
			// check if discount has been applied
			assertTrue(order.getEntries().get(0).getBasePrice().doubleValue() > 1);
			assertEquals(0, order.getTotalPrice(), 0.001);
			assertEquals("RegCode should be copied to the Order", testedRegCode, order.getRegistrationCode());
		}
		catch (RegCodeValidationException | RegCodeInvalidException ex)
		{
			fail("PlaceOrder error");
		}
	}

	@Test
	public void shouldPlaceOrderForRegcodePurchase() throws Exception
	{
		final String testedRegCode = "rc-111111111111";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_111111111111.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);

			CartActivationRequestDto cartInfo = createCartDto("111111111111");

			String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
					"coursePurchaseType");

			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);

			assertNotNull(orderCode);
			OrderModel order = wileycomOrderDao.findOrdersByCode(orderCode);
			assertNotNull(order);
			assertEquals(1, order.getEntries().size());
			assertEquals("WCOM_B2C_PHYSICAL", order.getEntries().get(0).getProduct().getCode());
			assertEquals(getUserService().getAnonymousUser(), order.getUser());
			// check if discount has been applied
			assertTrue(order.getEntries().get(0).getBasePrice().doubleValue() > 1);
			assertEquals(0, order.getTotalPrice(), 0.001);
			assertEquals("RegCode should be copied to the Order", testedRegCode, order.getRegistrationCode());
		}
		catch (RegCodeValidationException | RegCodeInvalidException ex)
		{
			fail("PlaceOrder error");
		}
	}


	@Test(expected = RegCodeInvalidException.class)
	public void shouldDetectNotFoundRegCode() throws Exception
	{
		final String testedRegCode = "invalid";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_NOT_FOUND, null);
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);
			try
			{
				CartActivationRequestDto cartInfo = createCartDto("111111111114");

				String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
						"coursePurchaseType");
			}
			catch (Exception ex)
			{
				int cartsAfter = getTableSize(CartModel._TYPECODE);
				assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
				throw ex;
			}
			fail("Should detect invalid reg code");
		}
		catch (RegCodeValidationException ex)
		{
			fail("PlaceOrder error");
		}

	}

	@Test(expected = RegCodeValidationException.class)
	public void shouldDetectEsbError() throws Exception
	{
		final String testedRegCode = "invalid";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_errormessage.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);
			try
			{
				CartActivationRequestDto cartInfo = createCartDto("111111111114");

				String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
						"coursePurchaseType");
			}
			catch (Exception ex)
			{
				int cartsAfter = getTableSize(CartModel._TYPECODE);
				assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
				throw ex;
			}
			fail("Should detect invalid reg code");
		}
		catch (RegCodeInvalidException ex)
		{
			fail("PlaceOrder error");
		}

	}

	@Test(expected = RegCodeInvalidException.class)
	public void shouldDetectBurntRegCode() throws Exception
	{
		final String testedRegCode = "rc-000";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_000.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);
			try
			{
				CartActivationRequestDto cartInfo = createCartDto("111111111114");

				String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
						"coursePurchaseType");
			}
			catch (Exception ex)
			{
				int cartsAfter = getTableSize(CartModel._TYPECODE);
				assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
				throw ex;
			}
			fail("Should detect burnt reg code");
		}
		catch (RegCodeValidationException ex)
		{
			fail("PlaceOrder error");
		}

	}

	@Test(expected = RegCodeInvalidException.class)
	public void shouldDetectRegCodeForUnknownProduct() throws Exception
	{
		final String testedRegCode = "rc-unknown";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_unknown.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);
			try
			{
				CartActivationRequestDto cartInfo = createCartDto("111111111114");

				String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
						"coursePurchaseType");
			}
			catch (Exception ex)
			{
				int cartsAfter = getTableSize(CartModel._TYPECODE);
				assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
				throw ex;
			}
			fail("Should detect wrong product");
		}
		catch (RegCodeValidationException ex)
		{
			fail("Wrong exception");
		}

	}

	@Test(expected = RegCodeInvalidException.class)
	public void shouldDetectRegCodeForAnotherProduct() throws Exception
	{
		final String testedRegCode = "rc-111111111115";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_111111111115.json");
		try
		{
			int cartsBefore = getTableSize(CartModel._TYPECODE);
			try
			{
				CartActivationRequestDto cartInfo = createCartDto("111111111114");

				String orderCode = wileyPlusCheckoutFacade.processRegistrationCodeForCourse(testedRegCode, cartInfo,
						"coursePurchaseType");
			}
			catch (Exception ex)
			{
				int cartsAfter = getTableSize(CartModel._TYPECODE);
				assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
				throw ex;
			}
			fail("Should detect wrong product");
		}
		catch (RegCodeValidationException ex)
		{
			fail("Wrong exception");
		}

	}

	private void createWireStubForRegcode(final String regCode, int expectedResponseStatus, final String jsonFileName)
			throws Exception
	{
		if (expectedResponseStatus != HttpStatus.SC_OK)
		{
			makeStubWithoutBody(regCode, expectedResponseStatus);
		}
		else
		{
			makeStubWithBody(regCode, expectedResponseStatus, jsonFileName);
		}
	}

	@Test(expected = RegCodeValidationException.class)
	public void shouldDetect400Error() throws Exception
	{
		final String testedRegCode = "rc-400";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_BAD_REQUEST, "response_error_anycode.json");
		int cartsBefore = getTableSize(CartModel._TYPECODE);
		try
		{
			String orderCode = wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);
		}
		catch (Exception ex)
		{
			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
			throw ex;
		}
		fail("Should detect ESB-error");
	}

	@Test(expected = RegCodeValidationException.class)
	public void shouldDetect401Error() throws Exception
	{
		final String testedRegCode = "rc-401";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_UNAUTHORIZED, "response_error_anycode.json");
		int cartsBefore = getTableSize(CartModel._TYPECODE);
		try
		{
			String orderCode = wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);
		}
		catch (Exception ex)
		{
			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
			throw ex;
		}
		fail("Should detect ESB-error");
	}

	@Test(expected = RegCodeValidationException.class)
	public void shouldDetect500Error() throws Exception
	{
		final String testedRegCode = "rc-500";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_INTERNAL_SERVER_ERROR, "response_error_anycode.json");
		int cartsBefore = getTableSize(CartModel._TYPECODE);
		try
		{
			String orderCode = wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);
		}
		catch (Exception ex)
		{
			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
			throw ex;
		}
		fail("Should detect ESB-error");
	}

	@Test(expected = RegCodeValidationException.class)
	public void shouldDetect503Error() throws Exception
	{
		final String testedRegCode = "rc-503";
		createWireStubForRegcode(testedRegCode, HttpStatus.SC_SERVICE_UNAVAILABLE, "response_error_anycode.json");
		int cartsBefore = getTableSize(CartModel._TYPECODE);
		try
		{
			String orderCode = wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);
		}
		catch (Exception ex)
		{
			int cartsAfter = getTableSize(CartModel._TYPECODE);
			assertEquals("Temporary cart should be removed", cartsBefore, cartsAfter);
			throw ex;
		}
		fail("Should detect ESB-error");
	}

	private void makeStubWithoutBody(final String regCode, int expectedResponseStatus) throws Exception
	{
		wireMock.
				stubFor(get(urlMatching(
						VALIDATE_REGCODE_PATH
								.replace("{regcode}", regCode)))
						.withHeader("Authorization", WireMock.equalTo("bearer bd55e2ce-92b0-46a2-a8f4-82a69440b586"))
						.withHeader("TransactionId", WireMock.equalTo("randomString"))
						.willReturn(
								aResponse()
										.withStatus(expectedResponseStatus)
						));
	}

	private void makeStubWithBody(final String regCode, int expectedResponseStatus, final String jsonFileName) throws Exception
	{
		wireMock.
				stubFor(get(urlMatching(
						VALIDATE_REGCODE_PATH
								.replace("{regcode}", regCode)))
						.withHeader("Authorization", WireMock.equalTo("bearer bd55e2ce-92b0-46a2-a8f4-82a69440b586"))
						.withHeader("TransactionId", WireMock.equalTo("randomString"))
						.willReturn(
								aResponse()
										.withStatus(expectedResponseStatus)
										.withHeader("Content-Type", "application/json")
										.withBody(loadFileAsString(TEST_REQUESTS_PATH + jsonFileName))
						));
	}

	private String makeResponceForRegcode(final String regCode)
	{
		try
		{
			final JSONObject responseBody = new JSONObject();

			if (!regCode.startsWith(VALID_REG_CODE_PREFIX))
			{
				responseBody.put("code", regCode);
				responseBody.put("message", "Something went wrong");
			}
			else
			{
				responseBody.put("code", regCode);
				responseBody.put("isbn", regCode.substring(VALID_REG_CODE_PREFIX.length()));
				responseBody.put("isBurned", regCode.endsWith("0"));
			}
			return responseBody.toString();
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying to build response.", e);
		}

	}


	private CartActivationRequestDto createCartDto(final String isbn)
	{
		CartActivationRequestDto cartDto = new CartActivationRequestDto();
		WileyPlusCourseDto course = new WileyPlusCourseDto();
		course.setIsbn(isbn);
		cartDto.setCourse(course);
		WebLinkDto continueUrl = new WebLinkDto();
		continueUrl.setUrl("someContinueURL");
		cartDto.continueURL(continueUrl);
		return cartDto;
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return sapErpRequestFactory;
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("updateCustomer", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final String testedRegCode = "rc-111111111111";
				createWireStubForRegcode(testedRegCode, HttpStatus.SC_OK, "response_success_111111111111.json");

				wileyPlusCheckoutFacade.placeRegCodeOrder(testedRegCode);
				return null;
			}
		});
		return funcMap;
	}
}
