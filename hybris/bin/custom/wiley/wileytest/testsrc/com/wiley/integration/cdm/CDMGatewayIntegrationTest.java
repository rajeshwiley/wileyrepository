package com.wiley.integration.cdm;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.integration.cdm.CDMGateway;
import com.wiley.core.integration.cdm.dto.CDMCreateUserResponse;
import com.wiley.utils.HandshakeUtils;

import static java.util.Arrays.asList;


/**
 * Integration test for {@link CDMGateway}.
 */
@IntegrationTest
public class CDMGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

  private static final Logger LOG = LoggerFactory.getLogger(CDMGatewayIntegrationTest.class);
  private static final String RESOURCE_PREFIX = "/wileytest/integration/CDMGatewayIntegrationTest/";
  private static final String CREATE_CUSTOMER_URL_PATTERN = "/cdmservice/v1/CreateCustomer";
  @Resource
  private CDMGateway cdmGateway;

  @Resource
  private CustomerAccountService customerAccountService;

  @Resource
  private BaseStoreService baseStoreService;

  @Resource
  private BaseSiteService baseSiteService;

  @Resource
  private HttpComponentsClientHttpRequestFactory cdmClientHttpRequestFactory;

  // Test data
  private static final String WEL_SITE_UID = "wel";
  private static final String TEST_HYBRIS_ORDER_CODE = "test_hybris_order";

  private static final List<String> ENABLED_SITES = asList("ags", "wel");
  private static final List<String> DISABLED_SITES = asList("wileyb2c", "wileyb2b");

  @Before
  public void setUp() throws Exception {
    final StubMapping stubMapping = setUpStubMapping(
            RESOURCE_PREFIX + "mappings/oauth2_success.json",
            RESOURCE_PREFIX + "mappings/response_oauthSuccess.json"
    );
    wireMock.addStubMapping(stubMapping);

    final HttpComponentsClientHttpRequestFactory requestFactory = getFactoryToAdjust();

    if (requestFactory != null)
    {
      HandshakeUtils.adjustRequestFactoryForTests(requestFactory);
    }

    // getting test contact form file to save it in cdm.
    importCsv(
            "/wileyfulfilmentprocess/test/integration/CDMGatewayIntegrationTest/testCreateCDMContact.impex",
            DEFAULT_ENCODING);


    wireMock.addMockServiceRequestListener((request, response) ->
    {
      LOG.debug("WireMock Request: {}", request);
      LOG.debug("WireMock Response: {}", response);
    });
  }

  @Test
  public void testCreateCDMCustomerWithGateway() throws Exception
  {

    final StubMapping stubMapping = setUpStubMapping(
            RESOURCE_PREFIX + "/mappings/create_customer_successful.json", // mapping
            200 // response status
    );

    wireMock.addStubMapping(stubMapping);

    // Given
    final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WEL_SITE_UID);

    final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
            baseStore);
    final CustomerModel customer = (CustomerModel) order.getUser();

    // When
    final CDMCreateUserResponse response = cdmGateway.createCDMCustomer(customer, order.getSite());

    wireMock.verify(1, getRequestPatternBuilder(
            CREATE_CUSTOMER_URL_PATTERN,
            RequestMethod.POST));

    assertNotNull("Should return respone.", response);
    assertEquals("OK", response.getStatus());
    assertEquals("123", response.getGuid());

  }

  @Override
  protected List<String> getDisabledSites()
  {
    return DISABLED_SITES;
  }

  @Override
  protected List<String> getEnabledSites()
  {
    return ENABLED_SITES;
  }

  @Override
  protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
  {
    Map<String, Callable<T>> funcMap = new HashMap<>();
    funcMap.put("createCDMCustomer", new Callable<T>()
    {
      @Override
      public T call() throws Exception
      {
        wireMock.addStubMapping(setUpStubMapping(
                RESOURCE_PREFIX + "/mappings/create_customer_successful.json", // mapping
                200 // response status
        ));

        // Given
        final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(site);

        final OrderModel order = customerAccountService.getOrderForCode(TEST_HYBRIS_ORDER_CODE,
                baseStoreService.getBaseStoreForUid(WEL_SITE_UID));
        final CustomerModel customer = (CustomerModel) order.getUser();
        cdmGateway.createCDMCustomer(customer, baseSite);
        return null;
      }
    });
    return funcMap;
  }

  @Override
  public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
  {
    return cdmClientHttpRequestFactory;
  }
}
