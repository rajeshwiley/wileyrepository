package com.wiley.core.wileycom.validation;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.model.WileyProductModel;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;


@IntegrationTest
public class WileyBaseProductIntegrationValidationIntegrationTest
		extends AbstractIntegrationValidationIntegrationTest<WileyProductModel>
{

	private static final String EXTERNAL_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE
			= "The attribute \"externalImage\" must contain both the normal and thumbnail formats.";

	private WileyProductModel testProduct;

	@Before
	public void setup()
	{
		super.setup();
		testProduct = (WileyProductModel) getProductService().getProductForCode(getCatalogVersion(), "TEST_PRODUCT");

	}

	@Test
	public void shouldValidateSizeConstraints()
	{
		validateSizeConstraint(testProduct::setName, "name", 1, 255, false);
		validateSizeConstraint(testProduct::setDescription, "description", 1, 21844);
		validateSizeConstraint(testProduct::setAboutAuthors, "aboutAuthors", 1, 21844);
		validateSizeConstraint(testProduct::setPressRelease, "pressRelease", 1, 21844);
		validateSizeConstraint(testProduct::setWhatsNew, "whatsNew", 1, 21844);
		validateSizeConstraint(testProduct::setNewToEdition, "newToEdition", 1, 21844);
		validateSizeConstraint(testProduct::setTableOfContents, "tableOfContents", 1, 5592404);
		validateSizeConstraint(testProduct::setReviews, "reviews", 1, 5592404);
		validateSizeConstraint(testProduct::setRelatedWebsites, "relatedWebsites", 1, 5592404);
		validateSizeConstraint(testProduct::setNotes, "notes", 1, 5592404);
		validateSizeConstraint(testProduct::setErrata, "errata", 1, 5592404);
		validateSizeConstraint(testProduct::setSearchKeywords, "searchKeywords", 1, 21844);
	}

	@Test
	public void shouldValidateNotNullConstraints()
	{
		validateNotNullConstraint(testProduct::setName, "name", randomAlphabetic(10));
		validateNotNullConstraint(testProduct::setTextLanguage, "textLanguage", new LanguageModel());
		validateNotNullConstraint(testProduct::setTextbook, "textbook", Boolean.TRUE);
		validateNotNullConstraint(testProduct::setBestSeller, "bestSeller", Boolean.TRUE);
	}


	@Test
	public void shouldValidateGalleryImagesContainerHasThumbnail()
	{
		final MediaContainerModel externalImage = testProduct.getExternalImage();
		removeImageWithName(externalImage, THUMBNAIL_IMAGE_NAME);
		failed(testProduct::setExternalImage, externalImage, EXTERNAL_IMAGES_MUST_CONTAIN_REQUIRED_IMAGE_FORMATS_MESSAGE);
	}

	@Override
	protected WileyProductModel getTestItem()
	{
		return testProduct;
	}
}
