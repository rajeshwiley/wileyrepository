package com.wiley.test.gateway;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import static org.junit.Assert.fail;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.test.gateway.exception.SomeSpecificRuntimeException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;


/**
 * Integration test contains test cases for spring configuration.
 */
@IntegrationTest
public class GatewayConfigurationIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private ApplicationContext parentApplicationContext;

	@Before
	public void setUp() throws Exception
	{
		this.parentApplicationContext = Registry.getApplicationContext();
	}

	@Test
	public void testVoidMethod() throws Exception
	{
		// Given
		ApplicationContext applicationContext = initializeTestApplicationContext(parentApplicationContext,
				"/wileytest/gateway/GatewayConfigurationIntegrationTest/context/spring-context.xml");

		SimpleTestGateway simpleTestGateway = applicationContext.getBean("simpleTestGateway", SimpleTestGateway.class);

		// When
		try
		{
			simpleTestGateway.voidMethod();
			fail("Expected SomeSpecificRuntimeException.");
		}
		catch (SomeSpecificRuntimeException e)
		{
			// Then
			// success
		}
	}

	@Test
	public void testReturnableMethod() throws Exception
	{
		ApplicationContext applicationContext = initializeTestApplicationContext(parentApplicationContext,
				"/wileytest/gateway/GatewayConfigurationIntegrationTest/context/spring-context.xml");

		SimpleTestGateway simpleTestGateway = applicationContext.getBean("simpleTestGateway", SimpleTestGateway.class);

		try
		{
			simpleTestGateway.returnableMethod();
			fail("Expected SomeSpecificRuntimeException.");
		}
		catch (SomeSpecificRuntimeException e)
		{
			// Then
			// success
		}
	}

	@Test
	public void testAsyncMethod() throws Exception
	{
		ApplicationContext applicationContext = initializeTestApplicationContext(parentApplicationContext,
				"/wileytest/gateway/GatewayConfigurationIntegrationTest/context/spring-context.xml");

		AsyncTestGateway asyncTestGateway = applicationContext.getBean("asyncTestGateway", AsyncTestGateway.class);

		try
		{
			Future<Object> future = asyncTestGateway.asyncMethod();
			future.get();
			fail("Expected SomeSpecificRuntimeException.");
		}
		catch (ExecutionException e)
		{
			// Then
			if (!(e.getCause() instanceof SomeSpecificRuntimeException))
			{
				throw e;
			}
			// else success
		}
	}

}
