package com.wiley.integration.media;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.util.Config;

import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.wileycom.media.WileycomGetMediaGateway;
import com.wiley.core.wileycom.media.WileycomVerifyMediaGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.head;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WileycomMediaGatewayIntegrationTest extends WileyAbstractWireMockTest
{
	private static final String REQUEST_PATH = "/WileyRemoteAPI/Cover.rapi?isbn=";
	private static final String ISBN = "123456789012";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = "http://localhost:" + WIREMOCK_PORT;
	private static final String IMAGE_URL = "/product_data/coverImage/12/1234567890/123456789012.jpg";
	private static final String LOCATION_HEADER_VALUE =
			WIRE_MOCK_URL + IMAGE_URL;
	private static final String LOCATION_HEADER = "Location";

	@Resource
	private WileycomVerifyMediaGateway wileycomVerifyMediaGateway;

	@Resource
	private WileycomGetMediaGateway wileycomGetMediaGateway;

	@Test
	public void testGetUrlWhenSuccessShouldReturnURL()
	{
		wireMock.stubFor(get(urlMatching(Pattern.quote(REQUEST_PATH + ISBN)))
				.willReturn(
						aResponse()
								.withStatus(302)
								.withHeader(LOCATION_HEADER, LOCATION_HEADER_VALUE)
				));
		wireMock.stubFor(head(urlMatching(Pattern.quote(IMAGE_URL)))
				.willReturn(
						aResponse()
								.withStatus(200)
				));

		String url = wileycomGetMediaGateway.getURL(ISBN);
		wileycomVerifyMediaGateway.verifyURL(url);


		Assert.assertEquals(url, LOCATION_HEADER_VALUE);
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void testGetUrlWhenFailureShouldThrowException()
	{
		wireMock.stubFor(get(urlMatching(Pattern.quote(REQUEST_PATH + ISBN)))
				.willReturn(
						aResponse()
								.withStatus(500)
				));

		wileycomGetMediaGateway.getURL(ISBN);
	}

	@Test(expected = HttpClientErrorException.class)
	public void testGetUrlWhenBrokenUrlShouldThrowException()
	{
		wireMock.stubFor(get(urlMatching(Pattern.quote(REQUEST_PATH + ISBN)))
				.willReturn(
						aResponse()
								.withStatus(302)
								.withHeader(LOCATION_HEADER, LOCATION_HEADER_VALUE)
				));
		wireMock.stubFor(head(urlMatching(Pattern.quote(IMAGE_URL)))
				.willReturn(
						aResponse()
								.withStatus(404)
				));

		String url = "";
		try
		{
			url = wileycomGetMediaGateway.getURL(ISBN);
		}
		catch (Exception e)
		{
			Assert.fail("Fail to get URL");
		}
		wileycomVerifyMediaGateway.verifyURL(url);
	}
}
