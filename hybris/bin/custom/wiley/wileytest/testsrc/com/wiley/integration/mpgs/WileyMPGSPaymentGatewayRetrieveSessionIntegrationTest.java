package com.wiley.integration.mpgs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.util.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.wiley.core.common.WileyAbstractWireMockTest;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;

import wiremock.org.apache.http.HttpStatus;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.wiley.utils.FileTestUtils.loadFileAsString;



@IntegrationTest
public class WileyMPGSPaymentGatewayRetrieveSessionIntegrationTest extends WileyAbstractWireMockTest
{
	private  static final String JSON_CONTENT_TYPE = "application/json";

	private static final String HOST = "http://localhost";
	private static final int WIREMOCK_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));
	private static final String WIRE_MOCK_URL = HOST + ":" + WIREMOCK_PORT;
	private static final String SESSION_ID = "SESSION0002111632485G7328485L72";
	private static final String RESTRIVE_SESSION_REQUEST_SUCCESS_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST/session/" + SESSION_ID;
	private static final String RESTRIVE_SESSION_REQUEST_ERROR_PATH =
			"/api/rest/version/44/merchant/TESTWILEYABATEST/session/";

	private static final String SUCCESS_RESPONSE_JSON_FILE_PATH =
						"/wileytest/integration/WileyMPGSIntegrationTest/retrieve_session_response_success.json";
	private static final String ERROR_RESPONSE_JSON_FILE_PATH =
			"/wileytest/integration/WileyMPGSIntegrationTest/retrieve_session_response_error.json";

	@Resource
 	private WileyMPGSPaymentGateway mpgsPaymentGateway;

	@Test
 	public void testRetrieveSessionSuccess() throws Exception
	{
		wireMock.stubFor(get(urlMatching(RESTRIVE_SESSION_REQUEST_SUCCESS_PATH)).willReturn(
				aResponse()
						.withStatus(HttpStatus.SC_OK)
						.withHeader("Content-Type", JSON_CONTENT_TYPE)
						.withBody(loadFileAsString(SUCCESS_RESPONSE_JSON_FILE_PATH))));

		WileyRetrieveSessionRequest retrieveSessionRequest = new WileyRetrieveSessionRequest(
				WIRE_MOCK_URL + RESTRIVE_SESSION_REQUEST_SUCCESS_PATH,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);


		WileyRetrieveSessionResponse retrieveSessionResponse = mpgsPaymentGateway.retrieveSession(retrieveSessionRequest);

		assertEquals(TransactionStatus.ACCEPTED.name(), retrieveSessionResponse.getStatus());
		assertNotNull("[sessionId] must be returned from retrieve session call", retrieveSessionResponse.getSessionId());
	}

	@Test(expected = HttpClientErrorException.class)
	public void testRetrieveSessionError() throws Exception
	{
		wireMock.stubFor(get(urlMatching(RESTRIVE_SESSION_REQUEST_ERROR_PATH)).willReturn(
				aResponse()
						.withStatus(HttpStatus.SC_BAD_REQUEST)
						.withHeader("Content-Type", JSON_CONTENT_TYPE)
						.withBody(loadFileAsString(ERROR_RESPONSE_JSON_FILE_PATH))));

		WileyRetrieveSessionRequest retrieveSessionRequest = new WileyRetrieveSessionRequest(
				WIRE_MOCK_URL + RESTRIVE_SESSION_REQUEST_ERROR_PATH,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);

		WileyRetrieveSessionResponse retrieveSessionResponse = mpgsPaymentGateway.retrieveSession(retrieveSessionRequest);
	}
}
