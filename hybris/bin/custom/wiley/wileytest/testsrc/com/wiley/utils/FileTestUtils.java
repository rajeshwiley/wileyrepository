package com.wiley.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Utils that is used for loading test data in tests
 *
 * @author Dzmitryi_Halahayeu
 *
 */
public final class FileTestUtils
{
	private FileTestUtils()
	{
	}

	public static String loadFileAsString(final String path) throws Exception
	{
		return getLines(path).collect(Collectors.joining());
	}

	public static List<String> loadFileAsStrings(final String path) throws Exception
	{
		return getLines(path).collect(Collectors.toList());
	}

	private static Stream<String> getLines(final String path) throws IOException, URISyntaxException
	{
		return Files.lines(Paths.get(XmlTestUtils.class.getResource(path).toURI()));
	}
}
