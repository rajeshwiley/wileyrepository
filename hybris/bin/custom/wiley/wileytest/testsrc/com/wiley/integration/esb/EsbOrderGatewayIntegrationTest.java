package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;
import com.wiley.core.integration.esb.EsbOrderGateway;

import static java.util.Arrays.asList;



/**
 * Integration test for the gateway {@link EsbOrderGateway} that uploads orders in ESB.
 */
@IntegrationTest
public class EsbOrderGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{
	private static final Logger LOG = LoggerFactory.getLogger(EsbOrderGatewayIntegrationTest.class);
	private static final String RESOURCE_PREFIX = "/wileytest/integration/EsbOrderGatewayIntegrationTest";

	private static final String TEST_ORDER_CODE = "ordeer";

	private static final String UPLOAD_ORDER_URL_PATTERN = "/orders";
	private static final RequestMethod UPLOAD_ORDER_REQUEST_METHOD = RequestMethod.POST;

	private static final List<String> DISABLED_SITES = asList("ags", "wel");
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");
	private static final String MAPPINGS_UPLOAD_ORDER_JSON = "/mappings/upload_order_%s.json";
	public static final String WILEYB2C = "wileyb2c";

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private EsbOrderGateway esbOrderGateway;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		importCsv(RESOURCE_PREFIX + "/impex/orders.impex", DEFAULT_ENCODING);

		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}


	@Test
	public void verifyUploadOrderSuccessCase() throws Exception
	{
		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // mapping
				204 // response status
		);

		wireMock.addStubMapping(stubMapping);


		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);
		// ---------- When ---------
		esbOrderGateway.sendOrder(orderModel);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				UPLOAD_ORDER_URL_PATTERN,
				UPLOAD_ORDER_REQUEST_METHOD));
	}


	@Test
	public void verifyUploadOrderCaseWhen400StatusCode() throws Exception
	{

		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				400, // response status code
				ExternalSystemBadRequestException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}


	@Test
	public void verifyUploadOrderCaseWhen401StatusCode() throws Exception
	{

		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);


		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				401, // response status code
				ExternalSystemUnauthorizedException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUploadOrderCaseWhen403StatusCode() throws Exception
	{

		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);


		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				403, // response status code
				ExternalSystemUnauthorizedException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}


	@Test
	public void verifyUploadOrderCaseWhen404StatusCode() throws Exception
	{
		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);


		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				404, // response status code
				ExternalSystemNotFoundException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}


	@Test
	public void verifyUploadOrderCaseWhen500StatusCode() throws Exception
	{
		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);


		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				500, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}


	@Test
	public void verifyUploadOrderCaseWhen503StatusCode() throws Exception
	{
		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);


		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbOrderGateway.sendOrder(orderModel), // executable method
				String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C), // stub mapping
				"/responses/error_response.json", // response body
				503, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPLOAD_ORDER_URL_PATTERN,  // expected request url
				UPLOAD_ORDER_REQUEST_METHOD // expected request method
		);
	}





	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{

		final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid(WILEYB2C);
		final OrderModel orderModel = customerAccountService.getOrderForCode(TEST_ORDER_CODE, baseStoreModel);
		orderModel.setSite(baseSiteService.getBaseSiteForUID(site));

		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("sendOrder", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				String jsonPath = RESOURCE_PREFIX + String.format(MAPPINGS_UPLOAD_ORDER_JSON,
						baseStoreService.getBaseStoreForUid(site).getUid());

				if (this.getClass().getResource(jsonPath) == null)
				{
					jsonPath = RESOURCE_PREFIX + String.format(MAPPINGS_UPLOAD_ORDER_JSON, WILEYB2C);
				}
				// load mapping
				wireMock.addStubMapping(setUpStubMapping(jsonPath, 204));
				esbOrderGateway.sendOrder(orderModel);
				return null;
			}
		});
		return funcMap;
	}
}
