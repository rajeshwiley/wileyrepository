package com.wiley.fulfilmentprocess.impl;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.pin.service.impl.DefaultPinIntegrationService;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.request;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.github.tomakehurst.wiremock.http.RequestMethod.GET;
import static com.github.tomakehurst.wiremock.http.RequestMethod.POST;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Integration test which mocks http responses from Wiley Core external system to
 * validate the behaviour of {@code DefaultPinIntegrationService}
 */
@IntegrationTest
public class DefaultPinIntegrationServiceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String ORDER_CODE = "order_1";
	private static final String MATCHED_URL = "/CGI-BIN/LANSAWEB.*";
	private static final int WILEYCORE_PORT = Integer.parseInt(Config.getParameter("wiremock.instanceSpecificPort"));

	@Rule
	public final WireMockRule wireMock = new WireMockRule(wireMockConfig().port(WILEYCORE_PORT));

	@Resource
	private DefaultPinIntegrationService defaultPinIntegrationService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wel/pins.impex", "utf-8");
	}

	@Test
	public void shouldUsePinForOrderPlacement()
	{
		assertTrue(defaultPinIntegrationService.isPinUsedForOrderPlacement(findOrder(ORDER_CODE)));
	}

	@Test
	public void shouldValidatePinForOrderReturnTrue()
	{
		setupResponse(GET, createResponse(false, true));
		assertTrue(defaultPinIntegrationService.validatePinForOrder(findOrder(ORDER_CODE)));
	}

	@Test
	public void shouldValidatePinForOrderReturnFalseIfInvalid()
	{
		setupResponse(GET, createResponse(false, false));
		assertFalse(defaultPinIntegrationService.validatePinForOrder(findOrder(ORDER_CODE)));
	}

	@Test
	public void shouldActivatePinForOrderReturnTrue()
	{
		setupResponse(POST, createResponse(true, true));
		assertTrue(defaultPinIntegrationService.activatePinForOrder(findOrder(ORDER_CODE)));
	}

	@Test
	public void shouldActivatePinForOrderReturnFalseIfInvalid()
	{
		setupResponse(POST, createResponse(true, false));
		assertFalse(defaultPinIntegrationService.activatePinForOrder(findOrder(ORDER_CODE)));
	}

	private OrderModel findOrder(final String orderCode)
	{
		final Map<String, String> params = new HashMap<>();
		params.put("code", orderCode);
		return (OrderModel) flexibleSearchService.search("SELECT {" + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
				+ "} WHERE {" + OrderModel.CODE + "} = ?code", params).getResult().get(0);
	}

	private void setupResponse(final RequestMethod method, final String response)
	{
		wireMock.stubFor(request(method.value(), urlMatching(MATCHED_URL)).willReturn(aResponse().withStatus(200)
				.withHeader("Content-Type", "text/html").withBody(response)));
	}

	private String createResponse(final boolean activate, final boolean success) {
		return "\n\n\n\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<message>\n\t<action>"
				+ (activate ? "ACTIVATE" : "VALIDATE")
				+ "</action>\n\t<pins>\n\n\t\t<pinresponse>\n\t\t\t<pin>PININ123456789</pin>\n\t\t\t"
				+ (success ? "<sku>9780470371671</sku>" : "<sku/>")
				+ "\n\t\t\t<responsecode>"
				+ (success ? "0000" : "0001")
				+ "</responsecode>\n\t\t\t"
				+ (success ? "<responsedescription/>" : "<responsedescription>Invalid PIN</responsedescription>\n")
				+ "\n\t\t</pinresponse>\n\n\n\t</pins>\n</message>";
	}
}
