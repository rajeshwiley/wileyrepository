package com.wiley.integration.apigee;

import javax.annotation.Resource;

import org.junit.Before;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest;


/**
 * Created by Maksim Kozich on 10.08.2018.
 */
public abstract class AbstractExternalTaxesRelatedIntegrationTest extends WileyAbstractOAuth2RequestFactoryAdjusterWireMockTest
{
	public static final String EXTERNAL_TAXES_CALCULATE_RESPONSE_MAPPING =
			"/wileytest/integration/common/mappings/taxes-calculate-200.json";

	@Resource
	private RestTemplate taxApigeeEdgeOAuth2RestTemplate;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		final StubMapping stubMapping = createStubMappingFrom(EXTERNAL_TAXES_CALCULATE_RESPONSE_MAPPING);
		wireMock.addStubMapping(stubMapping);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) taxApigeeEdgeOAuth2RestTemplate.getRequestFactory();
	}
}
