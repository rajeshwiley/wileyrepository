package com.wiley.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.esb.EsbExternalAddressGateway;

import static java.util.Arrays.asList;


/**
 * Created by Aliaksei_Zlobich on 7/14/2016.
 */
@IntegrationTest
public class EsbExternalAddressGatewayIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG = LoggerFactory.getLogger(EsbExternalAddressGatewayIntegrationTest.class);

	private static final String RESOURCE_PREFIX = "/wileytest/integration/EsbExternalAddressGatewayIntegrationTest";

	private static final String ADD_SHIPPING_ADDRESS_URL_PATTERN = "/customers/test-customer-id-1234/shippingAddresses";
	private static final RequestMethod ADD_SHIPPING_ADDRESS_REQUEST_METHOD = RequestMethod.POST;

	private static final String UPDATE_SHIPPING_ADDRESS_URL_PATTERN =
			"/customers/test-customer-id-1234/shippingAddresses/some-external-shipping-address-id-123";
	private static final RequestMethod UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD = RequestMethod.PUT;

	private static final String ADD_BILLING_ADDRESS_URL_PATTERN = "/customers/test-customer-id-1234/billingAddresses";
	private static final RequestMethod ADD_BILLING_ADDRESS_REQUEST_METHOD = RequestMethod.POST;

	private static final String UPDATE_BILLING_ADDRESS_URL_PATTERN =
			"/customers/test-customer-id-1234/billingAddresses/some-external-billing-address-id-123";
	private static final RequestMethod UPDATE_BILLING_ADDRESS_REQUEST_METHOD = RequestMethod.PUT;

	private static final String TEST_USER_UID = "test";

	private static final List<String> DISABLED_SITES = asList("ags", "wel");
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource
	private EsbExternalAddressGateway esbExternalAddressGateway;

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		importCsv(RESOURCE_PREFIX + "/impex/customerData.impex", DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
	}

	@Test
	public void verifyAddShippingAddressSuccessCase() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/add_shipping_address.json", // mapping
				204 // response status
		);

		wireMock.addStubMapping(stubMapping);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// ---------- When ---------
		esbExternalAddressGateway.addShippingAddress(addressModel, customerModel);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				ADD_SHIPPING_ADDRESS_URL_PATTERN,
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyAddShippingAddressWhen400StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addShippingAddress(addressModel, customerModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				400, // response status code
				ExternalSystemBadRequestException.class,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddShippingAddressWhen401StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addShippingAddress(addressModel, customerModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				401, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddShippingAddressWhen403StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addShippingAddress(addressModel, customerModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				403, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddShippingAddressWhen404StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addShippingAddress(addressModel, customerModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				404, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddShippingAddressWhen500StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addShippingAddress(addressModel, customerModel), // executable method
				"/mappings/add_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				500, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateShippingAddressSuccessCase() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/update_shipping_address.json", // mapping
				204 // response status
		);
		wireMock.addStubMapping(stubMapping);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// ---------- When ---------
		esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				UPDATE_SHIPPING_ADDRESS_URL_PATTERN,
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyUpdateShippingAddressWhen400StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel), // executable method
				"/mappings/update_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				400, // response status code
				ExternalSystemBadRequestException.class,  // expected exception
				UPDATE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateShippingAddressWhen401StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel), // executable method
				"/mappings/update_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				401, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateShippingAddressWhen404StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel), // executable method
				"/mappings/update_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				404, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateShippingAddressWhen500StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel), // executable method
				"/mappings/update_shipping_address.json", // stub mapping
				"/responses/error_response.json", // response body
				500, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_SHIPPING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_SHIPPING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddBillingAddressSuccessCase() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/add_billing_address.json", // mapping
				204 // response status
		);
		wireMock.addStubMapping(stubMapping);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// ---------- When ---------
		esbExternalAddressGateway.addBillingAddress(addressModel, customerModel);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				ADD_BILLING_ADDRESS_URL_PATTERN,
				ADD_BILLING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyAddBillingAddressWhen400StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addBillingAddress(addressModel, customerModel), // executable method
				"/mappings/add_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				400, // response status code
				ExternalSystemBadRequestException.class,  // expected exception
				ADD_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddBillingAddressWhen401StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addBillingAddress(addressModel, customerModel), // executable method
				"/mappings/add_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				401, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_BILLING_ADDRESS_REQUEST_METHOD  // expected request method
		);
	}

	@Test
	public void verifyAddBillingAddressWhen404StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addBillingAddress(addressModel, customerModel), // executable method
				"/mappings/add_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				404, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyAddBillingAddressWhen500StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.addBillingAddress(addressModel, customerModel), // executable method
				"/mappings/add_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				500, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				ADD_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				ADD_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateBillingAddressSuccessCase() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/update_billing_address.json", // mapping
				204 // response status
		);
		wireMock.addStubMapping(stubMapping);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// ---------- When ---------
		esbExternalAddressGateway.updateBillingAddress(addressModel, customerModel);

		// ---------- Then ----------
		wireMock.verify(1, getRequestPatternBuilder(
				UPDATE_BILLING_ADDRESS_URL_PATTERN,
				UPDATE_BILLING_ADDRESS_REQUEST_METHOD));
	}

	@Test
	public void verifyUpdateBillingAddressWhen400StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateBillingAddress(addressModel, customerModel), // executable method
				"/mappings/update_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				400, // response status code
				ExternalSystemBadRequestException.class,  // expected exception
				UPDATE_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateBillingAddressWhen401StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateBillingAddress(addressModel, customerModel), // executable method
				"/mappings/update_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				401, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateBillingAddressWhen404StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateBillingAddress(addressModel, customerModel), // executable method
				"/mappings/update_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				404, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Test
	public void verifyUpdateBillingAddressWhen500StatusCode() throws Exception
	{
		// --------- Given ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);

		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultPaymentAddress();

		// --------- Then ---------
		internalVerifyExecutableMethodWithExceptionBehavior(RESOURCE_PREFIX,
				() -> esbExternalAddressGateway.updateBillingAddress(addressModel, customerModel), // executable method
				"/mappings/update_billing_address.json", // stub mapping
				"/responses/error_response.json", // response body
				500, // response status code
				ExternalSystemInternalErrorException.class,  // expected exception
				UPDATE_BILLING_ADDRESS_URL_PATTERN,  // expected request url
				UPDATE_BILLING_ADDRESS_REQUEST_METHOD // expected request method
		);
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);

		Map<String, Callable<T>> funcMap = new HashMap<>();

		// --------- Shipping ---------
		importCsv(RESOURCE_PREFIX + "/impex/shippingAddress.impex", DEFAULT_ENCODING);
		final CustomerModel customerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel addressModel = customerModel.getDefaultShipmentAddress();
		funcMap.put("addShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + "/mappings/add_shipping_address.json", 204));
				esbExternalAddressGateway.addShippingAddress(addressModel, customerModel);
				return null;
			}
		});

		funcMap.put("updateShippingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + "/mappings/update_shipping_address.json", 204));
				esbExternalAddressGateway.updateShippingAddress(addressModel, customerModel);
				return null;
			}
		});
		// --------- Billing ---------
		importCsv(RESOURCE_PREFIX + "/impex/billingAddress.impex", DEFAULT_ENCODING);
		final CustomerModel billingCustomerModel = (CustomerModel) setUpUser(TEST_USER_UID);
		final AddressModel billingAddressModel = customerModel.getDefaultPaymentAddress();
		funcMap.put("addBillingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + "/mappings/add_billing_address.json", 204));
				esbExternalAddressGateway.addBillingAddress(billingAddressModel, billingCustomerModel);
				return null;
			}
		});

		funcMap.put("updateBillingAddress", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(RESOURCE_PREFIX + "/mappings/update_billing_address.json", 204));
				esbExternalAddressGateway.updateBillingAddress(billingAddressModel, billingCustomerModel);
				return null;
			}
		});
		return funcMap;
	}
}
