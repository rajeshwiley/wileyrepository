package com.wiley.integration.lightningsource;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.HttpClientErrorException;

import com.wiley.core.common.WileyAbstractStoreAwareChainTest;
import com.wiley.core.lightningsource.LightningSourceGateway;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;


/**
 * Created by Georgii_Gavrysh on 9/7/2016.
 */
@IntegrationTest
public class LightningSourceGatewayIntegrationTest extends WileyAbstractStoreAwareChainTest
{

	private static final Logger LOG = Logger.getLogger(LightningSourceGatewayIntegrationTest.class);

	private static final List<String> ENABLED_SITES = asList("wileyb2c");
	private static final List<String> DISABLED_SITES = asList("wileyb2b", "ags", "wel");

	@Resource(name = "lightningSourceGateway")
	private LightningSourceGateway lightningSourceGateway;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		importCsv("/wileycore/test/order/LightningSourceGatewayIntegrationTest/orders.impex", DEFAULT_ENCODING);
		wireMock.resetMappings();
	}

	@Test
	public void testSuccess()
	{
		wireMock.stubFor(get(urlMatching("/ebook/items/9781118416518\\?orderno=ls_order_1_LIGHTNING_PRODUCT_01&"
				+ "instanceid=CORE&billtocountry=US&format=json"))
				.willReturn(
						aResponse()
								.withStatus(200)
								.withHeader("Content-Type", "text/json; charset=utf-8")
								.withBody("{\"IPWSeBookDownloadURL\":\"https://ebooktest.a_link\"}")
				));

		final OrderModel orderModel = getOrder("ls_order_1");
		final String link =	lightningSourceGateway.getLink("CA", orderModel.getEntries().get(0));

		Assert.assertThat(link, is("https://ebooktest.a_link"));
	}

	@Test(expected = HttpClientErrorException.class)
	public void testFailureCountry()
	{
		wireMock.stubFor(get(urlMatching("/ebook/items/9781118416518\\?orderno=LS_ORDER_NO_COUNTRY_LIGHTNING_PRODUCT_01&"
				+ "instanceid=CORE&billtocountry=CA&format=json"))
				.willReturn(
						aResponse()
								.withStatus(412)
								.withHeader("Content-Type", "text/html; charset=utf-8")
								.withBody("Bill To Country is Required")
				));

		final OrderModel orderModel = getOrder("LS_ORDER_NO_COUNTRY");
		lightningSourceGateway.getLink("CA", orderModel.getEntries().get(0));
	}

	private OrderModel getOrder(final String orderCode)
	{
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid("wileyb2c");
		return customerAccountService.getOrderForCode(orderCode, baseStore);
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);

		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("getLink", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.stubFor(get(urlMatching("/ebook/items/9781118416518\\?orderno=ls_order_1_LIGHTNING_PRODUCT_01&"
						+ "instanceid=CORE&billtocountry=US&format=json"))
						.willReturn(
								aResponse()
										.withStatus(200)
										.withHeader("Content-Type", "text/json; charset=utf-8")
										.withBody("{\"IPWSeBookDownloadURL\":\"https://ebooktest.a_link\"}")
						));

				final OrderModel orderModel = getOrder("ls_order_1");
				lightningSourceGateway.getLink("CA", orderModel.getEntries().get(0));
				return null;
			}
		});
		return funcMap;
	}
}
