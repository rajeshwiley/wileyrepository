package com.wiley.integration.customer;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;

import static java.util.Arrays.asList;


@IntegrationTest
public class WileyCustomerGatewayUpdateCustomerIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyCustomerGatewayIntegrationTest.class);
	private static final String RESOURCE_PREFIX = "/wileytest/integration/WileyCustomerGatewayIntegrationTest/";
	private static final String UPDATE_CUSTOMER_URL_PATTERN = "/customers/%s/profile";

	private static final String CUSTOMER_ID = "customer@test.com";
	private static final String CUSTOMER_ID_ERROR = "error";

	private static final List<String> DISABLED_SITES = asList("ags", "wel");
	private static final List<String> ENABLED_SITES = asList("wileyb2c", "wileyb2b");

	@Resource
	private WileyCustomerGateway wileyCustomerGateway;
	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;
	@Resource
	private UserService userService;
	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;
	@Resource
	private BaseSiteService baseSiteService;
	@Before
	public void setUp() throws Exception
	{
		super.setUp();
		// getting test contact form file to save it in cdm.
		wireMock.addMockServiceRequestListener((request, response) ->
		{
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		userService.setCurrentUser(userService.getAdminUser());
		importCsv(RESOURCE_PREFIX + "/impex/testCustomer.impex",
				DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getAnonymousUser());

	}

	@Test
	public void testSuccessfulUpdateOfB2CCustomer() throws Exception
	{

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/update_b2c_customer_successful.json", // mapping
				200 // response status
		);

		wireMock.addStubMapping(stubMapping);

		final CustomerModel customer = (CustomerModel) userService.getUserForUID("customer@test.com");

		wileyCustomerGateway.updateCustomer(customer);

		wireMock.verify(1, getRequestPatternBuilder(
				String.format(UPDATE_CUSTOMER_URL_PATTERN, CUSTOMER_ID),
				RequestMethod.PUT));

	}
	@Test
	public void testSuccessfulUpdateOfB2BCustomer() throws Exception
	{

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/update_b2b_customer_successful.json", // mapping
				200 // response status
		);

		wireMock.addStubMapping(stubMapping);

		final CustomerModel customer = (CustomerModel) userService.getUserForUID("b2bcustomerrustic");

		// When
		wileyCustomerGateway.updateCustomer(customer);

		wireMock.verify(1, getRequestPatternBuilder(
				String.format(UPDATE_CUSTOMER_URL_PATTERN, CUSTOMER_ID),
				RequestMethod.PUT));

	}

	@Test(expected = ExternalSystemBadRequestException.class)
	public void testFailedUpdateOfCustomer() throws Exception
	{

		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/update_any_customer_error.json", // mapping
				400 // response status
		);

		wireMock.addStubMapping(stubMapping);

		final CustomerModel customer = (CustomerModel) userService.getUserForUID("b2bcustomerrustic");

		// When
		customer.setCustomerID(CUSTOMER_ID_ERROR);
		wileyCustomerGateway.updateCustomer(customer);

		wireMock.verify(1, getRequestPatternBuilder(
				String.format(UPDATE_CUSTOMER_URL_PATTERN, CUSTOMER_ID_ERROR),
				RequestMethod.PUT));

	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("updateCustomer", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				wireMock.addStubMapping(setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/update_b2c_customer_successful.json", // mapping
						200 // response status
				));
				final CustomerModel customer = (CustomerModel) userService.getUserForUID("customer@test.com");

				wileyCustomerGateway.updateCustomer(customer);
				return null;
			}
		});
		return funcMap;
	}
}
