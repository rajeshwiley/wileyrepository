package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponservices.model.MultiCodeCouponModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.coupon.WileyCouponService;
import com.wiley.core.coupon.impl.WileyCouponCodeGenerationServiceImpl;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.wiley.order.WileyCartFacade;
import com.wiley.facades.wiley.order.impl.WileyCartFacadeImpl;
import com.wiley.integration.apigee.AbstractExternalTaxesRelatedIntegrationTest;


/**
 * Integration test for {@link WileyCartFacadeImpl}
 */
@IntegrationTest
public class WileyasCartFacadeIntegrationTest extends AbstractExternalTaxesRelatedIntegrationTest
{
	private static final String RESOURCE_PREFIX = "/wileytest/import/cart/WileyasCartFacadeIntegrationTest";
	public static final String BASE_SITE = "asSite";
	public static final String CUSTOMER_ID = "test@test.com";
	public static final String CUSTOMER1_ID = "test1@test.com";
	public static final String CUSTOMER2_ID = "test2@test.com";
	public static final String USD_ISOCODE = "USD";
	public static final String EUR_ISOCODE = "EUR";
	public static final String GBP_ISOCODE = "GBP";
	public static final Double USD_EXTERNAL_PRICE = 11.19;
	public static final Double USD_DISCOUNT = 1.0;
	public static final Double USD_TOTAL_PRICE = USD_EXTERNAL_PRICE - USD_DISCOUNT;
	public static final Double EUR_EXTERNAL_PRICE = 21.29;
	public static final Double EUR_DISCOUNT = 2.0;
	public static final Double EUR_TOTAL_PRICE = EUR_EXTERNAL_PRICE - EUR_DISCOUNT;
	public static final Double GBP_EXTERNAL_PRICE = 31.39;
	public static final Double GBP_DISCOUNT = 3.0;
	public static final Double GBP_TOTAL_PRICE = GBP_EXTERNAL_PRICE - GBP_DISCOUNT;
	private static final String AS_QUOTED_CARD_GUID = "test-usd-cart-as-quoted-guid";
	private static final String AS_FUNDED_CARD_GUID = "test-usd-cart-as-funded-guid";
	private static final String AS_COUPON_CART1_GUID = "test-usd-cart1-guid";
	private static final String AS_COUPON_CART2_GUID = "test-usd-cart2-guid";
	private static final String AS_GENERAL_CARD_GUID = "test-usd-cart-guid";
	private static final String AS_GENERAL_EMPTY_CARD_GUID = "test-empty-usd-cart-guid";
	private static final String AS_RESTRICTED_PRODUCTS_CART_GUID = "test-restricted-products-cart-guid";
	public static final String TEST_COUPON_NAME = "TEST";
	public static final String COUPON_NOT_VALID_STATUS_CODE = "couponNotValid";
	public static final String TEST_VARIANT_PRODUCT_1_NAME = "test sample variant product 1";
	private static final String CALCULATE_TAXES_PATH = "/es/v1/taxes/calculate";

	@Resource(name = "wileyasCartFacade")
	private WileyCartFacade wileyasCartFacade;
	@Resource(name = "wileyasCheckoutFacade")
	private WileyCheckoutFacade wileyasCheckoutFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private CartService cartService;

	@Resource
	private WileyCouponCodeGenerationServiceImpl wileyCouponCodeGenerationService;

	@Resource
	private WileyCouponService wileyCouponService;

	@Before
	public void setUp() throws Exception
	{
		importCsv(RESOURCE_PREFIX + "/common.impex", DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));

		super.setUp();
	}

	@Test
	public void restoreUsdSavedCartUsingExternalPrices() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.restoreSavedCart();

		// then
		CartModel sessionCart = cartService.getSessionCart();
		assertTrue(sessionCart.getCalculated());
		assertEquals(USD_ISOCODE, sessionCart.getCurrency().getIsocode());
		assertEquals(USD_TOTAL_PRICE, sessionCart.getTotalPrice());
	}

	@Test
	public void restoreUsdSavedCartUsingGuidAndExistingSessionCart() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart();
		assertTrue(cartService.hasSessionCart());
		final String oldSessionCartGuid = cartService.getSessionCart().getGuid();

		// when
		wileyasCartFacade.restoreSavedCart(AS_GENERAL_EMPTY_CARD_GUID);

		// then
		CartModel sessionCart = cartService.getSessionCart();
		assertNotNull(sessionCart);
		assertNotEquals(oldSessionCartGuid, sessionCart.getGuid());
	}

	@Test
	public void restoreUsdSavedCartUsingWrongGuid() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart();
		assertTrue(cartService.hasSessionCart());
		final String oldSessionCartGuid = cartService.getSessionCart().getGuid();

		// when
		wileyasCartFacade.restoreSavedCart("not a card guid");

		// then
		CartModel sessionCart = cartService.getSessionCart();
		assertNotNull(sessionCart);
		assertEquals(oldSessionCartGuid, sessionCart.getGuid());
	}

	@Test
	public void restoreEurSavedCartUsingExternalPrices() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-eur.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.restoreSavedCart();

		// then
		CartModel sessionCart = cartService.getSessionCart();
		assertTrue(sessionCart.getCalculated());
		assertEquals(EUR_ISOCODE, sessionCart.getCurrency().getIsocode());
		assertEquals(EUR_TOTAL_PRICE, sessionCart.getTotalPrice());
	}

	@Test
	public void restoreGbpSavedCartUsingExternalPrices() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-gbp.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.restoreSavedCart();

		// then
		CartModel sessionCart = cartService.getSessionCart();
		assertTrue(sessionCart.getCalculated());
		assertEquals(GBP_ISOCODE, sessionCart.getCurrency().getIsocode());
		assertEquals(GBP_TOTAL_PRICE, sessionCart.getTotalPrice());
	}

	@Test
	public void testGetSessionCartWithEntryOrderingShouldPopulateMandatoryField() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-mandatory-optional.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart();

		// when
		CartData cartData = wileyasCartFacade.getSessionCartWithEntryOrdering(true);

		// then
		assertNotNull(cartData.getEntries());
		assertEquals(2, cartData.getEntries().size());

		OrderEntryData optionalOrderEntryData = cartData.getEntries().get(0);
		assertEquals("test_vp_optional", optionalOrderEntryData.getProduct().getCode());
		assertFalse(optionalOrderEntryData.getProduct().getMandatory());

		OrderEntryData mandatoryOrderEntryData = cartData.getEntries().get(1);
		assertEquals("test_vp_mandatory", mandatoryOrderEntryData.getProduct().getCode());
		assertTrue(mandatoryOrderEntryData.getProduct().getMandatory());
	}

	@Test
	public void testIsDiscountsAvailableForCurrentCartGeneral() throws ImpExException, CommerceCartRestorationException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.restoreSavedCart();

		// then
		assertTrue(wileyasCartFacade.isDiscountsAvailableForCurrentCart());
	}

	@Test
	public void testIsDiscountsAvailableForCurrentCartAsQuoted() throws ImpExException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd-as-quoted.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.switchSessionCart(AS_QUOTED_CARD_GUID);

		// then
		assertFalse(wileyasCartFacade.isDiscountsAvailableForCurrentCart());
	}

	@Test
	public void testIsDiscountsAvailableForCurrentCartAsFunded() throws ImpExException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd-as-funded.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.switchSessionCart(AS_FUNDED_CARD_GUID);

		// then
		assertFalse(wileyasCartFacade.isDiscountsAvailableForCurrentCart());
	}

	@Test
	public void testIsRemoveButtonAvailableForCurrentCartGeneral() throws ImpExException, CommerceCartRestorationException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.restoreSavedCart();

		// then
		assertTrue(wileyasCartFacade.isRemoveButtonAvailableForCurrentCart());
	}

	@Test
	public void testIsRemoveButtonAvailableForCurrentCartAsQuoted() throws ImpExException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd-as-quoted.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.switchSessionCart(AS_QUOTED_CARD_GUID);

		// then
		assertFalse(wileyasCartFacade.isRemoveButtonAvailableForCurrentCart());
	}

	@Test
	public void testIsRemoveButtonAvailableForCurrentCartAsFunded() throws ImpExException
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-usd-as-funded.impex", DEFAULT_ENCODING);

		// when
		wileyasCartFacade.switchSessionCart(AS_FUNDED_CARD_GUID);

		// then
		assertFalse(wileyasCartFacade.isRemoveButtonAvailableForCurrentCart());
	}

	@Test
	public void testExternalTaxServiceIsCalledWhenQuantityUpdated() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-external-tax.impex", DEFAULT_ENCODING);
		wileyasCartFacade.switchSessionCart("test-external-tax-cart-guid");
		assertFalse(cartService.getSessionCart().getTaxCalculated());

		// when
		wileyasCartFacade.updateCartEntry(0, 0);

		// then
		assertTrue(cartService.getSessionCart().getTaxCalculated());
	}

	@Test
	public void testValidateFailsForCartWithRedeemedCouponCode() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd-coupon.impex", DEFAULT_ENCODING);

		MultiCodeCouponModel testMultiCodeCoupon = (MultiCodeCouponModel) wileyCouponService.getCouponForCode(TEST_COUPON_NAME)
				.get();
		String generateCouponCode = wileyCouponCodeGenerationService.generateCouponCode(testMultiCodeCoupon);

		// when
		// first user applies coupon code coupon code to cart
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER1_ID));
		wileyasCartFacade.switchSessionCart(AS_COUPON_CART1_GUID);
		wileyCouponService.redeemCoupon(generateCouponCode, cartService.getSessionCart());
		assertEquals(1, cartService.getSessionCart().getAppliedCouponCodes().size());
		assertEquals(generateCouponCode, cartService.getSessionCart().getAppliedCouponCodes().iterator().next());

		List<CartModificationData> cart1ModificationDataList = wileyasCartFacade.validateCartData();
		assertTrue(CollectionUtils.isEmpty(cart1ModificationDataList));
		// first user places order
		wileyasCheckoutFacade.placeOrder();

		// second user applies same coupon code coupon code to his cart
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER2_ID));
		wileyasCartFacade.switchSessionCart(AS_COUPON_CART2_GUID);
		cartService.getSessionCart().setAppliedCouponCodes(Lists.newArrayList(generateCouponCode));

		// second user validates cart
		List<CartModificationData> cart2ModificationDataList = wileyasCartFacade.validateCartData();

		// then
		// redeemed coupon code is removed from cart
		assertTrue(CollectionUtils.isEmpty(cartService.getSessionCart().getAppliedCouponCodes()));
		// cart modification list has failed modification with coupon related message
		assertTrue(CollectionUtils.isNotEmpty(cart2ModificationDataList));
		assertEquals(1, cart2ModificationDataList.size());
		final CartModificationData commerceCartModification = cart2ModificationDataList.get(0);
		assertEquals(commerceCartModification.getStatusCode(), COUPON_NOT_VALID_STATUS_CODE);
	}

	@Test
	public void testValidateFailsForCartWithProductsRestrictedByOnlineOfflineDates() throws Exception
	{
		// given
		assertFalse(cartService.hasSessionCart());
		importCsv(RESOURCE_PREFIX + "/cart-restricted-products.impex", DEFAULT_ENCODING);
		wileyasCartFacade.switchSessionCart(AS_RESTRICTED_PRODUCTS_CART_GUID);

		// when
		List<CartModificationData> cartModificationDataList = wileyasCartFacade.validateCartData();

		// then
		assertTrue(CollectionUtils.isNotEmpty(cartModificationDataList));
		assertEquals(1, cartModificationDataList.size());
		final CartModificationData cartModificationData = cartModificationDataList.get(0);
		assertEquals(cartModificationData.getStatusCode(), CommerceCartModificationStatus.UNAVAILABLE);
		assertEquals(cartModificationData.getStatusMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(cartModificationData.getMessageParameters()[0], TEST_VARIANT_PRODUCT_1_NAME);
		assertTrue(CollectionUtils.isEmpty(cartService.getSessionCart().getEntries()));
	}

	@Test
	public void testHasSessionCartWithRestoredCartGuid() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart(AS_GENERAL_CARD_GUID);

		// when
		boolean result = wileyasCartFacade.hasSessionCartWithGuid(AS_GENERAL_CARD_GUID);

		// then
		assertTrue(result);
	}

	@Test
	public void testDoesNotHaveSessionCartWithOtherGuid() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart(AS_GENERAL_CARD_GUID);

		// when
		boolean result = wileyasCartFacade.hasSessionCartWithGuid(AS_GENERAL_EMPTY_CARD_GUID);

		// then
		assertFalse(result);
	}

	@Test
	public void testDoesNotHaveSessionCartWithEmptyGuid() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "/cart-usd.impex", DEFAULT_ENCODING);
		wileyasCartFacade.restoreSavedCart(AS_GENERAL_CARD_GUID);

		// when
		boolean result = wileyasCartFacade.hasSessionCartWithGuid(StringUtils.EMPTY);

		// then
		assertFalse(result);
	}
}
