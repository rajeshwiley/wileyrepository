package com.wiley.core.integration.esb;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.annotation.Resource;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.http.HttpStatus;
import org.apache.poi.ss.formula.functions.T;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.netflix.config.ConfigurationManager;
import com.wiley.core.common.WileyAbstractOAuth2StoreAwareChainTest;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.core.order.data.CartModificationMessageType;

import static java.util.Arrays.asList;


/**
 * The test contains cases for external cart calculation.<br/>
 * It tests {@link EsbCartCalculationGateway#verifyAndCalculateCart(CartModel)}.
 */
@IntegrationTest
public class EsbCartCalculationGatewayCartCalculationIntegrationTest extends WileyAbstractOAuth2StoreAwareChainTest
{

	private static final Logger LOG = LoggerFactory.getLogger(EsbCartCalculationGatewayCartCalculationIntegrationTest.class);

	private static final String RESOURCE_PREFIX =
			"/wileytest/integration/EsbCartCalculationGatewayCartCalculationIntegrationTest";

	private static final String CONFIG_CIRCUIT_BREAKER_ENABLED =
			"hystrix.command.externalCartCalculation.circuitBreaker.enabled";

	private static final String EXTERNAL_CART_CALCULATION_URL = "/b2b/accounts/sapAccount1/carts/test-cart";
	private static final RequestMethod EXTERNAL_CART_CALCULATION_REQUEST_METHOD = RequestMethod.PUT;

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

	private static final List<String> DISABLED_SITES = asList("ags", "wel", "wileyb2c");
	private static final List<String> ENABLED_SITES = asList("wileyb2b");

	@Resource
	private EsbCartCalculationGateway realEsbCartCalculationGateway;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private RestTemplate sapErpOAuth2RestTemplate;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		super.setUp();

		userService.setCurrentUser(userService.getAdminUser()); // needed to import impex files
		baseSiteService.setCurrentBaseSite("wileyb2b", true);

		importCsv(RESOURCE_PREFIX + "/impex/essentialdata.impex", DEFAULT_ENCODING);
		importCsv(RESOURCE_PREFIX + "/impex/cartdata.impex", DEFAULT_ENCODING);

		wireMock.addMockServiceRequestListener((request, response) -> {
			LOG.debug("WireMock Request: {}", request);
			LOG.debug("WireMock Response: {}", response);
		});

		userService.setCurrentUser(userService.getAnonymousUser()); // reset current user to anonymous
	}

	@Test
	public void shouldCalculateCartSuccessfully() throws Exception
	{
		// --------- Given ---------
		// load mapping
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_Success.json",
				RESOURCE_PREFIX + "/response/response_b2b_cartCalculation_Success.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		final List<ExternalCartModification> externalCartModifications = realEsbCartCalculationGateway.verifyAndCalculateCart(
				cart);

		// ---------- Then ----------
		assertNotNull(externalCartModifications);
		checkIfCartAndCartEntriesAreModified(cart);

		// checking state of the cart
		checkStateOfCart(cart,
				584.25, // subtotal
				100.55, // total discounts
				40.55, // delivery cost
				10.55, // total tax
				534.8, // total price
				2); // entries count

		assertNotNull("Cart.globalDiscountValues cannot be null.", cart.getGlobalDiscountValues());
		assertEquals("Cart.globalDiscountValues.size", 2, cart.getGlobalDiscountValues().size());
		checkCartDiscount(cart,
				"SOME_ABSOLUTE_DISCOUNT", // code
				true, // absolute
				20.35 // value
		);

		checkCartDiscount(cart,
				"SOME_RELATIVE_DISCOUNT", // code
				false, // absolute
				35.35 // value
		);

		// checking state of the cart entries
		final AbstractOrderEntryModel cartEntry0 = cart.getEntries().get(0);
		checkStateOfCartEntry(cartEntry0,
				"WCOM_PRODUCT", // product code
				55L, // quantity
				10.55, // base price
				580.25);  // total price

		final AbstractOrderEntryModel cartEntry1 = cart.getEntries().get(1);
		checkStateOfCartEntry(cartEntry1,
				"WCOM_PRODUCT2", // product code
				1L,  // quantity
				4.0,  // base price
				4.0);  // total price

		// checking external cart modification messages
		assertEquals("ExternalCartModifications.size", 2, externalCartModifications.size());

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT", // product code
				0,  // entry number
				55L, // quantity
				"success", // event code
				"Test confirmation message.", // message
				CartModificationMessageType.CONF, // message type
				"updated"); // status code

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT2", // product code
				1,  // entry number
				1L, // quantity
				"success", // event code
				"Test information message.", // message
				CartModificationMessageType.INFO, // message type
				"updated"); // status code

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.INSTOCK, // status
				40L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.BACK_ORDER, // status
				6L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRE_ORDER, // status
				5L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRINT_ON_DEMAND, // status
				4L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry1,
				StockLevelStatus.INSTOCK, // status
				1L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkCartEntryDiscounts(cartEntry0);

		checkCartEntryDiscounts(cartEntry1);
	}

	private void checkCartEntryDiscounts(final AbstractOrderEntryModel cartEntry1)
	{
		assertNotNull("CartEntry.discountValues cannot be null.", cartEntry1.getDiscountValues());
		assertEquals("CartEntry.discountValues.size", 2, cartEntry1.getDiscountValues().size());
		checkCartEntryDiscount(cartEntry1,
				"SOME_ABSOLUTE_DISCOUNT", // code
				true, // absolute
				10.35 // value
		);

		checkCartEntryDiscount(cartEntry1,
				"SOME_RELATIVE_DISCOUNT", // code
				false, // absolute
				20.0 // value
		);
	}


	@Test
	public void shouldAddNewEntryDuringCalculation() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = setUpStubMapping(RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_Success.json",
				RESOURCE_PREFIX + "/response/response_b2b_cartCalculation_AddedNewEntry_Success.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		final List<ExternalCartModification> externalCartModifications = realEsbCartCalculationGateway.verifyAndCalculateCart(
				cart);

		// ---------- Then ----------
		assertNotNull(externalCartModifications);

		checkIfCartAndCartEntriesAreModified(cart);

		// checking state of the cart
		checkStateOfCart(cart,
				1144.25, // subtotal
				100.55, // total discounts
				40.55, // delivery cost
				10.55, // total tax
				1094.8, // total price
				3); // entries count

		assertNotNull("Cart.globalDiscountValues cannot be null.", cart.getGlobalDiscountValues());
		assertEquals("Cart.globalDiscountValues.size", 2, cart.getGlobalDiscountValues().size());
		checkCartDiscount(cart,
				"SOME_ABSOLUTE_DISCOUNT", // code
				true, // absolute
				20.35 // value
		);

		checkCartDiscount(cart,
				"SOME_RELATIVE_DISCOUNT", // code
				false, // absolute
				35.0 // value
		);

		// checking state of the cart entries
		final AbstractOrderEntryModel cartEntry0 = cart.getEntries().get(0);
		checkStateOfCartEntry(cartEntry0,
				"WCOM_PRODUCT", // product code
				55L, // quantity
				10.55, // base price
				580.25);  // total price

		final AbstractOrderEntryModel cartEntry1 = cart.getEntries().get(1);
		checkStateOfCartEntry(cartEntry1,
				"WCOM_PRODUCT2", // product code
				1L,  // quantity
				(double) 4,  // base price
				(double) 4);  // total price

		final AbstractOrderEntryModel cartEntry2 = cart.getEntries().get(2);
		checkStateOfCartEntry(cartEntry2,
				"WCOM_PRODUCT3", // product code
				7L,  // quantity
				80.0,  // base price
				560.0);  // total price

		// checking external cart modification messages
		assertEquals("ExternalCartModifications.size", 3, externalCartModifications.size());

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT", // product code
				0,  // entry number
				55L, // quantity
				null, // event code
				null, // message
				null, // message type
				"updated"); // status code

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT2", // product code
				1,  // entry number
				1L, // quantity
				null, // event code
				null, // message
				null, // message type
				"updated"); // status code

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT3", // product code
				2,  // entry number
				7L, // quantity
				"addedProduct", // event code
				"Test confirmation message.", // message
				CartModificationMessageType.CONF, // message type
				"new"); // status code

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.INSTOCK, // status
				40L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.BACK_ORDER, // status
				6L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRE_ORDER, // status
				5L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRINT_ON_DEMAND, // status
				4L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry1,
				StockLevelStatus.INSTOCK, // status
				1L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry2,
				StockLevelStatus.INSTOCK, // status
				2L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry2,
				StockLevelStatus.BACK_ORDER, // status
				3L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry2,
				StockLevelStatus.PRE_ORDER, // status
				2L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry2,
				StockLevelStatus.PRINT_ON_DEMAND, // status
				1L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);
		assertTrue("CartEntry.discountValues should be empty.", cartEntry0.getDiscountValues().isEmpty());
		assertTrue("CartEntry.discountValues should be empty.", cartEntry1.getDiscountValues().isEmpty());
	}

	@Test
	public void shouldUpdateCartEntryQuantityDuringCalculation() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_Success.json",
				RESOURCE_PREFIX + "/response/response_b2b_cartCalculation_UpdateCartEntryQuantity_Success.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		final List<ExternalCartModification> externalCartModifications = realEsbCartCalculationGateway.verifyAndCalculateCart(
				cart);


		// ---------- Then ----------
		assertNotNull(externalCartModifications);

		checkIfCartAndCartEntriesAreModified(cart);

		// checking state of the cart
		checkStateOfCart(cart,
				109.55, // subtotal
				9.5, // total discounts
				10.5, // delivery cost
				10.5, // total tax
				121.05, // total price
				2); // entries count

		assertTrue("Cart.globalDiscountValues should be empty.", cart.getGlobalDiscountValues().isEmpty());

		// checking state of the cart entries
		final AbstractOrderEntryModel cartEntry0 = cart.getEntries().get(0);
		checkStateOfCartEntry(cartEntry0,
				"WCOM_PRODUCT", // product code
				10L, // quantity
				10.55, // base price
				105.5);  // total price

		final AbstractOrderEntryModel cartEntry1 = cart.getEntries().get(1);
		checkStateOfCartEntry(cartEntry1,
				"WCOM_PRODUCT2", // product code
				1L,  // quantity
				4.0,  // base price
				4.0);  // total price

		// checking external cart modification messages
		assertEquals("ExternalCartModifications.size", 2, externalCartModifications.size());

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT", // product code
				0,  // entry number
				10L, // quantity
				"productUpdate", // event code
				"Test error message.", // message
				CartModificationMessageType.ERROR, // message type
				"updated"); // status code

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT2", // product code
				1,  // entry number
				1L, // quantity
				null, // event code
				null, // message
				null, // message type
				"updated"); // status code

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.INSTOCK, // status
				4L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.BACK_ORDER, // status
				3L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRE_ORDER, // status
				2L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRINT_ON_DEMAND, // status
				1L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry1,
				StockLevelStatus.INSTOCK, // status
				1L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkCartEntryDiscounts(cartEntry0);

		assertTrue("CartEntry.discountValues should be empty.", cartEntry1.getDiscountValues().isEmpty());
	}

	@Test
	public void shouldRemoveCartEntryDuringCalculationCase() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = setUpStubMapping(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_Success.json",
				RESOURCE_PREFIX + "/response/response_b2b_cartCalculation_RemoveCartEntry_Success.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		final List<ExternalCartModification> externalCartModifications = realEsbCartCalculationGateway.verifyAndCalculateCart(
				cart);

		// ---------- Then ----------
		assertNotNull(externalCartModifications);

		checkIfCartAndCartEntriesAreModified(cart);

		// checking state of the cart
		checkStateOfCart(cart,
				580.25, // subtotal
				9.5, // total discounts
				10.5, // delivery cost
				10.5, // total tax
				591.75, // total price
				1); // entries count

		assertTrue("Cart.globalDiscountValues should be empty.", cart.getGlobalDiscountValues().isEmpty());

		// checking state of the cart entries
		final AbstractOrderEntryModel cartEntry0 = cart.getEntries().get(0);
		checkStateOfCartEntry(cartEntry0,
				"WCOM_PRODUCT", // product code
				55L, // quantity
				10.55, // base price
				580.25);  // total price

		// checking external cart modification messages
		assertEquals("ExternalCartModifications.size", 2, externalCartModifications.size());

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT", // product code
				0,  // entry number
				55L, // quantity
				null, // event code
				null, // message
				null, // message type
				"updated"); // status code

		checkExternalCartModification(externalCartModifications,
				"WCOM_PRODUCT2", // product code
				1,  // entry number
				0L, // quantity
				"outOfStock", // event code
				"Test error message.", // message
				CartModificationMessageType.ERROR, // message type
				"deleted"); // status code

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.INSTOCK, // status
				40L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.BACK_ORDER, // status
				6L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRE_ORDER, // status
				5L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		checkInventoryInformation(cartEntry0,
				StockLevelStatus.PRINT_ON_DEMAND, // status
				4L, // quantity
				getDate(DATE_TIME_FORMATTER, "2001-07-04T12:08:56.235-07:00") // estimatedDeliveryDate
		);

		assertTrue("CartEntry.discountValues should be empty.", cartEntry0.getDiscountValues().isEmpty());
	}

	@Test
	public void verify400Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_400.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
			fail("Expected ExternalSystemBadRequestException");
		}
		catch (ExternalSystemBadRequestException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify401Response() throws Exception
	{
		// --------- Given ---------
		setupWireMockPutErrorResponse("/b2b/accounts/sapAccount1/carts/test-cart", HttpStatus.SC_UNAUTHORIZED);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
		// Then expect exception
	}

	@Test(expected = ExternalSystemInternalErrorException.class)
	public void verify403Response() throws Exception
	{
		// --------- Given ---------
		setupWireMockPutErrorResponse("/b2b/accounts/sapAccount1/carts/test-cart", HttpStatus.SC_FORBIDDEN);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
		// Then expect exception
	}

	@Test
	public void verify404Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_404.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
			fail("Expected ExternalSystemInternalErrorException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	public void verify500Response() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_500.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		// ---------- When ---------
		try
		{
			realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
			fail("Expected ExternalSystemInternalErrorException");
		}
		catch (ExternalSystemInternalErrorException e)
		{
			// ---------- Then ----------
			// success
		}
	}

	@Test
	@Ignore // Ignored because some time a circuit breaker is not opened if exception occurs.
	public void verifyCircuitBreakerIntegration() throws Exception
	{
		// --------- Given ---------
		final StubMapping stubMapping = createStubMappingFrom(
				RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_500.json"
		);
		wireMock.addStubMapping(stubMapping);

		final UserModel user = setUpUser("test");
		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

		enableCircuitBreaker();

		try
		{
			// ---------- When ---------
			try
			{
				realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
				fail("Expected ExternalSystemInternalErrorException");
			}
			catch (ExternalSystemInternalErrorException e)
			{
				// success
			}

			try
			{
				realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
				fail("Expected ExternalSystemInternalErrorException");
			}
			catch (ExternalSystemInternalErrorException e)
			{
				// success
			}

			// ---------- Then ----------
			RequestPatternBuilder externalCartCalculationRequest = getRequestPatternBuilder(EXTERNAL_CART_CALCULATION_URL,
					EXTERNAL_CART_CALCULATION_REQUEST_METHOD);
			wireMock.verify(1, externalCartCalculationRequest);
		}
		finally
		{
			disableCircuitBreaker();
		}
	}

	private void checkStateOfCart(final CartModel cart, final Double expectedCartSubtotal,
			final Double expectedCartTotalDiscounts, final Double expectedCartDeliveryCost, final Double expectedCartTotalTax,
			final Double expectedCartTotalPrice, final int entriesCount)
	{
		assertEquals("Cart.subtotal", expectedCartSubtotal, cart.getSubtotal());
		assertEquals("Cart.totalDiscounts", expectedCartTotalDiscounts, cart.getTotalDiscounts());
		assertEquals("Cart.deliveryCost", expectedCartDeliveryCost, cart.getDeliveryCost());
		assertEquals("Cart.totalTax", expectedCartTotalTax, cart.getTotalTax());

		Boolean isTaxCalculated = expectedCartTotalTax != null;
		assertEquals("Cart.taxCalculated", isTaxCalculated, cart.getTaxCalculated());

		assertEquals("Cart.totalTaxValues", expectedCartTotalTax, sumTotalTaxValues(cart));
		assertEquals("Cart.totalPrice", expectedCartTotalPrice, cart.getTotalPrice());
		assertEquals("Cart.entries count", entriesCount, cart.getEntries().size());
	}

	private void checkStateOfCartEntry(final AbstractOrderEntryModel entryModel, final String productCode,
			final Long quantity, final Double basePrice, final Double totalPrice)
	{
		assertEquals("CartEntry.product.code", productCode, entryModel.getProduct().getCode());
		assertEquals("CartEntry.quantity", quantity, entryModel.getQuantity());
		assertEquals("CartEntry.basePrice", basePrice, entryModel.getBasePrice());
		assertEquals("CartEntry.totalPrice", totalPrice, entryModel.getTotalPrice());
	}

	private void checkExternalCartModification(final List<ExternalCartModification> externalCartModifications,
			final String productCode, final Integer entryNumber, final Long quantity,
			final String eventCode, final String message, final CartModificationMessageType messageType, final String statusCode)
	{
		// find cart modification for product
		final Optional<ExternalCartModification> expectedProductModification = externalCartModifications.stream()
				.filter(modification -> productCode.equals(modification.getProductCode()))
				.findFirst();

		assertTrue("External cart modification exists", expectedProductModification.isPresent());

		// checking state of the ExternalCartModification
		final ExternalCartModification modification = expectedProductModification.get();
		assertEquals("ExternalCartModification.productCode", productCode, modification.getProductCode());
		assertEquals("ExternalCartModification.entryNumber", entryNumber, modification.getEntryNumber());
		assertEquals("ExternalCartModification.quantity", quantity, modification.getQuantity());
		assertEquals("ExternalCartModification.eventCode", eventCode, modification.getEventCode());
		assertEquals("ExternalCartModification.message", message, modification.getMessage());
		assertEquals("ExternalCartModification.messageType", messageType, modification.getMessageType());
		assertEquals("ExternalCartModification.statusCode", statusCode, modification.getStatusCode());
	}

	private void checkInventoryInformation(final AbstractOrderEntryModel abstractOrderEntryModel,
			final StockLevelStatus stockLevelStatus, final Long quantity, final Date estimatedDeliveryDate)
	{
		final Set<ExternalInventoryModel> externalInventories = abstractOrderEntryModel.getExternalInventories();

		final ExternalInventoryModel externalInventory = externalInventories.stream()
				.filter(inventory -> stockLevelStatus.equals(inventory.getStatus()))
				.findFirst()
				.orElse(null);

		assertNotNull(String.format("Inventory with status [%s] should not be null", stockLevelStatus), externalInventory);
		assertFalse("ExternalInventory should not be new.", modelService.isNew(externalInventory));
		assertFalse("ExternalInventory should not be modified.", modelService.isModified(externalInventory));
		assertEquals("ExternalInventory.quantity", quantity, externalInventory.getQuantity());
		assertEquals("ExternalInventory.status", stockLevelStatus, externalInventory.getStatus());
		assertEquals("ExternalInventory.estimatedDeliveryDate", estimatedDeliveryDate,
				externalInventory.getEstimatedDeliveryDate());
	}

	private void checkCartEntryDiscount(final AbstractOrderEntryModel cartEntry, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = cartEntry.getDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode, discountValues);

		assertTrue(String.format("DiscountValue with code [%s] should be in cart entry.", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private void checkCartDiscount(final CartModel cartModel, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = cartModel.getGlobalDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode,
				discountValues);

		assertTrue(String.format("DiscountValue with code [%s] should be in cart", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private Optional<DiscountValue> findDiscountValueByCode(final String discountCode, final List<DiscountValue> discountValues)
	{
		return discountValues.stream()
				.filter(discountValue -> discountCode.equals(discountValue.getCode()))
				.findFirst();
	}

	private void checkDiscountValueState(final boolean absolute, final Double value, final DiscountValue discountValue)
	{
		assertEquals("DiscountValue.absolute", absolute, discountValue.isAbsolute());
		assertEquals("DiscountValue.value", value, Double.valueOf(discountValue.getValue()));
	}

	private Double sumTotalTaxValues(final CartModel cart)
	{
		return cart.getTotalTaxValues().stream()
				.mapToDouble(TaxValue::getAppliedValue)
				.sum();
	}

	private void checkIfCartAndCartEntriesAreModified(final CartModel cart)
	{
		assertTrue("Cart should be modified.", modelService.isModified(cart));

		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		for (AbstractOrderEntryModel entry : entries)
		{
			assertTrue("CartEntry should be modified.", modelService.isModified(entry));
		}
	}

	private void enableCircuitBreaker()
	{
		AbstractConfiguration configuration = ConfigurationManager.getConfigInstance();
		configuration.setProperty(CONFIG_CIRCUIT_BREAKER_ENABLED, Boolean.TRUE.toString());
	}

	private void disableCircuitBreaker()
	{
		AbstractConfiguration configuration = ConfigurationManager.getConfigInstance();
		configuration.setProperty(CONFIG_CIRCUIT_BREAKER_ENABLED, Boolean.FALSE.toString());
	}

	private Date getDate(final DateTimeFormatter dateTimeFormatter, final String dateTimeString)
	{
		return Date.from(ZonedDateTime.parse(dateTimeString, dateTimeFormatter).toInstant());
	}

	@Override
	public HttpComponentsClientHttpRequestFactory getFactoryToAdjust()
	{
		return (HttpComponentsClientHttpRequestFactory) sapErpOAuth2RestTemplate.getRequestFactory();
	}

	@Override
	protected List<String> getDisabledSites()
	{
		return DISABLED_SITES;
	}

	@Override
	protected List<String> getEnabledSites()
	{
		return ENABLED_SITES;
	}

	@Override
	protected Map<String, Callable<T>> getChainFunctions(final String site) throws Exception
	{
		baseSiteService.setCurrentBaseSite(site, true);
		Map<String, Callable<T>> funcMap = new HashMap<>();
		funcMap.put("verifyAndCalculateCart", new Callable<T>()
		{
			@Override
			public T call() throws Exception
			{
				final StubMapping stubMapping = setUpStubMapping(
						RESOURCE_PREFIX + "/mappings/b2b_cartCalculation_Success.json",
						RESOURCE_PREFIX + "/response/response_b2b_cartCalculation_Success.json"
				);
				wireMock.addStubMapping(stubMapping);

				final UserModel user = setUpUser("test");
				final CartModel cart = commerceCartService.getCartForCodeAndUser("test-cart", user);

				realEsbCartCalculationGateway.verifyAndCalculateCart(cart);
				return null;
			}
		});
		return funcMap;
	}
}