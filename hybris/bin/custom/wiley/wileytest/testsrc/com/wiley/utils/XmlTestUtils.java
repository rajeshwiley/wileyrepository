package com.wiley.utils;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Utils for work with xml in tests
 *
 * @author Dzmitryi_Halahayeu
 */
public final class XmlTestUtils
{
	private XmlTestUtils()
	{
	}

	public static Node getNodeByXPath(final Document doc, final String path) throws XPathExpressionException
	{
		NodeList nodeList = getNodeListByXPath(doc, path);
		if (nodeList.getLength() != 1)
		{
			return null;
		}
		return nodeList.item(0);
	}

	public static NodeList getNodeListByXPath(final Document doc, final String path) throws XPathExpressionException
	{
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xpath.compile(
				path);
		return (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
	}

	public static Double getDoubleValueByXpath(final Document doc, final String path) throws XPathExpressionException
	{
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = xpath.compile(
				path);
		return (Double) expr.evaluate(doc, XPathConstants.NUMBER);
	}


}
