<div style="text-align: center">
    <h2>Author Services Stub Login Page</h2>
    <br>
    <label>Session Token:</label>
    <input type="text" id="almSessionId" value="alm_id1">
    <br>
    <br>
    <label>URL:</label>
    <input size="100" type="text" id="url">
    <br/>
    <br/>
    <button onclick="addCookieAndRedirect()">Login</button>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
    $(document).ready(function () {
        var url;
        if (window.location.href.split("?")[1]) {
            var searchParams = new URLSearchParams(window.location.href.split("?")[1]);
            url = searchParams.get('returnUrl');
        } else {
            url = window.location.protocol + "//"
                + window.location.hostname + "/authorservices";
        }
        var hostname = window.location.hostname;
        if (hostname === "localhost" || hostname === "127.0.0.1" || hostname === "wiley.local") {
            url = url.replace(hostname, hostname + ":" + window.location.port);
        }
        document.getElementById("url").value = url;

        $(document).keypress(function (e) {
            if (e.keyCode === 13) {
                addCookieAndRedirect();
            }
        });
    });

    function addCookieAndRedirect() {
        var domain;
        if (window.location.href.indexOf("wiley.com") !== -1) {
            domain = "wiley.com";
        }
        var url = $('#url').val();
        var almSessionId = $('#almSessionId').val();
        document.cookie = "almSessionId=" + almSessionId + ";path=/" + (domain ? "; domain="  + domain : "");
        window.location = url;
    }
</script>