package com.wiley.wileyb2b.storefront.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link EmptyCartRedirectingHandlerInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmptyCartRedirectingHandlerInterceptorUnitTest
{

	@Mock
	private Wileyb2bCartFacade wileyb2bCartFacadeMock;

	@InjectMocks
	private EmptyCartRedirectingHandlerInterceptor emptyCartRedirectingHandlerInterceptor;

	// Test data

	@Mock
	private HttpServletRequest httpRequestMock;

	@Mock
	private HttpServletResponse httpResponseMock;

	@Mock
	private Object handlerMock;

	private ModelAndView modelAndViewStub;

	private static final String REDIRECT_URL = "/someRedirectUrl";

	private static final String MODEL_VIEW = "someView";

	@Before
	public void setUp() throws Exception
	{
		modelAndViewStub = new ModelAndView(MODEL_VIEW);

		emptyCartRedirectingHandlerInterceptor.setRedirectUrl(REDIRECT_URL);

	}

	@Test
	public void shouldRedirectIfEmptyCart() throws Exception
	{
		// Given
		when(wileyb2bCartFacadeMock.hasEntries()).thenReturn(false);

		// When
		emptyCartRedirectingHandlerInterceptor.postHandle(httpRequestMock, httpResponseMock, handlerMock, modelAndViewStub);

		// Then
		assertEquals("Expected redirecting.", AbstractController.REDIRECT_PREFIX + REDIRECT_URL, modelAndViewStub.getViewName());
	}

	@Test
	public void shouldNotRedirectIfCartIsNotEmpty() throws Exception
	{
		// Given
		when(wileyb2bCartFacadeMock.hasEntries()).thenReturn(true);

		// When
		emptyCartRedirectingHandlerInterceptor.postHandle(httpRequestMock, httpResponseMock, handlerMock, modelAndViewStub);

		// Then
		assertEquals("Redirecting is not expected.", MODEL_VIEW, modelAndViewStub.getViewName());
	}

	@Test
	public void shouldNotRedirectIfAjaxCall() throws Exception
	{
		// Given
		modelAndViewStub.setViewName(null);
		when(wileyb2bCartFacadeMock.hasEntries()).thenReturn(false);

		// When
		emptyCartRedirectingHandlerInterceptor.postHandle(httpRequestMock, httpResponseMock, handlerMock, modelAndViewStub);

		// Then
		assertEquals("Redirecting is not expected.", null, modelAndViewStub.getViewName());
	}

	@Test
	public void shouldNotRedirectIfIncludeRequest() throws Exception
	{
		// Given
		when(httpRequestMock.getAttribute("javax.servlet.include.request_uri")).thenReturn("someUri");
		when(wileyb2bCartFacadeMock.hasEntries()).thenReturn(false);

		// When
		emptyCartRedirectingHandlerInterceptor.postHandle(httpRequestMock, httpResponseMock, handlerMock, modelAndViewStub);

		// Then
		assertEquals("Redirecting is not expected.", MODEL_VIEW, modelAndViewStub.getViewName());
	}

	@Test
	public void shouldNotThrowExceptionIfModelIsNull() throws Exception
	{
		// Given
		modelAndViewStub = null;
		when(wileyb2bCartFacadeMock.hasEntries()).thenReturn(false);

		// When
		emptyCartRedirectingHandlerInterceptor.postHandle(httpRequestMock, httpResponseMock, handlerMock, modelAndViewStub);

		// Then
		// do nothing. No exceptions
	}

}