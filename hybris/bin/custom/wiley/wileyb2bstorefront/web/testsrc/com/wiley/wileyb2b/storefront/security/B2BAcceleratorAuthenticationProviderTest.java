package com.wiley.wileyb2b.storefront.security;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class B2BAcceleratorAuthenticationProviderTest
{
	private static final String USERNAME1 = "USERNAME1";
	private static final String USERNAME2 = "USERNAME2";
	private static final String ANONYMOUS = "ANONYMOUS";
	private static final String PRINCIPAL = "PRINCIPAL";

	@InjectMocks
	@Spy
	private B2BAcceleratorAuthenticationProvider b2BAcceleratorAuthenticationProvider;
	@Mock
	private BruteForceAttackCounter bruteForceAttackCounterMock;
	@Mock
	private Authentication authenticationMock;
	@Mock
	private CartService cartServiceMock;
	@Mock
	private CartModel cartMock;
	@Mock
	private CustomerModel sessionUserMock;
	@Mock
	private CustomerModel anonymousUserMock;
	@Mock
	private B2BCustomerModel b2BCustomerMock;
	@Mock
	private UserService userServiceMock;
	@Mock
	private B2BUnitModel b2BUnitMock;

	@Before
	public void setUp() throws Exception
	{
		when(authenticationMock.getName()).thenReturn(USERNAME1);
		when(authenticationMock.getPrincipal()).thenReturn(PRINCIPAL);

		when(bruteForceAttackCounterMock.isAttack(USERNAME1)).thenReturn(false);

		when(anonymousUserMock.getUid()).thenReturn(ANONYMOUS);
		when(userServiceMock.getAnonymousUser()).thenReturn(anonymousUserMock);
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
		when(b2BCustomerMock.getDefaultB2BUnit()).thenReturn(b2BUnitMock);
		when(cartMock.getUser()).thenReturn(sessionUserMock);
		doReturn(authenticationMock).when(b2BAcceleratorAuthenticationProvider).doAuthentication(authenticationMock);
	}

	@Test
	public void testAuthenticationOfAnonymousCustomer() throws Exception
	{
		//given
		when(sessionUserMock.getUid()).thenReturn(ANONYMOUS);
		when(userServiceMock.getUserForUID(USERNAME1)).thenReturn(b2BCustomerMock);

		//when
		b2BAcceleratorAuthenticationProvider.authenticate(authenticationMock);

		//then
		verify(cartMock).setUnit(b2BUnitMock);
		verify(cartServiceMock).setSessionCart(cartMock);
	}

	@Test
	public void testAuthenticationOfOtherCustomer() throws Exception
	{
		//given
		when(sessionUserMock.getUid()).thenReturn(USERNAME2);

		//when
		b2BAcceleratorAuthenticationProvider.authenticate(authenticationMock);

		//then
		verify(cartServiceMock).setSessionCart(null);
	}

	@Test
	public void testAuthenticationOfSameCustomer() throws Exception
	{
		//given
		when(sessionUserMock.getUid()).thenReturn(USERNAME1);

		//when
		b2BAcceleratorAuthenticationProvider.authenticate(authenticationMock);

		//then
		verify(cartServiceMock, never()).setSessionCart(any());
	}
}