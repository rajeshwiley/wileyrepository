package com.wiley.wileyb2b.storefront.checkout.steps.validation.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.Wileyb2bCheckoutFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultB2BPaymentTypeCheckoutStepValidatorTest
{

	private static final String ERROR_MESSAGES_HOLDER = GlobalMessages.ERROR_MESSAGES_HOLDER;

	@Mock
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacadeMock;

	@Mock
	private HttpServletRequest requestMock;

	@InjectMocks
	private DefaultB2BPaymentTypeCheckoutStepValidator validator = Mockito.spy(new DefaultB2BPaymentTypeCheckoutStepValidator());

	@Mock
	private RedirectAttributes redirectAttributesMock;
	@Mock
	private Map inputFlashMapMock;
	@Mock
	private Object errorMapMock;
	@Mock
	private Map outputFlashMapMock;
	@Mock
	private CartData cartDataMock;
	@Mock
	private B2BPaymentTypeData b2BPaymentTypeDataMock;
	@Mock
	private AddressData deliveryAddressMock;


	@Test
	public void shouldPreserveErrorMessages()
	{
		//given
		doReturn(false).when(wileyb2bCheckoutFacadeMock).setDefaultPaymentInfoForCheckout();
		when(validator.getInputFlashMap()).thenReturn(inputFlashMapMock);
		when(inputFlashMapMock.get(ERROR_MESSAGES_HOLDER)).thenReturn(errorMapMock);
		doReturn(outputFlashMapMock).when(validator).getOutputFlashMap();

		//testing
		validator.doValidateOnEnter(redirectAttributesMock);

		//verify
		verify(outputFlashMapMock).put(ERROR_MESSAGES_HOLDER, errorMapMock);
	}

	@Test
	public void shouldRedirectToDeliveryMethod()
	{
		//given
		doReturn(cartDataMock).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();
		doReturn(b2BPaymentTypeDataMock).when(cartDataMock).getPaymentType();
		doReturn(1L).when(cartDataMock).getDeliveryItemsQuantity();
		doReturn(CheckoutPaymentType.ACCOUNT.toString()).when(b2BPaymentTypeDataMock).getCode();
		doReturn(Arrays.asList(deliveryAddressMock)).when(wileyb2bCheckoutFacadeMock).getSupportedDeliveryAddresses(true);
		doReturn(true).when(wileyb2bCheckoutFacadeMock).setDeliveryAddress(any(AddressData.class));

		//testing
		ValidationResults result = validator.validateOnExit();

		//verify
		assertEquals(ValidationResults.REDIRECT_TO_DELIVERY_METHOD, result);
	}

	@Test
	public void shouldNotRedirectToDeliveryMethodWhenTwoDeliveryAddresses()
	{
		//given
		doReturn(cartDataMock).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();
		doReturn(b2BPaymentTypeDataMock).when(cartDataMock).getPaymentType();
		doReturn(CheckoutPaymentType.ACCOUNT.toString()).when(b2BPaymentTypeDataMock).getCode();
		doReturn(Arrays.asList(deliveryAddressMock, deliveryAddressMock)).when(wileyb2bCheckoutFacadeMock)
				.getSupportedDeliveryAddresses(true);

		//testing
		ValidationResults result = validator.validateOnExit();

		//verify
		assertEquals(ValidationResults.SUCCESS, result);
	}

	@Test
	public void shouldNotRedirectToDeliveryMethodWhenCardPaymentType()
	{
		//given
		doReturn(cartDataMock).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();
		doReturn(b2BPaymentTypeDataMock).when(cartDataMock).getPaymentType();
		doReturn(CheckoutPaymentType.CARD.toString()).when(b2BPaymentTypeDataMock).getCode();

		//testing
		ValidationResults result = validator.validateOnExit();

		//verify
		assertEquals(ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS, result);
	}

	@Test
	public void shouldNotRedirectToDeliveryMethodWhenPaymentTypeIsMissed()
	{
		//given
		doReturn(cartDataMock).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();
		doReturn(null).when(cartDataMock).getPaymentType();

		//testing
		ValidationResults result = validator.validateOnExit();

		//verify
		assertEquals(ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS, result);
	}

	@Test
	public void shouldNotRedirectToDeliveryMethodWhenDigitalCard()
	{
		//given
		doReturn(cartDataMock).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();
		doReturn(b2BPaymentTypeDataMock).when(cartDataMock).getPaymentType();
		doReturn(CheckoutPaymentType.ACCOUNT.toString()).when(b2BPaymentTypeDataMock).getCode();
		doReturn(Long.valueOf(0)).when(cartDataMock).getDeliveryItemsQuantity();

		//testing
		ValidationResults result = validator.validateOnExit();

		//verify
		assertEquals(ValidationResults.SUCCESS, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionWhenMissedCart()
	{
		//given
		doReturn(null).when(wileyb2bCheckoutFacadeMock).getCheckoutCart();

		//testing
		ValidationResults result = validator.validateOnExit();

	}

	@Test
	public void doValidateOnEnterShouldRedirectToDeliveryAddressStepIfZeroOrder()
	{
		//Given
		when(wileyb2bCheckoutFacadeMock.isZeroTotalOrder()).thenReturn(true);
		// When
		ValidationResults result = validator.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertEquals("In case of 0$ flow user should skip Payment Type (payment-type) Step",
				ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS, result);
	}

}
