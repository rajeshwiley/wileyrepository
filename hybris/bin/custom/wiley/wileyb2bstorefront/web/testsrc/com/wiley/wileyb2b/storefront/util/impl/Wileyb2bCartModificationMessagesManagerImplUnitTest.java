package com.wiley.wileyb2b.storefront.util.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.facades.order.data.CartModificationMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Default unit test for {@link Wileyb2bCartModificationMessagesManagerImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCartModificationMessagesManagerImplUnitTest
{

	@InjectMocks
	private Wileyb2bCartModificationMessagesManagerImpl wileyb2bCartModificationMessagesManager;


	@Test
	public void shouldAddOneCartModificationMessageAsInfo()
	{
		// Given
		String someMessage = "SomeMessage";
		CartModificationMessageType messageType = CartModificationMessageType.INFO;
		RedirectAttributes redirectAttributesStub = new RedirectAttributesModelMap();

		// When
		wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(someMessage,
				Optional.ofNullable(messageType), redirectAttributesStub);

		// Then
		checkCartModificationMessage(someMessage, GlobalMessages.INFO_MESSAGES_HOLDER,
				redirectAttributesStub.getFlashAttributes());
	}

	@Test
	public void shouldAddOneCartModificationMessageAsConf()
	{
		// Given
		String someMessage = "SomeMessage";
		CartModificationMessageType messageType = CartModificationMessageType.CONF;
		RedirectAttributes redirectAttributesStub = new RedirectAttributesModelMap();

		// When
		wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(someMessage,
				Optional.ofNullable(messageType), redirectAttributesStub);

		// Then
		checkCartModificationMessage(someMessage, GlobalMessages.CONF_MESSAGES_HOLDER,
				redirectAttributesStub.getFlashAttributes());
	}

	@Test
	public void shouldAddOneCartModificationMessageAsError()
	{
		// Given
		String someMessage = "SomeMessage";
		CartModificationMessageType messageType = CartModificationMessageType.ERROR;
		RedirectAttributes redirectAttributesStub = new RedirectAttributesModelMap();

		// When
		wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(someMessage,
				Optional.ofNullable(messageType), redirectAttributesStub);

		// Then
		checkCartModificationMessage(someMessage, GlobalMessages.ERROR_MESSAGES_HOLDER,
				redirectAttributesStub.getFlashAttributes());
	}

	@Test
	public void shouldAddOneCartModificationMessageAsErrorIfMessageTypeIsAbsent()
	{
		// Given
		String someMessage = "SomeMessage";
		CartModificationMessageType messageType = null;
		RedirectAttributes redirectAttributesStub = new RedirectAttributesModelMap();

		// When
		wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(someMessage,
				Optional.ofNullable(messageType), redirectAttributesStub);

		// Then
		checkCartModificationMessage(someMessage, GlobalMessages.ERROR_MESSAGES_HOLDER,
				redirectAttributesStub.getFlashAttributes());
	}

	@Test
	public void shouldAddListOrCartModificationMessages()
	{
		// Given
		Model model = new ExtendedModelMap();

		String infoMessage = "someInfoMessage";
		CartModificationMessageType infoMessageType = CartModificationMessageType.INFO;
		CartModificationMessage cartModificationInfoMessage = createCartModificationMessage(infoMessage, infoMessageType);

		String confMessage = "someConfMessage";
		CartModificationMessageType confMessageType = CartModificationMessageType.CONF;
		CartModificationMessage cartModificationConfMessage = createCartModificationMessage(confMessage, confMessageType);

		String errorMessage = "someErrorMessage";
		CartModificationMessageType errorMessageType = CartModificationMessageType.ERROR;
		CartModificationMessage cartModificationErrorMessage = createCartModificationMessage(errorMessage, errorMessageType);

		String nullTypeMessage = "nullTypeMessage";
		CartModificationMessageType nullType = null;
		CartModificationMessage cartModificationNullTypeMessage = createCartModificationMessage(nullTypeMessage, nullType);

		List<CartModificationMessage> messages = new ArrayList<>();
		messages.add(cartModificationInfoMessage);
		messages.add(cartModificationConfMessage);
		messages.add(cartModificationErrorMessage);
		messages.add(cartModificationNullTypeMessage);

		// When
		wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(messages, model);

		// Then
		checkCartModificationMessage(infoMessage, GlobalMessages.INFO_MESSAGES_HOLDER, model.asMap());
		checkCartModificationMessage(confMessage, GlobalMessages.CONF_MESSAGES_HOLDER, model.asMap());
		checkCartModificationMessage(errorMessage, GlobalMessages.ERROR_MESSAGES_HOLDER, model.asMap());
		checkCartModificationMessage(nullTypeMessage, GlobalMessages.ERROR_MESSAGES_HOLDER, model.asMap());
	}

	private CartModificationMessage createCartModificationMessage(final String message, final CartModificationMessageType type)
	{
		CartModificationMessage cartModificationMessage = new CartModificationMessage();
		cartModificationMessage.setMessage(message);
		cartModificationMessage.setType(type);
		return cartModificationMessage;
	}

	private void checkCartModificationMessage(final String someMessage, final String holder,
			final Map<String, ?> attributes)
	{
		Map<String, ?> flashAttributes = attributes;
		assertFalse("Flash attributes should not be empty.", flashAttributes.isEmpty());
		assertTrue("Flash attributes should have right holder.", flashAttributes.containsKey(holder));

		List<GlobalMessage> messages = (List<GlobalMessage>) flashAttributes.get(holder);
		assertEquals("Expected message in flash attributes.", someMessage, findMessageInGlobalMessages(someMessage, messages));
	}

	private String findMessageInGlobalMessages(final String someMessage, final List<GlobalMessage> messages)
	{
		return messages.stream()
				.filter(globalMessage -> someMessage.equals(globalMessage.getCode()))
				.map(globalMessage -> globalMessage.getCode())
				.findFirst()
				.orElse(null);
	}

}