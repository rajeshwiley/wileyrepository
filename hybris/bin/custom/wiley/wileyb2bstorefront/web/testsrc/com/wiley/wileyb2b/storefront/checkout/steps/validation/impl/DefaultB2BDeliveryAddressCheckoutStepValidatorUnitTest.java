package com.wiley.wileyb2b.storefront.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.Wileyb2bCheckoutFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultB2BDeliveryAddressCheckoutStepValidatorUnitTest
{
	@Mock
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacade;
	@InjectMocks
	private DefaultB2BDeliveryAddressCheckoutStepValidator testInstance = new DefaultB2BDeliveryAddressCheckoutStepValidator();
	@Mock
	private RedirectAttributes redirectAttributesMock;
	@Mock
	private CartData cartDataMock;

	@Before
	public void setUp() throws Exception
	{
		when(wileyb2bCheckoutFacade.getCheckoutCart()).thenReturn(cartDataMock);
		givenPaymentTypeIsSelected(true);
	}

	@Test
	public void doValidateOnEnterShouldRedirectToSummaryIfDigitalCartAndZeroOrder() throws Exception
	{
		//Given
		givenDigitalCart(true);
		givenZeroOrder(true);
		//When
		ValidationResults result = testInstance.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertEquals("Should redirect to Summary step if $0 order and digital cart",
				ValidationResults.REDIRECT_TO_SUMMARY, result);
	}

	@Test
	public void doValidateOnEnterShouldRedirectToPaymentMethodIfDigitalCartAndNonZeroOrder() throws Exception
	{
		//Given
		givenDigitalCart(true);
		givenZeroOrder(false);
		//When
		ValidationResults result = testInstance.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertEquals("Should redirect to Payment Method step if NON $0 order and digital cart",
				ValidationResults.REDIRECT_TO_PAYMENT_METHOD, result);
	}

	@Test
	public void doValidateOnEnterShouldGoToNextStepIfNonDigitalCart() throws Exception
	{
		//Given
		givenDigitalCart(false);
		//When
		ValidationResults result = testInstance.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertEquals("Should redirect to next step if cart is NOT digital",
				ValidationResults.SUCCESS, result);
	}

	@Test
	public void doValidateOnEnterShouldRedirectToPaymentTypeIfNonZeroOrderAndNoTypeSelected() throws Exception
	{
		//Given
		givenZeroOrder(false);
		givenPaymentTypeIsSelected(false);
		//When
		ValidationResults result = testInstance.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertEquals("Should redirect to Payment Type if NON zero order and no Payment Type is selected",
				ValidationResults.REDIRECT_TO_PAYMENT_TYPE, result);
	}

	@Test
	public void doValidateOnEnterShouldNotCountSelectedPaymentTypeIfZeroOrder() throws Exception
	{
		//Given
		givenZeroOrder(true);
		givenPaymentTypeIsSelected(true);
		//When
		ValidationResults result = testInstance.doValidateOnEnter(redirectAttributesMock);
		//Then
		assertNotEquals("Should not redirect back to Payment Type if NON zero order and Payment Type is selected",
				ValidationResults.REDIRECT_TO_PAYMENT_TYPE, result);
	}

	private void givenZeroOrder(final boolean isZeroOrder) {
		when(wileyb2bCheckoutFacade.isZeroTotalOrder()).thenReturn(isZeroOrder);
	}

	private void givenDigitalCart(final boolean isDigitalCart) {
		when(wileyb2bCheckoutFacade.isDigitalSessionCart()).thenReturn(isDigitalCart);
	}

	private void givenPaymentTypeIsSelected(boolean isSelected) {
		if (isSelected) {
			when(cartDataMock.getPaymentType()).thenReturn(mock(B2BPaymentTypeData.class));
		}
		else {
			when(cartDataMock.getPaymentType()).thenReturn(null);
		}
	}

}