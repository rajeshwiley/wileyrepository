package com.wiley.wileyb2b.storefront.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.facades.order.data.CartModificationMessage;
import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;
import com.wiley.wileyb2b.storefront.util.Wileyb2bCartModificationMessagesManager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link Wileyb2bExternalModificationMessagesBeforeViewHandler}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bExternalModificationMessagesBeforeViewHandlerUnitTest
{

	@Mock
	private Wileyb2bCartFacade wileyb2bCartFacadeMock;

	@Mock
	private Wileyb2bCartModificationMessagesManager wileyb2bCartModificationMessagesManagerMock;

	@InjectMocks
	private Wileyb2bExternalModificationMessagesBeforeViewHandler wileyb2bExternalModificationMessagesBeforeViewHandler;

	// Test data

	@Mock
	private HttpServletRequest httpServletRequestMock;

	@Mock
	private HttpServletResponse httpServletResponseMock;

	private ModelAndView modelAndViewStub;

	private CartModificationMessage cartModificationMessageStub;

	private static final String MESSAGE = "someMessage";

	private static final CartModificationMessageType MESSAGE_TYPE = CartModificationMessageType.INFO;

	private List<CartModificationMessage> cartModificationMessageListStub;

	@Before
	public void setUp() throws Exception
	{
		modelAndViewStub = new ModelAndView();
		cartModificationMessageStub = new CartModificationMessage();
		cartModificationMessageStub.setMessage(MESSAGE);
		cartModificationMessageStub.setType(MESSAGE_TYPE);

		cartModificationMessageListStub = Collections.singletonList(cartModificationMessageStub);

		when(wileyb2bCartFacadeMock.popCartModificationMessages()).thenReturn(cartModificationMessageListStub);
	}

	@Test
	public void shouldAddCartModificationMessagesIfModelIsNotNull() throws Exception
	{
		// Given
		doAnswer(invocation -> {
			List<CartModificationMessage> messages = (List<CartModificationMessage>) invocation.getArguments()[0];
			Model model = (Model) invocation.getArguments()[1];

			messages.forEach(message -> GlobalMessages.addInfoMessage(model, message.getMessage()));
			return null;
		}).when(wileyb2bCartModificationMessagesManagerMock).addCartModificationMessagesToGlobalMessages(
				same(cartModificationMessageListStub), any(Model.class));

		// When
		wileyb2bExternalModificationMessagesBeforeViewHandler.beforeView(httpServletRequestMock, httpServletResponseMock,
				modelAndViewStub);

		// Then
		Map<String, Object> model = modelAndViewStub.getModel();
		assertFalse(model.isEmpty());

		List<GlobalMessage> globalMessages = (List<GlobalMessage>) model.get(GlobalMessages.INFO_MESSAGES_HOLDER);
		assertNotNull("Expected that model will have info messages holder.", globalMessages);
		assertTrue("GlobalMessages holder should contain expected message.",
				doesGlobalMessagesContainMessage(globalMessages, MESSAGE));
	}

	private boolean doesGlobalMessagesContainMessage(final List<GlobalMessage> globalMessages, final String message)
	{
		return globalMessages.stream()
				.filter(globalMessage -> message.equals(globalMessage.getCode()))
				.findAny()
				.isPresent();
	}



}