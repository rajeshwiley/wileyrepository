/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.wileyb2b.storefront.checkout.steps.validation.AbstractB2BCheckoutStepValidator;


public class DefaultB2BPaymentTypeCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{
	private static final String ERROR_MESSAGES_HOLDER = GlobalMessages.ERROR_MESSAGES_HOLDER;

	@Resource
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacade;

	@Autowired
	private HttpServletRequest request;

	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		//store error messages explicitly, to be able pass them by chain through several redirects
		final Map<String, ?> inputFlashMap = getInputFlashMap();
		if (inputFlashMap != null)
		{
			final Object errorMap = inputFlashMap.get(ERROR_MESSAGES_HOLDER);
			getOutputFlashMap().put(ERROR_MESSAGES_HOLDER, errorMap);
		}

		if (wileyb2bCheckoutFacade.isZeroTotalOrder())
		{
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		return ValidationResults.SUCCESS;
	}

	@Override
	public ValidationResults validateOnExit()
	{
		ValidationResults validationResults = ValidationResults.SUCCESS;
		CartData cartData = getCheckoutFacade().getCheckoutCart();
		Assert.notNull(cartData);
		final B2BPaymentTypeData checkoutPaymentType = cartData.getPaymentType();
		if (checkoutPaymentType == null || !CheckoutPaymentType.ACCOUNT.getCode().equals(checkoutPaymentType.getCode()))
		{
			validationResults = ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}
		else
		{
			final List<? extends AddressData> deliveryAddresses = wileyb2bCheckoutFacade.getSupportedDeliveryAddresses(true);
			if (cartData.getDeliveryItemsQuantity() > 0
					&& CheckoutPaymentType.ACCOUNT.getCode().equals(checkoutPaymentType.getCode())
					&& CollectionUtils.isNotEmpty(deliveryAddresses) && deliveryAddresses.size() == 1)
			{
				wileyb2bCheckoutFacade.setDeliveryAddress(deliveryAddresses.stream().findFirst().get());
				validationResults = ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
			}
		}
		return validationResults;
	}

	protected Map getInputFlashMap()
	{
		return RequestContextUtils.getInputFlashMap(request);
	}

	protected Map getOutputFlashMap()
	{
		return RequestContextUtils.getOutputFlashMap(request);
	}
}
