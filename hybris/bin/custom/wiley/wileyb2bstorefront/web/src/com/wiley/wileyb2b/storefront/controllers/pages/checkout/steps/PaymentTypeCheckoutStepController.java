/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.B2BCostCenterFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.converters.Populator;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.wileyb2b.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2b.storefront.forms.PaymentTypeForm;
import com.wiley.wileyb2b.storefront.forms.validation.PaymentTypeFormValidator;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.AbstractWileycomCheckoutStepController;
import com.wiley.wileycom.storefrontcommons.forms.WileycomBillingAddressForm;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "/checkout/multi/payment-type")
public class PaymentTypeCheckoutStepController extends AbstractWileycomCheckoutStepController
{
	private static final String PAYMENT_TYPE = "payment-type";

	@Resource(name = "b2bCheckoutFacade")
	private Wileyb2bCheckoutFacade b2bCheckoutFacade;

	@Resource(name = "costCenterFacade")
	protected B2BCostCenterFacade costCenterFacade;

	@Resource(name = "paymentTypeFormValidator")
	private PaymentTypeFormValidator paymentTypeFormValidator;

	@Resource(name = "wileycomBillingAddressPopulator")
	private Populator<WileycomBillingAddressForm, AddressData> billingAddressPopulator;

	@ModelAttribute("paymentTypes")
	public Collection<B2BPaymentTypeData> getAllB2BPaymentTypes()
	{
		return b2bCheckoutFacade.getPaymentTypes();
	}

	@Override
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_TYPE)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		model.addAttribute("paymentTypeForm", preparePaymentTypeForm(cartData));
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_TYPE_PAGE;
	}

	@RequestMapping(value = "/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String choose(@RequestParam final String paymentType, @ModelAttribute final PaymentTypeForm paymentTypeForm,
			final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		paymentTypeFormValidator.validate(paymentTypeForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.paymenttype.formentry.invalid");
			model.addAttribute("paymentTypeForm", paymentTypeForm);
			prepareDataForPage(model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentType.breadcrumb"));
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_PAYMENT_TYPE_PAGE;
		}

		updateCheckoutCart(paymentType);

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/choose/validate-po-numbers", method = RequestMethod.POST)
	@RequireHardLogIn
	public String validatePoNumbers(@ModelAttribute("cartData") final CartData cartData, final Model model,
			final BindingResult bindingResult)
	{
		populateCartData(cartData);

		final List<String> invalidPONumbers = b2bCheckoutFacade.getInvalidPoNumbers(cartData);
		if (CollectionUtils.isNotEmpty(invalidPONumbers))
		{
			for (String invalidPONumber : invalidPONumbers)
			{
				findEntriesByPONumber(cartData.getEntries(), invalidPONumber).stream().forEach(entry ->
				{
					bindingResult.rejectValue(
							"entries[" + entry.getEntryNumber() + "].poNumber",
							"checkout.error.po.number.invalid");
					entry.setPoNumber(null);
				});
			}
		}

		return ControllerConstants.Views.Fragments.Checkout.PO_NUMBERS_POPUP;
	}

	private List<OrderEntryData> findEntriesByPONumber(final List<OrderEntryData> cartEntries, final String poNumber)
	{
		List<OrderEntryData> entriesWithPoNumber = Collections.emptyList();
		if (CollectionUtils.isNotEmpty(cartEntries) && StringUtils.isNotBlank(poNumber))
		{
			entriesWithPoNumber = cartEntries.stream()
					.filter(entry -> poNumber.equals(entry.getPoNumber()))
					.collect(Collectors.toList());
		}
		return entriesWithPoNumber;
	}

	private void populateCartData(final CartData newCartData)
	{
		final CartData savedCartData = getCheckoutFacade().getCheckoutCart();
		for (OrderEntryData newEntry : newCartData.getEntries())
		{
			savedCartData.getEntries().stream()
					.filter(savedEntry -> newEntry.getEntryNumber().equals(savedEntry.getEntryNumber()))
					.forEach(savedEntry ->
					{
						newEntry.setProduct(savedEntry.getProduct());
						newEntry.setQuantity(savedEntry.getQuantity());
						newEntry.setDeliveryPointOfService(savedEntry.getDeliveryPointOfService());
						newEntry.setTotalPrice(savedEntry.getTotalPrice());
					});
		}
	}

	@RequestMapping(value = "/choose/save-po-number", method = RequestMethod.POST)
	@RequireHardLogIn
	public String savePoNumbers(@ModelAttribute("cartData") final CartData cartData)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		b2bCheckoutFacade.savePoNumberInCart(cartData);
		b2bCheckoutFacade.setPaymentAddressFromUnit();
		updateCheckoutCart(CheckoutPaymentType.ACCOUNT.toString());
		return getCheckoutStep().nextStep();
	}

	protected void updateCheckoutCart(final String paymentType)
	{
		final CartData cartData = new CartData();

		// set payment type
		final B2BPaymentTypeData paymentTypeData = new B2BPaymentTypeData();
		paymentTypeData.setCode(paymentType);

		cartData.setPaymentType(paymentTypeData);
		b2bCheckoutFacade.updateCheckoutCart(cartData);
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	protected PaymentTypeForm preparePaymentTypeForm(final CartData cartData)
	{
		final PaymentTypeForm paymentTypeForm = new PaymentTypeForm();

		// set payment type
		if (cartData.getPaymentType() != null && StringUtils.isNotBlank(cartData.getPaymentType().getCode()))
		{
			paymentTypeForm.setPaymentType(cartData.getPaymentType().getCode());
		}
		else
		{
			paymentTypeForm.setPaymentType(CheckoutPaymentType.ACCOUNT.getCode());
		}

		// set purchase order number
		paymentTypeForm.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
		return paymentTypeForm;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_TYPE);
	}

	@RequestMapping(value = "/show-billing-address-form", method = RequestMethod.GET)
	@RequireHardLogIn
	public String showBillingAddressForm(final Model model)
	{
		model.addAttribute("billingCountries", getCheckoutFacade().getBillingCountries());
		model.addAttribute("formAction", "/checkout/multi/payment-type/save-billing-address-form");
		return "addon:/wileycomstorefrontcommons/fragments/checkout/billingaddress";
	}

	@RequestMapping(value = "/save-billing-address-form", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveBillingAddressForm(final Model model, @Valid final WileycomBillingAddressForm form,
			final BindingResult bindingErrors, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		if (bindingErrors.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
			return REDIRECT_PREFIX + "/checkout/multi/payment-type/choose";
		}

		final PaymentTypeForm paymentTypeForm = new PaymentTypeForm();
		paymentTypeForm.setPaymentType(CheckoutPaymentType.CARD.getCode());

		final AddressData addressData = new AddressData();
		billingAddressPopulator.populate(form, addressData);
		b2bCheckoutFacade.setNewPaymentAddress(addressData,
				form.isSavePaymentInfo() && !getCheckoutCustomerStrategy().isAnonymousCheckout());

		redirectAttributes.addFlashAttribute("billingAddressId", addressData.getId());

		updateCheckoutCart(paymentTypeForm.getPaymentType());
		return getCheckoutStep().nextStep();
	}
}
