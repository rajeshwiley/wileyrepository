/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.util.WileyDateUtils;
import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.facades.order.impl.Wileyb2bCheckoutFacadeImpl;
import com.wiley.wileyb2b.storefront.forms.forms.B2BShippingMethodForm;
import com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.AbstractWileycomDeliveryMethodCheckoutStepController;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "/checkout/multi/delivery-method")
public class DeliveryMethodCheckoutStepController extends AbstractWileycomDeliveryMethodCheckoutStepController
{
	@Autowired
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacade;

	@RequestMapping(value = "/choose", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	@RequireHardLogIn
	public String selectDeliveryMode(final B2BShippingMethodForm shippingMethodForm,
			final Model model) throws CMSItemNotFoundException
	{
		if (!validateDesiredShippingDate(shippingMethodForm.getDesiredShippingDate(), model)) {
			prepareDataForPage(model);
			return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_METHOD_PAGE;
		}

		if (StringUtils.isNotEmpty(shippingMethodForm.getSelectedDeliveryMethod()))
		{
			wileyb2bCheckoutFacade.setDeliveryMode(shippingMethodForm.getSelectedDeliveryMethod());
			wileyb2bCheckoutFacade.setDesiredShippingDate(shippingMethodForm.getDesiredShippingDate());
		}

		return getCheckoutStep().nextStep();
	}

	@Override
	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("shippingMethodForm", new B2BShippingMethodForm());
		super.prepareDataForPage(model);
	}

	private boolean validateDesiredShippingDate(final String desiredShippingDate, final Model model) {
		if (StringUtils.isBlank(desiredShippingDate)) {
			return true;
		}
		try
		{
			Date date = WileyDateUtils.convertDateStringToDate(desiredShippingDate, Wileyb2bCheckoutFacadeImpl.DATE_PATTERN);

			if (WileyDateUtils.pastDate(
					LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()),
					WileyDateUtils.DatePreciseMode.ROUND_TO_DAY)
					) {
				GlobalMessages.addErrorMessage(model, "checkout.shippingMethod.desiredShippingDate.dateInThePast");
				return false;
			}
		}
		catch (ParseException e)
		{
			GlobalMessages.addErrorMessage(model, "checkout.shippingMethod.desiredShippingDate.invalidFormat");
			LOG.info("Validation failed durin checkout, desiredShippingDate incorrect format");
			return false;
		}
		return true;
	}
}
