package com.wiley.wileyb2b.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Conventions;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.facades.order.data.CartModificationMessage;
import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;
import com.wiley.wileyb2b.storefront.util.Wileyb2bCartModificationMessagesManager;


/**
 * This BeforeViewHandler adds cartmodification messages to global messages.
 */
public class Wileyb2bExternalModificationMessagesBeforeViewHandler implements BeforeViewHandler
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bExternalModificationMessagesBeforeViewHandler.class);

	@Resource
	private Wileyb2bCartFacade wileyb2bCartFacade;

	@Resource
	private Wileyb2bCartModificationMessagesManager wileyb2bCartModificationMessagesManager;

	@Override
	public void beforeView(@Nonnull final HttpServletRequest request, @Nonnull final HttpServletResponse response,
			@Nonnull final ModelAndView modelAndView)
			throws Exception
	{
		Assert.notNull(request);
		Assert.notNull(response);
		Assert.notNull(modelAndView);

		LOG.debug("Populating cart modification messages for request path [{}]", request.getServletPath());

		Map<String, Object> model = modelAndView.getModel();

		if (model != null)
		{
			List<CartModificationMessage> cartModificationMessages = wileyb2bCartFacade.popCartModificationMessages();
			wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(cartModificationMessages,
					new ModelMapAdapter(model));
		}
		else
		{
			LOG.warn("There is no model for view [{}] and request path [{}]", modelAndView.getViewName(),
					request.getServletPath());
		}
	}

	private static class ModelMapAdapter implements Model
	{

		private Map<String, Object> map;

		ModelMapAdapter(@Nonnull final Map<String, Object> map)
		{
			Assert.state(map != null);
			this.map = map;
		}

		@Override
		public Model addAttribute(@Nonnull final String s, @Nonnull final Object o)
		{
			Assert.notNull(s);
			Assert.notNull(o);

			map.put(s, o);
			return this;
		}

		@Override
		public Model addAttribute(@Nonnull final Object o)
		{
			Assert.notNull(o);

			map.put(Conventions.getVariableName(o), o);
			return this;
		}

		@Override
		public Model addAllAttributes(@Nonnull final Collection<?> collection)
		{
			Assert.notNull(collection);

			collection.stream()
					.forEach(element -> map.put(Conventions.getVariableName(element), element));
			return this;
		}

		@Override
		public Model addAllAttributes(@Nonnull final Map<String, ?> map)
		{
			Assert.notNull(map);

			this.map.putAll(map);
			return this;
		}

		@Override
		public Model mergeAttributes(@Nonnull final Map<String, ?> map)
		{
			Assert.notNull(map);

			this.map.putAll(map);
			return this;
		}

		@Override
		public boolean containsAttribute(@Nullable final String s)
		{
			return map.containsKey(s);
		}

		@Override
		public Map<String, Object> asMap()
		{
			return map;
		}
	}
}
