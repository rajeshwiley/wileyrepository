package com.wiley.wileyb2b.storefront.interceptors;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;


/**
 * The purpose of this interceptor to redirect by url if cart is empty. it is used during checkout flow basically.
 */
public class EmptyCartRedirectingHandlerInterceptor extends HandlerInterceptorAdapter
{

	@Resource
	private Wileyb2bCartFacade wileyb2bCartFacade;

	private String redirectUrl;

	@Override
	public void postHandle(@Nonnull final HttpServletRequest request, @Nonnull final HttpServletResponse response,
			@Nullable final Object handler,
			@Nullable final ModelAndView modelAndView) throws Exception
	{
		Assert.notNull(request);
		Assert.notNull(response);

		if (modelAndView != null && !isIncludeRequest(request) && isSupportedView(modelAndView))
		{
			if (!wileyb2bCartFacade.hasEntries())
			{
				modelAndView.setViewName(AbstractController.REDIRECT_PREFIX + redirectUrl);
			}
		}
	}

	@Required
	public void setRedirectUrl(final String redirectUrl)
	{
		this.redirectUrl = redirectUrl;
	}

	private boolean isIncludeRequest(final HttpServletRequest request)
	{
		return request.getAttribute("javax.servlet.include.request_uri") != null;
	}

	// e.g. not AJAX request
	private boolean isSupportedView(final ModelAndView modelAndView)
	{
		if (StringUtils.contains(modelAndView.getViewName(), "orderConfirmation")) {
			return false;
		}
		return modelAndView.getViewName() != null || modelAndView.getView() instanceof RedirectView;
	}

}
