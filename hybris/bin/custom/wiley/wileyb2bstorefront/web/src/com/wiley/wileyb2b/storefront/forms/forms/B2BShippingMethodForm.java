package com.wiley.wileyb2b.storefront.forms.forms;

/**
 * Form is used to transfer required data from
 */
public class B2BShippingMethodForm
{
	private String selectedDeliveryMethod;

	private String desiredShippingDate;

	public String getSelectedDeliveryMethod()
	{
		return selectedDeliveryMethod;
	}

	public void setSelectedDeliveryMethod(final String selectedDeliveryMethod)
	{
		this.selectedDeliveryMethod = selectedDeliveryMethod;
	}

	public String getDesiredShippingDate()
	{
		return desiredShippingDate;
	}

	public void setDesiredShippingDate(final String desiredShippingDate)
	{
		this.desiredShippingDate = desiredShippingDate;
	}
}
