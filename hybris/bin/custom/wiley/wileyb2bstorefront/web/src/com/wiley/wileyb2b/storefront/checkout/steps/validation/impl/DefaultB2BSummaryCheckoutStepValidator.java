/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.wileyb2b.storefront.checkout.steps.validation.AbstractB2BCheckoutStepValidator;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.INFO_MESSAGES_HOLDER;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.addFlashMessage;


public class DefaultB2BSummaryCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{
	@Resource
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacade;

	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final CartData checkoutCart = getCheckoutFacade().getCheckoutCart();
		final B2BPaymentTypeData checkoutPaymentType = checkoutCart.getPaymentType();

		if (!wileyb2bCheckoutFacade.isZeroTotalOrder() && checkoutPaymentType == null)
		{
			addFlashMessage(redirectAttributes, INFO_MESSAGES_HOLDER, "checkout.multi.paymentType.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}

		if (!wileyb2bCheckoutFacade.isDigitalSessionCart())
		{
			if (getCheckoutFlowFacade().hasNoDeliveryAddress())
			{
				addFlashMessage(redirectAttributes, INFO_MESSAGES_HOLDER, "checkout.multi.deliveryAddress.notprovided");
				return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}

			if (getCheckoutFlowFacade().hasNoDeliveryMode() && !wileyb2bCheckoutFacade.getSupportedDeliveryModes().isEmpty())
			{
				addFlashMessage(redirectAttributes, INFO_MESSAGES_HOLDER, "checkout.multi.deliveryMethod.notprovided");
				return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
			}
		}

		if (!wileyb2bCheckoutFacade.isZeroTotalOrder()
				&& isCardPayment(checkoutPaymentType) && getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			addFlashMessage(redirectAttributes, INFO_MESSAGES_HOLDER, "checkout.multi.paymentDetails.notprovided");
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}

		if (!getCheckoutFacade().hasShippingItems())
		{
			checkoutCart.setDeliveryAddress(null);
		}

		return ValidationResults.SUCCESS;
	}

	private boolean isCardPayment(final B2BPaymentTypeData b2BPaymentTypeData)
	{
		return b2BPaymentTypeData != null && CheckoutPaymentType.CARD.getCode().equals(b2BPaymentTypeData.getCode());
	}
}
