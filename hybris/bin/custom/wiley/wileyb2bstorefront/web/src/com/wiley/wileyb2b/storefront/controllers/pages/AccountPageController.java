/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.facades.wileyb2b.order.Wileyb2bSendEmailWithOrderDetailsFacade;
import com.wiley.wileyb2b.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2b.storefront.forms.EmailInvoiceForm;
import com.wiley.wileyb2b.storefront.forms.Wileyb2bUpdateProfileForm;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomAccountPageController;

import static com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants.EXTERNAL_SERVICE_ERROR_KEY;
import static com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants.FORM_GLOBAL_ERROR_KEY;


/**
 * Controller for home page
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@Scope("tenant")
@RequestMapping("/my-account")
public class AccountPageController extends AbstractWileycomAccountPageController
{

	private static final Logger LOG = LoggerFactory.getLogger(AccountPageController.class);
	private static final String NOT_SUPPORTED_ON_B2B_SITE_MESSAGE = "On B2B site it is not allowed to fire update-email request.";

	@Resource(name = "wileyb2bOrderFacade")
	private B2BOrderFacade wileyb2bOrderFacade;

	@Resource(name = "wileyb2bUpdateProfileFormValidator")
	private ProfileValidator profileValidator;

	@Resource
	private Wileyb2bSendEmailWithOrderDetailsFacade wileyb2bSendEmailWithOrderDetailsFacade;


	@RequestMapping(value = "/update-email", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String editEmail(final Model model) throws CMSItemNotFoundException
	{
		throw new UnsupportedOperationException(NOT_SUPPORTED_ON_B2B_SITE_MESSAGE);
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.POST)
	@RequireHardLogIn
	@Override
	public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		throw new UnsupportedOperationException(NOT_SUPPORTED_ON_B2B_SITE_MESSAGE);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", getCustomerFacade().getNameSuffixes());

		final CustomerData customerData = getCustomerFacade().getCurrentCustomer();
		final Wileyb2bUpdateProfileForm updateProfileForm = new Wileyb2bUpdateProfileForm();

		updateProfileForm.setEmail(customerData.getEmail());
		updateProfileForm.setLoginId(customerData.getDisplayUid());
		updateProfileForm.setTitleCode(customerData.getTitleCode());
		updateProfileForm.setFirstName(customerData.getFirstName());
		updateProfileForm.setMiddleName(customerData.getMiddleName());
		updateProfileForm.setLastName(customerData.getLastName());
		updateProfileForm.setSuffixCode(customerData.getSuffixCode());

		model.addAttribute("updateProfileForm", updateProfileForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		model.addAttribute("breadcrumbs", getAccountBreadcrumbBuilder().getBreadcrumbs("text.account.profile"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(@ModelAttribute("updateProfileForm") final Wileyb2bUpdateProfileForm updateProfileForm,
			final BindingResult bindingResult,
			final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		getProfileValidator().validate(updateProfileForm, bindingResult);

		String returnAction = REDIRECT_TO_UPDATE_PROFILE;

		final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();
		updateProfileForm.setLoginId(currentCustomerData.getDisplayUid());
		updateProfileForm.setEmail(currentCustomerData.getEmail());

		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(updateProfileForm.getTitleCode());
		customerData.setFirstName(updateProfileForm.getFirstName());
		customerData.setMiddleName(updateProfileForm.getMiddleName());
		customerData.setLastName(updateProfileForm.getLastName());
		customerData.setSuffixCode(updateProfileForm.getSuffixCode());
		customerData.setUid(currentCustomerData.getUid());
		customerData.setDisplayUid(currentCustomerData.getDisplayUid());

		model.addAttribute("titleData", getUserFacade().getTitles());

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		if (bindingResult.hasErrors())
		{
			model.addAttribute("updateProfileForm", updateProfileForm);
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, FORM_GLOBAL_ERROR_KEY);
		}
		else
		{
			try
			{
				getCustomerFacade().updateProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

			}
			catch (ExternalSystemException e)
			{
				LOG.debug("can not update user '" + customerData.getUid() + "' due to, " + e.getMessage(), e);
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, EXTERNAL_SERVICE_ERROR_KEY);
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, FORM_GLOBAL_ERROR_KEY);
			}
		}

		model.addAttribute("breadcrumbs", getAccountBreadcrumbBuilder().getBreadcrumbs("text.account.profile"));
		return returnAction;
	}

	@Override
	public String order(@PathVariable("orderCode") final String orderCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		EmailInvoiceForm emailInvoiceForm = new EmailInvoiceForm();
		emailInvoiceForm.setEmail(getCustomerFacade().getCurrentCustomer().getEmail());
		model.addAttribute(emailInvoiceForm);

		return super.order(orderCode, model, redirectModel);
	}

	/**
	 * Submit of Email Invoice Form.
	 */
	@RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN + "/emailinvoice", method = RequestMethod.POST)
	public String submitEmailInvoice(@PathVariable("orderCode") final String orderCode, @Valid final EmailInvoiceForm form,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return ControllerConstants.Views.Fragments.Account.ACCOUNT_EMAIL_INVOICE_FORM;
		}

		boolean isEmailSent;
		try
		{
			isEmailSent = wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(orderCode, form.getEmail());
		}
		catch (UnknownIdentifierException | ExternalSystemException e)
		{
			isEmailSent = false;
			LOG.error("Failed on email sending", e);
		}

		if (isEmailSent)
		{
			GlobalMessages.addConfMessage(model, "emailInvoice.email.confirmation");
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "emailInvoice.externalSystem.failed");
		}

		return ControllerConstants.Views.Fragments.Account.ACCOUNT_EMAIL_INVOICE_FORM;


	}

	public ProfileValidator getProfileValidator()
	{
		return profileValidator;
	}

	@Override
	public OrderFacade getOrderFacade()
	{
		return wileyb2bOrderFacade;
	}
}
