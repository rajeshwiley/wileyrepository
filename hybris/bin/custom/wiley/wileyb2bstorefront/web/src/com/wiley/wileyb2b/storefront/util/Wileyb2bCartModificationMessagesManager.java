package com.wiley.wileyb2b.storefront.util;

import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.facades.order.data.CartModificationMessage;


/**
 * Contains util methods for working with cart modification messages on storefront.
 */
public interface Wileyb2bCartModificationMessagesManager
{

	/**
	 * Adds cart modification messages to global messages according to message type.
	 *
	 * @param cartModificationMessages
	 * @param model
	 */
	void addCartModificationMessagesToGlobalMessages(@Nonnull List<CartModificationMessage> cartModificationMessages,
			@Nonnull Model model);

	/**
	 * Adds cart modification messages to global messages according to message type. Uses flash messages holder.
	 *
	 * @param message
	 * @param messageType
	 * 		if message type null, the default (error) type will be used.
	 * @param redirectAttributes
	 */
	void addCartModificationMessagesToGlobalMessages(@Nonnull String message,
			@Nonnull Optional<CartModificationMessageType> messageType,
			@Nonnull RedirectAttributes redirectAttributes);

	/**
	 * Tries to add cart modification messages to cart.<br/>
	 * If status message is empty or null, the method returns related CartModificationData
	 *
	 * @param modifications
	 * @param redirectModel
	 * @return list of CartModificationData which have no status message and which were not added to GlobalMessages
	 */
	List<CartModificationData> addCartModificationMessagesToGlobalMessages(@Nonnull List<CartModificationData> modifications,
			@Nonnull RedirectAttributes redirectModel);

}
