/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.impl.GenericViewCMSComponentRenderer;


/**
 * b2bacceleratoraddon renderer for ProductAddToCartComponents
 */
public class B2BAcceleratorProductAddToCartComponentRenderer extends GenericViewCMSComponentRenderer
{

}
