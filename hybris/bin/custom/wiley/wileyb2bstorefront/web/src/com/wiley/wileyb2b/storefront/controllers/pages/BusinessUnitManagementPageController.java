/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.b2bcommercefacades.company.data.B2BSelectionData;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.facades.common.error.ErrorData;
import com.wiley.wileyb2b.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2b.storefront.forms.forms.Wileyb2bCustomerForm;


/**
 * Controller defines routes to manage Business Units within My Company section.
 */
@Controller
@Scope("tenant")
@RequestMapping("/my-company/organization-management/manage-unit")
public class BusinessUnitManagementPageController extends MyCompanyPageController
{
	private static final Logger LOG = Logger.getLogger(BusinessUnitManagementPageController.class);
	private static final String DEFAULT_SORT_CODE = UserModel.NAME;
	private static final String USER_GROUP_ADMINISTRATORS = "administrators";
	private static final String USER_GROUP_CUSTOMERS = "customers";
	private static final String USER_GROUP_MANAGERS = "managers";
	private static final int DEFAULT_PAGE_SIZE = 5;

	@Autowired
	private WileyCountryService wileyCountryService;

	@Autowired
	@Qualifier("defaultI18NFacade")
	private I18NFacade i18nFacade;

	@Autowired
	@Qualifier("wileycomAddressValidator")
	private Validator addressValidator;

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String manageUnits(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		B2BUnitData unitData = companyB2BCommerceFacade.getParentUnit();
		model.addAttribute("billingAddressSingletonList", Collections.singletonList(unitData.getBillingAddress()));
		return super.unitDetails(unitData, model, request);
	}

	@RequestMapping(value = "/createuser", method = RequestMethod.GET)
	@RequireHardLogIn
	public String createCustomerOfUnit(@RequestParam("unit") final String unit, @RequestParam("role") final String role,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String url = createUser(model);
		final Wileyb2bCustomerForm b2bCustomerForm = (Wileyb2bCustomerForm) model.asMap().get("b2BCustomerForm");
		b2bCustomerForm.setParentB2BUnit(unit);
		b2bCustomerForm.setRoles(Collections.singleton(role));

		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/createuser/?unit=%s&role=%s",
						urlEncode(unit), urlEncode(role)),
				getMessageSource().getMessage("text.company.organizationManagement", null, getI18nService().getCurrentLocale()),
				null));
		model.addAttribute("action", "manage.units");
		model.addAttribute("saveUrl", String.format(request.getContextPath()
						+ "/my-company/organization-management/manage-unit/createuser?unit=%s&role=%s",
				urlEncode(unit), urlEncode(role)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute("isUserCreation", true);
		return url;
	}

	@RequestMapping(value = "/createuser", method = RequestMethod.POST)
	@RequireHardLogIn
	public String createCustomerOfUnit(@RequestParam("unit") final String unit, @RequestParam("role") final String role,
			@Valid @ModelAttribute("b2BCustomerForm") final Wileyb2bCustomerForm b2BCustomerForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute("saveUrl", String.format(request.getContextPath()
						+ "/my-company/organization-management/manage-unit/createuser?unit=%s&role=%s",
				urlEncode(unit), urlEncode(role)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));

		b2BCustomerForm.setParentB2BUnit(unit);
		b2BCustomerForm.setRoles(Collections.singleton(role));

		final String url = createUser(b2BCustomerForm, bindingResult, model, redirectModel);

		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/createuser?unit=%s&role=%s",
						urlEncode(unit), urlEncode(role)),
				getMessageSource().getMessage("text.company.manage.units.createuser.breadcrumb",
						new Object[] { b2BCustomerForm.getUid() }, "Create Customer {0} ", getI18nService().getCurrentLocale()),
				null));

		model.addAttribute("action", "manage.units");
		model.addAttribute("isUserCreation", true);

		if (bindingResult.hasErrors() || model.containsAttribute(GlobalMessages.ERROR_MESSAGES_HOLDER))
		{
			return url;
		}
		else
		{
			return REDIRECT_TO_UNIT_DETAIL;
		}
	}

	@RequestMapping(value = "/edituser", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editCustomerOfUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String url = editUser(user, model);
		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/edituser/?unit=%s&user=%s",
						urlEncode(unit), urlEncode(user)),
				getMessageSource().getMessage("text.company.manage.units.editcustomer.breadcrumb", new Object[] { user },
						"Edit Customer {0} ", getI18nService().getCurrentLocale()),
				null));
		model.addAttribute("action", "manage.units");
		model.addAttribute("saveUrl", String.format(request.getContextPath()
						+ "/my-company/organization-management/manage-unit/edituser/?unit=%s&user=%s",
				urlEncode(unit), urlEncode(user)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		return url;
	}

	@RequestMapping(value = "/edituser", method = RequestMethod.POST)
	@RequireHardLogIn
	public String editCustomerOfUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			@Valid final Wileyb2bCustomerForm b2bCustomerForm, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final String url = editUser(user, b2bCustomerForm, bindingResult, model, redirectModel);
		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/edituser/?unit=%s&user=%s",
						urlEncode(unit), urlEncode(user)),
				getMessageSource().getMessage("text.company.manage.units.editcustomer.breadcrumb", new Object[] { user },
						"Edit Customer {0} ", getI18nService().getCurrentLocale()), null));
		if (bindingResult.hasErrors() || model.containsAttribute(GlobalMessages.ERROR_MESSAGES_HOLDER))
		{
			model.addAttribute("action", "manage.units");
			model.addAttribute("saveUrl", String.format(request.getContextPath()
							+ "/my-company/organization-management/manage-unit/edituser/?unit=%s&user=%s",
					urlEncode(unit), urlEncode(user)));
			model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
			return url;
		}
		else
		{
			return REDIRECT_TO_UNIT_DETAIL;
		}
	}

	@RequestMapping(value = "/{userGroup:^administrators$|^customers$|^managers$}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getPagedUserGroupForUnit(@PathVariable final String userGroup,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final AbstractSearchPageController.ShowMode showMode,
			@RequestParam("unit") final String unit, @RequestParam("role") final String role, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));

		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/customers?unit=%s&role=%s",
						urlEncode(unit), urlEncode(role)),
				getMessageSource().getMessage("text.company.manage.units." + userGroup, new Object[] { unit },
						getI18nService().getCurrentLocale()),
				null));

		final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unit);
		model.addAttribute("unit", unitData);

		// Handle paged search results
		final PageableData pageableData = createPageableData(page, DEFAULT_PAGE_SIZE, DEFAULT_SORT_CODE, showMode);
		final SearchPageData<CustomerData> searchPageData = getSearchPageDataForGroup(userGroup, unit, pageableData);
		populateModel(model, searchPageData, showMode);
		model.addAttribute("action", userGroup);
		model.addAttribute("baseUrl", MANAGE_UNITS_BASE_URL);
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_USER_LIST_PAGE;
	}

	private SearchPageData<CustomerData> getSearchPageDataForGroup(final String userGroup, final String unit,
			final PageableData pageableData)
	{
		SearchPageData<CustomerData> searchPageData = new SearchPageData<>();
		if (USER_GROUP_ADMINISTRATORS.equals(userGroup))
		{
			searchPageData = b2bUnitFacade.getPagedAdministratorsForUnit(pageableData, unit);
		}
		else if (USER_GROUP_CUSTOMERS.equals(userGroup))
		{
			searchPageData = b2bUnitFacade.getPagedCustomersForUnit(pageableData, unit);
		}
		else if (USER_GROUP_MANAGERS.equals(userGroup))
		{
			searchPageData = b2bUnitFacade.getPagedManagersForUnit(pageableData, unit);
		}
		return searchPageData;
	}

	@ResponseBody
	@RequestMapping(value = "/approvers/select", method =
			{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public B2BSelectionData selectApprover(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model) throws CMSItemNotFoundException
	{
		return populateDisplayNamesForRoles(b2bApproverFacade.addApproverToUnit(unit, user));
	}

	@ResponseBody
	@RequestMapping(value = "/approvers/deselect", method =
			{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public B2BSelectionData deselectApprover(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model) throws CMSItemNotFoundException
	{
		return populateDisplayNamesForRoles(b2bApproverFacade.removeApproverFromUnit(unit, user));
	}

	@RequestMapping(value = "/approvers/remove", method =
			{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeApproverFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		b2bApproverFacade.removeApproverFromUnit(unit, user);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "success.remove.user.from.unit",
				new Object[]
						{ user, unit });
		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/members/remove", method = { RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeMemberFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			@RequestParam("role") final String role, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		try
		{
			b2bUserFacade.removeUserRole(user, role);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "success.remove.user.from.unit",
					new Object[] { user, unit });
		}
		catch (ExternalSystemException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("can not remove user role for %s (%s) due to, %s", user, role, e.getMessage()), e);
			}
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "externalService.response.error");
		}
		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/members/disable", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String disableMemberFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			b2bUserFacade.disableCustomer(user);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.confirmation.user.disable");
		}
		catch (final ExternalSystemException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Can not disable user %s due to, %s", user, e.getMessage()), e);
			}
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "externalService.response.error");
		}
		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/members/enable", method = RequestMethod.POST)
	@RequireHardLogIn
	public String enableUser(@RequestParam("user") final String user, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException, DuplicateUidException
	{
		try
		{
			b2bUserFacade.enableCustomer(user);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.confirmation.user.enable");
		}
		catch (final ExternalSystemException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("can not enable user '" + user + "' due to, " + e.getMessage(), e);
			}
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "externalService.response.error");
		}
		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/members/confirm/remove", method =
	{ RequestMethod.GET })
	@RequireHardLogIn
	public String confirmRemoveMemberFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			@RequestParam("role") final String role, final Model model, final HttpServletRequest request)
					throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		addBreadcrumb(unit, model, new Breadcrumb("#",
				getMessageSource().getMessage(String.format("text.company.units.remove.%s.confirmation", role), new Object[]
		{ user, unit }, "Remove {0}", getI18nService().getCurrentLocale()), null));

		model.addAttribute("arguments", String.format("%s, %s", user, unit));
		model.addAttribute("pageType", "units");
		model.addAttribute("role", role);
		model.addAttribute("disableUrl",
				String.format(
						request.getContextPath()
								+ "/my-company/organization-management/manage-unit/members/remove/?unit=%s&user=%s&role=%s",
						urlEncode(unit), urlEncode(user), urlEncode(role)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_REMOVE_DISABLE_CONFIRMATION_PAGE;

	}

	@RequestMapping(value = "/approvers/confirm/remove", method =
	{ RequestMethod.GET })
	@RequireHardLogIn
	public String confirmRemoveApproverFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			@RequestParam("role") final String role, final Model model, final HttpServletRequest request)
					throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		addBreadcrumb(unit, model, new Breadcrumb("#",
				getMessageSource().getMessage(String.format("text.company.units.remove.%s.confirmation", role), new Object[]
		{ user, unit }, "Remove {0}", getI18nService().getCurrentLocale()), null));

		model.addAttribute("arguments", String.format("%s, %s", user, unit));
		model.addAttribute("page", "units");
		model.addAttribute("role", role);
		model.addAttribute("disableUrl",
				String.format(
					request.getContextPath()
					+ "/my-company/organization-management/manage-unit/approvers/remove/?unit=%s&user=%s",
					urlEncode(unit), urlEncode(user)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_REMOVE_DISABLE_CONFIRMATION_PAGE;
	}

	@RequestMapping(value = "/members/confirm/disable", method =
	{ RequestMethod.GET })
	@RequireHardLogIn
	public String confirmDisableMemberFromUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(user);
		breadcrumbs
				.add(new Breadcrumb(
						String.format("/my-company/organization-management/manage-unit/members/disable/?unit=%s&user=%s",
								urlEncode(unit), urlEncode(user)),
						getMessageSource().getMessage("text.company.manageUnit.disable.breadcrumb", new Object[]
		{ user }, "Disable {0}  Customer ", getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);

		model.addAttribute("userUid", user);
		model.addAttribute("disableUrl",
				String.format(
					request.getContextPath() + "/my-company/organization-management/manage-unit/members/disable/?unit=%s&user=%s",
					urlEncode(unit), urlEncode(user)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_DISBALE_CONFIRM_PAGE;

	}

	@ResponseBody
	@RequestMapping(value = "/members/select", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public B2BSelectionData selectCustomer(@RequestParam("user") final String user, @RequestParam("role") final String role,
			final Model model) throws CMSItemNotFoundException
	{
		return populateDisplayNamesForRoles(b2bUserFacade.addUserRole(user, role));
	}

	@ResponseBody
	@RequestMapping(value = "/members/deselect", method = { RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public B2BSelectionData deselectCustomer(@RequestParam("user") final String user, @RequestParam("role") final String role,
			final Model model) throws CMSItemNotFoundException
	{
		return populateDisplayNamesForRoles(b2bUserFacade.removeUserRole(user, role));
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addAddress(@RequestParam("unit") final String unit, final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("addressForm", new AddressForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);
		breadcrumbs.add(new Breadcrumb(String.format("/my-company/organization-management/manage-unit/add-address/?unit=%s",
				urlEncode(unit)), getMessageSource().getMessage("text.company.manage.units.addAddress", new Object[]
				{ unit }, "Add Address for {0} Business Unit ", getI18nService().getCurrentLocale()), null));
		final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unit);
		if (unitData != null)
		{
			model.addAttribute("unitName", unitData.getName());
		}
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("uid", unit);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_ADD_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addAddress(@RequestParam("unit") final String unit, final AddressForm addressForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		addressValidator.validate(addressForm, bindingResult);
		final String breadcrumbUrl = String.format("/my-company/organization-management/manage-unit/add-address/?unit=%s",
				urlEncode(unit));
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			addBreadcrumb(unit, model, new Breadcrumb(
					breadcrumbUrl, getMessageSource().getMessage("text.company.manage.units.addAddress", new Object[]
					{ unit }, "Add Address to {0} Business Unit ", getI18nService().getCurrentLocale()), null));
			return processFormErrors(unit, addressForm, model);
		}

		final AddressData newAddress = convertAddressFormToAddressData(addressForm);

		try
		{
			try
			{
				b2bUnitFacade.addAddressToUnit(newAddress, unit);
			}
			catch (Exception e)
			{
				LOG.error("Exception occured during adding address to unit: [" + unit + "]", e);
				GlobalMessages.addErrorMessage(model, "form.global.error.tryLater");
				addBreadcrumb(unit, model, new Breadcrumb(
						breadcrumbUrl, getMessageSource().getMessage("text.company.manage.units.addAddress", new Object[]
						{ unit }, "Add Address to {0} Business Unit ", getI18nService().getCurrentLocale()), null));
				return processFormErrors(unit, addressForm, model);
			}
			GlobalMessages.addFlashMessage(
					redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added");
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
		}

		return REDIRECT_TO_UNIT_DETAIL;
	}

	protected AddressData convertAddressFormToAddressData(final AddressForm addressForm)
	{
		final AddressData newAddress = new AddressData();
		newAddress.setId(addressForm.getAddressId());
		newAddress.setTitleCode(addressForm.getTitleCode());
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setRegion(
				i18nFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso())
		);
		final CountryData countryData = new CountryData();

		countryData.setIsocode(addressForm.getCountryIso());
		newAddress.setCountry(countryData);
		return newAddress;
	}

	@RequestMapping(value = "/remove-address", method = { RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("unit") final String unit, @RequestParam("addressId") final String addressId,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		if (!isShippingAddress(unit, addressId))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			return REDIRECT_TO_UNIT_DETAIL;
		}

		try
		{

			b2bUnitFacade.removeAddressFromUnit(unit, addressId);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.confirmation.address.removed");
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error.tryLater");
		}

		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/edit-address", method = { RequestMethod.GET })
	@RequireHardLogIn
	public String editAddress(@RequestParam("unit") final String unit, @RequestParam("addressId") final String addressId,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		final AddressForm addressForm = new AddressForm();
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("addressForm", addressForm);

		final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unit);
		if (unit != null)
		{
			Optional<AddressData> addressData = getAddressDataFromUnit(addressId, unitData);
			if (addressData.isPresent())
			{
				AddressData address = addressData.get();
				model.addAttribute("addressData", address);
				addressForm.setAddressId(address.getId());
				addressForm.setTitleCode(address.getTitleCode());
				addressForm.setFirstName(address.getFirstName());
				addressForm.setLastName(address.getLastName());
				addressForm.setLine1(address.getLine1());
				addressForm.setLine2(address.getLine2());
				addressForm.setTownCity(address.getTown());
				addressForm.setPostcode(address.getPostalCode());
				addressForm.setCountryIso(address.getCountry().getIsocode());
				if (address.getRegion() != null)
				{
					addressForm.setRegionIso(address.getRegion().getIsocode());
				}
			}
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "b2bunit.notfound");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);
		breadcrumbs.add(new Breadcrumb(String.format(
				"/my-company/organization-management/manage-unit/edit-address/?unit=%s&addressId=%s", urlEncode(unit),
				urlEncode(addressId)), getMessageSource().getMessage("text.company.manage.units.editAddress", new Object[]
				{ unit }, "Edit Address for {0} Business Unit ", getI18nService().getCurrentLocale()), null));

		if (addressForm.getCountryIso() != null)
		{
			addCountryAndRegionToModel(addressForm, model);
		}

		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("uid", unit);
		model.addAttribute("unitName", unitData.getName());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_ADD_ADDRESS_PAGE;
	}

	private boolean isShippingAddress(final String unitUid, final String addressId)
	{
		final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unitUid);
		if (unitData != null)
		{
			return unitData.getShippingAddresses().stream()
					.filter(addressData -> addressId.equals(addressData.getId()) && addressData.isShippingAddress())
					.findFirst().isPresent();

		}
		return false;
	}

	@RequestMapping(value = "/edit-address", method =
			{ RequestMethod.POST })
	@RequireHardLogIn
	public String editAddress(@RequestParam("unit") final String unit, final AddressForm addressForm,
			final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressValidator.validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			addBreadcrumb(unit, model, new Breadcrumb(String.format(
					"/my-company/organization-management/manage-unit/edit-address/?unit=%s&addressId=%s", urlEncode(unit),
					urlEncode(addressForm.getAddressId())),
					getMessageSource().getMessage("text.company.manage.units.editAddress.breadcrumb",
							new Object[]
									{ unit }, "Edit Address of {0} Business Unit ", getI18nService().getCurrentLocale()), null));

			final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unit);
			GlobalMessages.addErrorMessage(model, "form.global.error");
			if (unitData != null)
			{
				Optional<AddressData> address = getAddressDataFromUnit(addressForm.getAddressId(), unitData);
				if (address.isPresent())
				{
					model.addAttribute("addressData", address.get());
				}
			}
			return processFormErrors(unit, addressForm, model);
		}

		final AddressData newAddress = convertAddressFormToAddressData(addressForm);
		try
		{
			b2bUnitFacade.editAddressOfUnit(newAddress, unit);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.confirmation.address.updated");
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			GlobalMessages.addErrorMessage(model, "form.global.error.tryLater");
			return processFormErrors(unit, addressForm, model);
		}

		return REDIRECT_TO_UNIT_DETAIL;
	}

	@RequestMapping(value = "/viewuser", method = RequestMethod.GET)
	@RequireHardLogIn
	public String viewCustomerOfUnit(@RequestParam("unit") final String unit, @RequestParam("user") final String user,
			final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String url = manageUserDetail(user, model, request);
		addBreadcrumb(unit, model, new Breadcrumb(
				String.format("/my-company/organization-management/manage-unit/viewuser?unit=%s&user=%s",
						urlEncode(unit), urlEncode(user)),
				getMessageSource().getMessage("text.company.manage.units.viewcustomer.breadcrumb", new Object[] { user },
						"View Customer {0} ", getI18nService().getCurrentLocale()),
				null));
		model.addAttribute("saveUrl", String.format(request.getContextPath()
				+ "/my-company/organization-management/manage-unit/edituser?unit=%s&user=%s", urlEncode(unit), urlEncode(user)));
		model.addAttribute("cancelUrl", getCancelUrl(MANAGE_UNIT_DETAIL_URL, request.getContextPath(), unit));
		model.addAttribute("action", "manage.units");
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return url;
	}

	private void addCountryAndRegionToModel(final AddressForm addressForm, final Model model)
	{
		model.addAttribute("country", wileyCountryService.findCountryByCode(addressForm.getCountryIso()));
		if (addressForm.getCountryIso() != null)
		{
			model.addAttribute("regions", i18nFacade.getRegionsForCountryIso(addressForm.getCountryIso()));
		}
	}

	@ExceptionHandler(value = ExternalSystemException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ErrorData handleExternalSystemException(final ExternalSystemException e)
	{
		LOG.error("There was an error during call to external system", e);
		ErrorData error = new ErrorData();
		error.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
		error.setMessage(
				getMessageSource().getMessage("externalService.response.error", null, getI18nService().getCurrentLocale()));
		return error;
	}

	private String processFormErrors(final String unit, final AddressForm addressForm,
			final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("uid", unit);
		model.addAttribute("addressForm", addressForm);
		addCountryAndRegionToModel(addressForm, model);
		final B2BUnitData unitData = companyB2BCommerceFacade.getUnitForUid(unit);
		if (unitData != null)
		{
			model.addAttribute("unitName", unitData.getName());
		}
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_ADD_ADDRESS_PAGE;
	}

	private void addBreadcrumb(final String unit, final Model model, final Breadcrumb e)
	{
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unit);
		breadcrumbs.add(e);
		model.addAttribute("breadcrumbs", breadcrumbs);
	}

	private Optional<AddressData> getAddressDataFromUnit(final String addressId, final B2BUnitData unitData)
	{
		return unitData.getShippingAddresses()
				.stream()
				.filter(addressData ->
						addressData.getId() != null && addressData.getId().equals(addressId))
				.findFirst();
	}
}
