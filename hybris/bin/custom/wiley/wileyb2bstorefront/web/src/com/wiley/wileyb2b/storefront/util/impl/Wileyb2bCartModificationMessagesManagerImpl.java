package com.wiley.wileyb2b.storefront.util.impl;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.facades.order.data.CartModificationMessage;
import com.wiley.wileyb2b.storefront.util.Wileyb2bCartModificationMessagesManager;


/**
 * Default implementation of {@link Wileyb2bCartModificationMessagesManager}
 */
public class Wileyb2bCartModificationMessagesManagerImpl implements Wileyb2bCartModificationMessagesManager
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCartModificationMessagesManagerImpl.class);

	@Override
	public void addCartModificationMessagesToGlobalMessages(@Nonnull final List<CartModificationMessage> cartModificationMessages,
			@Nonnull final Model model)
	{
		Assert.notNull(cartModificationMessages);
		Assert.notNull(model);

		if (!cartModificationMessages.isEmpty())
		{
			cartModificationMessages
					.forEach(message -> addModificationMessageToGlobalMessages(message, model));
		}
	}

	@Override
	public void addCartModificationMessagesToGlobalMessages(@Nonnull final String message,
			@Nonnull final Optional<CartModificationMessageType> messageType,
			@Nonnull final RedirectAttributes redirectAttributes)
	{
		Assert.notNull(message);
		Assert.notNull(messageType);
		Assert.notNull(redirectAttributes);

		addToRedirectAttributes(redirectAttributes, messageType, message);
	}

	@Override
	public List<CartModificationData> addCartModificationMessagesToGlobalMessages(
			@Nonnull final List<CartModificationData> modifications, @Nonnull final RedirectAttributes redirectModel)
	{
		Assert.notNull(modifications);
		Assert.notNull(redirectModel);

		List<CartModificationData> filteredModifications = new ArrayList<>(modifications.size());
		for (CartModificationData modification : modifications)
		{
			String statusMessage = modification.getStatusMessage();
			if (org.apache.commons.lang.StringUtils.isNotEmpty(statusMessage))
			{
				addCartModificationMessagesToGlobalMessages(statusMessage,
						Optional.ofNullable(modification.getStatusMessageType()), redirectModel);
			}
			else
			{
				filteredModifications.add(modification);
			}
		}
		return filteredModifications;
	}

	private void addModificationMessageToGlobalMessages(final CartModificationMessage cartModificationMessage, final Model model)
	{
		CartModificationMessageType type = cartModificationMessage.getType();
		String message = cartModificationMessage.getMessage();

		if (StringUtils.isNotEmpty(message))
		{
			addToModel(model, Optional.ofNullable(type), message);
		}
	}

	private void addToModel(final Model model, final Optional<CartModificationMessageType> optionalType, final String message)
	{
		CartModificationMessageType type = optionalType.orElse(CartModificationMessageType.ERROR);
		switch (type)
		{
			case INFO:
				GlobalMessages.addInfoMessage(model, message);
				break;
			case CONF:
				GlobalMessages.addConfMessage(model, message);
				break;
			case ERROR:
				GlobalMessages.addErrorMessage(model, message);
				break;
			default:
				LOG.warn("Unknown cart modification message type [{}]. Using [error]", optionalType);
				GlobalMessages.addErrorMessage(model, message);
		}
	}

	private void addToRedirectAttributes(final RedirectAttributes redirectAttributes,
			final Optional<CartModificationMessageType> optionalType,
			final String message)
	{
		CartModificationMessageType type = optionalType.orElse(CartModificationMessageType.ERROR);
		switch (type)
		{
			case INFO:
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, message);
				break;
			case CONF:
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, message);
				break;
			case ERROR:
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, message);
				break;
			default:
				LOG.warn("Unknown cart modification message type [{}]. Using [error]", optionalType);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, message);
		}
	}
}
