package com.wiley.wileyb2b.storefront.forms.validation;

import com.wiley.wileyb2b.storefront.forms.Wileyb2bUpdateProfileForm;
import com.wiley.wileycom.storefrontcommons.validators.WileycomUpdateProfileFormValidator;


/**
 * Validates profile form for b2b customer
 */
public class Wileyb2bUpdateProfileFormValidator extends WileycomUpdateProfileFormValidator
{
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return Wileyb2bUpdateProfileForm.class.equals(aClass);
	}
}
