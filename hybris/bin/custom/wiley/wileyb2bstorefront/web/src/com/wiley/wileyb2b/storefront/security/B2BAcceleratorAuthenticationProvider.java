/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;


/**
 * Derived authentication provider supporting additional authentication checks. See
 * {@link de.hybris.platform.spring.security.RejectUserPreAuthenticationChecks}.
 *
 * <ul>
 * <li>prevent login without password for users created via CSCockpit</li>
 * <li>prevent login as user in group admingroup</li>
 * <li>prevent login as user if not authorised for B2B</li>
 * </ul>
 *
 * any login as admin disables SearchRestrictions and therefore no page can be viewed correctly
 */
public class B2BAcceleratorAuthenticationProvider extends CoreAuthenticationProvider
{
	private static final Logger LOG = Logger.getLogger(B2BAcceleratorAuthenticationProvider.class);

	private BruteForceAttackCounter bruteForceAttackCounter;
	private UserService userService;
	private ModelService modelService;
	private CartService cartService;

	private B2BUserGroupProvider b2bUserGroupProvider;


	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		final String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
		if (getBruteForceAttackCounter().isAttack(username))
		{
			try
			{
				final UserModel userModel = getUserService().getUserForUID(StringUtils.lowerCase(username));
				userModel.setLoginDisabled(true);
				getModelService().save(userModel);
				bruteForceAttackCounter.resetUserCounter(userModel.getUid());
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.warn("Brute force attack attempt for non existing user name " + username);
			}
			throw new BadCredentialsException(
					messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));

		}

		// check if the user of the cart matches the current user and if the
		// user is not anonymous. If otherwise, remove delete the session cart as it might
		// be stolen / from another user
		final CartModel sessionCart = getCartService().getSessionCart();
		final String sessionCartUserId = sessionCart.getUser().getUid();

		if (!username.equals(sessionCartUserId))
		{
			if (!sessionCartUserId.equals(userService.getAnonymousUser().getUid()))
			{
				// for switched user clear cart, ex. scenario user1 -> user2
				getCartService().setSessionCart(null);
			}
			else
			{
				// for login from anonymous user fill cart with mandatory unit attribute, ex. scenario anonymous -> user1
				setCartUnit(sessionCart, username);
				getCartService().setSessionCart(sessionCart);
			}
		}
		return doAuthentication(authentication);
	}

	protected Authentication doAuthentication(final Authentication authentication)
	{
		return super.authenticate(authentication);
	}


	private void setCartUnit(final CartModel cart, final String username)
	{
		try
		{
			UserModel customer = userService.getUserForUID(username);
			if (customer instanceof B2BCustomerModel)
			{
				cart.setUnit(((B2BCustomerModel) customer).getDefaultB2BUnit());
			}
		} catch (UnknownIdentifierException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("The user " + username + " does not exists.");
			}
		}
	}

	/**
	 * @see CoreAuthenticationProvider#additionalAuthenticationChecks(UserDetails,
	 * AbstractAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{
		super.additionalAuthenticationChecks(details, authentication);

		// Check if user has supplied no password
		if (StringUtils.isEmpty((String) authentication.getCredentials()))
		{
			throw new BadCredentialsException("Login without password");
		}

		final UserModel userModel = getUserService().getUserForUID(StringUtils.lowerCase(details.getUsername()));
		final UserGroupModel b2bgroup = getUserService().getUserGroupForUID(B2BConstants.B2BGROUP);
		// Check if the customer is B2B type
		if (getUserService().isMemberOfGroup(userModel, b2bgroup))
		{
			if (!getB2bUserGroupProvider().isUserAuthorized(details.getUsername()))
			{
				throw new InsufficientAuthenticationException(messages.getMessage("checkout.error.invalid.accountType",
						"You are not allowed to login"));
			}

			// if its a b2b user, check if it is active
			if (!getB2bUserGroupProvider().isUserEnabled(details.getUsername()))
			{
				throw new DisabledException("User " + details.getUsername() + " is disabled... "
						+ messages.getMessage("text.company.manage.units.disabled"));
			}
		}
		else
		{
			throw new LockedException("Login attempt without B2B role is rejected!");
		}
	}

	protected B2BUserGroupProvider getB2bUserGroupProvider()
	{
		return b2bUserGroupProvider;
	}

	public void setB2bUserGroupProvider(final B2BUserGroupProvider b2bUserGroupProvider)
	{
		this.b2bUserGroupProvider = b2bUserGroupProvider;
	}


	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
