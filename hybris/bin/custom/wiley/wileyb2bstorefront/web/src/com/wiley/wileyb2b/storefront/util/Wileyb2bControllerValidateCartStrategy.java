package com.wiley.wileyb2b.storefront.util;

import javax.annotation.Nonnull;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Strategy contains util methods to validate cart in controllers.
 */
public interface Wileyb2bControllerValidateCartStrategy
{

	/**
	 * Validates cart and put validation result into redirectModel.
	 *
	 * @param redirectModel
	 * @return
	 */
	boolean validateCart(@Nonnull final RedirectAttributes redirectModel);

}
