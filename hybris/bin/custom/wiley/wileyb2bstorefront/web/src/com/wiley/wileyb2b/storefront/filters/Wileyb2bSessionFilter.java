package com.wiley.wileyb2b.storefront.filters;

import de.hybris.platform.servicelayer.web.SessionFilter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.util.Assert;

import com.wiley.core.integration.ExternalCartModificationsStorageService;


/**
 * Wileyb2b specific session filter.
 */
public class Wileyb2bSessionFilter extends SessionFilter
{

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Override
	protected void activateSession(@Nonnull final HttpSession httpSession, @Nonnull final HttpServletRequest request)
	{
		Assert.notNull(httpSession);
		Assert.notNull(request);

		super.activateSession(httpSession, request);
		externalCartModificationsStorageService.enableStorageForCurrentSession();
	}
}
