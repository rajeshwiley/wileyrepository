package com.wiley.wileyb2b.storefront.controllers.pages;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.wileycom.storefrontcommons.controllers.AbstractUpdateCardDetailsController;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class B2BUpdateCardDetailsController extends AbstractUpdateCardDetailsController
{
	@Resource
	private WileyPaymentFacade wileyb2bPaymentFacade;

	@Override
	protected WileyPaymentFacade getPaymentFacade()
	{
		return wileyb2bPaymentFacade;
	}
}
