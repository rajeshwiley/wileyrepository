package com.wiley.wileyb2b.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;


/**
 * Represents profile form for b2b customer
 */
public class Wileyb2bUpdateProfileForm extends UpdateProfileForm
{
	private String loginId;
	private String email;
	private String middleName;
	private String suffixCode;

	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(final String loginId)
	{
		this.loginId = loginId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(final String middleName)
	{
		this.middleName = middleName;
	}

	public String getSuffixCode()
	{
		return suffixCode;
	}

	public void setSuffixCode(final String suffixCode)
	{
		this.suffixCode = suffixCode;
	}
}
