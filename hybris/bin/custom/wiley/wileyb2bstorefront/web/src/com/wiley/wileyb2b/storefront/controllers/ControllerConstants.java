/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers;

import de.hybris.platform.acceleratorcms.model.components.CartSuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.DynamicBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorcms.model.components.PurchasedCategorySuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SubCategoryListComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;


/**
 */
public interface ControllerConstants
{
	/**
	 * Class with action name constants
	 */
	interface Actions
	{
		interface Cms
		{
			String PREFIX = "/view/";
			String SUFFIX = "Controller";

			/**
			 * Default CMS component controller
			 */
			String DEFAULT_CMS_COMPONENT = PREFIX + "DefaultCMSComponentController";

			/**
			 * CMS components that have specific handlers
			 */
			String PURCHASED_CATEGORY_SUGGESTION_COMPONENT = PREFIX + PurchasedCategorySuggestionComponentModel._TYPECODE 
					+ SUFFIX;
			String CART_SUGGESTION_COMPONENT = PREFIX + CartSuggestionComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_REFERENCES_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX;
			String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_FEATURE_COMPONENT = PREFIX + ProductFeatureComponentModel._TYPECODE + SUFFIX;
			String CATEGORY_FEATURE_COMPONENT = PREFIX + CategoryFeatureComponentModel._TYPECODE + SUFFIX;
			String NAVIGATION_BAR_COMPONENT = PREFIX + NavigationBarComponentModel._TYPECODE + SUFFIX;
			String CMS_LINK_COMPONENT = PREFIX + CMSLinkComponentModel._TYPECODE + SUFFIX;
			String DYNAMIC_BANNER_COMPONENT = PREFIX + DynamicBannerComponentModel._TYPECODE + SUFFIX;
			String SUB_CATEGORY_LIST_COMPONENT = PREFIX + SubCategoryListComponentModel._TYPECODE + SUFFIX;
			String SIMPLE_RESPONSIVE_BANNER_COMPONENT = PREFIX + SimpleResponsiveBannerComponentModel._TYPECODE + SUFFIX;
		}
	}

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Cms
		{
			String COMPONENT_PREFIX = "cms/";
		}

		interface Pages
		{
			interface Account
			{
				String ACCOUNT_LOGIN_PAGE = "pages/account/accountLoginPage";
				String ACCOUNT_HOME_PAGE = "pages/account/accountHomePage";
				String ACCOUNT_ORDER_HISTORY_PAGE = "pages/account/accountOrderHistoryPage";
				String ACCOUNT_ORDER_PAGE = "pages/account/accountOrderPage";
				String ACCOUNT_PROFILE_PAGE = "pages/account/accountProfilePage";
				String ACCOUNT_PROFILE_EDIT_PAGE = "pages/account/accountProfileEditPage";
				String ACCOUNT_PROFILE_EMAIL_EDIT_PAGE = "pages/account/accountProfileEmailEditPage";
				String ACCOUNT_CHANGE_PASSWORD_PAGE = "pages/account/accountChangePasswordPage";
				String ACCOUNT_ADDRESS_BOOK_PAGE = "pages/account/accountAddressBookPage";
				String ACCOUNT_EDIT_ADDRESS_PAGE = "pages/account/accountEditAddressPage";
				String ACCOUNT_PAYMENT_INFO_PAGE = "pages/account/accountPaymentInfoPage";
				String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage";
				String ACCOUNT_PAYMENT_INFO_EDIT_BILLING_ADDRESS_PAGE = "pages/account/accountPaymentInfoEditBillingAddressPage";
			}

			interface Checkout
			{
				String CHECKOUT_REGISTER_PAGE = "pages/checkout/checkoutRegisterPage";
				String CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/checkoutConfirmationPage";
				String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLoginPage";
				String QUOTE_CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/quoteCheckoutConfirmationPage";
				String REPLENISHMENT_CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/replenishmentCheckoutConfirmationPage";
			}

			interface MultiStepCheckout
			{
				String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage";
				String GIFT_WRAP_PAGE = "pages/checkout/multi/giftWrapPage";
				String CHOOSE_PAYMENT_TYPE_PAGE = "pages/checkout/multi/choosePaymentTypePage";
				String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPage";
				String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage";
				String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
				String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
				String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
				String PAYMENT_OPTIONS_PAGE = "pages/checkout/multi/paymentOptions";
			}

			interface Password
			{
				String PASSWORD_RESET_CHANGE_PAGE = "pages/password/passwordResetChangePage";
				String PASSWORD_RESET_REQUEST = "pages/password/passwordResetRequestPage";
				String PASSWORD_RESET_REQUEST_CONFIRMATION = "pages/password/passwordResetRequestConfirmationPage";
			}

			interface Error
			{
				String ERROR_NOT_FOUND_PAGE = "pages/error/errorNotFoundPage";
			}

			interface Cart
			{
				String CART_PAGE = "pages/cart/cartPage";
			}

			interface StoreFinder
			{
				String STORE_FINDER_SEARCH_PAGE = "pages/storeFinder/storeFinderSearchPage";
				String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
				String STORE_FINDER_VIEW_MAP_PAGE = "pages/storeFinder/storeFinderViewMapPage";
			}

			interface Misc
			{
				String MISC_ROBOTS_PAGE = "pages/misc/miscRobotsPage";
				String MISC_SITE_MAP_PAGE = "pages/misc/miscSiteMapPage";
			}

			interface Guest
			{
				String GUEST_ORDER_PAGE = "pages/guest/guestOrderPage";
				String GUEST_ORDER_ERROR_PAGE = "pages/guest/guestOrderErrorPage";
			}

			interface Product
			{
				String WRITE_REVIEW = "pages/product/writeReview";
				String ORDER_FORM = "pages/product/productOrderFormPage";
			}

			interface MyCompany
			{
				String MY_COMPANY_LOGIN_PAGE = "pages/company/myCompanyLoginPage";
				String MY_COMPANY_MANAGE_UNIT_DETAILS_PAGE = "pages/company/myCompanyManageUnitDetailsPage";
				String MY_COMPANY_MANAGE_UNIT_USER_LIST_PAGE = "pages/company/myCompanyManageUnitUserListPage";
				String MY_COMPANY_MANAGE_UNIT_APPROVER_LIST_PAGE = "pages/company/myCompanyManageUnitApproversListPage";
				String MY_COMPANY_MANAGE_USER_DETAIL_PAGE = "pages/company/myCompanyManageUserDetailPage";
				String MY_COMPANY_MANAGE_USER_ADD_EDIT_FORM_PAGE = "pages/company/myCompanyManageUserAddEditFormPage";
				String MY_COMPANY_MANAGE_USERS_PAGE = "pages/company/myCompanyManageUsersPage";
				String MY_COMPANY_MANAGE_USER_DISBALE_CONFIRM_PAGE = "pages/company/myCompanyManageUserDisableConfirmPage";
				String MY_COMPANY_MANAGE_UNIT_ADD_ADDRESS_PAGE = "pages/company/myCompanyManageUnitAddAddressPage";
				String MY_COMPANY_MANAGE_USER_PERMISSIONS_PAGE = "pages/company/myCompanyManageUserPermissionsPage";
				String MY_COMPANY_MANAGE_USER_RESET_PASSWORD_PAGE = "pages/company/myCompanyManageUserPassword";
				String MY_COMPANY_MANAGE_USER_CUSTOMERS_PAGE = "pages/company/myCompanyManageUserCustomersPage";
				String MY_COMPANY_REMOVE_DISABLE_CONFIRMATION_PAGE = "pages/company/myCompanyRemoveDisableConfirmationPage";
				String MY_COMPANY_MANAGE_USER_B2B_USER_GROUPS_PAGE = "pages/company/myCompanyManageUserB2BUserGroupsPage";
				String MY_COMPANY_MANAGE_UNIT_DISBALE_CONFIRM_PAGE = "pages/company/myCompanyManageUnitDisableConfirmPage";
			}
		}

		interface Fragments
		{
			interface Account
			{
				String ACCOUNT_EMAIL_INVOICE_FORM = "fragments/account/accountEmailInvoiceForm";
			}
			interface Cart
			{
				String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
				String MINI_CART_PANEL = "fragments/cart/miniCartPanel";
				String MINI_CART_ERROR_PANEL = "fragments/cart/miniCartErrorPanel";
				String CART_POPUP = "fragments/cart/cartPopup";
				String EXPAND_GRID_IN_CART = "fragments/cart/expandGridInCart";
			}

			interface Checkout
			{
				String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm";
				String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm";
				String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup";
				String PO_NUMBERS_POPUP = "fragments/checkout/poNumbersPopup";
			}

			interface Password
			{
				String PASSWORD_RESET_REQUEST_POPUP = "fragments/password/passwordResetRequestPopup";
				String PASSWORD_RESET_REQUEST_FORM = "fragments/password/passwordResetRequestForm";
				String FORGOT_PASSWORD_VALIDATION_MESSAGE = "fragments/password/forgotPasswordValidationMessage";
			}

			interface Product
			{
				String FUTURE_STOCK_POPUP = "fragments/product/futureStockPopup";
				String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup";
				String ZOOM_IMAGES_POPUP = "fragments/product/zoomImagesPopup";
				String REVIEWS_TAB = "fragments/product/reviewsTab";
				String STORE_PICKUP_SEARCH_RESULTS = "fragments/product/storePickupSearchResults";
				String PRODUCT_LISTER = "fragments/product/productLister";
			}
		}
	}
}
