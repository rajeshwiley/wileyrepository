/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileyb2b.storefront.util.Wileyb2bCartModificationMessagesManager;
import com.wiley.wileycom.storefrontcommons.controllers.WileycomAddToCartController;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class AddToCartController extends WileycomAddToCartController
{
	private static final Logger LOG = LoggerFactory.getLogger(AddToCartController.class);

	@Resource
	private Wileyb2bCartModificationMessagesManager wileyb2bCartModificationMessagesManager;

	@RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
	public String addToCart(@RequestParam("productCodePost") final String code, @Valid final AddToCartForm form,
			final BindingResult bindingErrors, final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{

		return addProductToCart(code, form, bindingErrors, redirectAttributes, request);
	}

	@Override
	protected void addCartModificationMessages(final RedirectAttributes redirectAttributes, final long qty,
			final CartModificationData cartModification)
	{
		if (StringUtils.isNotEmpty(cartModification.getStatusMessage()))
		{
			wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(
					cartModification.getStatusMessage(), Optional.ofNullable(cartModification.getStatusMessageType()),
					redirectAttributes);
		}
		//nothing is displayed to end-uesr, if list of modification messages is empty
	}
}
