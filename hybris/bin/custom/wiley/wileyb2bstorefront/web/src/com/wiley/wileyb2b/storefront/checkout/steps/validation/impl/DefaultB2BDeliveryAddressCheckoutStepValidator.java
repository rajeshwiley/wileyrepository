/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.wileyb2b.storefront.checkout.steps.validation.AbstractB2BCheckoutStepValidator;


public class DefaultB2BDeliveryAddressCheckoutStepValidator extends AbstractB2BCheckoutStepValidator
{
	@Resource
	private Wileyb2bCheckoutFacade wileyb2bCheckoutFacade;

	@Override
	protected ValidationResults doValidateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final B2BPaymentTypeData checkoutPaymentType = wileyb2bCheckoutFacade.getCheckoutCart().getPaymentType();

		if (isNoPaymentTypeSelected(redirectAttributes, checkoutPaymentType))
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}

		if (wileyb2bCheckoutFacade.isDigitalSessionCart())
		{
			if (wileyb2bCheckoutFacade.isZeroTotalOrder())
			{
				return ValidationResults.REDIRECT_TO_SUMMARY;
			}
			else
			{
				return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
			}
		}

		return ValidationResults.SUCCESS;
	}

	private boolean isNoPaymentTypeSelected(final RedirectAttributes redirectAttributes,
			final B2BPaymentTypeData checkoutPaymentType)
	{
		if (!wileyb2bCheckoutFacade.isZeroTotalOrder() && checkoutPaymentType == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.paymentType.notprovided");
			return true;
		}
		return false;
	}
}
