package com.wiley.wileyb2b.storefront.util.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;
import com.wiley.wileyb2b.storefront.util.Wileyb2bCartModificationMessagesManager;
import com.wiley.wileyb2b.storefront.util.Wileyb2bControllerValidateCartStrategy;


/**
 * Default implementation of {@link Wileyb2bControllerValidateCartStrategy}
 */
public class Wileyb2bControllerValidateCartStrategyImpl implements Wileyb2bControllerValidateCartStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bControllerValidateCartStrategyImpl.class);

	@Resource
	private Wileyb2bCartFacade wileyb2bCartFacade;

	@Resource
	private Wileyb2bCartModificationMessagesManager wileyb2bCartModificationMessagesManager;

	@Override
	public boolean validateCart(@Nonnull final RedirectAttributes redirectModel)
	{
		Assert.notNull(redirectModel);

		//Validate the cart
		List<CartModificationData> modifications = new ArrayList<>();
		try
		{
			modifications = wileyb2bCartFacade.validateCartData();
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error("Failed to validate cart", e);
		}
		if (!modifications.isEmpty())
		{
			// it's needed to show cart modification messages in global messages
			List<CartModificationData> filteredModifications =
					wileyb2bCartModificationMessagesManager.addCartModificationMessagesToGlobalMessages(modifications,
							redirectModel);

			redirectModel.addFlashAttribute("validationData", filteredModifications);

			// Invalid cart. Bounce back to the cart page.
			return true;
		}
		return false;
	}
}
