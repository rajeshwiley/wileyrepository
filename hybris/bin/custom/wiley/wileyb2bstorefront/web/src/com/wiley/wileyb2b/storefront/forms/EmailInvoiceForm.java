package com.wiley.wileyb2b.storefront.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;


/**
 * Form for invoice emailing
 */
public class EmailInvoiceForm
{
	private String email;

	/**
	 * @return the email
	 */
	@NotNull(message = "{emailInvoice.email.invalid}")
	@Size(min = 1, max = 255, message = "{emailInvoice.email.invalid}")
	@Email(message = "{emailInvoice.email.invalid}")
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 * 		the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}


}
