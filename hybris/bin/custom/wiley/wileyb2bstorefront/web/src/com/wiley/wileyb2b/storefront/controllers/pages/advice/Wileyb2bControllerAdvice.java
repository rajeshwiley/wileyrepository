package com.wiley.wileyb2b.storefront.controllers.pages.advice;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Conventions;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.wiley.core.wileyb2b.order.exceptions.Wileyb2bCartCalculationSystemException;
import com.wiley.wileycom.storefrontcommons.advice.WileycomControllerAdvice;


/**
 * Controller advice for wileyb2b
 */
@ControllerAdvice
public class Wileyb2bControllerAdvice extends WileycomControllerAdvice
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bControllerAdvice.class);

	@ExceptionHandler(Wileyb2bCartCalculationSystemException.class)
	public String wileyb2bCartCalculationSystemExceptionHandler(final Wileyb2bCartCalculationSystemException exception,
			final HttpServletRequest request)
	{
		LOG.warn("Cart calculation exception occurred : {}", exception.getMessage());

		final FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(request);

		GlobalMessages.addFlashMessage(new ExceptionHandlerRedirectAttributes(outputFlashMap),
				GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.calculationException");

		String targetUrl = getReferer(request);
		if (StringUtils.isEmpty(targetUrl) || shouldUseFallbackStrategy(request))
		{
			targetUrl = "/cart";
		}
		return AbstractController.REDIRECT_PREFIX + targetUrl;
	}

	private String getReferer(final HttpServletRequest request)
	{
		return request.getHeader("referer");
	}

	// this is needed to avoid cyclic redirects
	private boolean shouldUseFallbackStrategy(final HttpServletRequest request)
	{
		final String referer = getReferer(request);
		final String requestURL = request.getRequestURL().toString();
		return requestURL.equals(referer); // cyclic redirect
	}

	/**
	 * Used to add global messages to FlashMap during exception handling via @ExceptionHandler annotation.<br/>
	 */
	private static class ExceptionHandlerRedirectAttributes implements RedirectAttributes
	{

		private FlashMap flashMap;

		ExceptionHandlerRedirectAttributes(final FlashMap flashMap)
		{
			this.flashMap = flashMap;
		}

		@Override
		public Map<String, ?> getFlashAttributes()
		{
			return flashMap;
		}

		@Override
		public RedirectAttributes addFlashAttribute(final Object o)
		{
			flashMap.put(Conventions.getVariableName(o), o);
			return this;
		}

		@Override
		public boolean containsAttribute(final String s)
		{
			return flashMap.containsKey(s);
		}

		@Override
		public RedirectAttributes addFlashAttribute(final String s, final Object o)
		{
			flashMap.put(s, o);
			return this;
		}

		@Override
		public RedirectAttributes mergeAttributes(final Map<String, ?> map)
		{
			flashMap.putAll(map);
			return this;
		}

		@Override
		public Map<String, Object> asMap()
		{
			return flashMap;
		}

		@Override
		public RedirectAttributes addAttribute(final String s, final Object o)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public RedirectAttributes addAttribute(final Object o)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public RedirectAttributes addAllAttributes(final Collection<?> collection)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Model addAllAttributes(final Map<String, ?> map)
		{
			throw new UnsupportedOperationException();
		}
	}

}
