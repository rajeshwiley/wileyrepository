/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2b.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.enums.B2BPeriodRange;
import de.hybris.platform.b2b.enums.B2BPermissionTypeEnum;
import de.hybris.platform.b2bacceleratorfacades.company.CompanyB2BCommerceFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPermissionData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPermissionTypeData;
import de.hybris.platform.b2bapprovalprocessfacades.company.B2BApproverFacade;
import de.hybris.platform.b2bapprovalprocessfacades.company.B2BPermissionFacade;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.b2bcommercefacades.company.B2BUserFacade;
import de.hybris.platform.b2bcommercefacades.company.data.B2BSelectionData;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.i18n.FormatFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationConflictingIdException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;
import com.wiley.facades.wileyb2b.customer.Wileyb2bCustomerFacade;
import com.wiley.wileyb2b.storefront.breadcrumb.impl.MyCompanyBreadcrumbBuilder;
import com.wiley.wileyb2b.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2b.storefront.forms.forms.B2BPermissionForm;
import com.wiley.wileyb2b.storefront.forms.forms.Wileyb2bCustomerForm;
import com.wiley.wileyb2b.storefront.forms.forms.validation.B2BPermissionFormValidator;


/**
 * Controller for organization management.
 */
@Controller
@Scope("tenant")
public class MyCompanyPageController extends AbstractSearchPageController
{
	protected static final String MY_COMPANY_CMS_PAGE = "my-company";
	protected static final String ORGANIZATION_MANAGEMENT_CMS_PAGE = "organizationManagement";
	protected static final String MANAGE_UNITS_CMS_PAGE = "manageUnits";
	protected static final String MANAGE_UNIT_DETAIL_URL = "/my-company/organization-management/manage-unit/";
	protected static final String MANAGE_USER_DETAILS_URL = "/my-company/organization-management/manage-users/details?user=%s";
	protected static final String REDIRECT_TO_USER_DETAILS = REDIRECT_PREFIX + MANAGE_USER_DETAILS_URL;
	protected static final String REDIRECT_TO_UNIT_DETAIL = REDIRECT_PREFIX + MANAGE_UNIT_DETAIL_URL;
	private static final Logger LOG = Logger.getLogger(MyCompanyPageController.class);
	protected static final String SINGLE_WHITEPSACE = " ";
	protected static final String MANAGE_UNITS_BASE_URL = "/my-company/organization-management/manage-unit";

	@Resource(name = "checkoutFacade")
	protected CheckoutFacade checkoutFacade;

	@Resource(name = "b2bCustomerFacade")
	protected Wileyb2bCustomerFacade customerFacade;

	@Resource(name = "b2bCommerceFacade")
	protected CompanyB2BCommerceFacade companyB2BCommerceFacade;

	@Resource(name = "wileyb2bTransactionalUserFacade")
	protected B2BUserFacade b2bUserFacade;

	@Resource(name = "b2bUnitFacade")
	protected B2BUnitFacade b2bUnitFacade;

	@Resource
	protected B2BApproverFacade b2bApproverFacade;

	@Resource(name = "b2bPermissionFacade")
	protected B2BPermissionFacade b2bPermissionFacade;

	@Resource(name = "myCompanyBreadcrumbBuilder")
	protected MyCompanyBreadcrumbBuilder myCompanyBreadcrumbBuilder;

	@Resource(name = "b2BPermissionFormValidator")
	protected B2BPermissionFormValidator b2BPermissionFormValidator;

	@Resource(name = "formatFactory")
	protected FormatFactory formatFactory;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	protected UserFacade getUserFacade()
	{
		return userFacade;
	}


	@InitBinder
	protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder)
	{
		final DateFormat dateFormat = new SimpleDateFormat(getMessageSource().getMessage("text.store.dateformat", null,
				getI18nService().getCurrentLocale()));
		final CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@ModelAttribute("b2bStore")
	public String getCurrentB2BStore()
	{
		return companyB2BCommerceFacade.getCurrentStore();
	}

	@RequestMapping(value = "/my-company", method = RequestMethod.GET)
	@RequireHardLogIn
	public String myCompany(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_COMPANY_CMS_PAGE));
		model.addAttribute("breadcrumbs", myCompanyBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute("unitUid", companyB2BCommerceFacade.getParentUnit().getUid());
		model.addAttribute("userUid", customerFacade.getCurrentCustomer().getUid());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/my-company/organization-management", method = RequestMethod.GET)
	@RequireHardLogIn
	public String organizationManagement(final Model model) throws CMSItemNotFoundException
	{
		return myCompany(model);
	}

	protected String unitDetails(final B2BUnitData unitData, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MANAGE_UNITS_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUnitsDetailsBreadcrumbs(unitData.getUid());
		model.addAttribute("breadcrumbs", breadcrumbs);

		if (!unitData.isActive())
		{
			GlobalMessages.addInfoMessage(model, "b2bunit.disabled.infomsg");
		}

		model.addAttribute("unit", unitData);
		model.addAttribute("user", customerFacade.getCurrentCustomer());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_UNIT_DETAILS_PAGE;
	}


	protected String createUser(final Model model) throws CMSItemNotFoundException
	{
		if (!model.containsAttribute("b2BCustomerForm"))
		{
			final Wileyb2bCustomerForm b2bCustomerForm = new Wileyb2bCustomerForm();
			b2bCustomerForm.setParentB2BUnit(companyB2BCommerceFacade.getParentUnit().getUid());

			// Add the b2bcustomergroup role by default
			b2bCustomerForm.setRoles(Collections.singletonList("b2bcustomergroup"));

			model.addAttribute("b2BCustomerForm", b2bCustomerForm);
		}
		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", customerFacade.getNameSuffixes());
		model.addAttribute("roles", populateRolesCheckBoxes(companyB2BCommerceFacade.getUserGroups()));

		final B2BUnitData unitData = companyB2BCommerceFacade.getParentUnit();
		model.addAttribute("parentUnit", unitData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-company/organization-management/manage-users/create", getMessageSource().getMessage(
				"text.company.organizationManagement.createuser", null, getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("/my-company/organization-management/manage-user", getMessageSource().getMessage(
				"text.company.manageUsers", null, getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_USER_ADD_EDIT_FORM_PAGE;
	}

	protected String createUser(final Wileyb2bCustomerForm b2BCustomerForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{

		if (bindingResult.hasErrors())
		{
			return processFormError(b2BCustomerForm, model, "form.global.error");
		}

		final CustomerData b2bCustomerData = new CustomerData();
		b2bCustomerData.setTitleCode(b2BCustomerForm.getTitleCode());
		b2bCustomerData.setFirstName(b2BCustomerForm.getFirstName());
		b2bCustomerData.setMiddleName(b2BCustomerForm.getMiddleName());
		b2bCustomerData.setLastName(b2BCustomerForm.getLastName());
		b2bCustomerData.setEmail(b2BCustomerForm.getEmail());
		b2bCustomerData.setDisplayUid(b2BCustomerForm.getEmail());
		b2bCustomerData.setUnit(companyB2BCommerceFacade.getParentUnit());
		b2bCustomerData.setRoles(b2BCustomerForm.getRoles());
		b2bCustomerData.setSuffixCode(b2BCustomerForm.getSuffixCode());
		model.addAttribute("b2BCustomerForm", b2BCustomerForm);
		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", customerFacade.getNameSuffixes());
		model.addAttribute("roles", populateRolesCheckBoxes(companyB2BCommerceFacade.getUserGroups()));

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-company/organization-management/", getMessageSource().getMessage(
				"text.company.organizationManagement", null, getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("/my-company/organization-management/manage-user", getMessageSource().getMessage(
				"text.company.manageUsers", null, getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);

		final B2BUnitData unitData = companyB2BCommerceFacade.getParentUnit();
		model.addAttribute("parentUnit", unitData);

		try
		{
			b2bUserFacade.updateCustomer(b2bCustomerData);
			b2bCustomerData.setUid(b2BCustomerForm.getEmail().toLowerCase());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.confirmation.user.added");
		}
		catch (final WileycomCustomerRegistrationConflictingIdException e)
		{
			bindingResult.rejectValue("email", "text.manageuser.error.email.exists.title");
			return processFormError(b2BCustomerForm, model, "form.global.error");
		}
		catch (ExternalSystemException | WileycomCustomerRegistrationFailureException e)
		{
			return processFormError(b2BCustomerForm, model, "form.global.error.tryLater");
		}
		return String.format(REDIRECT_TO_USER_DETAILS, urlEncode(b2bCustomerData.getUid()));
	}

	public String editUser(final String user, final Model model) throws CMSItemNotFoundException
	{
		if (!model.containsAttribute("b2BCustomerForm"))
		{
			final CustomerData customerData = companyB2BCommerceFacade.getCustomerForUid(user);
			final Wileyb2bCustomerForm b2bCustomerForm = new Wileyb2bCustomerForm();
			b2bCustomerForm.setUid(customerData.getUid());
			b2bCustomerForm.setTitleCode(customerData.getTitleCode());
			b2bCustomerForm.setFirstName(customerData.getFirstName());
			b2bCustomerForm.setLastName(customerData.getLastName());
			b2bCustomerForm.setMiddleName(customerData.getMiddleName());
			b2bCustomerForm.setEmail(customerData.getDisplayUid());
			b2bCustomerForm.setParentB2BUnit(b2bUserFacade.getParentUnitForCustomer(customerData.getUid()).getUid());
			b2bCustomerForm.setActive(customerData.isActive());
			b2bCustomerForm.setApproverGroups(customerData.getApproverGroups());
			b2bCustomerForm.setApprovers(customerData.getApprovers());
			b2bCustomerForm.setRoles(customerData.getRoles());
			b2bCustomerForm.setSuffixCode(customerData.getSuffixCode());
			model.addAttribute("b2BCustomerForm", b2bCustomerForm);
		}

		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", customerFacade.getNameSuffixes());
		model.addAttribute("roles", populateRolesCheckBoxes(companyB2BCommerceFacade.getUserGroups()));

		final B2BUnitData unitData = companyB2BCommerceFacade.getParentUnit();
		model.addAttribute("parentUnit", unitData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUserDetailsBreadcrumb(user);
		breadcrumbs.add(new Breadcrumb(String.format("/my-company/organization-management/manage-users/edit?user=%s",
				urlEncode(user)), getMessageSource().getMessage("text.company.manageusers.edit", new Object[]
				{ user }, "Edit {0} User", getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_USER_ADD_EDIT_FORM_PAGE;
	}

	protected String editUser(final String user, final Wileyb2bCustomerForm b2BCustomerForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			return processFormError(b2BCustomerForm, model, "form.global.error");
		}

		// A B2B Admin should not be able to downgrade their roles, they must at lest belong to B2B Administrator role
		if (customerFacade.getCurrentCustomer().getUid().equals(b2BCustomerForm.getUid()))
		{
			final Collection<String> roles =
					b2BCustomerForm.getRoles() != null ? b2BCustomerForm.getRoles() : new ArrayList<String>();
			if (!roles.contains(B2BConstants.B2BADMINGROUP))
			{
				GlobalMessages.addErrorMessage(model, "form.b2bcustomer.adminrole.error");
				roles.add(B2BConstants.B2BADMINGROUP);
				b2BCustomerForm.setRoles(roles);
				model.addAttribute("b2BCustomerForm", b2BCustomerForm);
				return editUser(b2BCustomerForm.getUid(), model);
			}
			else
			{
				// A session user can't modify their own parent unit.
				final B2BUnitData parentUnit = companyB2BCommerceFacade.getParentUnit();
				if (!parentUnit.getUid().equals(b2BCustomerForm.getParentB2BUnit()))
				{
					GlobalMessages.addErrorMessage(model, "form.b2bcustomer.parentunit.error");
					b2BCustomerForm.setParentB2BUnit(parentUnit.getUid());
					model.addAttribute("b2BCustomerForm", b2BCustomerForm);
					return editUser(b2BCustomerForm.getUid(), model);
				}
			}
		}

		final CustomerData b2bCustomerData = convertToCustomerData(b2BCustomerForm);
		model.addAttribute("b2BCustomerForm", b2BCustomerForm);
		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", customerFacade.getNameSuffixes());
		model.addAttribute("roles", populateRolesCheckBoxes(companyB2BCommerceFacade.getUserGroups()));

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		model.addAttribute("breadcrumbs", myCompanyBreadcrumbBuilder.createManageUserDetailsBreadcrumb(user));

		try
		{
			b2bUserFacade.updateCustomer(b2bCustomerData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "text.confirmation.user.edited");
		}
		catch (final ExternalSystemException e)
		{
			return processFormError(b2BCustomerForm, model, "form.global.error.tryLater");
		}
		return String.format(REDIRECT_TO_USER_DETAILS, urlEncode(b2bCustomerData.getEmail().toLowerCase()));
	}

	private CustomerData convertToCustomerData(final Wileyb2bCustomerForm b2BCustomerForm)
	{
		final CustomerData b2bCustomerData = new CustomerData();
		b2bCustomerData.setUid(b2BCustomerForm.getUid());
		b2bCustomerData.setTitleCode(b2BCustomerForm.getTitleCode());
		b2bCustomerData.setFirstName(b2BCustomerForm.getFirstName());
		b2bCustomerData.setMiddleName(b2BCustomerForm.getMiddleName());
		b2bCustomerData.setLastName(b2BCustomerForm.getLastName());
		b2bCustomerData.setEmail(b2BCustomerForm.getEmail());
		b2bCustomerData.setDisplayUid(b2BCustomerForm.getEmail());
		b2bCustomerData.setUnit(companyB2BCommerceFacade.getParentUnit());
		b2bCustomerData
				.setRoles(b2BCustomerForm.getRoles() != null ? b2BCustomerForm.getRoles() : Collections.emptyList());
		b2bCustomerData.setSuffixCode(b2BCustomerForm.getSuffixCode());
		return b2bCustomerData;
	}

	protected String manageUserDetail(final String user, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		final CustomerData customerData = companyB2BCommerceFacade.getCustomerForUid(user);
		model.addAttribute("customerData", customerData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORGANIZATION_MANAGEMENT_CMS_PAGE));
		final List<Breadcrumb> breadcrumbs = myCompanyBreadcrumbBuilder.createManageUserDetailsBreadcrumb(user);
		model.addAttribute("breadcrumbs", breadcrumbs);

		if (!customerData.getUnit().isActive())
		{
			GlobalMessages.addInfoMessage(model, "text.parentunit.disabled.warning");
		}
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return ControllerConstants.Views.Pages.MyCompany.MY_COMPANY_MANAGE_USER_DETAIL_PAGE;
	}


	/**
	 * Data class used to hold a drop down select option value. Holds the code identifier as well as the display name.
	 */
	public static class SelectOption
	{
		private final String code;
		private final String name;

		public SelectOption(final String code, final String name)
		{
			this.code = code;
			this.name = name;
		}

		public String getCode()
		{
			return code;
		}

		public String getName()
		{
			return name;
		}
	}

	protected B2BPermissionData populateB2BPermissionDataFromForm(final B2BPermissionForm b2BPermissionForm) throws ParseException
	{
		final B2BPermissionData b2BPermissionData = new B2BPermissionData();
		b2BPermissionData.setOriginalCode(b2BPermissionForm.getOriginalCode());
		final String permissionCode = b2BPermissionForm.getCode();
		if (StringUtils.isNotEmpty(permissionCode))
		{
			b2BPermissionData.setCode(permissionCode);
		}
		else
		{
			b2BPermissionData.setCode(assignPermissionName(b2BPermissionForm));
		}
		final B2BPermissionTypeData b2BPermissionTypeData = b2BPermissionForm.getB2BPermissionTypeData();
		b2BPermissionData.setB2BPermissionTypeData(b2BPermissionTypeData);
		final CurrencyData currencyData = new CurrencyData();
		currencyData.setIsocode(b2BPermissionForm.getCurrency());
		b2BPermissionData.setCurrency(currencyData);

		b2BPermissionData.setUnit(companyB2BCommerceFacade.getUnitForUid(b2BPermissionForm.getParentUnitName()));
		final String permissionTimespan = b2BPermissionForm.getTimeSpan();
		if (StringUtils.isNotEmpty(permissionTimespan))
		{
			b2BPermissionData.setPeriodRange(B2BPeriodRange.valueOf(b2BPermissionForm.getTimeSpan()));
		}
		final String monetaryValue = b2BPermissionForm.getValue();
		if (StringUtils.isNotEmpty(monetaryValue))
		{
			b2BPermissionData.setValue(Double.valueOf(formatFactory.createNumberFormat().parse(monetaryValue).doubleValue()));
		}
		return b2BPermissionData;
	}

	protected String assignPermissionName(final B2BPermissionForm b2BPermissionForm)
	{
		final StringBuilder permissionCode = new StringBuilder();
		final String permissionType = b2BPermissionForm.getB2BPermissionTypeData().getCode();

		if (!B2BPermissionTypeEnum.B2BBUDGETEXCEEDEDPERMISSION.equals(B2BPermissionTypeEnum.valueOf(permissionType)))
		{
			final String currency = b2BPermissionForm.getCurrency();
			permissionCode.append((StringUtils.isNotEmpty(currency) ? currency : ""));
			permissionCode.append(SINGLE_WHITEPSACE);
			permissionCode.append(b2BPermissionForm.getValue());
			permissionCode.append(SINGLE_WHITEPSACE);
			permissionCode.append(getMessageSource().getMessage("text.company.managePermissions.assignName.per", null,
					getI18nService().getCurrentLocale()));
			permissionCode.append(SINGLE_WHITEPSACE);
			final String timespan = b2BPermissionForm.getTimeSpan();
			permissionCode.append((StringUtils.isNotEmpty(timespan) ? timespan : getMessageSource().getMessage(
					"text.company.managePermissions.assignName.order", null, getI18nService().getCurrentLocale())));
		}
		else
		{
			permissionCode.append(getMessageSource().getMessage("text.company.managePermissions.assignName.budget", null,
					getI18nService().getCurrentLocale()));
			permissionCode.append(SINGLE_WHITEPSACE);
			permissionCode.append(b2BPermissionForm.getParentUnitName());
		}

		b2BPermissionForm.setCode(permissionCode.toString());
		return permissionCode.toString();
	}


	protected List<SelectOption> populateSelectBoxForString(final List<String> listOfDatas)
	{
		final List<SelectOption> selectBoxList = new ArrayList<>();
		for (final String data : listOfDatas)
		{
			selectBoxList.add(new SelectOption(data, data));
		}

		return selectBoxList;
	}

	protected List<SelectOption> populateRolesCheckBoxes(final List<String> roles)
	{
		final List<SelectOption> selectBoxList = new ArrayList<>();
		for (final String data : roles)
		{
			selectBoxList.add(new SelectOption(data, getMessageSource().getMessage(String.format("b2busergroup.%s.name", data),
					null, getI18nService().getCurrentLocale())));
		}

		return selectBoxList;
	}



	protected String urlEncode(final String url)
	{
		try
		{
			if (url != null)
			{
				return URLEncoder.encode(url, "UTF-8");
			}
			else
			{
				throw new IllegalArgumentException("Url cannot be null");
			}
		}
		catch (final UnsupportedEncodingException e)
		{
			return url;
		}

	}

	protected B2BSelectionData populateDisplayNamesForRoles(final B2BSelectionData b2BSelectionData)
	{
		final List<String> roles = b2BSelectionData.getRoles();
		final List<String> displayRoles = new ArrayList<>(roles.size());
		for (final String role : roles)
		{
			displayRoles.add(getMessageSource().getMessage("b2busergroup." + role + ".name", null, role,
					getI18nService().getCurrentLocale()));
		}
		b2BSelectionData.setDisplayRoles(displayRoles);
		return b2BSelectionData;
	}

	protected String getCancelUrl(final String url, final String contextPath, final String param)
	{
		return String.format(contextPath + url, urlEncode(param));
	}

	private String processFormError(final Wileyb2bCustomerForm b2BCustomerForm,
			final Model model, final String errorMessageCode) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, errorMessageCode);
		model.addAttribute("b2BCustomerForm", b2BCustomerForm);
		return editUser(b2BCustomerForm.getUid(), model);
	}
}
