<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="wileycomProduct" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/product"%>

<div class="product-list product-list-cart">
    <c:forEach items="${cartData.entries}" var="entry">
        <c:url value="${entry.product.url}" var="productUrl"/>

        <div class="product-list-item">
            <div class="product-list-image">
				<div class="thumb">
					<a href="${productUrl}">
						<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
					</a>
				</div>
			</div>
            <div class="row product-list-info">
                <div class="col-xs-10 col-sm-9">
                    <div id="productNumber_${entry.entryNumber}" class="product-list-title">
                        <a href="${productUrl}">
                            ${entry.product.name}
                        </a>
                    </div>
					<div class="product-list-description">
                        <div class="product-list-author">
                            <c:out value="${entry.product.authors}"/>
                        </div>
                        <div class="product-list-date">
                            <fmt:formatDate pattern="MMMM, yyyy" value="${entry.product.dateImprint}"/>
                        </div>
                        <div class="product-list-status">
                            <c:set var="entryStock" value="${entry.product.stock.stockLevelStatus.code}"/>

                            <c:forEach items="${entry.product.baseOptions}" var="option">
                                <c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
                                    <c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
                                        <c:set var="entryStock" value="${option.selected.stock.stockLevelStatus.code}"/>
                                    </c:forEach>
                                </c:if>
                            </c:forEach>

                            <wileycomProduct:CartItemInventoryStatusBlock entry="${entry}" showReleaseDate="true"/>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-sm-3">
                    <div class="product-item-buttons">
                        <c:if test="${entry.updateable}">
                            <ycommerce:testId code="cart_product_removeProduct">
                                <button class="button button-remove remove-item remove-entry-button" aria-label="remove" aria-describedby="productNumber_${entry.entryNumber}"
                                        id="removeEntry_${entry.entryNumber}">
                                </button>
                            </ycommerce:testId>
                        </c:if>
                    </div>
                </div>
                <div class="product-item-price">
                    <div class="col-xs-12 col-sm-5">
                        <div class="item-price">
                            <spring:theme code="basket.item.price" text="Item Price"/>
                            :
                            <format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="item-qty">
                            <c:url value="/cart/update" var="cartUpdateFormAction"/>
                            <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post"
                                       commandName="updateQuantityForm${entry.entryNumber}">
                                <input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
                                <input type="hidden" name="productCode" value="${entry.product.code}"/>
                                <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                <ycommerce:testId code="cart_product_quantity">
                                <div class="qty-label">
                                    <form:label cssClass="" path="quantity" for="quantity_${entry.entryNumber}">
                                        <spring:theme code="basket.page.qty"/>:
                                    </form:label>
                                </div>
                                    <c:set value="${entry.updateable}" var="entryUpdatable"/>
                                    <c:set value="${entry.doesProductHaveQuantity}" var="quantityEditable"/>
                                    <c:set value="update-entry-quantity-input item-qty-input" var="quantityFieldCssStyle"/>
                                    <c:set value="quantity_${entry.entryNumber}" var="quantityFieldId"/>
                                    <c:choose>
                                        <c:when test="${entryUpdatable and not quantityEditable}">
                                            <!-- This block is necessary to resolve issue when product cannot be removed if input field is disabled because quantity is not editable -->
                                            <div class="qty-input">
                                                <input type="hidden" value="${entry.quantity}" name="quantity">
                                                <input id="${quantityFieldId}" class="${quantityFieldCssStyle}" type="text"
                                                       value="${entry.quantity}" disabled="disabled" aria-label="quantity">
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <form:input cssClass="${quantityFieldCssStyle}" disabled="${not entryUpdatable}"
                                                        type="text" size="1" id="${quantityFieldId}" path="quantity" aria-label="quantity" maxlength="3"/>
                                        </c:otherwise>
                                    </c:choose>
                                </ycommerce:testId>
                            </form:form>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <div class="item-total-price">
                            <ycommerce:testId code="cart_totalProductPrice_label">
                                <format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
                            </ycommerce:testId>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </c:forEach>
</div>

<storepickup:pickupStorePopup/>
