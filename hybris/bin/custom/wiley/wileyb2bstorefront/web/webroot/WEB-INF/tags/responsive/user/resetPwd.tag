<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:theme code="updatePwd.pwd.invalid" var="errorMessageVar"/>
<spring:theme code="form.global.error" var="globalErrorMessageVar"/>
<div class="container account-reset-password">
	<div col="row">
		<div col="col-sm-12">
			<div class="site-form central-form">
					<div class="form-cell heading">
						<h1><spring:theme code="text.account.profile.resetPassword"/></h1>
						<p><b><spring:theme code="text.account.profile.resetPasswordDetails"/></b></p>
					</div>
					
					<form:form method="post" commandName="wileycomUpdatePasswordForm">
					<div class="form-cell thin">
							<formElement:formPasswordBox idKey="updatePwd.pwd" labelKey="updatePwd.pwd" path="pwd"
								mandatory="true" errorMessage="${errorMessageVar}"
								globalErrorMessage="${globalErrorMessageVar}"/>
					</div>
					<div class="form-cell">
							<formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd" path="checkPwd" inputCSS="form-control" mandatory="true" />
					</div>
					<div class="form-cell">
						<button type="submit" class="button button-main large" id="resetPassword">
						<spring:theme code="text.account.profile.resetPasswordButton" text="Reset Password" />
					</button>
					</div>
					</form:form>
			</div>
		</div>
	</div>
</div>