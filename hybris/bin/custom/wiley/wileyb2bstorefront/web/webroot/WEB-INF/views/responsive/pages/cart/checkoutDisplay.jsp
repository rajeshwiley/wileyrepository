<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
<div class="order-button-group">
	<ycommerce:testId code="checkoutButton">
			<button class="button button-main large checkoutButton" data-checkout-url="${checkoutUrl}"><spring:theme code="basket.proceed.checkout" text="Proceed to Checkout"/></button>
	</ycommerce:testId>
</div>