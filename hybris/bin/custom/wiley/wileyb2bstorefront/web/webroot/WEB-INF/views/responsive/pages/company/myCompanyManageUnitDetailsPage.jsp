<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Collections" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<spring:url value="/my-company/organization-management/manage-unit/add-address" var="addUnitAddressUrl">
	<spring:param name="unit" value="${unit.uid}"/>
</spring:url>

<c:set var="escapedUnitUid" value="${fn:escapeXml(unit.uid)}"/>
<c:set var="escapedUnitName" value="${fn:escapeXml(unit.name)}"/>

<template:page pageTitle="${pageTitle}">

	<div class="container my-company business-units-view-details wrap">
		<div id="globalMessages">
			<common:globalMessages/>
		</div>
		<div class="row">
			<company:myCompanyNav selected="units"/>
			<div class="col-xs-12 col-sm-9">
				<h1 class="hidden-xs">
					${empty escapedUnitName ? escapedUnitUid : escapedUnitName}
				</h1>
				<div class="my-company-wrap">
					<div class="annotation wrap">
						<div class="row">
							<div class="col-xs-12 col-sm-9">
								<div class="annotation-text">
									<spring:theme code="text.company.manage.units.subtitle" text=""/>
								</div>
							</div>
						</div>
					</div>

					<div class="unit-summary wrap">
						<div class="row">
							<div class="col-xs-12 col-sm-9">
								<div class="unit-summary-item">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<ycommerce:testId code="Unit_UnitDetailsPage_UnitID">
												<spring:theme code="text.company.unit.id" text="Business Unit ID: "/>
											</ycommerce:testId>
										</div>
										<div class="col-xs-12 col-sm-6">
											<ycommerce:testId code="Unit_UnitDetailsPage_UnitID">${escapedUnitUid}</ycommerce:testId>
										</div>
									</div>
								</div>

								<div class="unit-summary-item">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<spring:theme code="text.company.unit.name" text="Business Unit Name: "/>
										</div>
										<div class="col-xs-12 col-sm-6">
												${escapedUnitName}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="unit-list">
                    	<div class="unit-item">
                        	<h2><spring:theme code="text.company.unit.representative" text="Your Wiley Rep"/></h2>
                            <div class="unit-item-table">
                            	<div class="thead hidden-xs">
									<div class="row">
										<div class="col-sm-2"><spring:theme code="text.company.unit.representative.name" text="Name"/></div>
										<div class="col-sm-2"><spring:theme code="text.company.unit.representative.phone" text="Phone"/></div>
										<div class="col-sm-2"><spring:theme code="text.company.unit.representative.email" text="Email"/></div>
									</div>
                            	</div>
                                <div class="tbody">
                                	<div class="unit-item-row">
										<div class="row">
                                        	<div class="col-xs-12 col-sm-2">
                                                <div class="thead visible-xs">Name</div>
                                                <div class="td">${unit.sapSalesRepContactName}</div>
                                              </div>
                                            <div class="col-xs-12 col-sm-2">
                                            	<div class="thead visible-xs">Phone</div>
                                            	<div class="td">${unit.sapSalesRepContactPhone}</div>
                                            </div>
                                            <div class="col-xs-12 col-sm-2">
                                            	<div class="thead visible-xs">Email</div>
                                            	<div class="td">${unit.sapSalesRepContactEmail}</div>
                                            </div>
										</div>
									</div>
								</div>
                        	</div>
                    	</div>

						<company:addressTable unit="${unit}" addresses="${billingAddressSingletonList}" type="billing"/>

						<company:addressTable unit="${unit}" addresses="${unit.shippingAddresses}" type="shipping" showActions="true"/>

						<spring:url value="/my-company/organization-management/manage-unit/administrators" var="editAdministratorUrl">
							<spring:param name="unit" value="${unit.uid}"/>
							<spring:param name="role" value="b2badmingroup"/>
						</spring:url>
						<spring:url
								value="/my-company/organization-management/manage-unit/createuser"
								var="createUserUrl">
							<spring:param name="unit" value="${unit.uid}"/>
						</spring:url>
						<spring:url
								value="/my-company/organization-management/manage-unit/managers"
								var="editManagersUrl">
							<spring:param name="unit" value="${unit.uid}"/>
							<spring:param name="role" value="b2bmanagergroup"/>
						</spring:url>
						<spring:url
								value="/my-company/organization-management/manage-unit/customers"
								var="editCustomersUrl">
							<spring:param name="unit" value="${unit.uid}"/>
							<spring:param name="role" value="b2bcustomergroup"/>
						</spring:url>


						<company:userList users="${unit.administrators}" action="administrators" role="b2badmingroup"
										  editUrl="${editAdministratorUrl}"
										  createUrl="${createUserUrl}" subtitleKey="text.company.manage.units.view.administrators.subtitle"/>

						<company:userList users="${unit.managers}" action="managers" role="b2bmanagergroup"
										  editUrl="${editManagersUrl}"
										  createUrl="${createUserUrl}" subtitleKey="text.company.manage.units.view.managers.subtitle"/>

						<company:userList users="${unit.customers}" action="customers" role="b2bcustomergroup"
										  editUrl="${editCustomersUrl}"
										  createUrl="${createUserUrl}" subtitleKey="text.company.manage.units.view.customers.subtitle"/>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 visible-xs">
						<h1>
							${empty escapedUnitName ? escapedUnitUid : escapedUnitName}
						</h1>
					</div>
				</div>

			</div>
		</div>
	</div>


</template:page>
