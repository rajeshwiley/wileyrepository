<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multiCheckout"
    tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
    tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="sec"
    uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ taglib prefix="addressCommon" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/address" %>
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />

<c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
<c:url value="/checkout/multi/payment-method/add" var="addAnotherCardUrl"/>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-payment-billing-address">

<jsp:attribute name="pageScripts">
    <script type="text/javascript">
        $(document).ready(function() {
            wiley.collapseCheckoutBillingAddressForm();
        });

        wiley.collapseCheckoutBillingAddressForm = function() {
            var sameAsBillingCheckBox = $("#sameAsBilling");

            if(sameAsBillingCheckBox.length) {
                sameAsBillingCheckBox.change(function() {
                toggleSameAsShippingCheckbox();
            });

            if (!sameAsBillingCheckBox.prop("checked")) {
                toggleSameAsShippingCheckbox();
            }
        }
        };

        function toggleSameAsShippingCheckbox(){
            $('.checkout-add-credit-card-form.collapse').collapse('toggle');
            $('.shipping-address.collapse').collapse('toggle');
        }
    </script>
</jsp:attribute>

<jsp:body>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            <h1 class="page-title icon-lock"><spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" /></h1>
        </div>
    </div>
    <div class="row">
        <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}"
            progressBarId="${progressBarId}">
            <jsp:body>
                <div class="checkout-accordion-content-wrap">
                    <ycommerce:testId code="checkoutStepThree">
                             <div class="checkout-payment-buttons">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <c:if test="${not empty paymentInfos}">
                                            <button type="button"
                                                class="button button-main large" data-toggle="modal"
                                                data-target="#savedCardModelWnd">
                                                <spring:theme
                                                    code="checkout.multi.paymentMethod.addPaymentDetails.useSavedCard" />
                                            </button>
                                        </c:if>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <!--  this cms content slot is for Paypal express checkout -->
                                        <cms:pageSlot position="TopContent" var="feature" element="div">
                                            <cms:component component="${feature}" />
                                        </cms:pageSlot>
                                    </div>
                                </div>
                             </div>

                        <div class="checkout-payment-billing-address">
                            <div class="checkout-content-subtitle">
                                <spring:theme code="checkout.multi.paymentMethod.billingaddress.title" />
                            </div>
                            <div class="row">
								<c:if test="${not isDigitalSessionCart}">
									<div class="col-xs-12  col-sm-7">
										<div class="checkbox-component">
											<input type="checkbox" id="sameAsBilling" ${isBillingSameAsShipping ? 'checked' : ''}>
											<label for="sameAsBilling" >
												<spring:theme code="checkout.multi.paymentMethod.sameAsShippingAddress" />
											</label>
										</div>
										<div id="shipping-as-billing-address-div" class="shipping-address ${isDigitalSessionCart || not isBillingSameAsShipping ? 'collapse' : 'collapse in'}">
											<c:if test="${not empty deliveryAddress}">
												<p>
													${fn:escapeXml(deliveryAddress.title)}&nbsp;${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
													<br>
													<c:if
														test="${ not empty deliveryAddress.line1 }">
														${fn:escapeXml(deliveryAddress.line1)},&nbsp;
													</c:if>
													<c:if
														test="${ not empty deliveryAddress.line2 }">
														${fn:escapeXml(deliveryAddress.line2)},&nbsp;
													</c:if>
													<c:if
														test="${not empty deliveryAddress.town }">
														${fn:escapeXml(deliveryAddress.town)},&nbsp;
													</c:if>
													<c:if
														test="${ not empty deliveryAddress.region.name }">
														${fn:escapeXml(deliveryAddress.region.name)},&nbsp;
													</c:if>
													<c:if
														test="${ not empty deliveryAddress.postalCode }">
														${fn:escapeXml(deliveryAddress.postalCode)},&nbsp;
													</c:if>
													<c:if
														test="${ not empty deliveryAddress.country.name }">
														${fn:escapeXml(deliveryAddress.country.name)}
													</c:if>
												</p>
											</c:if>
										</div>
									</div>
                                </c:if>
                                <div class="col-xs-12 col-sm-5">
									<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
										<div class="checkbox-component save-to-address-book">
											<input type="checkbox" id="savePaymentId">
											<label for="savePaymentId"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.savePaymentInfo" /></label>
										</div>
									</sec:authorize>    
                                </div>
                            </div>
                        </div>
                        <div id="new-billing-address-div" class="checkout-add-credit-card-form ${isDigitalSessionCart || not isBillingSameAsShipping ? 'collapse in' : 'collapse'}">
                            <addressCommon:billingAddressFormSelector supportedCountries="${supportedCountries}" regions="${regions}" country="${country}"/>
                        </div>
                        <div class="checkout-accordion-button-next">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="order-summary-info-link">
                                        <a href="javascript:void(0);">
                                            <c:if test="${cartData.hasOutOfStockItems}">
                                                <spring:theme code="checkout.multi.paymentMethod.preBackOrderAlertMessage" />&nbsp;
                                            </c:if>
                                            <spring:theme code="checkout.multi.paymentMethod.seeOrderSummaryForMoreInformation" />
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 text-right">
                                    <button class="button button-secondary large add-another-card">
                                        <spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.addAnotherCreditCard" />
                                    </button>
                                    <div class="checkout-payment-credit-cards">
                                        <div class="credit-card"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <c:if test="${not empty paymentInfos}">
                            <div id="savedCardModelWnd" role="dialog"
                                class="modal fade modalWindow">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="row">
                                                <div class="col-xs-10 col-sm-10">
                                                    <div class="modal-title">
                                                        <spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.savedCards" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 col-sm-2">
                                                    <button type="button"
                                                        data-dismiss="modal" aria-label="Close" class="close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                            <div class="modal-wnd-list">
                                                <c:forEach items="${paymentInfos}"
                                                    var="paymentInfo" varStatus="status">
                                                    <div ${!status.last ? 'class="modal-wnd-list-item"' : 'class="modal-wnd-list-item-last"'}>
                                                        <div class="row">
                                                            <form action="${request.contextPath}/checkout/multi/payment-method/choose" method="GET">
                                                                <div class="col-xs-12 col-sm-8">
                                                                    <input type="hidden" name="selectedPaymentMethodId" value="${paymentInfo.id}" />
                                                                    <div class="modal-wnd-list-title">
                                                                        <p>${fn:escapeXml(paymentInfo.billingAddress.firstName)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.lastName)}</p>
                                                                    </div>
                                                                    <div class="modal-wnd-list-description">
                                                                        <p>${fn:escapeXml(paymentInfo.cardType)}</p>
                                                                        <p>${fn:escapeXml(paymentInfo.accountHolderName)}
                                                                        <p>************${fn:escapeXml(paymentInfo.cardNumber)}</p>
                                                                        <p>
                                                                            <spring:theme code="checkout.multi.paymentMethod.paymentDetails.expires" arguments="${fn:escapeXml(paymentInfo.expiryMonth)},${fn:escapeXml(paymentInfo.expiryYear)}" />
                                                                        </p>
                                                                        <p>${fn:escapeXml(paymentInfo.billingAddress.line1)}</p>
                                                                        <p>${fn:escapeXml(paymentInfo.billingAddress.town)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.region.isocodeShort)}</p>
                                                                        <p>${fn:escapeXml(paymentInfo.billingAddress.postalCode)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.country.isocode)}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="modal-wnd-item-button">
                                                                        <button type="submit" class="button button-main small" aria-label="Submit">
                                                                            <spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails" />
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </ycommerce:testId>
                </div>
            </jsp:body>
        </multiCheckout:checkoutSteps>

        <%--<multiCheckout:checkoutOrderDetails cartData="${cartData}" />--%>
        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <div class="order-brief-section checkout-desktop">
                <multiCheckout:orderSummarySection cartData="${cartData}" user="${user}" />
                <multiCheckout:checkoutOrderDetails cartData="${cartData}" showTax="true" />
            </div>
        </div>
    </div>
</jsp:body>
</template:page>