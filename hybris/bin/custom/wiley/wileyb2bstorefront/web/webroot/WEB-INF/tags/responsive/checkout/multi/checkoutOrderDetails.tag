<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showShipDeliveryEntries" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showPickupDeliveryEntries" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="multi-checkout-b2b" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>

<c:if test="${not empty cartData}">
	<div id="checkoutOrderDetails" class="span-9 last order-brief-section">
	        <div class="order-total-section">
				<multi-checkout-b2b:orderTotals cartData="${cartData}" showTaxEstimate="false" showTax="${showTax}"/>
				<c:if test="${showPlaceOrderButton}">
					
					<form:form action="${placeOrderUrl}" id="placeOrderForm2" commandName="placeOrderForm">
						<div class="order-terms-and-conds checkbox-component">
							<form:checkbox id="termsConds_mob" path="termsCheck"/>
							<spring:message code="checkout.summary.placeOrder.readTermsAndConditions.popup.link" var="termsAndConditionsPopupLink"/>
							<label for="termsConds_mob">
								<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" text="Terms and Conditions"/>
								&nbsp;<a class="termsAndConditionsLink" title="${termsAndConditionsPopupLink}"
									href="${getTermsAndConditionsUrl}">${termsAndConditionsPopupLink}</a>
							</label>
						</div>
						<button class="button button-main large"><spring:theme code="checkout.summary.placeOrder"/></button>
					</form:form>
				</c:if>
            </div>
			<cart:cartPromotions cartData="${cartData}"/>
			<c:forEach items="${cartData.pickupOrderGroups}" var="groupData" varStatus="status">
					<multi-checkout:pickupCartItems cartData="${cartData}" groupData="${groupData}" index="${status.index}" showHead="true" />
			</c:forEach>	
	</div>
</c:if>
