<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="b2b-multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>

<c:set var="isAccountPaymentType" value="${not empty cartData.paymentType && cartData.paymentType.code eq 'ACCOUNT'}"/>
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-shipping-information">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
			<h1 class="page-title icon-lock"><spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" /></h1>
		</div>
	</div>

    <div class="row">
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<multi-checkout:shipmentItemsSingleLine cartData="${cartData}" showDeliveryAddress="false" />
				<%--<multi-checkout:checkoutProgressBar steps="${checkoutSteps}" progressBarId="${progressBarId}"/>--%>
				<%--<div class="span-14 append-1">
                    <div id="checkoutContentPanel" class="clearfix">--%>

				<%--<div class="headline"><spring:theme code="checkout.multi.addressDetails" text="Address Details"/></div>
                <div class="required right"><spring:theme code="form.required" text="Fields marked * are required"/></div>
                <div class="description"><spring:theme code="checkout.multi.addEditform" text="Please use this form to add/edit an address."/></div>--%>
				<div class="checkout-accordion-content-wrap">
					<c:if test="${not empty deliveryAddresses}">
						<div class="checkout-shipping-address">
							<div class="checkout-content-subtitle"><spring:theme code="checkout.summary.shippingAddress" text="Shipping Address"></spring:theme></div>
							<div class="checkout-view-address-book">
								<button type="button" class="button button-main large"
										data-toggle="modal" data-target="#addressBookModelWnd">
									<spring:theme
											code="checkout.checkout.multi.deliveryAddress.viewAddressBook"
											text="View Address Book" />
								</button>
							</div>
						</div>
					</c:if>


					<c:if test="${empty addressFormEnabled or addressFormEnabled}">
						<div class="checkout-add-address-form checkout-shipping">
							<div class="checkout-indent">
								<div class="site-form">
									<div class="row">
										<div class="col-xs-12 col-sm-8">
											<address:addressFormSelector supportedCountries="${countries}"
																		 regions="${regions}"
																		 cancelUrl="${currentStepUrl}"
																		 country="${country}"/>
										</div>
									</div>
								</div>
								<address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select" />
							</div>
							<multi-checkout:pickupGroups cartData="${cartData}" />
						</div>
					

						<div class="checkout-accordion-button-next">
							<button id="addressSubmit" type="button"
								class="checkout-next button button-main large"><spring:theme code="checkout.multi.deliveryAddress.continue" text="Next Step"/></button>
						</div>
					</c:if>
					
				</div>

				<c:if test="${not empty deliveryAddresses}">
					<div id="addressBookModelWnd" role="dialog" class="modal fade modalWindow">
						<multi-checkout:selectAddressModal></multi-checkout:selectAddressModal>
					</div>
				</c:if>
				<%--</div>--%>
				<address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select"/>
				<%--</div>--%>
			</jsp:body>

		</multi-checkout:checkoutSteps>

		<div class="col-xs-12 col-sm-4 col-sm-offset-1">
			<div class="order-brief-section checkout-desktop">
				<b2b-multi-checkout:orderSummarySection cartData="${cartData}" user="${user}" />
				<b2b-multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="true"/>
			</div>
		</div>
	</div>

</template:page>
