<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="image-gallery">
	<div class="carousel">
		<div id="pdp-carousel-sync1" class="owl-carousel">
			<c:choose>
				<c:when test="${galleryImages == null || galleryImages.size() == 0}">
					<div class="item">
						<spring:theme code="img.missingProductImage.responsive.product" text="/" var="imagePath"/>
						<c:choose>
							<c:when test="${originalContextPath ne null}">
								<c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
							</c:when>
							<c:otherwise>
								<c:url value="${imagePath}" var="imageUrl" />
							</c:otherwise>
						</c:choose>
						<div class="item-image"><img src="${imageUrl}" alt=""></div>
					</div>
				</c:when>
				<c:otherwise>
					<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
						<div class="item">
							<div class="item-image"><img src="${container.zoom.url}" alt=""></div>
						</div>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
		<div id="pdp-carousel-sync2" class="owl-carousel">
			<c:choose>
				<c:when test="${galleryImages == null || galleryImages.size() == 0}">
					<div class="item">
						<spring:theme code="img.missingProductImage.responsive.product" text="/" var="imagePath"/>
						<c:choose>
							<c:when test="${originalContextPath ne null}">
								<c:url value="${imagePath}" var="imageUrl" context="${originalContextPath}"/>
							</c:when>
							<c:otherwise>
								<c:url value="${imagePath}" var="imageUrl" />
							</c:otherwise>
						</c:choose>
						<div class="item-image"><img src="${imageUrl}" alt=""></div>
					</div>
				</c:when>
				<c:otherwise>
					<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
						<div class="item">
							<div class="item-image"><img src="${container.product.url}" alt=""></div>
						</div>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
