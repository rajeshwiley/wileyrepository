<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${order.paymentType.code eq 'CARD'}">
	<div class="headline"><spring:theme code="text.paymentDetails" text="Payment Details"/></div>
	<ul>
		<li>${fn:escapeXml(order.paymentInfo.cardNumber)}</li>
		<li>${fn:escapeXml(order.paymentInfo.cardTypeData.name)}</li>
		<li><spring:theme code="paymentMethod.paymentDetails.expires" arguments="${fn:escapeXml(order.paymentInfo.expiryMonth)},${fn:escapeXml(order.paymentInfo.expiryYear)}"/></li>
	</ul>
</c:if>

<c:if test="${order.paymentType.code eq 'ACCOUNT'}">
	<div class="headline"><spring:theme code="text.paymentDetails" text="Payment Details"/></div>
	<ul>
		<li>${fn:escapeXml(order.b2bCustomerData.displayUid)}</li>
		<li>${fn:escapeXml(order.costCenter.name)}</li>
	</ul>
</c:if>
