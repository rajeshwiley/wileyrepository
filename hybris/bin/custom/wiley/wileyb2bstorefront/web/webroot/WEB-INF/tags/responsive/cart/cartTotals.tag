<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:url value="/cart/removeVoucher" var="cartRemoveVoucher"/>
<spring:url value="/cart/applyVoucher" var="cartApplyVoucher"/>

<div class="order-total-list">
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-8 col-sm-8">
				<spring:theme code="basket.page.totals.subtotal"/>
			</div>
			<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.subTotal}"/></div>
		</div>
	</div>
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-8 col-sm-8"><spring:theme code="text.account.order.shipping" text="Shipping:"/></div>
			<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.deliveryCost}" /></div>
		</div>
	</div>
	<c:if test="${showTax}">
		<div class="order-total-item">
		  	<div class="row">
				<div class="col-xs-8 col-sm-8">${sessionCountry.taxName}:</div>
				<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.totalTax}"/></div>
			</div>
		</div>
	</c:if>
	<c:if test="${cartData.totalDiscounts.value > 0}">
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-8 col-sm-8"><spring:theme code="basket.order.discount" text="Order Discount"/></div>
			<div class="col-xs-4 col-sm-4 text-right price">-<format:price priceData="${cartData.totalDiscounts}"/></div>
		</div>
	</div>
	</c:if>
	<c:if test="${not empty cartData.discountData}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-4 col-sm-4"><spring:theme code="text.voucher.discountCode" text="Discount Code"/>:</div>
				<div class="col-xs-8 col-sm-8 text-right price">${cartData.discountData[0].code}</div>
			</div>
		</div>
	</c:if>
</div>
<div class="order-promotion-code">
	<div class="promotion-field">
		<div class="site-form">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<spring:theme var="promotionPlaceholder" code='checkout.summary.promotion.message' />
					<form:form method="post" action="${cartApplyVoucher}" commandName="voucherForm">
						<div class="form-field input-group">
							<formElement:formInputBox idKey="applyVoucher.discountCode" path="discountCode" placeholder="${promotionPlaceholder}" labelKey="" ariaLabel="${promotionPlaceholder}"/>
							<div class="input-group-btn">
								<button type="submit" id="applyVoucher" class="button button-main large"><spring:theme code="basket.apply" text="Apply"/></button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<c:forEach items="${cartData.discountData}" var="discountData">
						<form:form action="${cartRemoveVoucher}" commandName="voucherForm" method="post">
							<form:hidden path="discountCode" value="${discountData.code}"/>
							<div class="row promo-message">
								<div class="col-xs-12 col-sm-8">
									<c:set var="discountCode"><c:out value="${discountData.code}"/></c:set>
									<c:set var="discountOff">
										<c:choose>
											<c:when test="${not empty discountData.price.formattedValue}">
												<c:out value="${discountData.price.formattedValue}"/>
											</c:when>
											<c:otherwise>
												<fmt:formatNumber var="discountOffInt" maxFractionDigits="0" value="${discountData.relativeValue}" />
												<spring:theme code="basket.discountOffText" arguments="${discountOffInt}"/>
											</c:otherwise>
										</c:choose>
									</c:set>
									<spring:theme code="basket.discountCode.appliedMessage"
										arguments="${discountCode},${discountOff},<span>,</span>" htmlEscape="false"/>
								</div>
								<div class="col-xs-12 col-sm-4">
									<button class="button button-main small"><spring:theme code="basket.remove" text="Remove"/></button>
								</div>
							</div>
						</form:form>
					</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="order-total-info order-total-title">
		<div class="row">
			<div class="col-xs-6 col-sm-6">
				<spring:theme code="text.account.order.orderTotals" />
			</div>
			<div class="col-xs-6 col-sm-6 text-right">
				<c:choose>
					<c:when test="${showTax}">
						<format:price priceData="${cartData.totalPriceWithTax}" />
					</c:when>
					<c:otherwise>
						<format:price priceData="${cartData.totalPrice}" />
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
