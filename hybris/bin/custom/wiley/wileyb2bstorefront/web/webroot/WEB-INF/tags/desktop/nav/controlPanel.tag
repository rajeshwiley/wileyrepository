<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="top" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowAll" required="true" type="java.lang.Boolean" %>
<%@ attribute name="supportShowPaged" required="true" type="java.lang.Boolean" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="numberPagesShown" required="false" type="java.lang.Integer" %>
<%@ attribute name="sortQueryParams" required="false" %>

<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/desktop/nav/pagination" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}" />

<c:if test="${searchPageData.pagination.totalNumberOfResults > 0}">
	<div class="control-panel ${(top) ? 'top': ''}">
		<div class="row">
			<div class="col-xs-4 col-sm-8">
				<div class="total-results">
					<ycommerce:testId code="searchResults_productsFound_label">
						<div class="totalResults">
							<spring:theme code="${themeMsgKey}.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}" />
						</div>
					</ycommerce:testId>
				</div>
			</div>
			<div class="col-xs-8 col-sm-4 text-right">
				<div clas="row">
					<div class="col-xs-12">
						<c:if test="${supportShowAll}">
							<spring:url value="${searchUrl}" var="showAllUrl" htmlEscape="true">
								<spring:param name="show" value="All" />
							</spring:url>
							<ycommerce:testId code="searchResults_showAll_link">
								<a class="show-hide visible-xs showAll" href="${showAllUrl}">
									<spring:theme code="${themeMsgKey}.showAllResults" />
								</a>
							</ycommerce:testId>
						</c:if>
						<c:if test="${supportShowPaged}">
							<spring:url value="${searchUrl}" var="showPageUrl" htmlEscape="true">
								<spring:param name="show" value="Page" />
							</spring:url>
							<ycommerce:testId code="searchResults_showPage_link">
								<a class="show-hide visible-xs showPagination" href="${showPageUrl}">
									<spring:theme code="${themeMsgKey}.showPageResults" />
								</a>
							</ycommerce:testId>
						</c:if>
					</div>
					<div clas="col-xs-12">
						<c:if test="${(searchPageData.pagination.numberOfPages > 1)}">
							<c:if test="${(searchPageData.pagination.numberOfPages > 1)}">
								<pagination:controlPanelPagination searchUrl="${searchUrl}" searchPageData="${searchPageData}" numberPagesShown="${numberPagesShown}" themeMsgKey="${themeMsgKey}" />
							</c:if>
						</c:if>
						<c:if test="${supportShowAll}">
							<spring:url value="${searchUrl}" var="showAllUrl" htmlEscape="true">
								<spring:param name="show" value="All" />
							</spring:url>
							<ycommerce:testId code="searchResults_showAll_link">
								<a class="show-hide hidden-xs showAll" href="${showAllUrl}">
									<spring:theme code="${themeMsgKey}.showAllResults" />
								</a>
							</ycommerce:testId>
						</c:if>
						<c:if test="${supportShowPaged}">
							<spring:url value="${searchUrl}" var="showPageUrl" htmlEscape="true">
								<spring:param name="show" value="Page" />
							</spring:url>
							<ycommerce:testId code="searchResults_showPage_link">
								<a class="show-hide hidden-xs showPagination" href="${showPageUrl}">
									<spring:theme code="${themeMsgKey}.showPageResults" />
								</a>
							</ycommerce:testId>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
