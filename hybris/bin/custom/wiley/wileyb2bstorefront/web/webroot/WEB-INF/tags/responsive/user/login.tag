<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>


<div class="site-form central-form">
	<form:form action="${action}" method="post" commandName="loginForm">
		<div class="form-cell heading">
			<h1><spring:theme code="login.title" /></h1>
		</div>

		<c:if test="${not empty message}">
			<span class="has-error">
				<spring:theme code="${message}" />
			</span>
		</c:if>

		<formElement:formInputBox idKey="j_username" labelKey="login.email" path="j_username"
			formFieldClass="form-field" mandatory="true" />

		<c:if test="${param.error}">
			<div class="form-cell has-error">
				<div class="help-block">
					<spring:theme code="login.loginFailureCauseUser" />
				</div>
			</div>
		</c:if>

		<formElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password" mandatory="true" />

		<c:if test="${param.error}">
			<div class="form-cell has-error">
				<div class="help-block">
					<spring:theme code="login.loginFailureCausePassword" />
				</div>
			</div>
		</c:if>

		<div class="form-cell" id="forgotPasswordLink">
			<div class="form-field">
				<a href="<c:url value='/login/pw/request'/>" class="js-password-forgotten"
						data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
					<spring:theme code="login.link.forgottenPwd" />
				</a>
			</div>
		</div>

		<button type="submit" class="button button-main large">
			<spring:theme code="${actionNameKey}" />
		</button>

		<c:if test="${expressCheckoutAllowed}">
			<button type="submit" class="btn btn-default btn-block expressCheckoutButton"><spring:theme code="text.expresscheckout.header" text="Express Checkout"/></button>
			<input id="expressCheckoutCheckbox" style="display:none" name="expressCheckoutEnabled" type="checkbox" class="form left doExpressCheckout" />
		</c:if>
	</form:form>
</div>
<div id="formForgotPassword"></div>

