<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<%@ attribute name="navNodes" required="true" type="java.util.List"%>

<c:forEach items="${navNodes}"
	var="topLevelNode">
	<ul class="dropdown-items">
		<c:forEach items="${topLevelNode.children}"
			var="secondLevelNode">
			<c:forEach items="${secondLevelNode.entries}" var="entry">
				<li><cms:component component="${entry.item}"
						evaluateRestriction="true" /></li>
			</c:forEach>
		</c:forEach>
	</ul>
</c:forEach>