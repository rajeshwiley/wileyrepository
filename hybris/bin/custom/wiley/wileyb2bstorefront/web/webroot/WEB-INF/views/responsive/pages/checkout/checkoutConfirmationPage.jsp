<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="b2b-order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="multi-checkout-b2b" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<%@ taglib prefix="multi-checkout-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>

<c:set var="hasShippedItems" value="${orderData.deliveryItemsQuantity > 0}" />
<c:url value="/my-account/orders" var="myaccounturl"></c:url>
<spring:theme code="checkout.shipping.methods.desired.shipping.date.format" text="MM/dd/YYYY" var="dateFormat" />
<template:page pageTitle="${pageTitle}" containerCSSClass="checkout-order-confirmation b2b wrap">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	

        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="sub-title-block">
              	<h2 class="icon-tick-green"><spring:theme code="checkout.orderConfirmation.thankYou"/></h2>
	            <p><spring:theme code="checkout.orderConfirmation.orderNumber" arguments="${orderData.code}"/></p>
	            <p><spring:theme code="checkout.orderConfirmation.copySentTo" arguments="${email}"/></p>
	            <multi-checkout:supportMessage />
            </div>
          </div>
        </div>
        	
        <div class="row">
          <div class="col-xs-12 col-sm-6 left-block">
            <div class="order-brief-section checkout-desktop wrap">
              <div class="print-section checkout-mobile"><a href="#" class="icon-print"></a></div>
              <c:if test="${hasShippedItems}">
				  <div class="order-brief-top">
					<div class="order-brief-top-item">
					  <div class="row">
						<div class="col-xs-12 col-sm-4 order-brief-top-title"><spring:theme code="checkout.pickup.items.to.be.shipped" text="Ship To:"/></div>
						<div class="col-xs-12 col-sm-8">
							<p class="user-name">${fn:escapeXml(deliveryAddress.title)}&nbsp;${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}</p>
							<p>
								<c:if test="${ not empty deliveryAddress.line1 }">
									${fn:escapeXml(deliveryAddress.line1)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.line2 }">
									${fn:escapeXml(deliveryAddress.line2)},&nbsp;
								</c:if>
								<c:if test="${not empty deliveryAddress.town }">
									${fn:escapeXml(deliveryAddress.town)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.region.name }">
									${fn:escapeXml(deliveryAddress.region.name)}
								</c:if>
							</p>
							<p>
								<c:if test="${ not empty deliveryAddress.postalCode }">
									${fn:escapeXml(deliveryAddress.postalCode)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.country.name }">
									${fn:escapeXml(deliveryAddress.country.name)}
								</c:if>
							</p>
						</div>
					  </div>
					</div>


					 <c:if test="${not empty orderData.orderMessage}">
					  <div class="order-brief-top-item">
					   <div class="row">
						<div class="col-xs-12 col-sm-4 order-brief-top-title"><spring:theme code="checkout.multi.order.summary.delivery.orderMessage"/></div>
						<div class="col-xs-12 col-sm-8">${orderData.orderMessage}</div>
					   </div>
					  </div>
					 </c:if>
					<c:if test="${not empty orderData.desiredShippingDate}">
						<fmt:formatDate pattern="${dateFormat}" value="${orderData.desiredShippingDate}" var="desiredShippingDate" />
						<div class="order-brief-top-item">
							<div class="row">
								<div class="col-xs-12 col-sm-4 order-brief-top-title"><spring:theme code="checkout.multi.order.summary.shipping.date"/></div>
								<div class="col-xs-12 col-sm-8">${desiredShippingDate}</div>
							</div>
						</div>
					</c:if>

					<c:if test="${not empty orderData.deliveryMode}">
						<div class="order-brief-top-item">
							<div class="row">
								<div class="col-xs-12 col-sm-4 order-brief-top-title"><spring:theme code="checkout.multi.order.summary.delivery.method"/></div>
								<div class="col-xs-12 col-sm-8">${orderData.deliveryMode.name}</div>
							</div>
						</div>
					</c:if>
             	</div>
             </c:if>
              <div class="product-list ${hasShippedItems ? '' : 'order-brief-top'}">
				<c:forEach items="${orderData.entries}" var="entry">
					<c:if test="${entry.deliveryPointOfService == null}">
						<c:url value="${entry.product.url}" var="productUrl"/>
						<div class="product-list-item">
				          <div class="row product-list-info">
				            <div class="col-xs-12 col-sm-9">
				              <div class="product-list-image"><a href="${productUrl}"><product:productPrimaryImage product="${entry.product}" format="thumbnail"/></a></div>
				              <div class="product-list-title"><a href="${productUrl}">${entry.product.name}</a></div>
				              <div class="product-list-description">
				                <div class="product-list-qty">
				                	<spring:theme code="basket.page.qty"/>:&nbsp;${entry.quantity}
				                	<c:if test="${not empty entry.poNumber}">
					                	<span class="product-list-qty">
					                		<spring:theme code="checkout.multi.order.po.number" arguments="${entry.poNumber}"/>
					                	</span>
				                	</c:if>
				                </div>
				              </div>
				            </div>
				            <div class="col-xs-12 col-sm-3">
				              <div class="product-item-price"><format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></div>
				            </div>
				          </div>
				        </div>
			        </c:if>
				</c:forEach>
		    </div>
              
             <div class="payment-type">
				<div class="row">
					<div class="col-xs-2 col-sm-2">
						<div class="payment-type-title"><spring:theme code="checkout.multi.payment" text="Payment:"></spring:theme></div>
					</div>
					<div class="col-xs-10 col-sm-10">
						<div class="payment-type-info">
							<c:choose>
								<c:when test="${orderData.paymentType.code eq 'CARD'}">
									<c:if test="${!empty paymentInfo.cardTypeData}">${paymentInfo.cardTypeData.name}, </c:if>
									<c:if test="${!empty paymentInfo.cardNumber}">************${paymentInfo.cardNumber}, </c:if>
									<c:if test="${!empty paymentInfo.expiryYear}">${paymentInfo.expiryMonth}/${paymentInfo.expiryYear}, </c:if>

									<multi-checkout-b2b:deliveryAddress address="${paymentInfo.billingAddress}" />
								</c:when>
								<c:when test="${orderData.paymentType.code eq 'ACCOUNT'}">
									<c:if test="${!empty orderData.paymentType}"> ${orderData.paymentType.displayName}, </c:if>
									<c:if test="${!empty user.unit}"><multi-checkout-b2b:deliveryAddress address="${user.unit.billingAddress}" /></c:if>
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
              
              <div class="order-total-section">
				<div class="order-total-list">
					<div class="order-total-title"><spring:theme code="order.order.totals"/></div>
					<div class="order-total-item">
						<div class="row">
							<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.subtotal"/></div>
							<div class="col-xs-6 col-sm-6 text-right price">
								<ycommerce:testId code="Order_Totals_Subtotal">
									<format:price priceData="${orderData.subTotal}"/>
								</ycommerce:testId>
							</div>
						</div>
					</div>
					<c:if test="${orderData.totalDiscounts.value > 0}">
						<div class="order-total-item">
							<div class="row">
								<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.discount"/></div>
								<div class="col-xs-6 col-sm-6 text-right price">
									<ycommerce:testId code="Order_Totals_Subtotal">
										-<format:price priceData="${orderData.totalDiscounts}"/>
									</ycommerce:testId>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${not empty orderData.discountData}">
						<div class="order-total-item">
							<div class="row">
								<div class="col-xs-6 col-sm-6">
									<spring:theme code="text.voucher.discountCode" />:
								</div>
								<div class="col-xs-6 col-sm-6 text-right price">
									${orderData.discountData[0].code}
								</div>
							</div>
						</div>
					</c:if>
					<div class="order-total-item">
						<div class="row">
							<div class="col-xs-6 col-sm-6">${sessionCountry.taxName}:</div>
							<div class="col-xs-6 col-sm-6 text-right price"><format:price priceData="${orderData.totalTax}"/></div>
						</div>
					</div>
					<multi-checkout-common:orderTotalsShippingItem abstractOrderData="${orderData}"/>
				</div>
				<div class="order-total-info order-total-title">
					<div class="row">
						<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.total"/></div>
						<div class="col-xs-6 col-sm-6 text-right">
							<ycommerce:testId code="cart_totalPrice_label">
								<c:choose>
									<c:when test="${showTax}">
										<format:price priceData="${orderData.totalPriceWithTax}"/>
									</c:when>
									<c:otherwise>
										<format:price priceData="${orderData.totalPrice}"/>
									</c:otherwise>
								</c:choose>
							</ycommerce:testId>
						</div>
					</div>
				</div>
			</div>
            </div>

            <div class="buttons-section text-center">
                <a href="${myaccounturl}">
                    <button class="button button-main large"><spring:theme code="header.link.account"/></button>
                </a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="print-section checkout-desktop"><a href="#" class="icon-print"><spring:theme code="product.share.print"/></a></div>
          </div>
        </div>
     
</template:page>
