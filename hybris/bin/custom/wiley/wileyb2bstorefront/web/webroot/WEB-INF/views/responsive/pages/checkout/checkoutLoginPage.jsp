<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="log-in wrap authorization-support-message-wr">

	<div col="row">
		<div class="col-sm-12">
			<multi-checkout:supportMessage />
			<cms:pageSlot position="LeftContentSlot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		<cms:pageSlot position="CenterContentSlot" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
		<cms:pageSlot position="RightContentSlot" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>

</template:page>