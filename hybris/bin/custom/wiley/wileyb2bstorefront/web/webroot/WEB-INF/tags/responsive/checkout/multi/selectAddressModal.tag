<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<div class="row">
				<div class="col-xs-10 col-sm-10">
					<div class="modal-title">
						<spring:theme
							code="checkout.multi.deliveryAddress.addressBook"
							text="Address Book" />
					</div>
				</div>
				<div class="col-xs-2 col-sm-2">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<div class="modal-wnd-list">
				<c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
					<form action="${request.contextPath}/checkout/multi/delivery-address/select" method="GET">
						<input type="hidden" name="selectedAddressCode" value="${deliveryAddress.id}" />

						<div ${!status.last ? 'class="modal-wnd-list-item"' : 'class="modal-wnd-list-item-last"'}>
							<div class="row">
								<div class="col-xs-12 col-sm-8">
									<%--<div class="modal-wnd-list-title">
										<p>Item Title</p>
									</div>--%>
									<div class="modal-wnd-list-description">
										<p>${fn:escapeXml(deliveryAddress.title)}&nbsp;
												${fn:escapeXml(deliveryAddress.firstName)}&nbsp;
												${fn:escapeXml(deliveryAddress.lastName)}</p>
										<p>${fn:escapeXml(deliveryAddress.line1)}</p>
										<p>${fn:escapeXml(deliveryAddress.line2)}</p>
										<p>${fn:escapeXml(deliveryAddress.town)}</p>
										<c:if test="${not empty deliveryAddress.region.name}">
											<p>${fn:escapeXml(deliveryAddress.region.name)}</p>
										</c:if>
										<p>${fn:escapeXml(deliveryAddress.country.name)}&nbsp;
												${fn:escapeXml(deliveryAddress.postalCode)}</p>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="modal-wnd-item-button">
										<button type="submit" class="button button-main small">
											<spring:theme
													code="checkout.multi.deliveryAddress.useThisAddress"
													text="Use this Address" />
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</c:forEach>
			</div>
		</div>
	</div>
</div>