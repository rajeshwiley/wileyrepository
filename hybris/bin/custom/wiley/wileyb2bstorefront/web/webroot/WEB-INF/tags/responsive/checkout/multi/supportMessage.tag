<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="support-message">
	<span class="progress-number"><spring:theme code="checkout.customer.support.first" text="Need Help with your checkout process?"/>&nbsp;
	</span><spring:theme code="checkout.customer.support.second" text="Contact US or Call phone number."/>
</div>
