<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account" %>
<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>



<div id="sendOrderByEmailPopup" role="dialog" class="modal fade modalWindow">
   <div class="modal-dialog">
      <div class="modal-content">

         <div class="modal-header">
            <div class="row">
               <div class="col-xs-10 col-sm-10">
                  <div class="modal-title">
                     <spring:theme code="emailInvoice.popup.title" />

                  </div>
               </div>
               <div class="col-xs-2 col-sm-2">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
               </div>
            </div>
         </div>
		<div class="modal-body">
			<account:emailInvoiceForm />
		</div>
      </div>
   </div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-9">
		<h1 class="full-width-gray">
			<spring:theme code="text.account.order.title.details" text="Order Details" />
		</h1>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="top-icons text-right" role="links">
			<a href="#" id="emailInvoiceLink" class="icon-email" aria-label="<spring:theme code='product.share.send.email' text='Send an e-mail'/>" data-toggle="modal" data-target="#sendOrderByEmailPopup">&nbsp;</a>
			<a href="#" class="icon-print2" aria-label="<spring:theme code='product.share.print' text='Print'/>">&nbsp;</a>
		</div>
	</div>
</div>

<spring:url value="/my-account/orders" var="ordersUrl"/>
<div class="account-orderdetail">
	<div class="account-orderdetail-overview">
		<ycommerce:testId code="orderDetail_overview_section">
			<order-common:accountOrderDetailsOverview order="${orderData}"/>
		</ycommerce:testId>
	</div>
</div>

