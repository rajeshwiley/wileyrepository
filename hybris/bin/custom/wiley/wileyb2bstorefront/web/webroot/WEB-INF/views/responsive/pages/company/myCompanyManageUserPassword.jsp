<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company"%>

<spring:url
	value="/my-company/organization-management/manage-users/resetpassword"
	var="resetpasswordUrl">
	<spring:param name="user" value="${customerResetPasswordForm.uid}"/>
</spring:url>

<template:page pageTitle="${pageTitle}" containerCSSClass="reset-password wrap">

        <div col="row">
          <div col="col-sm-12">
            <div class="site-form central-form">
              <form:form action="${resetpasswordUrl}" method="post" commandName="customerResetPasswordForm" autocomplete="off">
              <form:input type="hidden" name="uid" path="uid" id="uid" />
                <div class="form-cell heading">
                  <h1>Reset your password below</h1>
                </div>
                <div class="form-cell heading">
                    <p><spring:theme code="text.manageuser.password" text="Please enter and confirm the password for customer {0}" arguments="${loggedInUserEmail}"/></p>
                </div>
                <div class="form-cell thin">
                  <formElement:formPasswordBox idKey="profile-newPassword" labelKey="profile.pwd" path="newPassword" inputCSS="text password strength" mandatory="true"/>
                </div>
                <div class="form-cell">
                  <formElement:formPasswordBox idKey="profile.checkNewPassword" labelKey="profile.reenterPassword" path="checkNewPassword" inputCSS="text password" mandatory="true"/>
                </div>
                <div class="form-cell">
                  <button class="button button-main large confirm"><spring:theme code="text.company.user.resetPassword" text="Update Password"/></button>
                </div>
              </form:form>
            </div>
          </div>
        </div>
      </div>

</template:page>
