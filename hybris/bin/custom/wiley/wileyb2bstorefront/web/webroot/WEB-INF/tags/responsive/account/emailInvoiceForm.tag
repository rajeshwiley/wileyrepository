<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<common:globalMessages/>

<form:form id="emailInvoiceForm" method="post" action="${orderCode}/emailinvoice" commandName="emailInvoiceForm">

	<div class="description"><spring:theme code="emailInvoice.form.description"/></div>

	<div class="control-group">
		<formElement:formInputBox idKey="emailInvoice.email" labelKey="emailInvoice.email" path="email" inputCSS="text" mandatory="true"/>
	</div>

	<div class="modal-button-wrap">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-offset-4">
				<button id="emailInvoiceSubmitButton" class="button button-main medium">
					<spring:theme code="emailInvoice.form.button.submit"/>
				</button>
			</div>
			<div class="col-xs-12 col-sm-4">
				<button data-dismiss="modal" aria-label="Close" class="button button-outlined medium">
					<spring:theme code="emailInvoice.form.button.cancel"/>
				</button>
			</div>
		</div>
	</div>
</form:form>