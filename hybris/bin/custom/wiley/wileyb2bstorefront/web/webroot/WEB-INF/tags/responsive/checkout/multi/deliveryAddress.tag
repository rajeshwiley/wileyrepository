<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${!empty address && !empty address.firstName}">	    ${address.firstName} 	</c:if>
<c:if test="${!empty address && !empty address.lastName}">	    ${address.lastName},	</c:if>
<c:if test="${!empty address && !empty address.line1}">		    ${address.line1},		</c:if>
<c:if test="${!empty address && !empty address.line2}">		    ${address.line2},		</c:if>
<c:if test="${!empty address && !empty address.town}">		    ${address.town},		</c:if>
<c:if test="${!empty address && !empty address.region.name}">	${address.region.name} 	</c:if>
<c:if test="${!empty address && !empty address.postalCode}">	${address.postalCode}	</c:if>
<c:if test="${!empty address && !empty address.postalCode && !empty address.country.name}">,</c:if> 
<c:if test="${!empty address && !empty address.country.name}">  ${address.country.name}</c:if>
