<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty cartData.entries}">
    <c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>
    <c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
    <c:set var="showTax" value="false"/>

  <div class="order-total-top-section checkout-mobile">
	  <div class="order-summary">
		<div class="text">
			<c:choose>
				<c:when test="${fn:length(cartData.entries) > 1}">
					<spring:theme code="mobile.basket.page.totals.total.items" text="Cart ({0} items)" arguments="${fn:length(cartData.entries)}"/>
				</c:when>
				<c:otherwise>
					<spring:theme code="mobile.basket.page.totals.total.items.one" text="Cart ({0} item)" arguments="${fn:length(cartData.entries)}"/>
				</c:otherwise>
			</c:choose>
			&nbsp;<format:price priceData="${cartData.totalPrice}"/>
		</div>
	  </div>
	  <div class="order-button-group">
		  <button class="button button-main large checkoutButton" data-checkout-url="${checkoutUrl}"><spring:theme code="basket.proceed.checkout" text="Proceed to Checkout"/></button>
      </div>
      <div class="continue-shopping">
		  <a href="${continueShoppingUrl}"><spring:theme text="Continue Shopping" code="cart.page.continue"/></a>
	  </div>
  </div><!-- class="order-summary-section order-summary-mobile"  -->

    <div class="product-list product-list-cart">
        <cart:cartItems cartData="${cartData}"/>
    </div>
    <div class="product-list-buttons">
        <button class="button button-secondary small continueShoppingButton" data-continue-shopping-url="${continueShoppingUrl}">
            <spring:theme text="Continue Shopping" code="cart.page.continue"/>
        </button>
    </div>
</c:if>

