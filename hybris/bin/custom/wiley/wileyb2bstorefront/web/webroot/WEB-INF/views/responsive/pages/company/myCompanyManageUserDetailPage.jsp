<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/my-company/organization-management/manage-users/edit"
			var="editUserUrl">
	<spring:param name="user" value="${customerData.uid}"/>
</spring:url>
<spring:url value="/my-company/organization-management/manage-users/disable"
			var="disableUserUrl">
	<spring:param name="user" value="${customerData.uid}"/>
</spring:url>
<spring:url value="/my-company/organization-management/manage-users/enable"
			var="enableUserUrl">
	<spring:param name="user" value="${customerData.uid}"/>
</spring:url>
<spring:url value="/my-company/organization-management/manage-users/resetpassword"
			var="resetPasswordUrl">
	<spring:param name="user" value="${customerData.uid}"/>
</spring:url>

<template:page pageTitle="${pageTitle}">

<div class="container my-company manage-user wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-9 col-sm-offset-3">
			<h1 class="hidden-xs">${customerData.name == null ? customerData.displayUid : customerData.name}</h1>
		</div>
	</div>
	<div class="row">
		<company:myCompanyNav selected="users" />
		<div class="col-xs-12 col-sm-9">
			<div class="row">
				<div class="col-xs-12 visible-xs">
					<h1>${customerData.name == null ? customerData.displayUid : customerData.name}</h1>
				</div>
			</div>
			<div class="my-company-wrap">
				<a href="${editUserUrl}" class="button button-edit edit-mobile visible-xs edit" aria-label="<spring:theme code='text.company.manageUser.button.edit' text='Edit'/>" title="<spring:theme code='text.company.manageUser.button.edit' text='Edit'/>"></a>
				<div class="annotation wrap">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<div class="annotation-text">
								<p><spring:theme code="text.company.manageusers.details.subtitle" /></p>
							</div>
						</div>
                        <div class="col-xs-12 col-sm-4 text-right">
                            <div class="my-account-button-small">
                                <a href="${editUserUrl}" class="button button-edit hidden-xs edit" aria-label="<spring:theme code='text.company.manageUser.button.edit' text='Edit'/>" title="<spring:theme code='text.company.manageUser.button.edit' text='Edit'/>"></a>
                                <c:choose>
                                    <c:when test="${customerData.active}">
                                        <a href="${disableUserUrl}" class="button button-outlined small enable">
                                            <spring:theme code="text.company.manageusers.button.disableuser" text="Disable User" />
                                        </a>
                                    </c:when>
                                    <c:otherwise>
                                        <c:choose>
                                            <c:when test="${customerData.unit.active}">
                                                <form:form action="${enableUserUrl}">
                                                    <button class="button button-outlined small enable">
                                                        <spring:theme code="text.company.manageusers.button.enableuser" text="Enable User"/>
                                                    </button>
                                                </form:form>
                                            </c:when>
                                            <c:otherwise>
                                                <form:form action="${enableUserUrl}">
                                                    <button class="button button-outlined small disabled">
                                                        <spring:theme code="text.company.manageusers.button.enableuser" text="Enable User"/>
                                                    </button>
                                                </form:form>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="user-details-wrap">
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<div class="row">
								<div class="col-xs-6 col-sm-6">
									<p><spring:theme code="text.company.user.title" text="Title: " /></p>
									<p><spring:theme code="text.company.user.email" text="Email: " /></p>
									<p><spring:theme code="text.company.user.name" text="First Name: " /></p>
									<p><spring:theme code="text.company.user.middle" text="Middle Name: " /></p>
									<p><spring:theme code="text.company.unit.name" text="Last Name: " /></p>
									<p><spring:theme code="text.company.unit.suffix" text="Suffix: " /></p>
									<p><spring:theme code="text.company.user.password" text="Password: " /></p>
									<p><spring:theme code="text.company.user.userEnabledStatus" text="User Enabled Status: " /></p>
								</div>
								<div class="col-xs-6 col-sm-6">
									<p><spring:theme code="text.company.user.${customerData.titleCode}.name" text="N/A" /></p>
									<p>${customerData.displayUid}</p>
									<p>${empty customerData.firstName ? 'N/A' : fn:escapeXml(customerData.firstName)}</p>
									<p>${empty customerData.middleName ? 'N/A' : fn:escapeXml(customerData.middleName)}</p>
									<p>${empty customerData.lastName ? 'N/A' : fn:escapeXml(customerData.lastName)}</p>
									<p>${empty customerData.suffix ? 'N/A' : fn:escapeXml(customerData.suffix)}</p>
									<p>
										<a href="${resetPasswordUrl}">
											<spring:theme code="text.company.user.resetPassword" text="Reset Password" />
										</a>
									</p>
									<p>
										<c:choose>
											<c:when test="${customerData.active}">
												<spring:theme code="text.company.manage.unit.user.enable" text="Enable" />
											</c:when>
											<c:otherwise>
												<spring:theme code="text.company.manage.unit.user.disable" text="Disable" />
											</c:otherwise>
										</c:choose>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="user-roles-wrap">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<h2><spring:theme code="text.company.manageUser.roles" text="Roles"/></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-8">
							<div class="roles-description wrap">
								<p><spring:theme code="text.company.manageusers.roles.subtitle" arguments="${b2bStore}" /></p>
							</div>
							<div class="user-roles-title wrap">
								<spring:theme code="text.company.manageUser.userRoles" text="USER ROLES" />
							</div>
							<div class="user-roles-description">
								<c:forEach items="${customerData.roles}" var="group">
									<p><spring:theme code="b2busergroup.${group}.name" /></p>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</template:page>
