<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company" %>

<template:page pageTitle="${pageTitle}">
	<div class="container my-company user-confirm-disable wrap">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-sm-offset-3">
				<h1 class="hidden-xs">
					<spring:theme code="text.company.manageuser.user.disable" text="Confirm Disable" arguments="${userUid}"/>
				</h1>
			</div>
		</div>
		<div class="row">
			<company:myCompanyNav selected="users" />
			<div class="col-xs-12 col-sm-9">
				<div class="row">
					<div class="col-xs-12 visible-xs">
						<h1><spring:theme code="text.company.manageuser.user.disable" text="Confirm Disable" arguments="${userUid}"/></h1>
					</div>
				</div>
				<div class="my-company-wrap">
					<div class="annotation wrap-inner-bordered">
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<spring:theme code="text.company.manageuser.disableuser.confirmation" text="Doing this will prevent customer {0} from logging to storefront and placing order." arguments="${userUid}" /> <br />
								<spring:theme code="text.company.manageuser.disableuser.proceed" text="Do you want to proceed?" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<form:form action="${disableUrl}">
								<div class="my-account-button-wrap">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<button type="submit" class="button button-main large confirm"><spring:theme code="b2buser.submit.button" text="Submit"/></button>
										</div>
										<div class="col-xs-12 col-sm-6">
											<a href="${cancelUrl}" class="button button-outlined large no-confirm">
												<spring:theme code="b2buser.cancel.button" text="Cancel" />
											</a>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</template:page>
