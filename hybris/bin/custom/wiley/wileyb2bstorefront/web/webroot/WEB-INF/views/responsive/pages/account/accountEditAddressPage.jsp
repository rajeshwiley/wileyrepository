<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<spring:url value="/my-account/address-book" var="addressBookUrl"/>

<c:choose>
	<c:when test="${edit eq true }">
		<h1 class="full-width-gray"><spring:theme code="text.account.addressBook.updateAddress" text="Edit Address"/></h1>
	</c:when>
	<c:otherwise>
		<h1 class="full-width-gray"><spring:theme code="text.account.addressBook.addAddress" text="New Address"/></h1>
	</c:otherwise>
</c:choose>
<div class="account-section-content	 account-section-content-small my-account-wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="site-form">
				<address:addressFormSelector supportedCountries="${countries}" regions="${regions}" cancelUrl="/my-account/address-book" addressBook="true" />
			</div>
		</div>
    </div>
</div>
