<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:url value="/my-account/payment-details" var="paymentDetailsPageUrl" />
<h1 class="full-width-gray">
	<spring:theme code="text.account.paymentDetails.addCreditCard"/>
</h1>
<div class="my-account-wrap">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
                <form:form commandName="addressForm" method="POST">
                 <div class="site-form">
                    <input type="hidden" name="bill_state" id="address.billstate" />

                    <form:hidden path="paymentInfoId" />

                    <div id="countrySelector" data-address-code="${addressData.id}"
                        data-country-iso-code="${addressData.country.isocode}">
                        <formElement:formSelectBox formFieldClass="form-field select-component select-form-element"  idKey="address.country"
                            labelKey="address.country" path="countryIso" mandatory="true"
                            skipBlank="false" skipBlankMessageKey="address.country"
                            items="${supportedCountries}" itemValue="isocode"
                            selectedValue="${addressForm.countryIso}" />
                    </div>

                    <div id="i18nAddressForm" class="i18nAddressForm">
                        <c:if test="${not empty country}">
                            <address:addressFormElements regions="${regions}"
                                country="${country}" />
                        </c:if>
                    </div>
                    <c:choose>
                        <c:when test="${hasDefaultPayment}">
                            <formElement:formCheckbox idKey="address.defaultAddress" labelKey="text.account.paymentDetails.addCreditCard.default" path="defaultAddress" mandatory="false"/>
                        </c:when>
                        <c:otherwise>
                        <input type="hidden" name="defaultAddress" value="true"/>
                        </c:otherwise>
                    </c:choose>

                 <div class="my-account-button-wrap">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <button type="submit" class="button button-main large">
                                    <spring:theme code="text.button.continue"/>
                                </button>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <a href='${paymentDetailsPageUrl}'"
                                class="button button-main large button-outlined">
                                <spring:theme code="text.button.cancel"/>
                                </a>
                            </div>
                        </div>
                 </div>
                </div>
                </form:form>
        </div>
    </div>
</div>