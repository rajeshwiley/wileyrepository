<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<h1 class="full-width-gray"><spring:theme code="text.account.paymentDetails"/></h1>
<div class="payment-details">
            <div class="simple-list my-account-wrap">
              <div class="wrap-inner-bordered">
                <spring:url value="/my-account/payment-details/add-new-card" var="addNewCardUrl"/>
                <form action="${addNewCardUrl}">
                    <button class="button button-main large" type="submit"><spring:theme code="text.account.paymentDetails.addCreditCard"/></button>
                </form>
              </div>
              <c:choose>
              <c:when test="${not empty paymentInfoData}">
              <c:forEach items="${paymentInfoData}" var="paymentInfo">
              <div class="wrap-inner-bordered">
                <div class="row">
                  <div class="col-xs-12 col-sm-8">
                    <div class="simple-list-text">
                      <p>${fn:escapeXml(paymentInfo.accountHolderName)}<c:if test="${paymentInfo.defaultPaymentInfo}" >&nbsp;(<spring:theme code="text.account.paymentDetails.default"/>)</c:if></p>
                      <p>${fn:escapeXml(paymentInfo.cardTypeData.name)}</p>
                      <p>************${fn:escapeXml(paymentInfo.cardNumber)}</p>
                      <p>${fn:escapeXml(paymentInfo.expiryMonth)}&nbsp;/&nbsp;${fn:escapeXml(paymentInfo.expiryYear)}</p>
                      <p>${fn:escapeXml(paymentInfo.billingAddress.line1)}</p>
                      <p>${fn:escapeXml(paymentInfo.billingAddress.line2)}</p>
                      <p>${fn:escapeXml(paymentInfo.billingAddress.town)},&nbsp;${fn:escapeXml(paymentInfo.billingAddress.region.name)}</p>
                      <p>${fn:escapeXml(paymentInfo.billingAddress.country.name)}&nbsp;${fn:escapeXml(paymentInfo.billingAddress.postalCode)}</p>
                    </div>
                  </div>
                   <div class="col-xs-12 col-sm-4 text-right">
                      <div class="row">
                        <div class="col-xs-8 col-sm-8">
	                      <c:if test="${not paymentInfo.defaultPaymentInfo}" >
                          <div class="my-account-button-small">
                            <spring:url value="/my-account/set-default-payment-details" var="setDefaultUrl"/>
                            <form:form action="${setDefaultUrl}" method="POST">
                                <input type="hidden" value="${paymentInfo.id}" name="paymentInfoId"/>
                                <button class="button button-outlined large" type="submit"><spring:theme code="text.account.paymentDetails.setAsDefault"/></button>
                            </form:form>
                          </div>
	                      </c:if>
                        </div>
                      <c:url value="/my-account/payment-details/edit-billing-address/${paymentInfo.id}" var="editBillingAddressUrl" />
                      <div class="col-xs-4 col-sm-4">
                            <a href="${editBillingAddressUrl}" aria-label="<spring:theme code='text.account.addressBook.edit.payment.details'/>" class="button button-edit"></a>
                            <button data-toggle="modal" aria-label="<spring:theme code='text.account.addressBook.delete.payment'/>" data-target="#deletePayment-${paymentInfo.id}" class="button button-remove"></button>
                      </div>
                     </div>
                   </div>
                   <div id="deletePayment-${paymentInfo.id}" role="dialog" class="modal fade modalWindow deletePayment">
                       <div class="modal-dialog">
                         <div class="modal-content">
                           <div class="modal-header">
                             <div class="row">
                               <div class="col-xs-10 col-sm-10">
                                 <div class="modal-title"><spring:theme code="text.account.paymentDetails.delete.popup.title"/>
                                 </div>
                               </div>
                               <div class="col-xs-2 col-sm-2">
                                 <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
                               </div>
                             </div>
                           </div>
                           <div class="modal-body">
                             <div class="modal-wnd-content">
                               <p><spring:theme code="text.account.paymentDetails.delete.popup.title"/></p>
                             </div>
                             <div class="modal-wnd-list bold">
                               <div class="modal-wnd-list-item">
                                 <div>
                                    <p>${fn:escapeXml(paymentInfo.accountHolderName)}<c:if test="${paymentInfo.defaultPaymentInfo}" >&nbsp;(<spring:theme code="text.account.paymentDetails.default"/>)</c:if></p>
                                    <p>${fn:escapeXml(paymentInfo.cardTypeData.name)}</p>
                                    <p>************${fn:escapeXml(paymentInfo.cardNumber)}</p>
                                    <p>${fn:escapeXml(paymentInfo.expiryMonth)}&nbsp;/&nbsp;${fn:escapeXml(paymentInfo.expiryYear)}</p>
                                    <p>${fn:escapeXml(paymentInfo.billingAddress.line1)}</p>
                                    <p>${fn:escapeXml(paymentInfo.billingAddress.line2)}</p>
                                    <p>${fn:escapeXml(paymentInfo.billingAddress.town)},&nbsp;${fn:escapeXml(paymentInfo.billingAddress.region.name)}</p>
                                    <p>${fn:escapeXml(paymentInfo.billingAddress.country.name)}&nbsp;${fn:escapeXml(paymentInfo.billingAddress.postalCode)}</p>
                                 </div>
                               </div>
                               <div class="modal-button-wrap">
                                 <div class="row">
                                   <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                                     <spring:url value="/my-account/remove-payment-method" var="removeCardUrl"/>
                                     <form:form action="${removeCardUrl}" method="POST">
                                        <input type="hidden" value="${paymentInfo.id}" name="paymentInfoId"/>
                                        <button type="submit" class="button button-main medium"><spring:theme code="text.account.paymentDetails.delete"/></button>
                                     </form:form>
                                   </div>
                                   <div class="col-xs-12 col-sm-4">
                                     <button data-dismiss="modal" aria-label="Close" class="button button-outlined medium"><spring:theme code="text.account.profile.cancel"/></button>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                </div>
              </div>
              </c:forEach>
              </c:when>
                <c:otherwise>
                <div><spring:theme code="text.account.paymentDetails.noPaymentInformation"/></div>
                </c:otherwise>
            </c:choose>
            </div>
          </div>
</div>
