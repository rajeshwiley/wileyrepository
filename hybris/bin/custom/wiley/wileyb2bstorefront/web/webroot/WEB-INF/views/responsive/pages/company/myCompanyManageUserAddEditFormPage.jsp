<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="org-common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company"%>
<%@ taglib prefix="customFormElement" tagdir="/WEB-INF/tags/desktop/customFormElement"%>

<c:if test="${empty saveUrl}">
	<c:choose>
		<c:when test="${not empty b2BCustomerForm.uid}">
			<spring:url value="/my-company/organization-management/manage-users/edit" var="saveUrl">
				<spring:param name="user" value="${b2BCustomerForm.uid}"/>
			</spring:url>
		</c:when>
		<c:otherwise>
			<spring:url value="/my-company/organization-management/manage-users/create" var="saveUrl"/>
		</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${empty cancelUrl}">
	<c:choose>
		<c:when test="${not empty b2BCustomerForm.uid}">
			<spring:url value="/my-company/organization-management/manage-users/details"
						var="cancelUrl">
				<spring:param name="user" value="${b2BCustomerForm.uid}"/>
			</spring:url>
		</c:when>
		<c:otherwise>
			<c:url value="/my-company/organization-management/manage-users/" var="cancelUrl"/>
		</c:otherwise>
	</c:choose>
</c:if>

<c:set var="pageHeadline">
	<c:choose>
		<c:when test="${not empty b2BCustomerForm.uid}">
			<spring:theme code="text.company.${action}.edituser.title" text="${action}" arguments="${fn:escapeXml(b2BCustomerForm.parentB2BUnit)}"/>
		</c:when>
		<c:otherwise>
			<spring:theme code="text.company.${action}.adduser.title" text="${action}" arguments="${fn:escapeXml(param.unit)}"/>
		</c:otherwise>
	</c:choose>
</c:set>

<template:page pageTitle="${pageTitle}">
	<div class="container my-company create-user wrap">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-sm-offset-3">
				<h1 class="hidden-xs">${pageHeadline}</h1>
			</div>
		</div>
		<div class="row">
			<company:myCompanyNav selected="users" />
			<div class="col-xs-12 col-sm-9">
				<div class="row">
					<div class="col-xs-12 visible-xs">
						<h1>${pageHeadline}</h1>
					</div>
				</div>
				<div class="my-company-wrap">
					<form:form action="${saveUrl}" method="post" commandName="b2BCustomerForm">
						<form:input type="hidden" name="uid" path="uid" id="uid" />
						<div class="create-user-form wrap">
							<div class="row">
								<div class="col-xs-12 col-sm-4">
									<div class="site-form">
									    <spring:theme code='text.formField.optional' var="formFieldOptional"/>
										<div class="form-cell">
											<formElement:formSelectBox
												idKey="user.title" labelKey="user.title" path="titleCode" mandatory="false"
												formFieldClass="form-field select-component select-form-element" skipBlank="false"
												skipBlankMessageKey="form.select.empty" items="${titleData}" />
										</div>
										<div class="form-cell">
											<spring:theme code='user.emailAddress.placeholder' var="emailAddressPlaceholder"/>
											<formElement:formInputBox
												idKey="user.email" labelKey="user.emailAddress" path="email" inputCSS="text"
												formFieldClass="form-field" mandatory="true"
												placeholder="${emailAddressPlaceholder}"
												readonly="${not isUserCreation}" />
										</div>
										<div class="form-cell">
											<spring:theme code='user.firstName.placeholder' var="firstNamePlaceholder"/>
											<formElement:formInputBox
												idKey="user.firstName" labelKey="user.firstName" path="firstName" inputCSS="text"
												formFieldClass="form-field" mandatory="true"
												placeholder="${firstNamePlaceholder}" />
										</div>

										<div class="form-cell">
											<spring:theme code='user.middleName.placeholder' var="middleNamePlaceholder"/>
											<formElement:formInputBox
												idKey="user.middleName" labelKey="user.middleName" path="middleName" inputCSS="text"
												formFieldClass="form-field" mandatory="false"
												placeholder="${middleNamePlaceholder}" />
										</div>
										<div class="form-cell">
											<spring:theme code='user.lastName.placeholder' var="lastNamePlaceholder"/>
											<formElement:formInputBox
												idKey="user.lastName" labelKey="user.lastName" path="lastName" inputCSS="text"
												formFieldClass="form-field" mandatory="true"
												placeholder="${lastNamePlaceholder}" />
										</div>
										<div class="form-cell">
											<formElement:formSelectBox
												idKey="user.suffix" labelKey="user.suffix" path="suffixCode" mandatory="false"
												formFieldClass="form-field select-component select-form-element" skipBlank="false"
												skipBlankMessageKey="form.select.empty" items="${suffixData}" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="user-roles-wrap wrap-inner-bordered">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<div class="user-roles-option">
										<customFormElement:formCheckboxes
											idKey="text.company.user.roles"
											labelKey="text.company.user.roles"
											path="roles"
											mandatory="false"
											items="${roles}"
											disabled="${not empty param.unit and not empty param.role}"
											legendTag="h2"
											typeIdentifier="String" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-5">
								<div class="my-account-button-wrap">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<ycommerce:testId code="User_Save_button">
												<button type="submit" class="button button-main large save">
													<spring:theme code="text.account.user.saveChanges" text="Save Changes"/>
												</button>
											</ycommerce:testId>
										</div>
										<div class="col-xs-12 col-sm-6">
											<ycommerce:testId code="User_Cancel_button">
												<a href="${cancelUrl}" class="button button-outlined large cancel">
													<spring:theme code="b2bcustomer.cancel" text="Cancel" />
												</a>
											</ycommerce:testId>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-7 one-arrow-pagination-wrap">
								<div class="panel-pagination">
									<div class="pagination-element">
										<a href="${cancelUrl}" title="<spring:theme code='text.back' text='Back'/>" class="arrow-left">
											&nbsp;
										</a>
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</template:page>

