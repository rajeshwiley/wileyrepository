<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
    <cms:component component="${component}" />
</cms:pageSlot>

<header id="main-header-container" class="main-header-container" role="banner">
    <div class="container">
        <nav class="main-navigation-container" role="navigation">
            <div class="main-navigation-links">
                <cms:pageSlot position="SiteLogo" var="logo" limit="1" element="div" class="logo">
                    <cms:component component="${logo}" />
                </cms:pageSlot>
                <div class="navigation-links-items">

                    <c:if test="${empty hideHeaderLinks}">
                        <sec:authorize ifNotGranted="ROLE_ANONYMOUS">
                            <c:set var="maxNumberChars" value="25" />
                            <c:if test="${fn:length(user.firstName) gt maxNumberChars}">
                                <c:set target="${user}" property="firstName"
                                    value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
                            </c:if>
                            <div class="navigation-link-item welcome-link">
                                <ycommerce:testId code="header_LoggedUser">
                                    <spring:theme code="header.welcome" arguments="${user.firstName},${user.lastName}"
                                        htmlEscape="true" />
                                </ycommerce:testId>
                            </div>
                           <cms:pageSlot position="HeaderLinks" var="link">
                            <cms:component component="${link}" />
                        </cms:pageSlot>
                        </sec:authorize>
                        <sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
                            <div class="navigation-link-item">
                                <ycommerce:testId code="header_Login_link">
                                    <a href="<c:url value="/login"/>"> <spring:theme code="header.link.login" />
                                    </a>
                                </ycommerce:testId>
                            </div>                  
                        </sec:authorize>
                        <div class="navigation-link-item my-cart-link">
                            <cms:pageSlot position="MiniCart" var="cart" limit="1">
                                <cms:component component="${cart}" />
                            </cms:pageSlot>
                        </div>
                    </c:if>
                </div>
            </div>
            <c:if test="${empty hideHeaderLinks}">
                <cms:pageSlot position="NavigationBar" var="navBar" limit="1">
                    <cms:component component="${navBar}" />
                </cms:pageSlot>
            </c:if>
            
        </nav>
    </div>
</header>
