<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<c:url value="/cart/miniCart/${totalDisplay}" var="refreshMiniCartUrl" />
<c:url value="/cart/rollover/${component.uid}" var="rolloverPopupUrl" />
<c:url value="/cart" var="cartUrl" />
<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
	<ycommerce:testId code="header_cart_link">
		<c:url value="/login" var="cartUrl" />
		<a href="${cartUrl}" >
		</a>
	</ycommerce:testId>
</sec:authorize>
<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
<a href="${cartUrl}" 
	data-mini-cart-url="${rolloverPopupUrl}"
	data-mini-cart-refresh-url="${refreshMiniCartUrl}"
	data-mini-cart-name="<spring:theme code="text.cart"/>"
	data-mini-cart-empty-name="<spring:theme code="popup.cart.empty"/>">


	<ycommerce:testId code="miniCart_items_label">
		<span class="quantity">${totalItems}</span>
		<span class="price"> <c:if test="${totalDisplay == 'TOTAL'}">
				<format:price priceData="${totalPrice}" />
			</c:if> <c:if test="${totalDisplay == 'SUBTOTAL'}">
				<format:price priceData="${subTotal}" />
			</c:if> <c:if test="${totalDisplay == 'TOTAL_WITHOUT_DELIVERY'}">
				<format:price priceData="${totalNoDelivery}" />
			</c:if>
		</span>
	</ycommerce:testId>

</a>
</sec:authorize>