<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


	<h1 class="full-width-gray"><spring:theme code="text.account.profile.updatePersonalDetails"/></h1>

    <div class="my-account-wrap personal-details">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="site-form">
                    <form:form method="post" commandName="updateProfileForm">

                        <formElement:formInputBox idKey="profile.loginId" labelKey="profile.loginId" path="loginId" mandatory="true" labelCSS="form-label required" placeholder="profile.loginId.placeholder" disabled="true"/>
                        <formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email" inputCSS="disabled" mandatory="true" placeholder="profile.email.placeholder" disabled="true"/>
                        <formElement:formSelectBox idKey="profile.title" labelKey="profile.title" path="titleCode" mandatory="false" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${titleData}" formFieldClass="select-component select-form-element"/>
                        <formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" placeholder="profile.firstName.placeholder"/>
                        <formElement:formInputBox idKey="profile.middleName" labelKey="profile.middleName" path="middleName" inputCSS="text" mandatory="false" placeholder="profile.middleName.placeholder"/>
                        <formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" placeholder="profile.lastName.placeholder"/>
                        <formElement:formSelectBox idKey="profile.suffix" labelKey="profile.suffix" path="suffixCode"  mandatory="false" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${suffixData}" formFieldClass="select-component select-form-element"/>

                        <div class="my-account-button-wrap">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <button type="submit" class="button button-main large">
                                        <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                    </button>
                                </div>
                                <div class="col-xs-12 col-sm-5">
                                    <button type="button" class="button button-main large button-outlined backToHome">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form:form>
                </div>
            </div>
        </div>
	</div>

