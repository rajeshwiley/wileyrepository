<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ attribute name="subtotalsCssClasses" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<%@ taglib prefix="multi-checkout-common"
	tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>

<spring:url value="/checkout/multi/summary/applyVoucher" var="applyVoucherUrl"/>
<spring:url value="/checkout/multi/summary/removeVoucher" var="removeVoucherUrl"/>

<div class="order-total-list">
	<div class="order-total-title"><spring:theme code="order.order.totals"/></div>
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.subtotal"/></div>
			<div class="col-xs-6 col-sm-6 text-right price">
				<ycommerce:testId code="Order_Totals_Subtotal">
					<format:price priceData="${cartData.subTotal}"/>
				</ycommerce:testId>
			</div>
		</div>
	</div>
	<c:if test="${cartData.totalDiscounts.value > 0}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.discount"/></div>
				<div class="col-xs-6 col-sm-6 text-right price">
					<ycommerce:testId code="Order_Totals_Subtotal">
						-<format:price priceData="${cartData.totalDiscounts}"/>
					</ycommerce:testId>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${not empty cartData.discountData}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6">
					<spring:theme code="text.voucher.discountCode" />:
				</div>
				<div class="col-xs-6 col-sm-6 text-right price">
					${cartData.discountData[0].code}
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${showTax && cartData.taxCalculated}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6">${sessionCountry.taxName}:</div>
				<div class="col-xs-6 col-sm-6 text-right price">
					<format:price priceData="${cartData.totalTax}"/>
				</div>
			</div>
		</div>
	</c:if>
	<multi-checkout-common:orderTotalsShippingItem abstractOrderData="${cartData}"/>
</div>

<c:if test="${showVoucherForm}">
	<div class="order-promotion-code">
		<div class="promotion-field">
			<div class="site-form">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<spring:theme var="promotionPlaceholder" code='checkout.summary.promotion.message' />
						<form:form id="applyVoucherForm" action="${applyVoucherUrl}" commandName="voucherForm" method="post">
							<div class="form-field input-group">
								<formElement:formInputBox idKey="cart.voucher.apply" labelKey="" path="discountCode" placeholder="${promotionPlaceholder}" ariaLabel="${promotionPlaceholder}" />
								<div class="input-group-btn">
									<button class="button button-main large"><spring:theme code="basket.apply"/></button>
								</div>
							</div>
						</form:form>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<c:forEach items="${cartData.discountData}" var="discountData">
							<form:form action="${removeVoucherUrl}" commandName="voucherForm" method="post">
								<form:hidden path="discountCode" value="${discountData.code}"/>
								<div class="row promo-message">
									<div class="col-xs-12 col-sm-8">
										<c:set var="discountCode"><c:out value="${discountData.code}"/></c:set>
										<c:set var="discountOff">
											<c:choose>
												<c:when test="${not empty discountData.price.formattedValue}">
													<c:out value="${discountData.price.formattedValue}"/>
												</c:when>
												<c:otherwise>
													<fmt:formatNumber var="discountOffInt" maxFractionDigits="0" value="${discountData.relativeValue}" />
													<spring:theme code="basket.discountOffText" arguments="${discountOffInt}"/>
												</c:otherwise>
											</c:choose>
										</c:set>
										<spring:theme code="basket.discountCode.appliedMessage"
											arguments="${discountCode},${discountOff},<span>,</span>" htmlEscape="false"/>
									</div>
									<div class="col-xs-12 col-sm-4">
										<button class="button button-main small pull-right"><spring:theme code="basket.remove" text="Remove"/></button>
									</div>
								</div>
							</form:form>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>

<div class="order-total-info order-total-title">
	<div class="row">
		<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.total"/></div>
		<div class="col-xs-6 col-sm-6 text-right">
			<ycommerce:testId code="cart_totalPrice_label">
				<c:choose>
					<c:when test="${showTax}">
						<format:price priceData="${cartData.totalPriceWithTax}"/>
					</c:when>
					<c:otherwise>
						<format:price priceData="${cartData.totalPrice}"/>
					</c:otherwise>
				</c:choose>
			</ycommerce:testId>
		</div>
	</div>
</div>
