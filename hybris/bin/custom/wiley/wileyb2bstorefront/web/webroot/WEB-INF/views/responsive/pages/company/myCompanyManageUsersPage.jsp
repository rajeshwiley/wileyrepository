<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company"%>

<spring:url value="/my-company/organization-management/manage-users/create" var="manageUsersUrl" />

<template:page pageTitle="${pageTitle}">

	<div id="globalMessages">
		<common:globalMessages/>
	</div>

	<div class="container my-company all-users wrap">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-sm-offset-3">
				<h1 class="hidden-xs"><spring:theme code="text.company.manageusers.label" text="All Users"/></h1>
			</div>
		</div>
		<div class="row">
			<company:myCompanyNav selected="users" />
			<div class="col-xs-12 col-sm-9">
				<div class="row">
					<div class="col-xs-12 visible-xs">
						<h1><spring:theme code="text.company.manageusers.label" text="All Users"/></h1>
					</div>
				</div>
				<div class="my-company-wrap">
					<div class="wrap">
						<ycommerce:testId code="User_AddUser_button">
							<a href="${manageUsersUrl}" class="button button-main large add">
								<spring:theme code="text.company.manageUser.button.create" text="Create New User" />
							</a>
						</ycommerce:testId>
					</div>

					<nav:controlPanel
						top="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
						searchPageData="${searchPageData}"
						searchUrl="/my-company/organization-management/manage-users/?sort=${searchPageData.pagination.sort}"
						sortQueryParams="user=${param.user}" msgKey="text.company.manageUser.pageAll"
						numberPagesShown="${numberPagesShown}" />

					<form>
						<div class="common-table">
							<div class="thead hidden-xs">
								<div class="row">
									<div class="col-sm-3"><spring:theme code="text.company.column.name.name" text="Name" /></div>
									<div class="col-sm-2"><spring:theme code="text.company.column.roles.name" text="Roles" /></div>
									<div class="col-sm-2"><spring:theme code="text.company.column.status.name" text="Status" /></div>
									<div class="col-sm-2"><spring:theme code="text.company.manage.units.user.email" text="Email" /></div>
								</div>
							</div>
							<div class="tbody">
								<c:forEach items="${searchPageData.results}" var="user">
									<spring:url value="/my-company/organization-management/manage-users/details/" var="viewUserUrl">
										<spring:param name="user" value="${user.uid}" />
									</spring:url>
									<div class="item">
										<div class="row">
											<div class="col-xs-12 col-sm-3">
												<div class="row">
													<div class="col-xs-6 thead visible-xs">
														<spring:theme code="text.company.column.name.name" text="Name" />
													</div>
													<div class="col-xs-6 col-sm-12">
														<ycommerce:testId code="my-company_username_label">
															<p>
																<a href="${viewUserUrl}">
																	${fn:escapeXml(user.firstName)}&nbsp;${fn:escapeXml(user.lastName)}
																</a>
															</p>
														</ycommerce:testId>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-2">
												<div class="row">
													<div class="col-xs-6 thead visible-xs">
														<spring:theme code="text.company.column.roles.name" text="Roles" />
													</div>
													<div class="col-xs-6 col-sm-12">
														<ycommerce:testId code="my-company_user_roles_label">
															<c:forEach items="${user.roles}" var="role">
																<p><spring:theme code="b2busergroup.${role}.name" /></p>
															</c:forEach>
														</ycommerce:testId>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-2">
												<div class="row">
													<div class="col-xs-6 thead visible-xs">
														<spring:theme code="text.company.column.status.name" text="Status" />
													</div>
													<div class="col-xs-6 col-sm-12">
														<ycommerce:testId code="my-company_user_status_label">
															<p><spring:theme code="text.company.status.active.${user.active}" /></p>
														</ycommerce:testId>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-3">
												<div class="row">
													<div class="col-xs-6 thead visible-xs">
														<spring:theme code="text.company.manage.units.user.email" text="Email" />
													</div>
													<div class="col-xs-6 col-sm-12">
														<p>
															${user.displayUid}
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
					</form>

					<nav:controlPanel
						top="false" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}"
						searchPageData="${searchPageData}"
						searchUrl="/my-company/organization-management/manage-users/?sort=${searchPageData.pagination.sort}"
						sortQueryParams="user=${param.user}" msgKey="text.company.manageUser.pageAll"
						numberPagesShown="${numberPagesShown}" />

				</div>
			</div>
		</div>
	</div>
</template:page>
