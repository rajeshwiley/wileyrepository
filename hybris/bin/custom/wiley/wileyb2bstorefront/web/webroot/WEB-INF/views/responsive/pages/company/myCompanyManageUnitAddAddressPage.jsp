<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="org-common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<spring:url value="/my-company/organization-management/manage-unit/" var="cancelUrl">
	<spring:param name="unit" value="${uid}"/>
</spring:url>
<c:choose>
	<c:when test="${not empty addressData.id}">
		<spring:url value="/my-company/organization-management/manage-unit/edit-address/" var="actionUrl">
			<spring:param name="unit" value="${uid}"/>
			<spring:param name="addressId" value="${addressData.id}"/>
		</spring:url>
	</c:when>
	<c:otherwise>
		<spring:url value="/my-company/organization-management/manage-unit/add-address/" var="actionUrl">
			<spring:param name="unit" value="${uid}"/>
		</spring:url>
	</c:otherwise>
</c:choose>

<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages/>
	</div>
	<company:myCompanyNav selected="units"/>
	<div class="col-xs-12 col-sm-9">
		<h1>
			<spring:theme code="text.company.manage.units.addressForm.${empty addressData.id?'create':'edit'}.header" arguments="${unitName}"/>
		</h1>
		<div class="row">
			<div class="col-xs-12 visible-xs">
				<spring:theme code="text.company.manage.units.addressForm.${empty addressData.id?'create':'edit'}.header" arguments="${unitName}"/>
			</div>
		</div>
		
		<div class="my-company-wrap">
			<div class="create-shipping-address-form wrap">
				<div class="row">
					<div class="col-xs-12 col-sm-4">
						<div class="my-company-form-msg wrap">
							<spring:theme code="text.account.addressBook.addEditform"	  text="Please use this form to add/edit an address."/>
						</div>
						<div class="site-form">
							<address:addressFormSelector
									supportedCountries="${countryData}"
									regions="${regions}"
									cancelUrl="/my-company/organization-management/manage-unit"
									addressBook="true" country="${country.isocode}"
									submitUrl="?unit=${uid}"
									showMakeDefault="false"/>
							<br>
							<div class="row">
								<div class="col-xs-12 col-sm-6">
									<a href="${cancelUrl}">
										<button  id="backButton" type="submit" class="button button-main large">
											<spring:theme code="text.back" text="Back"/>
										</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</template:page>
