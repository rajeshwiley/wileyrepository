<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="true"
	type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set var="hasShippedItems"
	value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="firstShippedItem" value="true"></c:set>

<c:if test="${hasShippedItems}">
	<div class="checkout-full-width-wrap">
		<div class="checkout-accordion-content-wrap">
			<div class="checkout-shipment-list">
				<div class="checkout-content-subtitle">
					<spring:theme code="checkout.multi.shipment.items"
								  arguments="${cartData.physicalItemsQuantity}"
								  text="Shipment: ${cartData.physicalItemsQuantity} Item(s)"></spring:theme>
				</div>

				<c:forEach items="${cartData.entries}" var="entry">
					<c:if test="${not entry.product.isDigital}">
						<c:url value="${entry.product.url}" var="productUrl" />
						<div class="checkout-shipment-item">
							<div class="row">
								<div class="col-xs-12 col-sm-10">
										${entry.product.name}
								</div>
								<div class="col-xs-12 col-sm-2">
									<spring:theme code="checkout.multi.shipment.items.qty" />
									&nbsp;${entry.quantity}
								</div>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</div>
	</div>
</c:if>