<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="true" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="inputCSS" required="false" type="java.lang.String" %>
<%@ attribute name="tabindex" required="false" rtexprvalue="true" %>
<%@ attribute name="autocomplete" required="false" type="java.lang.String" %>
<%@ attribute name="formFieldClass" required="false" rtexprvalue="true" %>
<%@ attribute name="placeholder" required="false" rtexprvalue="true" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:if test="${empty formFieldClass}">
	<c:set var="formFieldClass" value="controls" />
</c:if>

<template:errorSpanField path="${path}">
	<ycommerce:testId code="LoginPage_Item_${idKey}">

	<div class="form-label ${mandatory != null && mandatory == true ? 'required' : ''}">
		<label class="control-label ${labelCSS}" for="${idKey}">
			<spring:theme code="${labelKey}"/>
			<c:if test="${mandatory == null || mandatory != true}">
				${' '}<spring:theme code="text.formField.optional"/>
			</c:if>
			<span class="skip"><form:errors path="${path}"/></span>
		</label>
	</div>

	<div class="${formFieldClass}">
		<form:input cssClass="${inputCSS}" id="${idKey}" path="${path}" tabindex="${tabindex}" autocomplete="${autocomplete}" placeholder="${placeholder}"/>
	</div>

	</ycommerce:testId>
</template:errorSpanField>
