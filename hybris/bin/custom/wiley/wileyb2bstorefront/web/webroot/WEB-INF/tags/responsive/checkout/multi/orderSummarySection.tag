<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="user" required="true" type="de.hybris.platform.commercefacades.user.data.CustomerData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="multi-checkout-b2b" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="wileycomProduct" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/product"%>

<spring:theme code="checkout.shipping.methods.desired.shipping.date.format" text="MM/dd/YYYY" var="dateFormat" />
<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}" />

<c:if test="${not empty cartData}">

	<div class="order-brief-top">
		<c:if test="${not hasShippedItems}">
        	<spring:theme code="checkout.pickup.no.delivery.required"/>
        </c:if>


        <c:if test="${hasShippedItems}">
			<c:if test="${!empty cartData.deliveryAddress}">
				<div class="order-brief-top-item">
					<div class="row">
						<div class="col-xs-12 col-sm-6 order-brief-top-title">Ship to:</div>
						<div class="col-xs-12 col-sm-6">
							<p>
								<c:if test="${!empty cartData.deliveryAddress.title}">${cartData.deliveryAddress.title}</c:if>
								<c:if test="${!empty cartData.deliveryAddress.firstName}">${cartData.deliveryAddress.firstName}</c:if>
								<c:if test="${!empty cartData.deliveryAddress.lastName}">${cartData.deliveryAddress.lastName}</c:if>
							</p>
							<p>
								<c:if test="${!empty cartData.deliveryAddress.line1}">${cartData.deliveryAddress.line1},</c:if>
								<c:if test="${!empty cartData.deliveryAddress.line2}">${cartData.deliveryAddress.line2},</c:if>
								<c:if test="${!empty cartData.deliveryAddress.town}">${cartData.deliveryAddress.town},</c:if>
								<c:if test="${!empty cartData.deliveryAddress.region}">${cartData.deliveryAddress.region.name}</c:if>
							</p>
							<p>
								<c:if test="${!empty cartData.deliveryAddress.postalCode}">${cartData.deliveryAddress.postalCode},</c:if>
								<c:if test="${!empty cartData.deliveryAddress.country}">${cartData.deliveryAddress.country.name}</c:if>
							</p>
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${!empty cartData.desiredShippingDate}">
				<fmt:formatDate pattern="${dateFormat}" value="${cartData.desiredShippingDate}" var="desiredShippingDate" />
				<div class="order-brief-top-item">
					<div class="row">
						<div class="col-xs-12 col-sm-6 order-brief-top-title">Desired Shipping Date:</div>
						<div class="col-xs-12 col-sm-6">
							<p>${desiredShippingDate}</p>
						</div>
					</div>
				</div>
			</c:if>

			<c:if test="${!empty cartData.deliveryMode}">
				<div class="order-brief-top-item">
					<div class="row">
						<div class="col-xs-12 col-sm-6 order-brief-top-title">Shipping Method:</div>
						<div class="col-xs-12 col-sm-6">
							<p>${cartData.deliveryMode.name}</p>
						</div>
					</div>
				</div>
			</c:if>
		</c:if>
	</div>

	<div class="product-list">
		<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
			<c:url value="${entry.product.url}" var="productUrl"/>
			<div class="product-list-item">
				<div class="row product-list-info">
					<div class="col-xs-9 col-sm-9">
						<div class="product-list-image">
							<a href="${productUrl}">
								<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
							</a>
						</div>
						<div class="product-list-title">
							<a href="${productUrl}">
								${entry.product.name}
							</a>
						</div>
						<div class="product-list-description">
							<div class="product-list-qty">Qty: ${entry.quantity}
								<c:if test="${not empty entry.poNumber}">
								    <spring:theme code="checkout.multi.order.po.number" arguments="${entry.poNumber}"/>
								</c:if>
							</div>
							<wileycomProduct:CartItemInventoryStatusBlock entry="${entry}" showReleaseDate="true"/>
						</div>
					</div>
					<div class="col-xs-3 col-sm-3 text-right">
						<div class="product-item-price"><format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>

	<c:if test="${!empty cartData.paymentInfo || cartData.paymentType.code eq 'ACCOUNT'}">
		<c:set var="paymentInfo" value="${cartData.paymentInfo}"/>
		<div class="payment-type">
			<div class="row">
				<div class="col-xs-12 col-sm-2">
					<div class="payment-type-title">Payment:</div>
				</div>
				<div class="col-xs-12 col-sm-10">
					<div class="payment-type-info">
						<c:choose>
							<c:when test="${cartData.paymentType.code eq 'CARD'}">
								<c:if test="${!empty paymentInfo.cardTypeData}">${paymentInfo.cardTypeData.name}, </c:if>
								<c:if test="${!empty paymentInfo.cardNumber}">************${paymentInfo.cardNumber}, </c:if>
								<c:if test="${!empty paymentInfo.expiryYear}">${paymentInfo.expiryMonth}/${paymentInfo.expiryYear}, </c:if>

								<multi-checkout-b2b:deliveryAddress address="${paymentInfo.billingAddress}" />
							</c:when>
							<c:when test="${cartData.paymentType.code eq 'ACCOUNT'}">
								<c:if test="${!empty cartData.paymentType}"> ${cartData.paymentType.displayName}, </c:if>
								<c:if test="${!empty user.unit}"><multi-checkout-b2b:deliveryAddress address="${user.unit.billingAddress}" /></c:if>
							</c:when>
						</c:choose>

					</div>
				</div>
			</div>
		</div>
	</c:if>
</c:if>
