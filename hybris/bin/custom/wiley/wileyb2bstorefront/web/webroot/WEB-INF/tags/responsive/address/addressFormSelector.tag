<%@ attribute name="supportedCountries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String"%>
<%@ attribute name="addressBook" required="false" type="java.lang.String"%>
<%@ attribute name="submitUrl" required="false" type="java.lang.String"%>
<%@ attribute name="showMakeDefault" required="false" type="java.lang.Boolean"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:if test="${showMakeDefault eq null}">
	<c:set var="showMakeDefault" value="true"/>
</c:if>

			<form:form method="post" commandName="addressForm" action="${submitUrl}">
				<form:hidden path="addressId" class="add_edit_delivery_address_id"
							 status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" />
				<input type="hidden" name="bill_state" id="address.billstate" />

				<div id="countrySelector" data-address-code="${addressData.id}"
					 data-country-iso-code="${addressData.country.isocode}" >
					<formElement:formSelectBox formFieldClass="form-field select-component select-form-element" idKey="address.country"
											   labelKey="address.country" path="countryIso" mandatory="true"
											   skipBlank="false" skipBlankMessageKey="address.country"
											   items="${supportedCountries}" itemValue="isocode"
											   selectedValue="${addressForm.countryIso}" />
				</div>
				<div id="i18nAddressForm" class="i18nAddressForm">
					<c:if test="${not empty country}">
						<address:addressFormElements regions="${regions}"
													 country="${country}" />
					</c:if>
				</div>

				<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
					<c:choose>
						<c:when test="${showSaveToAddressBook}">
							<formElement:formCheckbox idKey="saveAddressInMyAddressBook"
													  labelKey="checkout.summary.deliveryAddress.saveAddressInMyAddressBook"
													  path="saveInAddressBook" mandatory="true" wrapperCSS="save-to-address-book gray-bg"/>
						</c:when>
						<c:when test="${showMakeDefault && not addressBookEmpty && not isDefaultAddress}">
							<ycommerce:testId code="editAddress_defaultAddress_box">
								<formElement:formCheckbox idKey="defaultAddress"
														  labelKey="address.default" path="defaultAddress"
														  mandatory="true" wrapperCSS="save-to-address-book"/>
							</ycommerce:testId>
						</c:when>
					</c:choose>
				</sec:authorize>
				<div id="addressform_button_panel" class="my-account-button-wrap">
					<c:choose>
                    					<c:when test="${edit eq true && not addressBook}">
                    						<ycommerce:testId code="multicheckout_saveAddress_button">
                    							<button
                    								class="positive right change_address_button show_processing_message"
                    								type="submit">
                    								<spring:theme code="checkout.multi.saveAddress"
                    									text="Save address" />
                    							</button>
                    						</ycommerce:testId>
                    					</c:when>
						<%--TODO: This logic looks strange, show save button in case addressBook is true ONLY ?--%>
                    					<c:when test="${addressBook eq true}">
                    						<div class="row">
                    							<div class="col-xs-12 col-sm-6">
                    								<ycommerce:testId code="editAddress_saveAddress_button">
                    									<button
                    										class="change_address_button show_processing_message button button-main large"
                    										type="submit">
                    										<spring:theme code="text.button.save" text="Save" />
                    									</button>
                    								</ycommerce:testId>
                    							</div>
                    							<div class="col-xs-12 col-sm-6">
                    								<ycommerce:testId code="editAddress_cancelAddress_button">
                    									<c:url value="${cancelUrl}" var="cancel" />
                    									<a class="button button-main large button-outlined"
                    										href="${cancel}"> <spring:theme code="text.button.cancel"
                    											text="Cancel" />
                    									</a>
                    								</ycommerce:testId>
                    							</div>
                    						</div>
                    					</c:when>
                    </c:choose>
				</div>
			</form:form>
