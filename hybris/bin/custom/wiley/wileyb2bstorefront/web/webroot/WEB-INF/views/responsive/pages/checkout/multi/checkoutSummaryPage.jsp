<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="b2b-multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<spring:url value="/checkout/multi/summary/applyVoucher" var="applyVoucherUrl"/>
<spring:url value="/checkout/multi/summary/removeVoucher" var="removeVoucherUrl"/>

<c:set var="showPlaceOrderButton" value="${true}" scope="request" />
<c:set var="showVoucherForm" value="${true}" scope="request" />
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="order-review-page b2b-checkout">

	<c:set var="showTax" value="true"/>
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<h1 class="page-title icon-lock">
				<spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" />
			</h1>
		</div>
	</div>

		<div class="row">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<div class="order-total-section checkout-desktop">
					<div class="checkout-accordion-content-wrap">
						<div class="order-total-list">
							<div class="order-total-title"><spring:theme code="order.order.totals" text="Order Total"/></div>
							<div class="order-total-item">
								<div class="row">
									<div class="col-xs-12 col-sm-6"><spring:theme code="order.totals.subtotal" text="Order Total"/></div>
									<div class="col-xs-12 col-sm-6 text-right price"><format:price priceData="${cartData.subTotal}"/></div>
								</div>
							</div>
							<c:if test="${cartData.totalDiscounts.value > 0}">
								<div class="order-total-item">
									<div class="row">
										<div class="col-xs-12 col-sm-6"><spring:theme code="basket.page.totals.discount" text="Order Total"/></div>
										<div class="col-xs-12 col-sm-6 text-right price">-<format:price priceData="${cartData.totalDiscounts}"/></div>
									</div>
								</div>
							</c:if>
							<c:if test="${not empty cartData.discountData}">
								<div class="order-total-item">
									<div class="row">
										<div class="col-xs-6 col-sm-6"><spring:theme code="text.voucher.discountCode" />:</div>
										<div class="col-xs-6 col-sm-6 text-right price">${cartData.discountData[0].code}</div>
									</div>
								</div>
							</c:if>
							<c:if test="${cartData.taxCalculated}">
								<div class="order-total-item">
									<div class="row">
										<div class="col-xs-12 col-sm-6">${sessionCountry.taxName}:</div>
										<div class="col-xs-12 col-sm-6 text-right price"><format:price priceData="${cartData.totalTax}"/></div>
									</div>
								</div>
							</c:if>
							<c:if test="${cartData.showDeliveryCost}">
								<div class="order-total-item">
									<div class="row">
										<div class="col-xs-12 col-sm-6"><spring:theme code="basket.page.totals.delivery" text="Delivery:"/></div>
										<div class="col-xs-12 col-sm-6 text-right price"><format:price priceData="${cartData.deliveryCost}" displayFreeForZero="TRUE"/></div>
									</div>
								</div>
							</c:if>
						</div>

						<div class="order-promotion-code">
							<div class="promotion-field">
								<div class="site-form">
									<div class="row">
										<spring:theme var="promotionPlaceholder" code='checkout.summary.promotion.message' />
										<form:form id="applyVoucherForm" action="${applyVoucherUrl}" commandName="voucherForm" method="post">
											<div class="form-field input-group">
												<formElement:formInputBox idKey="cart.voucher.apply" labelKey="" path="discountCode" placeholder="${promotionPlaceholder}" ariaLabel="${promotionPlaceholder}" />
												<div class="input-group-btn">
													<button class="button button-main large"><spring:theme code="basket.apply"/></button>
												</div>
											</div>
										</form:form>
									</div>
									<div class="row">
										<c:forEach items="${cartData.discountData}" var="discountData">
											<form:form action="${removeVoucherUrl}" commandName="voucherForm" method="post">
												<form:hidden path="discountCode" value="${discountData.code}"/>
												<div class="row promo-message">
													<div class="col-xs-12 col-sm-8">
														<c:set var="discountCode"><c:out value="${discountData.code}"/></c:set>
														<c:set var="discountOff">
															<c:choose>
																<c:when test="${not empty discountData.price.formattedValue}">
																	<c:out value="${discountData.price.formattedValue}"/>
																</c:when>
																<c:otherwise>
																	<fmt:formatNumber var="discountOffInt" maxFractionDigits="0" value="${discountData.relativeValue}" />
																	<spring:theme code="basket.discountOffText" arguments="${discountOffInt}"/>
																</c:otherwise>
															</c:choose>
														</c:set>
														<spring:theme code="basket.discountCode.appliedMessage"
															arguments="${discountCode},${discountOff},<span>,</span>" htmlEscape="false"/>
													</div>
													<div class="col-xs-12 col-sm-4">
														<button class="button button-main small pull-right"><spring:theme code="basket.remove" text="Remove"/></button>
													</div>
												</div>
											</form:form>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
						<form:form action="${placeOrderUrl}" id="placeOrderForm1" commandName="placeOrderForm">
							<div class="order-total-info order-total-title">
								<div class="row">
									<div class="col-xs-12 col-sm-6"><spring:theme code="order.totals.total"/></div>
									<div class="col-xs-12 col-sm-6 text-right">
										<c:choose>
											<c:when test="${showTax}">
												<format:price priceData="${cartData.totalPriceWithTax}"/>
											</c:when>
											<c:otherwise>
												<format:price priceData="${cartData.totalPrice}"/>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</div>
							<div class="checkout-total-message">
								<p><spring:theme code="checkout.summary.order.message"/></p>
								<textarea cols="78" rows="6" id="orderMessageInput" maxlength="255"
								aria-label="<spring:theme code='checkout.summary.order.message'/>">${cartData.orderMessage}</textarea>
							</div>
					</div>
					<div class="checkout-full-width-wrap">
						<div class="checkout-accordion-content-wrap">
							<div class="order-terms-and-conds checkbox-component">
								<form:checkbox id="termsConds" path="termsCheck"/>
								<spring:message code="checkout.summary.placeOrder.readTermsAndConditions.popup.link" var="termsAndConditionsPopupLink"/>
								<label for="termsConds">
									<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" text="Terms and Conditions"/>
									&nbsp;<a class="termsAndConditionsLink" title="${termsAndConditionsPopupLink}"
										href="${getTermsAndConditionsUrl}">${termsAndConditionsPopupLink}</a>
								</label>
							</div>
						</div>
					</div>

					<c:if test="${requestSecurityCode}">
	 					<form:input type="hidden" class="securityCodeClass" path="securityCode"/>

	 					<div class="checkout-accordion-content-wrap">
	 						<button type="submit" class="button purchase-main-button"><spring:theme code="checkout.summary.placeOrder"/></button>
	 					</div>
	 				</c:if>

	 				<c:if test="${not requestSecurityCode}">
	 					<div class="checkout-accordion-content-wrap">
	 						<button type="submit" class="button button-main large"><spring:theme code="checkout.summary.placeOrder"/></button>
	 					</div>
	 				</c:if>
				</div>
			</form:form>
		</multi-checkout:checkoutSteps>

			<!-- Terms and Conditions Popup placeholder -->
			<div id="termsAndConditionsPopup" class="modal fade modalWindow"></div>

			<!-- TODO: Add order summary section -->
			<%--<b2b-multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="${showTax}"/>--%>
			<div class="col-xs-12 col-sm-4 col-sm-offset-1">
				<div class="order-brief-section checkout-desktop">
					<b2b-multi-checkout:orderSummarySection cartData="${cartData}" user="${user}" />
					<b2b-multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true" showPickupDeliveryEntries="true" showTax="${showTax}"/>
				</div>
			</div>
		</div>


</template:page>
