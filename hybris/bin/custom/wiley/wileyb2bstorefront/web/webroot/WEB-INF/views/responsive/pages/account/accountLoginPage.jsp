<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="returning-user wrap authorization-support-message-wr">

		<div class="row">
			<div class="col-sm-12">
				<multi-checkout:supportMessage />
			</div>
			<div class="col-sm-12">
				<cms:pageSlot position="CenterContentSlot" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>

</template:page>
