<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="deliveryMethods" required="true"
	type="java.util.List"%>
<%@ attribute name="selectedDeliveryMethodId" required="false"
	type="java.lang.String"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout-common"
	tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:forEach items="${deliveryMethods}" var="deliveryMethod">
	<div class="checkout-delivery-item radio-button-component">
		<multi-checkout-common:deliveryMethodDetails
			deliveryMethod="${deliveryMethod}"
			isSelected="${deliveryMethod.externalCode eq selectedDeliveryMethodId}" />
	</div>
</c:forEach>
