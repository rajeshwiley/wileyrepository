<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="unit" required="true" type="de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData" %>
<%@ attribute name="addresses" required="true" type="java.util.List" %>
<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="showActions" required="false" type="java.lang.Boolean" %>

<spring:url value="/my-company/organization-management/manage-unit/add-address" var="addUnitAddressUrl">
    <spring:param name="unit" value="${unit.uid}"/>
</spring:url>

<div class="unit-item">

	<h2><spring:theme code="text.company.manage.units.address.${type}"/></h2>

    <div class="annotation wrap">
        <div class="row">
            <div class="col-xs-12 col-sm-9">
                <div class="annotation-text">
                    <p><spring:theme code="text.company.manage.units.view.address.${type}.subtitle"/></p>
                </div>
            </div>
            <c:if test="${showActions}">
                <div class="col-xs-12 col-sm-3 text-right">
                    <div class="my-account-button-small">
                        <a href="${addUnitAddressUrl}" class="button create">
                            <button class="button button-outlined large hidden-xs">
                                <spring:theme code="text.company.manage.units.button.create" text="Create"/>
                            </button>
                        </a>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
    <div class="unit-item-table">
        <div class="thead hidden-xs">
            <div class="row">
                <div class="col-sm-2">
                    <spring:theme code="text.company.manage.units.address.line1" text="Address Line 1"/>
                </div>
                <div class="col-sm-2">
                    <spring:theme code="text.company.manage.units.address.line2" text="Address Line 2"/>
                </div>
                <div class="col-sm-2">
                    <spring:theme code="text.company.manage.units.address.towncity" text="Town/City"/>
                </div>
                <div class="col-sm-2">
                    <spring:theme code="text.company.manage.units.address.zipcode" text="Postal Code"/>
                </div>
                <div class="col-sm-2">
                    <spring:theme code="text.company.manage.units.address.country" text="Country"/>
                </div>

                <c:if test="${showActions}">
                    <div class="col-sm-2">
                        <spring:theme code="text.company.manage.units.address.actions" text="Actions"/>
                    </div>
                </c:if>
                
            </div>
        </div>
        <div class="tbody">
            <c:forEach items="${addresses}" var="address">
				<div class="unit-item-row">
					<div class="row">
						<div class="col-xs-12 col-sm-2">
						 <div class="thead visible-xs">
						 	<spring:theme code="text.company.manage.units.address.line1" text="Address Line 1"/>
                         </div>
                         <div class="td">${fn:escapeXml(address.line1)}</div>
						</div>
						<div class="col-xs-12 col-sm-2">
						 <div class="thead visible-xs">
						 	<spring:theme code="text.company.manage.units.address.line2" text="Address Line 2"/>
                         </div>
                         <div class="td">${fn:escapeXml(address.line2)}</div>
						</div>
						<div class="col-xs-12 col-sm-2">
						 <div class="thead visible-xs">
						 	<spring:theme code="text.company.manage.units.address.towncity" text="Town/City"/>
                         </div>
                         <div class="td">${fn:escapeXml(address.town)}</div>
						</div>
						<div class="col-xs-12 col-sm-2">
						 <div class="thead visible-xs">
						 	<spring:theme code="text.company.manage.units.address.zipcode" text="Postal Code"/>
                         </div>
                         <div class="td">${fn:escapeXml(address.postalCode)}</div>
						</div>
						<div class="col-xs-12 col-sm-2">
						 <div class="thead visible-xs">
						 	<spring:theme code="text.company.manage.units.address.country" text="Country"/>
                         </div>
                         <div class="td">${fn:escapeXml(address.country.name)}</div>
						</div>
						<c:if test="${showActions}">
							<div class="col-xs-12 col-sm-2">

								<spring:url
										value="/my-company/organization-management/manage-unit/edit-address/"
										var="editUnitAddressUrl">
									<spring:param name="unit" value="${unit.uid}"/>
									<spring:param name="addressId" value="${address.id}"/>
								</spring:url>

								<a href="${editUnitAddressUrl}">
									<spring:theme code="text.company.manage.units.edit"
												  text="Edit"/>
								</a>
								/
								<spring:url
										value="/my-company/organization-management/manage-unit/remove-address/"
										var="removeUnitAddressUrl">
									<spring:param name="unit" value="${unit.uid}"/>
									<spring:param name="addressId" value="${address.id}"/>
								</spring:url>

								<a href="${removeUnitAddressUrl}">
									<spring:theme code="text.company.manage.units.remove"
												  text="Remove"/>
								</a>
							</div>
						</c:if>
					</div>
				</div>
            </c:forEach>
        </div>
    </div>
</div>