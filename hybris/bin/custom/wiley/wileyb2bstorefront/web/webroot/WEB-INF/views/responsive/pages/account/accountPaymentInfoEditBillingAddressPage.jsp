<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:url value="/my-account/payment-details" var="paymentDetailsPageUrl" />
<h1 class="full-width-gray">
	<spring:theme code="text.account.editBillingAddress"
		text="Edit Billing Address" />
</h1>
<div class="row wrap">
	<div class="col-xs-12 col-sm-4">
		<div class="site-form">
			<form:form method="post"
				commandName="addressForm">
				<form:hidden path="addressId" class="add_edit_delivery_address_id"
					status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" />
				<input type="hidden" name="bill_state" id="address.billstate" />

				<form:hidden path="paymentInfoId" />

				<div id="countrySelector" data-address-code="${addressData.id}"
					data-country-iso-code="${addressData.country.isocode}"
					class="form-field select-component select-form-element">
					<formElement:formSelectBox idKey="address.country"
						labelKey="address.country" path="countryIso" mandatory="true"
						skipBlank="false" skipBlankMessageKey="address.country"
						items="${supportedCountries}" itemValue="isocode"
						selectedValue="${addressForm.countryIso}" />
				</div>
				<div id="i18nAddressForm" class="i18nAddressForm">
					<c:if test="${not empty country}">
						<address:addressFormElements regions="${regions}"
							country="${country}" />
					</c:if>
				</div>

				<div class="my-account-button-wrap">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<button type="submit" class="button button-main large">
								<spring:theme code="text.button.save" text="Save" />
							</button>
						</div>
						<div class="col-xs-12 col-sm-6">
							<button type="button" onclick="location.href='${paymentDetailsPageUrl}'"
								class="button button-main large button-outlined">
								<spring:theme code="text.button.cancel" text="Cancel" />
							</button>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>
</div>


