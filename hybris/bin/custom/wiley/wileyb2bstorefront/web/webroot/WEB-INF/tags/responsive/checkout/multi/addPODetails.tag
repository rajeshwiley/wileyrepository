<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>


<c:if test="${not empty cartData}">
	<div class="modal-wnd-list">
	<c:url value="/checkout/multi/payment-type/choose/save-po-number" var="addPONumberFormAction"/>

	
	<form:form method="post" action="${addPONumberFormAction}" modelAttribute="cartData" id="addPODetailsForm">
		
		<c:forEach items="${cartData.entries}" var="entry">
		
			<!-- Hidden inputs -->
			<form:input path="entries[${entry.entryNumber}].entryNumber" type="hidden" value="${entry.entryNumber}" />
		
			<div class="modal-wnd-list-item">
			
				<c:if test="${entry.deliveryPointOfService == null}">
					<c:url value="${entry.product.url}" var="productUrl" />
					<div class="modal-wnd-list-image">
					  <a href="${productUrl}"> <product:productPrimaryImage product="${entry.product}" format="thumbnail" /></a>
					 </div>
					 <div class="row modal-wnd-list-info">
					<div class="col-xs-12 col-sm-6">
						<div class="modal-wnd-list-title"><a href="${productUrl}">${entry.product.name}</a></div>
						<div class="modal-wnd-list-qty"><spring:theme code="basket.page.qty" />:
							${entry.quantity}</div>
					</div>
					<div class="col-xs-12 col-sm-2">
						<div class="modal-wnd-list-price"><format:price
								priceData="${entry.totalPrice}" displayFreeForZero="true" /></div>
					</div>
					<div class="col-xs-12 col-sm-4">
						<div class="modal-wnd-list-number">
						  <spring:theme code="checkout.multi.paymentType.payByInvoice.poInput.placeholder" var="poNumberPlaceholder" />
						  
						  <formElement:formInputBoxNoLabel idKey="poNumber_${entry.entryNumber}" path="entries[${entry.entryNumber}].poNumber" inputCSS="poNumberInput" 
						      placeholder="${poNumberPlaceholder}" labelKey=""/>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</c:forEach>
		<div class="modal-button-wrap">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                    <button id="payByInvoicePopupContinueButton" class="button button-main medium" type="button" name="paymentType" value="ACCOUNT" >
                        <spring:theme code="checkout.multi.paymentType.payByInvoice.continue" />
                    </button>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <button data-dismiss="modal" aria-label="Close"
                        class="button button-outlined medium"><spring:theme code="checkout.multi.paymentType.payByInvoice.cancel" /></button>
                </div>
            </div>
		</div>
		</form:form>
	</div>
</c:if>
