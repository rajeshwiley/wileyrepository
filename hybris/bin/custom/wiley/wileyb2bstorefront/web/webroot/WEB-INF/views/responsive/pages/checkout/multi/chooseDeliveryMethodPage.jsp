<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="wileycomMulti-checkout" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-shipping-method">

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
                <cms:component component="${feature}" />
            </cms:pageSlot>
            <h1 class="page-title icon-lock">
                <spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" />
            </h1>
        </div>
    </div>
    <div class="row">
        <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">

            <div class="checkout-accordion-content-wrap">
                <ycommerce:testId code="checkoutStepTwo">
                    <form:form id="selectDeliveryMethodForm" commandName="shippingMethodForm" method="post">

                        <div class="checkout-delivery-option-dates">
                            <div class="row">
                                <c:if test="${not empty deliveryMethods}">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="checkout-content-subtitle">
                                            <spring:theme
                                                code="b2b.checkout.summary.deliveryMode.selectDeliveryMethodForOrder"
                                                text="Delivery Options and Arrival Dates"></spring:theme>
                                        </div>
                                        <div class="checkout-delivery-list">
                                            <multi-checkout:deliveryMethodSelector deliveryMethods="${deliveryMethods}"
                                                selectedDeliveryMethodId="${cartData.deliveryMode.externalCode}" />
                                        </div>
                                    </div>
                                </c:if>
                                <div class="col-xs-12 col-sm-6">
                                    <wileycomMulti-checkout:checkoutDeliveryMessage />
                                </div>
                            </div>
                        </div>

                        <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" />

                        <div class="checkout-accordion-button-next">
                            <button id="deliveryMethodSubmit" type="button" class="button button-main large">
                                <spring:theme code="checkout.multi.deliveryMethod.continue" text="Next Step" />
                            </button>
                        </div>

                    </form:form>
                </ycommerce:testId>
            </div>
        </multi-checkout:checkoutSteps>
        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <div class="order-brief-section checkout-desktop">
                <multi-checkout:orderSummarySection cartData="${cartData}" user="${user}" />
                <multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true"
                    showPickupDeliveryEntries="false" showTax="true" />
            </div>
        </div>
    </div>

</template:page>

