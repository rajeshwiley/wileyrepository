<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ attribute name="themeMsgKey" required="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="hasPreviousPage" value="${searchPageData.pagination.currentPage > 0}"/>
<c:set var="hasNextPage" value="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"/>
<c:set var="limit" value="${numberPagesShown}"/>
<c:set var="halfLimit"><fmt:formatNumber value="${limit/2}" maxFractionDigits="0"/></c:set>

<div class="panel-pagination">
	<c:if test="${hasPreviousPage}">
		<spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
			<spring:param name="page" value="${searchPageData.pagination.currentPage - 1}" />
		</spring:url>
		<ycommerce:testId code="searchResults_previousPage_link">
			<div class="pagination-element">
				<a href="${previousPageUrl}" rel="prev" title="<spring:theme code='${themeMsgKey}.linkPreviousPage'/>" class="arrow-left">&nbsp;</a>
			</div>
		</ycommerce:testId>
	</c:if>

	<div class="pagination-element">
		<a class="pagination-item">${searchPageData.pagination.currentPage + 1}</a>
	</div>

	<div class="pagination-element">
		<span class="text"><spring:theme code="${themeMsgKey}.pagination.number.of" text="of"/></span>
	</div>

	<div class="pagination-element active">
		<spring:url value="${searchUrl}" var="lastPageUrl" htmlEscape="true">
			<spring:param name="page" value="${searchPageData.pagination.numberOfPages - 1}" />
		</spring:url>
		<ycommerce:testId code="pageNumber_link">
			<a href="${lastPageUrl}" class="pagination-item">${searchPageData.pagination.numberOfPages}</a>
		</ycommerce:testId>
	</div>

	<c:if test="${hasNextPage}">
		<spring:url value="${searchUrl}" var="nextPageUrl" htmlEscape="true">
			<spring:param name="page" value="${searchPageData.pagination.currentPage + 1}" />
		</spring:url>
		<ycommerce:testId code="searchResults_nextPage_link">
			<div class="pagination-element">
				<a href="${nextPageUrl}" rel="next" title="<spring:theme code='${themeMsgKey}.linkNextPage'/>" class="arrow-right">&nbsp;</a>
			</div>
		</ycommerce:testId>
	</c:if>
</div>
