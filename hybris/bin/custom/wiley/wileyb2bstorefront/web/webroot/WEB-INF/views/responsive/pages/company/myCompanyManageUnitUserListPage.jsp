<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="company" tagdir="/WEB-INF/tags/responsive/company"%>
<%@ taglib prefix="wileycomNav" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/nav"%>

<spring:theme code="text.company.select.action.label" var="selectAction" />
<spring:theme code="text.company.deselect.action.label" var="deselectAction"/>
<spring:theme code="text.company.disable.action.label" var="disableAction"/>

<c:if test="${empty cancelUrl}">
	<c:url value="/my-company/organization-management/manage-unit" var="cancelUrl"/>
</c:if>

<template:page pageTitle="${pageTitle}">

<div id="globalMessages">
	<common:globalMessages/>
</div>
<div class="container my-company all-users wrap">
	<div class="row">
		<company:myCompanyNav selected="units"/>
		<div class="col-xs-12 col-sm-9">
			<h1 class="hidden-xs"><spring:theme code="text.company.manage.units.${action}.title" arguments="${unit.name}"/></h1>
			<div class="my-company-wrap">
			
				<div class="annotation">
					<p><spring:theme code="text.company.manage.units.${action}.subtitle"/></p>
				</div>
				<div class="control-panel top">
					<div class="row">
						<div class="col-xs-4 col-sm-8">
							<div class="total-results"><spring:theme code="text.company.${action}.page.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}"/></div>
						</div>
						<div class="col-xs-8 col-sm-4 text-right">
							<div clas="row">
								<div clas="col-xs-12">
									<wileycomNav:pagination searchPageData="${searchPageData}" searchUrl="${baseUrl}/${action}?unit=${param.unit}&role=${param.role}" isPaginationModeUrlVisible="true"/>
								</div>
							</div>
						</div>
					</div>
				</div>
								
				<div class="users-table">
					<div class="thead hidden-xs">
						<div class="row">
							<div class="col-sm-4"><spring:theme code="text.company.name.name"/></div>
							<div class="col-sm-4"><spring:theme code="text.company.roles.name"/></div>
							<div class="col-sm-4"><spring:theme code="text.company.actions.name"/></div>
						</div>						
					</div>
											
					<div class="tbody">
						<c:forEach items="${searchPageData.results}" var="user">
							<spring:url value="/my-company/organization-management/manage-users/details"
										var="viewUrl">
								<spring:param name="user" value="${user.uid}"/>
							</spring:url>
							<spring:url value="${baseUrl}/members/select/"
										var="selectUrl">
								<spring:param name="user" value="${user.uid}"/>
								<spring:param name="role" value="${param.role}"/>
							</spring:url>
							<spring:url value="${baseUrl}/members/deselect/"
										var="deselectUrl">
								<spring:param name="user" value="${user.uid}"/>
								<spring:param name="role" value="${param.role}"/>
							</spring:url>
										
							<div class="user-item">
								<div class="row">
									<div class="col-xs-12 col-sm-4">
										<div class="row">
											<div class="col-xs-6 thead visible-xs"><spring:theme code="text.company.name.name"/></div>
											<div class="col-xs-6 col-sm-12"><a href="${viewUrl}" title="">${user.name}</a></div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="row">
											<div class="col-xs-6 thead visible-xs"><spring:theme code="text.company.roles.name"/></div>
											<div id="roles-${user.normalizedUid}" class="col-xs-6 col-sm-12">
												<c:forEach items="${user.roles}" var="role">
													<p><spring:theme code="b2busergroup.${role}.name"/></p>
												</c:forEach>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="row">
											<div class="col-xs-6 thead visible-xs"><spring:theme code="text.company.actions.name"/></div>
											<div class="col-xs-6 col-sm-12">
												<p>
													<c:choose>
														<c:when test="${user.active}">
															<span id="selection-${user.normalizedUid}">
																<c:choose>
																	<c:when test="${user.selected}">
																		${selectAction} /
																		<a href="#"
																			url="${deselectUrl}"
																			class="deselectUser">
																			${deselectAction}
																		</a>
																	</c:when>
																	<c:otherwise>
																		<a href="#" url="${selectUrl}" class="selectUser">${selectAction}</a> / ${deselectAction}
																	</c:otherwise>
																</c:choose>
															</span>
														</c:when>
														<c:otherwise>
															<spring:theme code="text.user.disabled"/>
														</c:otherwise>
													</c:choose>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				
				<div class="control-panel">
					<div class="row">
						<div class="col-xs-4 col-sm-8">
							<div class="total-results"><spring:theme code="text.company.${action}.page.totalResults" arguments="${searchPageData.pagination.totalNumberOfResults}"/></div>
						</div>
						<div class="col-xs-8 col-sm-4 text-right">
							<div clas="row">
								<div clas="col-xs-12">
									<wileycomNav:pagination searchPageData="${searchPageData}" searchUrl="${baseUrl}/${action}?unit=${param.unit}&role=${param.role}" isPaginationModeUrlVisible="true"/>
								</div>
							</div>
						</div>
					</div>
				</div>

				<c:if test="${empty searchPageData.results}">
					<p><spring:theme code="text.company.noentries"/></p>
				</c:if>
			</div>
		</div>
	</div>
</div>
	
	
	
<c:url value="${baseUrl}/members" var="membersActionLink" />
<script id="enableDisableLinksTemplate" type="text/x-jquery-tmpl">
	{{if selected}}
	${selectAction} / <a href="#"
				url="${membersActionLink}/deselect/?user={{= id}}&role=${param.role}"
				class="deselectUser">${deselectAction}</a>
	{{else}}
	<a href="#"
		url="${membersActionLink}/select/?user={{= id}}&role=${param.role}"
		class="selectUser">${selectAction}</a> / ${deselectAction}
	{{/if}}
</script>

<script id="userRolesTemplate" type="text/x-jquery-tmpl">
	{{each displayRoles}}
		<p>
			{{= $value}}
		</p>
	{{/each}}
</script>
</template:page>
