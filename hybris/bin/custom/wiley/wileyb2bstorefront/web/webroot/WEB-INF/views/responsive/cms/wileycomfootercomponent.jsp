<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<c:if test="${not empty navigationNodes }">
<div class="container">
	<div class="row">
		<c:forEach items="${navigationNodes}" var="node">
			<c:if test="${node.visible}">
				<div class="col-sm-3">
					<div class="main-footer-links">

						<c:forEach items="${node.links}" step="${component.wrapAfter}"
							varStatus="i">

							<c:if test="${component.wrapAfter > i.index}">
								<div class="footer-links-title">${node.title}</div>
							</c:if>
							<ul class="footer-links-items">
								<c:forEach items="${node.links}" var="childlink"
									begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
									<cms:component component="${childlink}"
										evaluateRestriction="true" element="li" />
								</c:forEach>
							</ul>
						</c:forEach>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
</div>
</c:if>
<div class="main-footer-copyright">
	<div class="container">
		<div class="footer-copyright-text">${notice}</div>
		<a class="footer-logo" href="${component.footerLogo.urlLink}"
			title="${component.footerLogo.media.altText}"> <img src="${component.footerLogo.media.url}"
			alt="Wiley">
		</a>
	</div>
</div>
