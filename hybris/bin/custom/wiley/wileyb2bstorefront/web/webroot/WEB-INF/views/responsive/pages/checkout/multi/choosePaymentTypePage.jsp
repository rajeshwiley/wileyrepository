<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="b2b-multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-payment b2b-checkout">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
                <cms:component component="${feature}" />
            </cms:pageSlot>
            <h1 class="page-title icon-lock">
                <spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" />
            </h1>
        </div>
    </div>

    <div class="row">
        <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
            <b2b-multi-checkout:paymentTypeForm />
        </multi-checkout:checkoutSteps>

        <!-- TODO: Add order summary section -->

        <div class="col-xs-12 col-sm-4 col-sm-offset-1">
            <div class="order-brief-section checkout-desktop">
                <b2b-multi-checkout:orderSummarySection cartData="${cartData}" user="${user}" />
                <b2b-multi-checkout:checkoutOrderDetails cartData="${cartData}" showShipDeliveryEntries="true"
                    showPickupDeliveryEntries="true" showTax="true" />
            </div>
        </div>
    </div>
</template:page>
