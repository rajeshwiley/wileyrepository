<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:theme code="text.addToCart" var="addToCartText"/>

<c:set var="productSummaryFragment">
	<c:if test="${not empty product.authors}">
		<spring:theme code="product.product.details.authors" var="productAuthorsText"/>
		<p>${productAuthorsText}: ${product.authors}</p>
	</c:if>
	<c:if test="${not empty product.isbn}">
		<spring:theme code="product.product.details.isbn" var="productIsbnText"/>
		<p>${productIsbnText}: ${product.isbn}</p>
	</c:if>
	<c:if test="${not empty product.subtype}">
		<spring:theme code="product.product.details.type" var="productTypeText"/>
		<spring:theme code="product.product.details.type.${product.subtype}" var="productTypeValueText"/>
		<p>${productTypeText}: ${productTypeValueText}</p>
	</c:if>
	<c:if test="${not empty product.dateImprint}">
		<spring:theme code="product.product.details.available" var="productAvailableText"/>
		<p>${productAvailableText}: <fmt:formatDate value="${product.dateImprint}" pattern="MMM, yyyy" /></p>
	</c:if>
</c:set>

<div class="container product-description">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="product-details visible-xs">
				<h2>${product.name}</h2>
				<div class="product-summary">
					${productSummaryFragment}
				</div>
			</div>
			<product:productImagePanel product="${product}" galleryImages="${galleryImages}"/>
		</div>
		<div class="col-xs-12 col-sm-8">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-push-6">
					<div class="product-add-to-cart">
						<cms:pageSlot position="AddToCart" var="component">
							<cms:component component="${component}"/>
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-pull-6">
					<div class="product-details hidden-xs">
						<h2>${product.name}</h2>
						<div class="product-summary">
							${productSummaryFragment}
						</div>
					</div>
					<div class="product-additional-inform">
					    ${product.description}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
