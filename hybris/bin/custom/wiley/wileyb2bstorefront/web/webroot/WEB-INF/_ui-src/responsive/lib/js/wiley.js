// put custom site specific js here. This file is similar to js files which we use for welstorefront and agsstorefront.
if (!wiley) {var wiley = {};}

/* Check tablet and mobile devices */
wiley.checkDevice = function() {
	var isDevice = {
		deviceIOS: function() {
			return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i);
		},
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/Lumia/i) ;
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i)|| navigator.userAgent.match(/BB10; Touch/);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		any: function() {
			return (isDevice.deviceIOS() || isDevice.Android() || isDevice.Windows() || isDevice.BlackBerry() || isDevice.Opera());
		}
	}

	return isDevice.any() ? true : false;
}
/* End check tablet and mobile devices */

/* Viewport fix for iPad */
wiley.iPadViewport = function() {
	var viewport = $("meta[name='viewport']");

	if(navigator.userAgent.match(/iPad/i)) {
		viewport.attr("content", "width=1024");
	}
};
/* End viewport fix for iPad */

wiley.mediaGallery = function() {
	var recommendedItems = $("#recommended-items");
	var sync1 = $("#pdp-carousel-sync1");
	var sync2 = $("#pdp-carousel-sync2");

	if(recommendedItems.length) {
		recommendedItems.owlCarousel({
			items: 4,
			itemsDesktop: false,
			itemsDesktopSmall: false,
			itemsTablet: false,
			itemsTabletSmall: false,
			itemsMobile: [640, 2],
			navigation: true,
			navigationText : false,
			pagination: false
		});
	}

	if(sync1.length) {
		sync1.owlCarousel({
			singleItem: true,
			slideSpeed: 1000,
			navigation: false,
			pagination: false,
			afterAction: syncPosition,
			responsiveRefreshRate: 200
		});
	}

	if(sync2.length) {
		sync2.owlCarousel({
			items: 4,
			pagination: false,
			navigation: true,
			navigationText : false,
			itemsDesktop: false,
			itemsDesktopSmall: false,
			itemsTablet: false,
			itemsMobile: false,
			responsiveRefreshRate : 100,
			afterInit : function(el) {
				el.find(".owl-item").eq(0).addClass("synced");
			}
		});
	}

	function syncPosition(el) {
		var current = this.currentItem;
		sync2
			.find(".owl-item")
			.removeClass("synced")
			.eq(current)
			.addClass("synced");

		if($("#sync2").data("owlCarousel") !== undefined) {
			center(current)
		}
	}

	sync2.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});

	function center(number) {
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;

		for(var i in sync2visible) {
			if(num === sync2visible[i]) {
				found = true;
			}
		}

		if(found === false) {
			if(num > sync2visible[sync2visible.length-1]) {
				sync2.trigger("owl.goTo", num - sync2visible.length+2)
			} else {
				if(num - 1 === -1) {
					num = 0;
				}
				sync2.trigger("owl.goTo", num);
			}
		} else if(num === sync2visible[sync2visible.length-1]) {
			sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]) {
			sync2.trigger("owl.goTo", num-1)
		}
	}
};

wiley.showDatePicker = function() {
	var datepicker = $(".date-picker");
	var datepickerIcon;

	if(wiley.isMobileDevice()) {
		if(datepicker.length) {
			datepicker.each(function () {

				$(this).attr("type", "date");
				datepickerIcon = $(this).parent().find("label");

				datepickerIcon.unbind("click.datepicker");
				$(this).unbind("change.datepicker");
				$(this).datepicker("destroy");

				$(this).bind("blur.inputDate", function() {

					var dateValue = $(this).val();
					var newDate = new Date(dateValue);
					var formattedDate = newDate.getMonth()+1 + "/" + newDate.getDate() + "/" + newDate.getFullYear();
					$(".date-desired").val(formattedDate);
				});
			});
		}

	} else {
		if(datepicker.length) {
			var dateToday = new Date();

			datepicker.each(function () {
				$(this).attr("type", "text");
				$(this).unbind("blur.inputDate");

				datepickerIcon = $(this).parent().find("label");

				$(this).datepicker({
					minDate: dateToday,
					showOtherMonths: true,
					selectOtherMonths: true
				});

				// toggle show/hide date picker by click icon
				datepickerIcon.bind("click.datepicker", function(event) {
					event.preventDefault();

					return $(this).datepicker("widget").is(":visible") ? $(this).off("focus").datepicker("hide") : $(this).on("focus").datepicker("show");
				});

				/* Add mask on keyboard changes */
				var maskBlock = $("<span></span>").text("MM/dd/yyyy").addClass("date-mask-string");
				var maskParentBlock = $(this).parent();
				var maskString =  maskBlock.text();

				var maskChars = maskString.split("");
				maskBlock.empty();

				$.each(maskChars, function (i) {
					var span = document.createElement("span");
					span.innerText = maskChars[i];
					maskBlock.append(span);
				});

				function displayMask() {
					var countValues = datepicker.val().length;
					maskBlock.find("span").css("opacity", 1);
					maskBlock.find("span:lt(" + countValues + ")").css("opacity", 0);
				}

				function removeMask() {
					maskParentBlock.find(".date-mask-string").remove();
				}

				$(this).on("keyup", function() {
					if(maskParentBlock.children(".date-mask-string").length > 0) {
						displayMask();
					} else {
						maskParentBlock.append(maskBlock);
						displayMask();
					}
				});

				$(this).bind("change.datepicker", function(event) {
					removeMask();

					var datePickerValue = $(this).val();
					$(".date-desired").val(datePickerValue);

				});
				/* Add mask on keyboard changes */
			});
		}
	}
};

wiley.distroyLaptopCustomSelect = function() {
	var selects = $(".select-component");
	var select, selectComponent, uiSelectMenuButton;

	if (selects.length) {
		selects.each(function () {
			selectComponent = $(this);
			uiSelectMenuButton = selectComponent.find(".ui-selectmenu-button");

			if(uiSelectMenuButton.length) {
				selectComponent.find("select").selectmenu("destroy");
				wiley.displayCustomMobileSelect(selectComponent);
			}
		});
	}
};

wiley.distroyMobileCustomSelect = function() {
	var selects = $(".select-component");
	var em,select, selectComponent;

	if (selects.length) {
		selects.each(function () {
			selectComponent = $(this);
			em = selectComponent.find("em");

			if(em.length) {
				em.remove();
				selects.removeClass("device");
				wiley.displayCustomSelect(selectComponent);
			}
		});
	}
};

wiley.displayCustomMobileSelect = function(selectComponent) {
	var selectComponent = selectComponent;
	var select = selectComponent.find("select");
	var em = selectComponent.find("em");
	var optionSelected,countryBanner;

	selectComponent.addClass("device");

	if(!em.length) {
		em = $("<em></em>");
		countryBanner = selectComponent.data("banner");

		if(countryBanner) {
			em.addClass("inner-banner").addClass(countryBanner);
		}

		selectComponent.prepend(em);
	}

	optionSelected = select.find('option:selected').text();
	em.text(optionSelected);
};

wiley.displayCustomSelect = function(selectComponent) {
	var selectComponent = selectComponent;
	var select = selectComponent.find("select");

	if(wiley.isMobileDevice()) {

		wiley.displayCustomMobileSelect(selectComponent);

		selectComponent.on("change", "select", function () {
			wiley.displayCustomMobileSelect(selectComponent);
		});

		selectComponent.on("touchstart", "select", function () {
			selectComponent.addClass("select-open");
		});

		selectComponent.on("blur", "select", function () {
			selectComponent.removeClass("select-open");
		});

	} else {

		select.selectmenu({
			width: 'inherit',
			create: function( event, ui ) {
				var countryBanner = selectComponent.data("banner");

				if(countryBanner) {
					var element = event.target;
					$(element).parent().find(".ui-selectmenu-button").addClass("inner-banner").addClass(countryBanner);
				}
			},

			open: function( event, ui ) {
				selectComponent.addClass("select-open");
				$('.ps-scrollbar-container').perfectScrollbar("update"); // visible perfectScrollbar by click on menu button
			},

			close: function( ) {
				selectComponent.removeClass("select-open");
			}
		}).selectmenu("menuWidget").addClass("ps-scrollbar-container always-visible").perfectScrollbar();
	}
};

wiley.onLoadCustomSelect = function() {
	var selectComponent;
	var selects;

	selects = $(".select-component");

	if (selects.length) {
		selects.each(function() {
			selectComponent = $(this);
			wiley.displayCustomSelect(selectComponent);
		})
	}
};

wiley.collapsePurchaseOption = function() {
	var purchaseAccordion = $("#purchase-accordion");

	if(purchaseAccordion.length) {
		var purchaseAccordionItems = purchaseAccordion.find(".purchase-accordion-heading");
		purchaseAccordionItems.each(function() {
			$(this).on("click", ".heading-center, .heading-left", function (e) {
				$(this).closest(".purchase-accordion-heading").toggleClass("current");
			});
		});
	}
};

wiley.onLoadMobileMyAccountMenu = function() {
	var button = $('.my-account-link');
	var closeButton = button.find("button.close");

	if (button.length && wiley.isMobileView) {
		button.find('> a').on('touchstart click', function (e) {
			e.preventDefault();
			button.addClass('shown');
		});

		closeButton.on('touchstart click', function(e) {
			e.preventDefault();
			button.removeClass('shown');
		});
	}
};

wiley.isMobileDevice = function() {
	return wiley.isMobileView || wiley.checkDevice();
};

wiley.triggerPopoverArrow = function() {
	var popoverId = $(this).attr('aria-describedby');
	var arrow = $(this).parent().find('.arrow');

	if($('#'+popoverId).length) {
		arrow.show();
	} else {
		arrow.hide();
	}
};

wiley.initPopover = function() {
	var popoverElements = $('[data-toggle="popover"]');
	var arrow, popoverWrap;

	if(popoverElements.length) {
		popoverElements.each(function() {
			$(this).popover({
				placement: "top",
				html: "true",
				container: ".purchase-options",
				animation: false
			});

			if(!$(this).parent('.popover-wrap').length) {
				popoverWrap = $("<span></span>").addClass("popover-wrap");
				$(this).wrap(popoverWrap);
				arrow = $("<span></span>").addClass("arrow");
				$(this).parent().prepend(arrow);
			}

			$(this).on('mouseenter mouseleave focusin focusout', wiley.triggerPopoverArrow);
		});
	}
};

wiley.destroyPopover = function() {
	var popoverElements = $('[data-toggle="popover"]');

	if(popoverElements.length) {
		popoverElements.each(function() {
			$(this).popover('destroy');
			$(this).parent().find('.arrow').hide();
			$(this).off('mouseenter mouseleave focusin focusout', wiley.triggerPopoverArrow);
		});
	}
};

wiley.animateTermsAndCondAnchor = function() {
	var termsConditionsPage = $('.terms-conditions');
	var termsAndCondNav = termsConditionsPage.find('.terms-and-cond-nav');
	var backToTop = termsConditionsPage.find('.back-to-top');
	var termsAndConditionsModal = $('#termsAndConditionsModal');

	termsAndCondNav.on('click', 'a', function(event) {
		event.preventDefault();

		var anchor =  $(this).data('anchor');
		var anchorTag =  $("a[name='"+ anchor +"']");
		var top = anchorTag.position().top;
		$('html, body').stop().animate({ scrollTop : top }, 'slow');

	});

	if(!termsAndConditionsModal.length) {
		backToTop.each(function() {
			backToTop.on('click', 'a', function(event) {
				event.preventDefault();
				$('html, body').stop().animate({ scrollTop : 0 }, 'slow');
			});
		});
	}
};

wiley.updateCheckboxes = function() {
	$.each($("input[type=checkbox]"), function(index, value) {
		$(value).prop("checked", $(value).prop('defaultChecked'));
	});
};

wiley.onLoadTriggerClick = function() {
	$(document).on("keydown", ".heading-center", function(event) {
		var keycode = ( event.keyCode || event.which);

		if (keycode == '13') {
			$(this).trigger('click');
		}
	});
};

wiley.onLoadTriggerHover = function() {
	$(document).on("keydown", ".my-account-link", function(event) {
		var keycode = ( event.keyCode || event.which);

		if (keycode == '13' && $(this).hasClass("my-account-link")) {
			event.preventDefault();
			$(this).toggleClass('shown');
		}
	});
};

wiley.imageChecker = function() {
	var errorImg = $(".product-list-image img");
	errorImg.on("error", function () {
		$(this).attr("src", "~/../responsive/common/images/no-image.png");
	});
};

wiley.imageChecker();

$(document).ready(function() {
	wiley.isMobileView = ($(window).width() <= 640);
	wiley.mediaGallery();
	wiley.showDatePicker();
	wiley.onLoadCustomSelect();
	wiley.collapsePurchaseOption();
	wiley.onLoadMobileMyAccountMenu();
	wiley.initPopover();
	wiley.animateTermsAndCondAnchor();
	wiley.iPadViewport();
	wiley.updateCheckboxes();
	wiley.onLoadTriggerClick();
	wiley.onLoadTriggerHover();
});

$(window).resize(function() {
	wiley.isMobileView = ($(window).width() <= 640);
	wiley.showDatePicker();
	wiley.destroyPopover();
	wiley.initPopover();

	if(wiley.isMobileDevice()) {
		wiley.distroyLaptopCustomSelect();

	} else {
		wiley.distroyMobileCustomSelect();
	}
});
