<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="users" required="true" type="java.util.List" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ attribute name="createUrl" required="true" type="java.lang.String" %>
<%@ attribute name="editUrl" required="true" type="java.lang.String" %>
<%@ attribute name="role" required="true" type="java.lang.String" %>
<%@ attribute name="subtitleKey" required="true" type="java.lang.String" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="unit-item">
	<div class="row">
		<div class="col-xs-9 col-sm-12">
			<h2><spring:theme code="text.company.manage.units.header.${action}" text="${action}"/></h2>
		</div>
		<div class="col-xs-3 visible-xs text-right">
			<button class="button button-edit" aria-label="<spring:theme code='text.account.addressBook.edit.billing.address'/>" onclick="window.location.href='${editUrl}'"></button>
		</div>
	</div>
	<div class="annotation wrap">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<div class="annotation-text">
					<p><spring:theme code="${subtitleKey}"/></p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 text-right">
				<div class="my-account-button-small">
					<button class="button button-edit hidden-xs" aria-label="<spring:theme code='text.account.addressBook.edit.billing.address'/>" onclick="window.location.href='${editUrl}'"></button>
					<button class="button button-outlined large hidden-xs" onclick="window.location.href='${createUrl}&role=${role}'">
						<spring:theme code="text.company.manage.units.users.button.createnew"/>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="unit-item-table">
		<div class="thead hidden-xs">
			<div class="row">
				<div class="col-sm-2"><spring:theme code="text.company.manage.units.user.name" text="Name"/></div>
				<div class="col-sm-2"><spring:theme code="text.company.manage.units.user.email" text="Email"/></div>
				<div class="col-sm-2"><spring:theme code="text.company.manage.units.user.roles" text="Roles"/></div>
				<div class="col-sm-2"><spring:theme code="text.company.manage.units.user.actions" text="Actions"/></div>
			</div>
		</div>
		<div class="tbody">
			<c:forEach items="${users}" var="user">
				<spring:url value="/my-company/organization-management/manage-unit/edituser/" var="editUserUrl">
					<spring:param name="unit" value="${unit.uid}"/>
					<spring:param name="user" value="${user.uid}"/>
				</spring:url>
				<spring:url value="/my-company/organization-management/manage-unit/viewuser/" var="viewUserUrl">
					<spring:param name="unit" value="${unit.uid}"/>
					<spring:param name="user" value="${user.uid}"/>
				</spring:url>
				<spring:url value="/my-company/organization-management/manage-unit/members/confirm/remove" var="removeUserUrl">
					<spring:param name="unit" value="${unit.uid}"/>
					<spring:param name="user" value="${user.uid}"/>
					<spring:param name="role" value="${role}"/>
				</spring:url>
				<spring:url value="/my-company/organization-management/manage-unit/members/confirm/disable" var="disableUserUrl">
					<spring:param name="unit" value="${unit.uid}"/>
					<spring:param name="user" value="${user.uid}"/>
				</spring:url>
				<spring:url value="/my-company/organization-management/manage-unit/members/enable" var="enableUserUrl">
					<spring:param name="user" value="${user.uid}"/>
				</spring:url>
				<div class="unit-item-row">
					<div class="row">
						<div class="col-xs-12 col-sm-2">
							<div class="thead visible-xs"><spring:theme code="text.company.manage.units.user.name" text="Name"/></div>
							<div class="td">
								<a href="${viewUserUrl}">${fn:escapeXml(user.firstName)}&nbsp;${fn:escapeXml(user.lastName)}</a>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="thead visible-xs"><spring:theme code="text.company.manage.units.user.email" text="Email"/></div>
							<div class="td">${fn:escapeXml(user.uid)}</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="thead visible-xs"><spring:theme code="text.company.manage.units.user.roles" text="Roles"/></div>
							<div class="td">
								<c:forEach items="${user.roles}" var="group">
									<p><spring:theme code="b2busergroup.${group}.name"/></p>
								</c:forEach>
							</div>
						</div>
						<div class="col-xs-12 col-sm-2">
							<div class="thead visible-xs"><spring:theme code="text.company.manage.units.user.actions" text="Actions"/></div>
							<div class="td">
								<a href="${editUserUrl}"><spring:theme code="text.company.manage.unit.user.edit" text="Edit"/></a>
								/
								<c:choose>
								<c:when test="${(fn:length(user.roles) eq 1)}">
								  <c:choose>
								    <c:when test="${user.active}">
								      <a href="${disableUserUrl}">
									 	<spring:theme code="text.company.manage.unit.user.disable"/>
									  </a>
								    </c:when>
								    <c:otherwise>
                                     <form:form name="enableUnitUserForm" action="${enableUserUrl}">
                                         <a href="javascript:document.forms['enableUnitUserForm'].submit()">
                                           <spring:theme code="text.company.manage.unit.user.enable"/>
                                        </a>
                                     </form:form>
								    </c:otherwise>
								  </c:choose>
								</c:when>
								<c:otherwise>
								<a href="${removeUserUrl}">
									<spring:theme code="text.company.manage.unit.user.remove"/>
								</a>
								</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<button class="button button-outlined large visible-xs" onclick="window.location.href='${createUrl}&role=${role}'">
		<spring:theme code="text.company.manage.units.users.button.createnew"/>
	</button>
</div>



