<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<sec:authorize ifAnyGranted="ROLE_ANONYMOUS">
    <button class="button button-outlined large" disabled="disabled">
        <spring:theme code="basket.register.or.login.to.add.to.cart"/>
    </button>
</sec:authorize>

<sec:authorize ifNotGranted="ROLE_ANONYMOUS">
    <c:choose>
        <c:when test="${product.purchasable}">
            <c:url value="${url}" var="addToCartUrl"/>
            <form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">

                <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
                    <product:productPricePanel product="${product}"/>
                </ycommerce:testId>

                <c:choose>
                    <c:when test="${product.doesProductHaveQuantity}">
                        <div class="product-qty">
                            <label for="qty"><spring:theme code="basket.add.to.qty"/></label>
                            <input type="text" maxlength="3" size="1" id="qty" name="qty" class="qty js-qty-selector-input" value="1" aria-label="quantity">
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div><spring:theme code="basket.add.to.qty"/>: 1</div>
                    </c:otherwise>
                </c:choose>

                <c:if test="${!product.isDigital}">
                    <div class="product-availability">
                        <c:forEach items="${product.inventoryStatus}" var="inventoryStatusItem">
                            <spring:theme code="product.add.to.availability"/>: <spring:theme code="product.add.to.availability.status.${inventoryStatusItem.statusCode}"/>
                        </c:forEach>
                    </div>
                </c:if>

                <input type="hidden" name="productCodePost" value="${product.code}"/>
                    <div class="add-to-cart">
                        <button type="submit" class="button button-main large js-add-to-cart">
                            <spring:theme code="basket.add.to.basket"/>
                        </button>
                    </div>
            </form:form>
        </c:when>
        <c:otherwise>
            <button class="button button-outlined large" disabled="disabled">
                <spring:theme code="basket.register.out.of.stock"/>
            </button>
        </c:otherwise>
    </c:choose>
</sec:authorize>
