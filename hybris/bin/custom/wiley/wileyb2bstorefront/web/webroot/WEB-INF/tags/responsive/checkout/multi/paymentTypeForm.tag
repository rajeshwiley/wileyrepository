<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
	

	<div class="checkout-accordion-content-wrap">
		<div class="checkout-payment-buttons">
          <div class="row">
            
            <div class="col-xs-12 col-sm-6">
              <button data-toggle="modal" data-target="#payByInvoice" class="button button-main large"><spring:theme code="checkout.multi.paymentType.invoice" text="Pay By Invoice"></spring:theme></button>
            </div>
            
            <form:form id="selectPaymentTypeForm" commandName="paymentTypeForm" action="${request.contextPath}/checkout/multi/payment-type/choose" method="post">
	            <div class="col-xs-12 col-sm-6">
	              <button type="submit" name="paymentType" value="CARD" class="button button-secondary large add-another-card">
	              	<spring:theme code="checkout.multi.paymentType.creditcard" text="Pay By Credit Card"/>
	              </button>
	              <div class="checkout-payment-credit-cards">
	                <div class="credit-card"></div>
	              </div>
	            </div>
			</form:form>
			
          </div>
        </div>
     </div>
     
	 <input id="poNumbersPopUpOpenField" type="hidden" value="${poNumbersPopUpOpen}" />
     <div id="payByInvoice" role="dialog" class="modal fade modalWindow">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-xs-10 col-sm-10">
							<div class="modal-title"><spring:theme code="checkout.multi.paymentType.invoice" text="Pay By Invoice"/></div>
						</div>
						<div class="col-xs-2 col-sm-2">
							<button type="button" data-dismiss="modal" aria-label="Close"
								class="close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				<div id="payByInvoiceModalBody" class="modal-body">
					<multi-checkout:addPODetails cartData="${cartData}" showTax="false" />
				</div>
			</div>
		</div>
	</div>

