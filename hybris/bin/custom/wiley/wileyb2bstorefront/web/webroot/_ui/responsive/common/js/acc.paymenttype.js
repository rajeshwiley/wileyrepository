ACC.paymentType = {
	
	_autoload: [
		"bindPayByInvoicePopupContinueButton"
	],

	bindPaymentTypeSelect: function ()
	{
		$("#paymentTypeSelect").change(function()
		{
			ACC.paymentType.showBillingAddressForm();
		});
	},
	
	bindPayByInvoicePopupContinueButton: function() {
		$("#payByInvoicePopupContinueButton").on("click", ACC.paymentType.payByInvoicePopupContinueClicked);
	},

	showBillingAddressForm: function() {
		var paymentTypeSelected = $("#paymentTypeSelect").val();
		var choosePaymentButton = $("#choosePaymentType_continue_button");
		var billingAddressContainer = $("#ccBillingAddressContainer");
		$('#ccBillingAddressSave').on("click", ACC.paymentType.billingAddressSaveButtonHandler);
		if (paymentTypeSelected == "CARD") {
			choosePaymentButton.on("click", ACC.paymentType.choosePaymentButtonHandler)
			$.ajax({
				url: ACC.config.encodedContextPath + '/checkout/multi/payment-type/show-billing-address-form',
				dataType: "html"
			}).done(function(data) {
				billingAddressContainer.html(data);
			});
		} else {
			$("#ccBillingAddressForm").remove();
			choosePaymentButton.off("click", ACC.paymentType.choosePaymentButtonHandler)
		}
	},

	choosePaymentButtonHandler: function(event) {
		event.preventDefault();
	},
	
	payByInvoicePopupContinueClicked: function(event) {
		var addPODetailsForm = $("#addPODetailsForm");
		var options = {
			url: ACC.config.encodedContextPath + '/checkout/multi/payment-type/choose/validate-po-numbers',
			data: addPODetailsForm.serialize(),
			type: 'POST',
			success: function (data)
			{
				//If there are no validation errors
				if(data.indexOf('has-error') === -1)
				{
					//Submit form and proceed to the next step
					$("#addPODetailsForm").get(0).submit();
				}
				else
				{
					$('#payByInvoiceModalBody').html(data);
					ACC.paymentType.bindPayByInvoicePopupContinueButton();
					
					//clear all PO number Inputs with error
					ACC.paybyinvoice.bindPoNumberClear();
					ACC.paybyinvoice.bindPoNumberCopy();
				}
			}
		};

		$.ajax(options);
	}

}


$(document).ready(function ()
{
	ACC.paymentType.bindPaymentTypeSelect();
	ACC.paymentType.showBillingAddressForm();
});




