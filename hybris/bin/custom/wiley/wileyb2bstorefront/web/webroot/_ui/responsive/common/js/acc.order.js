ACC.order = {

	_autoload: [
	    "backToOrderHistory",
	    "backToOrders",
	    "bindEmailInvoiceSubmitButton"
	],

	backToOrderHistory: function(){
		$(".orderBackBtn").on("click", function(){
			var sUrl = $(this).data("backToOrders");
			window.location = sUrl;
		});
	},
	backToOrders: function(){
		$(".orderTopBackBtn").on("click", function(){
			var sUrl = $(this).data("backToOrders");
			window.location = sUrl;
		});
	},

	bindEmailInvoiceSubmitButton : function() {
		$('#emailInvoiceSubmitButton').bind("click", function() {
			var emailInvoiceForm = $('#emailInvoiceForm');
			emailInvoiceForm.ajaxForm({
						success: function(data) {
							$('.modal-body').html(data);
							ACC.order.bindEmailInvoiceSubmitButton();
						},
						error: function(xht, textStatus, ex)
						{
							alert("Failed to email invoice. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
						}
			});
		});
	}
};