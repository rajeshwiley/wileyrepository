$(document).ready(function() { 
	
	$("#orderMessageInput").blur(function () {
		var orderMessage = $(this).val();
        var messageData = {
					'orderMessage': orderMessage
				};
		 
		 $.ajax({
				url: ACC.config.encodedContextPath + '/checkout/multi/summary/saveOrderMessage',
				async: true,
				type: "POST",
				data: messageData,
				})
	});

});
