ACC.paybyinvoice = {
		
		bindPoNumberCopy: function ()
		{
			$(".poNumberInput").blur(function () {
				 var currentValue = $(this).val();
				 $('.poNumberInput').each(function() {
					    if(!$(this).val()) {
					    	$(this).val(currentValue);
					    }
					});
			});
		},
		
		bindPoNumberClear: function ()
		{
			$('.poNumberInput').each(function() {
				if($(this).parents(".has-error").length == 1) {
		    	$(this).val("");
				}
		});
		},	

}
$(document).ready(function() { 
	ACC.paybyinvoice.bindPoNumberCopy();
});
