ACC.updateprofile = {


 	_autoload: [
		"bindUpdatePassword"
 	],

	bindUpdatePassword: function () {
		$('#updatePassword').on('click', function (e) {

			if(!ACC.validatePassword.validatePassword(e)){
				e.preventDefault();
			}else{
				var emailAddress = $('#emailAddress').data('email');
				$('#emailField').val(emailAddress);
				$('#titleField').val('notitle');
			}
		});
 	}
 };