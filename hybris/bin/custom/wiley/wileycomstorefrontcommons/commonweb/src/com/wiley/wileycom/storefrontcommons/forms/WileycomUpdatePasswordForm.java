package com.wiley.wileycom.storefrontcommons.forms;


import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EqualAttributes;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualAttributes(message = "{validation.checkPwd.equals}", value =
		{ "pwd", "checkPwd" })
public class WileycomUpdatePasswordForm

{
	private String pwd;
	private String checkPwd;
	private String token;


	@NotNull(message = "{updatePwd.pwd.invalid}")
	@Size(min = 5, max = 32, message = "{updatePwd.pwd.invalid}")
	public String getPwd()
	{
		return pwd;
	}

	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	@NotNull(message = "{updatePwd.checkPwd.invalid}")
	public String getCheckPwd()
	{
		return checkPwd;
	}

	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}
}
