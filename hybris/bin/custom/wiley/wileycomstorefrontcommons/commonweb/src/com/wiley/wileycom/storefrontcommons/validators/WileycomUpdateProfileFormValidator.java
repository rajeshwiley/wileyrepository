package com.wiley.wileycom.storefrontcommons.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;


public class WileycomUpdateProfileFormValidator extends ProfileValidator
{
	public static final String FIRST_NAME_FIELD_KEY = "firstName";
	public static final String FIRST_NAME_ERROR_KEY = "profile.firstName.invalid";
	public static final String MIDDLE_NAME_FIELD_KEY = "middleName";
	public static final String MIDDLE_NAME_ERROR_KEY = "profile.middleName.invalid";
	public static final String LAST_NAME_FIELD_KEY = "lastName";
	public static final String LAST_NAME_ERROR_KEY = "profile.lastName.invalid";
	public static final String NAME_COMBINED_LENGTH_ERROR_KEY = "profile.name.invalid.length";
	public static final int MAX_COMBINED_VALUE = 255;

	@Autowired
	private FieldValidator firstNameSizeFieldValidator;

	@Autowired
	private FieldValidator sizeFieldValidator;

	@Autowired
	private FieldValidator optionalSizeFieldValidator;

	@Override
	public void validate(final Object object, final Errors errors)
	{
		firstNameSizeFieldValidator.validate(errors, FIRST_NAME_FIELD_KEY, FIRST_NAME_ERROR_KEY);
		optionalSizeFieldValidator.validate(errors, MIDDLE_NAME_FIELD_KEY, MIDDLE_NAME_ERROR_KEY);
		sizeFieldValidator.validate(errors, LAST_NAME_FIELD_KEY, LAST_NAME_ERROR_KEY);
		final UpdateProfileForm updateProfileForm = (UpdateProfileForm) object;
		// First and last names are concatenated with a single space
		if ((updateProfileForm.getFirstName().length() + updateProfileForm.getLastName().length()) > MAX_COMBINED_VALUE - 1)
		{
			errors.rejectValue(FIRST_NAME_FIELD_KEY, NAME_COMBINED_LENGTH_ERROR_KEY);
			errors.rejectValue(LAST_NAME_FIELD_KEY, NAME_COMBINED_LENGTH_ERROR_KEY);
		}
	}

}
