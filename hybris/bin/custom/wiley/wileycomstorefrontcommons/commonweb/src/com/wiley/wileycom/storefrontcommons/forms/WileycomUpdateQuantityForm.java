package com.wiley.wileycom.storefrontcommons.forms;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Wiley.com specific form for validating update field on cart page.
 */
public class WileycomUpdateQuantityForm
{
	@NotNull(message = "{basket.error.quantity.notNull}")
	@Min(value = 0, message = "{basket.error.quantity.invalid}")
	@Digits(fraction = 0, integer = 3, message = "{basket.error.quantity.range}")
	private Long quantity;

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public Long getQuantity()
	{
		return quantity;
	}
}
