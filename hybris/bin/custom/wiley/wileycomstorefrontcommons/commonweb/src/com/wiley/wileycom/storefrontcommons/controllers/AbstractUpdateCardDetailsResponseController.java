package com.wiley.wileycom.storefrontcommons.controllers;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.exceptions.ExternalSystemClientErrorException;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.storefrontcommons.util.HttpServletRequestUtil;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;


@RequestMapping("/my-account/hop")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public abstract class AbstractUpdateCardDetailsResponseController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractUpdateCardDetailsResponseController.class);

	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";

	@Autowired
	private UserFacade userFacade;
	@Autowired
	private SessionService sessionService;

	private HttpServletRequestUtil httpServletRequestUtil = new HttpServletRequestUtil();

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleResponse(final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		final Map<String, String> resultMap = httpServletRequestUtil.getRequestParameterMap(request);
		try
		{
			final PaymentSubscriptionResultData paymentSubscriptionResultData =
					getPaymentFacade().completeHopValidatePayment(resultMap, true);

			if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
					&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, "account.card.added");
			}
			else
			{
				final Object[] parameters = { paymentSubscriptionResultData.getResultCode() };
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.placing.order.paymentFailed.validation", parameters);
				LOG.error("Failed to update card. Please check the log files for more information");
			}
		}
		catch (ExternalSystemClientErrorException ex)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.error.address.external.system.unavailable");
		}

		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	public void setHttpServletRequestUtil(final HttpServletRequestUtil httpServletRequestUtil)
	{
		this.httpServletRequestUtil = httpServletRequestUtil;
	}

	protected abstract WileyPaymentFacade getPaymentFacade();
}
