/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileycom.storefrontcommons.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;

import com.wiley.wileycom.storefrontcommons.errorHandlers.WileyUrlAuthenticationFailureHandler;


public class LoginAuthenticationFailureHandler extends WileyUrlAuthenticationFailureHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(LoginAuthenticationFailureHandler.class);
	private BruteForceAttackCounter bruteForceAttackCounter;
	private boolean invalidateSessionOnLoginFail;
	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException
	{
		// Register brute attacks
		bruteForceAttackCounter.registerLoginFailure(request.getParameter("j_username"));
		if (invalidateSessionOnLoginFail)
		{
			/*
				Fix for https://jira.wiley.ru/browse/ECSC-6551. This bug notices two different not connected issues
				1)Redirect back to form Reset Password. I have only slightly changed Ilya's hotfix #1578
				2)HTTP 500 when already logged user tries to login with wrong credentials.
				This is much more interesting.
				When user tries to log in with wrong credentials the following happens OOTB:

					spring security context is cleared (so that SecurityContextHolder.getContext().getAuthentication()
					is NULL)
					"remember me" cookie is cleared.
					HTTP session is not cleared and Hybris session is also valid

				Cleared "remember me" prevents RememberMeAuthenticationFilter to restore old user (the one who was
				logged before unsuccessful login attempt) authentication in Spring Security context. So after that,
				filter chain will process request with anonymous authentication in spring security context and previous
				user still set as Hybris Session user.
				This difference triggers functionality in SecurityUserCheckBeforeControllerHandler to invalidate current
				 request HTTP session in the middle of filter processing chain. Due to invalidated session Hybris session
				  also becomes invalid. It makes further filter in chain - BTGSegmentFilter to fail with exception as
				  there is no more Base Site attached to session that is required for BTG functionality to work.
				It is worth to mention that clearing 'remember me' cookie is not working in any storefront that is not
				mapped to /storefrontname (e.g no /en or other path elements after). Because cookie is set
				to /storefrontname path but is cleared for /storefrontname/en (checked on WEL and OOTB storefronts)
				- this simply means cookie is not cleared at all. And that's why it is only reproducible to AGS
				storefront that does not use any /en in its path.

				The fix is simple - clear http session on unsuccessful login attempt (before redirecting
				to /login?error=true url). This makes Hybris session to be lost and hybris session user will become
				equal to spring security user (anonymous). So SecurityUserCheckBeforeControllerHandler will not
				invalidate session in the middle of request filter processing chain(as users will not be different)
				For checkout we should not clear session for unsuccessful login attempt as this will clear a cart.
				That's why invalidateSessionOnLoginFail was added to be set to 'true' for normal login handler and
				'false' for checkout login handler.

			 */
			LOG.info("Login attempt failed. Invalidating session");
			request.getSession().invalidate();
		} else {
			// Store the j_username in the session
			request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", request.getParameter("j_username"));
		}
		super.onAuthenticationFailure(request, response, exception);
	}


	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	@Required
	public void setInvalidateSessionOnLoginFail(final boolean invalidateSessionOnLoginFail)
	{
		this.invalidateSessionOnLoginFail = invalidateSessionOnLoginFail;
	}
}
