package com.wiley.wileycom.storefrontcommons.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController.REDIRECT_PREFIX;


/**
 * Created by Uladzimir_Barouski on 6/15/2017.
 */
public final class WileyRedirectUrlResolverUtil
{
	private WileyRedirectUrlResolverUtil()
	{
	}

	public static String getLocalizedRedirect(final HttpServletRequest request)
	{
		String redirection = null;
		if (!request.getRequestURL().toString().contains(request.getRequestURI()))
		{
			request.setAttribute("org.springframework.web.servlet.View.responseStatus", HttpStatus.MOVED_PERMANENTLY);
			final String queryString = request.getQueryString();
			if (StringUtils.isNotEmpty(queryString))
			{
				redirection = REDIRECT_PREFIX + request.getServletPath() + "?" + queryString;
			}
			else
			{
				redirection = REDIRECT_PREFIX + request.getServletPath();
			}
		}
		return redirection;
	}

	public static String getRedirectToPage(final HttpServletRequest request, final int page)
	{
		StringBuilder redirect = new StringBuilder();
		final String currentUri = request.getRequestURI().substring(request.getContextPath().length());
		redirect.append(currentUri);
		String currentQuery = WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPage(request);
		redirect.append(currentQuery).append(currentQuery.isEmpty() ? "?" : "&").append("page=").append(page);
		return REDIRECT_PREFIX + redirect.toString();
	}

	public static String getRedirectToPage(final HttpServletRequest request, final String query)
	{
		StringBuilder redirect = new StringBuilder();
		final String currentUri = request.getRequestURI().substring(request.getContextPath().length());
		redirect.append(currentUri).append(query);
		return REDIRECT_PREFIX + redirect.toString();
	}
}
