package com.wiley.wileycom.storefrontcommons.errorHandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.util.Assert;

import com.wiley.core.wileycom.users.service.exception.WileyUserBadRequestException;
import com.wiley.core.wileycom.users.service.exception.WileyUserUnreachableException;


/**
 * Created by Mikhail_Asadchy on 20.06.2016.
 */
public class WileyUrlAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private String defaultFailureUrl;

    @Override
    public void onAuthenticationFailure(final HttpServletRequest request,
            final HttpServletResponse response,
            final AuthenticationException exception)
            throws IOException, ServletException
    {
        String failureUrl = defaultFailureUrl;
        if (exception instanceof WileyUserUnreachableException)
        {
            failureUrl = decorateUrlString(failureUrl, "errorCode", "500");
        }
        else if (exception instanceof WileyUserBadRequestException)
        {
            failureUrl = decorateUrlString(failureUrl, "errorCode", "404");
        }

        //<editor-fold desc="partially copypasted code from (SimpleUrlAuthenticationFailureHandler.onAuthenticationFailure)">
        if (failureUrl == null)
        {
            logger.debug("No failure URL set, sending 401 Unauthorized error");
            response.sendError(401, "Authentication Failed: " + exception.getMessage());
        }
        else
        {
            saveException(request, exception);
            if (isUseForward())
            {
                logger.debug("Forwarding to " + failureUrl);
                request.getRequestDispatcher(failureUrl).forward(request, response);
            }
            else
            {
                logger.debug("Redirecting to " + failureUrl);
                getRedirectStrategy().sendRedirect(request, response, failureUrl);
            }
        }
        //</editor-fold>
    }

    private String decorateUrlString(final String failureUrl, final String paramName, final String value) {
        final String delimiter = failureUrl.contains("?") ? "&" : "?";

        return failureUrl + delimiter + paramName + "=" + value;
    }

    public String getDefaultFailureUrl() {
        return defaultFailureUrl;
    }

    @Override
    public void setDefaultFailureUrl(final String defaultFailureUrl) {
        Assert.isTrue(UrlUtils.isValidRedirectUrl(defaultFailureUrl),
                "\'" + defaultFailureUrl + "\' is not a valid redirect URL");
        this.defaultFailureUrl = defaultFailureUrl;
    }
}
