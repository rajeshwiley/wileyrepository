package com.wiley.wileycom.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;

public class WileycomPaymentInfoBillingAddressForm extends AddressForm

{
	private String paymentInfoId;

	public String getPaymentInfoId()
	{
		return paymentInfoId;
	}

	public void setPaymentInfoId(final String paymentInfoId)
	{
		this.paymentInfoId = paymentInfoId;
	}

}
