package com.wiley.wileycom.storefrontcommons.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.CONTENT_SEARCH_BASE_PATH;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.SEARCH_BASE_PATH;


/**
 * Created by Raman_Hancharou on 7/12/2017.
 */
public final class WileySearchUrlHelperUtil
{

	public static final String UTF_8 = "UTF-8";
	private static final String CONTENT_SEARCH = "content-search";
	private static final String QUERY_STRING_SPLIT_PATTERN = "\\|";

	private WileySearchUrlHelperUtil()
	{
	}

	public static String getContentSearchServletPath(final HttpServletRequest searchRequest) throws UnsupportedEncodingException
	{
		return getPath(new StringBuilder(CONTENT_SEARCH), searchRequest);
	}

	public static boolean isContentTabMayBeShown(final HttpServletRequest request)
	{
		final String pq = request.getParameter("pq");

		if (StringUtils.isEmpty(request.getParameter("cq")) && StringUtils.isNotEmpty(pq))
		{
			final String[] pqParams = pq.split("\\|");
			return pqParams.length > 0 && pqParams[0].length() > 0;
		}

		return false;
	}

	public static String getEnabledSearchTabPath(final HttpServletRequest request) throws UnsupportedEncodingException
	{
		final String currentUri = request.getRequestURI().substring(request.getContextPath().length());
		final String resultUri = SEARCH_BASE_PATH.equals(currentUri) ? CONTENT_SEARCH_BASE_PATH : SEARCH_BASE_PATH;
		final StringBuilder result = new StringBuilder();
		result.append(request.getContextPath()).append(resultUri);
		return getPath(result, request);
	}

	private static String getPath(final StringBuilder result, final HttpServletRequest request)
			throws UnsupportedEncodingException
	{
		boolean cqAdded = false;
		final String pq = request.getParameter("pq");

		if (StringUtils.isEmpty(request.getParameter("cq")) && StringUtils.isNotEmpty(pq))
		{
			String searchText = getSearchText(pq);

			if (searchText != null)
			{
				result.append("?cq=").append(URLEncoder.encode(searchText, UTF_8));
				cqAdded = true;
			}
		}

		if (StringUtils.isNotEmpty(pq))
		{
			result.append(cqAdded ? '&' : '?').append("pq=").append(URLEncoder.encode(pq, UTF_8));
		}

		return result.toString();
	}

	public static boolean requestHasQueryParam(final HttpServletRequest request, final String paramName)
	{
		return request.getQueryString() != null
				&& Arrays.stream(request.getQueryString().split("&")).anyMatch(e -> e.startsWith(paramName));
	}

	public static String getSearchText(final String queryString)
	{
		String result = null;

		if (queryString != null)
		{
			String[] params = queryString.split(QUERY_STRING_SPLIT_PATTERN);

			if (params.length > 0)
			{
				result = params[0];
			}
		}

		return result;
	}

	public static String getCurrentSearchQueryWithoutPage(final HttpServletRequest request)
	{
		StringBuilder currentSearchQuery = new StringBuilder();
		StringBuilder query = new StringBuilder();
		if (request.getQueryString() != null)
		{
			Stream<String> params = Arrays.stream(request.getQueryString().split("&")).filter(s -> !s.startsWith("page"));
			params.forEach(param -> query.append(query.length() == 0 ? "?" : "&").append(param));
		}
		return currentSearchQuery.append(query).toString();
	}

	public static String getCurrentSearchQueryWithoutPageSize(final HttpServletRequest request)
	{
		StringBuilder currentSearchQuery = new StringBuilder();
		StringBuilder query = new StringBuilder();

		if (request.getQueryString() != null)
		{
			Stream<String> params = Arrays.stream(request.getQueryString().split("&")).filter(s -> !s.startsWith("size"));
			params.forEach(param -> query.append(query.length() == 0 ? "?" : "&").append(param));
		}

		return currentSearchQuery.append(query).toString();
	}

	public static String getCurrentSearchQueryWithoutPageAndPageSize(final HttpServletRequest request)
	{
		StringBuilder currentSearchQuery = new StringBuilder();
		StringBuilder query = new StringBuilder();

		if (request.getQueryString() != null)
		{
			Stream<String> params = Arrays.stream(request.getQueryString()
					.split("&"))
					.filter(s -> !s.startsWith("page") && !s.startsWith("size"));

			params.forEach(param -> query.append(query.length() == 0 ? "?" : "&").append(param));
		}

		return currentSearchQuery.append(query).toString();
	}
}
