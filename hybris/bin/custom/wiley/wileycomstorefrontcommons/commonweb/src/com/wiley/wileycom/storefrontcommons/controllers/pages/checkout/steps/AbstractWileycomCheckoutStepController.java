/**
 *
 */
package com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutGroup;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;


public abstract class AbstractWileycomCheckoutStepController extends AbstractCheckoutStepController
{
	@Resource(name = "wileycomAddressValidator")
	private SmartValidator wileycomAddressValidator;
	
	@Resource(name = "wileycomCheckoutFacade")
	private WileycomCheckoutFacade checkoutFacade;

	@Resource
	WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("checkoutSteps")
	@Override
	public List<CheckoutSteps> addCheckoutStepsToModel()
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		final Map<String, CheckoutStep> progressBarMap = checkoutGroup.getCheckoutProgressBar();
		final List<CheckoutSteps> newCheckoutSteps = new ArrayList<>(progressBarMap.size());
		int index = 1;

		for (final Map.Entry<String, CheckoutStep> entry : progressBarMap.entrySet())
		{
			final CheckoutStep checkoutStep = entry.getValue();
			if (checkoutStep.isEnabled())
			{
				final String progressBarId = checkoutStep.getProgressBarId();
				final String url = StringUtils.remove(checkoutStep.currentStep(), REDIRECT_PREFIX);
				final Integer stepNumber = index++;

				final CheckoutSteps step = new CheckoutSteps(progressBarId, url, stepNumber);
				newCheckoutSteps.add(step);
			}
		}
		return newCheckoutSteps;
	}

	@ModelAttribute("countries")
	@Override
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getDeliveryCountries();
	}
	/**
	 * Gets addresses for delivery address book during checkout. We want to see only saved addresses in address book,
	 * because it's confusing to see address in book before checkout and don't see it after. This case is possible in
	 * case error of saving address in external system. In this case we don't save it, but want to give possibility
	 * for user to complete checkout with this address.
	 *
	 * @param selectedAddressData current delivery address of user
	 * @return list of addresses for delivery address book
	 */
	@Override
	protected List<? extends AddressData> getDeliveryAddresses(final AddressData selectedAddressData)
	{
		List<? extends AddressData> supportedDeliveryAddresses = getCheckoutFacade().getSupportedDeliveryAddresses(true);
		return supportedDeliveryAddresses == null ? Collections.emptyList() : supportedDeliveryAddresses;
	}

	protected SmartValidator getWileycomAddressValidator()
	{
		return wileycomAddressValidator;
	}


}
