package com.wiley.wileycom.storefrontcommons.errorHandlers;

import de.hybris.platform.acceleratorfacades.device.DeviceDetectionFacade;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.wiley.storefrontcommons.errorHandlers.WileyGlobalExceptionHandler;
import com.wiley.storefrontcommons.web.view.WileyUiExperienceViewResolver;


@ControllerAdvice
public class WileycomGlobalExceptionHandler extends WileyGlobalExceptionHandler
{
	@Resource(name = "uiExperienceService")
	private UiExperienceService uiExperienceService;

	@Resource(name = "viewResolver")
	private WileyUiExperienceViewResolver wileyUiExperienceViewResolver;

	@Resource(name = "deviceDetectionFacade")
	private DeviceDetectionFacade deviceDetectionFacade;

	public String getCommonResourcePath(final HttpServletRequest request)
	{
		deviceDetectionFacade.initializeRequest(request);

		final Object urlEncodingAttributes = request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
		final String contextPath = StringUtils.remove(request.getContextPath(),
				(urlEncodingAttributes != null) ? urlEncodingAttributes.toString() : "");
		final String uiExperienceCode = uiExperienceService.getUiExperienceLevel().getCode();
		final String uiExperienceCodeLower = wileyUiExperienceViewResolver
				.getUiExperienceViewPrefix().isEmpty() ? uiExperienceCode.toLowerCase() : StringUtils
				.remove(wileyUiExperienceViewResolver.getUiExperienceViewPrefix()
						.get(uiExperienceService.getUiExperienceLevel()), "/");
		final String siteRootUrl = contextPath + "/_ui/" + uiExperienceCodeLower;
		return siteRootUrl + "/" + COMMON;
	}
}
