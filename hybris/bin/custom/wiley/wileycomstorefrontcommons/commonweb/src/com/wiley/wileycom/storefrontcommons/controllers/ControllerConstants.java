package com.wiley.wileycom.storefrontcommons.controllers;


public interface ControllerConstants
{

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Pages
		{
			interface MultiStepCheckout
			{
				String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
				String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
				String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
				String PAYMENT_OPTIONS_PAGE = "pages/checkout/multi/paymentOptions";
				String CHOOSE_DELIVERY_METHOD_PAGE = "pages/checkout/multi/chooseDeliveryMethodPage";

				// for checkstyle
				void dummy();
			}
		}

		interface Fragments
		{
			interface Account
			{
				String COUNTRY_ADDRESS_FORM = "fragments/address/countryAddressForm";
			}
		}
	}


	String SESSION_WPG_VALIDATION_ACCOUNT_TRANSACTION_KEY = "wpg_account_validate";
	String SESSION_WPG_VALIDATION_CHECKOUT_TRANSACTION_KEY = "wpg_checkout_validate";
	
	String FORM_GLOBAL_ERROR_KEY = "form.global.error";
	String EXTERNAL_SERVICE_ERROR_KEY = "externalService.response.error";
	
}
