package com.wiley.wileycom.storefrontcommons.validators;

import org.springframework.validation.Errors;


/**
 * Interface is used for validation form's field. 
 */
public interface FieldValidator
{
	void validate(Errors errors, String fieldKey, String errorKey);
}
