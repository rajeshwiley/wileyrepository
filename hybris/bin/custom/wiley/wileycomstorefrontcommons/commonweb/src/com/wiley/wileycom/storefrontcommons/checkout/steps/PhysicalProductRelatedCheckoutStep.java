package com.wiley.wileycom.storefrontcommons.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;

import com.wiley.facades.order.WileycomCheckoutFacade;


/**
 * A checkout step implementation which is enabled only if Cart contains at least one physical entry.
 */
public class PhysicalProductRelatedCheckoutStep extends CheckoutStep
{
	private WileycomCheckoutFacade checkoutFacade;

	@Override
	public boolean isEnabled()
	{
		return !checkoutFacade.isDigitalSessionCart();
	}

	public void setCheckoutFacade(final WileycomCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}
}
