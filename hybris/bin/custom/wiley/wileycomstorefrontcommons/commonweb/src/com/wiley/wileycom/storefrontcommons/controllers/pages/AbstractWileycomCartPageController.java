package com.wiley.wileycom.storefrontcommons.controllers.pages;

import de.hybris.platform.commercefacades.user.data.CountryData;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.ModelAttribute;

import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.storefrontcommons.controllers.pages.AbstractWileyCartPageController;


/**
 * Created by Uladzimir_Barouski on 7/6/2016.
 */
public abstract class AbstractWileycomCartPageController extends AbstractWileyCartPageController
{
	protected static final String CONTINUE_URL = "continueUrl";

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

}
