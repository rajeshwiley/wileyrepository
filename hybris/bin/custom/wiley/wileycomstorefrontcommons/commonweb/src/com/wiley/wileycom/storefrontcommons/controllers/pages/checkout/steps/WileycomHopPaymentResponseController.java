/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.payment.WileyPaymentFacade;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "/checkout/multi/hop")
public abstract class WileycomHopPaymentResponseController extends WileycomPaymentMethodCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomHopPaymentResponseController.class);

	private static final String VALIDATION_FAILED_KEY = "checkout.placing.order.paymentFailed.validation";
	
	@Autowired
	private WileyPaymentFacade wileyPaymentFacade;

	@Autowired
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleHopResponse(final Model model, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request)
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);

		try
		{
			final PaymentSubscriptionResultData paymentSubscriptionResultData = wileyPaymentFacade.completeHopValidatePayment(
					resultMap, true);

			if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
					&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
			{
				final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

				if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
				{
					getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
				}
				getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
	
					return getCheckoutStep().nextStep();
			}
			else
			{
				LOG.error(createLogMessage(new StringBuilder("Failed to validate credit card."), resultMap,
					new String[]{"operation", "returnCode", "returnMessage", "transID",
								 "vendorID", "merchantResponse", "timestamp"}).toString());

				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
					VALIDATION_FAILED_KEY, new Object[] { paymentSubscriptionResultData.getResultCode() });
				return enterStep(model, redirectAttributes);
			}
		}
		catch (final Exception ex)
		{

			LOG.error(String.format("Unexpected error while validating  card, reason [%1$s]. ", ex.getMessage()));
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					VALIDATION_FAILED_KEY);
			return REDIRECT_URL_CART;
		}
	}
	
	protected StringBuilder createLogMessage(final StringBuilder prefix, final Map params, final String[] keys)
	{
		StringBuilder result = prefix;
		for (String key : keys)
		{
			result.append(" ").append(key).append("=").append(params.get(key)).append(",");
		}
		return result;
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String doHostedOrderPageError(@RequestParam(required = true) final String decision,
			@RequestParam(required = true) final String reasonCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		String redirectUrl = REDIRECT_URL_ADD_PAYMENT_METHOD;
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			redirectUrl = getCheckoutStep().onValidation(validationResults);
		}
		model.addAttribute("decision", decision);
		model.addAttribute("reasonCode", reasonCode);
		model.addAttribute("redirectUrl", redirectUrl.replace(REDIRECT_PREFIX, ""));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.hostedOrderPageError.breadcrumb"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		GlobalMessages.addErrorMessage(model, "checkout.multi.hostedOrderPageError.globalError");

		return getErrorPage();
	}

	protected abstract String getErrorPage();
	
}
