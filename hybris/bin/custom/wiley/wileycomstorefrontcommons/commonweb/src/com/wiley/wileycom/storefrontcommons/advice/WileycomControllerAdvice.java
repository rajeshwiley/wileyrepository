package com.wiley.wileycom.storefrontcommons.advice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;


public class WileycomControllerAdvice
{
	@Autowired
	private StringTrimmerEditor stringTrimmer;

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.registerCustomEditor(String.class, stringTrimmer);
	}
}
