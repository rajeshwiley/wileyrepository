package com.wiley.wileycom.storefrontcommons.populators;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.wileycom.storefrontcommons.forms.WileycomBillingAddressForm;


public class WileycomBillingAddressPopulator implements Populator<WileycomBillingAddressForm, AddressData>
{
	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Override
	public void populate(final WileycomBillingAddressForm form, final AddressData addressData) throws ConversionException
	{
		if (form != null)
		{
			addressData.setFirstName(form.getFirstName());
			addressData.setLastName(form.getLastName());
			addressData.setLine1(form.getStreet1());
			addressData.setLine2(form.getStreet2());
			addressData.setTown(form.getCity());
			addressData.setPostalCode(form.getPostalCode());
			addressData.setCountry(i18NFacade.getCountryForIsocode(form.getCountry()));
			if (form.getState() != null)
			{
				addressData.setRegion(i18NFacade.getRegion(form.getCountry(), form.getState()));
			}
			addressData.setPhone(form.getPhoneNumber());
			addressData.setBillingAddress(Boolean.TRUE);
			addressData.setEmail(customerFacade.getCurrentCustomer().getEmail());
			addressData.setVisibleInAddressBook(true);
		}
	}
}