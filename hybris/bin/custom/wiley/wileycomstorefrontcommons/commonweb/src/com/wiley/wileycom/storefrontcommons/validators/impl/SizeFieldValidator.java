package com.wiley.wileycom.storefrontcommons.validators.impl;

import org.springframework.util.Assert;
import org.springframework.validation.Errors;

import com.wiley.wileycom.storefrontcommons.validators.FieldValidator;


class SizeFieldValidator implements FieldValidator
{
	private int min;
	private int max;

	@Override
	public void validate(final Errors errors, final String fieldKey, final String errorKey)
	{
		Assert.notNull(errors, "Errors object must not be null");
		String fieldValue = (String) errors.getFieldValue(fieldKey);
		if (fieldValue == null || fieldValue.length() < min || fieldValue.length() > max)
		{
			errors.rejectValue(fieldKey, errorKey);
		}
	}

	public void setMin(final int min)
	{
		this.min = min;
	}

	public void setMax(final int max)
	{
		this.max = max;
	}
}
