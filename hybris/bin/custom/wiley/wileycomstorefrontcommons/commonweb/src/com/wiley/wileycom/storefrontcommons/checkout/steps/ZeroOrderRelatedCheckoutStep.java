/**
 *
 */
package com.wiley.wileycom.storefrontcommons.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;

import com.wiley.facades.order.WileycomCheckoutFacade;


public class ZeroOrderRelatedCheckoutStep extends CheckoutStep
{
	private WileycomCheckoutFacade checkoutFacade;

	@Override
	public boolean isEnabled()
	{
		return !checkoutFacade.isZeroTotalOrder();
	}

	public void setCheckoutFacade(final WileycomCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}
}
