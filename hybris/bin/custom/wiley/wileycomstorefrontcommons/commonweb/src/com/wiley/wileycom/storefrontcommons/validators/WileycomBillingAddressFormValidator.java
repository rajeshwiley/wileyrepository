package com.wiley.wileycom.storefrontcommons.validators;

import javax.annotation.Resource;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.wileycom.storefrontcommons.forms.WileycomBillingAddressForm;

import static com.wiley.core.constants.WileyCoreConstants.CA_COUNTRY_ISO_CODE;
import static com.wiley.core.constants.WileyCoreConstants.CN_COUNTRY_ISO_CODE;
import static com.wiley.core.constants.WileyCoreConstants.JP_COUNTRY_ISO_CODE;
import static com.wiley.core.constants.WileyCoreConstants.USA_COUNTRY_ISO_CODE;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.CITY;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.COUNTRY;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.FIRST_NAME;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.LAST_NAME;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.LINE1;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.LINE2;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.POSTCODE;
import static com.wiley.wileycom.storefrontcommons.validators.WileycomBillingAddressFormValidator.AddressField.STATE;


public class WileycomBillingAddressFormValidator implements Validator
{

	@Resource
	private FieldValidator notNullFieldValidator;

	@Resource
	private FieldValidator sizeFieldValidator;

	@Resource
	private FieldValidator firstNameSizeFieldValidator;

	private boolean stopFieldValidationAfterFailure = true;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileycomBillingAddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object target, final Errors errors)
	{

		validateCommonFields(errors);
		String countryIsoCode = ((WileycomBillingAddressForm) target).getCountry();
		if (USA_COUNTRY_ISO_CODE.equals(countryIsoCode) || CA_COUNTRY_ISO_CODE.equals(countryIsoCode) || CN_COUNTRY_ISO_CODE
				.equals(countryIsoCode))
		{
			validateField(errors, STATE, notNullFieldValidator);
		}
		else if (JP_COUNTRY_ISO_CODE.equals(countryIsoCode))
		{
			validateField(errors, STATE, notNullFieldValidator);
			validateField(errors, LINE2, notNullFieldValidator, sizeFieldValidator);
		}
	}

	private void validateCommonFields(final Errors errors)
	{
		validateField(errors, COUNTRY, notNullFieldValidator);
		validateField(errors, FIRST_NAME, notNullFieldValidator, firstNameSizeFieldValidator);
		validateField(errors, LAST_NAME, notNullFieldValidator, sizeFieldValidator);
		validateField(errors, LINE1, notNullFieldValidator, sizeFieldValidator);
		validateField(errors, CITY, notNullFieldValidator, sizeFieldValidator);
		validateField(errors, POSTCODE, notNullFieldValidator, sizeFieldValidator);
	}

	private void validateField(final Errors errors, final AddressField field,
			final FieldValidator... fieldValidators)
	{
		int fieldErrorCount = errors.getFieldErrorCount(field.getFieldKey());
		for (FieldValidator fieldValidator : fieldValidators)
		{
			fieldValidator.validate(errors, field.getFieldKey(), field.getErrorKey());
			if (stopFieldValidationAfterFailure && errors.getFieldErrorCount(field.getFieldKey()) > fieldErrorCount)
			{
				return;
			}
		}
	}

	enum AddressField
	{
		COUNTRY("country", "address.country.invalid"),
		FIRST_NAME("firstName", "address.firstName.invalid"),
		LAST_NAME("lastName", "address.lastName.invalid"),
		LINE1("street1", "address.line1.invalid"),
		LINE2("street2", "address.line2.invalid"),
		CITY("city", "address.townCity.invalid"),
		STATE("state", "address.state.invalid"),
		POSTCODE("postalCode", "address.postcode.invalid");

		private final String fieldKey;
		private final String errorKey;

		AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
