package com.wiley.wileycom.storefrontcommons.filters;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import com.wiley.facades.locale.WileyLocaleFacade;
import com.wiley.storefrontcommons.locale.RedirectionException;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;


public class WileycomLocaleFilter extends OncePerRequestFilter
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomLocaleFilter.class);

	private final WileyLocaleFacade wileyLocaleFacade;
	private List<WileyLocaleHandler> wileyLocaleHandlers;

	@Autowired
	public WileycomLocaleFilter(final WileyLocaleFacade wileyLocaleFacade)
	{
		this.wileyLocaleFacade = wileyLocaleFacade;
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse,
			final FilterChain filterChain) throws ServletException, IOException
	{
		String encodedLocale = wileyLocaleFacade.getDefaultEncodedLocale();
		for (final WileyLocaleHandler wileyLocaleHandler : wileyLocaleHandlers)
		{
			Optional<String> encodedLocaleOptional;
			try
			{
				encodedLocaleOptional = wileyLocaleHandler.getEncodedLocale();
			}
			catch (RedirectionException e) {
				LOG.info("Redirect from [{}] to external site occurs, target url: [{}]",
						httpServletRequest.getRequestURI(), e.getTarget());
				httpServletResponse.sendRedirect(e.getTarget());
				break;
			}

			if (encodedLocaleOptional.isPresent())
			{
				encodedLocale = encodedLocaleOptional.get();
				LOG.debug("Encoded locale [{}] is provided by handler {}", encodedLocale,
						wileyLocaleHandler.getClass().getName());
				break;
			}
		}
		wileyLocaleFacade.setCurrentEncodedLocale(encodedLocale);
		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	@Required
	public void setWileyLocaleHandlers(final List<WileyLocaleHandler> wileyLocaleHandlers)
	{
		this.wileyLocaleHandlers = wileyLocaleHandlers;
	}
}
