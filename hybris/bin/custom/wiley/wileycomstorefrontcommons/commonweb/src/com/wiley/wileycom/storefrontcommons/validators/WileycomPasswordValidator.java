package com.wiley.wileycom.storefrontcommons.validators;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class WileycomPasswordValidator implements Validator
{
	private static final String PASSWORD_REGEX = "^[a-zA-Z0-9]{5,32}$";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePasswordForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdatePasswordForm passwordForm = (UpdatePasswordForm) object;
		final String currentPassword = passwordForm.getCurrentPassword();
		final String newPassword = passwordForm.getNewPassword();

		if (StringUtils.isEmpty(currentPassword))
		{
			errors.rejectValue("currentPassword", "profile.currentPassword.invalid");
		}

		if (!newPassword.matches(PASSWORD_REGEX))
		{
			errors.rejectValue("newPassword", "updatePwd.pwd.invalid");
		}
	}
}
