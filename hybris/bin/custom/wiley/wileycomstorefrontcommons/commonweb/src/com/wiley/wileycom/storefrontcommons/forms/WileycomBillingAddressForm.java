package com.wiley.wileycom.storefrontcommons.forms;

public class WileycomBillingAddressForm
{
	private String city;
	private String country;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String postalCode;
	private String state;
	private String street1;
	private String street2;
	private boolean savePaymentInfo;
	private boolean isBillingSameAsShipping;

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public String getStreet1()
	{
		return street1;
	}

	public void setStreet1(final String street1)
	{
		this.street1 = street1;
	}

	public String getStreet2()
	{
		return street2;
	}

	public void setStreet2(final String street2)
	{
		this.street2 = street2;
	}

	public boolean isSavePaymentInfo()
	{
		return savePaymentInfo;
	}

	public void setSavePaymentInfo(final boolean savePaymentInfo)
	{
		this.savePaymentInfo = savePaymentInfo;
	}

	public boolean isBillingSameAsShipping()
	{
		return isBillingSameAsShipping;
	}

	public void setBillingSameAsShipping(final boolean billingSameAsShipping)
	{
		isBillingSameAsShipping = billingSameAsShipping;
	}

}