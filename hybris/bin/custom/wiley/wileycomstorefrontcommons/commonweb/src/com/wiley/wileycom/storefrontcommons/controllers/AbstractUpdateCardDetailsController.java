package com.wiley.wileycom.storefrontcommons.controllers;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.customer.WileycomCustomerFacade;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.core.payment.WileyHttpRequestParams;
import com.wiley.wileycom.storefrontcommons.forms.WileycomPaymentInfoBillingAddressForm;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.Collection;

import static de.hybris.platform.acceleratorservices.payment.constants.PaymentConstants.PaymentProperties.HOP_DEBUG_MODE;

@RequestMapping(value = "/my-account/payment-details")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public abstract class AbstractUpdateCardDetailsController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractUpdateCardDetailsController.class);

	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE =
			REDIRECT_PREFIX + "/my-account/payment-details";
	private static final String RESPONSE_URL = "my-account/update-card-details/hop/response";
	private static final String MERCHANT_CALLBACK_URL = "/integration/merchant_callback";
	private static final String PAYMENT_ADD_NEW_CARD_PAGE = "add-new-card";


	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";

	@Resource
	private SessionService sessionService;

	@Resource
	private SmartValidator wileycomAddressValidator;

	@Resource
	private WileycomCustomerFacade wileycomCustomerFacade;

	@Resource
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource
	private CustomerFacade customerFacade;

	@Resource
	private UserFacade userFacade;

	@Resource
	private CheckoutFacade checkoutFacade;

	@Resource
	private I18NFacade i18NFacade;

	@RequestMapping(value = "/add-new-card", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addNewCard(final Model model) throws CMSItemNotFoundException
	{
		final WileycomPaymentInfoBillingAddressForm addressForm = new WileycomPaymentInfoBillingAddressForm();

		model.addAttribute("addressForm", addressForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_ADD_NEW_CARD_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_ADD_NEW_CARD_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	@RequestMapping(value = "/add-new-card", method = RequestMethod.POST)
	@RequireHardLogIn
	public String validateNewCard(@ModelAttribute(value = "addressForm") final WileycomPaymentInfoBillingAddressForm addressForm,
								  final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
	{
		try
		{
			wileycomAddressValidator.validate(addressForm, bindingResult);
			if (bindingResult.hasErrors())
			{
				return handleAddressFormError(addressForm, model);
			}


			final PaymentData paymentData = getPaymentFacade().beginHopValidatePayment(RESPONSE_URL,
					MERCHANT_CALLBACK_URL, createAddressData(addressForm));
			setRefererOperation(paymentData);
			sessionService.setAttribute(ControllerConstants.SESSION_WPG_VALIDATION_ACCOUNT_TRANSACTION_KEY,
					paymentData.getParameters().get(WileyHttpRequestParams.WPG_TRANSACTION_ID));

			final boolean hopDebugMode = getSiteConfigService().getBoolean(HOP_DEBUG_MODE, false);
			model.addAttribute("hostedOrderPageData", paymentData);
			model.addAttribute("hopDebugMode", hopDebugMode);

			storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));


			return ControllerConstants.Views.Pages.MultiStepCheckout.HOSTED_ORDER_POST_PAGE;
		} catch (final Exception e)
		{
			LOG.error("Failed to build updateCardDetails request", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.card.update.error.general", null);

		}
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	private AddressData createAddressData(final WileycomPaymentInfoBillingAddressForm addressForm)
	{
		final AddressData newAddress = new AddressData();
		newAddress.setId(addressForm.getAddressId());
		newAddress.setTitleCode(addressForm.getTitleCode());
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(false);
		newAddress.setVisibleInAddressBook(false);
		newAddress.setCountry(i18NFacade.getCountryForIsocode(addressForm.getCountryIso()));
		newAddress.setPhone(addressForm.getPhone());
		newAddress.setDefaultAddress(addressForm.getDefaultAddress());
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			newAddress.setRegion(i18NFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
		}
		return newAddress;
	}

	private String handleAddressFormError(final AddressForm addressForm, final Model model) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_ADD_NEW_CARD_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));

		if (addressForm.getCountryIso() != null)
		{
			model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}
		return getViewForPage(model);
	}

	@ModelAttribute("supportedCountries")
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getDeliveryCountries();
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	@ModelAttribute("breadcrumbs")
	public Collection<Breadcrumb> getBreadcrumbs()
	{
		return accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails");
	}

	@ModelAttribute("hasDefaultPayment")
	public boolean hasDefaultPayment()
	{
		return userFacade.getCCPaymentInfos(true).stream().anyMatch(CCPaymentInfoData::isDefaultPaymentInfo);
	}

	protected abstract WileyPaymentFacade getPaymentFacade();

	private void setRefererOperation(final PaymentData paymentData)
	{
		paymentData.getParameters().put("WPG_CUSTOM_referer_operation", "update-account-details");
	}
}
