package com.wiley.wileycom.storefrontcommons.controllers.pages;

import com.google.common.collect.Sets;
import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.core.strategies.robotsmetatag.RobotsMetatagResolverService;
import com.wiley.storefrontcommons.util.WileyMetaSanitizerUtil;
import com.wiley.storefrontcommons.util.WileyRobotsTagContentMapper;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.servlet.View;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class AbstractWileycomCategoryPageController extends AbstractCategoryPageController
{
	public static final String REQUEST_ATTR = "request";
	public static final String CATEGORY_CODE_ATTR = "categoryCode";
	public static final Set<String> NON_SEARCH_TEMPLATES = Sets.newHashSet("CategoryLandingPageTemplate");
	private static final String DEFAULT_SORT_OPTION = "relevance";
	private static final String PRODUCT_LIST_COMPONENT_ID = "ProductListComponent";

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "robotsMetatagResolverService")
	private RobotsMetatagResolverService robotsMetatagResolverService;

	@Value("#{'${wileyb2cstorefront.search.itemsPerPage}'.split(',')}")
	private List<Integer> itemsPerPage;

	/**
	 * Copied OOTB code from AbstractCategoryPageController, changed CategorySearchEvaluator to
	 * Wileyb2cCategorySearchEvaluator.
	 */
	protected String performSearchAndGetResultsPage(final String categoryCode,
			final String searchQuery, final int page, final int pageSize, final ShowMode showMode, final String sortCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response, final String pageType)
			throws UnsupportedEncodingException
	{
		return performSearchAndGetResultsPage(categoryCode, searchQuery, page, pageSize, showMode, sortCode, model, request,
				response, pageType, true);
	}

	protected String performSearchAndGetResultsPage(final String categoryCode,
			final String searchQuery, final int page, final int pageSize, final ShowMode showMode, final String sortCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response, final String pageType,
			final boolean useFacetViewMoreLimit)
			throws UnsupportedEncodingException
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);

		final String redirection = checkRequestUrl(request, response, getCategoryModelUrlResolver().resolve(category));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		final CategoryPageModel categoryPage = getCategoryPage(category);
		final PageTemplateModel pageTemplate = categoryPage.getMasterTemplate();

		final Wileyb2cCategorySearchEvaluator categorySearch = new Wileyb2cCategorySearchEvaluator(categoryCode,
				XSSFilterUtil.filter(searchQuery), page, pageSize, showMode, sortCode, categoryPage, pageType);
		categorySearch.setUseFacetViewMoreLimit(useFacetViewMoreLimit);

		ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = null;
		try
		{
			if (NON_SEARCH_TEMPLATES.contains(pageTemplate.getUid()))
			{
				// do not run search for landing pages as far as product results are not rendered here
				searchPageData = createEmptySearchResult(categoryCode);
			}
			else
			{
				categorySearch.doSearch();
				searchPageData = categorySearch.getSearchPageData();

				int numberOfPages = searchPageData.getPagination().getNumberOfPages();
				if (numberOfPages > 0 && page > searchPageData.getPagination().getNumberOfPages() - 1) {
					return WileyRedirectUrlResolverUtil.getRedirectToPage(request, numberOfPages - 1);
				}
			}
		}
		catch (final ConversionException e) // NOSONAR
		{
			searchPageData = createEmptySearchResult(categoryCode);
		}

		final boolean showCategoriesOnly = categorySearch.isShowCategoriesOnly();

		storeCmsPageInModel(model, categorySearch.getCategoryPage());
		storeContinueUrl(request);

		populateModel(model, searchPageData, showMode);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getSearchBreadcrumbBuilder().getBreadcrumbs(categoryCode, searchPageData));
		model.addAttribute("showCategoriesOnly", Boolean.valueOf(showCategoriesOnly));
		model.addAttribute("categoryName", category.getName());
		model.addAttribute("pageType", PageType.CATEGORY.name());
		model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());

		updatePageTitle(category, model);

		final RequestContextData requestContextData = getRequestContextData(request);
		requestContextData.setCategory(category);
		requestContextData.setSearch(searchPageData);

		if (robotsMetatagResolverService.needResolveByPageUid(categoryPage))
		{
			String robotsMetaTag = robotsMetatagResolverService.resolveByPageUid(categoryPage.getUid());
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, WileyRobotsTagContentMapper
					.getContentForTag(RobotsMetaTag.valueOf(robotsMetaTag)));
		}

		final String metaKeywords = WileyMetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewPage(categorySearch.getCategoryPage());
	}

	@Override
	protected String checkRequestUrl(final HttpServletRequest request, final HttpServletResponse response,
			final String resolvedUrlPath) throws UnsupportedEncodingException
	{
		final String redirect = super.checkRequestUrl(request, response, resolvedUrlPath);
		if (StringUtils.isEmpty(redirect))
		{
			return WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);
		}
		return redirect;
	}

	@Override
	protected void setUpMetaData(final Model model, final String metaKeywords, final String metaDescription)
	{
		final ServletRequest request = (ServletRequest) model.asMap().get(REQUEST_ATTR);
		final Map<String, String> pathVariables = (Map) request.getAttribute(View.PATH_VARIABLES);
		final String categoryCode = pathVariables.get(CATEGORY_CODE_ATTR);
		final CategoryModel categoryModel = getCommerceCategoryService().getCategoryForCode(categoryCode);
		final String seoDescription = MetaSanitizerUtil.sanitizeDescription(categoryModel.getSeoDescriptionTag());
		super.setUpMetaData(model, metaKeywords, seoDescription);
	}

	protected String parseSortCode(final String searchQuery)
	{
		if (searchQuery != null)
		{
			String[] qParams = searchQuery.split("\\|");
			return qParams.length > 1 ? qParams[1] : DEFAULT_SORT_OPTION;
		}
		return DEFAULT_SORT_OPTION;
	}

	protected void addProductListComponentToModel(final Model model) throws CMSItemNotFoundException
	{
		final AbstractCMSComponentModel component = cmsComponentService.getAbstractCMSComponent(PRODUCT_LIST_COMPONENT_ID);
		if (component != null)
		{
			model.addAttribute("component", component);
			model.addAttribute("actions", component.getActions());
		}
	}

	/**
	 * Copied OOTB code from AbstractWileycomCategoryPageController
	 * - populateSearchPageData changed to populateSearchPageDataWithFacetLimit;
	 * to avoid bulk facet values to be rendered,
	 * - added parameter facetCode.
	 */
	protected FacetRefinement<SearchStateData> performSearchAndGetFacetsWithFacetLimit(final String categoryCode,
			final String searchQuery, final int page, final int pageSize,
			final ShowMode showMode, final String sortCode, final String pageType, final String facetCode)
	{
		final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData =
				populateSearchPageDataWithFacetLimit(categoryCode, searchQuery, page, pageSize, showMode, sortCode,
						pageType, facetCode);
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());

		return refinement;
	}


	/**
	 * Copied OOTB code from AbstractCategoryPageController.populateSearchPageData but use Wileyb2cCategorySearchEvaluator
	 * with facetCode.
	 */
	protected ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> populateSearchPageDataWithFacetLimit(
			final String categoryCode, final String searchQuery, final int page, final int pageSize,
			final ShowMode showMode, final String sortCode, final String pageType, final String facetCode)
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
		final CategoryPageModel categoryPage = getCategoryPage(category);
		final Wileyb2cCategorySearchEvaluator categorySearch = new Wileyb2cCategorySearchEvaluator(categoryCode,
				XSSFilterUtil.filter(searchQuery), page, pageSize, showMode, sortCode, categoryPage, pageType);
		categorySearch.setFacetCode(facetCode);
		categorySearch.doSearch();

		return categorySearch.getSearchPageData();
	}

	/**
	 * Copied OOTB code from CategorySearchEvaluator,
	 * - removed if condition with line: categoryPage = getDefaultCategoryPage();
	 * to avoid redirecting to Product Listing Page instead of reloading Category Listing Page,
	 * - added parameter pageType.
	 */
	protected class Wileyb2cCategorySearchEvaluator extends CategorySearchEvaluator
	{

		private final String categoryCode;
		private final SearchQueryData searchQueryData = new SearchQueryData();
		private final int page;
		private final int pageSize;
		private final ShowMode showMode;
		private final String sortCode;
		private CategoryPageModel categoryPage;
		private boolean showCategoriesOnly;
		private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;
		private String pageType;
		private String facetCode;
		private Boolean useFacetViewMoreLimit;

		public Wileyb2cCategorySearchEvaluator(final String categoryCode, final String searchQuery,
				final int page, final int pageSize, final ShowMode showMode, final String sortCode,
				final CategoryPageModel categoryPage, final String pageType)
		{
			super(categoryCode, searchQuery, page, showMode, sortCode, categoryPage);

			this.categoryCode = categoryCode;
			this.searchQueryData.setValue(searchQuery);
			this.page = page;
			this.pageSize = pageSize;
			this.showMode = showMode;
			this.sortCode = sortCode;
			this.categoryPage = categoryPage;
			this.pageType = pageType;
		}

		@Override
		public void doSearch()
		{
			showCategoriesOnly = false;
			final SearchStateData searchState = new SearchStateData();
			searchState.setPageType(pageType);
			searchState.setFacetCode(facetCode);
			searchState.setUseFacetViewMoreLimit(useFacetViewMoreLimit);


			if (searchQueryData.getValue() == null)
			{
				// Direct category link without filtering
				final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);
				searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
				if (categoryPage != null)
				{
					showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
							&& CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
				}
			}
			else
			{
				// We have some search filtering
				searchState.setQuery(searchQueryData);

				final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);
				searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
			}
		}

		@Override
		public ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData()
		{
			return searchPageData;
		}

		public Boolean getUseFacetViewMoreLimit()
		{
			return useFacetViewMoreLimit;
		}

		public void setUseFacetViewMoreLimit(final Boolean useFacetViewMoreLimit)
		{
			this.useFacetViewMoreLimit = useFacetViewMoreLimit;
		}

		public String getFacetCode()
		{
			return facetCode;
		}

		public void setFacetCode(final String facetCode)
		{
			this.facetCode = facetCode;
		}
	}
}
