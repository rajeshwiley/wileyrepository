package com.wiley.wileycom.storefrontcommons.controllers;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import com.wiley.core.constants.WileyCoreConstants;

import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_KEY;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_VALIDATE;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.TRANSACTION_KEY;


@Controller
@RequestMapping(value = "/payment/hop")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class WPGPaymentResponseController extends AbstractPageController
{
	private static final String VALIDATE_CHECKOUT_PAYMENT_RESPONSE_HANDLER = "/checkout/multi/hop/response";
	private static final String VALIDATE_ACCOUNT_PAYMENT_RESPONSE_HANDLER = "/my-account/hop/response";

	@Autowired
	private SessionService sessionService;

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleHopResponse(final HttpServletRequest request)
	{
		final String operation = request.getParameter(OPERATION_KEY);
		final String transaction = request.getParameter(TRANSACTION_KEY);

		String operationHandler;
		if (OPERATION_VALIDATE.equals(operation))
		{
			if (isValidateFromCheckout(transaction))
			{
				operationHandler = FORWARD_PREFIX + VALIDATE_CHECKOUT_PAYMENT_RESPONSE_HANDLER;
			} else if (isValidateFromAccount(transaction)) {
				operationHandler = FORWARD_PREFIX + VALIDATE_ACCOUNT_PAYMENT_RESPONSE_HANDLER;
			} else {
				throw new IllegalStateException("There is no such transaction saved for validation in current session");
			}
		} else {
			throw new UnsupportedOperationException();
		}
		return operationHandler;
	}

	private boolean isValidateFromAccount(final String transactionId) {
		return transactionId.equals(
				sessionService.getAttribute(ControllerConstants.SESSION_WPG_VALIDATION_ACCOUNT_TRANSACTION_KEY));
	}
	private boolean isValidateFromCheckout(final String transactionId) {
		return transactionId.equals(
				sessionService.getAttribute(ControllerConstants.SESSION_WPG_VALIDATION_CHECKOUT_TRANSACTION_KEY));
	}

}
