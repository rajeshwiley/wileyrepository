package com.wiley.wileycom.storefrontcommons.validators.impl;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import com.wiley.wileycom.storefrontcommons.validators.FieldValidator;


public class NotNullFieldValidator implements FieldValidator
{
	@Override
	public void validate(final Errors errors, final String fieldKey, final String errorKey)
	{
		ValidationUtils.rejectIfEmpty(errors, fieldKey, errorKey);
	}
}
