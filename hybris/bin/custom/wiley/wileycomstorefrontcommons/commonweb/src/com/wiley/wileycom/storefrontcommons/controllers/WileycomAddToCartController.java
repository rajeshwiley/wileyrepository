package com.wiley.wileycom.storefrontcommons.controllers;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;
import com.wiley.core.wileycom.product.exception.ProductHasAlreadyBeenPurchasedException;


public class WileycomAddToCartController extends AbstractController
{

	protected static final String CART_PAGE_URL = "/cart";
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
	private static final String REDIRECT_TO_CART = REDIRECT_PREFIX + CART_PAGE_URL;

	private static final Logger LOG = Logger.getLogger(WileycomAddToCartController.class);

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	protected String addProductToCart(final String code, final AddToCartForm form,
			final BindingResult bindingErrors,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		if (bindingErrors.hasErrors())
		{
			return getViewWithBindingErrorMessages(redirectAttributes, bindingErrors);
		}

		final long qty = form.getQty();

		return addToCart(code, redirectAttributes, qty, request);
	}

	protected String addToCart(final String code, final RedirectAttributes redirectAttributes,
			final long qty, final HttpServletRequest request)
	{
		String redirectResult = REDIRECT_TO_CART;
		if (qty <= 0)
		{
			redirectAttributes.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
			redirectAttributes.addAttribute("quantity", 0L);
		}
		else
		{

			CartModificationData cartModification = null;

			try
			{
				cartModification = cartFacade.addToCart(code, qty);
			}
			catch (final CommerceCartModificationException ex)
			{
				redirectResult = handleCommerceCartModificationException(ex, redirectAttributes, code, request);
			}
			catch (final UnknownIdentifierException ex)
			{
				handleUnknownIdentifierException(ex, redirectAttributes, code);
			}

			if (cartModification != null)
			{
				addCartModificationMessages(redirectAttributes, qty, cartModification);
			}
		}
		return redirectResult;
	}

	protected void addCartModificationMessages(final RedirectAttributes redirectAttributes, final long qty,
			final CartModificationData cartModification)
	{
		//we use check cartModification.getQuantityAdded() != qty  instead of cartModification.getQuantityAdded() == 0
		//due to possibility to add multiple non countable products to cart using external cart calculation
		if (cartModification.getQuantityAdded() != qty && CommerceCartModificationStatus.SUCCESS.equals(
				cartModification.getStatusCode()))
		{
			// non-countable product already exists in cart
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.information.quantity.reducedNumberOfItemsAdded.nonCountableProduct");
		}
		else if (cartModification.getQuantityAdded() == 0L)
		{
			// product was not added not cart for some reasons
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
		}
		else if (cartModification.getQuantityAdded() < qty)
		{
			// product was added to cart with reduced quantity
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
		}
	}

	private String getViewWithBindingErrorMessages(final RedirectAttributes redirectAttributes,
			final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			if (isTypeMismatchError(error))
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						QUANTITY_INVALID_BINDING_MESSAGE_KEY);
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						error.getDefaultMessage());
			}
		}
		return REDIRECT_TO_CART;
	}

	private boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}

	protected String handleCommerceCartModificationException(final CommerceCartModificationException ex,
			final RedirectAttributes redirectAttributes, final String productCode, final HttpServletRequest request)
	{

		if (ex.getCause() instanceof ProductNotVisibleException)
		{
			LOG.warn(String.format("Could not add product [%s] to cart due to [%s].", productCode, ex.getMessage()));
			ProductNotVisibleException productNotVisibleException = (ProductNotVisibleException) ex.getCause();
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					productNotVisibleException.getErrorMessageCode(), productNotVisibleException.getErrorMessageParameters());
		}
		else if (ex.getCause() instanceof ProductHasAlreadyBeenPurchasedException)
		{
			LOG.warn(String.format("Could not add product [%s] to cart due to [%s].", productCode, ex.getMessage()));
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.error.product.hasAlreadyBeenPurchased");
			String referer = request.getHeader("Referer");
			return REDIRECT_PREFIX + referer;
		}
		else
		{
			// some backend exception occurred during cart modification.
			LOG.error("Error occurred during adding product to cart. Error message: " + ex.getMessage());
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"basket.error.occurred");
		}
		return REDIRECT_TO_CART;
	}

	protected void handleUnknownIdentifierException(final UnknownIdentifierException ex,
			final RedirectAttributes redirectAttributes, final String productCode)
	{
		// Assume that product not found due to marketing restrictions. e.g. onlineTo onlineFrom
		LOG.warn(String.format("Could not add product [%s] to cart due to [%s].", productCode, ex.getMessage()));
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
				WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE);
	}
}
