package com.wiley.wileycom.storefrontcommons.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@RequestMapping("/my-account")
public abstract class AbstractWileycomOrderHistoryPageController extends AbstractSearchPageController
{
	private static final String ORDER_HISTORY_CMS_PAGE = "orders";
	private static final ShowMode SHOW_MODE = ShowMode.Page;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Value("${account.order.history.itemsPerPage}")
	private int itemsPerPage;
	@Value("${account.order.history.sortType}")
	private String sortType;
	@Value("${account.order.history.orderDateFormat}")
	private String orderDateFormat;

	@Value("${account.order.history.productDateFormat}")
	private String productDateFormat;

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
						 final Model model)
			throws CMSItemNotFoundException
	{
		final PageableData pageableData = createPageableData(page, itemsPerPage, sortType, SHOW_MODE);

		final SearchPageData<OrderHistoryData> searchPageData = getOrderFacade().getPagedOrderHistoryForStatuses(pageableData);
		populateModel(model, searchPageData, SHOW_MODE);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@ModelAttribute("orderDateFormat")
	public String getOrderDateFormat()
	{
		return orderDateFormat;
	}

	@ModelAttribute("productDateFormat")
	public String getProductDateFormat()
	{
		return productDateFormat;
	}

	protected abstract OrderFacade getOrderFacade();

}
