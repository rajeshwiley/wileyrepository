package com.wiley.wileycom.storefrontcommons.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class WileycomVoucherForm
{
	private String discountCode;

	@NotNull(message = "{basket.discountCode.empty}")
	@Size(min = 3, message = "{basket.discountCode.empty}")
	public String getDiscountCode()
	{
		return discountCode;
	}

	public void setDiscountCode(final String discountCode)
	{
		this.discountCode = discountCode;
	}
}
