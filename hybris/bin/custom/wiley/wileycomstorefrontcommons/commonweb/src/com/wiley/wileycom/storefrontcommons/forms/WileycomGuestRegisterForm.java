package com.wiley.wileycom.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;


/**
 * WileycomGuestRegisterForm is used on Guest Registration step of checkout,
 * see mockup: web/webroot/_ui/responsive/theme-blue/pages/guest-checkout-order-confirmation.html
 */
public class WileycomGuestRegisterForm extends GuestRegisterForm
{
	/**
	 * Defines is user aggree to receive promotions updates
	 */
	private boolean acceptPromotions;

	public boolean getAcceptPromotions()
	{
		return acceptPromotions;
	}

	public void setAcceptPromotions(final boolean acceptPromotions)
	{
		this.acceptPromotions = acceptPromotions;
	}
}
