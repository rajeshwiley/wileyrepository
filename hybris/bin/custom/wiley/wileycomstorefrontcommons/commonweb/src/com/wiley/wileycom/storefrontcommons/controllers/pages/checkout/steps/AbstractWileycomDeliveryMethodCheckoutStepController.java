package com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-method")
public abstract class AbstractWileycomDeliveryMethodCheckoutStepController extends AbstractWileycomCheckoutStepController
{

	protected static final String DELIVERY_METHOD = "delivery-method";

	@Resource
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		prepareDataForPage(model);

		return ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_METHOD_PAGE;
	}

	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		final List<? extends DeliveryModeData> supportedDeliveryModes = wileycomCheckoutFacade.getSupportedDeliveryModes();

		// Try to set default delivery mode
		wileycomCheckoutFacade.setDeliveryModeIfAvailable(supportedDeliveryModes);

		final CartData cartData = wileycomCheckoutFacade.getCheckoutCart();
		model.addAttribute("cartData", cartData);
		model.addAttribute("deliveryMethods", supportedDeliveryModes);
		super.prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryMethod.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	/**
	 * This method gets called when the "Use Selected Delivery Method" button is clicked. It sets the selected delivery
	 * mode on the checkout facade and reloads the page highlighting the selected delivery Mode.
	 *
	 * @param selectedDeliveryMethod
	 * 		- the id of the delivery mode.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryMode(
			@RequestParam(value = "selectedDeliveryMethod", required = false) final String selectedDeliveryMethod)
	{
		if (selectedDeliveryMethod != null)
		{
			updateDeliveryMode(selectedDeliveryMethod);
		}
		return getCheckoutStep().nextStep();
	}

	protected void updateDeliveryMode(final String selectedDeliveryMethod)
	{
		if (StringUtils.isNotEmpty(selectedDeliveryMethod))
		{
			wileycomCheckoutFacade.setDeliveryMode(selectedDeliveryMethod);
		}
	}


	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_METHOD);
	}
}
