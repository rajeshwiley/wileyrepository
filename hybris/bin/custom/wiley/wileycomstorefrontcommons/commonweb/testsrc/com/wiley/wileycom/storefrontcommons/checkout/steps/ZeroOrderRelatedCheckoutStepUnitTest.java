/**
 *
 */
package com.wiley.wileycom.storefrontcommons.checkout.steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.order.WileycomCheckoutFacade;


/**
 * Unit test for {@code ZeroOrderRelatedCheckoutStepTest}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ZeroOrderRelatedCheckoutStepUnitTest
{
	@InjectMocks
	private ZeroOrderRelatedCheckoutStep zeroOrderRelatedCheckoutStep;

	@Mock
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Test
	public void shouldDisableStepWhenOrderTotalIsZero()
	{
		when(wileycomCheckoutFacade.isZeroTotalOrder()).thenReturn(true);
		assertFalse(zeroOrderRelatedCheckoutStep.isEnabled());
	}

	@Test
	public void shouldEnableStepWhenOrderTotalIsNotZero()
	{
		when(wileycomCheckoutFacade.isDigitalSessionCart()).thenReturn(false);
		assertTrue(zeroOrderRelatedCheckoutStep.isEnabled());
	}
}
