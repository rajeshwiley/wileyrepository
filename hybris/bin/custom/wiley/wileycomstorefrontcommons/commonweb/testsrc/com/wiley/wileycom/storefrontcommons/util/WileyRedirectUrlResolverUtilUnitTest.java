package com.wiley.wileycom.storefrontcommons.util;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController.REDIRECT_PREFIX;


/**
 * Created by Uladzimir_Barouski on 6/16/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRedirectUrlResolverUtilUnitTest
{
	public static final String SERVLET_PATH = "/anypage";
	public static final String PARAMS = "param1=111&param2=222";
	public static final String REDIRECT_WITHOUT_PARAMS = REDIRECT_PREFIX + SERVLET_PATH;
	public static final String REDIRECT_WITH_PARAMS = REDIRECT_PREFIX + SERVLET_PATH + "?" + PARAMS;
	public static final String URL_WITHOUT_LOCALE = "https://store.wiley.com" + SERVLET_PATH;
	public static final String URL_WITH_LOCALE = "https://store.wiley.com/en-us" + SERVLET_PATH;
	public static final String LOCALE = "/en-us";
	@Mock
	private HttpServletRequest requestMock;

	@Test
	public void testRedirectWhenUrlHasCorrectLocale() throws Exception
	{
		//Given
		when(requestMock.getRequestURL()).thenReturn(new StringBuffer(URL_WITH_LOCALE));
		when(requestMock.getRequestURI()).thenReturn(LOCALE);
		//When
		String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(requestMock);
		//Then
		assertNull(redirection);
	}

	@Test
	public void testRedirectWhenUrlHasIncorrectLocaleWithoutParams() throws Exception
	{
		//Given
		when(requestMock.getRequestURL()).thenReturn(new StringBuffer(URL_WITHOUT_LOCALE));
		when(requestMock.getRequestURI()).thenReturn(LOCALE);
		when(requestMock.getQueryString()).thenReturn(null);
		when(requestMock.getServletPath()).thenReturn(SERVLET_PATH);
		//When
		String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(requestMock);
		//Then
		assertNotNull(redirection);
		assertEquals(REDIRECT_WITHOUT_PARAMS, redirection);
	}

	@Test
	public void testRedirectWhenUrlHasIncorrectLocaleWithParams() throws Exception
	{
		//Given
		when(requestMock.getRequestURL()).thenReturn(new StringBuffer(URL_WITHOUT_LOCALE));
		when(requestMock.getRequestURI()).thenReturn(LOCALE);
		when(requestMock.getQueryString()).thenReturn(PARAMS);
		when(requestMock.getServletPath()).thenReturn(SERVLET_PATH);
		//When
		String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(requestMock);
		//Then
		assertNotNull(redirection);
		assertEquals(REDIRECT_WITH_PARAMS, redirection);
	}
}