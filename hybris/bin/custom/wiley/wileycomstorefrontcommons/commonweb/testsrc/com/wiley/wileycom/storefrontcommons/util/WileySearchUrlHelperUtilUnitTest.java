package com.wiley.wileycom.storefrontcommons.util;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants;


/**
 * Created by Raman_Hancharou on 7/14/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySearchUrlHelperUtilUnitTest
{

	public static final String CONTEXT_PATH = "/contextpath";
	public static final String PRODUCT_SEARCH_REQUEST_URI = CONTEXT_PATH + WileycomstorefrontcommonsConstants.SEARCH_BASE_PATH;
	public static final String CONTENT_SEARCH_REQUEST_URI =
			CONTEXT_PATH + WileycomstorefrontcommonsConstants.CONTENT_SEARCH_BASE_PATH;
	public static final String PQ_PARAM = "pq";
	public static final String PQ_VALUE = "searchText|sort|facet";
	public static final String PQ_VALUE_WITH_SPEC_CHARS = "\"&?%searchText|sort|facet";
	public static final String CQ_PARAM = "cq";
	public static final String CQ_VALUE = "searchText";
	public static final String CQ_VALUE_WITH_SPEC_CHARS = "\"&?%searchText";
	public static final String UTF_8 = "UTF-8";
	public static final char EQUAL_CHAR = '=';
	public static final char QUESTION_CHAR = '?';
	public static final char AMP_CHAR = '&';

	@Before
	public void setUp()
	{
		when(requestMock.getContextPath()).thenReturn(CONTEXT_PATH);
		when(requestMock.getParameter(PQ_PARAM)).thenReturn(PQ_VALUE);
		when(requestMock.getParameter(CQ_PARAM)).thenReturn(CQ_VALUE);
	}

	@Mock
	private HttpServletRequest requestMock;

	@Test
	public void testProductsSearchPath() throws Exception
	{
		//Given
		when(requestMock.getRequestURI()).thenReturn(CONTENT_SEARCH_REQUEST_URI);

		//When
		String path = WileySearchUrlHelperUtil.getEnabledSearchTabPath(requestMock);
		//Then
		assertEquals(buildExpectedProductsSearchPath(), path);
	}

	@Test
	public void testContentSearchPath() throws Exception
	{
		//Given
		when(requestMock.getRequestURI()).thenReturn(PRODUCT_SEARCH_REQUEST_URI);
		when(requestMock.getParameter(CQ_PARAM)).thenReturn(null);

		//When
		String path = WileySearchUrlHelperUtil.getEnabledSearchTabPath(requestMock);
		//Then
		assertEquals(buildExpectedContentSearchPath(), path);
	}

	@Test
	public void testContentSearchPathWithSpecialChars() throws Exception
	{
		//Given
		when(requestMock.getRequestURI()).thenReturn(PRODUCT_SEARCH_REQUEST_URI);
		when(requestMock.getParameter(CQ_PARAM)).thenReturn(null);
		when(requestMock.getParameter(PQ_PARAM)).thenReturn(PQ_VALUE_WITH_SPEC_CHARS);
		//When
		String path = WileySearchUrlHelperUtil.getEnabledSearchTabPath(requestMock);
		//Then
		assertEquals(buildExpectedContentSearchPathWithSpecChars(), path);
	}

	private String buildExpectedProductsSearchPath() throws UnsupportedEncodingException
	{
		final StringBuilder sb = new StringBuilder();
		sb.append(PRODUCT_SEARCH_REQUEST_URI).append(QUESTION_CHAR).append(PQ_PARAM).append(EQUAL_CHAR).append(
				URLEncoder.encode(PQ_VALUE, UTF_8));
		return sb.toString();
	}

	private String buildExpectedContentSearchPath() throws UnsupportedEncodingException
	{
		final StringBuilder sb = new StringBuilder();
		sb.append(CONTENT_SEARCH_REQUEST_URI).append(QUESTION_CHAR).append(CQ_PARAM).append(EQUAL_CHAR).append(
				URLEncoder.encode(CQ_VALUE, UTF_8)).append(AMP_CHAR).append(PQ_PARAM).append(EQUAL_CHAR).append(
				URLEncoder.encode(PQ_VALUE, UTF_8));
		return sb.toString();
	}

	private String buildExpectedContentSearchPathWithSpecChars() throws UnsupportedEncodingException
	{
		final StringBuilder sb = new StringBuilder();
		sb.append(CONTENT_SEARCH_REQUEST_URI).append(QUESTION_CHAR).append(CQ_PARAM).append(EQUAL_CHAR).append(
				URLEncoder.encode(CQ_VALUE_WITH_SPEC_CHARS, UTF_8)).append(AMP_CHAR).append(PQ_PARAM).append(EQUAL_CHAR).append(
				URLEncoder.encode(PQ_VALUE_WITH_SPEC_CHARS, UTF_8));
		return sb.toString();
	}


}
