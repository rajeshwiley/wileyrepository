package com.wiley.wileycom.storefrontcommons.checkout.steps;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.order.WileycomCheckoutFacade;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@code PhysicalProductRelatedCheckoutStep}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PhysicalProductRelatedCheckoutStepUnitTest
{
	@InjectMocks
	private PhysicalProductRelatedCheckoutStep physicalProductRelatedCheckoutStep;

	@Mock
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Test
	public void shouldStepDisabledWhenOnlyDigitalProductsAreInSessionCart()
	{
		when(wileycomCheckoutFacade.isDigitalSessionCart()).thenReturn(true);
		assertFalse(physicalProductRelatedCheckoutStep.isEnabled());
	}

	@Test
	public void shouldStepEnabledWhenSessionCartContainsAPhysicalProduct()
	{
		when(wileycomCheckoutFacade.isDigitalSessionCart()).thenReturn(false);
		assertTrue(physicalProductRelatedCheckoutStep.isEnabled());
	}
}
