package com.wiley.wileycom.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import static org.mockito.Mockito.when;


/**
 * Created by Georgii_Gavrysh on 7/14/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomAddressValidatorTest
{
	@InjectMocks
	private WileycomAddressValidator testedValidator;

	@Mock
	private I18NFacade i18NFacade;

	@Mock
	private AddressForm addressForm;

	private Errors errors;

	private String[] objects;

	@Before
	public void setUp()
	{
		errors = new BeanPropertyBindingResult(addressForm, "addressForm");
		objects = new String[] { "" };
		testedValidator.setI18NFacade(i18NFacade);
	}

	@Test
	public void testSuccess()
	{
		setUpSuccessFormForUS();

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 0);
	}

	@Test
	public void testLastNameMandatory()
	{
		setUpSuccessFormForUS();
		when(addressForm.getLastName()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "lastName");
	}

	@Test
	public void testFirstNameMandatory()
	{
		setUpSuccessFormForUS();
		when(addressForm.getFirstName()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "firstName");
	}

	@Test
	public void testCountryMandatory()
	{
		setUpSuccessFormForUS();
		when(addressForm.getCountryIso()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "countryIso");
	}

	@Test
	public void testLine1Mandatory()
	{
		setUpSuccessFormForUS();
		when(addressForm.getLine1()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "line1");
	}

	@Test
	public void testPhoneNotMandatory()
	{
		setUpSuccessFormForUS();
		when(addressForm.getPhone()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 0);
	}

	@Test
	public void testLine2NotMandatoryForUS()
	{
		setUpSuccessFormForUS();
		when(addressForm.getLine2()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 0);
	}

	@Test
	public void testRegionMandatoryForUS()
	{
		setUpSuccessFormForUS();
		when(addressForm.getRegionIso()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "regionIso");
	}

	@Test
	public void testNoRegionsForUS()
	{
		setUpSuccessFormForUS();
		final List<RegionData> regionDataList = Collections.EMPTY_LIST;
		when(i18NFacade.getRegionsForCountryIso("US")).thenReturn(regionDataList);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "regionIso");
	}

	@Test
	public void testRegionDoesNotExistForUS()
	{
		setUpSuccessFormForUS();
		final List<RegionData> regionDataList = new ArrayList<>();
		regionDataList.add(createRegionData("US", "US-AL"));
		when(i18NFacade.getRegionsForCountryIso("US")).thenReturn(regionDataList);
		when(i18NFacade.getRegion("US", "US-AL")).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "regionIso");
	}

	private RegionData createRegionData(final String countryIso, final String regionIso)
	{
		final RegionData regionData = new RegionData();
		regionData.setCountryIso("US");
		regionData.setIsocode("US-AL");
		return regionData;
	}

	@Test
	public void testLine2MandatoryForJapan()
	{
		setUpSuccessFormForJapan();
		when(addressForm.getLine2()).thenReturn(null);

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 1);
		Assert.assertEquals(errors.getFieldError().getField(), "line2");
	}

	@Test
	public void testSuccessRegionNotMandatoryForDefaultCountry()
	{
		setUpSuccessFormForDefault();

		testedValidator.validate(addressForm, errors, objects);

		Assert.assertEquals(errors.getErrorCount(), 0);
	}

	private void setUpSuccessFormForDefault()
	{
		when(addressForm.getCountryIso()).thenReturn("DEFAULT");
		when(addressForm.getFirstName()).thenReturn("Ivan");
		when(addressForm.getLastName()).thenReturn("Ivanov");
		when(addressForm.getLine1()).thenReturn("line1");
		when(addressForm.getLine2()).thenReturn("line2");
		when(addressForm.getTownCity()).thenReturn("Moscow");
		when(addressForm.getRegionIso()).thenReturn(null);
		when(addressForm.getPostcode()).thenReturn("11111");
		when(addressForm.getPhone()).thenReturn("+1(234)1234567");
	}

	private void setUpSuccessFormForJapan()
	{
		when(addressForm.getCountryIso()).thenReturn("JP");
		when(addressForm.getFirstName()).thenReturn("Yushi");
		when(addressForm.getLastName()).thenReturn("Mamomoto");
		when(addressForm.getLine1()).thenReturn("line1");
		when(addressForm.getLine2()).thenReturn("line2");
		when(addressForm.getTownCity()).thenReturn("Tokio");
		when(addressForm.getRegionIso()).thenReturn("JP-A1");
		when(addressForm.getPostcode()).thenReturn("11111");
		when(addressForm.getPhone()).thenReturn("+1(234)1234567");

		final List<RegionData> regionDataList = new ArrayList<>();
		final RegionData regionData1 = createRegionData("JP", "JP-A1");
		regionDataList.add(regionData1);
		when(i18NFacade.getRegionsForCountryIso("JP")).thenReturn(regionDataList);
		when(i18NFacade.getRegion("JP", "JP-A1")).thenReturn(regionData1);
	}

	private void setUpSuccessFormForUS()
	{
		when(addressForm.getCountryIso()).thenReturn("US");
		when(addressForm.getFirstName()).thenReturn("John");
		when(addressForm.getLastName()).thenReturn("Johnson");
		when(addressForm.getLine1()).thenReturn("line1");
		when(addressForm.getLine2()).thenReturn("line2");
		when(addressForm.getTownCity()).thenReturn("city");
		when(addressForm.getRegionIso()).thenReturn("US-AL");
		when(addressForm.getPostcode()).thenReturn("11111");
		when(addressForm.getPhone()).thenReturn("+1(234)1234567");

		final List<RegionData> regionDataList = new ArrayList<>();
		final RegionData regionData1 = createRegionData("US", "US-AL");
		regionDataList.add(regionData1);
		when(i18NFacade.getRegionsForCountryIso("US")).thenReturn(regionDataList);
		when(i18NFacade.getRegion("US", "US-AL")).thenReturn(regionData1);
	}
}
