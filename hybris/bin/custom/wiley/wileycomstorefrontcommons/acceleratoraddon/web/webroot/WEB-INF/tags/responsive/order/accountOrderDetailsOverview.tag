<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/shared/format"%>
<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>


<div class="my-account-wrap order-details">
    <div class="order-details-top">
        <div class="row">
            <div class="col-xs-12 col-sm-5">
                <div class="order-details">
                    <div class="order-number">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6"><spring:theme code="text.account.order.orderDetails.orderNumber"/></div>
                            <div class="col-xs-6 col-sm-6">${orderData.code}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6"><spring:theme code="text.account.order.orderPlaced"/></div>
                        <div class="col-xs-6 col-sm-6"><format-common:formattedDate date="${orderData.created}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6"><spring:theme code="text.account.order.orderStatus"/></div>
                        <div class="col-xs-6 col-sm-6">${orderData.statusDisplay}</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-sm-offset-2">
                <div class="order-details-total">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6"><spring:theme code="order.totals.subtotal"/></div>
                        <div class="col-xs-6 col-sm-6"><format:price priceData="${order.subTotal}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6"><spring:theme code="order.totals.savings"/></div>
                        <div class="col-xs-6 col-sm-6"><format:price priceData="${order.totalDiscounts}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6"><spring:theme code="order.totals.delivery"/></div>
                        <div class="col-xs-6 col-sm-6"><format:price priceData="${order.deliveryCost}"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">${sessionCountry.taxName}:</div>
                        <div class="col-xs-6 col-sm-6"><format:price priceData="${order.totalTax}"/></div>
                    </div>
                    <div class="order-details-total-sum">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6"><spring:theme code="order.totals.total"/> </div>
                            <div class="col-xs-6 col-sm-6"><format:price priceData="${order.totalPriceWithTax}"/></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="order-details-add-info">
        <div class="row">
            <div class="col-xs-12 col-sm-11 col-sm-offset-1">
                <div class="row">
                    <c:if test="${order.deliveryAddress != null}">
                        <div class="col-xs-12 col-sm-3">
                            <div class="add-info-title"><spring:theme code="text.account.order.deliveryAddress"/></div>
                            <div class="add-info-description">
                                <order-common:accountOrderDetailsAddress address="${order.deliveryAddress}"/>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${order.deliveryMode != null}">
                        <div class="col-xs-12 col-sm-3">
                            <div class="add-info-title">
                                <p><spring:theme code="text.account.order.deliveryMethod"/></p>
                            </div>
                            <div class="add-info-description">
                                <p>${fn:escapeXml(order.deliveryMode.name)}</p>
                                <p>${fn:escapeXml(order.deliveryMode.description)}</p>
                            </div>
                        </div>
                    </c:if>

                    <c:if test="${order.wileyPaymentInfo.paymentType ne 'ZERO_ORDER'}">
                        <div class="col-xs-12 col-sm-3">
                            <div class="add-info-title"><spring:theme code="text.account.order.paymentDetails"/></div>
                            <div class="add-info-description">
                                <c:choose>
                                    <c:when test="${order.wileyPaymentInfo.paymentType eq 'CARD'}">
                                        <p>${fn:escapeXml(order.paymentInfo.cardType)}</p>
                                        <p>**** **** **** ${fn:escapeXml(order.paymentInfo.cardNumber)}</p>
                                        <p><spring:theme code="text.account.payment.cardExpDate" arguments="${fn:escapeXml(order.paymentInfo.expiryMonth)},${fn:escapeXml(order.paymentInfo.expiryYear)}"/></p>
                                    </c:when>
                                    <c:when test="${(order.wileyPaymentInfo.paymentType eq 'INVOICE') or (order.wileyPaymentInfo.paymentType eq 'PAYPAL')}">
                                        <p><spring:theme code="text.account.order.type"/></p>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                        <c:if test="${order.wileyPaymentInfo != null}">
                            <div class="col-xs-12 col-sm-3">
                                <div class="add-info-title"><spring:theme code="text.account.order.billingAddress"/></div>
                                <div class="add-info-description">
                                    <order-common:accountOrderDetailsAddress address="${order.wileyPaymentInfo.billingAddress}"/>
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <div class="items-shipped-list">
        <order-common:accountOrderDetailsAllShippedItems order="${order}"/>
    </div>
</div>