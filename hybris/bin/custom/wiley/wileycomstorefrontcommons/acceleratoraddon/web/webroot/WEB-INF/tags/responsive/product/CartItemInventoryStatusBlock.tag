<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="showReleaseDate" required="true" type="java.lang.Boolean" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="product-list-status">
	<c:set var="inventoryStatusList" value="${entry.product.inventoryStatus}"/>
	<c:if test="${fn:length(inventoryStatusList) > 0 && !entry.product.isDigital}">
		<div class="status-title">
				<spring:theme code="basket.page.availability"/>:
		</div>
		<div class="status-description">
			<c:forEach items="${inventoryStatusList}" var="inventoryStatus">
				<div>
				    <spring:theme code="product.inventoryStatus.${inventoryStatus.statusCode}" var="inventoryStatusCode"/>
				    <c:choose>
                      <c:when test="${showReleaseDate eq true and not empty inventoryStatus.availableDate}">
                        <fmt:formatDate pattern="MM/dd/yyyy" value="${inventoryStatus.availableDate}" var="inventoryStatusAvailableDate"/>
                        <spring:theme code="product.inventoryStatus.statusDescription.withDate" arguments="${inventoryStatus.quantity},${inventoryStatusCode},${inventoryStatusAvailableDate}"/>
                      </c:when>
                      <c:otherwise>
                        <spring:theme code="product.inventoryStatus.statusDescription.withoutDate" arguments="${inventoryStatus.quantity},${inventoryStatusCode}"/>
                      </c:otherwise>
                    </c:choose>
				</div>
			</c:forEach>
		</div>
	</c:if>
</div>