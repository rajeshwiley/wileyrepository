<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="consignmentEntry" required="true" type="de.hybris.platform.commercefacades.order.data.ConsignmentEntryData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:formatDate value="${consignmentEntry.consignment.statusDate}" var="formattedDate"
                                            pattern="MM/dd/yyyy" />

<div class="row">
    <div class="col-xs-12 col-sm-6"><spring:theme code='consignmentEntry.line.part1' arguments="${consignmentEntry.shippedQuantity},${formattedDate}"/></div>
    <div class="col-xs-12 col-sm-6"><spring:theme code='consignmentEntry.line.part2' arguments="${consignmentEntry.consignment.trackingID}"/></div>
</div>