<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="address" required="true"
	type="de.hybris.platform.commercefacades.user.data.AddressData"%>

<p>${fn:escapeXml(address.firstName)},&nbsp;${fn:escapeXml(address.lastName)}
	<c:if test="${address.defaultAddress}">
			(<spring:theme code="text.default" text="Default" />)
		</c:if>
</p>
<p>${fn:escapeXml(address.line1)}</p>
<c:if test="${not empty fn:escapeXml(address.line2)}"><p>${fn:escapeXml(address.line2)}</p></c:if>
<p>
	${fn:escapeXml(address.town)}<c:if test="${not empty fn:escapeXml(address.region)}">,&nbsp;${fn:escapeXml(address.region.name)}</c:if>
</p>
<p>${fn:escapeXml(address.country.name)}&nbsp;${fn:escapeXml(address.postalCode)}</p>
<p>${fn:escapeXml(address.phone)}</p>