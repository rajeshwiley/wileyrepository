<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/address" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<form:form commandName="billingAddressForm">
	<address:billingAddressFormElements regions="${regions}" country="${country}"/>
	<input type="hidden" id="savePaymentInfo" name="savePaymentInfo"/>
	<input type="hidden" id="billingSameAsShipping" name="billingSameAsShipping"/>
</form:form>
