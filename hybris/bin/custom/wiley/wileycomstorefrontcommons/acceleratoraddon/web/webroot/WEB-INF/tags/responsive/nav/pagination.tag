<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="isPaginationModeUrlVisible" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:if test="${searchPageData.pagination.numberOfPages > 1}">
    <div class="panel-pagination">
        <div class="pagination-element">
            <c:if test="${searchPageData.pagination.currentPage > 0}">
            <spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
                        <spring:param name="page" value="${searchPageData.pagination.currentPage - 1}"/>
            </spring:url>
            </c:if>
            <a href="${not empty previousPageUrl ? previousPageUrl : '#'}" title="" class="arrow-left">&nbsp;</a>
        </div>
        <div class="pagination-element active"><a href="#" title="" class="pagination-item">${searchPageData.pagination.currentPage + 1}</a></div>
        <div class="pagination-element"><a href="#" title="" class="text">of</a></div>
        <spring:url value="${searchUrl}" var="lastPageUrl" htmlEscape="true">
            <spring:param name="page" value="${searchPageData.pagination.numberOfPages - 1}"/>
        </spring:url>
        <div class="pagination-element"><a href="${lastPageUrl}" title="" class="pagination-item">${searchPageData.pagination.numberOfPages}</a></div>
        <div class="pagination-element">
        <c:if test="${searchPageData.pagination.currentPage < (searchPageData.pagination.numberOfPages -1)}">
            <spring:url value="${searchUrl}" var="nextPageUrl" htmlEscape="true">
                        <spring:param name="page" value="${searchPageData.pagination.currentPage + 1}"/>
            </spring:url>
        </c:if>
        <a href="${not empty nextPageUrl ? nextPageUrl : '#'}" title="" class="arrow-right">&nbsp;</a>
        </div>
    </div>
</c:if>
<c:if test="${isPaginationModeUrlVisible}">
	<c:choose>
		<c:when test="${isShowAllAllowed}">
			<spring:url value="${searchUrl}" var="paginationUrl" htmlEscape="true">
				<spring:param name="show" value="All"/>
			</spring:url>
			<a href="${paginationUrl}" class="show-hide hidden-xs"><spring:theme code="text.pagination.url.showAll"/></a>
		</c:when>
		<c:when test="${isShowPageAllowed}">
			<spring:url value="${searchUrl}" var="paginationUrl" htmlEscape="true">
				<spring:param name="page" value="0"/>
			</spring:url>
			<a href="${paginationUrl}" class="show-hide hidden-xs"><spring:theme code="text.pagination.url.showPaginated"/></a>
		</c:when>
	</c:choose>
</c:if>