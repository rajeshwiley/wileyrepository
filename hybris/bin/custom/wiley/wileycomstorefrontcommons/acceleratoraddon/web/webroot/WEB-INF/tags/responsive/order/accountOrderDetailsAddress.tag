<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>

<p>${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}</p>
<p>${fn:escapeXml(address.line1)}</p>
<p>${fn:escapeXml(address.line2)}</p>
<p>${fn:escapeXml(address.town)},&nbsp;${fn:escapeXml(address.region.name)}</p>
<p>${fn:escapeXml(address.postalCode)}&nbsp;${fn:escapeXml(address.country.name)}</p>