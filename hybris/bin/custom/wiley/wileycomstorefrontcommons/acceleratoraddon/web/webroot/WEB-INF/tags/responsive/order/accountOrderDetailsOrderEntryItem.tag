<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

<c:url var="orderEntryCurrentProductUrl" value="${orderEntry.product.url}"/>

<div class="product-list-item">
    <div class="row">
        <div class="col-xs-12 col-sm-11 col-sm-offset-1">
            <div class="product-list-image"><product:productPrimaryImage product="${orderEntry.product}" format="thumbnail" /></div>
            <div class="product-list-info">
                <div class="product-list-title">
                <c:choose>
                    <c:when test="${not empty orderEntry.product.purchaseOptionType}">
                        ${orderEntry.product.name}
                    </c:when>
                    <c:otherwise>
                        <a href="${orderEntryCurrentProductUrl}">${orderEntry.product.name}</a>
                    </c:otherwise>
                </c:choose>
                </div>
                <div class="product-list-description">
                    <div class="row purchase-info">
                        <div class="col-xs-12 col-sm-6">
                            <p>${orderEntry.product.authors}</p>
                             <c:if test="${not empty orderEntry.product.purchaseOptionType}">
                                    <p>${orderEntry.product.purchaseOptionType}</p>
                             </c:if>
                            <p><spring:theme code="text.account.order.entry.productId" arguments="${fn:escapeXml(orderEntry.product.code)}"/></p>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <p><spring:theme code="text.account.order.entry.unitPrice"/>&nbsp;<format:price priceData="${orderEntry.basePrice}"/></p>
                            <p><spring:theme code="text.account.order.entry.quantity" arguments="${fn:escapeXml(orderEntry.quantity)}"/></p>
                            <p><spring:theme code="text.account.order.entry.total"/>&nbsp;<format:price priceData="${orderEntry.totalPrice}"/></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>