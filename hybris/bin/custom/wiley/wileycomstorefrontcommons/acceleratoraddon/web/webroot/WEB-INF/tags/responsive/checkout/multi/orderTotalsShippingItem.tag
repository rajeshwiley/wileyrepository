<%@ attribute name="abstractOrderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${abstractOrderData.showDeliveryCost}">
    <div id="deliveryCostItem" class="order-total-item">
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <spring:theme code="basket.page.totals.delivery" />
            </div>
            <div class="col-xs-6 col-sm-6 text-right price">
                <format:price priceData="${abstractOrderData.deliveryCost}" displayFreeForZero="TRUE" />
            </div>
        </div>
    </div>
</c:if>
