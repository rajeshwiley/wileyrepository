<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>

<c:if test="${not empty orderEntry.consignmentStatus and not orderEntry.product.isDigital}">
<div class="items-shipped">
	<c:if test="${orderEntry.consignmentStatus eq 'SHIPPED'}">
	    <span class="icon-check">&nbsp;</span>
    	<label for="itemsShipped"><spring:theme code='orderEntry.consignmentMessage.shipped' arguments="${orderEntry.quantity}"/></label>
    </c:if>

    <c:if test="${orderEntry.consignmentStatus eq 'PARTIALLY_SHIPPED'}">
        <span class="icon-error">&nbsp;</span>
    	<label for="itemsShipped"><spring:theme code='orderEntry.consignmentMessage.partially_shipped' arguments="${orderEntry.totalShipped},${orderEntry.quantity}"/></label>
    </c:if>

    <c:if test="${orderEntry.consignmentStatus eq 'WAITING'}">
        <span class="icon-error">&nbsp;</span>
    	<label for="itemsShipped"><spring:theme code='orderEntry.consignmentMessage.waiting' arguments="${orderEntry.totalShipped},${orderEntry.quantity}"/></label>
    </c:if>

   <c:if test="${orderEntry.consignmentStatus eq 'CANCELLED'}">
        <span class="icon-error">&nbsp;</span>
   	    <label for="itemsShipped"><spring:theme code='orderEntry.consignmentMessage.cancelled'/></label>
   </c:if>
   </div>
</c:if>
