<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="date" required="true" type="java.util.Date" %>
<%@ attribute name="attributes" required="false" type="java.lang.String" %>

<fmt:formatDate value="${date}" pattern="MM/dd/yyyy hh:mm a"/>