<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMethod" required="true" type="de.hybris.platform.commercefacades.order.data.DeliveryModeData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<input type=radio name="selectedDeliveryMethod" value="${deliveryMethod.externalCode}" id="${deliveryMethod.externalCode}" ${isSelected ? 'checked="checked"' : ''} />
<label for="${deliveryMethod.externalCode}">
    <div class="delivery-item-title">${deliveryMethod.name}</div>
    <div class="delivery-item-description">
        <c:choose>
            <c:when test="${not empty deliveryMethod.description}">
                <spring:theme code="checkout.shipping.methods.delivery.option.description.format.withDescription"
                              arguments="${deliveryMethod.deliveryCost.formattedValue}, ${deliveryMethod.description}"
                              text="Delivery Options and Arrival Dates">
                </spring:theme>
            </c:when>
            <c:otherwise>
                <spring:theme code="checkout.shipping.methods.delivery.option.description.format.withoutDescription"
                              arguments="${deliveryMethod.deliveryCost.formattedValue}"
                              text="Delivery Options and Arrival Dates">
                </spring:theme>
            </c:otherwise>
        </c:choose>
    </div>
</label>