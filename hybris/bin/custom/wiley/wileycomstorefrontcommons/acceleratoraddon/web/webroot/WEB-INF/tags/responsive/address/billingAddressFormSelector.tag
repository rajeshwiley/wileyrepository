<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="supportedCountries" required="false" type="java.util.List" %>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>

<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/address" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>


<c:url value="/checkout/multi/payment-method/save-billing-address-form" var="saveBillingAddressAction" />
<div class="site-form">
	<form:form method="post" commandName="billingAddressForm" action="${saveBillingAddressAction}">
		<div class="row">
			<div class="col-xs-12 col-sm-8">

				<div id="billingCountrySelector" data-display-title="false" class="clearfix">
					<formElement:formSelectBox idKey="address.country"
												labelKey="address.country"
												path="country"
												mandatory="true"
												skipBlank="false"
												skipBlankMessageKey="address.selectCountry"
												items="${supportedCountries}"
												itemValue="isocode"
												tabindex="${tabIndex}"
												selectCSSClass="form-control" />
				</div>
				
				<div id="i18nBillingAddressForm" class="billingAddressForm">
					<c:if test="${not empty country}">				
						<address:billingAddressFormElements regions="${regions}" country="${country}" tabindex="${tabIndex + 1}"/>
					</c:if>
					<input type="hidden" id="savePaymentInfo" name="savePaymentInfo"/>
                    <input type="hidden" id="billingSameAsShipping" name="billingSameAsShipping"/>
				</div>
			</div>	
		</div>
	</form:form>
</div>