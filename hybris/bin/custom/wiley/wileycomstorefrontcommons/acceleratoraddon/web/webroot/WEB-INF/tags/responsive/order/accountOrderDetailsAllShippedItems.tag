<%--TODO: This one is temporary--%>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/shared/format"%>
<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>


<c:forEach items="${order.entries}" var="orderEntry">
    <c:choose>
        <c:when test="${orderEntry.product.isDigital}">
            <c:if test="${orderEntry.consignmentStatus ne 'CANCELLED'}">
                <c:set var="orderContainsDigitalProducts" value="true"/>
            </c:if>
            <c:if test="${orderEntry.consignmentStatus eq 'CANCELLED'}">
                <c:set var="orderContainsDigitalCanceledProducts" value="true"/>
            </c:if>
        </c:when>
        <c:otherwise>
            <c:set var="orderContainsPhysicalProducts" value="true"/>
        </c:otherwise>
    </c:choose>
</c:forEach>

<c:if test="${orderContainsPhysicalProducts}">
    <div class="items-shipped-item">
        <div class="product-list order-details-product-list">
            <c:forEach items="${order.entries}" var="orderEntry">
                <order-common:consignmentStatusMessageForOrderEntry orderEntry="${orderEntry}" />
                <c:if test="${not orderEntry.product.isDigital}">
                    <order-common:accountOrderDetailsOrderEntryItem orderEntry="${orderEntry}" />
                    <order-common:consignmentsForOrderEntry orderEntry="${orderEntry}" />
                </c:if>
            </c:forEach>
        </div>
    </div>
</c:if>

<c:if test="${orderContainsDigitalProducts}">
    <div class="items-shipped-item">
        <div class="items-shipped"><span class="icon-digital">&nbsp;</span>
            <label for="itemsShipped">
                <spring:theme code="text.account.order.section.yourDigitalProduct" />
            </label>
        </div>
        <c:forEach items="${order.entries}" var="orderEntry">
            <c:if test="${orderEntry.consignmentStatus ne 'CANCELLED' && orderEntry.product.isDigital}">
                <div class="product-list order-details-product-list">
                    <order-common:accountOrderDetailsOrderEntryItem orderEntry="${orderEntry}" />
                </div>
            </c:if>
        </c:forEach>
    </div>
</c:if>

<c:if test="${orderContainsDigitalCanceledProducts}">
    <div class="items-shipped-item">
        <div class="items-shipped"><span class="icon-digital">&nbsp;</span>
            <label for="itemsShipped">
                <spring:theme code="text.account.order.section.cancelled" />
            </label>
        </div>
        <c:forEach items="${order.entries}" var="orderEntry">
            <div class="product-list order-details-product-list">
                <c:if test="${orderEntry.consignmentStatus eq 'CANCELLED' && orderEntry.product.isDigital}">
                    <order-common:accountOrderDetailsOrderEntryItem orderEntry="${orderEntry}" />
                </c:if>
            </div>
        </c:forEach>
    </div>
</c:if>
