<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/address"%>

<%@ attribute name="address" required="true"
	type="de.hybris.platform.commercefacades.user.data.AddressData"%>
<%@ attribute name="title" required="true" type="java.lang.String"%>

<div id="deleteAddressPopup-${address.id}"
	role="dialog" class="modal fade modalWindow deleteAddress">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="row">
					<div class="col-xs-10 col-sm-10">
						<div class="modal-title">${title}</div>
					</div>
					<div class="col-xs-2 col-sm-2">
						<button type="button" data-dismiss="modal" aria-label="Close"
							class="close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="modal-wnd-content">
					<p><spring:theme code="text.address.remove.following"
                           text="The following address will be deleted from your Address Book" /></p>
				</div>
				<div class="modal-wnd-list bold">
					<div class="modal-wnd-list-item">
						<div>
							<address:viewAddressItem address="${address}" />
						</div>
					</div>
					<div class="modal-button-wrap">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-sm-offset-4">
								<ycommerce:testId code="addressRemove_delete_button">
									<a class="button button-main medium" data-address-id="${address.id}"
										href="remove-address/${address.id}"> <spring:theme
											code="text.address.delete" text="Delete" />
									</a>
								</ycommerce:testId>
							</div>
							<div class="col-xs-12 col-sm-4">
								<a data-dismiss="modal" aria-label="Close" class="button button-outlined medium" data-address-id="${address.id}">
									<spring:theme code="text.button.cancel" text="Cancel" />
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>