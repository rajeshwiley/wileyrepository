<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">

	<div class="row terms-conditions wrap">
		<cms:pageSlot position="TermsAndConditionsMenu" var="feature" element="div" class="col-xs-12 col-sm-3">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
		
		<cms:pageSlot position="TermsAndConditionsText" var="feature" element="div" class="col-xs-12 col-sm-9">
			<h1><spring:theme code="termsAndConditions.page.title"/></h1>
			<div class="my-account-wrap">
				<cms:component component="${feature}"/>
			</div>
		</cms:pageSlot>
	</div>

</template:page>
