<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="orderEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>
<c:if test="${orderEntry.consignmentStatus ne 'CANCELLED' and orderEntry.consignmentStatus ne 'WAITING'}">
<div class="tracking-numbers">
    <div class="row">
        <div class="col-xs-12 col-sm-11 col-sm-offset-1">
            <div class="tracking-numbers-title"><spring:theme code='consignment.trackingNumbersSection.title'/></div>
            <div class="tracking-numbers-list">
				<c:forEach items="${orderEntry.consignmentEntries}" var="consignmentEntry">
            		<order-common:consignmentItemForOrderEntry consignmentEntry="${consignmentEntry}"/>
				</c:forEach>
            </div>
        </div>
    </div>
</div>
</c:if>