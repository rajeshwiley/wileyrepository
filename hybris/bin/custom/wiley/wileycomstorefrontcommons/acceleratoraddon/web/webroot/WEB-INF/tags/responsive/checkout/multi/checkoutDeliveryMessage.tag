<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="checkout-delivery-addition">
	<spring:theme code="checkout.multi.deliveryMethod.message"
				  text="Items will ship as soon as they are available. See the Order Summary area for more information." />
</div>