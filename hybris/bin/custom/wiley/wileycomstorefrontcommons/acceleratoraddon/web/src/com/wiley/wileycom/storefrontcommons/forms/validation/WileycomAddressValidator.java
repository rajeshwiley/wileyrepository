package com.wiley.wileycom.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.COUNTRY;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.FIRSTNAME;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.LASTNAME;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.LINE1;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.LINE2;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.PHONE;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.POSTCODE;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.REGION;
import static com.wiley.wileycom.storefrontcommons.forms.validation.WileycomAddressValidator.WileycomAddressField.TOWN;


/**
 * Created by Georgii_Gavrysh on 7/13/2016.
 */
public class WileycomAddressValidator implements SmartValidator
{

	protected static final int MAX_FIELD_LENGTH = 255;

	private I18NFacade i18NFacade;

	private static final Map<WileycomAddressField, Integer> MAX_LENGTHS = new HashMap<>();

	static
	{
		MAX_LENGTHS.put(COUNTRY, Integer.valueOf(MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(FIRSTNAME, Integer.valueOf(150));
		MAX_LENGTHS.put(LASTNAME, Integer.valueOf(255));
		MAX_LENGTHS.put(PHONE, Integer.valueOf(255));
		MAX_LENGTHS.put(LINE1, Integer.valueOf(255));
		MAX_LENGTHS.put(LINE2, Integer.valueOf(255));
		MAX_LENGTHS.put(TOWN, Integer.valueOf(255));
		MAX_LENGTHS.put(POSTCODE, Integer.valueOf(255));
	}

	@Override
	public void validate(final Object object, final Errors errors, final Object... objects)
	{
		Assert.isTrue(object instanceof AddressForm);
		AddressForm addressForm = (AddressForm) object;
		String formPrefix = (String) objects[0];
		validateStandardFields(addressForm, getMaxLengths(), formPrefix, errors);
		validateCountrySpecificFields(addressForm, getMaxLengths(), formPrefix, errors);
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm,
			final Map<WileycomAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		CountryValidationStrategy countryValidationStrategy = countryValidationStrategyMap.get(isoCode);
		if (countryValidationStrategy == null)
		{
			countryValidationStrategy = countryValidationStrategyMap.get("");
		}
		countryValidationStrategy.validate(addressForm, maxLengths, formPrefix, errors);
	}

	protected void validateStandardFields(final AddressForm addressForm,
			final Map<WileycomAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		validateStringField(addressForm.getCountryIso(), COUNTRY, formPrefix, maxLengths.get(COUNTRY), errors);
		validateStringField(addressForm.getFirstName(), FIRSTNAME, formPrefix, maxLengths.get(FIRSTNAME), errors);
		validateStringField(addressForm.getLastName(), LASTNAME, formPrefix, maxLengths.get(LASTNAME), errors);
		validateStringField(addressForm.getLine1(), LINE1, formPrefix, maxLengths.get(LINE1), errors);
		validateStringField(addressForm.getTownCity(), TOWN, formPrefix, maxLengths.get(TOWN), errors);
		validateFieldMaxLength(addressForm.getLine2(), LINE2, formPrefix, maxLengths.get(LINE2), errors);
		validateStringField(addressForm.getPostcode(), POSTCODE, formPrefix, maxLengths.get(POSTCODE), errors);
		validateFieldMaxLength(addressForm.getPhone(), PHONE, formPrefix, maxLengths.get(PHONE), errors);
	}

	protected Map<WileycomAddressField, Integer> getMaxLengths()
	{
		return MAX_LENGTHS;
	}

	protected static void validateStringField(final String addressField, final WileycomAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		validateStringField(addressField, fieldType, formPrefix, maxFieldLength, errors, String.valueOf(maxFieldLength));
	}

	/**
	 * Validates String field on Empty or length restrictions
	 *
	 * @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param maxFieldLength
	 * 		- Max fields length
	 * @param errors
	 * 		- errors bundle
	 * @param messageArguments
	 * 		- custom message arguments
	 */
	protected static void validateStringField(final String addressField, final WileycomAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors, final String... messageArguments)
	{
		if (StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			String errorMessageKey = fieldType.getErrorLimitExceedMessageKey();
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), errorMessageKey, messageArguments, errorMessageKey);
		}
	}

	/**
	 * Validates whether field max lengths is not exceeded, do no makes checks on null, empty string
	 *
	 * @param value
	 * 		- field value
	 * @param addressField
	 * 		- field metadata
	 * @param maxFieldLength
	 * 		- max field length allowed
	 * @param errors
	 * 		- errors bundle
	 */
	protected static void validateFieldMaxLength(final String value, final WileycomAddressField addressField,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.isNotEmpty(value) && value.length() > maxFieldLength)
		{
			String[] messageArguments = { String.valueOf(maxFieldLength) };
			errors.rejectValue(formPrefix + addressField.getFieldKey(),
					addressField.errorKey, messageArguments, addressField.errorKey);
		}
	}

	protected static void validateFieldNotNull(final String addressField, final WileycomAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateRegionExists(final RegionData region, final WileycomAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (region == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected void validateRegion(final AddressForm addressForm, final Map<WileycomAddressField, Integer> maxLengths,
			final String formPrefix, final Errors errors)
	{
		final String countryIsoCode = addressForm.getCountryIso();
		List<RegionData> regionsForCountryIso = i18NFacade.getRegionsForCountryIso(countryIsoCode);

		if (CollectionUtils.isEmpty(regionsForCountryIso))
		{
			rejectRegion(formPrefix, errors);
			return;
		}
		final String regionIso = addressForm.getRegionIso();
		if (regionIso == null)
		{
			rejectRegion(formPrefix, errors);
			return;
		}
		validateFieldNotNull(regionIso, REGION, formPrefix, errors);
		RegionData regionData = i18NFacade.getRegion(countryIsoCode, regionIso);
		validateRegionExists(regionData, REGION, formPrefix, errors);
	}

	private void rejectRegion(final String formPrefix, final Errors errors)
	{
		errors.rejectValue(formPrefix + REGION.getFieldKey(), REGION.getErrorKey());
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return false;
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		validate(object, errors, "");
	}

	public void setI18NFacade(final I18NFacade i18NFacade)
	{
		this.i18NFacade = i18NFacade;
	}

	public enum WileycomAddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid", "address.firstName.invalid"),
		LASTNAME("lastName", "address.lastName.invalid", "address.lastName.invalid"),
		LINE1("line1", "address.line1.invalid", "address.line1.invalid"),
		LINE2("line2", "address.line2.invalid", "address.line2.invalid"),
		TOWN("townCity", "address.townCity.invalid", "address.townCity.invalid"),
		PROVINCE("townCity", "address.cityProvince.invalid", "address.cityProvince.invalid"),
		POSTCODE("postcode", "address.postcode.invalid", "address.postcode.invalid"),
		REGION("regionIso", "address.state.invalid", "address.state.invalid"),
		COUNTRY("countryIso", "address.country.invalid", "address.country.invalid"),
		PHONE("phone", "address.phone.invalid", "address.phone.invalid");

		private final String fieldKey;
		private final String errorKey;
		private final String errorLimitExceedMessageKey;

		WileycomAddressField(final String fieldKey, final String errorKey, final String errorLimitExceedMessageKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
			this.errorLimitExceedMessageKey = errorLimitExceedMessageKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}

		public String getErrorLimitExceedMessageKey()
		{
			return errorLimitExceedMessageKey;
		}
	}

	public interface CountryValidationStrategy
	{
		void validate(AddressForm addressForm, Map<WileycomAddressField, Integer> maxLengths,
				String formPrefix, Errors errors);
	}

	private final Map<String, CountryValidationStrategy> countryValidationStrategyMap
			= new HashMap<String, CountryValidationStrategy>()
	{ {
		put("US", (addressForm, maxLengths, formPrefix, errors) -> {
			validateRegion(addressForm, maxLengths, formPrefix, errors);
		});
		put("CA", (addressForm, maxLengths, formPrefix, errors) -> {
			validateRegion(addressForm, maxLengths, formPrefix, errors);
		});
		put("JP", (addressForm, maxLengths, formPrefix, errors) -> {
			validateStringField(addressForm.getLine2(), LINE2, formPrefix, maxLengths.get(LINE2), errors);
			validateRegion(addressForm, maxLengths, formPrefix, errors);
		});
		put("CN", (addressForm, maxLengths, formPrefix, errors) -> {
			validateRegion(addressForm, maxLengths, formPrefix, errors);
		});
		put("", (addressForm, maxLengths, formPrefix, errors) -> {
		});
	} };

}
