/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileycom.storefrontcommons.constants;

/**
 * Global class for all Wileycomstorefrontcommons constants. You can add global constants for your extension into this class.
 */
public final class WileycomstorefrontcommonsConstants extends GeneratedWileycomstorefrontcommonsConstants
{
	public static final String ORIGINAL_CONTEXT_PATH = "originalContextPath";
	public static final String LOAD_MORE_FACETS_VALUES_PATH = "/loadMoreFacetValues";

	public static final String SEARCH_BASE_PATH = "/search";
	public static final String CONTENT_SEARCH_BASE_PATH = "/content-search";

	public static final String SUBJECTS_CATEGORY = "subjects";
	public static final String ITEMS_PER_PAGE = "itemsPerPage";
	public static final String CURRENT_PAGE_SIZE = "currentPageSize";
	public static final String MARKETING_CONTENT_MENU_ITEMS = "marketingContentMenuItems";
}
