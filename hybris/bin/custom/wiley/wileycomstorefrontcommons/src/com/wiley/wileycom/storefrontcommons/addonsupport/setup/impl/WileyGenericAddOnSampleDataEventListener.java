package com.wiley.wileycom.storefrontcommons.addonsupport.setup.impl;

import de.hybris.platform.addonsupport.setup.events.AddonSampleDataImportedEvent;
import de.hybris.platform.addonsupport.setup.impl.GenericAddOnSampleDataEventListener;
import de.hybris.platform.commerceservices.setup.data.ImportData;

import org.apache.commons.collections4.CollectionUtils;


/**
 * GenericAddOnSampleDataEventListener substitution to fix IndexOutOfBoundsException case, when setup has no stores.
 * In addition, added check of content catalog namse.
 */
public class WileyGenericAddOnSampleDataEventListener extends GenericAddOnSampleDataEventListener
{

	@Override
	protected String createCacheKey(final AddonSampleDataImportedEvent event)
	{
		final ImportData importData = event.getImportData().get(0);
		StringBuilder builder = new StringBuilder();
		builder.append(event.getContext().getExtensionName());
		builder.append("_");
		builder.append(importData.getProductCatalogName());
		if (CollectionUtils.isNotEmpty(importData.getContentCatalogNames()))
		{
			builder.append("_");
			builder.append(importData.getContentCatalogNames().get(0));
		}
		if (CollectionUtils.isNotEmpty(importData.getStoreNames()))
		{
			builder.append("_");
			builder.append(importData.getStoreNames().get(0));
		}
		return builder.toString();
	}
}
