package com.wiley.wileycom.initialdata.setup.impl;

import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;

import java.io.InputStream;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.wileycom.initialdata.setup.WileycomDataImportService;

import static de.hybris.platform.util.CSVConstants.DEFAULT_ENCODING;


public class WileycomDataImportServiceImpl implements WileycomDataImportService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomDataImportServiceImpl.class);
	private static final String ACTIVE_ATTRIBUTE = "active";
	private static final boolean FAIL_IF_MISSING = false;

	@Resource
	private ImportService importService;

	@Resource
	private SetupImpexService setupImpexService;

	@Resource
	private SetupSyncJobService setupSyncJobService;

	@Resource
	private SetupSolrIndexerService setupSolrIndexerService;

	@Override
	public boolean importCommonData(final String basePath)
	{
		setupImpexService.importImpexFile(basePath + "/common/common-data.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/school-data.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/access-rights.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/workflow.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/workflow_en.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/user-groups.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importProductData(final String basePath, final String productCatalogId)
	{
		final String catalogBasePath = basePath + "/productCatalogs/" + productCatalogId;
		setupImpexService.importImpexFile(catalogBasePath + "/catalog.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/categories.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/products.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/stocklevels.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/prices.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importContentData(final String basePath, final String contentCatalogId)
	{
		final String catalogBasePath = basePath + "/contentCatalogs/" + contentCatalogId;
		setupImpexService.importImpexFile(catalogBasePath + "/catalog.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/cms-content.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/email-content.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importSynchronizationData(final String basePath, final Set<String> productCatalogIds,
			final Set<String> contentCatalogIds)
	{
		productCatalogIds.stream().forEach(productCatalogId -> setupSyncJobService.createProductCatalogSyncJob(productCatalogId));
		contentCatalogIds.stream().forEach(contentCatalogId -> setupSyncJobService.createContentCatalogSyncJob(contentCatalogId));

		setupImpexService.importImpexFile(basePath + "/common/sync.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importStoreData(final String basePath, final String storeUid)
	{
		final String storeBasePath = basePath + "/stores/" + storeUid;
		setupImpexService.importImpexFile(storeBasePath + "/store.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/site.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/jobs.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/solr.impex", FAIL_IF_MISSING);
		setupSolrIndexerService.createSolrIndexerCronJobs(storeUid + "Index");
		setupImpexService.importImpexFile(storeBasePath + "/solrtrigger.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importExportedData(final String dataArchivePath, final String mediaArchivePath)
	{
		return importArchives(dataArchivePath, mediaArchivePath);
	}

	@Override
	public boolean synchronizeCatalog(final String catalogId)
	{
		final PerformResult syncCronJobResult = setupSyncJobService.executeCatalogSyncJob(catalogId);
		return !isSyncRerunNeeded(syncCronJobResult);
	}

	private boolean importArchives(final String dataArchivePath, final String mediaArchivePath)
	{
		LOG.info("Starting import of archive files. Data file {}, media file {}", dataArchivePath, mediaArchivePath);

		final ImportConfig importConfig = new ImportConfig();
		InputStream dataArchive = this.getClass().getResourceAsStream(dataArchivePath);
		InputStream mediaArchive = this.getClass().getResourceAsStream(mediaArchivePath);
		if (dataArchive == null || mediaArchive == null)
		{
			LOG.info("Archive import skipped due to file absence: data archive exists={}, media archive exists={}",
					dataArchive != null, mediaArchive != null);
			return false;
		}
		importConfig.setScript(new StreamBasedImpExResource(dataArchive, DEFAULT_ENCODING));
		importConfig.setMediaArchive(new StreamBasedImpExResource(mediaArchive, DEFAULT_ENCODING));
		importConfig.setRemoveOnSuccess(false);

		final ImportResult importResult = importService.importData(importConfig);

		final String importCronJobCode = importResult.getCronJob().getCode();
		if (importResult.isSuccessful())
		{
			LOG.info("Successfully finished {} and {} import, import cron job code={}", dataArchivePath, mediaArchivePath,
					importCronJobCode);
			return true;
		}
		else
		{
			LOG.error("Finished import with error, files are {} and {}, see import cronjob with code={}", dataArchivePath,
					mediaArchivePath, importCronJobCode);
			return false;
		}
	}

	/**
	 * Determines if the sync job needs to be rerun
	 *
	 * @param syncCronJobResult
	 * 		the sync job result
	 * @return true is the sync job is <tt>null</tt> or unsuccessful
	 */
	public boolean isSyncRerunNeeded(final PerformResult syncCronJobResult)
	{
		return syncCronJobResult == null
				|| (CronJobStatus.FINISHED.equals(syncCronJobResult.getStatus())
				&& !CronJobResult.SUCCESS.equals(syncCronJobResult.getResult()));
	}
}