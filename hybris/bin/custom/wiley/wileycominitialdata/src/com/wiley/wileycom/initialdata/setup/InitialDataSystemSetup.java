/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileycom.initialdata.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.mockito.internal.util.collections.Sets;
import org.springframework.util.StopWatch;

import com.wiley.core.wiley.service.WileyJobsDataImportService;
import com.wiley.wileycom.initialdata.constants.WileycomInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = WileycomInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	private static final String DEACTIVATE_JOBS = "deactivateJob";
	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String IMPORT_UAT_DATA = "importUatData";
	private static final String IMPORT_PROD_DATA_P41 = "importProdDataP41";
	private static final String IMPORT_SAMPLE_DATA_P41 = "importSampleDataP41";
	private static final String IMPORT_SAMPLE_DATA_P42 = "importSampleDataP42";
	private static final String IMPORT_EXPORTED_PRODUCT_DATA = "importExportedProductData";
	private static final String IMPORT_EXPORTED_CONTENT_DATA = "importExportedContentData";
	private static final String SYNC_CATALOGS = "syncCatalogs";
	private static final String EXECUTE_SOLR_PRODUCT_INDEXING = "executeSolrProductIndexing";
	private static final String ACTIVATE_JOBS = "activateJobs";

	@Resource
	private WileycomDataImportService wileycomDataImportService;

	@Resource
	private WileyJobsDataImportService wileyJobsDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();
		params.add(createBooleanSystemSetupParameter(DEACTIVATE_JOBS, "Deactivate Jobs", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_UAT_DATA, "Import UAT Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_PROD_DATA_P41, "Import Prod Data P4.1", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA_P41, "Import Sample Data P4.1", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA_P42, "Import Sample Data P4.2", true));
		params.add(
				createBooleanSystemSetupParameter(IMPORT_EXPORTED_PRODUCT_DATA, "Import Exported Enriched Product Data",
						false));
		params.add(createBooleanSystemSetupParameter(IMPORT_EXPORTED_CONTENT_DATA, "Import Exported Content Data", false));
		params.add(createBooleanSystemSetupParameter(SYNC_CATALOGS, "Sync Catalogs", true));
		params.add(createBooleanSystemSetupParameter(EXECUTE_SOLR_PRODUCT_INDEXING, "Execute SOLR Product Indexing", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_JOBS, "Activate Jobs", true));

		return params;
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 * 		the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		StopWatch stopWatch = new StopWatch();
		boolean syncCommonAndWileycomContentCatalogs = false;

		if (getBooleanSystemSetupParameter(context, DEACTIVATE_JOBS))
		{
			runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("wileyb2c-ContentSearchConfigJob"),
					"Deactivation of wileyb2c-ContentSearchConfigJob cron job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("wileyb2c-SiteMapMediaJob"),
					"Deactivation of wileyb2c-SiteMapMediaJob cron job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.deactivateJob("sync wileyProductCatalog:Staged->Online"),
					"Deactivation of 'sync wileyProductCatalog:Staged->Online' job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("update-wileyb2cIndex-cronJob"),
					"Deactivation of update-wileyb2cIndex-cronJob cron job", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_CORE_DATA))
		{

			runNextStep(() -> wileycomDataImportService.importCommonData("/wileycominitialdata/import/coredata"),
					"Import common data in /wileycominitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importProductData("/wileycominitialdata/import/coredata", "wileyProductCatalog"),
					"Import wileyProductCatalog data in /wileycominitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/coredata", "commonContentCatalog"),
					"Import commonContentCatalog data in /wileycominitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/coredata", "wileycomContentCatalog"),
					"Import wileycomContentCatalog data in /wileycominitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/coredata", "dtsContentCatalog"),
					"Import dtsContentCatalog data in /wileycominitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/coredata", "micrositesContentCatalog"),
					"Import micrositesContentCatalog data in /wileycominitialdata/import/coredata", context, stopWatch);

			final Set<String> contentCatalogs = Sets.newSet("commonContentCatalog", "wileycomContentCatalog", "dtsContentCatalog",
					"micrositesContentCatalog");
			runNextStep(() -> wileycomDataImportService.importSynchronizationData("/wileycominitialdata/import/coredata",
					Collections.singleton("wileyProductCatalog"), contentCatalogs),
					"Import synchronizationData (wileyProductCatalog, commonContentCatalog, wileycomContentCatalog, "
							+ "dtsContentCatalog, micrositesContentCatalog) data in /wileycominitialdata/import/coredata",
					context, stopWatch);

			runNextStep(() -> wileycomDataImportService.importStoreData("/wileycominitialdata/import/coredata", "wileyb2c"),
					"Import wileyb2c store data in /wileycominitialdata/import/coredata", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA))
		{
			runNextStep(() -> wileycomDataImportService.importCommonData("/wileycominitialdata/import/sampledata"),
					"Import common data in /wileycominitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importProductData("/wileycominitialdata/import/sampledata", "wileyProductCatalog"),
					"Import wileyProductCatalog data in /wileycominitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/sampledata", "wileycomContentCatalog"),
					"Import wileycomContentCatalog data in /wileycominitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/sampledata", "dtsContentCatalog"),
					"Import dtsContentCatalog data in /wileycominitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/sampledata", "micrositesContentCatalog"),
					"Import micrositesContentCatalog data in /wileycominitialdata/import/sampledata", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_UAT_DATA))
		{
			runNextStep(() -> wileycomDataImportService
							.importProductData("/wileycominitialdata/import/uatdata", "wileyProductCatalog"),
					"Import wileyProductCatalog data in /wileycominitialdata/import/uatdata", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA_P41))
		{
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/sampledatap41", "wileycomContentCatalog"),
					"Import wileyProductCatalog data in /wileycominitialdata/import/sampledatap41", context, stopWatch);
			syncCommonAndWileycomContentCatalogs = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA_P42))
		{
			importSampleDataP42(context, stopWatch);
			syncCommonAndWileycomContentCatalogs = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_PROD_DATA_P41))
		{
			runNextStep(() -> wileycomDataImportService
							.importContentData("/wileycominitialdata/import/proddatap41", "wileycomContentCatalog"),
					"Import wileyProductCatalog data in /wileycominitialdata/import/proddatap41", context, stopWatch);
			syncCommonAndWileycomContentCatalogs = true;
		}

		doSyncForCommonAndWileycomCatalog(syncCommonAndWileycomContentCatalogs, context, stopWatch);

		if (getBooleanSystemSetupParameter(context, IMPORT_EXPORTED_PRODUCT_DATA))
		{
			runNextStep(() -> wileycomDataImportService
							.importExportedData("/wileycominitialdata/import/exporteddata/productdata.zip",
									"/wileycominitialdata/import/exporteddata/productmedia.zip"),
					"Import of exported product data", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_EXPORTED_CONTENT_DATA))
		{
			runNextStep(() -> wileycomDataImportService
							.importExportedData("/wileycominitialdata/import/exporteddata/contentdata.zip",
									"/wileycominitialdata/import/exporteddata/contentmedia.zip"),
					"Import of exported content data", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, SYNC_CATALOGS))
		{
			runNextStep(() -> wileyJobsDataImportService.executeCronJob("productsValidationCronJob", false),
					"Execution of productsValidationCronJob cron job", context, stopWatch);
			runNextStep(() -> wileycomDataImportService.synchronizeCatalog("wileyProductCatalog"),
					"Synchronization of wileyProductCatalog", context, stopWatch);
			syncCommonAndWileycomContentCatalogs(context, stopWatch);
			runNextStep(() -> wileycomDataImportService.synchronizeCatalog("dtsContentCatalog"),
					"Synchronization of dtsContentCatalog", context, stopWatch);
			runNextStep(() -> wileycomDataImportService.synchronizeCatalog("micrositesContentCatalog"),
					"Synchronization of micrositesContentCatalog", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, EXECUTE_SOLR_PRODUCT_INDEXING))
		{
			runNextStep(() -> wileyJobsDataImportService.executeCronJob("full-wileyb2cIndex-cronJob", false),
					"Execution of full-wileyb2cIndex-cronJob cron job", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, ACTIVATE_JOBS))
		{
			runNextStep(() -> wileyJobsDataImportService.executeCronJob("wileyb2c-ContentSearchConfigJob", true),
					"Execution of wileyb2c-ContentSearchConfigJob cron job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.executeCronJob("wileyb2c-SiteMapMediaJob", true),
					"Execution of wileyb2c-SiteMapMediaJob cron job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.activateJob("sync wileyProductCatalog:Staged->Online"),
					"Activation of 'sync wileyProductCatalog:Staged->Online' job", context, stopWatch);
			runNextStep(() -> wileyJobsDataImportService.activateCronJob("update-wileyb2cIndex-cronJob", true),
					"Activation of update-wileyb2cIndex-cronJob cron job", context, stopWatch);

		}
	}

	private void importSampleDataP42(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileycomDataImportService
						.importContentData("/wileycominitialdata/import/sampledatap42", "wileycomContentCatalog"),
				"Import wileyContentCatalog data in /wileycominitialdata/import/sampledatap42", context, stopWatch);
		runNextStep(() -> wileycomDataImportService
						.importContentData("/wileycominitialdata/import/sampledatap42", "dtsContentCatalog"),
				"Import dtsContentCatalog data in /wileycominitialdata/import/sampledatap42", context, stopWatch);

		runNextStep(() -> wileycomDataImportService.synchronizeCatalog("dtsContentCatalog"),
				"Synchronization of dtsContentCatalog", context, stopWatch);
	}

	private void doSyncForCommonAndWileycomCatalog(final boolean isSyncNeed, final SystemSetupContext context,
			final StopWatch stopWatch)
	{
		if (isSyncNeed)
		{
			syncCommonAndWileycomContentCatalogs(context, stopWatch);
		}
	}

	private void syncCommonAndWileycomContentCatalogs(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileycomDataImportService.synchronizeCatalog("commonContentCatalog"),
				"Synchronization of commonContentCatalog", context, stopWatch);
		runNextStep(() -> wileycomDataImportService.synchronizeCatalog("wileycomContentCatalog"),
				"Synchronization of wileycomContentCatalog", context, stopWatch);
	}

	private void runNextStep(final Runnable runnable, final String stepName, final SystemSetupContext context,
			final StopWatch stopWatch)
	{
		logInfo(context, stepName + " has started");
		stopWatch.start(stepName);
		runnable.run();
		stopWatch.stop();
		logInfo(context, stepName + " done in " + stopWatch.getLastTaskTimeMillis() + " ms");
	}
}
