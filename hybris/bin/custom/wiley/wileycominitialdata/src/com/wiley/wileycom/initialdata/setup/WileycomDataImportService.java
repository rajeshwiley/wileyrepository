package com.wiley.wileycom.initialdata.setup;

import java.util.Set;


/**
 * Data import service
 */
public interface WileycomDataImportService
{

	/**
	 * Import of common data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @return True if import was successful
	 */
	boolean importCommonData(String basePath);

	/**
	 * Import of product data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param productCatalogId
	 * 		Catalog id
	 * @return True if import was successful
	 */
	boolean importProductData(String basePath, String productCatalogId);

	/**
	 * Import of content data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param contentCatalogId
	 * 		Catalog id
	 * @return True if import was successful
	 */
	boolean importContentData(String basePath, String contentCatalogId);

	/**
	 * Import of content data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param productCatalogIds
	 * 		Product catalog ids
	 *@param contentCatalogIds
 * 		Content catalog ids  @return True if import was successful
	 */
	boolean importSynchronizationData(String basePath, Set<String> productCatalogIds,
			Set<String> contentCatalogIds);

	/**
	 * Import of store data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param storeUid
	 * 		Store uid
	  * @return True if import was successful
	 */
	boolean importStoreData(String basePath, String storeUid);

	/**
	 * Import of exported data
	 *
	 * @param dataArchivePath
	 * 		Data archive path
	 * @param mediaArchivePath
	 * 		Media archive path
	 * @return True if import was successful
	 */
	boolean importExportedData(String dataArchivePath, String mediaArchivePath);

	/**
	 * Synchronize catalog
	 *
	 * @param catalogId
	 * 		Catalog id
	 * @return True if sync was executed
	 */
	boolean synchronizeCatalog(String catalogId);
}
