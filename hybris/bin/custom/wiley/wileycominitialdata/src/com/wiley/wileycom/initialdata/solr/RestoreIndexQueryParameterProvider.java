package com.wiley.wileycom.initialdata.solr;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.solrfacetsearch.provider.ParameterProvider;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.util.WileyDateUtils;


public class RestoreIndexQueryParameterProvider implements ParameterProvider
{
	private static final Logger LOG = LoggerFactory.getLogger(RestoreIndexQueryParameterProvider.class);

	private static final String LAST_INDEX_TIME = "lastIndexTime";
	private static final String DATE_FORMAT = "dd/MM/yyyy HH:mm";
	private static final String RESTORE_INDEX_DATE_PROPERTY = "restore.solr.index.date";

	@Resource
	private ConfigurationService configurationService;

	private String time;

	@Override
	public Map<String, Object> createParameters()
	{
		Map<String, Object> parameterMap = new HashMap<>();
		String dateString = getConfiguredFormattedDate();

		LOG.info("Setting restore date value: [{}] for restore index process.", dateString);
		try
		{
			parameterMap.put(LAST_INDEX_TIME, WileyDateUtils.convertDateStringToDate(dateString, DATE_FORMAT));
		}
		catch (ParseException e)
		{
			throw new IllegalArgumentException("Can't convert ["
					+ dateString + "] to date, required pattern is: [" + DATE_FORMAT + "]");
		}

		return parameterMap;
	}

	private String getConfiguredFormattedDate()
	{
		String dateString = getDefaultFormattedDate();
		String configsDateString = configurationService.getConfiguration().getString(RESTORE_INDEX_DATE_PROPERTY, null);

		return StringUtils.isNotBlank(configsDateString) ? configsDateString : dateString;
	}

	private String getDefaultFormattedDate()
	{
		LocalDateTime defaultDateTime = WileyDateUtils.getClosestPastLocalDateTime(LocalTime.parse(time));
		return defaultDateTime.format(DateTimeFormatter.ofPattern(DATE_FORMAT));
	}

	@Required
	public void setTime(final String time)
	{
		this.time = time;
	}
}
