package com.wiley.wileycom.initialdata.setup.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.wileycom.initialdata.setup.WileycomDataImportService;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Integration test for {@link WileycomDataImportService}
 *
 * Covers cases when SearchRestrictions are applied for catalogsyncuser and other sync cases.
 */
@IntegrationTest
public class WileycomDataImportServiceIntegrationTest extends ServicelayerTest
{
	public static final String TEST_SYNC_PRODUCT_CATALOG_CORRECT = "testSyncProductCatalogCorrect";
	public static final String TEST_SYNC_PRODUCT_CATALOG_INCORRECT = "testSyncProductCatalogIncorrect";
	public static final String TEST_SYNC_PRODUCT_CATALOG_EXTERNAL_IMAGE = "testSyncProductCatalogExternalImage";

	public static final String APPROVED_BASE_PRODUCT = "TEST_APPROVED_BASE_PRODUCT";
	public static final String APPROVED_VARIANT_PRODUCT_WITH_APPROVED_BASE_PRODUCT =
			"TEST_APPROVED_VARIANT_PRODUCT_WITH_APPROVED_BASE_PRODUCT";

	@Resource
	private WileycomDataImportService wileycomDataImportService;

	@Resource
	private SetupSyncJobService setupSyncJobService;

	@Resource
	private UserService userService;

	@Resource
	private ProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/catalogsyncuser-and-restriction.impex",
				"UTF-8");
	}

	@Test
	public void shouldSyncCorrectProducts() throws Exception
	{
		//given
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/correct/catalog.impex",
				"UTF-8");
		setupSyncJobService.createProductCatalogSyncJob(TEST_SYNC_PRODUCT_CATALOG_CORRECT);
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/correct/update-catalog-version-sync-job.impex",
				"UTF-8");
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/correct/products.impex",
				"UTF-8");

		//when
		boolean syncExecuted = wileycomDataImportService.synchronizeCatalog(TEST_SYNC_PRODUCT_CATALOG_CORRECT);
		//then
		assertThat(syncExecuted).isTrue();

		// if base and it's variant products have already been synchronized before,
		// sync of approved variant should be Ok even if base product has not 'approved' status

		//given
		CatalogVersionModel stagedCatalogVersion = catalogVersionService.getCatalogVersion(TEST_SYNC_PRODUCT_CATALOG_CORRECT,
				"Staged");

		ProductModel baseProductStaged = productService.getProductForCode(stagedCatalogVersion, APPROVED_BASE_PRODUCT);
		baseProductStaged.setApprovalStatus(ArticleApprovalStatus.CHECK);
		ProductModel variantProductStaged = productService.getProductForCode(stagedCatalogVersion,
				APPROVED_VARIANT_PRODUCT_WITH_APPROVED_BASE_PRODUCT);
		variantProductStaged.setDescription("updated description");
		modelService.saveAll(baseProductStaged, variantProductStaged);

		//when
		syncExecuted = wileycomDataImportService.synchronizeCatalog(TEST_SYNC_PRODUCT_CATALOG_CORRECT);
		//then
		assertThat(syncExecuted).isTrue();
	}

	@Test
	public void shouldNotSyncIncorrectProducts() throws Exception
	{
		//given
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/incorrect/catalog.impex",
				"UTF-8");
		setupSyncJobService.createProductCatalogSyncJob(TEST_SYNC_PRODUCT_CATALOG_INCORRECT);
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/incorrect/update-catalog-version-sync-job.impex",
				"UTF-8");
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/incorrect/products.impex",
				"UTF-8");
		//when
		boolean syncExecuted = wileycomDataImportService.synchronizeCatalog(TEST_SYNC_PRODUCT_CATALOG_INCORRECT);
		//then
		assertThat(syncExecuted).isFalse();
	}

	@Test
	public void shouldSyncNewlyCreatedExternalImageMediaContainer() throws Exception
	{
		//given
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/externalImage/catalog.impex",
				"UTF-8");
		setupSyncJobService.createProductCatalogSyncJob(TEST_SYNC_PRODUCT_CATALOG_EXTERNAL_IMAGE);
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/externalImage/update-catalog-version-sync-job.impex",
				"UTF-8");
		importCsv(
				"/wileycominitialdata/test/initialdata/setup/impl/"
						+ "WileycomDataImportServiceIntegrationTest/externalImage/products.impex",
				"UTF-8");
		//when
		boolean syncExecuted = wileycomDataImportService.synchronizeCatalog(TEST_SYNC_PRODUCT_CATALOG_EXTERNAL_IMAGE);
		//then
		assertThat(syncExecuted).isTrue();
	}
}
