package com.wiley.storefrontcommons.locale.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;


public class WileyLocaleFromGeolocationHandler implements WileyLocaleHandler
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyLocaleFromGeolocationHandler.class);
	static final String COUNTRY_HEADER_NAME = "CloudFront-Viewer-Country";

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private WileyLocaleService wileyLocaleService;
	@Autowired
	private CommerceCommonI18NService commerceCommonI18NService;
	@Autowired
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	private String encodedLocale;

	@Override
	public Optional<String> getEncodedLocale()
	{
		return Optional.ofNullable(encodedLocale);
	}

	public void init()
	{
		final String countryCode = request.getHeader(COUNTRY_HEADER_NAME);
		if (countryCode != null)
		{
			try
			{
				Optional<CountryModel> countryModelOpt = commerceCommonI18NService.getAllCountries()
						.stream()
						.filter(country -> country.getIsocode().equals(countryCode))
						.findFirst();

				if (countryModelOpt.isPresent()) {

					boolean hasExternalSiteRedirectForCountry =
							wileycomStoreSessionFacade.getExternalSiteRedirect(countryCode).isPresent();

					if (!hasExternalSiteRedirectForCountry) {
						CountryModel countryModel = countryModelOpt.get();
						encodedLocale = wileyLocaleService.getEncodedLocale(countryModel, countryModel.getDefaultLanguage());
					}
				}
			}
			catch (UnknownIdentifierException e)
			{
				LOG.error("No Country for code: [" + countryCode + "] found during resolving locale.", e);
			}
		}

	}
}
