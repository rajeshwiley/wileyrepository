package com.wiley.storefrontcommons.locale.impl;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.storefrontcommons.locale.RedirectionException;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.Optional;


public class WileyExternalSiteHandler implements WileyLocaleHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyExternalSiteHandler.class);

	@Autowired
	private BaseStoreService baseStoreService;
	@Autowired
	private WileyLocaleService wileyLocaleService;
	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Autowired
	private WileyLocaleFromRequestUrlHandler wileyLocaleFromRequestUrlHandler;

	@Override
	public Optional<String> getEncodedLocale()
	{
		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (CollectionUtils.isEmpty(currentBaseStore.getExternalLinks())) {
			return Optional.empty();
		}

		final Optional<String> encodedLocaleFromUrlOpt = wileyLocaleFromRequestUrlHandler.getEncodedLocale();
		if (encodedLocaleFromUrlOpt.isPresent()) {
			CountryModel currentCountryFromUrl =
					wileyLocaleService.getCountryForEncodedLocale(encodedLocaleFromUrlOpt.get());
			Optional<String> redirectUrl =
					wileycomStoreSessionFacade.getExternalSiteRedirect(currentCountryFromUrl.getIsocode());
			if (redirectUrl.isPresent()) {
				LOG.debug("External site redirect occurs for country to [{}], redirecting to [{}]",
						currentCountryFromUrl.getIsocode(), redirectUrl.get());
				throw new RedirectionException(redirectUrl.get());
			}
		}
		return Optional.empty();
	}
}
