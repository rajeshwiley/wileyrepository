package com.wiley.storefrontcommons.servicelayer.web;

import de.hybris.platform.core.MasterTenant;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.servicelayer.web.DefaultSessionCloseStrategy;
import de.hybris.platform.spring.HybrisContextLoaderListener;
import de.hybris.platform.util.Utilities;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Fix for ECSC-17657 "Prod: Fix java.lang.IllegalArgumentException: unknow web root '' cannot find any mapped extension"
 *
 * This is mostly a copy of DefaultSessionCloseStrategy with
 * applied fix (in case of empty webroot - use tenant wide timeout setting)
 */
public class WileySessionCloseStrategy extends DefaultSessionCloseStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySessionCloseStrategy.class);

	public void setTimeoutOnHttpSessionCreation(final HttpSession httpSession)
	{
		Tenant tenant = getCurrentTenant(httpSession);
		if (!trySettingExtensionSpecificTimeout(httpSession, tenant))
		{
			int sessionTimeoutInSecs = tenant.getConfig().getInt("default.session.timeout", 86400);
			httpSession.setMaxInactiveInterval(sessionTimeoutInSecs);
		}

	}

	private boolean trySettingExtensionSpecificTimeout(final HttpSession httpSession, final Tenant tenant)
	{
		String webApp = httpSession.getServletContext().getContextPath();
		boolean timeoutSet = false;
		if (StringUtils.isNotEmpty(webApp))
		{
			String extensionName = Utilities.getExtensionForWebroot(webApp, tenant.getTenantID());
			int extensionTimeout = tenant.getConfig().getInt(extensionName + ".session.timeout", -1);
			if (extensionTimeout > 0)
			{
				httpSession.setMaxInactiveInterval(extensionTimeout);
				timeoutSet = true;
			}
		}
		return timeoutSet;
	}

	private Tenant getCurrentTenant(final HttpSession httpSession)
	{
		String tenantId = HybrisContextLoaderListener.getTenantIDForWebapp(httpSession.getServletContext());
		if (tenantId == null)
		{
			LOG.debug("tenantId is null, using Master Tenant");
			return MasterTenant.getInstance();
		}
		else
		{
			Tenant tenant = Registry.getTenantByID(tenantId);
			if (tenant != null)
			{
				LOG.debug("tenantId is provided, using {}", tenantId);
				return tenant;
			}
			else
			{
				LOG.debug("could not find tenant by tenantId {} , using Master Tenant", tenantId);
				return MasterTenant.getInstance();
			}
		}
	}
}
