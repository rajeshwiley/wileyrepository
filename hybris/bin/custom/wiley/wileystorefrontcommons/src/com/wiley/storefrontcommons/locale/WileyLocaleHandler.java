package com.wiley.storefrontcommons.locale;

import java.util.Optional;


public interface WileyLocaleHandler
{
	Optional<String> getEncodedLocale();
}
