package com.wiley.storefrontcommons.component.cache.impl;

import de.hybris.platform.acceleratorcms.component.cache.impl.DefaultCmsCacheKeyProvider;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.core.model.c2l.CountryModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


public class CurrentCountryCmsCacheKeyProvider extends DefaultCmsCacheKeyProvider
{
	private WileycomI18NService wileycomI18NService;

	@Override
	public StringBuilder getKeyInternal(final HttpServletRequest request, final SimpleCMSComponentModel component)
	{
		final StringBuilder buffer = new StringBuilder(super.getKeyInternal(request, component));
		wileycomI18NService.getCurrentCountry().map(CountryModel::getIsocode).ifPresent(buffer::append);
		return buffer;
	}

	@Required
	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}
}
