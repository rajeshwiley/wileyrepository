package com.wiley.storefrontcommons.locale.impl;

import de.hybris.platform.core.model.c2l.CountryModel;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;
import com.wiley.storefrontcommons.security.cookie.EnhancedCookieGenerator;


public class WileyLocaleFromCookieHandler implements WileyLocaleHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyLocaleFromCookieHandler.class);

	@Autowired
	private HttpServletRequest request;
	@Autowired
	private WileyLocaleService wileyLocaleService;
	@Autowired
	private BaseStoreService baseStoreService;

	private EnhancedCookieGenerator countryCookieGenerator;

	private String encodedLocale;

	@Override
	public Optional<String> getEncodedLocale()
	{
		return Optional.ofNullable(encodedLocale);
	}

	public void init()
	{
		Optional<String> cookieCountryCodeHeader = countryCookieGenerator.findValueByName(request.getCookies());
		if (cookieCountryCodeHeader.isPresent())
		{
			final String cookieEncodedLocaleValue = cookieCountryCodeHeader.get();
			if (wileyLocaleService.isValidEncodedLocale(cookieEncodedLocaleValue)
					&& !isAnyExternalSiteTiedToSelectedCountry(cookieEncodedLocaleValue))
			{
				this.encodedLocale = cookieEncodedLocaleValue;
			}
			else
			{
				LOG.debug("Invalid Encoded Locale for cookieEncodedLocaleValue : [" + cookieEncodedLocaleValue + "]");
			}
		}
	}

	private boolean isAnyExternalSiteTiedToSelectedCountry(final String cookieEncodedLocaleValue)
	{
		boolean isExternalSiteTiedToSelectedCountry = false;
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (null != currentBaseStore && CollectionUtils.isNotEmpty(currentBaseStore.getExternalLinks()))
		{
			final CountryModel countryForLocale = wileyLocaleService.getCountryForEncodedLocale(cookieEncodedLocaleValue);
			isExternalSiteTiedToSelectedCountry = baseStoreService.getCurrentBaseStore().getExternalLinks().stream()
					.anyMatch(externalLink -> externalLink.getCountry().equals(countryForLocale));
		}
		return isExternalSiteTiedToSelectedCountry;
	}

	@Required
	public void setCountryCookieGenerator(final EnhancedCookieGenerator countryCookieGenerator)
	{
		this.countryCookieGenerator = countryCookieGenerator;
	}
}
