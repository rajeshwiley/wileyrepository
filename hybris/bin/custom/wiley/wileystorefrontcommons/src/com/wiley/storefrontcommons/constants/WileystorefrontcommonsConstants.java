/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.constants;

import static de.hybris.platform.addonsupport.controllers.AbstractAddOnController.FORWARD_PREFIX;
import static de.hybris.platform.addonsupport.controllers.AbstractAddOnController.REDIRECT_PREFIX;


/**
 * Global class for all Wileystorefrontcommons constants. You can add global constants for your extension into this class.
 */
public final class WileystorefrontcommonsConstants extends GeneratedWileystorefrontcommonsConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileystorefrontcommons";

	public static final String ALM_SESSION_TOKEN = "alm.session.token.cookie";
	public static final String ALM_REDIRECT_PAGE = "alm.redirect.page";
	public static final String AS_AGENT_GROUP_UID = "asagentgroup";
	public static final String DEFAULT_SESSION_TIMEOUT = "default.session.timeout";
	// implement here constants used by this extension

	public static final Integer MONTH_AMOUNT = 12;

	public static final String MESSAGE_KEY_PLACE_ORDER_FAILED = "checkout.placeOrder.failed";
	public static final String MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED = "text.checkout.multi.checkoutstep.notprovided";
	public static final String FORWARD_TO_404 = FORWARD_PREFIX + "/404";
	public static final String REDIRECT_TO_404 = REDIRECT_PREFIX + "/404";

	private WileystorefrontcommonsConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
