package com.wiley.storefrontcommons.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants;


@SuppressWarnings("PMD")
public class WileystorefrontcommonsManager extends GeneratedWileystorefrontcommonsManager
{
	public static WileystorefrontcommonsManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WileystorefrontcommonsManager) em.getExtension(WileystorefrontcommonsConstants.EXTENSIONNAME);
	}
}
