package com.wiley.storefrontcommons.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;


import org.apache.commons.lang.StringEscapeUtils;


/**
 * Resolves page title according to SEO title attribute.
 */
public class WileyPageTitleResolver extends PageTitleResolver
{

	public String resolveProductPageTitle(final ProductModel product)
	{
		String seoTitle = product.getSeoTitleTag();
		if (seoTitle != null)
		{
			return StringEscapeUtils.escapeHtml(seoTitle);
		}
		return super.resolveProductPageTitle(product);
	}

	public String resolveCategoryPageTitle(final CategoryModel category)
	{
		String seoTitle = category.getSeoTitleTag();
		if (seoTitle != null)
		{
			return StringEscapeUtils.escapeHtml(seoTitle);
		}
		return super.resolveCategoryPageTitle(category);
	}
}
