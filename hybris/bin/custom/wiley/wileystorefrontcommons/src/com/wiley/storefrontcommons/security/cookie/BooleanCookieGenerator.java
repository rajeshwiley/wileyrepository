package com.wiley.storefrontcommons.security.cookie;

import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;


/**
 * Cookie generator to encapsulate boolean value handling
 */
public class BooleanCookieGenerator extends EnhancedCookieGenerator
{
	@Override
	public void addCookie(final HttpServletResponse response, final String cookieValue)
	{
		super.addCookie(response, cookieValue != null ? Boolean.valueOf(cookieValue).toString() : null);
	}

	public void addCookie(final HttpServletResponse response, @NotNull final Boolean cookieValue)
	{
		super.addCookie(response, cookieValue.toString());
	}

	/**
	 * Search String cookie value and return it Boolean representation
	 *
	 * @param cookies
	 * 		Request cookies, could be null
	 * @return Return {@link Optional} boolean container with value, transformed from string value.
	 * If no cookie value found, returns {@link Optional#empty()}
	 */
	public Optional<Boolean> findBooleanValueByName(final Cookie[] cookies)
	{
		Optional<String> cookieValue = super.findValueByName(cookies);
		if (cookieValue.isPresent())
		{
			return Optional.of(Boolean.valueOf(cookieValue.get()));
		}

		return Optional.empty();
	}
}
