package com.wiley.storefrontcommons.security.strategy;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.RedirectStrategy;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyRedirectStrategy extends RedirectStrategy
{
	/**
	 * redirect without return url
	 *
	 * @param response
	 * @param url
	 * @throws IOException
	 */
	void sendRedirect(@Nonnull HttpServletResponse response, @Nonnull String url)
			throws IOException;
}
