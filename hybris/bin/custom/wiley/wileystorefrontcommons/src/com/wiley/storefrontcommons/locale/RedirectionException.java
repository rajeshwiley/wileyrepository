package com.wiley.storefrontcommons.locale;


public class RedirectionException extends RuntimeException
{
	private String target;

	public RedirectionException(final String target)
	{
		this.target = target;
	}

	public String getTarget()
	{
		return target;
	}
}
