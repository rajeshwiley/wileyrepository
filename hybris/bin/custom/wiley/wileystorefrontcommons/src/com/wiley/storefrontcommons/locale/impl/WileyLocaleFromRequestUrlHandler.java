package com.wiley.storefrontcommons.locale.impl;

import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;

import static de.hybris.platform.acceleratorservices.constants.AcceleratorServicesConstants.LANGUAGE_ENCODING;


public class WileyLocaleFromRequestUrlHandler implements WileyLocaleHandler
{
	private static final String SLASH = "/";

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private UrlEncoderService urlEncoderService;

	@Autowired
	private WileyLocaleService wileyLocaleService;

	private String encodedLocale;

	@Override
	public Optional<String> getEncodedLocale()
	{
		return Optional.ofNullable(encodedLocale);
	}

	public void init()
	{
		int index = ArrayUtils.indexOf(urlEncoderService.getEncodingAttributesForSite().toArray(), LANGUAGE_ENCODING);

		if (index != -1)
		{
			String uri = request.getRequestURI().toString();
			String contextPath = request.getContextPath();
			final String[] splitUri = StringUtils.split(uri, SLASH);
			int encodingStartIndex = ArrayUtils.isNotEmpty(splitUri) && StringUtils.remove(contextPath, SLASH).equals(
					splitUri[0]) ? 1 : 0;

			if (encodingStartIndex + index < splitUri.length)
			{
				String encodedLocale = splitUri[encodingStartIndex + index];
				if (wileyLocaleService.isValidEncodedLocale(encodedLocale))
				{
					this.encodedLocale = encodedLocale;
				}
			}
		}
	}
}
