package com.wiley.storefrontcommons.config;

public interface SiteContactConfigUtil
{
	/**
	 * Retrieves contact phone if failed to place the order
	 *
	 * @return property value or empty string in case of absent current site in session
	 */
	String getContactPhone();

	/**
	 * Retrieves contact mail if failed to place the order
	 *
	 * @return property value or empty string in case of absent current site in session
	 */
	String getContactMail();
}
