package com.wiley.storefrontcommons.config.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.storefrontcommons.config.SiteContactConfigUtil;
import com.wiley.storefrontcommons.locale.impl.WileyExternalSiteHandler;


public class SiteContactConfigUtilImpl implements SiteContactConfigUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyExternalSiteHandler.class);

	@Resource
	private CMSSiteService cmsSiteService;

	@Resource
	private SiteConfigService siteConfigService;


	public String getContactPhone()
	{
		return getProperty("checkout.placing.order.paymentFailed.phone");
	}

	public String getContactMail()
	{
		return getProperty("checkout.placing.order.paymentFailed.email");
	}

	private String getProperty(final String key)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (currentSite == null)
		{
			LOG.error("Current Site is null.Not possible to provide contact info");
			return StringUtils.EMPTY;
		}
		else
		{
			return siteConfigService.getProperty(key);
		}
	}
}
