package com.wiley.storefrontcommons.context.impl;

import de.hybris.platform.acceleratorcms.context.impl.DefaultContextInformationLoader;
import de.hybris.platform.acceleratorcms.preview.strategies.PreviewContextInformationLoaderStrategy;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.util.Locale;
import java.util.Optional;


/**
 * Created by Maksim_Kozich on 28.03.17.
 */
public class WileyContextInformationLoaderImpl extends DefaultContextInformationLoader
{
	@Override
	protected void loadFakeLanguage(final LanguageModel languageModel)
	{
		if (languageModel != null)
		{
			Optional<Locale> loc = getI18NService().getSupportedLocales().stream().filter(
					l -> l.toString().equals(languageModel.getIsocode()))
					.findFirst();
			// use super DefaultContextInformationLoader approach if locale not found
			getI18NService().setCurrentLocale(loc.orElse(new Locale(languageModel.getIsocode())));
		}
	}

	public static class LoadLanguageStrategy extends WileyContextInformationLoaderImpl implements
			PreviewContextInformationLoaderStrategy
	{

		@Override
		public void initContextFromPreview(final PreviewDataModel preview)
		{
			if (preview.getLanguage() != null)
			{
				super.loadFakeLanguage(preview.getLanguage());
			}

		}

	}
}
