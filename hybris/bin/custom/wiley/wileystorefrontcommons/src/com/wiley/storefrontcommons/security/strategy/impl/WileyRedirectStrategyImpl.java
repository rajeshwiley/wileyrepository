package com.wiley.storefrontcommons.security.strategy.impl;

import java.io.IOException;
import java.net.URLEncoder;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wiley.storefrontcommons.security.strategy.WileyRedirectStrategy;


/**
 * Custom implementation of RedirectStrategy
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyRedirectStrategyImpl implements WileyRedirectStrategy
{
	/**
	 * redirect with return url
	 *
	 * @param request
	 * @param response
	 * @param url
	 * @throws IOException
	 */
	@Override
	public void sendRedirect(@Nonnull final HttpServletRequest request,
			@Nonnull final HttpServletResponse response, @Nonnull final String url)
			throws IOException
	{
		final String requestUrl = new StringBuilder(request.getScheme()).append("://").append(request.getServerName())
				.append(request.getRequestURI()).toString();
		final String encodedReturnUrl = URLEncoder.encode(requestUrl, "UTF-8");
		final String encodedRedirectUrl = response.encodeRedirectURL(url) + "?returnUrl=" + encodedReturnUrl;
		response.sendRedirect(encodedRedirectUrl);
	}

	/**
	 * redirect without return url
	 *
	 * @param response
	 * @param url
	 * @throws IOException
	 */
	@Override
	public void sendRedirect(@Nonnull final HttpServletResponse response, @Nonnull final String url)
			throws IOException
	{
		final String encodedRedirectUrl = response.encodeRedirectURL(url);
		response.sendRedirect(encodedRedirectUrl);
	}
}
