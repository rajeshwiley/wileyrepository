package com.wiley.storefrontcommons.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.site.BaseSiteService;

import java.util.Date;
import java.util.Iterator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.util.Assert;

import com.wiley.storefrontcommons.security.entry.LoginFailure;


public class RedisBruteForceAttackCounter implements BruteForceAttackCounter
{
	private static final Logger LOG = Logger.getLogger(RedisBruteForceAttackCounter.class);

	private static final String LOGIN_FAILURE_COUNTER_KEY = "loginFailureCounterHash";

	private final Integer maxFailedLogins;
	private final Integer cacheSizeLimit;
	private final Integer cacheExpiration;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource(name = "redisTemplate")
	private HashOperations<String, String, LoginFailure> hashOperations;

	public RedisBruteForceAttackCounter(final Integer maxFailedLogins, final Integer cacheExpiration,
			final Integer cacheSizeLimit)
	{
		Assert.notNull(maxFailedLogins, "Constructor param maxFailedLogins must not be null.");
		Assert.notNull(cacheExpiration, "Constructor param cacheExpiration must not be null.");
		Assert.notNull(cacheSizeLimit, "Constructor param cacheSizeLimit must not be null.");

		this.maxFailedLogins = maxFailedLogins;
		this.cacheSizeLimit = cacheSizeLimit;
		this.cacheExpiration = cacheExpiration;
	}

	@Override
	public void registerLoginFailure(final String userUid)
	{
		if (StringUtils.isNotEmpty(userUid))
		{
			final LoginFailure count = get(prepareUserUid(userUid), Integer.valueOf(0));
			count.setCounter(Integer.valueOf(Math.min(count.getCounter().intValue() + 1, maxFailedLogins.intValue() + 1)));
			count.setDate(new Date());
			hashOperations.put(getLoginFailureCounterKey(), prepareUserUid(userUid), count);

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Failed Login for user " + userUid + ", count now " + count.getCounter());
			}
		}
	}


	@Override
	public boolean isAttack(final String userUid)
	{
		if (StringUtils.isNotEmpty(userUid))
		{
			return maxFailedLogins.compareTo(get(prepareUserUid(userUid), Integer.valueOf(0)).getCounter()) <= 0;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void resetUserCounter(final String userUid)
	{
 		if (StringUtils.isNotEmpty(userUid))
		{
			hashOperations.delete(getLoginFailureCounterKey(), prepareUserUid(userUid));
		}
	}

	@Override
	public int getUserFailedLogins(final String userUid)
	{
		if (StringUtils.isNotEmpty(userUid))
		{
			return get(prepareUserUid(userUid), Integer.valueOf(0)).getCounter().intValue();
		}
		else
		{
			return 0;
		}
	}


	protected LoginFailure get(final String userUid, final Integer startValue)
	{
		LoginFailure value = hashOperations.get(getLoginFailureCounterKey(), prepareUserUid(userUid));
		if (value == null)
		{
			value = new LoginFailure(startValue, new Date());
			hashOperations.put(getLoginFailureCounterKey(), prepareUserUid(userUid), value);
			if (hashOperations.size(getLoginFailureCounterKey()) > cacheSizeLimit.intValue())
			{
				evict();
			}
		}
		return value;
	}


	protected String prepareUserUid(final String userUid)
	{
		return StringUtils.lowerCase(userUid);
	}


	protected void evict()
	{
		if (hashOperations.size(getLoginFailureCounterKey()) > cacheSizeLimit.intValue())
		{
			final Iterator<String> cacheIterator = hashOperations.keys(getLoginFailureCounterKey()).iterator();
			final Date dateLimit = DateUtils.addMinutes(new Date(), 0 - cacheExpiration.intValue());
			while (cacheIterator.hasNext())
			{
				final String userKey = cacheIterator.next();
				final LoginFailure value = hashOperations.get(getLoginFailureCounterKey(), userKey);
				if (value.getDate().before(dateLimit))
				{
					hashOperations.delete(getLoginFailureCounterKey(), userKey);
				}
			}
		}
	}

	protected String getLoginFailureCounterKey() {
		return baseSiteService.getCurrentBaseSite().getUid() + LOGIN_FAILURE_COUNTER_KEY;
	}
}
