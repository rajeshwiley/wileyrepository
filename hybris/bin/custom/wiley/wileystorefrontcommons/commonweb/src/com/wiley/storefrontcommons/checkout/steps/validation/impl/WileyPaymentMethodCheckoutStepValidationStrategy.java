package com.wiley.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import javax.annotation.Resource;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.flow.WileyCheckoutFlowFacade;
import com.wiley.storefrontcommons.checkout.steps.validation.AbstractWileyCheckoutStepValidationStrategy;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED;


public class WileyPaymentMethodCheckoutStepValidationStrategy extends AbstractWileyCheckoutStepValidationStrategy
{
	@Resource(name = "wileyCheckoutFlowFacade")
	private WileyCheckoutFlowFacade checkoutFlowFacade;

	@Override
	public ValidationResults validate(final RedirectAttributes redirectAttributes)
	{
		ValidationResults validationResults = ValidationResults.SUCCESS;

		if (checkoutFlowFacade.hasNoPaymentInfo())
		{
			validationResults = ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
			addFlashErrorMessage(redirectAttributes, MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED);

		}

		return validationResults;
	}
}
