package com.wiley.storefrontcommons.authentication;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.util.Assert;
import org.springframework.web.util.WebUtils;

import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;
import com.wiley.core.integration.alm.authentication.service.AlmAuthenticationService;
import com.wiley.storefrontcommons.errorHandlers.WileyGlobalExceptionHandler;
import com.wiley.storefrontcommons.security.WileyAssistedServiceAgentLoginService;
import com.wiley.storefrontcommons.security.WileyLoginService;
import com.wiley.storefrontcommons.security.strategy.WileyRedirectStrategy;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.ALM_SESSION_TOKEN;
import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Alm authentication filter
 */
public class WileyAlmAuthenticationFilter extends AbstractAuthenticationProcessingFilter
{
	@Autowired
	private AlmAuthenticationService almAuthenticationService;

	@Autowired
	private WileyRedirectStrategy redirectStrategy;

	@Autowired
	private WileyAssistedServiceAgentLoginService assistedServiceAgentLoginService;

	@Autowired
	private WileyLoginService wileyLoginService;

	@Autowired
	private WileyGlobalExceptionHandler wileyGlobalExceptionHandler;

	@Value("${wileyas.base.url}")
	private String baseUrl;

	@Value("${" + ALM_SESSION_TOKEN + "}")
	private String almSessionTokenCookie;

	@Autowired
	public WileyAlmAuthenticationFilter(final WileyCookieRequestMatcher wileyCookieRequestMatcher)
	{
		super(wileyCookieRequestMatcher);
	}

	@Override
	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException
	{
		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) res;
		final Cookie almSessionId = WebUtils.getCookie(request, this.almSessionTokenCookie);

		if (almSessionId == null || StringUtils.isBlank(almSessionId.getValue()))
		{
			unsuccessfulAuthentication(request, response,
					new AuthenticationServiceException("Customer token is empty"));
		}
		else
		{
			super.doFilter(request, response, chain);
		}
	}

	@Override
	public Authentication attemptAuthentication(final HttpServletRequest request,
			final HttpServletResponse response)
			throws AuthenticationException, IOException
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication != null)
		{
			wileyLoginService.closeSession(request);
			redirectStrategy.sendRedirect(response,
					request.getRequestURL().toString());
			return null;
		}

		final String almSessionToken = captureAlmSessionToken(request, almSessionTokenCookie);
		Assert.notNull(almSessionToken, "almSessionToken cannot be null.");
		final AlmAuthenticationResponseDto almAuthenticationResponseDto =
				almAuthenticationService.validateSessionToken(almSessionToken);
		final String userId = almAuthenticationResponseDto.getUserId();
		final String imitateeId = almAuthenticationResponseDto.getImitateeId();
		final HttpStatus httpStatus = almAuthenticationResponseDto.getHttpStatus();
		request.setAttribute("commonResourcePath", wileyGlobalExceptionHandler.getCommonResourcePath(request));
		request.setAttribute("wileyasBaseUrl", baseUrl);

		if (NOT_FOUND.equals(httpStatus))
		{
			throw new AuthenticationServiceException("Validation service has returned "
					+ httpStatus + " code");
		}
		else if (httpStatus != null && httpStatus.is5xxServerError())
		{
			wileyLoginService.closeSession(request);
			throw new WileyAuthenticationException("Validation service has returned "
					+ httpStatus + " code");
		}
		if (StringUtils.isNotBlank(imitateeId))
		{
			authentication = assistedServiceAgentLoginService
					.startAsmSession(almSessionToken, request, response, userId, imitateeId);
		}
		else
		{
			authentication = wileyLoginService
					.checkOrCreateUserAndAuthenticate(almSessionToken, userId);
		}

		if (authentication == null)
		{
			//need for redirection to 500 page
			wileyLoginService.closeSession(request);
			throw new WileyAuthenticationException("Unexpected Error");
		}

		return authentication;
	}

	private String captureAlmSessionToken(final HttpServletRequest request,
			final String cookieName)
	{
		final Cookie cookie = WebUtils.getCookie(request, cookieName);
		if (cookie != null)
		{
			return cookie.getValue();
		}
		return null;
	}
}
