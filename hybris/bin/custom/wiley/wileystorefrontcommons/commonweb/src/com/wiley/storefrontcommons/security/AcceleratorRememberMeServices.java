/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.security;

import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import com.wiley.storefrontcommons.web.wrappers.RemoveEncodingHttpServletRequestWrapper;


/**
 * The type Accelerator remember me services.
 */
public class AcceleratorRememberMeServices extends TokenBasedRememberMeServices
{
	private UserService userService;
	private CustomerFacade customerFacade;
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	private StoreSessionFacade storeSessionFacade;
	private CommonI18NService commonI18NService;
	private UrlEncoderService urlEncoderService;
	private SecureTokenService secureTokenService;

	public AcceleratorRememberMeServices(final String key,
			final UserDetailsService userDetailsService)
	{
		super(key, userDetailsService);
	}

	@Override
	protected void setCookie(final String[] tokens, final int maxAge, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		if (!getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			super.setCookie(tokens, maxAge, new RemoveEncodingHttpServletRequestWrapper(request, getUrlEncodingPattern(request)),
					response);
		}
	}

	@Override
	public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication)
	{
		super.logout(new RemoveEncodingHttpServletRequestWrapper(request, getUrlEncodingPattern(request)), response,
				authentication);
	}

	@Override
	protected Authentication createSuccessfulAuthentication(final HttpServletRequest request, final UserDetails user)
	{
		getUserService().setCurrentUser(getUserService().getUserForUID(user.getUsername()));

		if (StringUtils.isNotEmpty(getUrlEncoderService().getUrlEncodingPattern()))
		{
			getCustomerFacade().rememberMeLoginSuccessWithUrlEncoding(getUrlEncoderService().isLanguageEncodingEnabled(),
					getUrlEncoderService().isCurrencyEncodingEnabled());
		}
		else
		{
			getCustomerFacade().loginSuccess();
		}
		final RememberMeAuthenticationToken auth = new RememberMeAuthenticationToken(getKey(), user, user.getAuthorities());
		auth.setDetails(getAuthenticationDetailsSource().buildDetails(request));
		return auth;
	}

	@Override
	protected String retrievePassword(final Authentication authentication)
	{
		return getUserService().getUserForUID(authentication.getPrincipal().toString()).getEncodedPassword();
	}

	/**
	 * Gets url encoding pattern.
	 *
	 * @param request
	 * 		the request
	 * @return the url encoding pattern
	 */
	protected String getUrlEncodingPattern(final HttpServletRequest request)
	{
		final String encodingAttributes = (String) request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
		return StringUtils.defaultString(encodingAttributes);
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * Gets customer facade.
	 *
	 * @return the customer facade
	 */
	protected CustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	/**
	 * Sets customer facade.
	 *
	 * @param customerFacade
	 * 		the customer facade
	 */
	@Required
	public void setCustomerFacade(final CustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	/**
	 * Gets checkout customer strategy.
	 *
	 * @return the checkout customer strategy
	 */
	protected CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	/**
	 * Sets checkout customer strategy.
	 *
	 * @param checkoutCustomerStrategy
	 * 		the checkout customer strategy
	 */
	@Required
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	/**
	 * Gets url encoder service.
	 *
	 * @return the url encoder service
	 */
	protected UrlEncoderService getUrlEncoderService()
	{
		return urlEncoderService;
	}

	/**
	 * Sets url encoder service.
	 *
	 * @param urlEncoderService
	 * 		the url encoder service
	 */
	@Required
	public void setUrlEncoderService(final UrlEncoderService urlEncoderService)
	{
		this.urlEncoderService = urlEncoderService;
	}


	/**
	 * Gets store session facade.
	 *
	 * @return the store session facade
	 */
	protected StoreSessionFacade getStoreSessionFacade()
	{
		return storeSessionFacade;
	}

	/**
	 * Sets store session facade.
	 *
	 * @param storeSessionFacade
	 * 		the store session facade
	 */
	@Required
	public void setStoreSessionFacade(final StoreSessionFacade storeSessionFacade)
	{
		this.storeSessionFacade = storeSessionFacade;
	}

	/**
	 * Gets common i 18 n service.
	 *
	 * @return the common i 18 n service
	 */
	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * Sets common i 18 n service.
	 *
	 * @param commonI18NService
	 * 		the common i 18 n service
	 */
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * Gets secure token service.
	 *
	 * @return the secure token service
	 */
	protected SecureTokenService getSecureTokenService()
	{
		return secureTokenService;
	}

	/**
	 * Sets secure token service.
	 *
	 * @param secureTokenService
	 * 		the secure token service
	 */
	@Required
	public void setSecureTokenService(final SecureTokenService secureTokenService)
	{
		this.secureTokenService = secureTokenService;
	}
}
