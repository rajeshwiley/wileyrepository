/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.filters;

import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistory;
import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistoryEntry;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.CookieGenerator;


/**
 * Filter that initializes the session for the agsstorefront. This is a spring configured filter that is
 * executed by the PlatformFilterChain.
 */
public class StorefrontFilter extends OncePerRequestFilter
{
	/**
	 * The constant AJAX_REQUEST_HEADER_NAME.
	 */
	public static final String AJAX_REQUEST_HEADER_NAME = "X-Requested-With";
	/**
	 * The constant ORIGINAL_REFERER.
	 */
	public static final String ORIGINAL_REFERER = "originalReferer";

	private StoreSessionFacade storeSessionFacade;
	private BrowseHistory browseHistory;
	private CookieGenerator cookieGenerator;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		final HttpSession session = request.getSession();
		final String queryString = request.getQueryString();

		if (isSessionNotInitialized(session, queryString))
		{
			initDefaults(request);

			markSessionInitialized(session);
		}

		// For secure requests ensure that the JSESSIONID cookie is visible to insecure requests
		if (isRequestSecure(request))
		{
			fixSecureHttpJSessionIdCookie(request, response);
		}

		if (isGetMethod(request))
		{
			if (StringUtils.isBlank(request.getHeader(AJAX_REQUEST_HEADER_NAME)))
			{
				String requestURL = request.getRequestURL().toString();
				session.setAttribute(ORIGINAL_REFERER, StringUtils.isNotBlank(queryString) ? requestURL + "?"
						+ queryString : requestURL);
			}

			getBrowseHistory().addBrowseHistoryEntry(new BrowseHistoryEntry(request.getRequestURI(), null));
		}

		filterChain.doFilter(request, response);
	}

	/**
	 * Is get method boolean.
	 *
	 * @param httpRequest
	 * 		the http request
	 * @return the boolean
	 */
	protected boolean isGetMethod(final HttpServletRequest httpRequest)
	{
		return "GET".equalsIgnoreCase(httpRequest.getMethod());
	}

	/**
	 * Is request secure boolean.
	 *
	 * @param httpRequest
	 * 		the http request
	 * @return the boolean
	 */
	protected boolean isRequestSecure(final HttpServletRequest httpRequest)
	{
		return httpRequest.isSecure();
	}

	/**
	 * Is session not initialized boolean.
	 *
	 * @param session
	 * 		the session
	 * @param queryString
	 * 		the query string
	 * @return the boolean
	 */
	protected boolean isSessionNotInitialized(final HttpSession session, final String queryString)
	{
		return session.isNew() || StringUtils.contains(queryString, CMSFilter.CLEAR_CMSSITE_PARAM)
				|| !isSessionInitialized(session);
	}

	/**
	 * Sets store session facade.
	 *
	 * @param storeSessionFacade
	 * 		the store session facade
	 */
	@Required
	public void setStoreSessionFacade(final StoreSessionFacade storeSessionFacade)
	{
		this.storeSessionFacade = storeSessionFacade;
	}

	/**
	 * Sets browse history.
	 *
	 * @param browseHistory
	 * 		the browse history
	 */
	@Required
	public void setBrowseHistory(final BrowseHistory browseHistory)
	{
		this.browseHistory = browseHistory;
	}

	/**
	 * Init defaults.
	 *
	 * @param request
	 * 		the request
	 */
	protected void initDefaults(final HttpServletRequest request)
	{
		final StoreSessionFacade storeSessionFacade = getStoreSessionFacade();

		storeSessionFacade.initializeSession(Collections.list(request.getLocales()));
	}

	/**
	 * Gets store session facade.
	 *
	 * @return the store session facade
	 */
	protected StoreSessionFacade getStoreSessionFacade()
	{
		return storeSessionFacade;
	}

	/**
	 * Gets browse history.
	 *
	 * @return the browse history
	 */
	protected BrowseHistory getBrowseHistory()
	{
		return browseHistory;
	}


	/**
	 * Is session initialized boolean.
	 *
	 * @param session
	 * 		the session
	 * @return the boolean
	 */
	protected boolean isSessionInitialized(final HttpSession session)
	{
		return session.getAttribute(this.getClass().getName()) != null;
	}

	/**
	 * Mark session initialized.
	 *
	 * @param session
	 * 		the session
	 */
	protected void markSessionInitialized(final HttpSession session)
	{
		session.setAttribute(this.getClass().getName(), "initialized");
	}

	/**
	 * Fix secure http j session id cookie.
	 *
	 * @param httpServletRequest
	 * 		the http servlet request
	 * @param httpServletResponse
	 * 		the http servlet response
	 */
	protected void fixSecureHttpJSessionIdCookie(final HttpServletRequest httpServletRequest,
			final HttpServletResponse httpServletResponse)
	{
		final HttpSession session = httpServletRequest.getSession(false);
		if (session != null)
		{
			getCookieGenerator().addCookie(httpServletResponse, session.getId());
		}

	}


	/**
	 * Gets cookie generator.
	 *
	 * @return the cookie generator
	 */
	protected CookieGenerator getCookieGenerator()
	{
		return cookieGenerator;
	}

	/**
	 * Sets cookie generator.
	 *
	 * @param cookieGenerator
	 * 		the cookie generator
	 */
	@Required
	public void setCookieGenerator(final CookieGenerator cookieGenerator)
	{
		this.cookieGenerator = cookieGenerator;
	}
}
