package com.wiley.storefrontcommons.util;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.wiley.core.enums.RobotsMetaTag;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.INDEX_FOLLOW;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW;


public final class WileyRobotsTagContentMapper
{
	private WileyRobotsTagContentMapper()
	{
	}

	public static final ImmutableMap<RobotsMetaTag, String> ROBOTS_META_TAG_MAP =
			Maps.immutableEnumMap(
					ImmutableMap.of(RobotsMetaTag.INDEX_FOLLOW, INDEX_FOLLOW,
							RobotsMetaTag.INDEX_NOFOLLOW, INDEX_NOFOLLOW,
							RobotsMetaTag.NOINDEX_FOLLOW, NOINDEX_FOLLOW,
							RobotsMetaTag.NOINDEX_NOFOLLOW, NOINDEX_NOFOLLOW));

	public static String getContentForTag(final RobotsMetaTag tag)
	{
		return ROBOTS_META_TAG_MAP.get(tag);
	}
}
