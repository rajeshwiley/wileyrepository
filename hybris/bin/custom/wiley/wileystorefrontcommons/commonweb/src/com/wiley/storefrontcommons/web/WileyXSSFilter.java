package com.wiley.storefrontcommons.web;

import de.hybris.platform.servicelayer.web.XSSFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;


/**
 * Wiley XSSFilter.
 */
public class WileyXSSFilter extends XSSFilter
{

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException
	{
		this.initFromConfig(new WileyXSSFilterConfig(filterConfig));
	}
}

