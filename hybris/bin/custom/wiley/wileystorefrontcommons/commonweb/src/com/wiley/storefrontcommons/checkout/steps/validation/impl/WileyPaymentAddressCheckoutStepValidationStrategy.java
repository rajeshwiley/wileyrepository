package com.wiley.storefrontcommons.checkout.steps.validation.impl;

import com.wiley.facades.flow.WileyCheckoutFlowFacade;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.storefrontcommons.checkout.steps.validation.AbstractWileyCheckoutStepValidationStrategy;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED;
import static com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants.Views.MultiStepCheckout.CART_ZERO_ORDER_MESSAGE_PROPERTY;


public class WileyPaymentAddressCheckoutStepValidationStrategy extends AbstractWileyCheckoutStepValidationStrategy
{

	@Resource(name = "wileyCheckoutFlowFacade")
	private WileyCheckoutFlowFacade checkoutFlowFacade;
	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Override
	public ValidationResults validate(final RedirectAttributes redirectAttributes)
	{
		ValidationResults validationResults = ValidationResults.SUCCESS;

		validationResults = validatePaymentAddress(redirectAttributes, validationResults);
		if (!wileyCheckoutFacade.isNonZeroOrder())
		{
			validationResults = handleZeroOrder(redirectAttributes);
		}

		return validationResults;
	}

	protected ValidationResults validatePaymentAddress(final RedirectAttributes redirectAttributes,
			final ValidationResults validationResults)
	{
		ValidationResults result = validationResults;
		if (!checkoutFlowFacade.hasValidPaymentAddress())
		{
			result = handleInvalidPaymentAddress(redirectAttributes);
		}
		return result;
	}

	protected ValidationResults handleInvalidPaymentAddress(final RedirectAttributes redirectAttributes)
	{
		addFlashErrorMessage(redirectAttributes, MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED);
		return ValidationResults.REDIRECT_TO_PAYMENT_ADDRESS;
	}

	private ValidationResults handleZeroOrder(final RedirectAttributes redirectAttributes)
	{
		addFlashWarningMessage(redirectAttributes, CART_ZERO_ORDER_MESSAGE_PROPERTY);
		return ValidationResults.REDIRECT_TO_CART;
	}

	protected WileyCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}
}