package com.wiley.storefrontcommons.forms.validation;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.Validator;

import com.wiley.core.enums.TaxExemptionEnum;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;

import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.COMPANY;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.COUNTRY;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.DEPARTMENT;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.EMAIL;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.FIRSTNAME;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.LASTNAME;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.LINE1;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.LINE2;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.PHONE;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.POSTCODE;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.PROVINCE;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.REGION;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.TOWN;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.ZIPCODE;


/**
 * {@link WileyAddressForm} validator for wiley storefronts.
 */
@Component("wileyAddressFormValidator")
public class WileyAddressFormValidator implements SmartValidator
{
	protected static final int MAX_FIELD_LENGTH = 255;
	protected static final String FIELD_LIMIT_EXCEED = "address.field.limit.exceed";
	protected static final String UNICODE_NOT_SUPPORTED = "address.field.unicode.error";
	protected static final String PHONE_INVALID = "address.phone.invalid";
	protected static final String PHONE_PATTERN = "^([0-9]{7,15}$)";

	@Resource(name = "wileyI18NFacade")
	protected I18NFacade wileyI18NFacade;

	@Resource(name = "wileyTaxExemptionDetailsValidator")
	private Validator wileyTaxExemptionDetailsValidator;

	@Resource(name = "wileyVatRegistrationDetailsValidator")
	private Validator wileyVatRegistrationDetailsValidator;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Autowired
	private WileyCommonI18NService i18nService;

	private static final Map<WileyAddressField, Integer> MAX_LENGTHS = new HashMap<>();

	static
	{
		MAX_LENGTHS.put(FIRSTNAME, Integer.valueOf(35));
		MAX_LENGTHS.put(LASTNAME, Integer.valueOf(35));
		MAX_LENGTHS.put(LINE1, Integer.valueOf(35));
		MAX_LENGTHS.put(LINE2, Integer.valueOf(35));
		MAX_LENGTHS.put(PROVINCE, Integer.valueOf(35));
		MAX_LENGTHS.put(TOWN, Integer.valueOf(35));
		MAX_LENGTHS.put(POSTCODE, Integer.valueOf(9));
		MAX_LENGTHS.put(ZIPCODE, Integer.valueOf(9));
		MAX_LENGTHS.put(PHONE, Integer.valueOf(15));
		MAX_LENGTHS.put(COUNTRY, Integer.valueOf(MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(EMAIL, Integer.valueOf(35));
		MAX_LENGTHS.put(COMPANY, Integer.valueOf(35));
		MAX_LENGTHS.put(DEPARTMENT, Integer.valueOf(35));
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileyAddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		validate(object, errors, "");
	}

	@Override
	public void validate(final Object object, final Errors errors, final Object... objects)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("object", object);
		ServicesUtil.validateParameterNotNullStandardMessage("errors", errors);
		ServicesUtil.validateParameterNotNullStandardMessage("objects", objects);

		final WileyAddressForm addressForm = (WileyAddressForm) object;
		final String countryIso = addressForm.getCountryIso();
		final Locale locale = i18nService.getLocaleByCountryIsoCodeWithCurrentLanguage(countryIso);

		final String formPrefix = (String) objects[0];
		validateStandardFields(locale, addressForm, getMaxLengths(), formPrefix, errors);
		validateCountrySpecificFields(locale, addressForm, getMaxLengths(), formPrefix, errors);
	}

	protected void validateTaxInformation(final WileyAddressForm addressForm, final Errors errors,
			final CountryData addressCountry)
	{
		final TaxExemptionEnum applicableTaxExemption = addressCountry.getApplicableTaxExemption();
		if (applicableTaxExemption != null)
		{
			switch (applicableTaxExemption)
			{
				case VAT:
					WileyVatRegistrationDetailsForm vatRegistrationDetailsForm = addressForm.getVatRegistrationDetailsForm();
					vatRegistrationDetailsForm.setCountryIso(addressForm.getCountryIso());
					getWileyVatRegistrationDetailsValidator().validate(vatRegistrationDetailsForm, errors);
					break;
				case TAX:
					WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsForm = addressForm.getTaxExemptionDetailsForm();
					getWileyTaxExemptionDetailsValidator().validate(wileyTaxExemptionDetailsForm, errors);
					break;
				default:
			}
		}
	}

	protected void validateStandardFields(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		validateStringField(locale, addressForm.getCountryIso(), COUNTRY, formPrefix, maxLengths.get(COUNTRY), errors);
		validateStringField(locale, addressForm.getFirstName(), FIRSTNAME, formPrefix, maxLengths.get(FIRSTNAME), errors);
		validateStringField(locale, addressForm.getLastName(), LASTNAME, formPrefix, maxLengths.get(LASTNAME), errors);
		validateStringField(locale, addressForm.getLine1(), LINE1, formPrefix, maxLengths.get(LINE1), errors);
		validateFieldMaxLength(locale, addressForm.getLine2(), LINE2, formPrefix, maxLengths.get(LINE2), errors);
		validateFieldMatchesPattern(locale, addressForm.getPhone(), PHONE, formPrefix, PHONE_PATTERN, errors);
		validateEmail(locale, addressForm.getEmail(), EMAIL, formPrefix, maxLengths.get(EMAIL), errors);
		validateAdditionalInfo(locale, addressForm, maxLengths, formPrefix, errors);
	}

	protected void validateAdditionalInfo(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths,
			final String formPrefix, final Errors errors)
	{
		validateFieldMaxLength(locale, addressForm.getCompany(), COMPANY, formPrefix, maxLengths.get(COMPANY), errors);
		validateFieldMaxLength(locale, addressForm.getDepartment(), DEPARTMENT, formPrefix, maxLengths.get(DEPARTMENT), errors);
	}

	protected void validateCountrySpecificFields(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		final String countryIsoCode = addressForm.getCountryIso();
		ServicesUtil.validateParameterNotNullStandardMessage("addressForm.countryIso", countryIsoCode);
		final List<RegionData> regionsForCountryIso = wileyI18NFacade.getRegionsForCountryIso(countryIsoCode);
		if (CollectionUtils.isNotEmpty(regionsForCountryIso))
		{
			final String regionIso = addressForm.getRegionIso();
			validateFieldNotNull(regionIso, REGION, formPrefix, errors);
			if (regionIso != null)
			{
				final RegionData regionData = wileyI18NFacade.getRegion(countryIsoCode, regionIso);
				validateRegionExists(regionData, REGION, formPrefix, errors);
			}
			validateStringField(locale, addressForm.getTownCity(), TOWN, formPrefix, maxLengths.get(TOWN), errors);
			validateStringField(locale, addressForm.getPostcode(), ZIPCODE, formPrefix, maxLengths.get(ZIPCODE), errors);
		}
		else
		{
			validateStringField(locale, addressForm.getPostcode(), POSTCODE, formPrefix, maxLengths.get(POSTCODE), errors);
			validateStringField(locale, addressForm.getTownCity(), PROVINCE, formPrefix, maxLengths.get(PROVINCE), errors);
		}
		CountryData addressCountry = wileyI18NFacade.getCountryForIsocode(countryIsoCode);
		validateTaxInformation(addressForm, errors, addressCountry);
	}

	protected void validateStringField(final Locale locale, final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		validateStringField(addressField, fieldType, formPrefix, maxFieldLength, errors, String.valueOf(maxFieldLength));
	}

	/**
	 * Get label for provided field
	 *
	 * @param fieldType
	 * 		field on form
	 * @param locale
	 * 		of the field label
	 * @return label for given field
	 */
	protected String getFieldLabel(final WileyAddressField fieldType, final Locale locale)
	{
		switch (fieldType)
		{
			case FIRSTNAME:
				return getValueForKeyOrDefaultFieldLabel("address.firstName", fieldType, locale);
			case LASTNAME:
				return getValueForKeyOrDefaultFieldLabel("address.surname", fieldType, locale);
			case LINE1:
				return getValueForKeyOrDefaultFieldLabel("address.line1", fieldType, locale);
			case LINE2:
				return getValueForKeyOrDefaultFieldLabel("address.line2", fieldType, locale);
			case TOWN:
				return getValueForKeyOrDefaultFieldLabel("address.townCity", fieldType, locale);
			case PROVINCE:
				return getValueForKeyOrDefaultFieldLabel("address.state", fieldType, locale);
			case ZIPCODE:
				return getValueForKeyOrDefaultFieldLabel("address.zipcode", fieldType, locale);
			case POSTCODE:
				return getValueForKeyOrDefaultFieldLabel("address.postcode", fieldType, locale);
			case PHONE:
				return getValueForKeyOrDefaultFieldLabel("address.phone", fieldType, locale);
			case EMAIL:
				return getValueForKeyOrDefaultFieldLabel("address.email", fieldType, locale);
			default:
				return getDefaultFieldLabel(fieldType);
		}
	}

	protected String getValueForKeyOrDefaultFieldLabel(final String keyInPropertyFile,
			final WileyAddressField fieldType, final Locale locale)
	{
		String result = null;

		if (keyInPropertyFile != null)
		{
			result = getValueForKey(keyInPropertyFile, locale);
		}

		return result == null ? getDefaultFieldLabel(fieldType) : result;
	}

	protected String getDefaultFieldLabel(final WileyAddressField fieldType)
	{
		return fieldType.getFieldKey();
	}

	/**
	 * Get string value from property file
	 *
	 * @param keyInPropertyFile
	 * 		key
	 * @param locale
	 * 		for the key result localization
	 * @return String value or null
	 */
	protected String getValueForKey(final String keyInPropertyFile, final Locale locale)
	{
		return messageSource != null ? messageSource.getMessage(keyInPropertyFile, null, locale) : null;
	}

	/**
	 * Validates String field on Empty or length restrictions
	 *
	 * @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param maxFieldLength
	 * 		- Max fields length
	 * @param errors
	 * 		- errors bundle
	 * @param messageArguments
	 * 		- custom message arguments
	 */
	protected static void validateStringField(final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors,
			final String... messageArguments)
	{
		if (StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			final String errorMessageKey = fieldType.getErrorMessageKey();
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), errorMessageKey, messageArguments, errorMessageKey);
		}
	}

	/**
	 * Validates String field on empty and pattern match
	 *
	 * @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param formPrefix
	 * 		- form prefix
	 * @param pattern
	 * 		- pattern to match
	 * @param errors
	 * 		- errors bundle
	 */
	protected static void validateFieldMatchesPattern(final Locale locale, final String addressField,
			final WileyAddressField fieldType, final String formPrefix, final String pattern, final Errors errors)
	{
		validateFieldNotNull(addressField, fieldType, formPrefix, errors);
		if (addressField != null)
		{
			Pattern r = Pattern.compile(pattern);
			Matcher matcher = r.matcher(addressField);

			if (!matcher.matches())
			{
				errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
			}
		}
	}

	protected static void validateFieldNotNull(final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateRegionExists(final RegionData region, final WileyAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (region == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected void validateEmail(final Locale locale, final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		if (addressField == null || !validateEmailAddress(addressField) || StringUtils.length(addressField) > maxFieldLength)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected boolean validateEmailAddress(final String email)
	{
		return EmailValidator.getInstance().isValid(email);
	}

	/**
	 * Validates whether field max lengths is not exceeded, do no makes checks on
	 * null, empty string
	 *
	 * @param value
	 * 		- field value
	 * @param addressField
	 * 		- field metadata
	 * @param maxFieldLength
	 * 		- max field length allowed
	 * @param errors
	 * 		- errors bundle
	 */
	protected static void validateFieldMaxLength(final Locale locale, final String value, final WileyAddressField addressField,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.isNotEmpty(value) && value.length() > maxFieldLength)
		{
			final String[] messageArguments =
					{ String.valueOf(maxFieldLength) };
			errors.rejectValue(formPrefix + addressField.getFieldKey(), FIELD_LIMIT_EXCEED, messageArguments,
					FIELD_LIMIT_EXCEED);
		}
	}

	protected Map<WileyAddressField, Integer> getMaxLengths()
	{
		return MAX_LENGTHS;
	}

	public Validator getWileyTaxExemptionDetailsValidator()
	{
		return wileyTaxExemptionDetailsValidator;
	}

	public Validator getWileyVatRegistrationDetailsValidator()
	{
		return wileyVatRegistrationDetailsValidator;
	}

	public enum WileyAddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		LASTNAME("lastName", "address.lastName.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		LINE1("line1", "address.line1.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		LINE2("line2", "address.line2.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		TOWN("townCity", "address.townCity.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		PROVINCE("townCity", "address.cityProvince.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		ZIPCODE("postcode", "address.zipcode.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		POSTCODE("postcode", "address.postcode.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		REGION("regionIso", "address.state.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		COUNTRY("countryIso", "address.country.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		PHONE("phone", "address.phone.invalid", PHONE_INVALID, UNICODE_NOT_SUPPORTED),
		EMAIL("email", "address.email.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		COMPANY("company", "address.company.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED),
		DEPARTMENT("department", "address.department.invalid", FIELD_LIMIT_EXCEED, UNICODE_NOT_SUPPORTED);

		private final String fieldKey;
		private final String errorKey;
		private final String errorMessageKey;

		private final String errorUnicodeMessageKey;

		WileyAddressField(final String fieldKey, final String errorKey, final String errorMessageKey,
				final String errorUnicodeMessageKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
			this.errorMessageKey = errorMessageKey;
			this.errorUnicodeMessageKey = errorUnicodeMessageKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}

		public String getErrorMessageKey()
		{
			return errorMessageKey;
		}

		public String getErrorUnicodeMessageKey()
		{
			return errorUnicodeMessageKey;
		}

	}

}
