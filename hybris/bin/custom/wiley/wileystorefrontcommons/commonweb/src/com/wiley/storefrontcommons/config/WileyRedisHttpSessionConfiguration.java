package com.wiley.storefrontcommons.config;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.config.ConfigIntf;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;

import com.wiley.storefrontcommons.listener.WileyDefaultSessionTimeoutChangeListener;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Configuration
@EnableScheduling
public class WileyRedisHttpSessionConfiguration extends RedisHttpSessionConfiguration
{
	@Bean
	public RedisOperationsSessionRepository sessionRepository(
			@Qualifier("sessionRedisTemplate") final RedisOperations<Object, Object> sessionRedisTemplate,
			final ApplicationEventPublisher applicationEventPublisher)
	{
		final RedisOperationsSessionRepository sessionRepository =
				super.sessionRepository(sessionRedisTemplate, applicationEventPublisher);
		wileyDefaultSessionTimeoutChangeListener(sessionRepository);
		return sessionRepository;
	}

	@Bean
	public WileyDefaultSessionTimeoutChangeListener wileyDefaultSessionTimeoutChangeListener(
			final RedisOperationsSessionRepository repository)
	{
		final WileyDefaultSessionTimeoutChangeListener listener =
				new WileyDefaultSessionTimeoutChangeListener(repository);
		final ConfigIntf config = Registry.getCurrentTenantNoFallback().getConfig();
		config.registerConfigChangeListener(listener);
		return listener;
	}
}
