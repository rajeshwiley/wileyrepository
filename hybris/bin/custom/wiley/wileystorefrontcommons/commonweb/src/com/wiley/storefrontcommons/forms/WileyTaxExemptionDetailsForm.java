package com.wiley.storefrontcommons.forms;

import java.util.Date;


public class WileyTaxExemptionDetailsForm
{
	public static final String TAX_EXEMPTION_DETAILS_FORM_FIELD_PREFIX = "taxExemptionDetailsForm.";
	public static final String TAX_NUMBER = "taxNumber";
	public static final String TAX_NUMBER_FIELD_KEY = TAX_EXEMPTION_DETAILS_FORM_FIELD_PREFIX + TAX_NUMBER;
	public static final String TAX_NUMBER_EXPIRATION_DATE = "taxNumberExpirationDate";
	public static final String TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY =
			TAX_EXEMPTION_DETAILS_FORM_FIELD_PREFIX + TAX_NUMBER_EXPIRATION_DATE;
	private String taxNumber;
	private Date taxNumberExpirationDate;

	public String getTaxNumber()
	{
		return taxNumber;
	}

	public void setTaxNumber(final String taxNumber)
	{
		this.taxNumber = taxNumber;
	}

	public Date getTaxNumberExpirationDate()
	{
		return taxNumberExpirationDate;
	}

	public void setTaxNumberExpirationDate(final Date taxNumberExpirationDate)
	{
		this.taxNumberExpirationDate = taxNumberExpirationDate;
	}
}
