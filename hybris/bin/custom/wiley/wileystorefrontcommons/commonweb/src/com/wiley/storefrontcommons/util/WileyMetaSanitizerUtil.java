package com.wiley.storefrontcommons.util;

import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.catalog.model.KeywordModel;

import java.util.List;
import java.util.stream.Collectors;


public final class WileyMetaSanitizerUtil
{
	public static String sanitizeKeywords(final List<KeywordModel> keywords)
	{
		return MetaSanitizerUtil.sanitizeKeywords(keywords.stream()
				.map(KeywordModel::getKeyword)
				.collect(Collectors.toSet()));
	}

	private WileyMetaSanitizerUtil()
	{
	}
}