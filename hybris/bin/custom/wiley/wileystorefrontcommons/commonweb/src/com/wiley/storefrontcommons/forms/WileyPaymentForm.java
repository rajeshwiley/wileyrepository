package com.wiley.storefrontcommons.forms;

public class WileyPaymentForm
{
	private String paymentMethod;
	private String sessionId;
	private Boolean saveInAccount;

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(final String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	public Boolean getSaveInAccount()
	{
		return saveInAccount;
	}

	public void setSaveInAccount(final Boolean saveInAccount)
	{
		this.saveInAccount = saveInAccount;
	}
}
