package com.wiley.storefrontcommons.security.impl;

import de.hybris.platform.core.model.user.UserModel;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Nonnull;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;


/**
 * Assisted Service Auth token that deals with {@link WileyAssistedServiceAgentPrincipal} as principal.
 */
public class WileyAssistedServiceAuthenticationToken extends UsernamePasswordAuthenticationToken
{
	private boolean emulating = false;
	private final UserModel emulatedUser;

	public WileyAssistedServiceAuthenticationToken(
			@Nonnull final WileyAssistedServiceAgentPrincipal principal,
			@Nonnull final String asmAgentSessionToken, @Nonnull final UserModel emulatedUser)
	{
		super(principal, asmAgentSessionToken);
		this.emulatedUser = emulatedUser;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities()
	{
		if (!isEmulating())
		{
			return Collections.emptyList();
		}
		else
		{
			return super.getAuthorities();
		}
	}

	public boolean isEmulating()
	{
		return emulating;
	}

	/**
	 * Set whether or not as agent token is used for emulating customer.
	 *
	 * @param emulating
	 */
	public void setEmulating(final boolean emulating)
	{
		this.emulating = emulating;
	}

	@Override
	public String getName()
	{
		return ((WileyAssistedServiceAgentPrincipal) getPrincipal()).getName();
	}

	public UserModel getEmulatedUser()
	{
		return emulatedUser;
	}
}