package com.wiley.storefrontcommons.controllers.integration;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.core.healthcheck.WileyHealthCheckService;


/**
 * The type Health check controller.
 */
@RequestMapping
public class HealthCheckController extends BaseIntegrationController
{
	private WileyHealthCheckService healthCheckService;

	/**
	 * Health check string.
	 */
	@RequestMapping(value = "/health", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> healthCheck(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException
	{
		initializeSiteFromRequest(request);

		return getHealthCheckService().performInstanceHealthCheck();
	}

	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public void ping()
	{
		// Do nothing, just return the status.
	}

	@RequestMapping(value = "/version", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, Object> version(final HttpServletRequest request) throws IOException
	{
		initializeSiteFromRequest(request);

		return getHealthCheckService().getBuildDetails();
	}

	public WileyHealthCheckService getHealthCheckService()
	{
		return healthCheckService;
	}

	public void setHealthCheckService(final WileyHealthCheckService healthCheckService)
	{
		this.healthCheckService = healthCheckService;
	}
}