package com.wiley.storefrontcommons.security;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;


/**
 * Wiley assisted login service to work with alm agent
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyAssistedServiceAgentLoginService
{
	Authentication startAsmSession(
			@Nonnull String almSessionToken,
			@Nonnull HttpServletRequest request,
			@Nonnull HttpServletResponse response,
			@Nonnull String userId,
			@Nonnull String imitateeId);
}
