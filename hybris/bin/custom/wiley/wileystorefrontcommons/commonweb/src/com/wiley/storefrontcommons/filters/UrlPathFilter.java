/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.filters;

import java.io.IOException;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UrlPathHelper;


/**
 * The type Url path filter.
 */
public class UrlPathFilter extends OncePerRequestFilter
{
	private UrlPathHelper urlPathHelper;
	private Map<String, Filter> urlPathMapping;
	private Filter defaultFilter;

	/**
	 * Gets url path helper.
	 *
	 * @return the url path helper
	 */
	public UrlPathHelper getUrlPathHelper()
	{
		return urlPathHelper;
	}

	/**
	 * Sets url path helper.
	 *
	 * @param urlPathHelper
	 * 		the url path helper
	 */
	@Required
	public void setUrlPathHelper(final UrlPathHelper urlPathHelper)
	{
		this.urlPathHelper = urlPathHelper;
	}

	/**
	 * Gets url path mapping.
	 *
	 * @return the url path mapping
	 */
	public Map<String, Filter> getUrlPathMapping()
	{
		return urlPathMapping;
	}

	/**
	 * Sets url path mapping.
	 *
	 * @param urlPathMapping
	 * 		the url path mapping
	 */
	public void setUrlPathMapping(final Map<String, Filter> urlPathMapping)
	{
		this.urlPathMapping = urlPathMapping;
	}

	/**
	 * Gets default filter.
	 *
	 * @return the default filter
	 */
	public Filter getDefaultFilter()
	{
		return defaultFilter;
	}

	/**
	 * Sets default filter.
	 *
	 * @param defaultFilter
	 * 		the default filter
	 */
	@Required
	public void setDefaultFilter(final Filter defaultFilter)
	{
		this.defaultFilter = defaultFilter;
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final Map<String, Filter> mapping = getUrlPathMapping();
		if (mapping != null && !mapping.isEmpty())
		{
			final String servletPath = getUrlPathHelper().getServletPath(request);
			for (final Map.Entry<String, Filter> entry : mapping.entrySet())
			{
				if (servletPath.startsWith(entry.getKey()))
				{
					entry.getValue().doFilter(request, response, filterChain);
					return;
				}
			}
		}
		getDefaultFilter().doFilter(request, response, filterChain);
	}
}
