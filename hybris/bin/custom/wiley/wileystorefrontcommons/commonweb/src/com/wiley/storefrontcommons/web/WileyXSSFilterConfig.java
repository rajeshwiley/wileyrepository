package com.wiley.storefrontcommons.web;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.servicelayer.web.DefaultXSSFilterConfig;
import de.hybris.platform.servicelayer.web.XSSFilter;
import de.hybris.platform.servicelayer.web.XSSMatchAction;
import de.hybris.platform.spring.HybrisContextLoaderListener;
import de.hybris.platform.util.Utilities;
import de.hybris.platform.util.config.ConfigIntf;
import de.hybris.platform.util.config.ConfigIntf.ConfigChangeListener;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.FilterConfig;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Wiley XSSFilterConfig. This config class provides additional availability to handle {@code extensionName}
 * init parameter and use it if contextPath is empty or equals '/'.
 *
 * This class is a whole refactored copy of the OOTB {@link DefaultXSSFilterConfig} excluding the constructor.
 * This class hasn't been extended from the {@link DefaultXSSFilterConfig} because of inaccessible private fields.
 * Initially the class has derived from Hybris 6.2 version, but later has updated according to Hybris 6.8 changes.
 */
public class WileyXSSFilterConfig implements XSSFilter.XSSFilterConfig
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyXSSFilterConfig.class);
	public static final String ROOT_CONTEXT = "/";
	public static final String INIT_PARAMETER_EXTENSION_NAME = "extensionName";
	public static final String XSS_FILTER_ENABLED_PROPERTY = "xss.filter.enabled";
	public static final String XSS_FILTER_ACTION_PROPERTY = "xss.filter.action";
	public static final String XSS_FILTER_SORT_RULES_PROPERTY = "xss.filter.sort.rules";
	public static final String XSS_FILTER_RULE_REGEX = "xss\\.filter\\.rule\\..*(?i)";
	public static final String XSS_FILTER_HEADER_REGEX = "xss\\.filter\\.header\\..*(?i)";
	public static final String XSS_FILTER_CONFIG_REGEX = "\\.)?xss\\.filter\\..*";
	public static final String HEADER_HOST_ALLOW_REGEX = "header\\.host\\.allow\\.(.*)";
	public static final String MASTER_TENANT_ID = "master";

	private final ConfigIntf masterTenantConfiguration = Registry.getMasterTenant().getConfig();
	private final String webroot;
	private final String extensionName;
	private final String tenantIDForWebapp;
	private WileyXSSFilterConfig.FilterConfigChangeListener changeListener;

	public WileyXSSFilterConfig(final FilterConfig filterConfig)
	{
		String contextPath = filterConfig.getServletContext().getContextPath();

		String extensionName = filterConfig.getInitParameter(INIT_PARAMETER_EXTENSION_NAME);
		this.webroot = contextPath;

		tenantIDForWebapp = HybrisContextLoaderListener.getTenantIDForWebapp(filterConfig.getServletContext());

		if ((ROOT_CONTEXT.equals(contextPath) || StringUtils.isEmpty(contextPath)) && StringUtils.isNotEmpty(extensionName))
		{
			this.extensionName = extensionName;
		}
		else
		{
			this.extensionName = Utilities.getExtensionForWebroot(webroot,
					tenantIDForWebapp == null ? MASTER_TENANT_ID : tenantIDForWebapp);
		}
	}

	public void registerForConfigChanges(final XSSFilter toUpdateOnConfigChange)
	{
		unregisterConfigChangeListener();
		changeListener = new WileyXSSFilterConfig.FilterConfigChangeListener(toUpdateOnConfigChange,
				masterTenantConfiguration, "(" + extensionName + XSS_FILTER_CONFIG_REGEX);
		masterTenantConfiguration.registerConfigChangeListener(changeListener);
	}

	public void unregisterConfigChangeListener()
	{
		if (changeListener != null)
		{
			changeListener.unregister();
			changeListener = null;
		}
	}

	public boolean isEnabled()
	{
		String enabledForExtension = masterTenantConfiguration.getParameter(
				extensionName + "." + XSS_FILTER_ENABLED_PROPERTY);

		boolean enabled = StringUtils.isBlank(enabledForExtension) ?
				masterTenantConfiguration.getBoolean(XSS_FILTER_ENABLED_PROPERTY, true) :
				Boolean.parseBoolean(enabledForExtension);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("web xss filter for " + webroot + "(" + extensionName + ") enabled: " + enabled);
		}

		return enabled;
	}

	public XSSMatchAction getActionOnMatch()
	{
		String actionCode = masterTenantConfiguration.getParameter(extensionName + "." + XSS_FILTER_ACTION_PROPERTY);

		if (StringUtils.isBlank(actionCode))
		{
			actionCode = masterTenantConfiguration.getString(XSS_FILTER_ACTION_PROPERTY, XSSMatchAction.STRIP.toString());
		}

		XSSMatchAction action;

		try
		{
			action = XSSMatchAction.valueOf(actionCode);
		}
		catch (IllegalArgumentException var3)
		{
			LOG.error("Illegal match action code \'" + actionCode + "\' configured. Using " + XSSMatchAction.STRIP + " instead");
			action = XSSMatchAction.STRIP;
		}

		LOG.debug("web xss filter for " + webroot + "(" + extensionName + ") is using action:" + action);
		return action;
	}

	public Map<String, String> getPatternDefinitions()
	{
		Map<String, String> rules = new LinkedHashMap(
				this.masterTenantConfiguration.getParametersMatching(XSS_FILTER_RULE_REGEX));

		rules.putAll(this.masterTenantConfiguration
				.getParametersMatching(this.extensionName + "\\.(" + XSS_FILTER_RULE_REGEX + ")", true));

		return rules;
	}

	@Override
	public Map<String, String> getHeadersToInject()
	{
		Map<String, String> headers = new LinkedHashMap(masterTenantConfiguration.getParametersMatching(XSS_FILTER_HEADER_REGEX));

		headers.putAll(
				masterTenantConfiguration.getParametersMatching(extensionName + "\\.(" + XSS_FILTER_HEADER_REGEX + ")", true));

		return headers;
	}

	@Override
	public Map<String, String> getHostHeaderWhiteList()
	{
		ConfigIntf configuration;

		if (tenantIDForWebapp != null)
		{
			Tenant tenant = Registry.getTenantByID(tenantIDForWebapp);

			if (tenant == null)
			{
				throw new RuntimeException("Couldn't find tenant '" + tenantIDForWebapp + "'");
			}

			configuration = tenant.getConfig();
		}
		else
		{
			configuration = masterTenantConfiguration;
		}

		return configuration.getParametersMatching(HEADER_HOST_ALLOW_REGEX, true);
	}

	public boolean sortRules()
	{
		String enabledForExtension = masterTenantConfiguration.getParameter(extensionName + "." + XSS_FILTER_SORT_RULES_PROPERTY);

		boolean sorted = StringUtils.isBlank(enabledForExtension) ?
				masterTenantConfiguration.getBoolean(XSS_FILTER_SORT_RULES_PROPERTY, true) :
				Boolean.parseBoolean(enabledForExtension);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("web xss filter for " + webroot + "(" + extensionName + ") is sorting rules: " + sorted);
		}

		return sorted;
	}

	private static class FilterConfigChangeListener implements ConfigChangeListener
	{
		private final ConfigIntf config;
		private final XSSFilter filter;
		private final String keyRregExp;

		FilterConfigChangeListener(final XSSFilter filter, final ConfigIntf config, final String keyRregExp)
		{
			this.filter = filter;
			this.config = config;
			this.keyRregExp = keyRregExp;
		}

		public void configChanged(final String key, final String newValue)
		{
			if (key.matches(this.keyRregExp))
			{
				this.filter.reloadOnConfigChange();
			}
		}

		void unregister()
		{
			this.config.unregisterConfigChangeListener(this);
		}
	}
}