/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.product.WileyProductFacade;
import com.wiley.storefrontcommons.config.SiteContactConfigUtil;
import com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants;
import com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants;

import static com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages.STATIC_ERROR_MESSAGES_HOLDER;


@RequestMapping(value = "/checkout/multi/summary")
public class WileySummaryCheckoutStepController extends AbstractWileyCheckoutStepController
{
	private static final Logger LOGGER = Logger.getLogger(WileySummaryCheckoutStepController.class);

	private static final String SUMMARY = "summary";

	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Resource
	private SiteContactConfigUtil siteContactConfigUtil;

	@Resource
	private WileyProductFacade wileyProductFacade;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, // NOSONAR
			CommerceCartModificationException
	{
		wileyCheckoutFacade.calculateCart();

		if (validateCart(redirectAttributes))
		{
			return REDIRECT_URL_CART;
		}
		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();
		if (!wileyCheckoutFacade.checkSelectedPaymentMethodIsAvailable(cartData))
		{
			wileyCheckoutFacade.resetCartPaymentInfo();
			return REDIRECT_URL_PAYMENT_ADDRESS;
		}
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = wileyProductFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE));
				entry.setProduct(product);
			}
		}

		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());

		model.addAttribute(new PlaceOrderForm());

		setTaxWarningMessage(model);

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return WileystorefrontcommonsControllerConstants.Views.MultiStepCheckout.CHECKOUT_SUMMARY_PAGE;
	}


	@RequestMapping(value = "/placeOrder")
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, // NOSONAR
			InvalidCartException, CommerceCartModificationException
	{
		if (!validateBeforePlaceOrder(placeOrderForm, model))
		{
			return enterStep(model, redirectAttributes);
		}

		//Validate the cart
		if (!validateCart(redirectAttributes))
		{
			PaymentAuthorizationResultData authorizationResult = PaymentAuthorizationResultData.failure("Unexpected error");
			try
			{
				authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult("");
			}
			catch (final AdapterException ae)
			{
				// handle a case where a wrong paymentProvider configurations on the store
				// see getCommerceCheckoutService().getPaymentProvider()
				LOGGER.error(ae.getMessage(), ae);
			}
			if (!authorizationResult.isSuccess()
					&& authorizationResult.getPaymentMethod() == PaymentAuthorizationResultData.PaymentMethod.MPGS)
			{
				LOGGER.error("Failed to create authorization.  Please check the log files for more information");

				String errorDescription = getMessageSource().getMessage(authorizationResult.getErrorCode(), null,
						getI18nService().getCurrentLocale());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						WileyMPGSConstants.MESSAGE_KEY_ISSUE_COMMON_MESSAGE,
						new Object[] { siteContactConfigUtil.getContactMail(), siteContactConfigUtil.getContactPhone(),
								errorDescription });
				return getCheckoutStep().currentStep();
			}

			if (!authorizationResult.isSuccess()
					&& authorizationResult.getPaymentMethod() == PaymentAuthorizationResultData.PaymentMethod.PAYPAL)
			{
				authorizationResult.getErrors()
						.stream()
						.forEach(e -> GlobalMessages.addFlashMessage(redirectAttributes, STATIC_ERROR_MESSAGES_HOLDER, e));
				String redirectUrl = getHopPaymentRepeatRedirectUrl();
				if (redirectUrl == null)
				{
					return getCheckoutStep().currentStep();
				}
				return REDIRECT_PREFIX + redirectUrl;
			}

			return placeOrder(redirectAttributes, getCheckoutStep().currentStep());
		}
		return REDIRECT_PREFIX + "/cart";
	}


	private String placeOrder(final RedirectAttributes redirectAttributes, final String errorUrl)
	{
		final OrderData orderData;
		try
		{
			orderData = wileyCheckoutFacade.placeOrder();
			return redirectToOrderConfirmationPage(orderData);
		}
		catch (final Exception e)
		{
			LOGGER.error("Failed to place Order", e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					WileystorefrontcommonsConstants.MESSAGE_KEY_PLACE_ORDER_FAILED);

			return errorUrl;
		}
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 * 		The spring form of the order being submitted
	 * @param model
	 * 		A spring Model
	 * @return False if the order form is invalid and true if everything is valid.
	 */
	private boolean validateBeforePlaceOrder(final PlaceOrderForm placeOrderForm, final Model model)
	{
		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			return false;
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			return false;
		}
		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();

		if (!wileyCheckoutFacade.containsTaxValues())
		{
			LOGGER.warn(String.format("Cart %s does not have any tax values, which means the tax cacluation "
					+ "was not properly done, placement of order can't continue", cartData.getCode()));
			//TODO Remove tax calculation check. Tax check will be implemented in separate story
			//GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			//return false;
		}

		if (!cartData.isCalculated())
		{
			LOGGER.error(String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue",
					cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			return false;
		}

		return true;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}


	public Collection<CountryData> getCountries()
	{
		return null;
	}

	public Map<String, CountryData> getCountryDataMap()
	{
		return null;
	}

	@RequestMapping(value = "/placeZeroOrder", method = RequestMethod.POST)
	public String placeZeroOrder(final HttpServletRequest request, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		if (!validateCart(redirectAttributes))
		{
			return placeOrder(redirectAttributes, REDIRECT_PREFIX + "/cart");
		}
		return REDIRECT_PREFIX + "/cart";
	}
}
