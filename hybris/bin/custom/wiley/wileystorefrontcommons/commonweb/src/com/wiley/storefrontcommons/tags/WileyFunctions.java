package com.wiley.storefrontcommons.tags;

import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;


/**
 * JSP EL Functions. This file contains static methods that are used by JSP EL.
 */
public final class WileyFunctions
{
	private WileyFunctions()
	{

	}

	public static ImageData getProductImageForTypeAndFormat(final Object product, final String type, final String format)
	{
		if (product != null && format != null && type != null)
		{
			final Collection<ImageData> images = getImages(product);
			if (CollectionUtils.isNotEmpty(images))
			{
				return getImageForTypeAndFormat(images, type, format);
			}
		}
		return null;
	}

	private static Collection<ImageData> getImages(final Object product)
	{
		if (product instanceof ProductData)
		{
			return ((ProductData) product).getImages();
		}
		else if (product instanceof VariantOptionData)
		{
			return ((VariantOptionData) product).getImages();
		}
		return null;
	}

	private static ImageData getImageForTypeAndFormat(final Collection<ImageData> images,
			final String type, final String format)
	{
		return images.stream()
				.filter(imageData -> type.equals(imageData.getImageType().name()) && format.equals(imageData.getFormat()))
				.findFirst().orElse(null);

	}

	/**
	 * Test if a abstractOrder has an applied promotion for the specified entry number.
	 *
	 * @param abstractOrder
	 * 		the abstractOrder
	 * @param entryNumber
	 * 		the entry number
	 * @return true if there is an applied promotion for the entry number
	 */
	public static boolean doesAppliedPromotionExistForAbstractOrderEntry(final AbstractOrderData abstractOrder,
			final int entryNumber)
	{
		return abstractOrder != null && Functions.doesPromotionExistForOrderEntry(abstractOrder.getAppliedProductPromotions(),
				entryNumber);
	}

	/**
	 * Test if a abstractOrder has an potential promotion for the specified entry number.
	 *
	 * @param abstractOrder
	 * 		the abstractOrder
	 * @param entryNumber
	 * 		the entry number
	 * @return true if there is an potential promotion for the entry number
	 */
	public static boolean doesPotentialPromotionExistForAbstractOrderEntry(final AbstractOrderData abstractOrder,
			final int entryNumber)
	{
		return abstractOrder instanceof CartData && Functions.doesPromotionExistForOrderEntry(
				((CartData) abstractOrder).getPotentialProductPromotions(), entryNumber);
	}
}