package com.wiley.storefrontcommons.checkout.steps.validation;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public abstract class AbstractWileyCheckoutStepValidationStrategy implements WileyCheckoutStepValidationStrategy
{
	protected void addFlashErrorMessage(final RedirectAttributes redirectAttributes, final String messageKey)
	{
		if (redirectAttributes != null)
		{
			addFlashMessageToGlobalMessages(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, messageKey);
		}
	}

	protected void addFlashWarningMessage(final RedirectAttributes redirectAttributes, final String messageKey)
	{
		addFlashMessageToGlobalMessages(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, messageKey);
	}

	// wrap static method invocation to make is possible to verify in test that method called with expected parameters
	protected void addFlashMessageToGlobalMessages(final RedirectAttributes redirectAttributes, final String messageHolder,
			final String messageKey)
	{
		GlobalMessages.addFlashMessage(redirectAttributes, messageHolder, messageKey);
	}
}
