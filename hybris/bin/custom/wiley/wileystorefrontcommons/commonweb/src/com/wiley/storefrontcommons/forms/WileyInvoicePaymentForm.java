package com.wiley.storefrontcommons.forms;

public class WileyInvoicePaymentForm
{
	private String purchaseOrderNumber;
	private String userNotes;

	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getUserNotes()
	{
		return userNotes;
	}

	public void setUserNotes(final String userNotes)
	{
		this.userNotes = userNotes;
	}
}
