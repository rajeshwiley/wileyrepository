package com.wiley.storefrontcommons.forms;

/**
 * Form for edit order page
 */
public class WileyOrderForm
{
	private WileyAddressForm wileyAddressForm;

	private WileyPaymentForm wileyPaymentForm;

	private WileyInvoicePaymentForm wileyInvoicePaymentForm;

	private WileyInvoicePaymentForm wileyProformaPaymentForm;

	public WileyAddressForm getWileyAddressForm()
	{
		return wileyAddressForm;
	}

	public void setWileyAddressForm(final WileyAddressForm wileyAddressForm)
	{
		this.wileyAddressForm = wileyAddressForm;
	}

	public WileyPaymentForm getWileyPaymentForm()
	{
		return wileyPaymentForm;
	}

	public void setWileyPaymentForm(final WileyPaymentForm wileyPaymentForm)
	{
		this.wileyPaymentForm = wileyPaymentForm;
	}

	public WileyInvoicePaymentForm getWileyInvoicePaymentForm()
	{
		return wileyInvoicePaymentForm;
	}

	public void setWileyInvoicePaymentForm(final WileyInvoicePaymentForm wileyInvoicePaymentForm)
	{
		this.wileyInvoicePaymentForm = wileyInvoicePaymentForm;
	}

	public WileyInvoicePaymentForm getWileyProformaPaymentForm()
	{
		return wileyProformaPaymentForm;
	}

	public void setWileyProformaPaymentForm(final WileyInvoicePaymentForm wileyProformaPaymentForm)
	{
		this.wileyProformaPaymentForm = wileyProformaPaymentForm;
	}
}
