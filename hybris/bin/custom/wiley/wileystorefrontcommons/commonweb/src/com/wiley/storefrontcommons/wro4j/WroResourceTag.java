package com.wiley.storefrontcommons.wro4j;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;


public class WroResourceTag extends TagSupport
{

	private static final long serialVersionUID = 1L;

	//Generated file by maven wro4j plugin
	private static final String WRO4J_MAPPING_FILE = "/WEB-INF/wromapping.properties";

	private static Properties wroMapping;

	private String resource;

	private String path;

	@Override
	public int doStartTag() throws JspException
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getPath());
		if (!getPath().endsWith("/"))
		{
			sb.append('/');
		}
		try
		{
			sb.append(getWroMapping().getProperty(getResource(), getResource()));
			pageContext.getOut().write(sb.toString());
		}
		catch (IOException io)
		{
			throw new JspException(io);
		}
		return super.doStartTag();
	}

	private Properties getWroMapping() throws IOException
	{
		if (wroMapping == null)
		{
			wroMapping = new Properties();
			InputStream inputStream = pageContext.getServletContext().getResourceAsStream(WRO4J_MAPPING_FILE);
			if (inputStream == null)
			{
				throw new FileNotFoundException("property file '" + WRO4J_MAPPING_FILE + "' not found in the classpath");
			}
			wroMapping.load(inputStream);
		}
		return wroMapping;
	}

	public String getResource()
	{
		return resource;
	}

	public void setResource(final String resource)
	{
		this.resource = resource;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(final String path)
	{
		this.path = path;
	}

}