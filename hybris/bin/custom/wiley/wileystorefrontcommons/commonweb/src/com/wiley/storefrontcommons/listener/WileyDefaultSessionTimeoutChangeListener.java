package com.wiley.storefrontcommons.listener;

import de.hybris.platform.util.config.ConfigIntf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.DEFAULT_SESSION_TIMEOUT;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyDefaultSessionTimeoutChangeListener implements ConfigIntf.ConfigChangeListener
{
	private static final Logger LOGGER =
			LoggerFactory.getLogger(WileyDefaultSessionTimeoutChangeListener.class);

	private RedisOperationsSessionRepository repository;

	public WileyDefaultSessionTimeoutChangeListener(final RedisOperationsSessionRepository repository)
	{
		this.repository = repository;
	}

	@Override
	public void configChanged(final String key, final String value)
	{
		if (DEFAULT_SESSION_TIMEOUT.equals(key))
		{
			try
			{
				repository.setDefaultMaxInactiveInterval(Integer.parseInt(value));
			}
			catch (NumberFormatException ex)
			{
				if (LOGGER.isWarnEnabled())
				{
					LOGGER.warn("Wrong format for property \"" + DEFAULT_SESSION_TIMEOUT + "\" - \""
							+ value + "\" can't be converted to int");
				}
			}
		}
	}
}
