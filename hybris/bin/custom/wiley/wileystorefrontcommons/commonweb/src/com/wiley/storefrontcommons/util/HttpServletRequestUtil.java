package com.wiley.storefrontcommons.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


public class HttpServletRequestUtil
{
	public Map<String, String> getRequestParameterMap(final HttpServletRequest request)
	{
		final Map<String, String> map = new HashMap<>();
		Collections.list(request.getParameterNames()).forEach(
				paramName -> map.put(paramName, request.getParameter(paramName)));
		return map;
	}

}
