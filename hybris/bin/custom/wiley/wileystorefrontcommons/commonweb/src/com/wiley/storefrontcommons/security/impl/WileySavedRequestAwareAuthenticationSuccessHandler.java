package com.wiley.storefrontcommons.security.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;


/**
  * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileySavedRequestAwareAuthenticationSuccessHandler
		extends SavedRequestAwareAuthenticationSuccessHandler
{
	@Override
	protected String determineTargetUrl(final HttpServletRequest request,
			final HttpServletResponse response)
	{
		return request.getRequestURL().toString();
	}
}
