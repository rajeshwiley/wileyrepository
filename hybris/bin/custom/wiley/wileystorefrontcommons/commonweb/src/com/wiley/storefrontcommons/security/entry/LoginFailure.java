package com.wiley.storefrontcommons.security.entry;

import java.io.Serializable;
import java.util.Date;


public class LoginFailure implements Serializable
{
	private static final long serialVersionUID = -726901420446463622L;

	private Integer counter;
	private Date date;

	public LoginFailure()
	{
		this.counter = Integer.valueOf(0);
		this.date = new Date();
	}

	public LoginFailure(final Integer counter, final Date date)
	{
		this.counter = counter;
		this.date = date;
	}

	public Integer getCounter()
	{
		return counter;
	}

	public void setCounter(final Integer counter)
	{
		this.counter = counter;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

}
