package com.wiley.storefrontcommons.util;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.i18n.WileyCommonI18NService;

import static com.wiley.storefrontcommons.web.i18n.StoreSessionLocaleResolver.LOCALE_REQUEST_ATTRIBUTE_NAME;


/**
 * Class which manages locale per needed request.
 */
public class RequestLocaleManager
{
	@Autowired
	private WileyCommonI18NService i18nService;

	/**
	 * Set locale per one request only.
	 *
	 * @param request
	 * 		which is being used for locale settings
	 * @param countryIso
	 * 		code for the locale
	 */
	public void setLocale(final HttpServletRequest request, final String countryIso)
	{
		if (StringUtils.isNotEmpty(countryIso))
		{
			Locale locale = i18nService.getLocaleByCountryIsoCodeWithCurrentLanguage(countryIso);
			request.setAttribute(LOCALE_REQUEST_ATTRIBUTE_NAME, locale);
		}
	}
}
