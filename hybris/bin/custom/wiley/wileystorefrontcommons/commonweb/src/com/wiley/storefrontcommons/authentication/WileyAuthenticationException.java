package com.wiley.storefrontcommons.authentication;

/**
 * Specific implementation of RuntimeException
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyAuthenticationException extends RuntimeException
{

	public WileyAuthenticationException(final String message)
	{
		super(message);
	}

	public WileyAuthenticationException(final String message, final Throwable e)
	{
		super(message, e);
	}
}
