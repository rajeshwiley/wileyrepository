package com.wiley.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.exceptions.WileyVatTaxNumberNotValidException;
import com.wiley.facades.as.address.WileyasAddressVerificationFacade;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.user.WileyUserFacade;
import com.wiley.storefrontcommons.beans.propertyeditors.WileyFallbackToTodayDateEditor;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyOrderForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;
import com.wiley.storefrontcommons.util.RequestLocaleManager;
import com.wiley.storefrontcommons.util.WileyAddressDataUtil;

import static com.wiley.storefrontcommons.controllers.
		WileystorefrontcommonsControllerConstants.Views.MultiStepCheckout.CART_CURRENCY_UPDATE_MESSAGE_PROPERTY;


@RequestMapping(value = "/checkout/multi/payment-address")
public class WileyPaymentAddressCheckoutStepController extends AbstractWileyCheckoutStepController
{
	private static final String PAYMENT_ADDRESS_STEP = "payment-address";

	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "wileyAddressFormValidator")
	private SmartValidator wileyAddressFormValidator;

	@Resource(name = "wileyUserFacade")
	private WileyUserFacade wileyUserFacade;

	@Resource(name = "wileyI18NFacade")
	private I18NFacade wileyI18NFacade;

	@Resource(name = "wileyAddressDataUtil")
	private WileyAddressDataUtil wileyAddressDataUtil;

	@Resource
	private WileyFallbackToTodayDateEditor wileyFallbackToTodayDateEditor;

	@Resource(name = "wileyasAddressVerificationFacade")
	private WileyasAddressVerificationFacade addressVerificationFacade;

	@Resource
	private RequestLocaleManager requestLocaleManager;

	@Resource(name = "wileyasOrderFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyOrderForm> wileyasOrderFormConverter;

	@InitBinder
	protected void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder)
	{
		binder.registerCustomEditor(Date.class, wileyFallbackToTodayDateEditor);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_ADDRESS_STEP)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{

		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();

		setupAddPaymentAddressPage(model);

		final WileyOrderForm wileyOrderForm = wileyasOrderFormConverter.convert(cartData);
		final WileyAddressForm wileyPaymentAddressForm = wileyOrderForm.getWileyAddressForm();

		model.addAttribute("addressForm", wileyPaymentAddressForm);
		model.addAttribute("cartData", cartData);
		model.addAttribute("country", wileyPaymentAddressForm.getCountryIso());
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(wileyPaymentAddressForm.getCountryIso()));
		model.addAttribute("addressFormCountry", cartData.getCountry());

		return com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants
				.Views.MultiStepCheckout.ADD_PAYMENT_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(@ModelAttribute("addressForm") final WileyAddressForm addressForm,
			final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		wileyAddressFormValidator.validate(addressForm, bindingResult);
		String countryIso = addressForm.getCountryIso();
		if (StringUtils.isNotBlank(countryIso))
		{
			CountryData addressFormCountry = getI18NFacade().getCountryForIsocode(countryIso);
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIso));
			String regionIso = addressForm.getRegionIso();
			if (StringUtils.isNotBlank(regionIso))
			{
				model.addAttribute("addressFormRegion", getI18NFacade().getRegion(countryIso, regionIso));
			}
			model.addAttribute("country", countryIso);
			model.addAttribute("addressFormCountry", addressFormCountry);
			// validate tax details via facade only if there are no other validator errors
			validateTaxDetails(addressForm, addressFormCountry, bindingResult);
		}

		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();

		if (bindingResult.hasErrors())
		{
			addressForm.setShowSuggestionAddresses(false);
			addressForm.setSuggestionSelected(false);
			model.addAttribute("cartData", cartData);
			requestLocaleManager.setLocale(request, countryIso);
			return handleAddressForm(model);
		}

		final AddressData newAddress = wileyAddressDataUtil.convert(addressForm);

		// we should show address doctor window until address was not selected
		if (!addressForm.isSuggestionSelected() && addressVerificationFacade
				.shouldCallSuggestionDoctor(cartData.getPaymentMode()))
		{
			final AddressVerificationResult<AddressVerificationDecision> suggestions = addressVerificationFacade
					.verifyAddressData(newAddress);

			if (!suggestions.isSkipAddressDoctor())
			{
				model.addAttribute("suggestions", suggestions);
				addressForm.setShowSuggestionAddresses(true);

				model.addAttribute("cartData", cartData);
				return handleAddressForm(model);
			}
		}

		wileyCheckoutFacade.resetCartPaymentInfo();
		wileyCheckoutFacade.setPaymentAddressData(newAddress);

		final String newCountry = newAddress.getCountry().getIsocode();
		final CountryData country = getCartFacade().getSessionCart().getCountry();
		if (!newCountry.equals(country.getIsocode()))
		{
			wileyCheckoutFacade.setCartCountry(newCountry);
			setDefaultCurrencyInCartForCurrentPaymentAddress(redirectAttributes);
		}
		if (Boolean.TRUE.equals(addressForm.getSaveInAddressBook()))
		{
			wileyUserFacade.addAddress(newAddress);
		}
		saveTaxDetailsToSessionCart(addressForm, newAddress.getCountry());

		if (validateCart(redirectAttributes)) {
			return REDIRECT_URL_CART;
		}
		wileyCheckoutFacade.calculateCart();

		return getCheckoutStep(PAYMENT_ADDRESS_STEP).nextStep();
	}

	private String handleAddressForm(final Model model) throws CMSItemNotFoundException
	{
		setupAddPaymentAddressPage(model);
		return com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants
				.Views.MultiStepCheckout.ADD_PAYMENT_ADDRESS_PAGE;
	}

	/**
	 * This method gets called when the "Use this Address" button is clicked. It
	 * sets the selected delivery address on the checkout facade - if it has
	 * changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 * 		- the id of the delivery address.
	 * @return - a URL to the page to load.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectPaymentAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = wileyCheckoutFacade.getAddressDatabyId(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				final AddressData cartCheckoutPaymentAddress = wileyCheckoutFacade.getCheckoutCart().getPaymentAddress();
				if (isAddressIdChanged(cartCheckoutPaymentAddress, selectedAddressData))
				{
					clearTaxExemptions();
					wileyCheckoutFacade.resetCartPaymentInfo();
					wileyCheckoutFacade.setPaymentAddressData(selectedAddressData);
					wileyCheckoutFacade.setCartCountry(selectedAddressData.getCountry().getIsocode());
					setDefaultCurrencyInCartForCurrentPaymentAddress(redirectAttributes);
				}
				wileyUserFacade.setDefaultAddress(selectedAddressData);
			}
		}
		wileyCheckoutFacade.calculateCart();
		return getCheckoutStep(PAYMENT_ADDRESS_STEP).nextStep();
	}


	protected void setupAddPaymentAddressPage(final Model model) throws CMSItemNotFoundException
	{
		prepareDataForPage(model);

		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", getCheckoutFlowFacade().hasNoPaymentInfo());
		model.addAttribute("paymentAddresses", wileyUserFacade.getPaymentAddressesForCurrentUser());
		model.addAttribute("permitChangePaymentCountry",
				wileyCheckoutFacade.getCheckoutCart().getOrderConfiguration().isPermitChangePaymentCountry());
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.billingAddress.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep(PAYMENT_ADDRESS_STEP));
	}

	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep(PAYMENT_ADDRESS_STEP).previousStep();
	}

	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep(PAYMENT_ADDRESS_STEP).nextStep();
	}

	@Override
	public I18NFacade getI18NFacade()
	{
		return wileyI18NFacade;
	}

	protected void saveTaxDetailsToSessionCart(final WileyAddressForm addressForm, final CountryData country)
	{

		if (country.getApplicableTaxExemption() != null)
		{
			switch (country.getApplicableTaxExemption())
			{
				case TAX:
					wileyCheckoutFacade.setTaxExemptionDetails(
							addressForm.getTaxExemptionDetailsForm().getTaxNumber(),
							addressForm.getTaxExemptionDetailsForm().getTaxNumberExpirationDate()
					);
					break;
				case VAT:
					wileyCheckoutFacade.setVatRegistrationDetails(addressForm.getVatRegistrationDetailsForm().getTaxNumber());
					break;
				default:
					clearTaxExemptions();
			}
		}
		else
		{
			clearTaxExemptions();
		}
	}

	private void clearTaxExemptions()
	{
		wileyCheckoutFacade.setTaxExemptionDetails(null, null);
		wileyCheckoutFacade.setVatRegistrationDetails(null);
	}

	protected void validateTaxDetails(final WileyAddressForm addressForm, final CountryData country,
			final BindingResult bindingResult)
	{
		if (!bindingResult.hasFieldErrors() && country.getApplicableTaxExemption() != null)
		{
			try
			{
				String taxNumber = null;
				switch (country.getApplicableTaxExemption())
				{
					case VAT:
						taxNumber = addressForm.getVatRegistrationDetailsForm().getTaxNumber();
						break;
					default:
				}
				wileyCheckoutFacade.validateTaxInformation(addressForm.getCountryIso(), taxNumber);
			}
			catch (WileyVatTaxNumberNotValidException e)
			{
				bindingResult.rejectValue(WileyVatRegistrationDetailsForm.TAX_NUMBER_FIELD_KEY,
						WileyVatRegistrationDetailsForm.TAX_NUMBER_ERROR_KEY);
			}
		}
	}

	private void setDefaultCurrencyInCartForCurrentPaymentAddress(final RedirectAttributes redirectAttributes)
	{
		if (wileyCheckoutFacade.setDefaultCurrencyInCartForCurrentPaymentAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					CART_CURRENCY_UPDATE_MESSAGE_PROPERTY);
		}
	}

	public void setWileyAddressFormValidator(final SmartValidator wileyAddressFormValidator)
	{
		this.wileyAddressFormValidator = wileyAddressFormValidator;
	}
}
