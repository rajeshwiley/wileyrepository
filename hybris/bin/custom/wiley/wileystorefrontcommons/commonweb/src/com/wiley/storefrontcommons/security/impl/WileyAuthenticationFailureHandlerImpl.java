package com.wiley.storefrontcommons.security.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.wiley.storefrontcommons.security.WileyLoginService;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.ALM_REDIRECT_PAGE;


/**
 * Spring security authentication failure handler
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyAuthenticationFailureHandlerImpl extends SimpleUrlAuthenticationFailureHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyAuthenticationFailureHandlerImpl.class);

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private WileyLoginService wileyLoginService;

	private boolean forwardToDestination;

	@Override
	public void onAuthenticationFailure(final HttpServletRequest request,
			final HttpServletResponse response, final AuthenticationException exception)
			throws IOException, ServletException
	{
		wileyLoginService.closeSession(request);
		final String defaultFailureUrl = getDefaultFailureUrl();
		if (getDefaultFailureUrl() == null)
		{
			LOG.debug("No failure URL set, sending 401 Unauthorized error");

			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Authentication Failed: " + exception.getMessage());
		}
		else
		{
			saveException(request, exception);

			if (isForwardToDestination())
			{
				LOG.debug("Forwarding to " + defaultFailureUrl);

				request.getRequestDispatcher(defaultFailureUrl)
						.forward(request, response);
			}
			else
			{
				LOG.debug("Redirecting to " + defaultFailureUrl);
				getRedirectStrategy().sendRedirect(request, response, defaultFailureUrl);
			}
		}
	}

	private String getDefaultFailureUrl()
	{
		return configurationService.getConfiguration().getString(ALM_REDIRECT_PAGE);
	}

	public boolean isForwardToDestination()
	{
		return forwardToDestination;
	}

	public void setForwardToDestination(final boolean forwardToDestination)
	{
		this.forwardToDestination = forwardToDestination;
	}
}
