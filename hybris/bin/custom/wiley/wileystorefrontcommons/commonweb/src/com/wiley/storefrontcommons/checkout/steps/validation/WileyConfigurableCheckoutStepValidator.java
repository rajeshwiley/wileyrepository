package com.wiley.storefrontcommons.checkout.steps.validation;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public class WileyConfigurableCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOGGER = Logger.getLogger(WileyConfigurableCheckoutStepValidator.class);

	private List<WileyCheckoutStepValidationStrategy> validateOnEnterStrategies;
	private List<WileyCheckoutStepValidationStrategy> validateOnExitStrategies;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		return getValidationResult(redirectAttributes, validateOnEnterStrategies);
	}

	@Override
	public ValidationResults validateOnExit()
	{
		return getValidationResult(null, validateOnEnterStrategies);
	}

	private ValidationResults getValidationResult(final RedirectAttributes redirectAttributes,
			final List<WileyCheckoutStepValidationStrategy> strategies)
	{
		for (WileyCheckoutStepValidationStrategy validationStrategy : strategies)
		{
			ValidationResults validationResults = validationStrategy.validate(redirectAttributes);
			if (ValidationResults.SUCCESS != validationResults)
			{
				return validationResults;
			}
		}
		return ValidationResults.SUCCESS;
	}

	public void setValidateOnEnterStrategies(
			final List<WileyCheckoutStepValidationStrategy> validateOnEnterStrategies)
	{
		this.validateOnEnterStrategies = validateOnEnterStrategies;
	}

	public void setValidateOnExitStrategies(
			final List<WileyCheckoutStepValidationStrategy> validateOnExitStrategies)
	{
		this.validateOnExitStrategies = validateOnExitStrategies;
	}
}
