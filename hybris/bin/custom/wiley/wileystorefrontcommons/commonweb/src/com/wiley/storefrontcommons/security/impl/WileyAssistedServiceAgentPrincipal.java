package com.wiley.storefrontcommons.security.impl;

import java.io.Serializable;


/**
 * OOTB_CODE Represents Assisted Service Agent principal.
 */
public class WileyAssistedServiceAgentPrincipal implements Serializable
{
	private final String name;

	public String getName()
	{
		return name;
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public WileyAssistedServiceAgentPrincipal(final String name)
	{
		this.name = name;
	}
}