package com.wiley.storefrontcommons.web;

import de.hybris.platform.jalo.media.MediaManager;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CloudFrontMediaUrlFilter extends OncePerRequestFilter
{
	private String preferredUrlStrategyId;
	private String cloudFrontHeaderValue;
	private String cloudFrontHeaderName;

	private SessionService sessionService;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain)
			throws ServletException, IOException
	{
		if (getCloudFrontHeaderValue().equals(request.getHeader(getCloudFrontHeaderName())))
		{
			getSessionService().setAttribute(MediaManager.PREFERRED_URL_STRATEGY_ID, getPreferredUrlStrategyId());
		}
		else
		{
			getSessionService().removeAttribute(MediaManager.PREFERRED_URL_STRATEGY_ID);
		}

		filterChain.doFilter(request, response);
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	private String getPreferredUrlStrategyId()
	{
		return preferredUrlStrategyId;
	}

	@Required
	public void setPreferredUrlStrategyId(final String preferredUrlStrategyId)
	{
		this.preferredUrlStrategyId = preferredUrlStrategyId;
	}

	public String getCloudFrontHeaderValue()
	{
		return cloudFrontHeaderValue;
	}

	@Required
	public void setCloudFrontHeaderValue(final String cloudFrontHeaderValue)
	{
		this.cloudFrontHeaderValue = cloudFrontHeaderValue;
	}

	public String getCloudFrontHeaderName()
	{
		return cloudFrontHeaderName;
	}

	@Required
	public void setCloudFrontHeaderName(final String cloudFrontHeaderName)
	{
		this.cloudFrontHeaderName = cloudFrontHeaderName;
	}
}