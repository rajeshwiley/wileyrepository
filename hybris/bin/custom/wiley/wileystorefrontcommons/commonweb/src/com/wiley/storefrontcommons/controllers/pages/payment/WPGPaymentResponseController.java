package com.wiley.storefrontcommons.controllers.pages.payment;


import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_AUTH;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_KEY;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_VALIDATE;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;

import javax.naming.OperationNotSupportedException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.storefrontcommons.util.HttpServletRequestUtil;

@RequestMapping(value = "/payment/hop")
public class WPGPaymentResponseController extends AbstractPageController
{
	private static final String AUTH_PAYMENT_RESPONSE_HANDLER = "/checkout/multi/hop/response";
	private String validatePaymentResponseHandler;
	private HttpServletRequestUtil httpServletRequestUtil = new HttpServletRequestUtil();


	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleHopResponse(final HttpServletRequest request) throws OperationNotSupportedException
	{
		final String operation = request.getParameter(OPERATION_KEY);
		String operationHandler;
		if (OPERATION_AUTH.equals(operation))
		{
			operationHandler = FORWARD_PREFIX + AUTH_PAYMENT_RESPONSE_HANDLER;
		}
		else if (OPERATION_VALIDATE.equals(operation))
		{
			operationHandler = FORWARD_PREFIX + validatePaymentResponseHandler;
		}
		else
		{
			throw new OperationNotSupportedException();
		}
		return operationHandler;
	}

	public void setHttpServletRequestUtil(final HttpServletRequestUtil httpServletRequestUtil)
	{
		this.httpServletRequestUtil = httpServletRequestUtil;
	}

	public String getValidatePaymentResponseHandler()
	{
		return validatePaymentResponseHandler;
	}

	@Required
	public void setValidatePaymentResponseHandler(final String validatePaymentResponseHandler)
	{
		this.validatePaymentResponseHandler = validatePaymentResponseHandler;
	}
}
