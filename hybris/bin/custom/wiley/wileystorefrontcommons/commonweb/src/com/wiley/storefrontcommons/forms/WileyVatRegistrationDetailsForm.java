package com.wiley.storefrontcommons.forms;

public class WileyVatRegistrationDetailsForm
{
	public static final String TAX_NUMBER_FIELD_KEY = "vatRegistrationDetailsForm.taxNumber";
	public static final String TAX_NUMBER_ERROR_KEY =
			"checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.invalid";

	private String countryIso;
	private String taxNumber;

	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public String getTaxNumber()
	{
		return taxNumber;
	}

	public void setTaxNumber(final String taxNumber)
	{
		this.taxNumber = taxNumber;
	}

}
