package com.wiley.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class VoucherForm implements Serializable
{
	@NotNull(message = "{text.voucher.apply.invalid.error}")
	@Size(min = 1, max = 255, message = "{text.voucher.apply.invalid.error}")
	private String voucherCode;

	public String getVoucherCode()
	{
		return voucherCode;
	}

	public void setVoucherCode(final String voucherCode)
	{
		this.voucherCode = XSSFilterUtil.filter(voucherCode);
	}
}
