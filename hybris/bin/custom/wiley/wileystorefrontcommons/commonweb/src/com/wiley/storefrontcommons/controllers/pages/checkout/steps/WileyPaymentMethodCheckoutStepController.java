package com.wiley.storefrontcommons.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.enums.MPGSSupportedCardType;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.payment.data.WileyMPGSPaymentResult;
import com.wiley.facades.payment.mpgs.WileyMPGSPaymentFacade;
import com.wiley.storefrontcommons.config.SiteContactConfigUtil;
import com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants;
import com.wiley.storefrontcommons.forms.WileyInvoicePaymentForm;
import com.wiley.storefrontcommons.forms.WileyPaymentForm;

import static com.wiley.core.enums.PaymentModeEnum.CARD;
import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.MONTH_AMOUNT;
import static
		com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants.Payment.PAY_PAL_FLOW_TYPE_CHECKOUT;
import static com.wiley.storefrontcommons.controllers.WileystorefrontcommonsControllerConstants.Payment.PAY_PAL_FLOW_TYPE_KEY;


@RequestMapping(value = "/checkout/multi/payment-method")
public class WileyPaymentMethodCheckoutStepController extends AbstractWileyCheckoutStepController
{
	private static final String PAYMENT_METHOD = "payment-method";
	public static final String CART_DATA_ATTR = "cartData";
	private static final String PAYMENT_INFOS = "paymentInfos";
	private static final String PAYPAL_CHECKOUT_URL = "/paypal/checkout/hop/expressCheckoutShortcut";

	@Resource(name = "wileyMpgsPaymentFacade")
	private WileyMPGSPaymentFacade wileyMPGSPaymentFacade;

	@Resource
	private SiteContactConfigUtil siteContactConfigUtil;

	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	@ModelAttribute("paymentCardMonths")
	public List<SelectOption> getPaymentCardMonths()
	{
		final List<SelectOption> months = new ArrayList<>();
		for (Integer monthNumber = 1; monthNumber <= MONTH_AMOUNT; monthNumber++)
		{
			months.add(new SelectOption(monthNumber.toString(),
					getMessageSource().getMessage("payment.month." + monthNumber, null, getI18nService().getCurrentLocale())));
		}
		return months;
	}

	@ModelAttribute("cardExpiryYears")
	public List<SelectOption> getCardExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		int currentYear = Year.now().getValue();
		final Integer yearAmount = Integer.valueOf(getSiteConfigService().getProperty("payment.mpgs.calendar.yearAmount"));

		for (int year = currentYear; year <= (currentYear + yearAmount); year++)
		{
			expiryYears.add(new SelectOption(String.valueOf(year), String.valueOf(year)));
		}
		return expiryYears;
	}

	@ModelAttribute("supportedCardTypes")
	public Set<String> getSupportedCardTypes()
	{
		Set<String> supportedCardTypes = new HashSet<String>();
		for (final MPGSSupportedCardType cardType : MPGSSupportedCardType.values())
		{
			supportedCardTypes.add(cardType.getStringValue());
		}
		return supportedCardTypes;
	}

	@ModelAttribute("hostedSessionErrorMessages")
	public String getHostedSessionErrorMessages()
	{
		Map<String, String> messageMap = new HashMap<>();
		messageMap.put("globalAlertCorrectMessages",
				getMessageSource().getMessage("form.global.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("processingError", getMessageSource()
				.getMessage("payment.hostedSesstion.processing.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeInvalid",
				getMessageSource().getMessage("payment.securityCode.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeNotProvided",
				getMessageSource().getMessage("payment.securityCode.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("expirationDateInvalid",
				getMessageSource().getMessage("payment.expirationDate.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNumberInvalid",
				getMessageSource().getMessage("payment.paymentCardNumber.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNotSupported", getMessageSource().getMessage("payment.paymentCard.notSupported", null,
				getI18nService().getCurrentLocale()));
		Gson gson = new Gson();
		return gson.toJson(messageMap);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();
		model.addAttribute(CART_DATA_ATTR, cartData);

		model.addAttribute("wileyPaymentForm", new WileyPaymentForm());
		model.addAttribute("wileyInvoicePaymentForm",
				createWileyInvoicePaymentForm(cartData, PaymentModeEnum.INVOICE.getCode()));
		model.addAttribute("wileyProformaPaymentForm",
				createWileyInvoicePaymentForm(cartData, PaymentModeEnum.PROFORMA.getCode()));

		wileyCheckoutFacade.setDeliveryModeIfAvailable();

		setupAddPaymentPage(model);
		setTaxWarningMessage(model);

		model.addAttribute(PAYMENT_INFOS, getUserFacade().getCCPaymentInfos(true));
		return WileystorefrontcommonsControllerConstants.Views.MultiStepCheckout.ADD_PAYMENT_METHOD_PAGE;
	}

	@RequestMapping(value = { "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid final WileyPaymentForm wileyPaymentForm,
			final BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		wileyCheckoutFacade.clearInvoicePaymentData();
		String sessionId = wileyPaymentForm.getSessionId();

		final WileyMPGSPaymentResult mpgsPaymentResult = wileyMPGSPaymentFacade
				.retrieveSessionAndTokenize(sessionId, wileyPaymentForm.getSaveInAccount());

		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();
		if (!mpgsPaymentResult.getIsOperationSuccessful())
		{
			setupAddPaymentPage(model);
			String messageKey = mpgsPaymentResult.getMessageKey();
			model.addAttribute("cartData", cartData);
			return handleMPGSErrors(redirectAttributes, messageKey, MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL, model);
		}
		wileyCheckoutFacade.setPaymentMode(CARD);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		setTaxWarningMessage(model);

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = { "/add/invoice" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String addInvoice(final Model model, @Valid final WileyInvoicePaymentForm wileyInvoicePaymentForm)
	{
		wileyCheckoutFacade.setInvoicePaymentDetails(wileyInvoicePaymentForm.getUserNotes(),
				wileyInvoicePaymentForm.getPurchaseOrderNumber(), PaymentModeEnum.INVOICE.getCode());

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = { "/add/proforma" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String addProforma(final Model model, @Valid final WileyInvoicePaymentForm wileyProformaPaymentForm)
	{
		wileyCheckoutFacade.setInvoicePaymentDetails(wileyProformaPaymentForm.getUserNotes(),
				wileyProformaPaymentForm.getPurchaseOrderNumber(), PaymentModeEnum.PROFORMA.getCode());

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = { "/add/paypal" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String addPaypal()
	{
		wileyCheckoutFacade.clearInvoicePaymentData();
		getSessionService().setAttribute(PAY_PAL_FLOW_TYPE_KEY, PAY_PAL_FLOW_TYPE_CHECKOUT);
		return FORWARD_PREFIX + PAYPAL_CHECKOUT_URL;
	}

	@RequestMapping(value = "/choose", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId)
	{
		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			wileyCheckoutFacade.setPaymentDetails(selectedPaymentMethodId);
			wileyCheckoutFacade.setPaymentMode(CARD);
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", getCheckoutFlowFacade().hasNoPaymentInfo());
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	private String handleMPGSErrors(final RedirectAttributes ra, final String messageKey, final String pageLabel,
			final Model model) throws CMSItemNotFoundException
	{
		prepareErrorMessage(ra, messageKey);

		return getCheckoutStep().currentStep();
	}

	protected void prepareErrorMessage(final RedirectAttributes ra, final String messageKey)
	{
		GlobalMessages.addFlashMessage(ra, GlobalMessages.ERROR_MESSAGES_HOLDER,
				WileyMPGSConstants.MESSAGE_KEY_ISSUE_COMMON_MESSAGE,
				new Object[] { siteContactConfigUtil.getContactMail(), siteContactConfigUtil.getContactPhone(), messageKey });
	}

	private WileyInvoicePaymentForm createWileyInvoicePaymentForm(final CartData cartData, final String paymentMode)
	{
		WileyInvoicePaymentForm wileyInvoicePaymentForm = new WileyInvoicePaymentForm();
		if (cartData.getPaymentMode() != null && cartData.getPaymentMode().equals(paymentMode))
		{
			wileyInvoicePaymentForm.setPurchaseOrderNumber(cartData.getPurchaseOrderNumber());
			wileyInvoicePaymentForm.setUserNotes(cartData.getUserNotes());
		}
		return wileyInvoicePaymentForm;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	public Collection<CountryData> getCountries()
	{
		return null;
	}

	public Map<String, CountryData> getCountryDataMap()
	{
		return null;
	}

}
