package com.wiley.storefrontcommons.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.ui.Model;

import com.wiley.facades.wiley.session.WileyFailedModificationsFacade;


/**
 * @author Dzmitryi_Halahayeu
 */
public class AbstractWileyCartPageController extends AbstractCartPageController
{
	@Resource
	private WileyFailedModificationsFacade wileyFailedModificationsFacade;

	protected void showEntryRemovedGlobalMessage(final Model model)
	{
		List<CartModificationData> cartModificationDataList = wileyFailedModificationsFacade.popAll();
		cartModificationDataList.stream().forEach(modification ->
				GlobalMessages.addMessage(model, GlobalMessages.INFO_MESSAGES_HOLDER,
						modification.getStatusMessage(), modification.getMessageParameters()));
	}


	protected String getVoucherAppliedMessage(final String voucherCode)
	{
		return getMessageSource().getMessage("text.voucher.apply.success", new Object[] { voucherCode },
				getI18nService().getCurrentLocale());
	}


}
