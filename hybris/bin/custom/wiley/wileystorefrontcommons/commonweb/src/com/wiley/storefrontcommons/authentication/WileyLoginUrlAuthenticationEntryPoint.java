package com.wiley.storefrontcommons.authentication;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.wiley.storefrontcommons.security.strategy.WileyRedirectStrategy;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.ALM_REDIRECT_PAGE;


public class WileyLoginUrlAuthenticationEntryPoint implements AuthenticationEntryPoint
{
	@Autowired
	private WileyRedirectStrategy redirectStrategy;

	@Value("${" + ALM_REDIRECT_PAGE + "}")
	private String redirectUrl;

	@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException e) throws IOException
	{
		redirectStrategy.sendRedirect(request, response, redirectUrl);
	}
}
