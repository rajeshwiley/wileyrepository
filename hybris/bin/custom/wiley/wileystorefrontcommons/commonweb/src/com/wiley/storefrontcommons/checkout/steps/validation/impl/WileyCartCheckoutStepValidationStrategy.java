package com.wiley.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.storefrontcommons.checkout.steps.validation.AbstractWileyCheckoutStepValidationStrategy;


public class WileyCartCheckoutStepValidationStrategy extends AbstractWileyCheckoutStepValidationStrategy
{
	private static final Logger LOGGER = Logger.getLogger(WileyCartCheckoutStepValidationStrategy.class);

	@Resource(name = "wileyCheckoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Override
	public ValidationResults validate(final RedirectAttributes redirectAttributes)
	{
		ValidationResults validationResults = ValidationResults.SUCCESS;

		if (!checkoutFlowFacade.hasValidCart())
		{
			LOGGER.info("Missing, empty or unsupported cart");
			validationResults = ValidationResults.REDIRECT_TO_CART;
		}
		return validationResults;
	}
}
