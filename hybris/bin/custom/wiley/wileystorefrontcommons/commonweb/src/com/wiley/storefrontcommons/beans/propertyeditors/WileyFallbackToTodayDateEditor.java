package com.wiley.storefrontcommons.beans.propertyeditors;

import java.beans.PropertyEditorSupport;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.wiley.storefrontcommons.util.WileyAddressDataUtil;


@Component
public class WileyFallbackToTodayDateEditor extends PropertyEditorSupport
{
	@Resource(name = "wileyAddressDataUtil")
	private WileyAddressDataUtil wileyAddressDataUtil;

	@Override
	public void setAsText(final String text) throws IllegalArgumentException
	{
		Date taxNumberExpirationDate = wileyAddressDataUtil.deserializeTaxNumberExpirationDate(text);
		this.setValue(taxNumberExpirationDate);
	}

	@Override
	public String getAsText()
	{
		Date value = (Date) this.getValue();
		return getWileyAddressDataUtil().serializeTaxNumberExpirationDate(value);
	}

	protected WileyAddressDataUtil getWileyAddressDataUtil()
	{
		return wileyAddressDataUtil;
	}
}
