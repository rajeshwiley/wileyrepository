package com.wiley.storefrontcommons.web;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * This filter adds slash trailing slash to the URL if it is missed
 */
public class WileyTrailingSlashRedirectionFilter extends OncePerRequestFilter
{

	private Pattern exclusionPattern;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final Matcher matcher = exclusionPattern.matcher(request.getRequestURI());
		if (matcher.find() && !request.getParameterNames().hasMoreElements())
		{
			String redirectUrl = request.getRequestURL() + "/";
			response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
			response.setHeader("Location", redirectUrl);
		} else {
			filterChain.doFilter(request, response);
		}

	}

	@Value("${wiley.trailing.slash.exclude.regexp}")
	public void setExclusionRegexp(final String regexp) {
		exclusionPattern = Pattern.compile(regexp);
	}
}
