package com.wiley.storefrontcommons.security;

import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;


/**
 * Wiley login service to work with alm user
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyLoginService
{
	Authentication checkOrCreateUserAndAuthenticate(@Nonnull String almSessionToken,
			@Nonnull String userId);

	void closeSession(@Nonnull HttpServletRequest request);

	CustomerModel createCustomer(@Nonnull String userId);
}
