package com.wiley.storefrontcommons.controllers.util;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.FlashMapManager;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.support.SessionFlashMapManager;


public final class WileyGlobalMessages
{
	public static final String STATIC_CONF_MESSAGES_HOLDER = "staticConfMsgs";
	public static final String STATIC_INFO_MESSAGES_HOLDER = "staticInfoMsgs";
	public static final String STATIC_ERROR_MESSAGES_HOLDER = "staticErrorMsgs";
	public static final String ERROR_MESSAGES_HOLDER = "accErrorMsgs";
	public static final String SUCCESS_MESSAGES_HOLDER = "accSuccessMsgs";

	private WileyGlobalMessages()
	{
	}

	public static void addGlobalMessagesAsFlashAttribute(final Model model, final RedirectAttributes redirectAttributes)
	{
		if (model.containsAttribute(GlobalMessages.INFO_MESSAGES_HOLDER))
		{
			redirectAttributes.addFlashAttribute(GlobalMessages.INFO_MESSAGES_HOLDER,
					model.asMap().get(GlobalMessages.INFO_MESSAGES_HOLDER));
		}

		if (model.containsAttribute(GlobalMessages.ERROR_MESSAGES_HOLDER))
		{
			redirectAttributes.addFlashAttribute(GlobalMessages.ERROR_MESSAGES_HOLDER,
					model.asMap().get(GlobalMessages.ERROR_MESSAGES_HOLDER));
		}

		if (model.containsAttribute(GlobalMessages.CONF_MESSAGES_HOLDER))
		{
			redirectAttributes.addFlashAttribute(GlobalMessages.CONF_MESSAGES_HOLDER,
					model.asMap().get(GlobalMessages.CONF_MESSAGES_HOLDER));
		}
	}
	
	public static void addSessionErrorMessage(final HttpServletRequest request, final HttpServletResponse response,
			final String messageKey)
	{
		addSessionMessage(request, response, messageKey, ERROR_MESSAGES_HOLDER);
	}

	public static void addSessionSuccessMessage(final HttpServletRequest request, final HttpServletResponse response,
			final String messageKey)
	{
		addSessionMessage(request, response, messageKey, SUCCESS_MESSAGES_HOLDER);
	}

	public static void addSessionMessage(final HttpServletRequest request, final HttpServletResponse response,
			final String messageKey, final String messagesHolder)
	{
		final FlashMap outputFlash = RequestContextUtils.getOutputFlashMap(request);
		FlashMap flashMap = outputFlash == null ? new FlashMap() : outputFlash;
		addMessage(messageKey, flashMap, messagesHolder);
		final FlashMapManager outputFlashMapManager = RequestContextUtils.getFlashMapManager(request);
		FlashMapManager flashMapManager =
				outputFlashMapManager == null ? new SessionFlashMapManager() : outputFlashMapManager;
		flashMapManager.saveOutputFlashMap(flashMap, request, response);
	}

	private static void addMessage(final String messageKey, final FlashMap flashMap, final String messagesHolder)
	{
		final GlobalMessage message = new GlobalMessage();
		message.setCode(messageKey);
		List<GlobalMessage> messages = null;
		if (flashMap.containsKey(messagesHolder))
		{
			messages = new ArrayList<>((List<GlobalMessage>) flashMap.get(messagesHolder));
			messages.add(message);

		}
		else
		{
			messages = Collections.singletonList(message);
		}

		flashMap.put(messagesHolder, messages);
	}
}
