package com.wiley.storefrontcommons.forms.databind.module;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.databind.WileyTaxExemptionDetailsFormDeserializer;
import com.wiley.storefrontcommons.forms.databind.WileyTaxExemptionDetailsFormSerializer;


public class WileyModule extends SimpleModule implements InitializingBean
{
	private static final String NAME = "WileyModule";

	@Autowired
	private WileyTaxExemptionDetailsFormSerializer wileyTaxExemptionDetailsFormSerializer;

	@Autowired
	private WileyTaxExemptionDetailsFormDeserializer wileyTaxExemptionDetailsFormDeserializer;

	public WileyModule()
	{
		super(NAME);
	}

	@Override
	public void afterPropertiesSet() throws Exception
	{
		addSerializer(WileyTaxExemptionDetailsForm.class, wileyTaxExemptionDetailsFormSerializer);
		addDeserializer(WileyTaxExemptionDetailsForm.class, wileyTaxExemptionDetailsFormDeserializer);
	}
}
