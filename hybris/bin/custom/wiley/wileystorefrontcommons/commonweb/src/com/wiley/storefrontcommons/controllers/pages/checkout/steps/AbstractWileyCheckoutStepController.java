package com.wiley.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

import com.paypal.hybris.constants.PaypalConstants;
import com.wiley.facades.order.WileyCheckoutFacade;


public abstract class AbstractWileyCheckoutStepController extends AbstractCheckoutStepController
{
	protected static final String TAX_WARNING_MESSAGE = "tax.not.calculated.warning.message";
	protected static final String REDIRECT_URL_PAYMENT_ADDRESS = REDIRECT_PREFIX + "/checkout/multi/payment-address/add";

	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	protected String getHopPaymentReturnUrl()
	{
		final String hopPaymentReturnUrl = wileyCheckoutFacade.getSavedHopPaymentReturnUrlAndRemove();
		if (hopPaymentReturnUrl != null)
		{
			return hopPaymentReturnUrl;
		}
		return "/cart";
	}

	protected String getHopPaymentRepeatRedirectUrl()
	{
		final String repeatRedirectUrl = getSessionService().getAttribute(PaypalConstants.PAY_PAL_REPEAT_REDIRECT_URL);
		if (StringUtils.isNotBlank(repeatRedirectUrl))
		{
			getSessionService().removeAttribute(PaypalConstants.PAY_PAL_REPEAT_REDIRECT_URL);
			return repeatRedirectUrl;
		}
		return null;
	}

	protected void setTaxWarningMessage(final Model model)
	{
		if (!wileyCheckoutFacade.isTaxAvailable() && !wileyCheckoutFacade.isZeroTotalOrder())
		{
			GlobalMessages.addInfoMessage(model, TAX_WARNING_MESSAGE);
		}
	}
}
