package com.wiley.storefrontcommons.checkout.steps.validation;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public interface WileyCheckoutStepValidationStrategy
{
    ValidationResults validate(RedirectAttributes redirectAttributes);
}

