/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.controllers;

/**
 * The interface Wileystorefrontcommons controller constants.
 */
public interface WileystorefrontcommonsControllerConstants
{
	interface Views
	{

		interface MultiStepCheckout // NOSONAR
		{
			String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
			String ADD_PAYMENT_ADDRESS_PAGE = "pages/checkout/multi/addPaymentAddressPage";
			String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPageNoPayPal";

			String CART_CURRENCY_UPDATE_MESSAGE_PROPERTY = "basket.page.message.cart.currency.update";
			String CART_ZERO_ORDER_MESSAGE_PROPERTY = "basket.page.message.zero.order";
		}
	}

	interface Payment
	{
		String PAY_PAL_FLOW_TYPE_KEY = "payPalFlowType";
		String PAY_PAL_FLOW_TYPE_CART = "cart";
		String PAY_PAL_FLOW_TYPE_CHECKOUT = "checkout";
	}
}