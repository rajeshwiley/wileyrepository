package com.wiley.storefrontcommons.errorHandlers;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class WileyGlobalExceptionHandler
{
	protected static final String COMMON = "common";
	private static final Logger LOG = LoggerFactory.getLogger(WileyGlobalExceptionHandler.class);

	private static final String ERROR_PAGE_PREFIX = "pages/error";
	private static final String ERROR_PAGE = ERROR_PAGE_PREFIX + "/serverError";
	private static final String METHOD_NOT_SUPPORTED_ERROR_PAGE = ERROR_PAGE_PREFIX + "/methodNotSupportedError";
	private static final String HOME_PAGE_URL = "homePageUrl";
	private static final String COMMON_RESOURCE_PATH = "commonResourcePath";

	@Value("${wiley.homepage.url}")
	private String homepageUrl;

	@ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(code = HttpStatus.METHOD_NOT_ALLOWED, reason = "Method not allowed", value = HttpStatus.METHOD_NOT_ALLOWED)
	public ModelAndView methodNotSupportedException(final HttpServletRequest request, final HttpServletResponse response,
			final Exception exc)
	{
		LOG.warn(exc.getMessage(), exc);

		final ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(METHOD_NOT_SUPPORTED_ERROR_PAGE);

		request.setAttribute(HOME_PAGE_URL, homepageUrl);
		request.setAttribute(COMMON_RESOURCE_PATH, getCommonResourcePath(request));
		request.setAttribute("currentYear", LocalDateTime.now().getYear());

		return modelAndView;
	}

	@ExceptionHandler(value = Throwable.class)
	public ModelAndView defaultErrorHandler(final HttpServletRequest request, final HttpServletResponse response,
			final Exception exc) throws Exception
	{
		//if any exception annotated with own response status - re-throw it as is
		if (AnnotationUtils.findAnnotation(exc.getClass(), ResponseStatus.class) != null)
		{
			throw exc;
		}

		LOG.error("Unhandled exception happened. Http 500 page will be displayed", exc);
		return getErrorModelAndView(request);
	}

	private ModelAndView getErrorModelAndView(final HttpServletRequest request)
	{
		final ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(ERROR_PAGE);
		modelAndView.addObject(HOME_PAGE_URL, homepageUrl);
		modelAndView.addObject(COMMON_RESOURCE_PATH, getCommonResourcePath(request));
		return modelAndView;
	}

	public String getCommonResourcePath(final HttpServletRequest request)
	{
		final Object urlEncodingAttributes = request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
		final String contextPath = StringUtils.remove(request.getContextPath(),
				(urlEncodingAttributes != null) ? urlEncodingAttributes.toString() : "");
		final String siteRootUrl = contextPath + "/_ui/responsive";
		return siteRootUrl + "/common";
	}
}
