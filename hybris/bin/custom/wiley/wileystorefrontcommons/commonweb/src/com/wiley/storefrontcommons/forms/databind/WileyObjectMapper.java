package com.wiley.storefrontcommons.forms.databind;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.storefrontcommons.forms.databind.module.WileyModule;


public class WileyObjectMapper extends ObjectMapper implements InitializingBean
{
	@Autowired
	private WileyModule wileyModule;

	@Override
	public void afterPropertiesSet() throws Exception
	{
		// reflect logic from MappingJackson2HttpMessageConverter default constructor
		Jackson2ObjectMapperBuilder.json().configure(this);
		registerModule(wileyModule);
	}
}
