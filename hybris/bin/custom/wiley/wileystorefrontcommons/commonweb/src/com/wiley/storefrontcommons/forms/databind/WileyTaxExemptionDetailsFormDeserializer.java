package com.wiley.storefrontcommons.forms.databind;

import java.io.IOException;
import java.util.Date;

import javax.annotation.Resource;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.util.WileyAddressDataUtil;



public class WileyTaxExemptionDetailsFormDeserializer extends JsonDeserializer<WileyTaxExemptionDetailsForm>
{

	@Resource(name = "wileyAddressDataUtil")
	private WileyAddressDataUtil wileyAddressDataUtil;

	@Override
	public WileyTaxExemptionDetailsForm deserialize(final JsonParser jsonParser,
			final DeserializationContext deserializationContext)
			throws IOException
	{
		WileyTaxExemptionDetailsForm result = new WileyTaxExemptionDetailsForm();

		JsonNode node = jsonParser.getCodec().readTree(jsonParser);

		String taxNumber = node.get(WileyTaxExemptionDetailsForm.TAX_NUMBER).asText();
		result.setTaxNumber(taxNumber);

		String dateText = node.get(WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE).asText();
		Date taxNumberExpirationDate = wileyAddressDataUtil.deserializeTaxNumberExpirationDate(dateText);
		result.setTaxNumberExpirationDate(taxNumberExpirationDate);

		return result;
	}
}
