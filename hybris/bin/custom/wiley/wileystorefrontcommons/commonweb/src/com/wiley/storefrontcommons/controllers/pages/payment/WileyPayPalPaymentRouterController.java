package com.wiley.storefrontcommons.controllers.pages.payment;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.wiley.facades.order.WileyCheckoutFacade;

import static com.wiley.storefrontcommons.controllers.
		WileystorefrontcommonsControllerConstants.Payment.PAY_PAL_FLOW_TYPE_CHECKOUT;
import static com.wiley.storefrontcommons.controllers.
		WileystorefrontcommonsControllerConstants.Payment.PAY_PAL_FLOW_TYPE_KEY;
import static com.wiley.storefrontcommons.controllers.
		WileystorefrontcommonsControllerConstants.Views.MultiStepCheckout.CART_CURRENCY_UPDATE_MESSAGE_PROPERTY;


@Controller
@RequestMapping(value = "/checkout/paypal")
public class WileyPayPalPaymentRouterController extends AbstractController
{
	private static final String CHECKOUT_SUMMARY_STEP = "/checkout/multi/summary/view";
	private static final String REDIRECT_URL_ADD_PAYMENT_ADDRESS = REDIRECT_PREFIX + "/checkout/multi/payment-address/add";
	private static final String PAYPAL_PAYMENT_SUCCESS_RETURN_URL = "paypal.payment.success.return";

	@Resource
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Resource
	private SessionService sessionService;

	@Resource
	private SiteConfigService siteConfigService;

	/**
	 * This method is used for updating some cart details and routing to proper page
	 */
	@RequestMapping(value = "/handle-success-response", method = RequestMethod.GET)
	@RequireHardLogIn
	public String handleSuccessResponse(final RedirectAttributes redirectAttributes)
	{
		final CartData cartData = wileyCheckoutFacade.getCheckoutCart();
		if (!wileyCheckoutFacade.checkSelectedPaymentMethodIsAvailable(cartData))
		{
			wileyCheckoutFacade.resetCartPaymentInfo();
			return REDIRECT_URL_ADD_PAYMENT_ADDRESS;
		}
		AddressData paymentAddress = cartData.getPaymentAddress();
		if (paymentAddress != null && paymentAddress.getCountry() != null)
		{
			wileyCheckoutFacade.setCartCountry(paymentAddress.getCountry().getIsocode());
			setDefaultCurrencyInCartForCurrentPaymentAddress(redirectAttributes);
		}
		return REDIRECT_PREFIX + getNextStepUrl();
	}

	/**
	 * This method is used for routing to proper page
	 */
	@RequestMapping(value = "/handle-error-response", method = RequestMethod.GET)
	@RequireHardLogIn
	public String handleErrorResponse(final HttpServletRequest request, final RedirectAttributes redirectAttributes)
	{
		Optional<? extends Map<String, ?>> flashScope = Optional.of(RequestContextUtils.getInputFlashMap(request));
		flashScope.ifPresent(s -> s.forEach(redirectAttributes::addFlashAttribute));

		wileyCheckoutFacade.resetCartPaymentInfo();
		return REDIRECT_URL_ADD_PAYMENT_ADDRESS;
	}

	private String getNextStepUrl()
	{
		String payPalFlowType = sessionService.getAttribute(PAY_PAL_FLOW_TYPE_KEY);
		sessionService.removeAttribute(PAY_PAL_FLOW_TYPE_KEY);
		if (PAY_PAL_FLOW_TYPE_CHECKOUT.equals(payPalFlowType))
		{
			return siteConfigService.getProperty(PAYPAL_PAYMENT_SUCCESS_RETURN_URL);
		}
		return CHECKOUT_SUMMARY_STEP;
	}

	private void setDefaultCurrencyInCartForCurrentPaymentAddress(final RedirectAttributes redirectAttributes)
	{
		if (wileyCheckoutFacade.setDefaultCurrencyInCartForCurrentPaymentAddress())
		{
			wileyCheckoutFacade.calculateCart();
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					CART_CURRENCY_UPDATE_MESSAGE_PROPERTY);
		}
	}
}
