package com.wiley.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class WileyAddressForm extends AddressForm
{
	private String email;

	private WileyTaxExemptionDetailsForm taxExemptionDetailsForm;

	private WileyVatRegistrationDetailsForm vatRegistrationDetailsForm;
	
	private String company;
	private String department;
	private boolean showSuggestionAddresses;
	private boolean suggestionSelected;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public WileyTaxExemptionDetailsForm getTaxExemptionDetailsForm()
	{
		return taxExemptionDetailsForm;
	}

	public void setTaxExemptionDetailsForm(final WileyTaxExemptionDetailsForm taxExemptionDetailsForm)
	{
		this.taxExemptionDetailsForm = taxExemptionDetailsForm;
	}

	public WileyVatRegistrationDetailsForm getVatRegistrationDetailsForm()
	{
		return vatRegistrationDetailsForm;

	}

	public void setVatRegistrationDetailsForm(final WileyVatRegistrationDetailsForm vatRegistrationDetailsForm)
	{
		this.vatRegistrationDetailsForm = vatRegistrationDetailsForm;
	}
	
	public String getCompany()
	{
		return company;
	}
	
	public void setCompany(final String company)
	{
		this.company = company;
	}
	
	public String getDepartment()
	{
		return department;
	}
	
	public void setDepartment(final String department)
	{
		this.department = department;
	}

	public boolean isShowSuggestionAddresses()
	{
		return this.showSuggestionAddresses;
	}

	public void setShowSuggestionAddresses(final boolean showSuggestionAddresses)
	{
		this.showSuggestionAddresses = showSuggestionAddresses;
	}

	public boolean isSuggestionSelected()
	{
		return suggestionSelected;
	}

	public void setSuggestionSelected(final boolean suggestionSelected)
	{
		this.suggestionSelected = suggestionSelected;
	}
}
