package com.wiley.storefrontcommons.security.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.wiley.storefrontcommons.security.WileyAssistedServiceAgentAuthoritiesManager;


/**
 * OOTB_CODE DefaultAssistedServiceAgentAuthoritiesManager
 */
public class WileyAssistedServiceAgentAuthoritiesManagerImpl implements WileyAssistedServiceAgentAuthoritiesManager
{
	private UserDetailsService userDetailsService;

	private AssistedServiceService assistedServiceService;

	@Override
	public void addCustomerAuthoritiesToAgent(final String customerId)
	{
		final Set<GrantedAuthority> agentAuthorities = addAuthoritiesToSession();
		final Collection<? extends GrantedAuthority> customerAuthorities = getUserDetailsService().loadUserByUsername(customerId)
				.getAuthorities();
		if (CollectionUtils.isNotEmpty(customerAuthorities) && agentAuthorities.addAll(customerAuthorities))
		{
			updateAuthentication();
		}
	}

	@Override
	public void restoreInitialAuthorities()
	{
		updateAuthentication();
	}

	/**
	 * Add current agent authorities to the ASM session.
	 *
	 * @return The current agent authorities.
	 */
	protected Set<GrantedAuthority> addAuthoritiesToSession()
	{
		final SecurityContext context = SecurityContextHolder.getContext();
		final Collection<? extends GrantedAuthority> authorities = context.getAuthentication().getAuthorities();
		getAssistedServiceService().getAsmSession().setInitialAgentAuthorities(authorities);
		return new HashSet<>(authorities);
	}

	/**
	 * Update the agent authentication token with new authorities.
	 */
	protected void updateAuthentication()
	{
		final SecurityContext context = SecurityContextHolder.getContext();
		final WileyAssistedServiceAuthenticationToken currentAuth =
				(WileyAssistedServiceAuthenticationToken) context.getAuthentication();
		final WileyAssistedServiceAgentPrincipal principal = (WileyAssistedServiceAgentPrincipal) currentAuth.getPrincipal();
		final WileyAssistedServiceAuthenticationToken updatedAuth = new WileyAssistedServiceAuthenticationToken(principal,
				(String) currentAuth.getCredentials(), currentAuth.getEmulatedUser());
		updatedAuth.setDetails(currentAuth.getDetails());
		updatedAuth.setEmulating(currentAuth.isEmulating());
		context.setAuthentication(updatedAuth);
	}

	protected UserDetailsService getUserDetailsService()
	{
		return userDetailsService;
	}

	@Required
	public void setUserDetailsService(final UserDetailsService userDetailsService)
	{
		this.userDetailsService = userDetailsService;
	}

	protected AssistedServiceService getAssistedServiceService()
	{
		return assistedServiceService;
	}

	@Required
	public void setAssistedServiceService(final AssistedServiceService assistedServiceService)
	{
		this.assistedServiceService = assistedServiceService;
	}

}
