package com.wiley.storefrontcommons.forms.validation;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;


/**
 * {@link WileyVatRegistrationDetailsForm} validator for wiley storefronts.
 */
@Component("wileyVatRegistrationDetailsValidator")
public class WileyVatRegistrationDetailsValidator implements Validator
{
	protected static final Logger LOG = LoggerFactory.getLogger(WileyVatRegistrationDetailsValidator.class);

	protected static final int TAX_NUMBER_MAX_FIELD_LENGTH = 255;
	protected static final String FIELD_LIMIT_EXCEED = "address.field.limit.exceed";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileyVatRegistrationDetailsForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsForm = (WileyVatRegistrationDetailsForm) object;
		validateTaxNumber(errors, wileyVatRegistrationDetailsForm);
	}

	protected void validateTaxNumber(final Errors errors, final WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsForm)
	{
		final String taxNumber = wileyVatRegistrationDetailsForm.getTaxNumber();
		if (StringUtils.isNotEmpty(taxNumber))
		{
			if (StringUtils.length(taxNumber) > TAX_NUMBER_MAX_FIELD_LENGTH)
			{
				errors.rejectValue(WileyVatRegistrationDetailsForm.TAX_NUMBER_FIELD_KEY, FIELD_LIMIT_EXCEED,
						new Object[] { String.valueOf(TAX_NUMBER_MAX_FIELD_LENGTH) }, FIELD_LIMIT_EXCEED);
			}
		}
	}
}
