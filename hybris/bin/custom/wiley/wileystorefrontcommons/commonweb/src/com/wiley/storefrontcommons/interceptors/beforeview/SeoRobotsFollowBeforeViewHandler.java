/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package com.wiley.storefrontcommons.interceptors.beforeview;

import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.storefrontcommons.util.WileyRobotsTagContentMapper;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.INDEX_FOLLOW;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.META_ROBOTS;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW;

/**
 * The type Seo robots follow before view handler.
 */
public class SeoRobotsFollowBeforeViewHandler implements BeforeViewHandler
{
	public static final String META_TAGS = "metatags";
	public static final String ROBOTS = "robots";
	public static final String CMS_PAGE = "cmsPage";

	private Map<String, String> robotIndexForJSONMapping;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		if (modelAndView != null)
		{
			String robotsMetaTagValue = NOINDEX_NOFOLLOW;

			if (existsRobotsAttributeForCmsPage(modelAndView))
			{
				robotsMetaTagValue = WileyRobotsTagContentMapper
						.getContentForTag(getRobotsMetaTagAttributeFromCmsPage(modelAndView));
			}
			else if (containsRobotsAttributeInModel(modelAndView))
			{
				robotsMetaTagValue = (String) modelAndView.getModel().get(META_ROBOTS);
			}
			else if (RequestMethod.GET.name().equalsIgnoreCase(request.getMethod()))
			{
				robotsMetaTagValue = getRobotsMetaTagForGetRequest(request);
			}
			else if (RequestMethod.POST.name().equalsIgnoreCase(request.getMethod()))
			{
				robotsMetaTagValue = NOINDEX_NOFOLLOW;
			}

			addMetaTagToModel(robotsMetaTagValue, modelAndView);
		}
	}

	private boolean containsRobotsAttributeInModel(final ModelAndView modelAndView)
	{
		return modelAndView.getModel().containsKey(META_ROBOTS);
	}

	private boolean existsRobotsAttributeForCmsPage(final ModelAndView modelAndView)
	{
		return cmsPageAttributeExists(modelAndView)
				&& getRobotsMetaTagAttributeFromCmsPage(modelAndView) != RobotsMetaTag.UNDEFINED;
	}

	/**
	 * Gets robot index for json mapping.
	 *
	 * @return the robot index for json mapping
	 */
	protected Map<String, String> getRobotIndexForJSONMapping()
	{
		return robotIndexForJSONMapping;
	}

	/**
	 * Sets robot index for json mapping.
	 *
	 * @param robotIndexForJSONMapping
	 * 		the robot index for json mapping
	 */
	public void setRobotIndexForJSONMapping(final Map<String, String> robotIndexForJSONMapping)
	{
		this.robotIndexForJSONMapping = robotIndexForJSONMapping;
	}

	private boolean cmsPageAttributeExists(final ModelAndView modelAndView)
	{
		return modelAndView.getModel().containsKey(CMS_PAGE);
	}

	private RobotsMetaTag getRobotsMetaTagAttributeFromCmsPage(final ModelAndView modelAndView)
	{
		RobotsMetaTag result = ((AbstractPageModel) modelAndView.getModel().get(CMS_PAGE)).getRobotsMetaTag();
		return result == null ? RobotsMetaTag.UNDEFINED : result;
	}

	private void addMetaTagToModel(final String metaTag, final ModelAndView modelAndView)
	{
		if (modelAndView != null)
		{
			final MetaElementData metaElement = new MetaElementData();
			metaElement.setName(ROBOTS);
			metaElement.setContent(metaTag);

			if (modelAndView.getModel().containsKey(META_TAGS))
			{
				((List<MetaElementData>) modelAndView.getModel().get(META_TAGS)).add(metaElement);
			}
			else
			{
				List<MetaElementData> metaElementList = new ArrayList<>();
				metaElementList.add(metaElement);
				modelAndView.addObject(META_TAGS, metaElementList);
			}
		}
	}

	private String getRobotsMetaTagForGetRequest(final HttpServletRequest request)
	{
		String robotsMetaTagValue = INDEX_FOLLOW;
		//Since no model attribute metaRobots can be set for JSON response, then configure that servlet path in the xml.
		//If its a regular response and this setting has to be overriden then set model attribute metaRobots
		if (CollectionUtils.contains(getRobotIndexForJSONMapping().keySet().iterator(), request.getServletPath()))
		{
			robotsMetaTagValue = getRobotIndexForJSONMapping().get(request.getServletPath());
		}

		return robotsMetaTagValue;
	}
}