package com.wiley.storefrontcommons.util;

import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;

import com.wiley.storefrontcommons.forms.WileyAddressForm;


public class WileyAddressDataUtil extends AddressDataUtil implements Converter<WileyAddressForm, AddressData>
{
	@Resource(name = "wileyI18NFacade")
	private I18NFacade wileyI18NFacade;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Override
	public AddressData convert(final WileyAddressForm wileyAddressForm) throws ConversionException
	{
		return convert(wileyAddressForm, new AddressData());
	}

	@Override
	public AddressData convert(final WileyAddressForm wileyAddressForm, final AddressData addressData) throws ConversionException
	{
		populateAddressData(wileyAddressForm, addressData);
		return addressData;
	}

	public void populateAddressData(final WileyAddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm != null)
		{
			newAddress.setId(addressForm.getAddressId());
			newAddress.setFirstName(addressForm.getFirstName());
			newAddress.setLastName(addressForm.getLastName());
			newAddress.setLine1(addressForm.getLine1());
			newAddress.setLine2(addressForm.getLine2());
			newAddress.setTown(addressForm.getTownCity());
			newAddress.setPostalCode(addressForm.getPostcode());
			newAddress.setPhone(addressForm.getPhone());
			newAddress.setEmail(addressForm.getEmail());
			newAddress.setBillingAddress(true);
			newAddress.setShippingAddress(false);
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(
					addressForm.getSaveInAddressBook() != null && addressForm.getSaveInAddressBook().booleanValue());
			if (StringUtils.isNotEmpty(addressForm.getCountryIso()))
			{
				newAddress.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));
			}
			if (StringUtils.isNotEmpty(addressForm.getRegionIso()))
			{
				newAddress.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
			}
			copyAdditionalInformation(addressForm, newAddress);
		}
	}

	public void copyAdditionalInformation(final WileyAddressForm addressForm, final AddressData newAddress)
	{
		newAddress.setCompanyName(addressForm.getCompany());
		newAddress.setDepartment(addressForm.getDepartment());
	}

	public Date deserializeTaxNumberExpirationDate(final String dateText)
	{
		final DateFormat dateFormat = getTaxNumberExpirationDateFormat();

		Date taxNumberExpirationDate = null;
		if (org.springframework.util.StringUtils.hasText(dateText))
		{
			final Date today = getTodayDate();
			try
			{
				Date date = dateFormat.parse(dateText);
				if (date.before(today))
				{
					taxNumberExpirationDate = today;
				}
				else
				{
					taxNumberExpirationDate = date;
				}
			}
			catch (ParseException var3)
			{
				taxNumberExpirationDate = today;
			}
		}
		return taxNumberExpirationDate;
	}

	public String serializeTaxNumberExpirationDate(final Date date)
	{
		return date != null ? getTaxNumberExpirationDateFormat().format(date) : "";
	}

	private DateFormat getTaxNumberExpirationDateFormat()
	{
		//		final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		final DateFormat dateFormat = new SimpleDateFormat(
				getMessageSource().getMessage("text.store.dateformat", null, getI18nService().getCurrentLocale()));
		dateFormat.setLenient(false);
		return dateFormat;
	}

	@Override
	public I18NFacade getI18NFacade()
	{
		return wileyI18NFacade;
	}

	protected MessageSource getMessageSource()
	{
		return messageSource;
	}

	protected I18NService getI18nService()
	{
		return i18nService;
	}

	public Date getTodayDate()
	{
		return new Date();
	}
}
