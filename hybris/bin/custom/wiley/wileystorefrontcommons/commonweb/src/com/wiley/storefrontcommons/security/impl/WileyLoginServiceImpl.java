package com.wiley.storefrontcommons.security.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.wiley.core.integration.alm.user.dto.UserDto;
import com.wiley.core.integration.alm.user.service.AlmUserService;
import com.wiley.storefrontcommons.security.WileyLoginService;


/**
 * Wiley login service to work with alm user
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyLoginServiceImpl implements WileyLoginService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyLoginServiceImpl.class);

	@Autowired
	@Qualifier("originalUidUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private Converter<UserDto, CustomerModel> wileyCustomerModelReverseConverter;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private AlmUserService almUserGatewayService;

	@Autowired
	private ValidationService validationService;

	@Override
	public Authentication checkOrCreateUserAndAuthenticate(@Nonnull final String almSessionToken,
			@Nonnull final String userId)
	{
		UserDetails userDetails;
		try
		{
			userDetails = userDetailsService.loadUserByUsername(userId);
		}
		catch (UsernameNotFoundException ex)
		{
			LOG.warn("Customer doesn't exist in Hybris db, it will be created and authenticated.");
			createCustomer(userId);
			userDetails = userDetailsService.loadUserByUsername(userId);
		}
		return authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(userDetails, almSessionToken));
	}

	@Override
	public void closeSession(@Nonnull final HttpServletRequest request)
	{
		sessionService.closeCurrentSession();
		request.getSession().invalidate();
	}

	@Override
	public CustomerModel createCustomer(@Nonnull final String userId)
	{
		final UserDto userDto = almUserGatewayService.getUserData(userId);
		validateCustomerData(userDto);

		final CustomerModel customerModel = wileyCustomerModelReverseConverter.convert(userDto);
		try
		{
			customerAccountService.register(customerModel, null);
		}
		catch (DuplicateUidException e)
		{
			throw new IllegalStateException("Create customer failed", e);
		}
		return customerModel;
	}

	protected void validateCustomerData(final UserDto userDto)
	{
		Set<HybrisConstraintViolation> constrainViolations = validationService.validate(userDto);
		for (HybrisConstraintViolation hybrisConstraintViolation : constrainViolations)
		{
			throw new IllegalArgumentException(hybrisConstraintViolation.toString());
		}
	}
}
