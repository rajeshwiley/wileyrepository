package com.wiley.storefrontcommons.forms.validation;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;


/**
 * {@link WileyTaxExemptionDetailsForm} validator for wiley storefronts.
 */
@Component("wileyTaxExemptionDetailsValidator")
public class WileyTaxExemptionDetailsValidator implements Validator
{
	protected static final String TAX_NUMBER_REQUIRED_KEY =
			"checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumber.required";
	protected static final String TAX_NUMBER_EXPIRATION_DATE_REQUIRED_KEY =
			"checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumberExpirationDate.required";
	protected static final int TAX_NUMBER_MAX_FIELD_LENGTH = 255;
	protected static final String FIELD_LIMIT_EXCEED = "address.field.limit.exceed";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileyTaxExemptionDetailsForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsForm = (WileyTaxExemptionDetailsForm) object;
		if (wileyTaxExemptionDetailsForm != null)
		{
			validateBothOrNoneProvided(wileyTaxExemptionDetailsForm, errors);
			validateTaxNumberLength(wileyTaxExemptionDetailsForm.getTaxNumber(), errors);
		}
	}

	protected void validateBothOrNoneProvided(final WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsForm,
			final Errors errors)
	{
		final String taxNumber = wileyTaxExemptionDetailsForm.getTaxNumber();
		final Date expirationDate = wileyTaxExemptionDetailsForm.getTaxNumberExpirationDate();
		if (StringUtils.isEmpty(taxNumber) && expirationDate != null)
		{
			errors.rejectValue(WileyTaxExemptionDetailsForm.TAX_NUMBER_FIELD_KEY, TAX_NUMBER_REQUIRED_KEY);
		}
		else if (StringUtils.isNotEmpty(taxNumber) && expirationDate == null)
		{
			errors.rejectValue(WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY,
					TAX_NUMBER_EXPIRATION_DATE_REQUIRED_KEY);
		}
	}

	protected void validateTaxNumberLength(final String taxNumber, final Errors errors)
	{
		if (StringUtils.isNotEmpty(taxNumber) && StringUtils.length(taxNumber) > TAX_NUMBER_MAX_FIELD_LENGTH)
		{
			errors.rejectValue(WileyTaxExemptionDetailsForm.TAX_NUMBER_FIELD_KEY, FIELD_LIMIT_EXCEED,
					new Object[] { String.valueOf(TAX_NUMBER_MAX_FIELD_LENGTH) }, FIELD_LIMIT_EXCEED);
		}
	}
}
