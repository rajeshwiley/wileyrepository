package com.wiley.storefrontcommons.security;

/**
 * OOTB_CODE Handles agent roles, to enable changing authorities dynamically.
 */
public interface WileyAssistedServiceAgentAuthoritiesManager
{

	/**
	 * Add authorities (roles) from a customer to the current agent. It also adds the initial agent authorities to the
	 * session, so it can be restored later.
	 *
	 * @param customerId
	 * 		The id of the user the roles will be merged to the agent.
	 */
	void addCustomerAuthoritiesToAgent(String customerId);

	/**
	 * Restore agent initial authorities.
	 */
	void restoreInitialAuthorities();
}
