package com.wiley.storefrontcommons.authentication;

import javax.annotation.Nonnull;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.util.WebUtils;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.ALM_SESSION_TOKEN;


public class WileyCookieRequestMatcher implements RequestMatcher
{
	@Value("${" + ALM_SESSION_TOKEN + "}")
	private String almSessionTokenCookie;

	/**
	 * Check the customer and agents cookies. return true if session should be created
	 *
	 * @param request
	 * @return
	 */
	@Override
	public boolean matches(@Nonnull final HttpServletRequest request)
	{
		final Cookie almSessionToken = WebUtils.getCookie(request, this.almSessionTokenCookie);

		boolean isUserNotLogged = isUserNotLoggedIn();
		return almSessionToken != null && (isUserNotLogged || isUserGoingToRelogin(almSessionToken));
	}

	private boolean isUserNotLoggedIn()
	{
		return SecurityContextHolder.getContext().getAuthentication() == null;
	}

	private boolean isUserGoingToRelogin(final Cookie cookie)
	{
		return SecurityContextHolder.getContext().getAuthentication() != null && !SecurityContextHolder.getContext()
				.getAuthentication().getCredentials().equals(cookie.getValue());
	}
}
