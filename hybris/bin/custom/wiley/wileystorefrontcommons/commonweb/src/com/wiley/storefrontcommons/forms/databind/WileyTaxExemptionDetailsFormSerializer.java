package com.wiley.storefrontcommons.forms.databind;

import java.io.IOException;
import java.util.Date;

import javax.annotation.Resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.util.WileyAddressDataUtil;


public class WileyTaxExemptionDetailsFormSerializer extends JsonSerializer<WileyTaxExemptionDetailsForm>
{
	@Resource(name = "wileyAddressDataUtil")
	private WileyAddressDataUtil wileyAddressDataUtil;

	@Override
	public void serialize(final WileyTaxExemptionDetailsForm form, final JsonGenerator jsonGenerator,
			final SerializerProvider serializerProvider)
			throws IOException
	{
		Date taxNumberExpirationDate = form.getTaxNumberExpirationDate();
		String formattedDate = getWileyAddressDataUtil().serializeTaxNumberExpirationDate(taxNumberExpirationDate);

		jsonGenerator.writeStartObject();
		jsonGenerator.writeStringField(WileyTaxExemptionDetailsForm.TAX_NUMBER, form.getTaxNumber());
		jsonGenerator.writeStringField(WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE, formattedDate);
		jsonGenerator.writeEndObject();
	}

	protected WileyAddressDataUtil getWileyAddressDataUtil()
	{
		return wileyAddressDataUtil;
	}
}
