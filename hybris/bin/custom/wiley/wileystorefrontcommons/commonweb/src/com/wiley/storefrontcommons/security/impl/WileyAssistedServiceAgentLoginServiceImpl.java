package com.wiley.storefrontcommons.security.impl;


import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import com.wiley.storefrontcommons.authentication.WileyAuthenticationException;
import com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants;
import com.wiley.storefrontcommons.security.WileyAssistedServiceAgentAuthoritiesManager;
import com.wiley.storefrontcommons.security.WileyAssistedServiceAgentLoginService;
import com.wiley.storefrontcommons.security.WileyLoginService;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;


/**
 * Wiley assisted login service to work with alm agent
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyAssistedServiceAgentLoginServiceImpl implements WileyAssistedServiceAgentLoginService
{
	private static final Logger LOG = LoggerFactory
			.getLogger(WileyAssistedServiceAgentLoginServiceImpl.class);

	@Autowired
	@Qualifier("guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Autowired
	private WileyAssistedServiceFacade assistedServiceFacade;

	@Autowired
	private AssistedServiceService assistedServiceService;

	@Autowired
	private WileyAssistedServiceAgentAuthoritiesManager authoritiesManager;

	@Autowired
	@Qualifier("wileyasUserService")
	private UserService userService;

	@Autowired
	private WileyLoginService wileyLoginService;

	@Autowired
	private CommonI18NService commonI18NService;

	public Authentication startAsmSession(
			@Nonnull final String almSessionToken,
			@Nonnull final HttpServletRequest request,
			@Nonnull final HttpServletResponse response,
			@Nonnull final String userId,
			@Nonnull final String imitateeId)
	{
		if (StringUtils.isBlank(userId))
		{
			throw new AuthenticationServiceException("agent uid is empty");
		}
		try
		{
			Authentication authentication = null;
			final UserModel agent = userService.getUserForUID(userId);
			UserModel user;
			try
			{
				user = userService.getUserForUID(imitateeId);
			}
			catch (UnknownIdentifierException ex)
			{
				LOG.warn("Customer doesn't exist in Hybris db, it will be created and authenticated.");
				user = wileyLoginService.createCustomer(imitateeId);
			}
			if (!assistedServiceFacade.isAssistedServiceAgentLoggedIn() || shouldStartNewAsmSession(agent, user))
			{
				logoutAssistedServiceAgentIfNecessary();
				launchAssistedServiceModeIfNecessary();

				assistedServiceFacade.loginAssistedServiceAgent(agent);
				login(userId, almSessionToken, request, response, user);
				assistedServiceFacade.emulateAfterLogin();

				final CartModel lastCart = assistedServiceService.getLatestModifiedCart(user);
				String lastCartCode = null;
				if (lastCart != null)
				{
					lastCartCode = lastCart.getCode();
					commonI18NService.setCurrentCurrency(lastCart.getCurrency());
				}
				assistedServiceFacade.emulateCustomer(user.getUid(), lastCartCode);
				authentication = refreshSpringSecurityTokenInternal();
				setSessionTimeout();
			}
			return authentication;
		}
		catch (AssistedServiceException e)
		{
			wileyLoginService.closeSession(request);
			throw new WileyAuthenticationException("Unexpected Error", e);
		}
	}

	private void login(final String almAgentUid,
			final String asmAgentSessionToken,
			final HttpServletRequest request,
			final HttpServletResponse response,
			final UserModel emulatedUser)
	{
		final WileyAssistedServiceAgentPrincipal principal = new WileyAssistedServiceAgentPrincipal(almAgentUid);
		final WileyAssistedServiceAuthenticationToken token =
				new WileyAssistedServiceAuthenticationToken(principal, asmAgentSessionToken, emulatedUser);
		token.setDetails(new WebAuthenticationDetails(request));
		SecurityContextHolder.getContext().setAuthentication(token);
		guidCookieStrategy.setCookie(request, response);
	}

	private boolean shouldStartNewAsmSession(final UserModel agent, final UserModel customer)
	{
		final AssistedServiceSession asmSession = assistedServiceFacade.getAsmSession();
		return !agent.equals(asmSession.getAgent())
				|| !customer.equals(asmSession.getEmulatedCustomer());
	}

	private void logoutAssistedServiceAgentIfNecessary() throws AssistedServiceException
	{
		if (assistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			assistedServiceFacade.logoutAssistedServiceAgent();
		}
	}

	private void launchAssistedServiceModeIfNecessary()
	{
		if (!assistedServiceFacade.isAssistedServiceModeLaunched())
		{
			assistedServiceFacade.launchAssistedServiceMode();
		}
	}

	// OOTB_CODE
	private Authentication refreshSpringSecurityTokenInternal()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof WileyAssistedServiceAuthenticationToken)
		{
			final UserModel currentUser = userService.getCurrentUser();
			if (currentUser == null || userService.isAnonymousUser(currentUser) || isASAgent(currentUser))
			{
				((WileyAssistedServiceAuthenticationToken) authentication).setEmulating(false);
			}
			else
			{
				((WileyAssistedServiceAuthenticationToken) authentication).setEmulating(true);
				authoritiesManager.addCustomerAuthoritiesToAgent(currentUser.getUid());
			}
		}
		return authentication;
	}

	// OOTB_CODE
	private boolean isASAgent(final UserModel currentUser)
	{
		final Set<UserGroupModel> userGroups = userService.getAllUserGroupsForUser(currentUser);
		for (final UserGroupModel userGroup : userGroups)
		{
			if (WileystorefrontcommonsConstants.AS_AGENT_GROUP_UID.equals(userGroup.getUid()))
			{
				return true;
			}
		}
		return false;
	}

	// OOTB_CODE
	private void setSessionTimeout()
	{
		JaloSession.getCurrentSession().setTimeout(assistedServiceFacade.getAssistedServiceSessionTimeout());
		// since agent is logged in - change session timeout to the value from properties
		((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession()
				.setMaxInactiveInterval(assistedServiceFacade.getAssistedServiceSessionTimeout()); // in seconds
	}
}
