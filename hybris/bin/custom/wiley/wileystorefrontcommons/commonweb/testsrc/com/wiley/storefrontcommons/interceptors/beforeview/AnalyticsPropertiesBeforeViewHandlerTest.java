package com.wiley.storefrontcommons.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.constants.WileyCoreConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AnalyticsPropertiesBeforeViewHandlerTest
{
	private static final String ANALYTICS_SCRIPT_URL_AGS_VALUE = "//analytics_script_url_for_ags";
	private static final String ANALYTICS_SCRIPT_URL_MODEL_KEY = "adobeAnalyticsScriptURL";
	private static final String ISO_CODE = "USA";

	@Mock
	private SiteConfigService siteConfigService;

	@Mock
	CommonI18NService commonI18NService;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	CurrencyModel currencyModel;

	private ModelAndView modelAndView = new ModelAndView();

	@InjectMocks
	@Spy
	private AnalyticsPropertiesBeforeViewHandler analyticsPropertiesBeforeViewHandler;

	@Before
	public void setUp() throws NoSuchFieldException, IllegalAccessException
	{
		doNothing().when(analyticsPropertiesBeforeViewHandler).registerConfigChangeLister(); // Disable Tenant activation
		when(currencyModel.getIsocode()).thenReturn(ISO_CODE);
		when(commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);
		when(siteConfigService.getProperty(WileyCoreConstants.ANALYTICS_SCRIPT_URL_CONFIG_KEY)).thenReturn(
				ANALYTICS_SCRIPT_URL_AGS_VALUE);
	}

	/**
	 * Should use AGS specific Adobe Analytics URL
	 */
	@Test
	public void shouldUseAgsSpecificAdobeAnalyticsUrl()
	{
		analyticsPropertiesBeforeViewHandler.beforeView(request, response, modelAndView);
		assertEquals(modelAndView.getModelMap().get(ANALYTICS_SCRIPT_URL_MODEL_KEY), ANALYTICS_SCRIPT_URL_AGS_VALUE);
	}
}
