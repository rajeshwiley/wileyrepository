package com.wiley.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;
import com.wiley.storefrontcommons.util.RequestLocaleManager;

import static com.wiley.core.enums.TaxExemptionEnum.TAX;
import static com.wiley.core.enums.TaxExemptionEnum.VAT;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentAddressCheckoutStepControllerTest
{
	private static final String TEST_TAX_NUMBER = "1";
	private static final String TEST_COUNTRY_ISO = "countryIso";

	@Mock
	private WileyAddressForm wileyAddressFormMock;

	@Mock
	private WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsFormMock;

	@Mock
	private CountryData countryDataMock;

	@Mock
	private BindingResult bindingResultMock;

	@Mock
	private WileyCheckoutFacade wileyCheckoutFacadeMock;

	@Mock
	private RequestLocaleManager requestLocaleManager; //NOPMD

	@InjectMocks
	private WileyPaymentAddressCheckoutStepController wileyPaymentAddressCheckoutStepController;

	@Before
	public void setUp()
	{
		when(wileyAddressFormMock.getVatRegistrationDetailsForm()).thenReturn(wileyVatRegistrationDetailsFormMock);
		when(wileyVatRegistrationDetailsFormMock.getTaxNumber()).thenReturn(TEST_TAX_NUMBER);
		when(wileyVatRegistrationDetailsFormMock.getCountryIso()).thenReturn(TEST_COUNTRY_ISO);
		when(bindingResultMock.hasFieldErrors()).thenReturn(false);
		doNothing().when(wileyCheckoutFacadeMock).validateTaxInformation(anyString(), anyString());
	}

	@Test
	public void testValidateTaxDetailsVatCountry()
	{
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(VAT);

		wileyPaymentAddressCheckoutStepController.validateTaxDetails(wileyAddressFormMock, countryDataMock, bindingResultMock);

		verify(wileyAddressFormMock).getVatRegistrationDetailsForm();
		verify(wileyVatRegistrationDetailsFormMock).getTaxNumber();
		verify(wileyCheckoutFacadeMock).validateTaxInformation(anyString(), anyString());
	}

	@Test
	public void testValidateTaxDetailsNonVatCountry()
	{
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(TAX);

		wileyPaymentAddressCheckoutStepController.validateTaxDetails(wileyAddressFormMock, countryDataMock, bindingResultMock);

		verify(wileyAddressFormMock, never()).getVatRegistrationDetailsForm();
		verify(wileyCheckoutFacadeMock, times(1)).validateTaxInformation(anyString(), anyString());
	}
}
