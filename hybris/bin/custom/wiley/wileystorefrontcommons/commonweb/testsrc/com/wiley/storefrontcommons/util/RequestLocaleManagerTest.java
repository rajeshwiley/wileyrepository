package com.wiley.storefrontcommons.util;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.storefrontcommons.web.i18n.StoreSessionLocaleResolver;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RequestLocaleManagerTest
{
	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private WileyCommonI18NService i18NService;

	@Spy
	@InjectMocks
	private RequestLocaleManager manager;

	private Locale locale = new Locale("en", "US");

	@Test
	public void setLocaleWhenCountryIsoIsEmpty()
	{
		when(i18NService.getLocaleByCountryIsoCodeWithCurrentLanguage(null)).thenReturn(locale);

		manager.setLocale(mockRequest, null);

		verify(i18NService, never()).getLocaleByCountryIsoCodeWithCurrentLanguage(null);
		verify(mockRequest, never()).setAttribute(StoreSessionLocaleResolver.class.getName() + ".LOCALE", locale);
	}

	@Test
	public void setLocaleWhenCountryIsoIsNotEmpty()
	{
		final Locale expectedLocale = new Locale("en", "CA");
		when(i18NService.getLocaleByCountryIsoCodeWithCurrentLanguage("CA")).thenReturn(expectedLocale);

		manager.setLocale(mockRequest, "CA");

		verify(i18NService, times(1)).getLocaleByCountryIsoCodeWithCurrentLanguage("CA");
		verify(mockRequest).setAttribute(StoreSessionLocaleResolver.class.getName() + ".LOCALE", expectedLocale);
	}
}