package com.wiley.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyVatRegistrationDetailsValidatorUnitTest
{
	@Mock
	private WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsFormMock;

	@Mock
	private Errors errorsMock;

	@InjectMocks
	private WileyVatRegistrationDetailsValidator testInstance;

	@Test
	public void validateFormShouldNotRejectValuesIfTaxNumberNull()
	{
		// given
		when(wileyVatRegistrationDetailsFormMock.getTaxNumber()).thenReturn(null);

		// when
		testInstance.validate(wileyVatRegistrationDetailsFormMock, errorsMock);

		// then
		verify(errorsMock, never()).rejectValue(any(), any());
	}

	@Test
	public void validateFormShouldNotRejectValuesIfTaxNumberEmpty()
	{
		// given
		when(wileyVatRegistrationDetailsFormMock.getTaxNumber()).thenReturn(StringUtils.EMPTY);

		// when
		testInstance.validate(wileyVatRegistrationDetailsFormMock, errorsMock);

		// then
		verify(errorsMock, never()).rejectValue(any(), any());
	}

	@Test
	public void validateFormShouldRejectTaxNumberIfLimitExceed()
	{
		// given
		String taxNumberExceedLimit = StringUtils.repeat("x",
				WileyVatRegistrationDetailsValidator.TAX_NUMBER_MAX_FIELD_LENGTH + 1);
		when(wileyVatRegistrationDetailsFormMock.getTaxNumber()).thenReturn(taxNumberExceedLimit);

		// when
		testInstance.validate(wileyVatRegistrationDetailsFormMock, errorsMock);

		// then
		verify(errorsMock).rejectValue(
				WileyVatRegistrationDetailsForm.TAX_NUMBER_FIELD_KEY,
				WileyVatRegistrationDetailsValidator.FIELD_LIMIT_EXCEED,
				new Object[] { String.valueOf(WileyVatRegistrationDetailsValidator.TAX_NUMBER_MAX_FIELD_LENGTH) },
				WileyVatRegistrationDetailsValidator.FIELD_LIMIT_EXCEED
		);
	}
}
