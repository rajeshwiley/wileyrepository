package com.wiley.storefrontcommons.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HttpServletRequestUtilTest
{
	private static final String PARAMETER_KEY_1 = "key1";
	private static final String PARAMETER_KEY_2 = "key2";
	private static final String PARAMETER_VALUE_1 = "value1";
	private static final String PARAMETER_VALUE_2 = "value2";

	private HttpServletRequestUtil testInstance = new HttpServletRequestUtil();

	@Mock
	private HttpServletRequest mockRequest;

	private Enumeration<String> parameterNames = Collections.enumeration(Arrays.asList(PARAMETER_KEY_1, PARAMETER_KEY_2));

	@Before
	public void setUp()
	{
		when(mockRequest.getParameterNames()).thenReturn(parameterNames);
		when(mockRequest.getParameter(PARAMETER_KEY_1)).thenReturn(PARAMETER_VALUE_1);
		when(mockRequest.getParameter(PARAMETER_KEY_2)).thenReturn(PARAMETER_VALUE_2);
	}

	@Test
	public void shouldReturnParameterMapFromRequest()
	{
		Map<String, String> requestParameterMap = testInstance.getRequestParameterMap(mockRequest);
		assertTrue(requestParameterMap.containsKey(PARAMETER_KEY_1));
		assertTrue(requestParameterMap.containsKey(PARAMETER_KEY_2));
		assertEquals(PARAMETER_VALUE_1, requestParameterMap.get(PARAMETER_KEY_1));
		assertEquals(PARAMETER_VALUE_2, requestParameterMap.get(PARAMETER_KEY_2));
	}
}
