package com.wiley.storefrontcommons.web;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Uladzimir_Barouski on 12/15/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyTrailingSlashRedirectionFilterUnitTest
{
	public static final String HOST = "http://host";
	public static final String URI_WITHOUT_TRAILING_SLASH = "/storefront/category/product";
	public static final String FILE_URI = "/storefront/category/product.any";
	public static final String URI_WITH_TRAILINF_SLASH = "/storefront/category/product/";
	public static final String EXCLUSION_REGEXP = "^(?!(.*\\/health$)|(.*\\/remtitle$)|(.*\\..*$)|(.*\\/$)).*$";
	@Mock
	private FilterChain filterChain;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private HttpServletRequest httpServletRequest;
	@Mock
	private HttpServletResponse httpServletResponse;

	private WileyTrailingSlashRedirectionFilter wileyTrailingSlashRedirectionFilter = new WileyTrailingSlashRedirectionFilter();

	@Before
	public void setup() {
		wileyTrailingSlashRedirectionFilter
				.setExclusionRegexp(EXCLUSION_REGEXP);
	}
	@Test
	public void checkRedirectToTrailingSlashUrl() throws IOException, ServletException
	{
		initialize(URI_WITHOUT_TRAILING_SLASH, false, httpServletRequest, httpServletResponse);
		wileyTrailingSlashRedirectionFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		verify(filterChain, never()).doFilter(httpServletRequest, httpServletResponse);
		verify(httpServletResponse, times(1)).setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		verify(httpServletResponse, times(1)).setHeader("Location", HOST + URI_WITH_TRAILINF_SLASH);
	}

	@Test
	public void checkWhenUrlHasTrailingSlash() throws IOException, ServletException
	{
		initialize(URI_WITH_TRAILINF_SLASH, false, httpServletRequest, httpServletResponse);
		wileyTrailingSlashRedirectionFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		verify(filterChain, times(1)).doFilter(httpServletRequest, httpServletResponse);
	}

	@Test
	public void checkWhenUrlHasParameters() throws IOException, ServletException
	{
		initialize(URI_WITHOUT_TRAILING_SLASH, true, httpServletRequest, httpServletResponse);
		wileyTrailingSlashRedirectionFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		verify(filterChain, times(1)).doFilter(httpServletRequest, httpServletResponse);
	}

	@Test
	public void checkWhenFileUrl() throws IOException, ServletException
	{
		initialize(FILE_URI, false, httpServletRequest, httpServletResponse);
		wileyTrailingSlashRedirectionFilter.doFilterInternal(httpServletRequest, httpServletResponse, filterChain);
		verify(filterChain, times(1)).doFilter(httpServletRequest, httpServletResponse);
	}

	private void initialize(final String t, final boolean t2, final HttpServletRequest httpServletRequest,
			final HttpServletResponse httpServletResponse) throws IOException, ServletException
	{
		when(httpServletRequest.getRequestURI()).thenReturn(t);
		when(httpServletRequest.getRequestURL()).thenReturn(new StringBuffer(HOST + t));
		when(httpServletRequest.getParameterNames().hasMoreElements()).thenReturn(t2);
		doNothing().when(filterChain).doFilter(httpServletRequest, httpServletResponse);
	}
}