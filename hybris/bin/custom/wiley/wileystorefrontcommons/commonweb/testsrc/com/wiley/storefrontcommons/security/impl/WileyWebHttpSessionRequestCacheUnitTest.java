/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.security.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.junit.Assert.assertEquals;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.ArgumentMatcher;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

import static java.util.Collections.enumeration;
import static java.util.Collections.singleton;


/**
 * The type Web http session request cache unit test.
 */
@UnitTest
public class WileyWebHttpSessionRequestCacheUnitTest
{
	//

	@InjectMocks
	private final WileyWebHttpSessionRequestCache cache = new WileyWebHttpSessionRequestCache();

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private SessionService sessionService;

	@Mock
	private Authentication authentication;

	/**
	 * Prepare.
	 */
	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);
	}


	/**
	 * Test save request.
	 */
	@Test
	public void testSaveRequest()
	{

		SecurityContextHolder.getContext().setAuthentication(authentication);
		BDDMockito.given(request.getRequestURL()).willReturn(new StringBuffer("dummy"));
		BDDMockito.given(request.getScheme()).willReturn("dummy");
		BDDMockito.given(request.getHeaders("referer")).willReturn(enumeration(singleton("some blah")));
		BDDMockito.given(request.getHeaderNames()).willReturn(enumeration(singleton("referer")));
		BDDMockito.given(request.getSession(false)).willReturn(null);

		cache.saveRequest(request, response);

		Mockito.verify(request.getSession()).setAttribute(Mockito.eq("SPRING_SECURITY_SAVED_REQUEST"),
				Mockito.argThat(new DefaultSavedRequestArgumentMatcher("some blah")));
	}


	/**
	 * Test calc redirect url with encoding attrs.
	 */
	@Test
	public void testCalcRedirectUrlWithEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/en", "/agsstorefront/electronics/en",
						"https://electronics.local:9002/agsstorefront/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/en", "/agsstorefront/electronics/en",
						"https://electronics.local:9002/agsstorefront/electronics/en/"));
	}


	/**
	 * Test calc redirect url with mismatch encoding attrs.
	 */
	@Test
	public void testCalcRedirectUrlWithMismatchEncodingAttrs()
	{
		assertEquals(
				"electronics/en",
				executeCalculateRelativeRedirectUrl("electronics/ja/Y/Z", "/agsstorefront/electronics/ja/Y/Z",
						"https://electronics.local:9002/agsstorefront/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/ja/Y/Z", "/agsstorefront/electronics/en",
						"https://electronics.local:9002/agsstorefront/electronics/en/"));
	}

	/**
	 * Test calc redirect url without encoding attrs.
	 */
	@Test
	public void testCalcRedirectUrlWithoutEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("", "/agsstorefront",
						"https://electronics.local:9002/agsstorefront"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("", "/agsstorefront",
						"https://electronics.local:9002/agsstorefront/"));
	}

	/**
	 * Test calc redirect url with encoding attrs servlet path.
	 */
	@Test
	public void testCalcRedirectUrlWithEncodingAttrsServletPath()
	{
		assertEquals(
				"/Open-Catalogue/Cameras/Digital-Cameras/c/575",
				executeCalculateRelativeRedirectUrl("electronics/en", "/agsstorefront/electronics/en",
						"https://electronics.local:9002/agsstorefront/electronics/en/Open-Catalogue/Cameras/Digital-Cameras/c"
								+ "/575"));
	}

	/**
	 * Test calc redirect url empty context without encoding attrs.
	 */
	@Test
	public void testCalcRedirectUrlEmptyContextWithoutEncodingAttrs()
	{
		assertEquals("/", executeCalculateRelativeRedirectUrl("", "", "https://electronics.local:9002/"));
	}

	/**
	 * Test calc redirect url empty context with encoding attrs.
	 */
	@Test
	public void testCalcRedirectUrlEmptyContextWithEncodingAttrs()
	{
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/en", "/electronics/en",
						"https://electronics.local:9002/electronics/en"));
		assertEquals(
				"/",
				executeCalculateRelativeRedirectUrl("electronics/en", "/electronics/en",
						"https://electronics.local:9002/electronics/en/"));
	}

	/**
	 * Test calc redirect url empty context with enc attrs servlet path.
	 */
	@Test
	public void testCalcRedirectUrlEmptyContextWithEncAttrsServletPath()
	{
		assertEquals(
				"/login",
				executeCalculateRelativeRedirectUrl("electronics/en", "/electronics/en",
						"https://electronics.local:9002/electronics/en/login"));
		assertEquals(
				"/login/",
				executeCalculateRelativeRedirectUrl("electronics/en", "/electronics/en",
						"https://electronics.local:9002/electronics/en/login/"));
		assertEquals(
				"/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584",
				executeCalculateRelativeRedirectUrl("electronics/en", "/electronics/en",
						"https://electronics.local:9002/electronics/en/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584"));
	}

	/**
	 * Test calc redirect url empty context without enc attrs servlet path.
	 */
	@Test
	public void testCalcRedirectUrlEmptyContextWithoutEncAttrsServletPath()
	{
		assertEquals(
				"Open-Catalogue/Cameras/Hand-held-Camcorders/c/584",
				executeCalculateRelativeRedirectUrl("", "",
						"https://electronics.local:9002/Open-Catalogue/Cameras/Hand-held-Camcorders/c/584"));
	}

	/**
	 * Execute calculate relative redirect url string.
	 *
	 * @param urlEncodingAttrs
	 * 		the url encoding attrs
	 * @param contextPath
	 * 		the context path
	 * @param url
	 * 		the url
	 * @return the string
	 */
	protected String executeCalculateRelativeRedirectUrl(final String urlEncodingAttrs, final String contextPath,
			final String url)
	{
		BDDMockito.given(sessionService.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES)).willReturn(urlEncodingAttrs);
		return cache.calculateRelativeRedirectUrl(contextPath, url);
	}


	/**
	 * The type Default saved request argument matcher.
	 */
	class DefaultSavedRequestArgumentMatcher extends ArgumentMatcher<DefaultSavedRequest>
	{

		private final String url;

		/**
		 * Instantiates a new Default saved request argument matcher.
		 *
		 * @param url
		 * 		the url
		 */
		DefaultSavedRequestArgumentMatcher(final String url)
		{
			this.url = url;
		}

		@Override
		public boolean matches(final Object argument)
		{
			if (argument instanceof DefaultSavedRequest)
			{
				final DefaultSavedRequest arg = (DefaultSavedRequest) argument;
				return url.equals(arg.getRedirectUrl());
			}
			return false;
		}

	}
}
