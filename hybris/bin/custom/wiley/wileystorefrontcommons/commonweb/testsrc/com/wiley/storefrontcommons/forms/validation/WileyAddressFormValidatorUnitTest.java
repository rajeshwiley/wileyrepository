package com.wiley.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.facades.i18n.WileyI18NFacade;
import com.wiley.storefrontcommons.forms.WileyAddressForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAddressFormValidatorUnitTest
{
	private static final String COUNTRY_ISO = "iso";

	@InjectMocks
	private WileyAddressFormValidator wileyAddressFormValidator;

	@Mock
	private Validator wileyTaxExemptionDetailsValidator;

	@Mock
	private Validator wileyVatRegistrationDetailsValidator;

	@Mock
	private WileyI18NFacade wileyI18NFacadeMock;

	@Mock
	private WileyAddressForm wileyAddressFormMock;

	@Mock
	private Errors errorsMock;

	@Mock
	private Object objectMock;

	@Mock
	private CountryData countryDataMock;

	@Mock
	private WileyCommonI18NService wileyCommonI18nService;

	private Locale locale = Locale.US;

	@Before
	public void setUp()
	{
		// need to return empty strings to avoid NPE in regex
		when(wileyAddressFormMock.getPhone()).thenReturn(StringUtils.EMPTY);
		when(wileyAddressFormMock.getEmail()).thenReturn(StringUtils.EMPTY);
		when(wileyAddressFormMock.getCountryIso()).thenReturn(COUNTRY_ISO);
		when(wileyI18NFacadeMock.getCountryForIsocode(COUNTRY_ISO)).thenReturn(countryDataMock);
		when(wileyCommonI18nService.getLocaleByCountryIsoCodeWithCurrentLanguage(COUNTRY_ISO)).thenReturn(locale);
	}

	@Test
	public void shouldFailValidateWithNullWileyAddressForm()
	{
		try
		{
			wileyAddressFormValidator.validate(null, errorsMock, objectMock);
			fail("IllegalArgumentException expected");
		}
		catch (IllegalArgumentException e)
		{
			assertEquals("Parameter object can not be null", e.getMessage());
		}
	}

	@Test
	public void shouldFailValidateWithNullErrors()
	{
		try
		{
			wileyAddressFormValidator.validate(wileyAddressFormMock, null, objectMock);
			fail("IllegalArgumentException expected");
		}
		catch (IllegalArgumentException e)
		{
			assertEquals("Parameter errors can not be null", e.getMessage());
		}
	}

	@Test
	public void shouldFailValidateWithNullCountryData()
	{
		try
		{
			wileyAddressFormValidator.validate(wileyAddressFormMock, errorsMock, null);
			fail("IllegalArgumentException expected");
		}
		catch (IllegalArgumentException e)
		{
			assertEquals("Parameter objects can not be null", e.getMessage());
		}
	}

	@Test
	public void shouldNotFailWithNullCountryApplicableTaxExemption()
	{
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(null);

		wileyAddressFormValidator.validate(wileyAddressFormMock, errorsMock);

		verify(wileyTaxExemptionDetailsValidator, never()).validate(any(), any());
		verify(wileyVatRegistrationDetailsValidator, never()).validate(any(), any());
	}
}