package com.wiley.storefrontcommons.beans.propertyeditors;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.storefrontcommons.util.WileyAddressDataUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFallbackToTodayDateEditorTest
{
	public static final String TEXT_DATE = "21/31/2020";

	@Mock
	private Date dateMock;

	@Mock
	private WileyAddressDataUtil wileyAddressDataUtilMock;

	@Spy
	@InjectMocks
	private WileyFallbackToTodayDateEditor testInstance;

	@Test
	public void validateFormShouldSetExpirationDateFromUtil()
	{
		// given
		when(wileyAddressDataUtilMock.deserializeTaxNumberExpirationDate(TEXT_DATE)).thenReturn(dateMock);

		// when
		testInstance.setAsText(TEXT_DATE);

		// then
		verify(testInstance).setValue(dateMock);
	}
}