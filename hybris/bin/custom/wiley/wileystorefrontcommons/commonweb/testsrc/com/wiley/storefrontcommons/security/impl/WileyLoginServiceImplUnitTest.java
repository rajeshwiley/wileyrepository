package com.wiley.storefrontcommons.security.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.wiley.core.integration.alm.user.dto.UserDto;
import com.wiley.core.integration.alm.user.service.AlmUserService;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.spring.security.CoreUserDetailsService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;

import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLoginServiceImplUnitTest
{
	private static final String ALM_USER_ID = "almUserId";

	@Mock
	private CoreUserDetailsService coreUserDetailsService;

	@Mock
	private AuthenticationManager authenticationManager;

	@Mock
	private Converter<UserDto, CustomerModel> wileyasUserDataConverter;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private AlmUserService userGatewayService;

	@Mock
	private ValidationService validationService;

	@Mock
	private HybrisConstraintViolation constraintViolation;

	@InjectMocks
	private WileyLoginServiceImpl wileyLoginService;

	private UserDto userDto = new UserDto();

	private CustomerModel customerModel = new CustomerModel();

	@Before
	public void setUp()
	{
		when(coreUserDetailsService.loadUserByUsername(ALM_USER_ID)).thenThrow(new UsernameNotFoundException(""))
				.thenReturn(null);
		when(userGatewayService.getUserData(ALM_USER_ID)).thenReturn(userDto);
		when(wileyasUserDataConverter.convert(userDto)).thenReturn(customerModel);
		when(constraintViolation.toString()).thenReturn("test msg");

	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionForInvalidData()
	{
		when(validationService.validate(userDto)).thenReturn(new HashSet<>(Arrays.asList(constraintViolation)));
		wileyLoginService.checkOrCreateUserAndAuthenticate(null, ALM_USER_ID);

	}

	@Test
	public void shouldCreateCustomerForValidData() throws DuplicateUidException
	{
		when(validationService.validate(userDto)).thenReturn(Collections.emptySet());
		wileyLoginService.checkOrCreateUserAndAuthenticate(null, ALM_USER_ID);
		Mockito.verify(customerAccountService, Mockito.times(1)).register(customerModel, null);
	}
}
