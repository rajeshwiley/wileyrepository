package com.wiley.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyTaxExemptionDetailsValidatorUnitTest
{
	public static final String TAX_NUMBER = StringUtils.repeat("x",
			WileyTaxExemptionDetailsValidator.TAX_NUMBER_MAX_FIELD_LENGTH);

	private Date taxNumberExpirationDate = new GregorianCalendar(2018, 4, 24).getTime();

	@Mock
	private WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsFormMock;

	@Mock
	private Errors errorsMock;

	@Mock
	private FieldError fieldErrorBindingFailure;

	@Mock
	private FieldError fieldErrorNotBindingFailure;

	@Spy
	@InjectMocks
	private WileyTaxExemptionDetailsValidator testInstance;

	@Before
	public void setUp()
	{
		when(wileyTaxExemptionDetailsFormMock.getTaxNumber()).thenReturn(TAX_NUMBER);
		when(wileyTaxExemptionDetailsFormMock.getTaxNumberExpirationDate()).thenReturn(taxNumberExpirationDate);
		when(errorsMock.hasFieldErrors(WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY)).thenReturn(false);
		when(fieldErrorBindingFailure.isBindingFailure()).thenReturn(true);
		when(fieldErrorNotBindingFailure.isBindingFailure()).thenReturn(false);
	}

	@Test
	public void validateCorrectFormShouldNotRejectValues()
	{
		// given

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock, never()).rejectValue(any(), any());
	}

	@Test
	public void validateNullFormShouldNotRejectValues()
	{
		// given

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock, never()).rejectValue(any(), any());
	}

	@Test
	public void validateFormShouldNotRejectValuesIfNoneOfTwoFieldsProvided()
	{
		// given
		when(wileyTaxExemptionDetailsFormMock.getTaxNumber()).thenReturn(null);
		when(wileyTaxExemptionDetailsFormMock.getTaxNumberExpirationDate()).thenReturn(null);

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock, never()).rejectValue(any(), any());
	}

	@Test
	public void validateFormShouldRejectTaxNumberIfOnlyExpirationDateProvided()
	{
		// given
		when(wileyTaxExemptionDetailsFormMock.getTaxNumber()).thenReturn(null);

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock).rejectValue(WileyTaxExemptionDetailsForm.TAX_NUMBER_FIELD_KEY,
				WileyTaxExemptionDetailsValidator.TAX_NUMBER_REQUIRED_KEY);
	}

	@Test
	public void validateFormShouldRejectExpirationDateIfOnlyTaxNumberProvided()
	{
		// given
		when(wileyTaxExemptionDetailsFormMock.getTaxNumberExpirationDate()).thenReturn(null);

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock).rejectValue(WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY,
				WileyTaxExemptionDetailsValidator.TAX_NUMBER_EXPIRATION_DATE_REQUIRED_KEY);
	}

	@Test
	public void validateFormShouldRejectTaxNumberIfLimitExceed()
	{
		// given
		String taxNumberExceedLimit = StringUtils.repeat("x",
				WileyTaxExemptionDetailsValidator.TAX_NUMBER_MAX_FIELD_LENGTH + 1);
		when(wileyTaxExemptionDetailsFormMock.getTaxNumber()).thenReturn(taxNumberExceedLimit);

		// when
		testInstance.validate(wileyTaxExemptionDetailsFormMock, errorsMock);

		// then
		verify(errorsMock).rejectValue(
				WileyTaxExemptionDetailsForm.TAX_NUMBER_FIELD_KEY,
				WileyTaxExemptionDetailsValidator.FIELD_LIMIT_EXCEED,
				new Object[] { String.valueOf(WileyTaxExemptionDetailsValidator.TAX_NUMBER_MAX_FIELD_LENGTH) },
				WileyTaxExemptionDetailsValidator.FIELD_LIMIT_EXCEED
		);
	}
}
