package com.wiley.storefrontcommons.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import static org.mockito.Mockito.mock;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.storefrontcommons.util.WileyRobotsTagContentMapper;

import static com.wiley.storefrontcommons.interceptors.beforeview.SeoRobotsFollowBeforeViewHandler.CMS_PAGE;
import static com.wiley.storefrontcommons.interceptors.beforeview.SeoRobotsFollowBeforeViewHandler.META_TAGS;
import static com.wiley.storefrontcommons.interceptors.beforeview.SeoRobotsFollowBeforeViewHandler.ROBOTS;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants.SeoRobots.META_ROBOTS;
import static junit.framework.Assert.assertTrue;


@UnitTest
public class SeoRobotsFollowBeforeViewHandlerTest
{
	private MockHttpServletRequest request = new MockHttpServletRequest();
	private MockHttpServletResponse response = new MockHttpServletResponse();
	private ModelAndView modelAndView = new ModelAndView();

	private SeoRobotsFollowBeforeViewHandler seoRobotsFollowBeforeViewHandler = new SeoRobotsFollowBeforeViewHandler();

	@Before
	public void setUp() throws Exception
	{
		seoRobotsFollowBeforeViewHandler.setRobotIndexForJSONMapping(Collections.emptyMap());
	}

	@Test
	public void shouldUseNoIndexNoFollowByDefault()
	{
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseIndexFollowRobotsMetaTagDefinedInController()
	{
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.INDEX_FOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexNoFollowRobotsMetaTagDefinedInController()
	{
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.INDEX_NOFOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseNoIndexFollowRobotsMetaTagDefinedInController()
	{
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.NOINDEX_FOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_FOLLOW));
	}

	@Test
	public void shouldUseNoIndexNoFollowRobotsMetaTagDefinedInController()
	{
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.NOINDEX_NOFOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseRobotsMetaTagDefinedInPageAttributeInsteadOfController()
	{
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.INDEX_FOLLOW);
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.NOINDEX_NOFOLLOW);

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseIndexFollowRobotsMetaTagDefinedInPageAttribute()
	{
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.INDEX_FOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexNoFollowRobotsMetaTagDefinedInPageAttribute()
	{
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.INDEX_NOFOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseNoIndexFollowRobotsMetaTagDefinedInPageAttribute()
	{
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.NOINDEX_FOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_FOLLOW));
	}

	@Test
	public void shouldUseNoIndexNoFollowRobotsMetaTagDefinedInPageAttribute()
	{
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.NOINDEX_NOFOLLOW);
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseNoIndexNoFollowForUndefinedRobotsMetaTagWhenRequestIsPost()
	{
		request.setMethod(RequestMethod.POST.name());
		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);
		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.NOINDEX_NOFOLLOW));
	}

	@Test
	public void shouldUseIndexFollowRobotsMetaTagDefinedInControllerWhenRequestIsPost()
	{
		request.setMethod(RequestMethod.POST.name());
		setUpControllerRobotsMetaTagAttribute(RobotsMetaTag.INDEX_FOLLOW);

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexFollowRobotsMetaTagDefinedInPageAttributeWhenRequestIsPost()
	{
		request.setMethod(RequestMethod.POST.name());
		setUpPageRobotsMetaTagAttribute(RobotsMetaTag.INDEX_FOLLOW);

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexFollowForUndefinedRobotsMetaTagWhenRequestIsGetAndSecure()
	{
		request.setMethod(RequestMethod.GET.name());
		request.setSecure(true);

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexFollowForUndefinedRobotsMetaTagWhenRequestIsGetAndNotSecure()
	{
		request.setMethod(RequestMethod.GET.name());
		request.setSecure(false);
		request.setServletPath("unused value");
		seoRobotsFollowBeforeViewHandler.setRobotIndexForJSONMapping(new HashMap<String, String>());

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_FOLLOW));
	}

	@Test
	public void shouldUseIndexNoFollowForUndefinedRobotsMetaTagWhenRequestIsGetAndNotSecureAndJsonIsSet()
	{
		request.setMethod(RequestMethod.GET.name());
		request.setSecure(false);
		request.setServletPath("servlet path");

		Map<String, String> robotIndexForJSONMapping = new HashMap<>();
		robotIndexForJSONMapping.put(request.getServletPath(),
				WileyRobotsTagContentMapper.getContentForTag(RobotsMetaTag.INDEX_NOFOLLOW));
		seoRobotsFollowBeforeViewHandler.setRobotIndexForJSONMapping(robotIndexForJSONMapping);

		seoRobotsFollowBeforeViewHandler.beforeView(request, response, modelAndView);

		assertTrue(checkRobotsMetaTagFor(RobotsMetaTag.INDEX_NOFOLLOW));
	}

	private void setUpControllerRobotsMetaTagAttribute(final RobotsMetaTag robotsMetaTag)
	{
		String robotsMetaTagValue = WileyRobotsTagContentMapper.getContentForTag(robotsMetaTag);
		modelAndView.addObject(META_ROBOTS, robotsMetaTagValue);
	}

	private void setUpPageRobotsMetaTagAttribute(final RobotsMetaTag robotsMetaTag)
	{
		AbstractPageModel pageModel = mock(AbstractPageModel.class);
		Mockito.when(pageModel.getRobotsMetaTag()).thenReturn(robotsMetaTag);
		pageModel.setRobotsMetaTag(robotsMetaTag);
		modelAndView.addObject(CMS_PAGE, pageModel);
	}

	private boolean checkRobotsMetaTagFor(final RobotsMetaTag robotsMetaTag)
	{
		boolean properMetaTagFound = false;
		String robotsMetaTagValue = WileyRobotsTagContentMapper.getContentForTag(robotsMetaTag);
		MetaElementData elementData = ((List<MetaElementData>) modelAndView.getModel().get(META_TAGS)).get(0);

		if (ROBOTS.equals(elementData.getName()) && robotsMetaTagValue.equals(elementData.getContent()))
		{
			properMetaTagFound = true;
		}

		return properMetaTagFound;
	}
}