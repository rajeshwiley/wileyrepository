package com.wiley.storefrontcommons.controllers.pages.payment;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.naming.OperationNotSupportedException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



import com.wiley.storefrontcommons.util.HttpServletRequestUtil;

import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_AUTH;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_KEY;
import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_VALIDATE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGPaymentResponseControllerTest
{
	private static final String AUTH_PAYMENT_RESPONSE_HANDLER = "forward:/checkout/multi/hop/response";
	private static final String VALIDATE_PAYMENT_RESPONSE_HANDLER = "forward:/my-account/hop/response";

	private WPGPaymentResponseController testInstance = new WPGPaymentResponseController();

	@Mock
	private Model mockModel;
	@Mock
	private HttpServletRequest mockRequest;
	@Mock
	private RedirectAttributes mockRedirectAttributes;
	@Mock
	private HttpServletRequestUtil mockHttpServletRequestUtil;

	@Before
	public void setUp()
	{
		testInstance.setValidatePaymentResponseHandler("/my-account/hop/response");
		testInstance.setHttpServletRequestUtil(mockHttpServletRequestUtil);
	}

	@Test
	public void shouldForwardToValidatePaymentResponseHandler() throws OperationNotSupportedException
	{
		when(mockRequest.getParameter(OPERATION_KEY)).thenReturn(OPERATION_VALIDATE);
		final String actualResult = testInstance.doHandleHopResponse(mockRequest);
		assertEquals(VALIDATE_PAYMENT_RESPONSE_HANDLER, actualResult);
	}

	@Test
	public void shouldForwardToAuthPaymentResponseHandler() throws OperationNotSupportedException
	{
		when(mockRequest.getParameter(OPERATION_KEY)).thenReturn(OPERATION_AUTH);
		final String actualResult = testInstance.doHandleHopResponse(mockRequest);
		assertEquals(AUTH_PAYMENT_RESPONSE_HANDLER, actualResult);
	}

}
