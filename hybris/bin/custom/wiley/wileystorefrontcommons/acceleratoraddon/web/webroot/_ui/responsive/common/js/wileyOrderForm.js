ACC.wileyOrderForm = {
    _autoload: [
        "bindWileyOrderFormEvents",
        "bindWileyCountrySpecificAddressForm"
    ],

    bindWileyOrderFormEvents: function () {
        $(document).on('click', '#submitWileyOrderForm', function (event) {
            event.preventDefault();
            if ($("#wileyPaymentForm").is(":visible")) {
                HostedSession.clearErrors();
                PaymentSession.updateSessionFromForm('card');
            } else {
                ACC.wileyOrderForm.getSuggestionAddresses();
            }
            $(this).prop('disabled', true);
        });
        $(document).on('click', '#suggestionAddressSubmitOrderEdit', function (event) {
            event.preventDefault();
            ACC.checkout.setSuggestedAddress($('#i18nAddressForm'), $(this));
            ACC.wileyOrderForm.hideOrderSuggestionAddressesPopup();
            ACC.wileyOrderForm.updateOrder();
	        $(this).prop('disabled', true);
        });
        $(document).on('click', '#confirmSubmitWileyOrderForm', function (event) {
            event.preventDefault();
            ACC.wileyOrderForm.updateOrderConfirm();
	        $(this).prop('disabled', true);
        });
    },

    bindWileyCountrySpecificAddressForm: function() {
        $(document).on("change selectmenuchange", '#countrySelector select', function() {
            var isoCode = $(this).val();
            var params = { countryIsoCode: isoCode };
            window.location.search = jQuery.param( params );
        })
    },

    getSuggestionAddresses: function (response) {
        if (response != undefined) {
            $("input[name='sessionId']").val(response.session.id);
        }

        var $wileyOrderForm = $('#wileyOrderForm');
        var wileyOrderFormJSON = ACC.wileyOrderForm.jsonifyWileyOrderForm($wileyOrderForm);
        var url = $wileyOrderForm.data("orderSuggestionAddressesUrl");

        $.ajax({
            url: url,
            data: wileyOrderFormJSON,
            type: "POST",
            contentType: "application/json",
            accept: "application/json",
            success: function (popupResult, statusText, xhr) {
                if (popupResult && popupResult.length) {
                    ACC.wileyOrderForm.displayOrderSuggestionAddressesPopup(popupResult);
                }
                else {
                    ACC.wileyOrderForm.updateOrder();
                }
            },
            error: function (orderFormErrorsResult, statusText, xhr) {
                ACC.wileyOrderForm.displayOrderErrors(orderFormErrorsResult)
            }
        });
    },

    updateOrder: function (response) {
        if (response != undefined) {
            $("input[name='sessionId']").val(response.session.id);
        }

        var $wileyOrderForm = $('#wileyOrderForm');
        var wileyOrderFormJSON = ACC.wileyOrderForm.jsonifyWileyOrderForm($wileyOrderForm);
        var url = $wileyOrderForm.data("orderUpdateUrl");

        $.ajax({
            url: url,
            data: wileyOrderFormJSON,
            type: "POST",
            contentType: "application/json",
            accept: "application/json",
            success: function (popupResult, statusText, xhr) {
                ACC.wileyOrderForm.displayOrderTotalsPopup(popupResult)
            },
            error: function (orderFormErrorsResult, statusText, xhr) {
                ACC.wileyOrderForm.displayOrderErrors(orderFormErrorsResult)
            }
        });
    },

    updateOrderConfirm: function () {
        var $wileyOrderForm = $('#wileyOrderForm');
        var wileyOrderFormJSON = ACC.wileyOrderForm.jsonifyWileyOrderForm($wileyOrderForm);
        var url = $wileyOrderForm.data("orderUpdateConfirmUrl");

        $.ajax({
            url: url,
            data: wileyOrderFormJSON,
            type: "POST",
            contentType: "application/json",
            accept: "application/json",
            success: function (updateResult) {
                window.location.replace(updateResult.redirectUrl);
            },
            error: function (orderFormErrorsResult, statusText, xhr) {
                if (orderFormErrorsResult.status == 403) {
                    window.location.replace(orderFormErrorsResult.responseJSON.redirectUrl);
                } else {
                    ACC.wileyOrderForm.displayOrderErrors(orderFormErrorsResult)
                }
            }
        });
    },

    jsonifyWileyOrderForm: function ($wileyOrderForm) {
        var wileyOrderFormData = {};
        ACC.wileyOrderForm.jsonifyForm(wileyOrderFormData, $('#addressForm'), "wileyAddressForm");
        ACC.wileyOrderForm.jsonifyForm(wileyOrderFormData, $('#wileyPaymentForm:visible'), "wileyPaymentForm");
        ACC.wileyOrderForm.jsonifyForm(wileyOrderFormData, $('#wileyInvoicePaymentForm:visible'), "wileyInvoicePaymentForm");
        ACC.wileyOrderForm.jsonifyForm(wileyOrderFormData, $('#wileyProformaPaymentForm:visible'), "wileyProformaPaymentForm");
        return JSON.stringify(wileyOrderFormData);
    },

    jsonifyForm: function (resultArray, $form, prefix) {
        var formArray = ACC.wileyOrderForm.getFormArray($form);
        var formObject = {};
        for (var i = 0; i < formArray.length; i++) {
            var name = formArray[i]['name'];
            var value = formArray[i]['value'];
            if (name != 'CSRFToken' && name.indexOf('_') === -1) {
                if (name.indexOf('.') !== -1) {
                    var names = name.split(".");
                    var nestedObject = formObject[names[0]];
                    if (nestedObject == undefined) {
                        nestedObject = {};
                    }
                    nestedObject[names[1]] = value;
                    formObject[names[0]] = nestedObject;
                } else {
                    formObject[name] = value;
                }
            }
        }
        if (!$.isEmptyObject(formObject)) {
            resultArray[prefix] = formObject;
        }
    },

    getFormArray: function ($form) {
        /* Get input values from form */
        var values = $form.serializeArray();

        /* Because serializeArray() ignores unset checkboxes and radio buttons: */
        values = values.concat(
            jQuery('input[type=checkbox]', $form).map(
                function () {
                    return {
                        "name": this.name,
                        "value": $(this).prop("checked")
                    }
                }).get()
        );
        return values;
    },

    displayOrderTotalsPopup: function (htmlResult) {
        $("#confirmOrderUpdateModal").html(htmlResult);
        $("#confirmOrderUpdateModal").modal('show');
    },

    displayOrderSuggestionAddressesPopup: function (htmlResult) {
        $("#suggestedAddressesModal").html(htmlResult);
        $("#suggestedAddressesModal").modal('show');
    },

    hideOrderSuggestionAddressesPopup: function () {
        $("#suggestedAddressesModal").modal('hide');
    },

    displayOrderErrors: function (orderFormErrorsResult) {
        $("#confirmOrderUpdateModal").modal('hide');
        if (orderFormErrorsResult.responseJSON != undefined) {
            if (orderFormErrorsResult.responseJSON.billingInformationStep != undefined) {
                $("#billingInformationStep").replaceWith(orderFormErrorsResult.responseJSON.billingInformationStep);
                ACC.address.showAddressFormButtonPanel();
                wiley.showDatePicker();
                ACC.address.toggleSubmitBtnBasedOnRequiredFields('#addressForm', '#submitWileyOrderForm');
                ACC.address.bindInteractivePatternValidators();
            }
            var globalMessagesHtml = orderFormErrorsResult.responseJSON.globalMessages;
            if (globalMessagesHtml != undefined) {
                if ($("#global-messages").length) {
                    $("#global-messages").append($(globalMessagesHtml).html());
                } else {
                    $("body").prepend($(globalMessagesHtml));
                }
                $("#global-messages").removeClass("hidden");
                wiley.setFlashMessage();
            }
        }
    }
}
