<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="render" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${render}">
	<style id="antiClickjack">body{display:none !important;}</style>
</c:if>