ACC.wileyPaymentForm = {
	_autoload: [
		"bindWileyPaymentFormEvents"
	],

	bindWileyPaymentFormEvents: function ()
	{
		$('#submitWileyPaymentForm').click(function(event) {
			ACC.wileyPaymentForm.paymentFormContinueEventHandling(event);
		});

		$('#wileyPaymentForm').on('keydown', 'input, select', function(event) {
            if(event.which == 13)  {
            	ACC.paymentForm.paymentFormContinueEventHandling(event);
            }
        });
	},

	paymentFormContinueEventHandling: function(event) {
    	event.preventDefault();
    	HostedSession.clearErrors();
    	ACC.common.disableButton('submitWileyPaymentForm');
    	PaymentSession.updateSessionFromForm('card');
    }
}