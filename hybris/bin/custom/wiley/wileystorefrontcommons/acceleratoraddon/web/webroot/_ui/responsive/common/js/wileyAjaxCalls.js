wiley.updateMultipleSectionsAjax = function (data, checkErrorCasesFunction) {
    var elements = $(data).find("element");

    $(elements).each(function(index) {
        var section = $(this);
        var sectionKey = section.find("key").html();
        var sectionValue = section.find("value");
        $("#" + sectionKey).replaceWith(sectionValue[0].innerText);
    });

    checkErrorCasesFunction.call();
}