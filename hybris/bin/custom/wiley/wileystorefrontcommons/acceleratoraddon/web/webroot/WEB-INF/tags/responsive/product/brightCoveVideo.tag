<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="video" required="true" type="com.wiley.facades.product.data.BrightCoveVideoData"%>

<%-- lazy owl attributes are added to avoid issue with carousel functionality --%>
<div class="
-container lazyOwl owl-loaded" id="${video.videoId}">
	<div style="display: block; position: relative; max-width: 100%;">
		<div style="padding-top: 56.25%;">
			<video data-video-id="${video.videoId}" data-account="${video.accountId}" data-player="${video.playerId}" data-embed="default" class="video-js" controls="" 
				style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
			</video>
    		<script src="//players.brightcove.net/${video.accountId}/${video.playerId}_default/index.min.js"></script>
    	</div>
    </div>
</div>