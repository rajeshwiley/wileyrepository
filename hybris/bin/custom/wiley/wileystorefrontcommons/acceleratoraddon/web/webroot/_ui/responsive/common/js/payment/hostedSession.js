var HostedSession = {
	errorMessages : '',
	validation : function(response){
		HostedSession.validateField(response.errors.cardNumber,'#card-number', HostedSession.errorMessages.paymentCardNumberInvalid);
		HostedSession.validateField(response.errors.expiryYear, '#expiry-year', HostedSession.errorMessages.expirationDateInvalid);
		HostedSession.validateField(response.errors.expiryMonth,'#expiry-month', HostedSession.errorMessages.expirationDateInvalid);
		HostedSession.validateField(response.errors.securityCode,'#security-code', HostedSession.errorMessages.securityCodeInvalid);
	},
	showGlobalAlert : function(message){
		$('#global-error-message-list').append(message);
		$("#global-alert").removeClass('hidden');
	},
	validateField : function(fieldName, fieldId, message){
       if (fieldName) {
			$(fieldId).addClass("has-error");
       		if (fieldId === "#card-number") {
				HostedSession.addUnderfieldError("cart-number", message);
       		} else if (fieldId === "#expiry-year") {
				HostedSession.addUnderfieldError("date-year", message);
       		} else if (fieldId === "#expiry-month") {
				HostedSession.addUnderfieldError("date-month", message);
       		} else if (fieldId === "#security-code") {
				HostedSession.addUnderfieldError("code", message);
       		} else {
       			HostedSession.showGlobalAlert(message);
       		}
    	}
    },
	submitForm : function(response, form){
		$("input[name='sessionId']").val(response.session.id);
        form.submit();
	},
	clearErrors : function(){
		$('#global-error-message-list').text("");
		$('[id*="\\.errors"]').each(function() {
			$(this).text('');
		});
		$('.has-error').each(function() {
			$(this).removeClass('has-error');
		});
		$("#global-alert").addClass("hidden");
		$("#global-alert").removeClass("fade");
		HostedSession.setInitErrorsState("cart-number");
		HostedSession.setInitErrorsState("date-month");
		HostedSession.setInitErrorsState("date-year");
		HostedSession.setInitErrorsState("code");
	},
	setInitErrorsState : function(fieldId){
		$("#block-alert-" + fieldId).addClass("hidden");
		$("#text-alert-" + fieldId).empty();
	},
	addUnderfieldError : function(fieldId, msg){
		$("#text-alert-" + fieldId).append(msg);
		$("#block-alert-" + fieldId).removeClass('hidden');
	}
};
