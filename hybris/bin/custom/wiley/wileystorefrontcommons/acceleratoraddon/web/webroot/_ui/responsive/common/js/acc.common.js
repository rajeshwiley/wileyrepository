ACC.common.disableButton = function(buttonId)
{
	$("#" + buttonId).prop("disabled",true);
};

ACC.common.enableButton = function(buttonId)
{
	$("#" + buttonId).prop("disabled",false);
};

