/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.storefrontcommons.context.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;


/**
 * Unit test for {@link WileyContextInformationLoaderImpl}
 *
 * @author Maksim_Kozich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyContextInformationLoaderImplTest
{
	private static final String SUPPORTED_LANG_CODE = "en_US";

	private static final Locale EN_US_LOCALE = new Locale("en", "US");

	private static final Locale EN_AU_LOCALE = new Locale("en", "AU");

	private static final Locale EN_GB_LOCALE = new Locale("en", "GB");

	private static final Set<Locale> SUPPORTED_LOCALES = Sets.newHashSet(EN_US_LOCALE, EN_AU_LOCALE, EN_GB_LOCALE);

	private static final String NOT_SUPPORTED_LANG_CODE = "en_BY";

	private static final Locale NOT_SUPPORTED_LOCALE = new Locale(NOT_SUPPORTED_LANG_CODE);

	@InjectMocks
	private WileyContextInformationLoaderImpl wileyContextInformationLoader;

	@Mock
	private I18NService i18NServiceMock;

	@Mock
	private LanguageModel languageModelMock;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		when(i18NServiceMock.getSupportedLocales()).thenReturn(SUPPORTED_LOCALES);
	}

	@Test
	public void testSupportedLanguage()
	{
		// given
		when(languageModelMock.getIsocode()).thenReturn(SUPPORTED_LANG_CODE);

		// when
		wileyContextInformationLoader.loadFakeLanguage(languageModelMock);

		// then
		verify(i18NServiceMock).setCurrentLocale(same(EN_US_LOCALE));
	}

	@Test
	public void testNotSupportedLanguage()
	{
		// given
		when(languageModelMock.getIsocode()).thenReturn(NOT_SUPPORTED_LANG_CODE);

		// when
		wileyContextInformationLoader.loadFakeLanguage(languageModelMock);

		// then
		verify(i18NServiceMock).setCurrentLocale(eq(NOT_SUPPORTED_LOCALE));
	}

	@Test
	public void testNullLanguage()
	{
		// given

		// when
		wileyContextInformationLoader.loadFakeLanguage(null);

		// then
		verify(i18NServiceMock, never()).getSupportedLocales();
		verify(i18NServiceMock, never()).setCurrentLocale(any());
	}
}
