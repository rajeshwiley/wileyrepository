package com.wiley.storefrontcommons.locale.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;

import static com.wiley.storefrontcommons.locale.impl.WileyLocaleFromGeolocationHandler.COUNTRY_HEADER_NAME;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileyLocaleFromGeolocationHandlerUnitTest
{
	private static final String COUNTRY_REDIRECT_URL = "http://random.site";
	private static final String VALID_COUNTRY_CODE = "US";
	private static final String VALID_ENCODED_LOCALE = "en_us";



	@Mock
	private HttpServletRequest request;
	@Mock
	private WileyLocaleService wileyLocaleService;
	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;
	@Mock
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@InjectMocks
	private WileyLocaleFromGeolocationHandler testInstance = new WileyLocaleFromGeolocationHandler();

	@Before
	public void setUp() throws Exception
	{
		when(wileycomStoreSessionFacade.getExternalSiteRedirect(VALID_COUNTRY_CODE)).thenReturn(Optional.empty());
	}

	@Test
	public void shouldSetEncodedLocaleIfRequestHeaderContainsValidCountry() {
		when(request.getHeader(COUNTRY_HEADER_NAME)).thenReturn(VALID_COUNTRY_CODE);
		CountryModel validCountry = givenCountry(VALID_COUNTRY_CODE);
		when(commerceCommonI18NService.getAllCountries())
				.thenReturn(
						Collections.singletonList(validCountry)
				);

		when(wileyLocaleService.getEncodedLocale(eq(validCountry), any())).thenReturn(VALID_COUNTRY_CODE);
		//When
		testInstance.init();
		//Then
		testInstance.getEncodedLocale().equals(VALID_ENCODED_LOCALE);
	}


	@Test
	public void encodedLocaleShouldBeEmptyIfRequestHeaderContainsCountryWithRedirectToExternalSite() {
		when(wileycomStoreSessionFacade.getExternalSiteRedirect(VALID_COUNTRY_CODE))
				.thenReturn(Optional.of(COUNTRY_REDIRECT_URL));

		when(request.getHeader(COUNTRY_HEADER_NAME)).thenReturn(VALID_COUNTRY_CODE);
		CountryModel validCountry = givenCountry(VALID_COUNTRY_CODE);
		when(commerceCommonI18NService.getAllCountries())
				.thenReturn(
						Collections.singletonList(validCountry)
				);

		when(wileyLocaleService.getEncodedLocale(eq(validCountry), any())).thenReturn(VALID_COUNTRY_CODE);
		//When
		testInstance.init();
		//Then
		assertEquals(Optional.empty(), testInstance.getEncodedLocale());
	}

	@Test
	public void encodedLocaleShouldBeEmptyIfRequestHeaderCountryIsNotValid() {
		when(request.getHeader(COUNTRY_HEADER_NAME)).thenReturn(VALID_COUNTRY_CODE);
		when(commerceCommonI18NService.getAllCountries())
				.thenReturn(
						Collections.emptyList()
				);
		//When
		testInstance.init();
		//Then
		assertEquals(Optional.empty(), testInstance.getEncodedLocale());
	}

	@Test
	public void encodedLocaleShouldBeEmptyIfRequestHeaderMissed() {
		when(request.getHeader(COUNTRY_HEADER_NAME)).thenReturn(null);
		//When
		testInstance.init();
		//Then
		assertEquals(Optional.empty(), testInstance.getEncodedLocale());
	}

	private CountryModel givenCountry(final String code) {
		CountryModel country = mock(CountryModel.class);
		when(country.getIsocode()).thenReturn(code);
		return country;
	}

}