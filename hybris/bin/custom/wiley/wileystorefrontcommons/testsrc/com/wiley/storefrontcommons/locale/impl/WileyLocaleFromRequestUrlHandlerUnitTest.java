package com.wiley.storefrontcommons.locale.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.locale.WileyLocaleService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLocaleFromRequestUrlHandlerUnitTest
{
	private static final String VALID_ENCODED_LOCALE_VALUE = "en-us";
	private static final String INVALID_ENCODED_LOCALE_VALUE = "blabla";
	private static final String CONTEXT_PATH = "/wileyb2cstorefront";
	private static final String CONTEXT_PATH_WITH_ENCODED_LOCALE = CONTEXT_PATH + "/" + VALID_ENCODED_LOCALE_VALUE;
	private static final String CONTEXT_PATH_WITH_INVALID_ENCODED_LOCALE = CONTEXT_PATH + "/" + INVALID_ENCODED_LOCALE_VALUE;
	private static final String CONTEXT_PATH_WITHOUT_ENCODED_LOCALE = CONTEXT_PATH;
	private static final Collection<String> ENCODING_ATTRIBUTES_WITH_LANGUAGE = Arrays.asList("language");
	private static final Collection<String> ENCODING_ATTRIBUTES_WITHOUT_LANGUAGE = Arrays.asList("currency");


	@InjectMocks
	private WileyLocaleFromRequestUrlHandler testInstance;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private UrlEncoderService mockUrlEncoderService;
	@Mock
	private WileyLocaleService mockWileyLocaleService;

	@Before
	public void setUp()
	{
		when(mockRequest.getContextPath()).thenReturn(CONTEXT_PATH);
		when(mockRequest.getRequestURI()).thenReturn(CONTEXT_PATH_WITH_ENCODED_LOCALE);
		when(mockUrlEncoderService.getEncodingAttributesForSite()).thenReturn(ENCODING_ATTRIBUTES_WITH_LANGUAGE);
		when(mockWileyLocaleService.isValidEncodedLocale(VALID_ENCODED_LOCALE_VALUE)).thenReturn(true);
		when(mockWileyLocaleService.isValidEncodedLocale(INVALID_ENCODED_LOCALE_VALUE)).thenReturn(false);
	}

	@Test
	public void shouldInitializeEncodedLocaleIfItPresentedInUrl()
	{
		testInstance.init();

		final Optional<String> encodedLocale = testInstance.getEncodedLocale();
		assertTrue(encodedLocale.isPresent());
		assertEquals(VALID_ENCODED_LOCALE_VALUE, encodedLocale.get());
	}


	@Test
	public void shouldInitializeEncodedLocaleWithEmptyValueIfItNotPresentedInUrl()
	{
		when(mockRequest.getRequestURI()).thenReturn(CONTEXT_PATH_WITHOUT_ENCODED_LOCALE);

		testInstance.init();

		final Optional<String> encodedLocale = testInstance.getEncodedLocale();
		assertFalse(encodedLocale.isPresent());
	}

	@Test
	public void shouldInitializeEncodedLocaleWithEmptyValueIfItInvalidInUrl()
	{
		when(mockRequest.getRequestURI()).thenReturn(CONTEXT_PATH_WITH_INVALID_ENCODED_LOCALE);

		testInstance.init();

		final Optional<String> encodedLocale = testInstance.getEncodedLocale();
		assertFalse(encodedLocale.isPresent());
	}

	@Test
	public void shouldInitializeEncodedLocaleWithEmptyValueIfLanguageEncodingAttrDisabled()
	{
		when(mockUrlEncoderService.getEncodingAttributesForSite()).thenReturn(ENCODING_ATTRIBUTES_WITHOUT_LANGUAGE);

		testInstance.init();

		final Optional<String> encodedLocale = testInstance.getEncodedLocale();
		assertFalse(encodedLocale.isPresent());
	}
}
