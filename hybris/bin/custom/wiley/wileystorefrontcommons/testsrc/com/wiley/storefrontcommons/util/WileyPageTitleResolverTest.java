package com.wiley.storefrontcommons.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit tests for WileyPageTitleResolver.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPageTitleResolverTest
{
	private static final String TITLE_WORD_SEPARATOR = " | ";
	private static final String SEO_TITLE = "seo title";
	private static final String PRODUCT_NAME = "product name";
	private static final String CATEGORY_NAME = "categoryName";
	private static final String SITE_NAME = "siteName";
	@Mock
	private CMSSiteService mockCmsSiteService;

	@Mock
	private ProductModel mockProductModel;

	@Mock
	private CategoryModel mockcategoryModel;

	@Mock
	private CMSSiteService cmsSiteService;

	@Mock
	private CMSSiteModel cmsSiteModel;

	@Mock
	private CommerceCategoryService commerceCategoryService;

	@InjectMocks
	private WileyPageTitleResolver testedInstance = new WileyPageTitleResolver();

	@Before
	public void setup()
	{
		when(mockProductModel.getName()).thenReturn(PRODUCT_NAME);
	}

	@Test
	public void shouldReturnLocalizedSeoPageTitleIfItIsSetForTheProduct()
	{
		when(mockProductModel.getSeoTitleTag()).thenReturn(SEO_TITLE);
		String pageTitle = testedInstance.resolveProductPageTitle(mockProductModel);
		assertEquals("Should return SEO title if it is set.", SEO_TITLE, pageTitle);
	}

	@Test
	public void shouldReturnOOTBPageTitleIfSeoTitleIsNotSetForTheProduct()
	{
		when(mockProductModel.getSeoTitleTag()).thenReturn(null);
		String pageTitle = testedInstance.resolveProductPageTitle(mockProductModel);
		assertEquals("Should return OOTB page title if SEO title is not set for the product.", PRODUCT_NAME, pageTitle);
	}

	@Test
	public void shouldReturnLocalizedSeoPageTitleIfItIsSetForTheCategory()
	{
		//given
		when(mockcategoryModel.getSeoTitleTag()).thenReturn(SEO_TITLE);
		//when
		String pageTitle = testedInstance.resolveCategoryPageTitle(mockcategoryModel);
		//then
		assertEquals("Should return SEO title if it is set.", SEO_TITLE, pageTitle);
	}


	@Test
	public void shouldReturnOOTBPageTitleIfSeoTitleIsNotSetForCategory()
	{
		//given
		final List<CategoryModel> categoryList = new ArrayList<>();
		final CategoryModel categoryModel = Mockito.mock(CategoryModel.class);
		categoryList.add(mockcategoryModel);
		when(commerceCategoryService.getPathsForCategory(mockcategoryModel))
				.thenReturn(Collections.singletonList(categoryList));
		when(mockcategoryModel.getSeoTitleTag()).thenReturn(null);
		when(mockcategoryModel.getName()).thenReturn(CATEGORY_NAME);
		when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteModel);
		when(cmsSiteModel.getName()).thenReturn(SITE_NAME);
		//when
		String pageTitle = testedInstance.resolveCategoryPageTitle(mockcategoryModel);
		//then
		assertEquals("Should return OOTB page title if SEO title is not set for the category.",
				CATEGORY_NAME + TITLE_WORD_SEPARATOR + SITE_NAME, pageTitle);

	}


}
