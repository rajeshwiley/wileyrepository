package com.wiley.storefrontcommons.locale.impl;

import com.wiley.core.model.WileyExternalSiteLinkModel;
import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.storefrontcommons.security.cookie.EnhancedCookieGenerator;


/**
 * Created by Maksim_Kozich on 22.06.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLocaleFromCookieHandlerTest
{
	private static final Cookie[] COOKIES = new Cookie[2];
	private static final String COOKIE_VALUE = "cookie value";
	private static final Optional<String> COOKIE_VALUE_OPTIONAL = Optional.of(COOKIE_VALUE);
	private static final Optional<String> EMPTY_COOKIE_VALUE_OPTIONAL = Optional.empty();
	private static final boolean COOKIE_VALUE_VALID = true;
	private static final boolean COOKIE_VALUE_NOT_VALID = false;

	@Mock
	private HttpServletRequest requestMock;

	@Mock
	private EnhancedCookieGenerator countryCookieGeneratorMock;

	@Mock
	private WileyLocaleService wileyLocaleServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private WileyExternalSiteLinkModel wileyExternalSiteLinkModelMock;

	@InjectMocks
	private WileyLocaleFromCookieHandler localeFromCookieHandler;

	private Set<WileyExternalSiteLinkModel> externalSiteLinkModels;
	private Set<WileyExternalSiteLinkModel> emptyExternalSiteLinkModels;

	@Before
	public void setUp()
	{
		emptyExternalSiteLinkModels = new HashSet<>();
		externalSiteLinkModels = new HashSet<>();
		externalSiteLinkModels.add(wileyExternalSiteLinkModelMock);

		when(requestMock.getCookies()).thenReturn(COOKIES);
		when(countryCookieGeneratorMock.findValueByName(COOKIES)).thenReturn(COOKIE_VALUE_OPTIONAL);
	}

	@Test
	public void shouldNotPopulateLocaleWhenCookieIsNotPresent()
	{
		//given
		when(countryCookieGeneratorMock.findValueByName(COOKIES)).thenReturn(EMPTY_COOKIE_VALUE_OPTIONAL);

		//when
		localeFromCookieHandler.init();

		//then
		verify(wileyLocaleServiceMock, never()).isValidEncodedLocale(any());
		Assert.assertFalse(localeFromCookieHandler.getEncodedLocale().isPresent());
	}

	@Test
	public void shouldNotPopulateLocaleWhenCookieIsPresentAndLocaleIsNotValid()
	{
		//given
		when(wileyLocaleServiceMock.isValidEncodedLocale(COOKIE_VALUE)).thenReturn(COOKIE_VALUE_NOT_VALID);

		//when
		localeFromCookieHandler.init();

		//then
		Assert.assertFalse(localeFromCookieHandler.getEncodedLocale().isPresent());
	}

	@Test
	public void shouldPopulateLocaleWhenCookieIsPresentAndLocaleIsValidAndIsNotTiedToAnyExternalSite()
	{
		//given
		when(wileyLocaleServiceMock.isValidEncodedLocale(COOKIE_VALUE)).thenReturn(COOKIE_VALUE_VALID);
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
		when(baseStoreModelMock.getExternalLinks()).thenReturn(emptyExternalSiteLinkModels);
		when(wileyLocaleServiceMock.getCountryForEncodedLocale(anyString())).thenReturn(countryModelMock);

		//when
		localeFromCookieHandler.init();

		//then
		Assert.assertTrue(localeFromCookieHandler.getEncodedLocale().isPresent());
		Assert.assertEquals(localeFromCookieHandler.getEncodedLocale().get(), COOKIE_VALUE);
	}

	@Test
	public void shouldNotPopulateLocaleWhenCookieIsPresentAndLocaleIsValidAndIsTiedToExternalSite()
	{
		//given
		when(wileyLocaleServiceMock.isValidEncodedLocale(COOKIE_VALUE)).thenReturn(COOKIE_VALUE_VALID);
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
		when(baseStoreModelMock.getExternalLinks()).thenReturn(externalSiteLinkModels);
		when(wileyLocaleServiceMock.getCountryForEncodedLocale(anyString())).thenReturn(countryModelMock);
		when(wileyExternalSiteLinkModelMock.getCountry()).thenReturn(countryModelMock);

		//when
		localeFromCookieHandler.init();

		//then
		Assert.assertFalse(localeFromCookieHandler.getEncodedLocale().isPresent());
	}
}
