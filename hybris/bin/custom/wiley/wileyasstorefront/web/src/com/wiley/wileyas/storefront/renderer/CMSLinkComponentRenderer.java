/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.renderer;

import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;

import com.wiley.storefrontcommons.renderer.WileyCMSLinkComponentRenderer;


/**
 * The type Cms link component renderer.
 */
public class CMSLinkComponentRenderer extends WileyCMSLinkComponentRenderer
{
	private static final Logger LOG = Logger.getLogger(CMSLinkComponentRenderer.class);

	@Override
	public void renderComponent(final PageContext pageContext, final CMSLinkComponentModel component) throws IOException
	{
		try
		{
			final String url = getUrl(component);
			final String encodedUrl = UrlSupport.resolveUrl(url, null, pageContext);

			final JspWriter out = pageContext.getOut();

			if (StringUtils.isNotBlank(component.getLinkName()) && StringUtils.isBlank(encodedUrl))
			{
				// <span class="empty-nav-item">${component.linkName}</span>
				out.write("<span class=\"empty-nav-item\">");
				out.write(component.getLinkName());
				out.write("</span>");
			}
			else
			{
				// <a href="${encodedUrl}" ${component.styleAttributes} title="${component.linkName}"
				// ${component.target == null || component.target == 'SAMEWINDOW' ?
				// '' : 'target="_blank"'}>${component.linkName}</a>

				out.write("<a href=\"");
				out.write(encodedUrl);
				out.write("\" ");

				// Write additional attributes onto the link
				if (component.getStyleAttributes() != null)
				{
					out.write(component.getStyleAttributes());
				}

				if (StringUtils.isNotBlank(component.getLinkName()))
				{
					out.write(" title=\"");
					out.write(component.getLinkName());
					out.write("\" ");
				}

				if (component.getTarget() != null && !LinkTargets.SAMEWINDOW.equals(component.getTarget()))
				{
					out.write(" target=\"_blank\"");
				}
				out.write(">");
				if (StringUtils.isNotBlank(component.getLinkName()))
				{
					out.write(component.getLinkName());
				}
				out.write("</a>");
			}
		}
		catch (final JspException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
		}
	}
}
