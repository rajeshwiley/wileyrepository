/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.controllers;

import de.hybris.platform.acceleratorcms.model.components.CartSuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.DynamicBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorcms.model.components.PurchasedCategorySuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SubCategoryListComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;


/**
 */
public interface ControllerConstants
{
	// Constant names cannot be changed due to their usage in dependant extensions, thus nosonar

	/**
	 * Class with action name constants
	 */
	interface Actions
	{
		interface Cms // NOSONAR
		{
			String PREFIX = "/view/"; // NOSONAR
			String SUFFIX = "Controller"; // NOSONAR

			/**
			 * Default CMS component controller
			 */
			String DEFAULT_CMS_COMPONENT = PREFIX + "DefaultCMSComponentController"; // NOSONAR

			/**
			 * CMS components that have specific handlers
			 */
			String PURCHASED_CATEGORY_SUGGESTION_COMPONENT = PREFIX + PurchasedCategorySuggestionComponentModel._TYPECODE
					+ SUFFIX;
			// NOSONAR
			String CART_SUGGESTION_COMPONENT = PREFIX + CartSuggestionComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String PRODUCT_REFERENCES_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String PRODUCT_FEATURE_COMPONENT = PREFIX + ProductFeatureComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String CATEGORY_FEATURE_COMPONENT = PREFIX + CategoryFeatureComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String NAVIGATION_BAR_COMPONENT = PREFIX + NavigationBarComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String CMS_LINK_COMPONENT = PREFIX + CMSLinkComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String DYNAMIC_BANNER_COMPONENT = PREFIX + DynamicBannerComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String SUB_CATEGORY_LIST_COMPONENT = PREFIX + SubCategoryListComponentModel._TYPECODE + SUFFIX; // NOSONAR
			String SIMPLE_RESPONSIVE_BANNER_COMPONENT = PREFIX + SimpleResponsiveBannerComponentModel._TYPECODE + SUFFIX;
			// NOSONAR
		}
	}

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Cms // NOSONAR
		{
			String COMPONENT_PREFIX = "cms/"; // NOSONAR
		}

		interface Pages
		{
			interface Account // NOSONAR
			{
				String ACCOUNT_LOGIN_PAGE = "pages/account/accountLoginPage"; // NOSONAR
				String ACCOUNT_HOME_PAGE = "pages/account/accountHomePage"; // NOSONAR
				String ACCOUNT_ORDER_HISTORY_PAGE = "pages/account/accountOrderHistoryPage"; // NOSONAR
				String ACCOUNT_ORDER_PAGE = "pages/account/accountOrderPage"; // NOSONAR
				String ACCOUNT_PROFILE_PAGE = "pages/account/accountProfilePage"; // NOSONAR
				String ACCOUNT_PROFILE_EDIT_PAGE = "pages/account/accountProfileEditPage"; // NOSONAR
				String ACCOUNT_PROFILE_EMAIL_EDIT_PAGE = "pages/account/accountProfileEmailEditPage"; // NOSONAR
				String ACCOUNT_CHANGE_PASSWORD_PAGE = "pages/account/accountChangePasswordPage"; // NOSONAR
				String ACCOUNT_ADDRESS_BOOK_PAGE = "pages/account/accountAddressBookPage"; // NOSONAR
				String ACCOUNT_EDIT_ADDRESS_PAGE = "pages/account/accountEditAddressPage"; // NOSONAR
				String ACCOUNT_PAYMENT_INFO_PAGE = "pages/account/accountPaymentInfoPage"; // NOSONAR
				String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage"; // NOSONAR
			}

			interface Checkout // NOSONAR
			{
				String CHECKOUT_REGISTER_PAGE = "pages/checkout/checkoutRegisterPage"; // NOSONAR
				String CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/checkoutConfirmationPageNoPayPal"; // NOSONAR
				String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLoginPage"; // NOSONAR
			}

			interface MultiStepCheckout // NOSONAR
			{
				String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage"; // NOSONAR
				String CHOOSE_DELIVERY_METHOD_PAGE = "pages/checkout/multi/chooseDeliveryMethodPage"; // NOSONAR
				String CHOOSE_PICKUP_LOCATION_PAGE = "pages/checkout/multi/choosePickupLocationPage"; // NOSONAR
				String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage"; // NOSONAR
				String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPageNoPayPal"; // NOSONAR
				String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage"; // NOSONAR
				String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage"; // NOSONAR
				String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage"; // NOSONAR
				String GIFT_WRAP_PAGE = "pages/checkout/multi/giftWrapPage"; // NOSONAR
			}

			interface Password // NOSONAR
			{
				String PASSWORD_RESET_CHANGE_PAGE = "pages/password/passwordResetChangePage"; // NOSONAR
				String PASSWORD_RESET_REQUEST = "pages/password/passwordResetRequestPage"; // NOSONAR
				String PASSWORD_RESET_REQUEST_CONFIRMATION = "pages/password/passwordResetRequestConfirmationPage"; // NOSONAR
			}

			interface Error // NOSONAR
			{
				String ERROR_NOT_FOUND_PAGE = "pages/error/errorNotFoundPage"; // NOSONAR
			}

			interface Cart // NOSONAR
			{
				String CART_PAGE = "pages/cart/cartPage"; // NOSONAR
			}

			interface StoreFinder // NOSONAR
			{
				String STORE_FINDER_SEARCH_PAGE = "pages/storeFinder/storeFinderSearchPage"; // NOSONAR
				String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage"; // NOSONAR
				String STORE_FINDER_VIEW_MAP_PAGE = "pages/storeFinder/storeFinderViewMapPage"; // NOSONAR
			}

			interface Misc // NOSONAR
			{
				String MISC_ROBOTS_PAGE = "pages/misc/miscRobotsPage"; // NOSONAR
				String MISC_SITE_MAP_PAGE = "pages/misc/miscSiteMapPage"; // NOSONAR
			}

			interface Guest // NOSONAR
			{ // NOSONAR
				String GUEST_ORDER_PAGE = "pages/guest/guestOrderPage"; // NOSONAR
				String GUEST_ORDER_ERROR_PAGE = "pages/guest/guestOrderErrorPage"; // NOSONAR
			}

			interface Product // NOSONAR
			{
				String WRITE_REVIEW = "pages/product/writeReview"; // NOSONAR
				String ORDER_FORM = "pages/product/productOrderFormPage"; // NOSONAR
			}

			interface QuickOrder // NOSONAR
			{
				String QUICK_ORDER_PAGE = "pages/quickOrder/quickOrderPage"; // NOSONAR
			}

			interface CSV // NOSONAR
			{
				String IMPORT_CSV_SAVED_CART_PAGE = "pages/csv/importCSVSavedCartPage"; // NOSONAR
			}
		}

		interface Fragments
		{
			interface Cart // NOSONAR
			{
				String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup"; // NOSONAR
				String MINI_CART_PANEL = "fragments/cart/miniCartPanel"; // NOSONAR
				String MINI_CART_ERROR_PANEL = "fragments/cart/miniCartErrorPanel"; // NOSONAR
				String CART_POPUP = "fragments/cart/cartPopup"; // NOSONAR
				String EXPAND_GRID_IN_CART = "fragments/cart/expandGridInCart"; // NOSONAR
			}

			interface Account // NOSONAR
			{
				String COUNTRY_ADDRESS_FORM = "fragments/address/countryAddressForm"; // NOSONAR
				String SAVED_CART_RESTORE_POPUP = "fragments/account/savedCartRestorePopup"; // NOSONAR
				String ORDER_TOTALS_POPUP = "fragments/account/orderTotalsPopup"; // NOSONAR
				String ORDER_SUGGESTION_POPUP = "fragments/account/orderEditingSuggestionAddressesPopup";
				String ORDER_EDITING_ERROR_RESPONSE = "fragments/account/orderEditingErrorResponse"; // NOSONAR
				String ORDER_EDITING_REDIRECT_RESPONSE = "fragments/account/orderEditingRedirectResponse"; // NOSONAR
			}

			interface Checkout // NOSONAR
			{
				String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup"; // NOSONAR
				String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm"; // NOSONAR
				String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm"; // NOSONAR
			}

			interface Password // NOSONAR
			{
				String PASSWORD_RESET_REQUEST_POPUP = "fragments/password/passwordResetRequestPopup"; // NOSONAR
				String FORGOT_PASSWORD_VALIDATION_MESSAGE = "fragments/password/forgotPasswordValidationMessage"; // NOSONAR
			}

			interface Product // NOSONAR
			{
				String FUTURE_STOCK_POPUP = "fragments/product/futureStockPopup"; // NOSONAR
				String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup"; // NOSONAR
				String ZOOM_IMAGES_POPUP = "fragments/product/zoomImagesPopup"; // NOSONAR
				String REVIEWS_TAB = "fragments/product/reviewsTab"; // NOSONAR
				String STORE_PICKUP_SEARCH_RESULTS = "fragments/product/storePickupSearchResults"; // NOSONAR
			}
		}
	}

	/**
	 * The type Keys.
	 */
	interface Keys
	{
		String UPDATE_ORDER_NONEDITABLE_ERROR_MESSAGE = "update.order.noneditable.error.message";
		String ASM_UPDATE_ORDER_NONEDITABLE_ERROR_MESSAGE = "asm.update.order.noneditable.error.message";
		String UPDATE_ORDER_SUCCESS_MESSAGE = "update.order.success.message";
		String MESSAGE_KEY_AS_CHECKOUT_ISSUE_MESSAGE = "mpgs.as.checkout.result.issue";
	}
}
