package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;


public class WileyAddressFormTaxDetailsPopulator implements Populator<AbstractOrderData, WileyAddressForm>
{
	@Resource(name = "wileyasTaxExemptionDetailsFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyTaxExemptionDetailsForm> wileyasTaxExemptionDetailsFormConverter;

	@Resource(name = "wileyasVatRegistrationDetailsFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyVatRegistrationDetailsForm>
			wileyasVatRegistrationDetailsFormConverter;

	@Override
	public void populate(final AbstractOrderData abstractOrderData, final WileyAddressForm wileyAddressForm)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("abstractOrderData", abstractOrderData);
		ServicesUtil.validateParameterNotNullStandardMessage("wileyAddressForm", wileyAddressForm);

		if (shouldPopulateTaxDetails(abstractOrderData))
		{
			switch (abstractOrderData.getPaymentAddress().getCountry().getApplicableTaxExemption())
			{
				case TAX:
					WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsForm =
							getWileyasTaxExemptionDetailsFormConverter().convert(abstractOrderData);
					wileyAddressForm.setTaxExemptionDetailsForm(wileyTaxExemptionDetailsForm);
					break;
				case VAT:
					WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsForm =
							getWileyasVatRegistrationDetailsFormConverter().convert(abstractOrderData);
					wileyAddressForm.setVatRegistrationDetailsForm(wileyVatRegistrationDetailsForm);
					break;
				default:
			}
		}
	}

	protected boolean shouldPopulateTaxDetails(final AbstractOrderData abstractOrderData)
	{
		boolean result = abstractOrderData.getPaymentAddress() != null
				&& abstractOrderData.getPaymentAddress().getCountry() != null
				&& abstractOrderData.getPaymentAddress().getCountry().getApplicableTaxExemption() != null;
		return result;
	}

	//CHECKSTYLE:OFF
	protected AbstractPopulatingConverter<AbstractOrderData, WileyTaxExemptionDetailsForm> getWileyasTaxExemptionDetailsFormConverter()
	{
		return wileyasTaxExemptionDetailsFormConverter;
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyVatRegistrationDetailsForm> getWileyasVatRegistrationDetailsFormConverter()
	{
		return wileyasVatRegistrationDetailsFormConverter;
	}
	//CHECKSTYLE:ON
}
