package com.wiley.wileyas.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.impl.CachingCMSComponentRenderer;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.servicelayer.exceptions.AttributeNotSupportedException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;


/**
 * Overridden OOTB Assisted Service component renderer.
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasAssistedServiceComponentRenderer extends CachingCMSComponentRenderer
{
	private static final Logger LOG = Logger.getLogger(WileyasAssistedServiceComponentRenderer.class);

	@Resource
	private AssistedServiceFacade assistedServiceFacade;

	@Resource
	private CMSComponentService cmsComponentService;

	@Resource
	private ModelService modelService;

	@Override
	public void renderComponent(final PageContext pageContext, final AbstractCMSComponentModel component)
			throws ServletException, IOException
	{
		boolean asmSessionStatus = assistedServiceFacade.isAssistedServiceModeLaunched();

		// render component only when it's necessary
		if (asmSessionStatus || assistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			final Map<String, Object> variables = getVariablesToExpose(component);
			if (variables != null)
			{
				for (final Map.Entry<String, Object> entry : variables.entrySet())
				{
					pageContext.setAttribute("wiley_as_" + entry.getKey(), entry.getValue(),
							PageContext.REQUEST_SCOPE);
				}
			}
			super.renderComponent(pageContext, component);
		}
	}

	private Map<String, Object> getVariablesToExpose(final AbstractCMSComponentModel component)
	{
		final Map<String, Object> exposedVariables = new HashMap<>();
		for (final String property : cmsComponentService.getEditorProperties(component))
		{
			try
			{
				final Object value = modelService.getAttributeValue(component, property);
				exposedVariables.put(property, value);
			}
			catch (final AttributeNotSupportedException e)
			{
				LOG.debug(e.getMessage(), e);
			}
		}
		exposedVariables.putAll(assistedServiceFacade.getAssistedServiceSessionAttributes());
		return exposedVariables;
	}
}