/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.url;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.ProductPageModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;

import java.util.Map;

import com.wiley.storefrontcommons.url.DefaultPreviewDataModelUrlResolver;


/**
 * Responsible for generating correct URL for given page.
 */
public class WileyasDefaultPreviewDataModelUrlResolver extends DefaultPreviewDataModelUrlResolver
{
	/**
	 * Returns the relative URL for the specified page <code>page</code>.
	 *
	 * @return relative URL for the specified page
	 */
	@Override
	public String resolve(final PreviewDataModel previewDataModel)
	{
		if (previewDataModel != null)
		{
			final AbstractPageModel page = previewDataModel.getPage();

			final String url = processPage(page, getPageMapping());
			if (url != null)
			{
				return url;
			}

			if (page instanceof ContentPageModel)
			{
				// Construct URL to preview the Page by UID
				return "/preview-content?uid=" + page.getUid();
			}

			if (page instanceof CategoryPageModel)
			{
				return getCategoryModelUrlResolver().resolve(getPreviewValueForCategoryPage(previewDataModel));
			}

			if (page instanceof ProductPageModel)
			{
				return getProductModelUrlResolver().resolve(getPreviewValueForProductPage(previewDataModel));
			}
		}
		return "/";
	}

	protected String processPage(final AbstractPageModel page, final Map<String, String> pageMapping)
	{
		if (pageMapping != null && page != null)
		{
			final String urlForUid = checkPageUid(page, pageMapping);
			if (urlForUid != null)
			{
				return urlForUid;
			}

			final String urlForLabel = checkPageLabel(page, pageMapping);
			if (urlForLabel != null)
			{
				return urlForLabel;
			}
		}
		return null;
	}

	protected String checkPageLabel(final AbstractPageModel page, final Map<String, String> pageMapping)
	{
		// For ContentPages also lookup by label
		if (page instanceof ContentPageModel)
		{
			final String pageLabel = ((ContentPageModel) page).getLabel();
			if (pageLabel != null)
			{
				final String url = pageMapping.get(pageLabel);
				if (url != null && !url.isEmpty())
				{
					return url;
				}
			}
		}
		return null;
	}

	protected String checkPageUid(final AbstractPageModel page, final Map<String, String> pageMapping)
	{
		// Lookup the page mapping by page UID
		final String pageUid = page.getUid();
		if (pageUid != null)
		{
			final String url = pageMapping.get(pageUid);
			if (url != null && !url.isEmpty())
			{
				return url;
			}
		}
		return null;
	}
}
