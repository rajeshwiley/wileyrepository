package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyInvoicePaymentForm;
import com.wiley.storefrontcommons.forms.WileyOrderForm;
import com.wiley.storefrontcommons.forms.WileyPaymentForm;


public class WileyOrderFormPopulator implements Populator<AbstractOrderData, WileyOrderForm>
{
	@Resource(name = "wileyasAddressFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyAddressForm> wileyasAddressFormConverter;

	@Resource(name = "wileyasPaymentFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyPaymentForm> wileyasPaymentFormConverter;

	@Resource(name = "wileyasInvoicePaymentFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyInvoicePaymentForm> wileyasInvoicePaymentFormConverter;

	@Resource(name = "wileyasProformaPaymentFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyInvoicePaymentForm> wileyasProformaPaymentFormConverter;

	@Override
	public void populate(final AbstractOrderData abstractOrderData, final WileyOrderForm wileyOrderForm)
			throws ConversionException
	{
		WileyAddressForm wileyAddressForm = getWileyasAddressFormConverter().convert(abstractOrderData);
		wileyOrderForm.setWileyAddressForm(wileyAddressForm);

		WileyPaymentForm wileyPaymentForm = getWileyasPaymentFormConverter().convert(abstractOrderData);
		wileyOrderForm.setWileyPaymentForm(wileyPaymentForm);

		WileyInvoicePaymentForm wileyInvoicePaymentForm = getWileyasInvoicePaymentFormConverter().convert(abstractOrderData);
		wileyOrderForm.setWileyInvoicePaymentForm(wileyInvoicePaymentForm);

		WileyInvoicePaymentForm wileyProformaPaymentForm = getWileyasProformaPaymentFormConverter().convert(abstractOrderData);
		wileyOrderForm.setWileyProformaPaymentForm(wileyProformaPaymentForm);
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyAddressForm> getWileyasAddressFormConverter()
	{
		return wileyasAddressFormConverter;
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyPaymentForm> getWileyasPaymentFormConverter()
	{
		return wileyasPaymentFormConverter;
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyInvoicePaymentForm> getWileyasInvoicePaymentFormConverter()
	{
		return wileyasInvoicePaymentFormConverter;
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyInvoicePaymentForm> getWileyasProformaPaymentFormConverter()
	{
		return wileyasProformaPaymentFormConverter;
	}
}