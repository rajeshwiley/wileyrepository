package com.wiley.wileyas.storefront.errorHandler;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ControllerAdvice;

import com.wiley.storefrontcommons.errorHandlers.WileyGlobalExceptionHandler;
import com.wiley.storefrontcommons.web.view.WileyUiExperienceViewResolver;


@ControllerAdvice
public class WileyasGlobalExceptionHandler extends WileyGlobalExceptionHandler
{
	@Resource(name = "uiExperienceService")
	private UiExperienceService uiExperienceService;

	@Resource(name = "viewResolver")
	private WileyUiExperienceViewResolver uiExperienceViewResolver;

	@Value("${wileyas.base.url}")
	private String baseUrl;

	public String getCommonResourcePath(final HttpServletRequest request)
	{
		final Object urlEncodingAttributes = request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
		final String contextPath = StringUtils.remove(request.getContextPath(),
				(urlEncodingAttributes != null) ? urlEncodingAttributes.toString() : "");
		final String uiExperienceCode = uiExperienceService.getUiExperienceLevel().getCode();
		final String uiExperienceCodeLower = uiExperienceViewResolver.getUiExperienceViewPrefix().isEmpty() ? uiExperienceCode
				.toLowerCase() : StringUtils.remove(
				uiExperienceViewResolver.getUiExperienceViewPrefix().get(uiExperienceService.getUiExperienceLevel()), "/");
		final String siteRootUrl = contextPath + "/_ui/" + uiExperienceCodeLower;
		request.setAttribute("wileyasBaseUrl", baseUrl);
		return siteRootUrl + "/" + COMMON;
	}
}
