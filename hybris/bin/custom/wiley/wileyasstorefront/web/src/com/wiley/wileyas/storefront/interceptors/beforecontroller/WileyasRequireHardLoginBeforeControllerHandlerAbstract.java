/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.interceptors.beforecontroller;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;

import com.wiley.storefrontcommons.interceptors.beforecontroller.AbstractWileyRequireHardLoginBeforeControllerHandler;
import com.wiley.wileyas.storefront.security.evaluator.impl.RequireHardLoginEvaluator;


/**
 */
public class WileyasRequireHardLoginBeforeControllerHandlerAbstract extends AbstractWileyRequireHardLoginBeforeControllerHandler
{
	private RequireHardLoginEvaluator requireHardLoginEvaluator;

	protected RequireHardLoginEvaluator getRequireHardLoginEvaluator()
	{
		return requireHardLoginEvaluator;
	}

	@Required
	public void setRequireHardLoginEvaluator(final RequireHardLoginEvaluator requireHardLoginEvaluator)
	{
		this.requireHardLoginEvaluator = requireHardLoginEvaluator;
	}

	@Override
	public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
			final HandlerMethod handler) throws Exception
	{
		// We only care if the request is secure
		if (request.isSecure())
		{
			// Check if the handler has our annotation
			final RequireHardLogIn annotation = findAnnotation(handler, RequireHardLogIn.class);
			if (annotation != null)
			{
				boolean redirect = requireHardLoginEvaluator.evaluate(request, response);

				if (redirect)
				{
					LOG.warn("Redirection required");
					getRedirectStrategy().sendRedirect(request, response, getRedirectUrl(request));
					return false;
				}
			}
		}

		return true;
	}
}
