package com.wiley.wileyas.storefront.checkout.steps.strategies.impl;


import org.springframework.beans.factory.annotation.Autowired;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.paypal.hybris.addon.strategies.TaxAvailabilityCheckStrategy;


public class WileyasPayPalTaxAvailabilityCheckStrategy implements TaxAvailabilityCheckStrategy
{

	@Autowired
	private WileyCheckoutFacade checkoutFacade;
	
	@Override
	public boolean isAvailable()
	{
		return checkoutFacade.isTaxAvailable();
	}
	
}
