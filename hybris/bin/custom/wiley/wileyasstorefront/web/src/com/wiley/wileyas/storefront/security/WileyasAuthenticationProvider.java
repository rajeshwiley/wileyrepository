package com.wiley.wileyas.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;
import de.hybris.platform.spring.security.CoreUserDetails;

import java.util.Collections;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.wiley.storefrontcommons.security.impl.WileyAssistedServiceAuthenticationToken;


public class WileyasAuthenticationProvider extends CoreAuthenticationProvider
{
	private static final Logger LOG = Logger.getLogger(WileyasAuthenticationProvider.class);

	private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();
	private static final String NONE_PROVIDED = "NONE_PROVIDED";
	private GrantedAuthority adminAuthority = new SimpleGrantedAuthority(ROLE_ADMIN_GROUP);

	private final BruteForceAttackCounter bruteForceAttackCounter;
	private final UserService userService;
	private final ModelService modelService;

	private final UserDetailsChecker postAuthenticationChecks =
			new WileyasAuthenticationProvider.DefaultPostAuthenticationChecks();

	@Autowired
	public WileyasAuthenticationProvider(
			final BruteForceAttackCounter bruteForceAttackCounter, final UserService userService,
			final ModelService modelService)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
		this.userService = userService;
		this.modelService = modelService;
	}

	/**
	 * Hybris OOTB code from
	 *
	 * @see de.hybris.platform.spring.security.CoreAuthenticationProvider#authenticate(Authentication)
	 * and
	 * @see de.hybris.platform.acceleratorstorefrontcommons.security.AbstractAcceleratorAuthenticationProvider#authenticate(
	 *Authentication)
	 *
	 * Removed password/password+token check.
	 */
	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		final String username;
		if (authentication instanceof WileyAssistedServiceAuthenticationToken)
		{
			final WileyAssistedServiceAuthenticationToken token = (WileyAssistedServiceAuthenticationToken) authentication;
			username = (token.getEmulatedUser() == null) ? NONE_PROVIDED : token.getEmulatedUser().getUid();
		}
		else
		{
			username = (authentication.getPrincipal() == null) ? NONE_PROVIDED : authentication.getName();
		}

		if (bruteForceAttackCounter.isAttack(username))
		{
			try
			{
				final UserModel userModel = userService.getUserForUID(StringUtils.lowerCase(username));
				userModel.setLoginDisabled(true);
				modelService.save(userModel);
				bruteForceAttackCounter.resetUserCounter(userModel.getUid());
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.warn("Brute force attack attempt for non existing user name " + username);
			}

			throw new BadCredentialsException(
					messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"));

		}


		if (Registry.hasCurrentTenant() && JaloConnection.getInstance().isSystemInitialized())
		{
			UserDetails userDetails;

			try
			{
				userDetails = super.retrieveUser(username);
			}
			catch (UsernameNotFoundException var6)
			{
				throw new BadCredentialsException(
						super.messages.getMessage("CoreAuthenticationProvider.badCredentials", "Bad credentials"), var6);
			}

			getPreAuthenticationChecks().check(userDetails);
			User user = UserManager.getInstance().getUserByLogin(userDetails.getUsername());
			// removed check password/token+password block

			additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);
			postAuthenticationChecks.check(userDetails);
			JaloSession.getCurrentSession().setUser(user);
			return super.createSuccessAuthentication(authentication, userDetails);
		}
		else
		{
			return super.createSuccessAuthentication(authentication,
					new CoreUserDetails("systemNotInitialized", "systemNotInitialized", true, false, true, true,
							Collections.EMPTY_LIST, null));
		}
	}

	/**
	 * @see de.hybris.platform.spring.security.CoreAuthenticationProvider#additionalAuthenticationChecks(
	 *org.springframework.security.core.userdetails.UserDetails,
	 * org.springframework.security.authentication.AbstractAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{
		super.additionalAuthenticationChecks(details, authentication);

		// Check if the user is in role admingroup
		if (getAdminAuthority() != null && details.getAuthorities().contains(getAdminAuthority()))
		{
			throw new LockedException("Login attempt as " + Constants.USER.ADMIN_USERGROUP + " is rejected");
		}
	}

	public void setAdminGroup(final String adminGroup)
	{
		if (StringUtils.isBlank(adminGroup))
		{
			adminAuthority = null;
		}
		else
		{
			adminAuthority = new SimpleGrantedAuthority(adminGroup);
		}
	}

	protected GrantedAuthority getAdminAuthority()
	{
		return adminAuthority;
	}

	private static final class DefaultPostAuthenticationChecks implements UserDetailsChecker
	{
		protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();

		private DefaultPostAuthenticationChecks()
		{
		}

		public void check(final UserDetails user)
		{
			if (!user.isCredentialsNonExpired())
			{
				throw new CredentialsExpiredException(this.messages
						.getMessage("CoreAuthenticationProvider.credentialsExpired", "User credentials have expired"));
			}
		}
	}
}
