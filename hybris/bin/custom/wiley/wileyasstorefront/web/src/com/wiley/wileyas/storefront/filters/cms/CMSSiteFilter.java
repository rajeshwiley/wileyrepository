/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.filters.cms;

import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorservices.site.strategies.SiteChannelValidationStrategy;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.cms2.misc.CMSFilter;
import de.hybris.platform.cms2.model.preview.CMSPreviewTicketModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.jalo.JaloObjectNoLongerValidException;
import de.hybris.platform.jalo.c2l.LocalizableItem;
import de.hybris.platform.servicelayer.model.AbstractItemModel;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.storefrontcommons.filters.cms.WileyCMSSiteFilter;


/**
 * Responsible for setting up application - to main responsibility belongs:
 * <p>
 * <ul>
 * <li>Matches current site by current URL</li>
 * <li>Setting current site in session (through {@link CMSSiteService} )</li>
 * <li>Setting current catalog version (through {@link CMSSiteService} )</li>
 * <li>Setting session catalog versions (through {@link CatalogService} )</li>
 * </ul>
 * </p>
 * <br/>
 * <b>Note</b>: In former versions (i.e. 4.1.1 and earlier) as a preview mechanism we used
 * {@link de.hybris.platform.cms2.misc.AbstractPreviewServlet} which actually is obsolete. All necessary logic was
 * adapted and moved here. This is a spring configured filter that is executed by the PlatformFilterChain.
 */
public class CMSSiteFilter extends WileyCMSSiteFilter
{
	private SiteChannelValidationStrategy siteChannelValidationStrategy;

	@Override
	protected void doFilterInternal(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final String requestURL = httpRequest.getRequestURL().toString();

		final CmsPageRequestContextData cmsPageRequestContextData = getCmsPageContextService().initialiseCmsPageContextForRequest(
				httpRequest);

		// check whether exits valid preview data
		if (cmsPageRequestContextData.getPreviewData() == null)
		{
			// process normal request (i.e. normal browser non-cmscockpit request)
			if (processNormalRequest(httpRequest, httpResponse))
			{
				// proceed filters
				filterChain.doFilter(httpRequest, httpResponse);
			}
		}
		else if (StringUtils.contains(requestURL, PREVIEW_TOKEN))
		{
			final String redirectURL = processPreviewRequest(httpRequest, cmsPageRequestContextData);

			// redirect to computed URL
			if (redirectURL.charAt(0) == '/')
			{
				final String contextPath = httpRequest.getContextPath();
				final String encodedRedirectUrl = httpResponse.encodeRedirectURL(contextPath + redirectURL);
				httpResponse.sendRedirect(encodedRedirectUrl);
			}
			else
			{
				final String encodedRedirectUrl = httpResponse.encodeRedirectURL(redirectURL);
				httpResponse.sendRedirect(encodedRedirectUrl);
			}

			// next filter in chain won't be invoked!!!
		}
		else
		{
			if (httpRequest.getSession().isNew())
			{
				processPreviewData(httpRequest, cmsPageRequestContextData.getPreviewData());
			}
			// proceed filters
			filterChain.doFilter(httpRequest, httpResponse);
		}
	}

	protected void processPreviewData(final HttpServletRequest httpRequest, final PreviewDataModel previewDataModel)
	{
		previewDataModel.setLanguage(filterPreviewLanguageForSite(httpRequest, previewDataModel));

		//load necessary information
		getContextInformationLoader().initializePreviewRequest(previewDataModel);
		//load fake context information
		getContextInformationLoader().loadFakeContextInformation(httpRequest, previewDataModel);
	}

	/**
	 * Processing preview request (i.e. request with additional parameters like {@link CMSFilter#PREVIEW_TOKEN} requested
	 * from cmscockpit) )
	 * <p/>
	 * <b>Note:</b> Processing preview data in order to generate target URL, and load necessary information in user
	 * session
	 * <ul>
	 * <li>Initialize information (Active CMSSite, Catalog versions,Current catalog version ) information getting from
	 * valid Preview Data</li>
	 * <li>Load all fake information (like: User, User group, Language, Time ...)
	 * <li>Generating target URL according to Preview Data
	 * </ul>
	 *
	 * @param httpRequest
	 * 		current request
	 * @return target URL
	 */
	protected String processPreviewRequest(final HttpServletRequest httpRequest,
			final CmsPageRequestContextData cmsPageRequestContextData)
	{
		final PreviewDataModel previewDataModel = cmsPageRequestContextData.getPreviewData();
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
		if (currentBaseSite != null && !currentBaseSite.equals(previewDataModel.getActiveSite()))
		{
			httpRequest.getSession(false).invalidate();
			return httpRequest.getRequestURL() + "?" + httpRequest.getQueryString();
		}
		processPreviewData(httpRequest, previewDataModel);

		//generate destination URL
		final String destinationURL = generatePreviewUrl(httpRequest, previewDataModel);

		// persist changes
		previewDataModel.setResourcePath(destinationURL);
		getContextInformationLoader().storePreviewData(previewDataModel);

		final CMSPreviewTicketModel ticket = getCmsPreviewService().createPreviewTicket(previewDataModel);
		String parameterDelimiter = "?";
		if (StringUtils.contains(destinationURL, "?"))
		{
			parameterDelimiter = "&";
		}
		return destinationURL + parameterDelimiter + PREVIEW_TICKET_ID_PARAM + "=" + ticket.getId();
	}

	/**
	 * Filters the preview language to a language supported by the site. If the requested preview language is not
	 * supported, returns the default site language instead.
	 *
	 * @param httpRequest
	 * 		current request
	 * @param previewDataModel
	 * 		the preview data model
	 * @return LanguageModel the filtered language for previewing
	 */
	protected LanguageModel filterPreviewLanguageForSite(final HttpServletRequest httpRequest,
			final PreviewDataModel previewDataModel)
	{
		final BaseSiteModel previewSite = previewDataModel.getActiveSite() == null ? getCurrentCmsSite() : previewDataModel
				.getActiveSite();
		getBaseSiteService().setCurrentBaseSite(previewSite, false);
		final Collection<LanguageModel> siteLanguages = getCommerceCommonI18NService().getAllLanguages();
		if (siteLanguages.contains(previewDataModel.getLanguage()))
		{
			// The preview language is supported
			return previewDataModel.getLanguage();
		}
		return getCommerceCommonI18NService().getDefaultLanguage();
	}

	/**
	 * Enables or disables language fall back
	 * <p/>
	 *
	 * @param httpRequest
	 * 		current request
	 * @param enabled
	 * 		enabled or disabled
	 */
	protected void setFallbackLanguage(final HttpServletRequest httpRequest, final Boolean enabled)
	{
		if (getSessionService() != null)
		{
			getSessionService().setAttribute(LocalizableItem.LANGUAGE_FALLBACK_ENABLED, enabled);
			getSessionService().setAttribute(AbstractItemModel.LANGUAGE_FALLBACK_ENABLED_SERVICE_LAYER, enabled);
		}
	}

	protected CMSSiteModel getCurrentCmsSite()
	{
		try
		{
			return getCmsSiteService().getCurrentSite();
		}
		catch (final JaloObjectNoLongerValidException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
			return null;
		}
	}

	protected SiteChannelValidationStrategy getSiteChannelValidationStrategy()
	{
		return siteChannelValidationStrategy;
	}

	@Required
	public void setSiteChannelValidationStrategy(final SiteChannelValidationStrategy siteChannelValidationStrategy)
	{
		this.siteChannelValidationStrategy = siteChannelValidationStrategy;
	}
}
