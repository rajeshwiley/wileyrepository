package com.wiley.wileyas.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.exceptions.WileyOrderCreditCardDetailsNotValidException;
import com.wiley.core.exceptions.WileyOrderNonEditableException;
import com.wiley.core.exceptions.WileyOrderPaymentMethodNotSupportedException;
import com.wiley.core.exceptions.WileyVatTaxNumberNotValidException;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.enums.MPGSSupportedCardType;
import com.wiley.facades.as.address.WileyasAddressVerificationFacade;
import com.wiley.facades.as.order.CancelResult;
import com.wiley.facades.as.order.WileyasCancelOrderFacade;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.as.order.WileyasUpdateOrderFacade;
import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;
import com.wiley.facades.user.WileyUserFacade;
import com.wiley.facades.wiley.order.dto.OrderUpdateRequestDTO;
import com.wiley.storefrontcommons.beans.propertyeditors.WileyFallbackToTodayDateEditor;
import com.wiley.storefrontcommons.config.SiteContactConfigUtil;
import com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyOrderForm;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;
import com.wiley.storefrontcommons.util.RequestLocaleManager;
import com.wiley.storefrontcommons.util.WileyAddressDataUtil;
import com.wiley.wileyas.storefront.controllers.ControllerConstants;
import com.wiley.wileyas.storefront.forms.WileyasOrderCancelReasonForm;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.MONTH_AMOUNT;


/**
 * Controller for cancel order page
 */
@Controller
@RequestMapping(value = "/my-account/order")
public class OrderPageController extends AbstractPageController
{
	private static final String PAYPAL_PAYMENT_ERROR = "text.account.orderEditing.paypal.payment.error";
	private static final Logger LOG = Logger.getLogger(OrderPageController.class);
	private static final String REDIRECT_TO_ORDER_HISTORY_DETAIL_PAGE = REDIRECT_PREFIX + "/my-account/order/%s";
	private static final String ORDER_GUID_PATH_VARIABLE_PATTERN = "{orderGuid:.*}";
	private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";
	private static final String ORDER_EDITING_CMS_PAGE = "orderEditingPage";
	private static final String ADDRESS_FORM_ATTR = "addressForm";
	private static final String ADDRESS_FORM_COUNTRY_ATTR = "addressFormCountry";
	private static final String ORDER_DATA_ATTR = "orderData";
	private static final String UPDATED_ORDER_DATA_ATTR = "updatedOrderData";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	@Resource(name = "wileyasOrderFacade")
	private WileyasOrderFacade orderFacade;

	@Resource(name = "wileyasUpdateOrderFacade")
	private WileyasUpdateOrderFacade wileyasUpdateOrderFacade;

	@Resource(name = "wileyasCancelOrderFacade")
	private WileyasCancelOrderFacade wileyasCancelOrderFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "wileyAddressDataUtil")
	private WileyAddressDataUtil wileyAddressDataUtil;

	@Resource(name = "wileyUserFacade")
	private WileyUserFacade wileyUserFacade;

	@Resource(name = "wileyasAddressFormValidator")
	private SmartValidator wileyasAddressFormValidator;

	@Resource(name = "wileyI18NFacade")
	private I18NFacade wileyI18NFacade;

	@Resource
	private WileyFallbackToTodayDateEditor wileyFallbackToTodayDateEditor;

	@Resource(name = "wileyAssistedServiceFacade")
	private WileyAssistedServiceFacade wileyAssistedServiceFacade;

	@Resource
	private SiteContactConfigUtil siteContactConfigUtil;

	@Resource(name = "wileyasAddressVerificationFacade")
	private WileyasAddressVerificationFacade addressVerificationFacade;

	@Resource(name = "wileyasOrderFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyOrderForm> wileyasOrderFormConverter;

	@Resource
	private RequestLocaleManager requestLocaleManager;

	@InitBinder
	protected void initBinder(final ServletRequestDataBinder binder)
	{
		binder.registerCustomEditor(Date.class, WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY,
				wileyFallbackToTodayDateEditor);
	}

	/*
	 * submit the confirmed cancel items to be cancelled
	 */
	@RequireHardLogIn
	@RequestMapping(value = ORDER_GUID_PATH_VARIABLE_PATTERN + "/cancel/submit", method = RequestMethod.POST)
	public String submitCancelOrderPage(@PathVariable("orderGuid") final String orderGuid,
			final RedirectAttributes redirectModel,
			@ModelAttribute final WileyasOrderCancelReasonForm orderCancelReasonForm)
	{
		try
		{
			final String orderCode = orderFacade.getOrderDetailsForGUID(orderGuid).getCode();
			CancelResult result = wileyasCancelOrderFacade.cancelOrder(orderCode, orderCancelReasonForm.getCancelReason());
			switch (result)
			{
				case STATUS_CHANGED_ERROR:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"text.account.orderHistory.cancelOrder.alreadyCompletedText");
					break;
				case BP_ACTIVE_ERROR:
				case UNKNOWN_ERROR:
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"text.account.orderHistory.cancelOrder.errorText");
					break;
				default:
					GlobalMessages.addFlashMessage(redirectModel, WileyGlobalMessages.SUCCESS_MESSAGES_HOLDER,
							"text.account.orderHistory.cancelOrder.successText");
			}
			return String.format(REDIRECT_TO_ORDER_HISTORY_DETAIL_PAGE, orderGuid);
		}
		catch (Exception exception)
		{
			LOG.error(exception.getMessage(), exception);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.account.orderHistory.cancelOrder.errorText");
			return String.format(REDIRECT_TO_ORDER_HISTORY_DETAIL_PAGE, orderGuid);
		}
	}

	@RequestMapping(value = ORDER_GUID_PATH_VARIABLE_PATTERN + "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editOrder(@PathVariable("orderGuid") final String orderGuid,
			@RequestParam(required = false) final String countryIsoCode, final Model model,
			final RedirectAttributes redirectModel, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final OrderData orderData = orderFacade.getOrderDetailsForGUID(orderGuid);
		final String orderCode = orderData.getCode();
		if (!wileyasUpdateOrderFacade.isEditable(orderCode))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					ControllerConstants.Keys.UPDATE_ORDER_NONEDITABLE_ERROR_MESSAGE, null);
			return String.format(REDIRECT_TO_ORDER_HISTORY_DETAIL_PAGE, orderGuid);
		}
		if (Objects.nonNull(countryIsoCode))
		{
			orderFacade.changeCountryForOrderData(orderData, countryIsoCode);
			requestLocaleManager.setLocale(request, countryIsoCode);
		}
		try
		{
			prepareDataForOrderEditingPage(orderData, model);

			WileyOrderForm wileyOrderForm = wileyasOrderFormConverter.convert(orderData);
			setupPaymentInformationStepData(wileyOrderForm, model);
			setupBillingInformationStepData(wileyOrderForm, orderData, model);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found",
					null);
			return REDIRECT_TO_ORDER_HISTORY_PAGE;
		}
		return getViewForPage(model);
	}

	@RequestMapping(value = ORDER_GUID_PATH_VARIABLE_PATTERN + "/suggestion", method = RequestMethod.POST)
	@RequireHardLogIn
	public String suggestionAddresses(@PathVariable("orderGuid") final String orderGuid,
			@RequestBody final WileyOrderForm wileyOrderForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		String result = validateAddressForm(orderGuid, wileyOrderForm, model,
				getAddressFormBindingResult(wileyOrderForm), response);
		String paymentMode = null;
		if (wileyOrderForm.getWileyPaymentForm() != null)
		{
			paymentMode = wileyOrderForm.getWileyPaymentForm().getPaymentMethod();
		}

		WileyAddressForm addressForm = wileyOrderForm.getWileyAddressForm();
		String countryIsoCode = addressForm.getCountryIso();

		if (result == null && addressVerificationFacade.shouldCallSuggestionDoctor(paymentMode))
		{
			AddressVerificationResult<AddressVerificationDecision> suggestions = addressVerificationFacade
					.verifyAddressData(wileyAddressDataUtil.convert(addressForm));

			model.addAttribute("action", "edit");

			if (suggestions.isSkipAddressDoctor())
			{
				return result;
			}

			model.addAttribute("wileyBillingAddressForm", addressForm);
			if (StringUtils.isNotBlank(countryIsoCode))
			{
				String regionIso = addressForm.getRegionIso();
				if (StringUtils.isNotBlank(regionIso))
				{
					model.addAttribute("addressFormRegion", wileyI18NFacade.getRegion(countryIsoCode, regionIso));
				}
				model.addAttribute(ADDRESS_FORM_COUNTRY_ATTR, wileyI18NFacade.getCountryForIsocode(countryIsoCode));
			}
			model.addAttribute("suggestions", suggestions);

			return ControllerConstants.Views.Fragments.Account.ORDER_SUGGESTION_POPUP;
		}

		if (StringUtils.isNotBlank(countryIsoCode))
		{
			requestLocaleManager.setLocale(request, countryIsoCode);
		}

		return result;
	}

	@RequestMapping(value = ORDER_GUID_PATH_VARIABLE_PATTERN + "/update", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateOrder(@PathVariable("orderGuid") final String orderGuid,
			@RequestBody final WileyOrderForm wileyOrderForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		String result = updateOrderInternal(orderGuid, wileyOrderForm, model, request, response, false);

		if (result == null)
		{
			model.addAttribute("action", "edit");
			result = ControllerConstants.Views.Fragments.Account.ORDER_TOTALS_POPUP;
		}

		return result;
	}

	@RequestMapping(value = ORDER_GUID_PATH_VARIABLE_PATTERN + "/updateConfirm", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateOrderConfirm(@PathVariable("orderGuid") final String orderGuid,
			@RequestBody final WileyOrderForm wileyOrderForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		String result = updateOrderInternal(orderGuid, wileyOrderForm, model, request, response, true);

		if (result == null)
		{
			WileyGlobalMessages.addSessionSuccessMessage(request, response,
					ControllerConstants.Keys.UPDATE_ORDER_SUCCESS_MESSAGE);
			result = ControllerConstants.Views.Fragments.Account.ORDER_EDITING_REDIRECT_RESPONSE;
		}

		return result;
	}

	private String validateAddressForm(final String orderGuid, final WileyOrderForm wileyOrderForm, final Model model,
			final BindingResult addressFormBindingResult, final HttpServletResponse response)
	{
		String result = null;
		final OrderData orderData = orderFacade.getOrderDetailsForGUID(orderGuid);

		wileyasAddressFormValidator.validate(wileyOrderForm.getWileyAddressForm(), addressFormBindingResult);
		if (addressFormBindingResult.hasErrors())
		{
			result = handleInvalidAddressForm(wileyOrderForm, model, response, orderData, addressFormBindingResult);
		}

		return result;
	}

	private BindingResult getAddressFormBindingResult(final WileyOrderForm wileyOrderForm)
	{
		final BindingResult addressFormBindingResult
				= new BeanPropertyBindingResult(wileyOrderForm.getWileyAddressForm(), ADDRESS_FORM_ATTR);
		addressFormBindingResult.getPropertyEditorRegistry().registerCustomEditor(Date.class,
				WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY, wileyFallbackToTodayDateEditor);
		return addressFormBindingResult;
	}

	private String updateOrderInternal(final String orderGuid, final WileyOrderForm wileyOrderForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final boolean saveChanges)
	{
		final OrderData orderData = orderFacade.getOrderDetailsForGUID(orderGuid);
		model.addAttribute(ORDER_DATA_ATTR, orderData);

		final BindingResult addressFormBindingResult = getAddressFormBindingResult(wileyOrderForm);

		String result = validateAddressForm(orderGuid, wileyOrderForm, model, addressFormBindingResult, response);
		if (StringUtils.isEmpty(result))
		{
			try
			{
				OrderUpdateRequestDTO orderUpdateRequestDTO = createOrderUpdateRequest(wileyOrderForm, orderData);

				OrderData updatedOrderData = wileyasUpdateOrderFacade.updateOrder(orderUpdateRequestDTO, saveChanges);
				model.addAttribute(UPDATED_ORDER_DATA_ATTR, updatedOrderData);
			}
			catch (WileyVatTaxNumberNotValidException e)
			{
				LOG.info(e.getMessage(), e);
				addressFormBindingResult.rejectValue(WileyVatRegistrationDetailsForm.TAX_NUMBER_FIELD_KEY,
						WileyVatRegistrationDetailsForm.TAX_NUMBER_ERROR_KEY);
				result = handleInvalidAddressForm(wileyOrderForm, model, response, orderData, addressFormBindingResult);
			}

			catch (WileyOrderNonEditableException e)
			{
				LOG.info(e.getMessage(), e);
				result = showOrderNotEditableMessage(request, response);
			}
			catch (WileyOrderPaymentMethodNotSupportedException e)
			{
				LOG.info(e.getMessage(), e);
				result = showErrorMessage(model, response, PAYPAL_PAYMENT_ERROR);
			}
			catch (WileyOrderCreditCardDetailsNotValidException e)
			{
				LOG.info(e.getMessage(), e);
				handleMPGSErrors(model, e);
				result = handleInvalidAddressForm(wileyOrderForm, model, response, orderData, addressFormBindingResult);
			}
			catch (Exception e)
			{
				LOG.error(e.getMessage(), e);
				result = showErrorMessage(model, response, "update.order.error.message");
			}
		}
		return result;
	}

	private void handleMPGSErrors(final Model model, final WileyOrderCreditCardDetailsNotValidException e)
	{
		String errorDescription = e.getErrorCode() != null ? getMessageSource().getMessage(e.getErrorCode(), null,
				getI18nService().getCurrentLocale()) : StringUtils.EMPTY;
		Object[] attributes = new Object[] { errorDescription };
		GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
				WileyMPGSConstants.MESSAGE_KEY_ISSUE_COMMON_MESSAGE,
				attributes);
	}

	private String showOrderNotEditableMessage(final HttpServletRequest request, final HttpServletResponse response)
	{
		String messageKey;
		if (wileyAssistedServiceFacade.isAssistedServiceModeLaunched())
		{
			messageKey = ControllerConstants.Keys.ASM_UPDATE_ORDER_NONEDITABLE_ERROR_MESSAGE;
		}
		else
		{
			messageKey = ControllerConstants.Keys.UPDATE_ORDER_NONEDITABLE_ERROR_MESSAGE;
		}
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		WileyGlobalMessages.addSessionErrorMessage(request, response, messageKey);
		return ControllerConstants.Views.Fragments.Account.ORDER_EDITING_REDIRECT_RESPONSE;
	}

	private OrderUpdateRequestDTO createOrderUpdateRequest(final WileyOrderForm wileyOrderForm, final OrderData orderData)
	{
		OrderUpdateRequestDTO orderUpdateRequestDTO = new OrderUpdateRequestDTO();
		orderUpdateRequestDTO.setOrderCode(orderData.getCode());
		saveAddressDetailsToOrderUpdateRequest(wileyOrderForm.getWileyAddressForm(), orderUpdateRequestDTO);
		savePaymentDetailsToOrderUpdateRequest(wileyOrderForm, orderUpdateRequestDTO);
		return orderUpdateRequestDTO;
	}

	private String showErrorMessage(final Model model, final HttpServletResponse response, final String messageKey)
	{
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		GlobalMessages.addErrorMessage(model, messageKey);
		return ControllerConstants.Views.Fragments.Account.ORDER_EDITING_ERROR_RESPONSE;
	}

	private String handleInvalidAddressForm(final WileyOrderForm wileyOrderForm, final Model model,
			final HttpServletResponse response, final OrderData orderData, final BindingResult addressFormBindingResult)
	{
		model.addAttribute("org.springframework.validation.BindingResult.addressForm", addressFormBindingResult);
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		setupPaymentInformationStepData(wileyOrderForm, model);
		setupBillingInformationStepData(wileyOrderForm, orderData, model);
		return ControllerConstants.Views.Fragments.Account.ORDER_EDITING_ERROR_RESPONSE;
	}

	// TODO: move this logic to populator
	protected void saveAddressDetailsToOrderUpdateRequest(final WileyAddressForm addressForm,
			final OrderUpdateRequestDTO orderUpdateRequestDTO)
	{
		AddressData paymentAddress = wileyAddressDataUtil.convert(addressForm);
		orderUpdateRequestDTO.setPaymentAddress(paymentAddress);

		final CountryData country = paymentAddress.getCountry();
		if (country.getApplicableTaxExemption() != null)
		{
			switch (country.getApplicableTaxExemption())
			{
				case TAX:
					orderUpdateRequestDTO.setTaxNumber(addressForm.getTaxExemptionDetailsForm().getTaxNumber());
					orderUpdateRequestDTO.setTaxNumberExpirationDate(
							addressForm.getTaxExemptionDetailsForm().getTaxNumberExpirationDate());
					break;
				case VAT:
					orderUpdateRequestDTO.setTaxNumber(addressForm.getVatRegistrationDetailsForm().getTaxNumber());
					orderUpdateRequestDTO.setTaxNumberExpirationDate(null);
					break;
				default:
					orderUpdateRequestDTO.setTaxNumber(null);
					orderUpdateRequestDTO.setTaxNumberExpirationDate(null);
			}
		}
	}

	// TODO: move this logic to populator
	protected void savePaymentDetailsToOrderUpdateRequest(final WileyOrderForm wileyOrderForm,
			final OrderUpdateRequestDTO orderUpdateRequestDTO)
	{
		if (wileyOrderForm.getWileyPaymentForm() != null)
		{
			orderUpdateRequestDTO.setPaymentMode(PaymentModeEnum.CARD);
			orderUpdateRequestDTO.setSessionId(wileyOrderForm.getWileyPaymentForm().getSessionId());
		}
		else if (wileyOrderForm.getWileyInvoicePaymentForm() != null)
		{
			orderUpdateRequestDTO.setPaymentMode(PaymentModeEnum.INVOICE);
			orderUpdateRequestDTO.setUserNotes(wileyOrderForm.getWileyInvoicePaymentForm().getUserNotes());
			orderUpdateRequestDTO.setPurchaseOrderNumber(wileyOrderForm.getWileyInvoicePaymentForm().getPurchaseOrderNumber());
		}
		else if (wileyOrderForm.getWileyProformaPaymentForm() != null)
		{
			orderUpdateRequestDTO.setPaymentMode(PaymentModeEnum.PROFORMA);
			orderUpdateRequestDTO.setUserNotes(wileyOrderForm.getWileyProformaPaymentForm().getUserNotes());
			orderUpdateRequestDTO.setPurchaseOrderNumber(wileyOrderForm.getWileyProformaPaymentForm().getPurchaseOrderNumber());
		}
	}

	protected void prepareDataForOrderEditingPage(final OrderData orderData, final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(getCheckoutFacade().isTaxEstimationEnabledForCart()));
		model.addAttribute("orderData", orderData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_EDITING_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_EDITING_CMS_PAGE));
	}

	protected void setupPaymentInformationStepData(final WileyOrderForm wileyOrderForm,
			final Model model)
	{
		model.addAttribute("wileyPaymentForm", wileyOrderForm.getWileyPaymentForm());
		model.addAttribute("wileyInvoicePaymentForm", wileyOrderForm.getWileyInvoicePaymentForm());
		model.addAttribute("wileyProformaPaymentForm", wileyOrderForm.getWileyProformaPaymentForm());
	}


	protected void setupBillingInformationStepData(final WileyOrderForm wileyOrderForm, final OrderData orderData,
			final Model model)
	{
		model.addAttribute("hasNoPaymentInfo", orderData.getPaymentInfo() == null);
		model.addAttribute("permitChangePaymentCountry",
				orderData.getOrderConfiguration().isPermitChangePaymentCountry());

		String countryIso = wileyOrderForm.getWileyAddressForm().getCountryIso();
		CountryData countryData = getI18NFacade().getCountryForIsocode(countryIso);
		model.addAttribute("country", countryIso);
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIso));
		model.addAttribute("supportedCountries", getCheckoutFacade().getDeliveryCountries());
		model.addAttribute(ADDRESS_FORM_COUNTRY_ATTR, countryData);
		model.addAttribute(ADDRESS_FORM_ATTR, wileyOrderForm.getWileyAddressForm());
	}

	@ModelAttribute("paymentCardMonths")
	public List<AbstractCheckoutController.SelectOption> getPaymentCardMonths()
	{
		final List<AbstractCheckoutController.SelectOption> months = new ArrayList<>();
		for (Integer monthNumber = 1; monthNumber <= MONTH_AMOUNT; monthNumber++)
		{
			months.add(new AbstractCheckoutController.SelectOption(monthNumber.toString(),
					getMessageSource().getMessage("payment.month." + monthNumber, null, getI18nService().getCurrentLocale())));
		}
		return months;
	}

	@ModelAttribute("cardExpiryYears")
	public List<AbstractCheckoutController.SelectOption> getCardExpiryYears()
	{
		final List<AbstractCheckoutController.SelectOption> expiryYears =
				new ArrayList<AbstractCheckoutController.SelectOption>();
		int currentYear = Year.now().getValue();
		final Integer yearAmount = Integer.valueOf(getSiteConfigService().getProperty("payment.mpgs.calendar.yearAmount"));

		for (int year = currentYear; year <= (currentYear + yearAmount); year++)
		{
			expiryYears.add(new AbstractCheckoutController.SelectOption(String.valueOf(year), String.valueOf(year)));
		}
		return expiryYears;
	}

	@ModelAttribute("supportedCardTypes")
	public Set<String> getSupportedCardTypes()
	{
		Set<String> supportedCardTypes = new HashSet<String>();
		for (final MPGSSupportedCardType cardType : MPGSSupportedCardType.values())
		{
			supportedCardTypes.add(cardType.getStringValue());
		}
		return supportedCardTypes;
	}

	@ModelAttribute("hostedSessionErrorMessages")
	public String getHostedSessionErrorMessages()
	{
		Map<String, String> messageMap = new HashMap<>();
		messageMap.put("globalAlertCorrectMessages",
				getMessageSource().getMessage(FORM_GLOBAL_ERROR, null, getI18nService().getCurrentLocale()));
		messageMap.put("processingError", getMessageSource()
				.getMessage("payment.hostedSesstion.processing.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeInvalid",
				getMessageSource().getMessage("payment.securityCode.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeNotProvided",
				getMessageSource().getMessage("payment.securityCode.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("expirationDateInvalid",
				getMessageSource().getMessage("payment.expirationDate.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNumberInvalid",
				getMessageSource().getMessage("payment.paymentCardNumber.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNotSupported", getMessageSource().getMessage("payment.paymentCard.notSupported", null,
				getI18nService().getCurrentLocale()));
		Gson gson = new Gson();
		return gson.toJson(messageMap);
	}

	protected AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	protected I18NFacade getI18NFacade()
	{
		return wileyI18NFacade;
	}
}
