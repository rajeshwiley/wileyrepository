package com.wiley.wileyas.storefront.filters;

import com.wiley.facades.wiley.order.WileyCartFacade;
import com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.PathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Filter that the validates the user's cart. This is a spring configured filter that is executed by the
 * PlatformFilterChain.
 */
public class WileyAsCartValidationFilter extends OncePerRequestFilter
{
    private static final String INVALID_CART_ID_MESSAGE = "checkout.error.invalid.cartId";
    private static final String CART_ID_PARAMETER = "cartId";
    private static final String CART_PATH = "/cart";

    private WileyCartFacade wileyasCartFacade;
    private PathMatcher pathMatcher;
    private Set<String> cartValidationUrlSet;

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                       final FilterChain filterChain) throws ServletException, IOException
    {
        if (RequestMethod.POST.name().equalsIgnoreCase(request.getMethod())
                && requestContainsAnyUrl(request) && !isCartIdValid(request))
        {
            WileyGlobalMessages.addSessionErrorMessage(request, response, INVALID_CART_ID_MESSAGE);
            String redirectUrl = getRedirectUrl(request);
            response.sendRedirect(redirectUrl);
            return;
        }
        filterChain.doFilter(request, response);
    }

    protected boolean isCartIdValid(final HttpServletRequest request)
    {
        String cartId = request.getParameter(CART_ID_PARAMETER);
        return StringUtils.isNotBlank(cartId) && cartId.equals(wileyasCartFacade.getSessionCartGuid());
    }

    protected String getRedirectUrl(final HttpServletRequest request)
    {
        String redirectUrl = request.getContextPath() + CART_PATH;
        return StringUtils.isNotBlank(wileyasCartFacade.getSessionCartGuid()) ?
                redirectUrl + "/" + wileyasCartFacade.getSessionCartGuid() : redirectUrl;
    }

    protected boolean requestContainsAnyUrl(final HttpServletRequest request)
    {
        final Set<String> inputSet = getCartValidationUrlSet();
        final String servletPath = request.getServletPath();

        for (final String input : inputSet)
        {
            if (getPathMatcher().match(input, servletPath))
            {
                return true;
            }
        }

        return false;
    }

    public WileyCartFacade getWileyasCartFacade()
    {
        return wileyasCartFacade;
    }

    @Required
    public void setWileyasCartFacade(final WileyCartFacade wileyasCartFacade)
    {
        this.wileyasCartFacade = wileyasCartFacade;
    }

    public Set<String> getCartValidationUrlSet()
    {
        return cartValidationUrlSet;
    }

    @Required
    public void setCartValidationUrlSet(final Set<String> cartValidationUrlSet)
    {
        this.cartValidationUrlSet = cartValidationUrlSet;
    }

    public PathMatcher getPathMatcher()
    {
        return pathMatcher;
    }

    @Required
    public void setPathMatcher(final PathMatcher pathMatcher)
    {
        this.pathMatcher = pathMatcher;
    }
}
