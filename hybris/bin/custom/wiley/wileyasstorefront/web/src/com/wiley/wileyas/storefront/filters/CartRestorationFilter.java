package com.wiley.wileyas.storefront.filters;

import de.hybris.platform.acceleratorservices.util.RegexParser;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.wiley.order.WileyCartFacade;
import com.wiley.storefrontcommons.filters.WileyCartRestorationFilter;


/**
 * Filter that the restores the user's cart. This is a spring configured filter that is executed by the
 * PlatformFilterChain.
 */
public class CartRestorationFilter extends WileyCartRestorationFilter
{
	private static final Logger LOG = Logger.getLogger(CartRestorationFilter.class);
	private static final String CART = "/cart";

	private WileyCartFacade cartFacade;
	private RegexParser regexParser;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (getUserService().isAnonymousUser(getUserService().getCurrentUser()))
		{
			processAnonymousUser(request, response);
		}
		else
		{
			restoreCart(request);
		}

		filterChain.doFilter(request, response);
	}

	private void restoreCart(final HttpServletRequest request)
	{
		final Optional<String> cartGuid = getCartGuidFromRequest(request);
		if (cartGuid.isPresent())
		{
			cartRestoration(cartGuid.get());
		}
		else if (isCartRestoreNeeded(request))
		{
			restoreCartWithNoCode();
		}

	}

	protected void restoreCartWithNoCode()
	{
		getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
		try
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart());
		}
		catch (final CommerceCartRestorationException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
			getSessionService().setAttribute(WebConstants.CART_RESTORATION, WebConstants.CART_RESTORATION_ERROR_STATUS);
		}
	}

	// This method checks should we restore GENERAL cart or not
	private boolean isCartRestoreNeeded(final HttpServletRequest request)
	{
		// true for case when user log in and a new user session is created
		if (!getCartService().hasSessionCart() && getSessionService().getAttribute(WebConstants.CART_RESTORATION) == null)
		{
			return true;
		}
		// true fo case when user has session and switched from another site
		if (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite().equals(
				getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid())))
		{
			return true;
		}
		final boolean isCartPage = request.getServletPath().equals(CART);
		// true for case when user switched from different cart
		if (isCartPage && !getCartFacade().hasSessionCartWithGuid(getCartFacade().getLatestGeneralCartGuid()))
		{
			return true;
		}
		return false;
	}

	protected void processAnonymousUser(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (getCartService().hasSessionCart()
				&& getBaseSiteService().getCurrentBaseSite().equals(
				getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid())))
		{
			final String guid = getCartService().getSessionCart().getGuid();

			if (!StringUtils.isEmpty(guid))
			{
				getCartRestoreCookieGenerator().addCookie(response, guid);
			}
		}
		else if (request.getSession().isNew()
				|| (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite().equals(
				getBaseSiteService().getBaseSiteForUID(getCartService().getSessionCart().getSite().getUid()))))
		{
			processRestoration(request);
		}
	}

	protected void processRestoration(final HttpServletRequest request)
	{
		String cartGuid = null;

		if (request.getCookies() != null)
		{
			final String anonymousCartCookieName = getCartRestoreCookieGenerator().getCookieName();

			for (final Cookie cookie : request.getCookies())
			{
				if (anonymousCartCookieName.equals(cookie.getName()))
				{
					cartGuid = cookie.getValue();
					break;
				}
			}
		}

		cartRestoration(cartGuid);
	}

	protected void cartRestoration(final String cartGuid)
	{
		if (!getCartFacade().hasSessionCartWithGuid(cartGuid))
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(cartGuid));
			}
			catch (final CommerceCartRestorationException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e);
				}
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
						WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
	}

	protected WileyCartFacade getCartFacade()
	{
		return cartFacade;
	}

	@Required
	public void setCartFacade(final WileyCartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	/**
	 * Gets guid from request's path
	 */
	protected Optional<String> getCartGuidFromRequest(final HttpServletRequest request)
	{
		/**
		 * Gets guid part from request '/cart/guid-example/some_action'
		 */
		final String guid = regexParser.parse(request.getServletPath(), 1);
		Optional<String> result = Optional.empty();

		if (StringUtils.isNotEmpty(guid))
		{
			result = Optional.of(guid);
		}
		else
		{
			LOG.debug("URI does not contains cart guid.");
		}

		return result;
	}

	public RegexParser getRegexParser()
	{
		return regexParser;
	}

	public void setRegexParser(final RegexParser regexParser)
	{
		this.regexParser = regexParser;
	}
}
