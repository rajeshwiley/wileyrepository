package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.storefrontcommons.forms.WileyAddressForm;


public class WileyAddressFormBillingInformationPopulator implements Populator<AbstractOrderData, WileyAddressForm>
{
	@Override
	public void populate(final AbstractOrderData abstractOrderData, final WileyAddressForm wileyAddressForm)
			throws ConversionException
	{
		final AddressData paymentAddress = abstractOrderData.getPaymentAddress();
		if (paymentAddress != null)
		{
			wileyAddressForm.setAddressId(paymentAddress.getId());
			wileyAddressForm.setFirstName(paymentAddress.getFirstName());
			wileyAddressForm.setLastName(paymentAddress.getLastName());
			wileyAddressForm.setLine1(paymentAddress.getLine1());
			wileyAddressForm.setLine2(paymentAddress.getLine2());
			wileyAddressForm.setTownCity(paymentAddress.getTown());
			wileyAddressForm.setPostcode(paymentAddress.getPostalCode());
			wileyAddressForm.setPhone(paymentAddress.getPhone());
			wileyAddressForm.setEmail(paymentAddress.getEmail());
			wileyAddressForm.setDepartment(paymentAddress.getDepartment());
			wileyAddressForm.setCompany(paymentAddress.getCompanyName());
			if (paymentAddress.getCountry() != null)
			{
				wileyAddressForm.setCountryIso(paymentAddress.getCountry().getIsocode());
			}
			if (paymentAddress.getRegion() != null)
			{
				wileyAddressForm.setRegionIso(paymentAddress.getRegion().getIsocode());
			}
		}
		else
		{
			final CountryData abstractOrderCountry = abstractOrderData.getCountry();
			if (abstractOrderCountry != null)
			{
				wileyAddressForm.setCountryIso(abstractOrderCountry.getIsocode());
			}
		}
	}
}
