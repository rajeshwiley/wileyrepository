package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;


public class WileyTaxExemptionDetailsFormPopulator implements Populator<AbstractOrderData, WileyTaxExemptionDetailsForm>
{
	@Override
	public void populate(final AbstractOrderData abstractOrderData,
			final WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsForm)
			throws ConversionException
	{
		wileyTaxExemptionDetailsForm.setTaxNumber(abstractOrderData.getTaxNumber());
		wileyTaxExemptionDetailsForm.setTaxNumberExpirationDate(abstractOrderData.getTaxNumberExpirationDate());
	}
}
