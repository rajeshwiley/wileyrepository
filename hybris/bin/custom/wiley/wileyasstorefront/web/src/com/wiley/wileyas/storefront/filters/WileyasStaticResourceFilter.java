/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.filters;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.config.ConfigIntf;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletResponse;

import com.wiley.storefrontcommons.filters.WileyStaticResourceFilter;


/**
 * Filter which allows us to bypass all of the spring filters for requests to a given path.
 * This is a simple servlet filter. It is intended to be used to serve static resources from the
 * web application. I can be used early in the filter chain to bypass any spring setup or hybris
 * session setup for the static resources.
 */
public class WileyasStaticResourceFilter extends WileyStaticResourceFilter
{
	protected void readConfiguredHeaderParamsAndWriteToResponse(final HttpServletResponse httpResponse)
			throws UnsupportedEncodingException
	{
		if (headerParams == null)
		{
			// Lazily build the headers from the configuration options
			headerParams = new ConcurrentHashMap<String, String>();
			final ConfigIntf config = Registry.getMasterTenant().getConfig();

			addHeaderParamsFromConfig(config);

			// Create the change listener and register it
			cfgChangeListener = new ConfigChangeListener();
			config.registerConfigChangeListener(cfgChangeListener);
		}

		// Add any configured headers to the response
		for (final Map.Entry<String, String> param : headerParams.entrySet())
		{
			httpResponse.setHeader(param.getKey(), param.getValue());
		}
	}

	protected void addHeaderParamsFromConfig(final ConfigIntf config)
	{
		for (final String key : config.getAllParameters().keySet())
		{
			if (key.startsWith(HEADER_PROPERTIES_PREFIX))
			{
				final String headerKey = key.substring(HEADER_PROPERTIES_PREFIX.length(), key.length());
				if (!headerKey.isEmpty())
				{
					headerParams.put(headerKey, config.getParameter(key));
				}
			}
		}
	}
}