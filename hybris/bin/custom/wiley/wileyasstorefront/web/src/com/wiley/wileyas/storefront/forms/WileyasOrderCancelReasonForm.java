package com.wiley.wileyas.storefront.forms;

import de.hybris.platform.basecommerce.enums.CancelReason;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasOrderCancelReasonForm
{
	private CancelReason cancelReason;

	private String localizedName;

	public CancelReason getCancelReason()
	{
		return cancelReason;
	}

	public void setCancelReason(final CancelReason cancelReason)
	{
		this.cancelReason = cancelReason;
	}

	public String getLocalizedName()
	{
		return localizedName;
	}

	public void setLocalizedName(final String localizedName)
	{
		this.localizedName = localizedName;
	}
}
