package com.wiley.wileyas.storefront.forms.converters;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;


public class WileyTaxExemptionDetailsFormConverter
		extends AbstractPopulatingConverter<AbstractOrderData, WileyTaxExemptionDetailsForm>
{

}
