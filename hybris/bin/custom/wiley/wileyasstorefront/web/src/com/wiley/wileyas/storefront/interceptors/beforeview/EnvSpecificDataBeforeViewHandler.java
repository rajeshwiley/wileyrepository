package com.wiley.wileyas.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class EnvSpecificDataBeforeViewHandler implements BeforeViewHandler
{
	private static final String DASHBOARD_URL_PROPERTY = "wileyas.dashboard.url";

	@Value("${wileyas.dashboard.url}")
	private String dashboardUrl;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response,
			final ModelAndView modelAndView)
	{
		modelAndView.addObject("wileyasDashboardUrl", dashboardUrl);
	}
}
