package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;


public class WileyVatRegistrationDetailsFormPopulator implements Populator<AbstractOrderData, WileyVatRegistrationDetailsForm>
{
	@Override
	public void populate(final AbstractOrderData abstractOrderData,
			final WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsForm)
			throws ConversionException
	{
		wileyVatRegistrationDetailsForm.setTaxNumber(abstractOrderData.getTaxNumber());
	}
}
