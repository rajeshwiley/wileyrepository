package com.wiley.wileyas.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.as.address.WileyasAddressVerificationFacade;
import com.wiley.storefrontcommons.beans.propertyeditors.WileyFallbackToTodayDateEditor;
import com.wiley.storefrontcommons.checkout.steps.validation.impl.WileyPaymentAddressCheckoutStepValidationStrategy;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator;


public class WileyasPaymentAddressCheckoutStepValidationStrategy extends WileyPaymentAddressCheckoutStepValidationStrategy
{
	protected static final String ADDRESS_FORM_ATTR = "addressForm";

	@Resource
	private WileyAddressFormValidator wileyasAddressFormValidator;

	@Resource(name = "wileyasAddressFormConverter")
	private AbstractPopulatingConverter<AbstractOrderData, WileyAddressForm> wileyasAddressFormConverter;

	@Resource
	private WileyFallbackToTodayDateEditor wileyFallbackToTodayDateEditor;

	@Resource
	private WileyasAddressVerificationFacade wileyasAddressVerificationFacade;

	@Override
	protected ValidationResults validatePaymentAddress(final RedirectAttributes redirectAttributes,
			final ValidationResults validationResults)
	{
		ValidationResults result = validationResults;
		if (getWileyasAddressVerificationFacade().shouldCallCheckoutCartAddressValidation())
		{
			final CartData checkoutCart = getWileyCheckoutFacade().getCheckoutCart();
			WileyAddressForm addressForm = getWileyasAddressFormConverter().convert(checkoutCart);
			BindingResult addressFormBindingResult = createAddressFormBindingResult(addressForm);
			getWileyasAddressFormValidator().validate(addressForm, addressFormBindingResult);
			if (addressFormBindingResult.hasErrors())
			{
				result = handleInvalidPaymentAddress(redirectAttributes);
			}
		}
		return result;
	}

	@Override
	protected void addFlashMessageToGlobalMessages(final RedirectAttributes redirectAttributes, final String messageHolder,
			final String messageKey)
	{
		// just call super to verify method is called in test
		super.addFlashMessageToGlobalMessages(redirectAttributes, messageHolder, messageKey);
	}

	protected BindingResult createAddressFormBindingResult(final WileyAddressForm addressForm)
	{
		final BindingResult addressFormBindingResult
				= new BeanPropertyBindingResult(addressForm, ADDRESS_FORM_ATTR);
		addressFormBindingResult.getPropertyEditorRegistry().registerCustomEditor(Date.class,
				WileyTaxExemptionDetailsForm.TAX_NUMBER_EXPIRATION_DATE_FIELD_KEY, getWileyFallbackToTodayDateEditor());
		return addressFormBindingResult;
	}

	protected WileyAddressFormValidator getWileyasAddressFormValidator()
	{
		return wileyasAddressFormValidator;
	}

	protected AbstractPopulatingConverter<AbstractOrderData, WileyAddressForm> getWileyasAddressFormConverter()
	{
		return wileyasAddressFormConverter;
	}

	protected WileyFallbackToTodayDateEditor getWileyFallbackToTodayDateEditor()
	{
		return wileyFallbackToTodayDateEditor;
	}

	protected WileyasAddressVerificationFacade getWileyasAddressVerificationFacade()
	{
		return wileyasAddressVerificationFacade;
	}
}
