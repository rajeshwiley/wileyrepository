package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.storefrontcommons.forms.WileyInvoicePaymentForm;

import java.util.Objects;
import java.util.Optional;


public class WileyInvoicePaymentFormPopulator implements Populator<AbstractOrderData, WileyInvoicePaymentForm>
{
	private PaymentModeEnum paymentMode;

	public WileyInvoicePaymentFormPopulator(final PaymentModeEnum paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	@Override
	public void populate(final AbstractOrderData abstractOrderData, final WileyInvoicePaymentForm wileyInvoicePaymentForm)
			throws ConversionException
	{
		if (Objects.equals(abstractOrderData.getPaymentMode(), Optional.ofNullable(paymentMode).orElseGet(null)))
		{
			wileyInvoicePaymentForm.setPurchaseOrderNumber(abstractOrderData.getPurchaseOrderNumber());
			wileyInvoicePaymentForm.setUserNotes(abstractOrderData.getUserNotes());
		}
	}
}
