package com.wiley.wileyas.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.storefrontcommons.controllers.pages.checkout.steps.WileyPaymentMethodCheckoutStepController;
import com.wiley.wileyas.storefront.controllers.ControllerConstants;


public class WileyasPaymentMethodCheckoutStepController extends WileyPaymentMethodCheckoutStepController
{
	@Override
	protected void prepareErrorMessage(final RedirectAttributes ra, final String messageKey)
	{
		GlobalMessages.addFlashMessage(ra, GlobalMessages.ERROR_MESSAGES_HOLDER,
				ControllerConstants.Keys.MESSAGE_KEY_AS_CHECKOUT_ISSUE_MESSAGE);
	}
}
