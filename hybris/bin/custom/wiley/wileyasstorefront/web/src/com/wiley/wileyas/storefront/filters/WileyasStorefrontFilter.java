/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.wiley.wileyas.storefront.filters;

import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistoryEntry;

import java.io.IOException;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.PathMatcher;


/**
 * Filter that initializes the session for the wileyasstorefront. This is a spring configured filter that is
 * executed by the PlatformFilterChain.
 */
public class WileyasStorefrontFilter extends com.wiley.storefrontcommons.filters.StorefrontFilter
{
	private Set<String> refererExcludeUrlSet;
	private PathMatcher pathMatcher;

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		final HttpSession session = request.getSession();
		final String queryString = request.getQueryString();

		if (isSessionNotInitialized(session, queryString))
		{
			initDefaults(request);

			markSessionInitialized(session);
		}

		// For secure requests ensure that the JSESSIONID cookie is visible to insecure requests
		if (isRequestSecure(request))
		{
			fixSecureHttpJSessionIdCookie(request, response);
		}

		if (isGetMethod(request))
		{
			if (StringUtils.isBlank(request.getHeader(AJAX_REQUEST_HEADER_NAME)) && !isRequestPathExcluded(request))
			{
				final String requestURL = request.getRequestURL().toString();
				session.setAttribute(ORIGINAL_REFERER, StringUtils.isNotBlank(queryString) ? requestURL + "?" + queryString :
						requestURL);
			}

			getBrowseHistory().addBrowseHistoryEntry(new BrowseHistoryEntry(request.getRequestURI(), null));
		}

		filterChain.doFilter(request, response);
	}

	protected boolean isRequestPathExcluded(final HttpServletRequest request)
	{
		final Set<String> inputSet = getRefererExcludeUrlSet();
		final String servletPath = request.getServletPath();

		for (final String input : inputSet)
		{
			if (getPathMatcher().match(input, servletPath))
			{
				return true;
			}
		}

		return false;
	}

	protected Set<String> getRefererExcludeUrlSet()
	{
		return refererExcludeUrlSet;
	}

	@Required
	public void setRefererExcludeUrlSet(final Set<String> refererExcludeUrlSet)
	{
		this.refererExcludeUrlSet = refererExcludeUrlSet;
	}

	protected PathMatcher getPathMatcher()
	{
		return pathMatcher;
	}

	@Required
	public void setPathMatcher(final PathMatcher pathMatcher)
	{
		this.pathMatcher = pathMatcher;
	}
}
