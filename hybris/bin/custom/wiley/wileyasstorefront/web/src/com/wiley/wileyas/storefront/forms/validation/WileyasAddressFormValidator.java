package com.wiley.wileyas.storefront.forms.validation;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator;

import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.COMPANY;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.DEPARTMENT;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.LINE2;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.POSTCODE;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.REGION;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.TOWN;
import static com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator.WileyAddressField.ZIPCODE;


public class WileyasAddressFormValidator extends WileyAddressFormValidator
{
	@Override
	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = WileyCoreConstants.EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

	@Override
	protected void validateStandardFields(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		super.validateStandardFields(locale, addressForm, maxLengths, formPrefix, errors);
		if (StringUtils.isNotBlank(addressForm.getLine2()))
		{
			validateStringField(locale, addressForm.getLine2(), LINE2, formPrefix, maxLengths.get(LINE2), errors);
		}
	}

	@Override
	protected void validateStringField(final Locale locale, final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		validateUnicodeCharacters(locale, addressField, fieldType, formPrefix, errors);
		super.validateStringField(locale, addressField, fieldType, formPrefix, maxFieldLength, errors);
	}

	@Override
	protected void validateEmail(final Locale locale, final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		validateUnicodeCharacters(locale, addressField, fieldType, formPrefix, errors);
		super.validateEmail(locale, addressField, fieldType, formPrefix, maxFieldLength, errors);
	}

	@Override
	protected void validateAdditionalInfo(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		validateUnicodeCharacters(locale, addressForm.getCompany(), COMPANY, formPrefix, errors);
		validateUnicodeCharacters(locale, addressForm.getDepartment(), DEPARTMENT, formPrefix, errors);
		super.validateAdditionalInfo(locale, addressForm, maxLengths, formPrefix, errors);
	}

	@Override
	protected String getFieldLabel(final WileyAddressField fieldType, final Locale locale)
	{
		switch (fieldType)
		{
			case COMPANY:
				return getValueForKeyOrDefaultFieldLabel("checkout.multi.paymentAddress.company.label", fieldType, locale);
			case DEPARTMENT:
				return getValueForKeyOrDefaultFieldLabel("checkout.multi.paymentAddress.department.label", fieldType, locale);
			default:
				return super.getFieldLabel(fieldType, locale);
		}
	}

	@Override
	protected void validateCountrySpecificFields(final Locale locale, final WileyAddressForm addressForm,
			final Map<WileyAddressField, Integer> maxLengths, final String formPrefix, final Errors errors)
	{
		final String countryIsoCode = addressForm.getCountryIso();
		final List<RegionData> regionsForCountryIso = wileyI18NFacade.getRegionsForCountryIso(countryIsoCode);

		if (CollectionUtils.isNotEmpty(regionsForCountryIso))
		{
			final String regionIso = addressForm.getRegionIso();
			validateFieldNotNull(regionIso, REGION, formPrefix, errors);

			if (regionIso != null)
			{
				final RegionData regionData = wileyI18NFacade.getRegion(countryIsoCode, regionIso);
				validateRegionExists(regionData, REGION, formPrefix, errors);
			}

			validateStringField(locale, addressForm.getPostcode(), ZIPCODE, formPrefix, maxLengths.get(ZIPCODE), errors);
		}
		else
		{
			validateStringField(locale, addressForm.getPostcode(), POSTCODE, formPrefix, maxLengths.get(POSTCODE), errors);
		}

		validateStringField(locale, addressForm.getTownCity(), TOWN, formPrefix, maxLengths.get(TOWN), errors);

		CountryData addressCountry = wileyI18NFacade.getCountryForIsocode(countryIsoCode);
		validateTaxInformation(addressForm, errors, addressCountry);
	}

	private void validateUnicodeCharacters(final Locale locale, final String addressField,
			final WileyAddressField fieldType, final String formPrefix, final Errors errors)
	{
		if (StringUtils.isNotBlank(addressField) && !isValidUnicodeCharacters(addressField))
		{
			//ECSC-26357 We should display field label in error message
			String fieldLabel = getFieldLabel(fieldType, locale);
			final String[] messageArguments = { fieldLabel };
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorUnicodeMessageKey(),
					messageArguments, fieldType.getErrorUnicodeMessageKey());
		}
	}

	private boolean isValidUnicodeCharacters(final String addressField)
	{
		final Matcher matcher = WileyCoreConstants.CHARACTERS_REGEX.matcher(addressField);
		return matcher.matches();
	}
}