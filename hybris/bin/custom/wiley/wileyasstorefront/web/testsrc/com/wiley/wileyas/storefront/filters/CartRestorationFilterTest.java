package com.wiley.wileyas.storefront.filters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.util.RegexParser;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;

import com.wiley.facades.wiley.order.WileyCartFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CartRestorationFilterTest
{
	private static final String CART_GUID = "test_cart_guid";
	private static final String CART_GUID_2 = "test_cart_guid_2";
	private static final String PATH_WITHOUT_GUID = "/cart/";
	private static final String PATH_WITH_GUID = "/cart/" + CART_GUID;
	private static final String PATH_WITH_GUID_INSIDE = "/cart/" + CART_GUID + "/some/other/case";
	private static final String PATH_WITH_GUID_AND_SLASH_IN_THE_END = "/cart/" + CART_GUID + "/";
	private static final String PATH_EMPTY = "/";
	private static final String CHECKOUT_STEP = "/checkout/multi/payment-address/add";
	private static final String CART_PAGE = "/cart";
	private static final String SITE_UID = "Site_uid";

	private static final String CART_GUID_REGEX = "^\\/cart\\/?([^\\/]*)";

	@InjectMocks
	@Spy
	private CartRestorationFilter filter;

	@Mock
	private HttpServletRequest mockRequest;

	@Mock
	private HttpServletResponse mockResponse;

	@Mock
	private HttpSession mockHttpSession;

	@Mock
	private FilterChain mockFilterChain;

	@Mock
	private UserModel mockUserModel;

	@Mock
	private UserService mockUserService;

	@Mock
	private SessionService mockSessionService;

	@Mock
	private CartService mockCartService;

	@Mock
	private WileyCartFacade mockWileyCartFacade;

	@Mock
	private CartRestorationData cartRestorationDataMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	private RegexParser regexParser;

	@Before
	public void initFilter() throws CommerceCartRestorationException
	{
		when(mockUserService.getCurrentUser()).thenReturn(mockUserModel);
		when(mockUserService.isAnonymousUser(mockUserModel)).thenReturn(false);

		regexParser = new RegexParser();
		regexParser.setRegex(CART_GUID_REGEX);
		regexParser.afterPropertiesSet();

		filter.setRegexParser(regexParser);

		when(mockRequest.getMethod()).thenReturn(HttpMethod.GET.toString());
		when(mockRequest.getSession()).thenReturn(mockHttpSession);
	}

	@Test
	public void shouldRestoreAnonymousUserCart() throws IOException, ServletException
	{
		when(mockUserService.isAnonymousUser(mockUserModel)).thenReturn(true);

		filter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

		verify(filter).processAnonymousUser(mockRequest, mockResponse);
	}

	@Test
	public void shouldRestoreCartWithGuid() throws IOException, ServletException
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITH_GUID);

		filter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

		verify(filter).cartRestoration(CART_GUID);
	}

	@Test
	public void shouldRestoreCartWithOutGuid() throws IOException, ServletException
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITHOUT_GUID);

		filter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

		verify(filter).restoreCartWithNoCode();
	}

	@Test
	public void shouldRestoreCartWhenSessionCartChanged() throws IOException, ServletException
	{
		prepareDataForCartRestoration();

		when(mockRequest.getServletPath()).thenReturn(CART_PAGE);

		filter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

		verify(filter).restoreCartWithNoCode();
	}

	@Test
	public void shouldNotRestoreCartWhenUserOnCheckoutStep() throws IOException, ServletException
	{
		prepareDataForCartRestoration();

		when(mockRequest.getServletPath()).thenReturn(CHECKOUT_STEP);

		filter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

		verify(filter, never()).restoreCartWithNoCode();
	}

	@Test
	public void shouldReturnGuidWhenCartPathWithGuid()
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITH_GUID);
		Optional<String> guid = filter.getCartGuidFromRequest(mockRequest);
		assertTrue(guid.isPresent());
		assertEquals(CART_GUID, guid.get());
	}

	@Test
	public void shouldReturnGuidWhenCartPathWithGuidInside()
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITH_GUID_INSIDE);
		Optional<String> guid = filter.getCartGuidFromRequest(mockRequest);
		assertTrue(guid.isPresent());
		assertEquals(CART_GUID, guid.get());
	}

	@Test
	public void shouldReturnGuidWhenCartPathWithGuidAndSlashInTheEnd()
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITH_GUID_AND_SLASH_IN_THE_END);
		Optional<String> guid = filter.getCartGuidFromRequest(mockRequest);
		assertTrue(guid.isPresent());
		assertEquals(CART_GUID, guid.get());
	}

	@Test
	public void shouldNotReturnGuidWhenCartPathWithoutGuid()
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_WITHOUT_GUID);
		Optional<String> guid = filter.getCartGuidFromRequest(mockRequest);
		assertFalse(guid.isPresent());
	}

	@Test
	public void shouldNotReturnGuidWhenPathIsEmpty()
	{
		when(mockRequest.getServletPath()).thenReturn(PATH_EMPTY);
		Optional<String> guid = filter.getCartGuidFromRequest(mockRequest);
		assertFalse(guid.isPresent());
	}

	@Test
	public void cartRestorationShouldNotRestoreAlreadySessionCart() throws CommerceCartRestorationException
	{
		when(mockWileyCartFacade.hasSessionCartWithGuid(CART_GUID)).thenReturn(true);

		filter.cartRestoration(CART_GUID);

		verify(mockSessionService, never()).setAttribute(any(), any());
		verify(mockWileyCartFacade, never()).restoreSavedCart(any());
	}

	@Test
	public void cartRestorationShouldRestoreSessionCart() throws CommerceCartRestorationException
	{
		when(mockWileyCartFacade.hasSessionCartWithGuid(CART_GUID)).thenReturn(false);
		when(mockWileyCartFacade.restoreSavedCart(CART_GUID)).thenReturn(cartRestorationDataMock);

		filter.cartRestoration(CART_GUID);

		verify(mockSessionService).setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
		verify(mockSessionService).setAttribute(WebConstants.CART_RESTORATION, cartRestorationDataMock);
	}

	private void prepareDataForCartRestoration()
	{
		BaseSiteModel baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid(SITE_UID);
		CartModel cartModel = new CartModel();
		cartModel.setSite(baseSiteModel);
		when(mockCartService.hasSessionCart()).thenReturn(true);
		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(baseSiteModel);
		when(mockCartService.getSessionCart()).thenReturn(cartModel);
		when(baseSiteServiceMock.getBaseSiteForUID(SITE_UID)).thenReturn(baseSiteModel);
		when(mockWileyCartFacade.getSessionCartGuid()).thenReturn(CART_GUID);
		when(mockWileyCartFacade.getLatestGeneralCartGuid()).thenReturn(CART_GUID_2);
	}
}
