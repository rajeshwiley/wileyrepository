package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.storefrontcommons.forms.WileyAddressForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAddressFormBillingInformationPopulatorUnitTest
{
	public static final String TEST_ID = "test id";
	public static final String TEST_FIRST_NAME = "test first name";
	public static final String TEST_LAST_NAME = "test last name";
	public static final String TEST_LINE_1 = "test line1";
	public static final String TEST_LINE_2 = "test line2";
	public static final String TEST_TOWN = "test town";
	public static final String TEST_POSTAL = "test postal";
	public static final String TEST_PHONE = "test phone";
	public static final String TEST_EMAIL = "test email";
	public static final String TEST_DEPARTMENT = "test department";
	public static final String TEST_COMPANY = "test company";
	public static final String TEST_ADDRESS_COUNTRY_ISO = "test address country iso";
	public static final String TEST_ADDRESS_REGION_ISO = "test address region iso";
	public static final String TEST_ORDER_COUNTRY_ISO = "test order country iso";

	@InjectMocks
	private WileyAddressFormBillingInformationPopulator testInstance;

	@Mock
	private AbstractOrderData abstractOrderDataMock;

	@Mock
	private WileyAddressForm wileyAddressFormMock;

	@Mock
	private AddressData addressDataMock;

	@Mock
	private CountryData addressCountryDataMock;

	@Mock
	private CountryData orderCountryDataMock;

	@Mock
	private RegionData regionDataMock;

	@Test
	public void testPopulateBillingInformationDetails()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(addressDataMock);
		when(addressDataMock.getId()).thenReturn(TEST_ID);
		when(addressDataMock.getFirstName()).thenReturn(TEST_FIRST_NAME);
		when(addressDataMock.getLastName()).thenReturn(TEST_LAST_NAME);
		when(addressDataMock.getLine1()).thenReturn(TEST_LINE_1);
		when(addressDataMock.getLine2()).thenReturn(TEST_LINE_2);
		when(addressDataMock.getTown()).thenReturn(TEST_TOWN);
		when(addressDataMock.getPostalCode()).thenReturn(TEST_POSTAL);
		when(addressDataMock.getPhone()).thenReturn(TEST_PHONE);
		when(addressDataMock.getEmail()).thenReturn(TEST_EMAIL);
		when(addressDataMock.getDepartment()).thenReturn(TEST_DEPARTMENT);
		when(addressDataMock.getCompanyName()).thenReturn(TEST_COMPANY);
		when(addressDataMock.getCountry()).thenReturn(addressCountryDataMock);
		when(addressCountryDataMock.getIsocode()).thenReturn(TEST_ADDRESS_COUNTRY_ISO);
		when(addressDataMock.getRegion()).thenReturn(regionDataMock);
		when(regionDataMock.getIsocode()).thenReturn(TEST_ADDRESS_REGION_ISO);


		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verify(wileyAddressFormMock).setAddressId(TEST_ID);
		verify(wileyAddressFormMock).setFirstName(TEST_FIRST_NAME);
		verify(wileyAddressFormMock).setLastName(TEST_LAST_NAME);
		verify(wileyAddressFormMock).setLine1(TEST_LINE_1);
		verify(wileyAddressFormMock).setLine2(TEST_LINE_2);
		verify(wileyAddressFormMock).setTownCity(TEST_TOWN);
		verify(wileyAddressFormMock).setPostcode(TEST_POSTAL);
		verify(wileyAddressFormMock).setPhone(TEST_PHONE);
		verify(wileyAddressFormMock).setEmail(TEST_EMAIL);
		verify(wileyAddressFormMock).setDepartment(TEST_DEPARTMENT);
		verify(wileyAddressFormMock).setCompany(TEST_COMPANY);
		verify(wileyAddressFormMock).setCountryIso(TEST_ADDRESS_COUNTRY_ISO);
		verify(wileyAddressFormMock).setRegionIso(TEST_ADDRESS_REGION_ISO);
	}

	@Test
	public void testDoesNotPopulateCountryIsoIfCountryNull()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(addressDataMock);
		when(addressDataMock.getCountry()).thenReturn(null);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verify(wileyAddressFormMock, never()).setCountryIso(any());
	}

	@Test
	public void testDoesNotPopulateRegionIsoIfCountryNull()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(addressDataMock);
		when(addressDataMock.getRegion()).thenReturn(null);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verify(wileyAddressFormMock, never()).setRegionIso(any());
	}

	@Test
	public void testPopulateOnlyCountryIsoFromOrderIfPaymentAddressNull()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(null);
		when(abstractOrderDataMock.getCountry()).thenReturn(orderCountryDataMock);
		when(orderCountryDataMock.getIsocode()).thenReturn(TEST_ORDER_COUNTRY_ISO);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then

		verifyNothingButCountryIsPopulated();
		verify(wileyAddressFormMock).setCountryIso(TEST_ORDER_COUNTRY_ISO);
	}

	@Test
	public void testPopulateNothingIfPaymentAddressNullAndOrderCountryIsNull()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(null);
		when(abstractOrderDataMock.getCountry()).thenReturn(null);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then

		verifyNothingButCountryIsPopulated();
		verify(wileyAddressFormMock, never()).setCountryIso(any());
	}

	protected void verifyNothingButCountryIsPopulated()
	{
		verify(wileyAddressFormMock, never()).setAddressId(any());
		verify(wileyAddressFormMock, never()).setFirstName(any());
		verify(wileyAddressFormMock, never()).setLastName(any());
		verify(wileyAddressFormMock, never()).setLine1(any());
		verify(wileyAddressFormMock, never()).setLine2(any());
		verify(wileyAddressFormMock, never()).setTownCity(any());
		verify(wileyAddressFormMock, never()).setPostcode(any());
		verify(wileyAddressFormMock, never()).setPhone(any());
		verify(wileyAddressFormMock, never()).setEmail(any());
		verify(wileyAddressFormMock, never()).setDepartment(any());
		verify(wileyAddressFormMock, never()).setCompany(any());
		verify(wileyAddressFormMock, never()).setRegionIso(any());
	}

}