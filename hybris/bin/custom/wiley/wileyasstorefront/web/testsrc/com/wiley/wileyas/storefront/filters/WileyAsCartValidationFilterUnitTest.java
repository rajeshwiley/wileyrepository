package com.wiley.wileyas.storefront.filters;

import com.wiley.facades.wiley.order.WileyCartFacade;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.PathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.FlashMapManager;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAsCartValidationFilterUnitTest
{
    private static final String CART_ID = "1234";
    private static final String CART_ID_PARAMETER = "cartId";
    private static final String CART_PATH = "/cart";

    @Mock
    private WileyCartFacade wileyasCartFacadeMock;

    @Mock
    private HttpServletRequest requestMock;

    @Mock
    private HttpServletResponse responseMock;

    @Mock
    private FlashMapManager flashMapManagerMock;

    @Mock
    private FilterChain filterChainMock;

    @Mock
    private PathMatcher pathMatcherMock;

    private Set<String> cartValidationUrlSet;

    @InjectMocks
    private WileyAsCartValidationFilter wileyAsCartValidationFilter;

    @Before
    public void setUp()
    {
        prepareCartValidationUrlSet();
        when(pathMatcherMock.match(any(), any())).thenReturn(true);
        when(requestMock.getServletPath()).thenReturn("");
        when(wileyasCartFacadeMock.getSessionCartGuid()).thenReturn(CART_ID);
        when(requestMock.getParameter(CART_ID_PARAMETER)).thenReturn(CART_ID);
        when(requestMock.getMethod()).thenReturn(RequestMethod.POST.name());
        when(requestMock.getAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE)).thenReturn(new FlashMap());
        when(requestMock.getAttribute(DispatcherServlet.FLASH_MAP_MANAGER_ATTRIBUTE)).thenReturn(flashMapManagerMock);
        when(requestMock.getContextPath()).thenReturn("");
    }

    @Test
    public void shouldDoFilterWhenNoMathingUrls() throws Exception
    {
        //given
        when(pathMatcherMock.match(any(), any())).thenReturn(false);

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, null, filterChainMock);

        // then
        verify(filterChainMock).doFilter(requestMock, null);
    }

    @Test
    public void shouldDoFilterForGetMethod() throws Exception
    {
        //given
        when(requestMock.getMethod()).thenReturn(RequestMethod.GET.name());

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, null, filterChainMock);

        // then
        verify(filterChainMock).doFilter(requestMock, null);
    }

    @Test
    public void shouldDoNotDoFilterForInvalidCartId() throws Exception
    {
        //given
        when(requestMock.getParameter(CART_ID_PARAMETER)).thenReturn("123");

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, responseMock, null);

        // then
        verify(responseMock).sendRedirect(any());
    }

    @Test
    public void shouldDoFilterForValidCartId() throws Exception
    {
        //given

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, responseMock, filterChainMock);

        // then
        verify(filterChainMock).doFilter(requestMock, responseMock);
    }

    @Test
    public void shouldRedirectToSessionCartWhenGuidNotEmpty() throws Exception
    {
        //given
        when(requestMock.getParameter(CART_ID_PARAMETER)).thenReturn("123");

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, responseMock, null);

        // then
        verify(responseMock).sendRedirect(CART_PATH + "/" + CART_ID);
    }

    @Test
    public void shouldRedirectToGeneralCartWhenGuidEmpty() throws Exception
    {
        //given
        when(wileyasCartFacadeMock.getSessionCartGuid()).thenReturn(null);
        when(requestMock.getParameter(CART_ID_PARAMETER)).thenReturn("123");

        // when
        wileyAsCartValidationFilter.doFilterInternal(requestMock, responseMock, null);

        // then
        verify(responseMock).sendRedirect(CART_PATH);
    }

    private void prepareCartValidationUrlSet()
    {
        cartValidationUrlSet = new HashSet<>();
        cartValidationUrlSet.add("/**/checkout/multi/**");
        cartValidationUrlSet.add("/**/cart/paypal-checkout/**");
        cartValidationUrlSet.add("/**/cart/voucher/**");
        cartValidationUrlSet.add("/**/cart/update");
        wileyAsCartValidationFilter.setCartValidationUrlSet(cartValidationUrlSet);
    }
}
