package com.wiley.wileyas.storefront.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.as.address.WileyasAddressVerificationFacade;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasPaymentAddressCheckoutStepValidationStrategyUnitTest
{
	@Spy
	@InjectMocks
	private WileyasPaymentAddressCheckoutStepValidationStrategy validationStrategy;

	@Mock
	private WileyAddressFormValidator wileyasAddressFormValidatorMock;

	@Mock
	private AbstractPopulatingConverter<AbstractOrderData, WileyAddressForm> wileyasAddressFormConverterMock;

	@Mock
	private WileyasAddressVerificationFacade wileyasAddressVerificationFacadeMock;

	@Mock
	private WileyCheckoutFacade wileyCheckoutFacadeMock;

	@Mock
	private RedirectAttributes redirectAttributesMock;

	@Mock
	private CartData checkoutCartDataMock;

	@Mock
	private WileyAddressForm wileyAddressFormMock;

	@Test
	public void testShouldRedirectToPaymentAddressIfValidationFails()
	{
		// given
		when(wileyasAddressVerificationFacadeMock.shouldCallCheckoutCartAddressValidation()).thenReturn(true);
		when(wileyCheckoutFacadeMock.getCheckoutCart()).thenReturn(checkoutCartDataMock);
		when(wileyasAddressFormConverterMock.convert(checkoutCartDataMock)).thenReturn(wileyAddressFormMock);
		doAnswer(invocation -> {
			BeanPropertyBindingResult bindingResult = (BeanPropertyBindingResult) invocation.getArguments()[1];
			bindingResult.rejectValue(WileyAddressFormValidator.WileyAddressField.LASTNAME.getFieldKey(),
					WileyAddressFormValidator.WileyAddressField.LASTNAME.getErrorKey());
			return null;
		}).when(wileyasAddressFormValidatorMock).validate(any(WileyAddressForm.class), any(BindingResult.class));

		// when
		final ValidationResults validationResults = validationStrategy.validatePaymentAddress(redirectAttributesMock,
				ValidationResults.SUCCESS);

		// then
		Assert.assertEquals(ValidationResults.REDIRECT_TO_PAYMENT_ADDRESS, validationResults);

		final ArgumentCaptor<BeanPropertyBindingResult> captor = ArgumentCaptor.forClass(BeanPropertyBindingResult.class);
		verify(wileyasAddressFormValidatorMock).validate(eq(wileyAddressFormMock), captor.capture());

		BeanPropertyBindingResult bindingResult = captor.getValue();
		Assert.assertEquals(wileyAddressFormMock, bindingResult.getTarget());
		Assert.assertEquals(WileyasPaymentAddressCheckoutStepValidationStrategy.ADDRESS_FORM_ATTR, bindingResult.getObjectName());

		verify(validationStrategy).addFlashMessageToGlobalMessages(redirectAttributesMock, GlobalMessages.ERROR_MESSAGES_HOLDER,
				WileystorefrontcommonsConstants.MESSAGE_KEY_REQUIRED_FIELDS_NOT_PROVIDED);
	}

	@Test
	public void testShouldValidatePaymentAddress()
	{
		when(wileyasAddressVerificationFacadeMock.shouldCallCheckoutCartAddressValidation()).thenReturn(true);
		when(wileyCheckoutFacadeMock.getCheckoutCart()).thenReturn(checkoutCartDataMock);
		when(wileyasAddressFormConverterMock.convert(checkoutCartDataMock)).thenReturn(wileyAddressFormMock);

		final ValidationResults validationResults = validationStrategy.validatePaymentAddress(redirectAttributesMock,
				ValidationResults.SUCCESS);

		Assert.assertEquals(ValidationResults.SUCCESS, validationResults);
		verify(wileyasAddressFormValidatorMock).validate(eq(wileyAddressFormMock), any());
	}

	@Test
	public void testShouldNotValidatePaymentAddress()
	{
		when(wileyasAddressVerificationFacadeMock.shouldCallCheckoutCartAddressValidation()).thenReturn(false);

		final ValidationResults validationResults = validationStrategy.validatePaymentAddress(redirectAttributesMock,
				ValidationResults.SUCCESS);

		Assert.assertEquals(ValidationResults.SUCCESS, validationResults);
		verify(wileyCheckoutFacadeMock, never()).getCheckoutCart();
		verify(wileyasAddressFormConverterMock, never()).convert(any());
		verify(wileyasAddressFormValidatorMock, never()).validate(any(), any());
	}
}