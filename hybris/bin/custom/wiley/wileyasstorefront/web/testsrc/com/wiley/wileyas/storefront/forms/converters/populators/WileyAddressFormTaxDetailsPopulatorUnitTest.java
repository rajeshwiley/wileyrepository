package com.wiley.wileyas.storefront.forms.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.TaxExemptionEnum;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.WileyTaxExemptionDetailsForm;
import com.wiley.storefrontcommons.forms.WileyVatRegistrationDetailsForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAddressFormTaxDetailsPopulatorUnitTest
{
	@Spy
	@InjectMocks
	private WileyAddressFormTaxDetailsPopulator testInstance;

	@Mock
	private AbstractPopulatingConverter<AbstractOrderData, WileyTaxExemptionDetailsForm>
			wileyasTaxExemptionDetailsFormConverterMock;

	@Mock
	private AbstractPopulatingConverter<AbstractOrderData, WileyVatRegistrationDetailsForm>
			wileyasVatRegistrationDetailsFormConverterMock;

	@Mock
	private AbstractOrderData abstractOrderDataMock;

	@Mock
	private WileyAddressForm wileyAddressFormMock;

	@Mock
	private AddressData paymentAddressMock;

	@Mock
	private CountryData countryDataMock;

	@Mock
	private WileyTaxExemptionDetailsForm wileyTaxExemptionDetailsFormMock;

	@Mock
	private WileyVatRegistrationDetailsForm wileyVatRegistrationDetailsFormMock;

	@Before
	public void setUp()
	{
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(paymentAddressMock);
		when(paymentAddressMock.getCountry()).thenReturn(countryDataMock);
	}

	@Test
	public void testPopulateNullAbstractOrderData()
	{
		// given

		try
		{
			// when
			testInstance.populate(null, wileyAddressFormMock);
			fail("Should throw IllegalArgumentException");
		}
		catch (final IllegalArgumentException e)
		{
			// then
			assertEquals("Parameter abstractOrderData can not be null", e.getMessage());
		}
	}

	@Test
	public void testPopulateNullWileyAddressForm()
	{
		// given

		try
		{
			// when
			testInstance.populate(abstractOrderDataMock, null);
			fail("Should throw IllegalArgumentException");
		}
		catch (final IllegalArgumentException e)
		{
			// then
			assertEquals("Parameter wileyAddressForm can not be null", e.getMessage());
		}
	}

	@Test
	public void testShouldNotPopulateTaxDetailsIfNotNeeded()
	{
		// given
		doReturn(false).when(testInstance).shouldPopulateTaxDetails(abstractOrderDataMock);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verifyTaxDetailsNotPopulated();
	}

	@Test
	public void testShouldNotPopulateTaxDetailsFormForNullPaymentAddress()
	{
		// given
		when(abstractOrderDataMock.getPaymentAddress()).thenReturn(null);

		// when
		boolean result = testInstance.shouldPopulateTaxDetails(abstractOrderDataMock);

		// then
		assertFalse(result);
	}

	@Test
	public void testShouldNotPopulateTaxDetailsFormForNullCountryData()
	{
		// given
		when(paymentAddressMock.getCountry()).thenReturn(null);

		// when
		boolean result = testInstance.shouldPopulateTaxDetails(abstractOrderDataMock);

		// then
		assertFalse(result);
	}

	@Test
	public void testShouldNotPopulateTaxDetailsFormForNullApplicableTaxExemption()
	{
		// given
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(null);

		// when
		boolean result = testInstance.shouldPopulateTaxDetails(abstractOrderDataMock);

		// then
		assertFalse(result);
	}

	@Test
	public void testShouldPopulateTaxDetailsFormForNullApplicableTaxExemption()
	{
		// given
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(null);

		// when
		boolean result = testInstance.shouldPopulateTaxDetails(abstractOrderDataMock);

		// then
		assertFalse(result);
	}

	@Test
	public void testShouldPopulateTaxDetailsFormForTAX()
	{
		// given
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(TaxExemptionEnum.TAX);
		doReturn(wileyasTaxExemptionDetailsFormConverterMock).when(testInstance)
				.getWileyasTaxExemptionDetailsFormConverter();
		doReturn(wileyTaxExemptionDetailsFormMock).when(
				wileyasTaxExemptionDetailsFormConverterMock).convert(abstractOrderDataMock);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verify(wileyAddressFormMock).setTaxExemptionDetailsForm(wileyTaxExemptionDetailsFormMock);

		verify(wileyasVatRegistrationDetailsFormConverterMock, never()).convert(any());
		verify(wileyAddressFormMock, never()).setVatRegistrationDetailsForm(any());
	}

	@Test
	public void testShouldPopulateTaxDetailsFormForVAT()
	{
		// given
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(TaxExemptionEnum.VAT);
		doReturn(wileyasVatRegistrationDetailsFormConverterMock).when(testInstance)
				.getWileyasVatRegistrationDetailsFormConverter();
		doReturn(wileyVatRegistrationDetailsFormMock).when(
				wileyasVatRegistrationDetailsFormConverterMock).convert(abstractOrderDataMock);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verify(wileyasTaxExemptionDetailsFormConverterMock, never()).convert(any());
		verify(wileyAddressFormMock, never()).setTaxExemptionDetailsForm(any());

		verify(wileyasVatRegistrationDetailsFormConverterMock).convert(abstractOrderDataMock);
		verify(wileyAddressFormMock).setVatRegistrationDetailsForm(wileyVatRegistrationDetailsFormMock);
	}

	@Test
	public void testShouldNotPopulateTaxDetailsIfNONE()
	{
		// given
		when(countryDataMock.getApplicableTaxExemption()).thenReturn(TaxExemptionEnum.NONE);

		// when
		testInstance.populate(abstractOrderDataMock, wileyAddressFormMock);

		// then
		verifyTaxDetailsNotPopulated();
	}

	protected void verifyTaxDetailsNotPopulated()
	{
		verify(wileyasTaxExemptionDetailsFormConverterMock, never()).convert(any());
		verify(wileyAddressFormMock, never()).setTaxExemptionDetailsForm(any());
		verify(wileyasVatRegistrationDetailsFormConverterMock, never()).convert(any());
		verify(wileyAddressFormMock, never()).setVatRegistrationDetailsForm(any());
	}

}