package com.wiley.wileyas.storefront.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.facades.i18n.WileyI18NFacade;
import com.wiley.storefrontcommons.forms.WileyAddressForm;
import com.wiley.storefrontcommons.forms.validation.WileyAddressFormValidator;
import com.wiley.storefrontcommons.util.RequestLocaleManager;

import static com.wiley.core.enums.TaxExemptionEnum.TAX;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressFormValidatorUnitTest
{
	private static final String LANGUAGE = "en";
	private static final String COUNTRY_ISO = "US";
	private static final String ISO_CODE = "US-AL";
	private static final String FIRST_NAME = "First Name";
	private static final String LAST_NAME = "lastName";
	private static final String LINE_1 = "Line 1";
	private static final String CITY = "City";
	private static final String POSTAL_CODE = "223322";
	private static final String PHONE = "5552233";
	private static final String EMAIL = "Email@email.com";
	private static final String INVALID_EMAIL = "Email@email.cooom";
	private static final String EMAIL_FIELD = "email";
	private static final String COMPANY = "Company";
	private static final String DEPARTMENT = "Department";
	private static final String ADDRESS_FORM_ATTR = "addressForm";
	private static final String NOT_SUPPORTED_UNICODE_CHARACTERS_1 = "Тест";
	private static final String NOT_SUPPORTED_UNICODE_CHARACTERS_2 = "クイズ";
	private static final String NOT_SUPPORTED_UNICODE_CHARACTERS_3 = "اختبار";
	private static final String NOT_SUPPORTED_UNICODE_CHARACTERS_4 = "ŢĕşŦ";
	private static final int NO_ERRORS = 0;
	private static final int ERRORS_COUNT = 1;
	private static final int ERRORS_COUNT_4 = 4;
	private static final List<WileyAddressFormValidator.WileyAddressField> MANDATORY_FIELDS = Arrays.asList(
			WileyAddressFormValidator.WileyAddressField.FIRSTNAME,
			WileyAddressFormValidator.WileyAddressField.LASTNAME,
			WileyAddressFormValidator.WileyAddressField.LINE1,
			WileyAddressFormValidator.WileyAddressField.TOWN,
			WileyAddressFormValidator.WileyAddressField.REGION,
			WileyAddressFormValidator.WileyAddressField.ZIPCODE,
			WileyAddressFormValidator.WileyAddressField.PHONE,
			WileyAddressFormValidator.WileyAddressField.EMAIL
	);

	@InjectMocks
	private WileyasAddressFormValidator wileyasAddressFormValidator;

	@Mock
	private WileyI18NFacade wileyI18NFacade;

	private WileyAddressForm wileyAddressForm;

	@Mock
	private Validator wileyTaxExemptionDetailsValidator;

	@Mock
	private MessageSource messageSource;

	@Mock
	private WileyCommonI18NService i18NService;

	@Mock
	private RequestLocaleManager requestLocaleManager;

	private Errors errors;

	@Before
	public void setUp()
	{
		setUpSuccessAddressForm();
		setUpRegionData();
		setUpCountryData();
		errors = new BeanPropertyBindingResult(wileyAddressForm, ADDRESS_FORM_ATTR);
	}

	@Test
	public void shouldValidate()
	{
		wileyasAddressFormValidator.validate(wileyAddressForm, errors);

		assertEquals(NO_ERRORS, errors.getErrorCount());
	}

	@Test
	public void shouldNotValidateWithoutMandatoryField()
	{
		wileyAddressForm.setLastName(null);
		wileyasAddressFormValidator.validate(wileyAddressForm, errors);

		assertEquals(ERRORS_COUNT, errors.getErrorCount());
		assertEquals(LAST_NAME, errors.getFieldError().getField());
	}

	@Test
	public void shouldNotValidateWithoutMandatoryFields()
	{
		setUpMandatoryFieldsAddressForm();

		wileyasAddressFormValidator.validate(wileyAddressForm, errors);

		assertEquals(MANDATORY_FIELDS.size(), errors.getErrorCount());
		for (WileyAddressFormValidator.WileyAddressField mandatoryField : MANDATORY_FIELDS)
		{
			validateHasAddressFieldError(mandatoryField);
		}
	}

	protected void validateHasAddressFieldError(final WileyAddressFormValidator.WileyAddressField wileyAddressField)
	{
		assertTrue(errors.hasFieldErrors(wileyAddressField.getFieldKey()));
		assertEquals(wileyAddressField.getErrorKey(), errors.getFieldError(wileyAddressField.getFieldKey()).getCode());
	}

	@Test
	public void shouldValidateWithEmailTopLevelDomainLenghtMoreThenFourCharacters()
	{
		wileyAddressForm.setEmail(INVALID_EMAIL);
		wileyasAddressFormValidator.validate(wileyAddressForm, errors);

		assertFalse(errors.hasErrors());
	}

	@Test
	public void shouldNotValidateWithNotSupportedUnicodeCharacters()
	{
		setUpFieldsWithNotSupportedUnicodeCharacters();

		wileyasAddressFormValidator.validate(wileyAddressForm, errors);

		assertTrue(errors.hasErrors());
		assertEquals(ERRORS_COUNT_4, errors.getErrorCount());
	}

	private void setUpSuccessAddressForm()
	{
		wileyAddressForm = new WileyAddressForm();
		wileyAddressForm.setCountryIso(COUNTRY_ISO);
		wileyAddressForm.setFirstName(FIRST_NAME);
		wileyAddressForm.setLastName(LAST_NAME);
		wileyAddressForm.setLine1(LINE_1);
		wileyAddressForm.setTownCity(CITY);
		wileyAddressForm.setPostcode(POSTAL_CODE);
		wileyAddressForm.setPhone(PHONE);
		wileyAddressForm.setEmail(EMAIL);
		wileyAddressForm.setCompany(COMPANY);
		wileyAddressForm.setDepartment(DEPARTMENT);
		wileyAddressForm.setRegionIso(ISO_CODE);
	}

	private void setUpMandatoryFieldsAddressForm()
	{
		wileyAddressForm = new WileyAddressForm();
		wileyAddressForm.setCountryIso(COUNTRY_ISO);
	}

	private void setUpRegionData()
	{
		final List<RegionData> regionDataList = new ArrayList<>();
		final RegionData regionData = createRegionData(COUNTRY_ISO, ISO_CODE);
		final Locale locale = new Locale(LANGUAGE, COUNTRY_ISO);
		regionDataList.add(regionData);

		when(i18NService.getLocaleByCountryIsoCodeWithCurrentLanguage(COUNTRY_ISO)).thenReturn(locale);
		when(wileyI18NFacade.getRegionsForCountryIso(COUNTRY_ISO)).thenReturn(regionDataList);
		when(wileyI18NFacade.getRegion(COUNTRY_ISO, ISO_CODE)).thenReturn(regionData);
	}

	private void setUpFieldsWithNotSupportedUnicodeCharacters()
	{
		wileyAddressForm.setFirstName(NOT_SUPPORTED_UNICODE_CHARACTERS_1);
		wileyAddressForm.setTownCity(NOT_SUPPORTED_UNICODE_CHARACTERS_2);
		wileyAddressForm.setCompany(NOT_SUPPORTED_UNICODE_CHARACTERS_3);
		wileyAddressForm.setDepartment(NOT_SUPPORTED_UNICODE_CHARACTERS_4);
	}

	private RegionData createRegionData(final String countryIso, final String regionIso)
	{
		final RegionData regionData = new RegionData();
		regionData.setCountryIso(COUNTRY_ISO);
		regionData.setIsocode(ISO_CODE);
		return regionData;
	}

	private void setUpCountryData()
	{
		final CountryData countryData = new CountryData();
		countryData.setApplicableTaxExemption(TAX);
		when(wileyI18NFacade.getCountryForIsocode(COUNTRY_ISO)).thenReturn(countryData);
	}
}
