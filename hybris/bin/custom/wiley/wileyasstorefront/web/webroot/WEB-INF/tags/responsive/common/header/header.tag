<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<cms:component uid="AssistedServiceComponent" />

<header role="banner" class="header-container">
    <nav role="navigation" class="header-container-navigation">
        <div class="navbar-header">
            <div type="button" data-toggle="collapse" data-target="#willey-navbar-collapse" aria-expanded="false" class="navbar-toggle collapsed"><span class="patty"></span></div>
            <cms:pageSlot position="SiteLogo" var="feature">
                <cms:component component="${feature}" element="div class='navigation-logo'"/>
            </cms:pageSlot>
            <div class="header-title-mobile"><spring:theme code="text.header.menu" /></div>
        </div>

        <div id="willey-navbar-collapse" class="collapse navbar-collapse">
            <div class="container-navigation">
                <div class="container-navigation-item-1">
                    <cms:pageSlot position="SiteLogo" var="feature">
                        <cms:component component="${feature}" element="div class='navigation-logo'"/>
                    </cms:pageSlot>
                </div>
                <div class="container-navigation-item-2">
                    <div class="navigation-menu">
                        <div class="menu-item">
                        	<a href="${wileyasDashboardUrl}" title="<spring:theme code='header.dashboard'/>">
                        		 <spring:theme code="header.dashboard"/>
                        	</a>
                        </div>
                        <div class="dropdown show menu-item menu-item-dropdown">
                            <a href="#" role="button" id="menu-dropdown-help-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                <spring:theme code="header.help"/>
                            </a>
                            <div aria-labelledby="menu-dropdown-help-block" class="dropdown-menu">
                            	<spring:theme code="header.help.chat.url" var="headerHelpChatUrl"/>
                            	<a href="${headerHelpChatUrl}" class="dropdown-item" target="_blank">
                                    <spring:theme code="header.help.chat"/>
                                </a>
                                <spring:theme code="header.help.chat.url" var="headerFaqUrl"/>
                                <a href="${headerFaqUrl}" class="dropdown-item" target="_blank">
                                    <spring:theme code="header.faq"/>
                                </a>
                                <spring:theme code="header.contact.information.url" var="headerContactInformationUrl"/>
                                <a href="${headerContactInformationUrl}" class="dropdown-item" target="_blank">
                                    <spring:theme code="header.contact.information"/>
                                </a>
                            </div>
                        </div>
                        <div class="menu-item user-name">
                            <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                                <ycommerce:testId code="header_LoggedUser">
                                    <spring:theme code="header.welcome" arguments="${user.firstName} ${user.lastName}" htmlEscape="true" />
                                </ycommerce:testId>
                            </sec:authorize>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>