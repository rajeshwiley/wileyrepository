<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div class="shopping-cart-bottom">
    <cart:cartPromotions cartData="${cartData}"/>
    <div class="total-details-wrapper">
        <cart:cartDiscounts cartData="${cartData}" />
        <div class="total-details-table">
            <cart:cartTotals cartData="${cartData}" showTax="${cartData.taxAvailable}"/>
            <cart:ajaxCartTotals/>
        
        </div>
    </div>
    <cart:cartPotentialPromotions cartData="${cartData}"/>
</div>
