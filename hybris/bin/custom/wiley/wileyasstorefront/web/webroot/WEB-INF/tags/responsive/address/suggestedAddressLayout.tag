<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="line1" required="true" type="java.lang.String"%>
<%@ attribute name="line2" required="false" type="java.lang.String"%>
<%@ attribute name="town" required="true" type="java.lang.String"%>
<%@ attribute name="postCode" required="true" type="java.lang.String"%>
<%@ attribute name="countryData" required="false" type="de.hybris.platform.commercefacades.user.data.CountryData"%>
<%@ attribute name="regionData" required="false" type="de.hybris.platform.commercefacades.user.data.RegionData"%>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="saved-info-value">
    <p>${line1}</p>
    <p>
        <c:if test="${not empty line2}">${line2}</c:if>
    </p>
    <p>
        <span>${town}</span>,&nbsp;
        <c:if test="${not empty regionData and not empty regionData.name}">
            <span>${regionData.name}</span>,&nbsp;
        </c:if>
        <span>${postCode}</span>
    </p>
    <p>${countryData.name}</p>
</div>