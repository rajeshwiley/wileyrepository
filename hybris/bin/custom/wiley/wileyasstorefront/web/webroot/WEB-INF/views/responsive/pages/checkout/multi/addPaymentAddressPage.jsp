<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="tax" tagdir="/WEB-INF/tags/responsive/tax"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" renderAntiClickJacking="true">
    <c:url value="${currentStepUrl}" var="choosePaymentMethodUrl" />
    <multiCheckout:checkoutStepsBreadcrumb />
    <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
        <jsp:body>
            <ycommerce:testId code="checkoutStepTwo">
					    <address:addressFormSelector supportedCountries="${countries}"
                             regions="${regions}" cancelUrl="${currentStepUrl}" country="${country}" />
            </ycommerce:testId>
        </jsp:body>
    </multiCheckout:checkoutSteps>
</template:page>