<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<%@ attribute name="abstractOrderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="showCurrentPaymentMode" required="false" type="java.lang.Boolean" %>
<c:set var="showCurrentPaymentMode" value="${showCurrentPaymentMode != null ? showCurrentPaymentMode : false}"/>
<%@ attribute name="showNextStepButton" required="false" type="java.lang.Boolean" %>
<c:set var="showNextStepButton" value="${showNextStepButton != null ? showNextStepButton : true}"/>


<c:set value="${not empty abstractOrderData.paymentMode ? abstractOrderData.paymentMode : 'card'}" var="selectedPaymentMode"/>
<c:if test="${!abstractOrderData.taxAvailable and abstractOrderData.paymentMode != 'proforma' and abstractOrderData.paymentMode != 'invoice'}">
    <c:set value="invoice" var="selectedPaymentMode"/>
</c:if>

<div class="form-wrapper">
    <input type="hidden" name="cartId" value="${abstractOrderData.guid }"/>
    <div class="form-top">
        <div class="form-title">
            <spring:theme
                    code="text.multi.checkout.payment.options"/>
        </div>
        <ul role="tablist" class="tab-links" id="paymentMethods">
            <c:set var="permittedPaymentModes"
                   value="${abstractOrderData.orderConfiguration.permittedPaymentModes}"/>
            <multiCheckout:paymentMode paymentMode="current" permittedPaymentModes="${permittedPaymentModes}">
                <li class="current-option-tab active">
                    <a href="#current-option-tab" role="tab" data-toggle="tab">
                        <form:radiobutton
                                path="wileyPaymentForm.paymentMethod" id="current"
                                value="current" name="payment-options"/>
                        <label for="current">
                            <span><spring:theme code="text.multi.checkout.paymentmethod.current"/></span>
                        </label>
                    </a>
                </li>
            </multiCheckout:paymentMode>
            <multiCheckout:paymentMode paymentMode="card" permittedPaymentModes="${permittedPaymentModes}">
                <li class="credit-card-tab ${!showCurrentPaymentMode and selectedPaymentMode eq 'card' ? 'active' : ''}">
                    <a href="#credit-card-tab" role="tab" data-toggle="tab">
                        <form:radiobutton
                                disabled="${!abstractOrderData.taxAvailable && (abstractOrderData.totalPrice.value gt 0)}"
                                path="wileyPaymentForm.paymentMethod" id="card"
                                value="card" name="payment-options"/>
                        <label for="card">
                            <span><spring:theme code="text.multi.checkout.paymentmethod.creditcard"/></span>
                        </label>
                    </a>
                </li>
            </multiCheckout:paymentMode>
            <multiCheckout:paymentMode paymentMode="invoice" permittedPaymentModes="${permittedPaymentModes}">
                <li class="invoice-tab ${!showCurrentPaymentMode and selectedPaymentMode eq 'invoice' ? 'active' : ''}">
                    <a href="#invoice-tab" role="tab" data-toggle="tab">
                        <form:radiobutton
                                path="wileyPaymentForm.paymentMethod" id="invoice"
                                value="invoice" name="payment-options"/>
                        <label for="invoice">
                            <span><spring:theme code="text.multi.checkout.paymentmethod.invoice"/></span>
                        </label>
                    </a>
                </li>
            </multiCheckout:paymentMode>
            <multiCheckout:paymentMode paymentMode="proforma"
                                       permittedPaymentModes="${permittedPaymentModes}">
                <li class="proforma-tab ${!showCurrentPaymentMode and selectedPaymentMode eq 'proforma' ? 'active' : ''}">
                    <a href="#proforma-tab"
                       role="tab" data-toggle="tab">
                        <form:radiobutton
                                path="wileyPaymentForm.paymentMethod" id="proforma"
                                value="proforma" name="payment-options"/>
                        <label for="proforma">
                            <span><spring:theme code="text.multi.checkout.paymentmethod.proforma"/></span>
                        </label>
                    </a>
                </li>
            </multiCheckout:paymentMode>
            <multiCheckout:paymentMode paymentMode="paypal" permittedPaymentModes="${permittedPaymentModes}">
                <li class="tab-link-paypal ${!showCurrentPaymentMode and selectedPaymentMode eq 'paypal' ? 'active' : ''}">
                    <a href="#paypal-tab" role="tab" data-toggle="tab">
                        <form:radiobutton
                                disabled="${!abstractOrderData.taxAvailable && (abstractOrderData.totalPrice.value gt 0)}"
                                path="wileyPaymentForm.paymentMethod" id="paypal"
                                value="paypal" name="payment-options"/>
                        <label for="paypal">
                            <span><img src="${commonResourcePath}/images/paypal_active_icon@2x.png"  alt="PayPal"/></span>
                        </label>
                    </a>
                    <a href="#" class="link-info" id="what-is-paypal-link">
                        <spring:theme code="text.multi.checkout.paymentmethod.paypal"/>
                    </a>
                </li>
            </multiCheckout:paymentMode>
        </ul>
    </div>
    <div class="tab-content">
        <c:if test="${showCurrentPaymentMode}">
            <div id="current-option-tab" class="tab-pane fade active in">
                <order:paymentMethodItem order="${abstractOrderData}"/>
            </div>
        </c:if>
        <multiCheckout:creditCardTab abstractOrderData="${abstractOrderData}" active="${!showCurrentPaymentMode and selectedPaymentMode eq 'card'}"
                                     showSubmitButton="${showNextStepButton}"/>
        <multiCheckout:invoiceTab active="${!showCurrentPaymentMode and selectedPaymentMode eq 'invoice'}" paymentMode="invoice"
                                  formName="wileyInvoicePaymentForm" showSubmitButton="${showNextStepButton}"/>
        <multiCheckout:invoiceTab active="${!showCurrentPaymentMode and selectedPaymentMode eq 'proforma'}" paymentMode="proforma"
                                  formName="wileyProformaPaymentForm" showSubmitButton="${showNextStepButton}"/>
        <c:if test="${!showCurrentPaymentMode}">
            <multiCheckout:paypalTab active="${!showCurrentPaymentMode and selectedPaymentMode eq 'paypal'}"/>
        </c:if>
    </div>
</div>