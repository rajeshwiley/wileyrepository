<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="tab-content order-review-billing">
	<h3 class="order-review-section-title"><spring:theme code="text.multi.checkout.orderreview.billinginfo" /></h3>
	<div class="order-review-items-list">
		<c:if test="${not empty order.paymentAddress.firstName or not empty order.paymentAddress.lastName}">
			<div class="order-review-item">
				<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.name" /></div>
				<div class="order-review-value">
						<c:if test="${not empty order.paymentAddress.firstName}">${order.paymentAddress.firstName}</c:if>&nbsp;
						<c:if test="${not empty order.paymentAddress.lastName}">${order.paymentAddress.lastName}</c:if>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty order.paymentAddress.companyName}">
			<div class="order-review-item">
				<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.university" /></div>
				<div class="order-review-value">${order.paymentAddress.companyName}</div>
			</div>
		</c:if>
		<c:if test="${not empty order.paymentAddress.department}">
			<div class="order-review-item">
				<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.department" /></div>
				<div class="order-review-value">${order.paymentAddress.department}</div>
			</div>
		</c:if>
		<div class="order-review-item">
			<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.address" /></div>
			<div class="order-review-value">
				${order.paymentAddress.addressSummary}
			</div>
		</div>
        <c:if test="${not empty order.paymentAddress.phone}">
            <div class="order-review-item">
                <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.phone"/></div>
                <div class="order-review-value">${order.paymentAddress.phone}</div>
            </div>
        </c:if>
		<c:if test="${not empty order.paymentAddress.email}">
		  <div class="order-review-item">
		  	<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.email" /></div>
		  	<div class="order-review-value">${order.paymentAddress.email}</div>
		  </div>
		</c:if>
	</div>
	<c:if test="${not empty order.taxNumber}">
		<c:if test="${order.paymentAddress.country.applicableTaxExemption == 'TAX'}">
			<h3 class="order-review-section-title"><spring:theme code="text.multi.checkout.orderreview.billinginfo.taxdetails" /></h3>
			<div class="order-review-items-list">
				<div class="order-review-item">
					<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.taxnumber" /></div>
					<div class="order-review-value">${order.taxNumber}</div>
				</div>
				<c:if test="${not empty order.taxNumberExpirationDate}">
					<div class="order-review-item">
						<div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.billinginfo.taxexpirationdate" /></div>
						<div class="order-review-value"><fmt:formatDate pattern="dd MMMM YYYY" value="${order.taxNumberExpirationDate}" /></div>
					</div>
				</c:if>
			</div>
		</c:if>
		<c:if test="${order.paymentAddress.country.applicableTaxExemption == 'VAT'}">
			<h3 class="order-review-section-title"><spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.title" /></h3>
			<div class="order-review-items-list">
				<div class="order-review-item">
					<div class="order-review-label"><spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.label" /></div>
					<div class="order-review-value">${order.taxNumber}</div>
				</div>
			</div>
		</c:if>
	</c:if>
</div>