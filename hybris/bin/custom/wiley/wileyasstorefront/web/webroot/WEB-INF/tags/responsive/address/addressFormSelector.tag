<%@ attribute name="supportedCountries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="cancelUrl" required="false" type="java.lang.String"%>
<%@ attribute name="addressBook" required="false" type="java.lang.String"%>
<%@ attribute name="showNextStepButton" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showSaveAddressButton" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="tax" tagdir="/WEB-INF/tags/responsive/tax"%>

<c:set var="showNextStepButton" value="${showNextStepButton != null ? showNextStepButton : true}"/>
<c:set var="showSaveAddressButton" value="${showSaveAddressButton != null ? showSaveAddressButton : true}"/>

<div>
    <form:form id="addressForm" class="form-wrapper" method="post" modelAttribute="addressForm" commandName="addressForm" autocomplete="off">
        <c:set var="disabledCss" value="${not permitChangePaymentCountry ? 'pointer-events-none' : ''}"/>
        <div class="form-content column">
            <div class="form-title">
                <spring:theme code="text.checkout.multi.paymentAddress.title"/>
            </div>
            <c:if test="${not empty paymentAddresses}">
                <button type="button" class="button form-title-button small"
                        data-toggle="modal" data-target="#savedAddressesModal">
                    <spring:theme code="text.checkout.multi.paymentAddress.viewAddressBook"/>
                </button>
            </c:if>
            <c:if test="${empty addressFormEnabled or addressFormEnabled}">
                <div class="required-field-legend">
                    <spring:theme code="text.checkout.multi.paymentAddress.requiredMessage"/>
                </div>
                <input type="hidden" name="bill_state" id="address.billstate"/>
                <input type="hidden" name="cartId" id="carId" value="${cartData.guid }"/>
                <div class="form-controls-wrapper">
                    <div class="input-group">
                        <div class="form-field required" id="countrySelector"
                            data-address-code="${addressData.id}" data-country-iso-code="${addressData.country.isocode}">
                        <formElement:formSelectBox idKey="address.country"
                                labelKey="address.country" path="countryIso" mandatory="true"
                                skipBlank="false" skipBlankMessageKey="address.country"
                                items="${supportedCountries}" itemValue="isocode"
                                selectedValue="${addressForm.countryIso}" selectCSSClass="${disabledCss}" />
                        </div>
                    </div>
                    <div id="i18nAddressForm" class="input-group-wr"
                         data-applicable-tax-exemption="${not empty addressFormCountry.applicableTaxExemption ? addressFormCountry.applicableTaxExemption : ''}">
                        <c:if test="${not empty country}">
                            <form:hidden path="addressId"/>
                            <address:addressFormElements regions="${regions}"
                                country="${country}" />
                        </c:if>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="form-content column">
            <address:additionalBillingInfo/>
            <tax:taxExemptionDetailsFormElements country="${country}"/>
            <tax:vatRegistrationDetailsFormElements country="${country}"/>
        </div>
        <c:if test="${showSaveAddressButton || showNextStepButton}">
            <div class="form-footer">
                <c:if test="${showSaveAddressButton}">
                    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
                        <formElement:formCheckbox   idKey="saveAddressInMyAddressBook"
                                                    labelKey="checkout.summary.paymentAddress.saveAddressInMyAddressBook"
                                                    path="saveInAddressBook" inputCSS="add-address-left-input"
                                                    labelCSS="custom-checkbox" mandatory="true"/>
                    </sec:authorize>
                </c:if>
                <c:if test="${showNextStepButton}">
                    <button id="addressSubmit" type="button" class="button button-main small">
                        <spring:theme code="checkout.multi.deliveryAddress.continue"/>
                    </button>
                </c:if>
            </div>
        </c:if>
        <input id="showSuggestionAddresses" name="showSuggestionAddresses" type="hidden" value="${addressForm.showSuggestionAddresses}"/>
        <input id="suggestionSelected" name="suggestionSelected" type="hidden" value="${addressForm.suggestionSelected}"/>
    </form:form>
    <div id="suggestedAddressesModal" tabindex="-1" role="dialog" aria-labelledby="suggestedAddressesModalTitle" aria-hidden="true" class="modal fade">
        <address:suggestedAddresses wileyBillingAddressForm="${addressForm}"/>
    </div>
    <address:addressBook/>
</div>
