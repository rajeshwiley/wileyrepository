<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<div class="form-top">
    <div class="form-title">
        <spring:theme code="text.productDetails" text="Product Details"/>
    </div>
    <div id="orderReviewProductTable" class="order-review-table">
        <div class="products-table">
            <div class="table-head">
                <div class="table-head-item item-product">
                    <spring:theme code="text.product" text="Product"/>
                </div>
                <div class="table-head-item item-quantity">
                    <spring:theme code="text.quantity" text="Quantity"/>
                </div>
                <div class="table-head-item item-total">
                    <spring:theme code="text.total" text="Total"/>
                </div>
            </div>
            <div class="table-body">
                <c:forEach items="${order.entries}" var="entry">
                    <c:url value="${entry.product.url}" var="productUrl"/>
                    <div class="table-body-row">
                        <div class="table-body-item item-image">
                            <c:choose>
                                <c:when test="${not empty entry.article.pictureUrl}">
                                    <img src="${entry.article.pictureUrl}"/>
                                </c:when>
                                <c:otherwise>
                                    <product:productPrimaryImage product="${entry.product}"/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="table-body-mobile-wrap">
                            <div class="table-body-item item-description">
                                <span class="product-label">${entry.product.name}</span>
                                <c:if test="${not empty entry.article.journalName}">
                                <h4 class="item-description-title">${entry.article.journalName}</h4>
                                </c:if>
                                <p class="item-description-text">
                                        ${entry.article.summary}
                                </p>
                                <div class="description-extra-info">
                                    <c:if test="${not empty entry.article.doi}">
                                        <div class="extra-item">
                                            <p class="item-title">
                                                <spring:theme code="product.article.doi"/>
                                            </p>
                                            <p class="item-value">${entry.article.doi}</p>
                                        </div>
                                    </c:if>
                                    <c:if test="${not empty entry.namedDeliveryDate}">
                                        <div class="extra-item">
                                            <p class="item-title">
                                                <spring:theme code="basket.page.delivery.date"/>
                                            </p>
                                            <p class="item-value">
                                                <fmt:formatDate pattern="dd MMMM YYYY"
                                                                value="${entry.namedDeliveryDate}"/>
                                            </p>
                                        </div>
                                    </c:if>
                                    <product:productListerClassifications product="${entry.product}"/>
                                </div>
                            </div>
                            <div class="table-body-item item-quantity">
                                <c:if test="${entry.product.countable }">
                                    <p class="item-title-mobile">
                                        <spring:theme code="text.multi.checkout.orderreview.quantity"/>
                                    </p>
                                    <p class="item-value">${entry.quantity}</p>
                                </c:if>
                            </div>
                            <div class="table-body-item item-total">
                                <p class="item-title-mobile">
                                    <spring:theme code="basket.page.total"/>
                                </p>
                                <p class="item-value">
                                    <format:price priceData="${entry.totalPrice}"
                                                  displayFreeForZero="true"/>
                                </p>
                            </div>
                        </div>
                        <div class="item-price-discount">
                            <c:forEach items="${order.appliedProductPromotions}" var="promotion">
                                <c:set var="displayed" value="false"/>
                                <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                    <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
                                        <c:set var="displayed" value="true"/>
                                        <ycommerce:testId code="cart_potentialPromotion_label">
                                            <p>${promotion.description}</p>
                                        </ycommerce:testId>
                                    </c:if>
                                </c:forEach>
                            </c:forEach>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <order:orderTotalsItem order="${order}"/>
    </div>
</div>