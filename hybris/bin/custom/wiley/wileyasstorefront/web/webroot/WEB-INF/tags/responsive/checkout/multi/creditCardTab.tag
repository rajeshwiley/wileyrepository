<%@ tag body-content="empty" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="active" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showSubmitButton" required="false" type="java.lang.Boolean" %>
<%@ attribute name="abstractOrderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<c:set var="showSubmitButton" value="${showSubmitButton != null ? showSubmitButton : true}"/>

<script src="${abstractOrderData.hostedSessionScriptUrl}"></script>

<div id="credit-card-tab" class="tab-pane fade ${active ? 'active in' : ''}">
    <form:form id="wileyPaymentForm">
        <div class="form-content">
            <div class="form-title"><spring:theme code="text.multi.checkout.paymentmethod.multi.payment"/><b>&nbsp;<spring:theme
                    code="text.multi.checkout.paymentmethod.creditcard"/></b></div>
            <c:if test="${not empty paymentInfos}">
                <a href="#" data-toggle="modal" data-target="#creditCardsModal" class="button form-title-button small">
                    <spring:theme code="text.multi.checkout.paymentmethod.saved.credit.cards"/>
                </a>
            </c:if>
            <div class="required-field-legend">
                <spring:theme code="text.multi.checkout.paymentmethod.requred.fields"/>
            </div>
            <div class="form-controls-wrapper payment-controls-custom">
                <div class="input-group">
                    <div id="card-number" class="form-field required">
                        <label class="control-label form-label required" for="card-number-input">
                            <b><spring:theme code="text.multi.checkout.paymentmethod.cardNumber"/></b>
                        </label>
                        <input type="text" id="card-number-input" pattern="[0-9]+"
                               title="<spring:theme code="text.multi.checkout.paymentmethod.please.input.numbers.only"/>"
                               placeholder="<spring:theme code="text.multi.checkout.paymentmethod.enter.card.number"/>"
                               class="input-field card-number-input form-control middle" value="" required readonly>
                    </div>
                    <div id="block-alert-cart-number" class="form-field-message error-message hidden">
                        <img src="${commonResourcePath}/images/error-discount-icon.png"
                            srcset="${commonResourcePath}/images/error-discount-icon@2x.png 2x, ${commonResourcePath}/images/error-discount-icon@3x.png 3x">
                        <span id="text-alert-cart-number"></span>
                    </div>
                </div>
                <div class="card-type-icons">
                    <img src="${commonResourcePath}/images/visa_active@3x.png" alt="Visa" />
                    <img src="${commonResourcePath}/images/master_active@3x.png" alt="Master Card" />
                    <img src="${commonResourcePath}/images/discovery_active@3x.png" alt="Discover" />
                    <img src="${commonResourcePath}/images/american-active@2x.png" alt="American Express"/>
                </div>
                <div class="input-group">
                    <div id="expiry-month" class="form-field required">
                        <label for="expiry-month-select">
                            <spring:theme code="text.multi.checkout.paymentmethod.expiration.date"/>
                        </label>
                        <div class="select-component small">
                            <select id="expiry-month-select" required>
                                <c:forEach items="${paymentCardMonths}" var="month">
                                    <option value="${fn:escapeXml(month.code)}">${fn:escapeXml(month.name)}</option>
                                </c:forEach>
                            </select>
                            <div id="block-alert-date-month" class="form-field-message error-message hidden">
                                <img src="${commonResourcePath}/images/error-discount-icon.png"
                                    srcset="${commonResourcePath}/images/error-discount-icon@2x.png 2x, ${commonResourcePath}/images/error-discount-icon@3x.png 3x">
                                <span id="text-alert-date-month"></span>
                            </div>
                        </div>
                        <div id="expiry-year" class="select-component small">
                            <select id="expiry-year-select" required>
                                <c:forEach items="${cardExpiryYears}" var="year">
                                    <option value="${fn:escapeXml(year.code)}">${fn:escapeXml(year.name)}</option>
                                </c:forEach>
                            </select>
                            <div id="block-alert-date-year" class="form-field-message error-message hidden">
                                <img src="${commonResourcePath}/images/error-discount-icon.png"
                                    srcset="${commonResourcePath}/images/error-discount-icon@2x.png 2x, ${commonResourcePath}/images/error-discount-icon@3x.png 3x">
                                <span id="text-alert-date-year"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div id="security-code" class="form-field required">
                        <label for="security-code-input">
                            <spring:theme code="text.multi.checkout.paymentmethod.security.code"/>
                            <span class="popover-help-icon">&nbsp;
                                <a role="menuitem" tabindex="0" data-toggle="popover"
                                   aria-label="information icon"
                                   data-content="<spring:theme code="text.multi.checkout.paymentmethod.security.code.tooltip.message"/>"
                                   class="popover-help"></a>
                            </span>
                        </label>
                        <input id="security-code-input" maxlength="3" pattern="[0-9]+"
                               title="<spring:theme code="text.multi.checkout.paymentmethod.please.input.numbers.only"/>"
                               placeholder="<spring:theme code="text.multi.checkout.paymentmethod.enter.security.code"/>"
                               type="text" class="input-field security-code-input form-control small" value="" readonly required>
                        <input type="hidden" name="sessionId" id="sessionId">
                        <input type="hidden" value="${supportedCardTypes}" id="supportedCardTypes">
                        <input type="hidden" id="hostedSessionErrorMessages">
                        <input type="hidden" name="cartId" value="${cartData.guid}"/>
                    </div>
                    <div id="block-alert-code" class="form-field-message error-message hidden">
                        <img src="${commonResourcePath}/images/error-discount-icon.png"
                            srcset="${commonResourcePath}/images/error-discount-icon@2x.png 2x, ${commonResourcePath}/images/error-discount-icon@3x.png 3x">
                        <span id="text-alert-code"></span>
                    </div>
                </div>
            </div>
        </div>
        <c:if test="${showSubmitButton}">
            <div class="form-footer">
                <label class="custom-checkbox">
                    <input type="hidden" value="on" name="_saveInAccount"/>
                    <input id="savePaymentInfo" name="saveInAccount" type="checkbox"><spring:theme code="text.multi.checkout.paymentmethod.save.to.credit.card.info"/>
                </label>
								        <button id="submitWileyPaymentForm" type="button" class="button button-main small">
								            <spring:theme code="text.multi.checkout.next.step"/>
								        </button>
            </div>
        </c:if>
    </form:form>
    <c:if test="${not empty paymentInfos}">
        <!-- Modal-->
        <div id="creditCardsModal" tabindex="-1" role="dialog" aria-labelledby="creditCardsModalTitle" aria-hidden="true"
             class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true"></span>
                                <span class="sr-only"><spring:theme code="text.multi.checkout.paymentmethod.popup.close"/></span>
                            </button>
                            <h4 id="creditCardsModalTitle" class="modal-title">
                                <spring:theme code="text.multi.checkout.paymentmethod.saved.credit.cards"/>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form:form/>
                            <c:forEach var="payment" items="${paymentInfos}">
                                <div class="saved-card-info">
                                    <div class="saved-info-value">${fn:escapeXml(payment.cardTypeData.name)}</div>
                                    <div class="saved-info-value"><spring:theme code="text.multi.checkout.paymentmethod.card.pattern"/>&nbsp;${fn:escapeXml(payment.cardNumber)}</div>
                                    <div class="saved-info-value">
                                        Expired <fmt:formatNumber pattern="00" value="${fn:escapeXml(payment.expiryMonth)}"/>/<fmt:formatNumber pattern="2000" value="${fn:escapeXml(payment.expiryYear)}"/>
                                    </div>
                                    <form:form action="${request.contextPath}/checkout/multi/payment-method/choose" method="POST">
                                        <input type="hidden" name="selectedPaymentMethodId" value="${fn:escapeXml(payment.id)}">
                                        <input type="hidden" name="cartId" value="${cartData.guid}"/>
																																								<button type="submit" class="button button-main">
																																												<spring:theme code="text.multi.checkout.paymentmethod.use.this.payment.method"/>
																																								</button>
                                    </form:form>
                                </div>
                            </c:forEach>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </c:if>
</div>

<script type="text/javascript"
        src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/payment/hostedSession.js"></script>
<script type="text/javascript"
        src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/payment/paymentSession.js"></script>
<script>HostedSession.errorMessages = ${hostedSessionErrorMessages};</script>
