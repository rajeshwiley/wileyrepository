<%@ attribute name="regions" required="true" type="java.util.List" %>
<%@ attribute name="country" required="false" type="java.lang.String" %>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="minlength7">
    <spring:theme code="text.input.length.7"/>
</c:set>

<c:set var="maxlength35">
    <spring:theme code="text.input.length.35"/>
</c:set>

<c:set var="maxlength15">
    <spring:theme code="text.input.length.15"/>
</c:set>

<c:set var="maxlength9">
    <spring:theme code="text.input.length.9"/>
</c:set>

<c:choose>
    <c:when test="${country == 'CA'}">
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.firstName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.lastName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line1.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                          mandatory="false" maxlength="${maxlength35}" placeholder="address.line2.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.townCity.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true"
                                           skipBlank="false" skipBlankMessageKey="address.province" items="${regions}"
                                           itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
                                           selectedValue="${addressForm.regionIso}"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength9}"
                                          placeholder="address.zipcode.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength15}" placeholder="address.phone.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.email.placeholder"/>
            </div>
        </div>
    </c:when>
    <c:when test="${country == 'CN'}">
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.firstName" labelKey="address.givenName" path="firstName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.givenName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.surname" labelKey="address.familyName" path="lastName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.familyName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line1" labelKey="address.street" path="line1" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line1.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                          mandatory="false" maxlength="${maxlength35}" placeholder="address.line2.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.townCity.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formSelectBox idKey="address.region" labelKey="address.province" path="regionIso" mandatory="true"
                                           skipBlank="false" skipBlankMessageKey="address.selectProvince" items="${regions}"
                                           itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
                                           selectedValue="${addressForm.regionIso}"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength9}"
                                          placeholder="address.zipcode.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength15}" placeholder="address.phone.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.email.placeholder"/>
            </div>
        </div>
    </c:when>
    <c:when test="${country == 'JP'}">
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.firstName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.lastName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line1.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line2.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.townCity.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formSelectBox idKey="address.region" labelKey="address.prefecture" path="regionIso" mandatory="true"
                                           skipBlank="false" skipBlankMessageKey="address.selectPrefecture" items="${regions}"
                                           itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
                                           selectedValue="${addressForm.regionIso}"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.postalcode" labelKey="address.postcode" path="postcode"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength9}"
                                          placeholder="address.postcode.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength15}" placeholder="address.phone.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.email.placeholder"/>
            </div>
        </div>
    </c:when>
    <c:when test="${regions.size() > 0}">
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.firstName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.lastName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line1.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                          mandatory="false" maxlength="${maxlength35}" placeholder="address.line2.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.townCity.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formSelectBox idKey="address.region" labelKey="address.state" path="regionIso" mandatory="true"
                                           skipBlank="false" skipBlankMessageKey="address.state" items="${regions}"
                                           itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
                                           selectedValue="${addressForm.regionIso}"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength9}"
                                          placeholder="address.zipcode.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength15}" placeholder="address.phone.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.email.placeholder"/>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.firstName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.lastName.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.line1.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="form-control"
                                          mandatory="false" maxlength="${maxlength35}" placeholder="address.line2.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength35}"
                                          placeholder="address.townCity.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.postcode" labelKey="address.zipcode" path="postcode"
                                          inputCSS="form-control" mandatory="true" maxlength="${maxlength9}"
                                          placeholder="address.zipcode.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.phone" labelKey="address.phone" path="phone" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength15}" placeholder="address.phone.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field required">
                <formElement:formInputBox idKey="address.email" labelKey="address.email" path="email" inputCSS="form-control"
                                          mandatory="true" maxlength="${maxlength35}" placeholder="address.email.placeholder"/>
            </div>
        </div>
    </c:otherwise>
</c:choose>

