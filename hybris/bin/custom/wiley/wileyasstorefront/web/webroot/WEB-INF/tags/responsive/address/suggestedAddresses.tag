<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<%@ attribute name="wileyBillingAddressForm" required="true" type="com.wiley.storefrontcommons.forms.WileyAddressForm"%>
<%@ attribute name="suggestionPage" required="false" type="java.lang.String"%>

<c:set var="suggestionButtonId"
       value="${suggestionPage eq 'OrderEdit' ? 'suggestionAddressSubmitOrderEdit' : 'suggestionAddressSubmit'}"/>

<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <form>
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true"></span><span class="sr-only"><spring:message
                        code="address.suggestion.button.close"/></span>
                </button>
                <h4 id="suggestedAddressesModalTitle" class="modal-title">
                    Wiley Author Services
                </h4>
            </div>
            <div class="modal-body">
                <c:choose>
                    <c:when test="${suggestions.hasSuggestions}">
                        <address:suggestedAddressHasSuggestions wileyBillingAddressForm="${wileyBillingAddressForm}"
                                                                suggestionSubmitBtnId="${suggestionButtonId}"/>
                    </c:when>
                    <c:otherwise>
                        <address:suggestedAddressNoSuggestions wileyBillingAddressForm="${wileyBillingAddressForm}"
                                                               suggestionSubmitBtnId="${suggestionButtonId}"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </form>
    </div>
</div>
