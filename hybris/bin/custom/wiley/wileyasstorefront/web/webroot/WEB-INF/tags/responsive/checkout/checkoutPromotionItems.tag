<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

 
<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${ycommerce:doesPotentialPromotionExistForAbstractOrderEntry(cartData, entry.entryNumber)}">
	<div class="item-price-discount">
		<c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
			<c:set var="displayed" value="false"/>
			<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
				<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
					<c:set var="displayed" value="true"/>
					<p>
						<ycommerce:testId code="cart_potentialPromotion_label">
							${promotion.description}
						</ycommerce:testId>
					</p>
				</c:if>
			</c:forEach>
		</c:forEach>
	</div>
</c:if>
<c:if test="${ycommerce:doesAppliedPromotionExistForAbstractOrderEntry(cartData, entry.entryNumber)}">
	<div class="item-price-discount">
		<c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
			<c:set var="displayed" value="false"/>
			<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
				<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
					<c:set var="displayed" value="true"/>
					<p>
						<ycommerce:testId code="cart_appliedPromotion_label">
							${promotion.description}
						</ycommerce:testId>
					</p>
				</c:if>
			</c:forEach>
		</c:forEach>
	</div>
</c:if>