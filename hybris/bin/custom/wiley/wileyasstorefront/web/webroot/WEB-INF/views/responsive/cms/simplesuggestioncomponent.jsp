<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>


<c:choose>
    <c:when test="${not empty suggestions and component.maximumNumberProducts > 0}">
        <div class="recommended-items-wrapper">
            <div class="recommended-items-title">${component.title}</div>
            <div class="recommended-items-carousel-wrapper">
                <div class="recommended-items owl-carousel">
                    <c:forEach end="${component.maximumNumberProducts}" items="${suggestions}" var="suggestion">
                        <product:productSuggestion
                            product="${suggestion}"
                            displayProductPrices="${component.displayProductPrices}"
                            displayProductTitles="${component.displayProductTitles}" />
                    </c:forEach>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>
