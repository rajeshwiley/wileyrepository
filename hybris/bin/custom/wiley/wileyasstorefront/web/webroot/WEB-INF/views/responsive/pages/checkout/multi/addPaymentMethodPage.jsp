<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="wileyAddonPayment" tagdir="/WEB-INF/tags/addons/wileystorefrontcommons/responsive/payment" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags/responsive/error" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" renderAntiClickJacking="true">
    <jsp:attribute name="pageScripts">
        <script src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/payment/wileyPaymentForm.js"></script>
        <script>HostedSession.errorMessages = ${hostedSessionErrorMessages};</script>
    </jsp:attribute>
    <jsp:body>
        <c:url value="${currentStepUrl}" var="choosePaymentMethodUrl"/>
        <multiCheckout:checkoutStepsBreadcrumb/>
        <error:paymentGlobalAlert messageKey="global-error-message-list"/>
        <multiCheckout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
            <jsp:body>
                <multiCheckout:addPaymentMethodStep abstractOrderData="${cartData}"/>
            </jsp:body>
        </multiCheckout:checkoutSteps>
    </jsp:body>
</template:page>