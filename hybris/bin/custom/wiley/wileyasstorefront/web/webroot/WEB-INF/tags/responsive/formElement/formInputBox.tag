<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="idKey" required="true" type="java.lang.String"%>
<%@ attribute name="labelKey" required="true" type="java.lang.String"%>
<%@ attribute name="path" required="true" type="java.lang.String"%>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@ attribute name="labelCSS" required="false" type="java.lang.String"%>
<%@ attribute name="inputCSS" required="false" type="java.lang.String"%>
<%@ attribute name="placeholder" required="false" type="java.lang.String"%>
<%@ attribute name="tooltip" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" rtexprvalue="true"%>
<%@ attribute name="autocomplete" required="false" type="java.lang.String"%>
<%@ attribute name="minlength" required="false" type="java.lang.String"%>
<%@ attribute name="maxlength" required="false" type="java.lang.String"%>
<%@ attribute name="pattern" required="false" type="java.lang.String"%>
<%@ attribute name="min" required="false" type="java.lang.String"%>
<%@ attribute name="max" required="false" type="java.lang.String"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<template:errorSpanField path="${path}">
    <ycommerce:testId code="LoginPage_Item_${idKey}">
        <label class="control-label ${labelCSS}" for="${idKey}">
            <spring:theme code="${labelKey}"/>
            <c:if test="${mandatory != null && mandatory == false}">
                <spring:theme code="login.optional"/>
            </c:if>

            <c:if test="${tooltip != null}">
                <spring:theme code="${tooltip}" var="tooltipMessage"/>
                <span class="popover-help-icon">
                	&nbsp;<a role="menuitem" tabindex="0" data-toggle="popover" aria-label="information icon"
                             data-content="${tooltipMessage}"
                             class="popover-help"></a>
                </span>
            </c:if>

        </label>

        <spring:theme code="${placeholder}" var="placeHolderMessage"/>

        <form:input cssClass="${inputCSS} form-control" id="${idKey}" path="${path}" minlength="${minlength}"
                    maxlength="${maxlength}" pattern="${pattern}" tabindex="${tabindex}" autocomplete="${autocomplete}"
                    placeholder="${placeHolderMessage}" min="${min}" max="${max}"/>

    </ycommerce:testId>
</template:errorSpanField>
