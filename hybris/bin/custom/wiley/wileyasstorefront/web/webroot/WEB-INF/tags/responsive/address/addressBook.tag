<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<div id="savedAddressesModal" tabindex="-1" role="dialog" aria-labelledby="savedAddressesModalTitle" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close"><span aria-hidden="true"></span><span class="sr-only">Close</span>
                </button>
                <h4 id="savedAddressesModalTitle" class="modal-title">
                    <spring:theme code="text.checkout.multi.paymentAddress.savedAddresses"/>
                </h4>
            </div>
            <div class="modal-body">
                <c:forEach items="${paymentAddresses}" var="paymentAddress" varStatus="status">
                    <form:form id="savedAddressesForm${paymentAddress.id}" action="${request.contextPath}/checkout/multi/payment-address/select" method="post">
                        <input type="hidden" name="selectedAddressCode" value="${paymentAddress.id}"/>
                        <input type="hidden" name="cartId" value="${cartData.guid}"/>
                        <div class="saved-card-info">
                            <div class="saved-info-title">
                                ${fn:escapeXml(paymentAddress.firstName)}&nbsp;
                                ${fn:escapeXml(paymentAddress.lastName)}
                            </div>
                            <div class="saved-info-value">
                                ${fn:escapeXml(paymentAddress.line1)},
                                <c:if test="${not empty paymentAddress.line2}">
                                    ${fn:escapeXml(paymentAddress.line2)},
                                </c:if>
                                ${fn:escapeXml(paymentAddress.town)},
                                <c:if test="${not empty paymentAddress.region.name}">
                                    ${fn:escapeXml(paymentAddress.region.name)}
                                </c:if>
                            </div>
                            <div class="saved-info-value">
                                ${fn:escapeXml(paymentAddress.postalCode)},
                                ${fn:escapeXml(paymentAddress.country.name)}
                            </div>
                            <div class="saved-info-value">
                                ${fn:escapeXml(paymentAddress.phone)}
                            </div>
                            <button type="submit" class="button button-main">
                                <spring:theme code="text.checkout.multi.paymentAddress.useThisAddress"/>
                            </button>
                        </div>
                    </form:form>
                </c:forEach>
            </div>
        </div>
    </div>
</div>