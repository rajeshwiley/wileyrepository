var wiley = {
	isMobileView: $(window).width() <= 640,
	/* Check tablet and mobile devices */
	checkDevice: {
		isDeviceIOS: function () {
			return navigator.userAgent.match(/(iPhone|iPod|iPad)/i);
		},
		isDeviceIPad: function () {
			return navigator.userAgent.match(/iPad/i);
		},
		isMobileSafari: function () {
			return navigator.userAgent.match(/(iPhone|iPod)/i) && !navigator.userAgent.match(/(CriOS|OPiOS|FxiOS|mercury)/i);
		},
		isAndroid: function () {
			return navigator.userAgent.match(/Android/i);
		},
		isWindows: function () {
			return navigator.userAgent.match(/(IEMobile|webOS|Lumia)/i);
		},
		isBlackBerry: function () {
			return navigator.userAgent.match(/(BlackBerry|BB10; Touch)/i);
		},
		isOpera: function () {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		isAny: function () {
			return (this.isDeviceIOS() || this.isAndroid() || this.isWindows() || this.isBlackBerry() || this.isOpera());
		}
	},
	helper: {
		isMobileDevice: function () {
			return wiley.isMobileView || wiley.checkDevice.isAny();
		},
		validateTaxes: function () {
			var form = $('#addressForm');

			// remove possible error blocks from previous server-side validation
			$(document).on('focusin', '#vatRegistrationDetailsForm\\.taxNumber', function(){
				if($(this).parent().find(".help-block").not('.ui-error').length){
					$(this).valid();
				}
			});

			$.validator.addMethod("validVat", function (value, element) {
				return this.optional(element) || jsvat.checkVAT(value, $("#address\\.country").val()).isValid === true;
			}, 'Incorrect VAT ID. Please check the number for errors.');

			form.validate({
				ignore: ".ignore",
				rules: {
					"vatRegistrationDetailsForm.taxNumber": {
						required: false,
						validVat: true
					}
				},
				messages: {
					"taxExemptionDetailsForm.taxNumber": "Please enter a tax number",
					"taxExemptionDetailsForm.taxNumberExpirationDate": "Please enter a tax code expiration date"
				},
				errorElement: "span",
				errorPlacement: function (errorElement, element) {
					var errorInBlock = $("<div class='help-block ui-error'>").append(errorElement);
					var parent = $(element).parent();

					// remove possible error blocks from previous server-side validation
					parent.find(".help-block").not('.ui-error').remove();
					parent.append(errorInBlock);
				},
				success: function (errorElement, element) {
					// also remove possible error blocks from previous server-side validation
					$(element).parent().find(".help-block").remove();
				},
				highlight: function (element, errorClass, validClass) {
					// do not highlight input with error class. Add 'has-error' class to parent instead to align with errorSpanField.tag
					$(element).parent().addClass('has-error');
				},
				unhighlight: function (element, errorClass, validClass) {
					$(element).parent().removeClass('has-error');
				}
			});

			var toggleTaxNumberRequired = function() {
				if ($('#taxExemptionDetailsForm\\.taxNumber').val() !== '' || $('#taxExemptionDetailsForm\\.taxNumberExpirationDate').val() !== '') {
					$('#taxExemptionDetailsForm\\.taxNumber').closest('.form-field').addClass('required');
					$('#taxExemptionDetailsForm\\.taxNumberExpirationDate').closest('.form-field').addClass('required');
				} else {
					$('#taxExemptionDetailsForm\\.taxNumber').closest('.form-field').removeClass('required');
					$('#taxExemptionDetailsForm\\.taxNumberExpirationDate').closest('.form-field').removeClass('required');
				}
			};

			form.find("#taxExemptionDetailsForm\\.taxNumber, #taxExemptionDetailsForm\\.taxNumberExpirationDate")
				.keyup(toggleTaxNumberRequired)
				.change(toggleTaxNumberRequired);
		},
		displayCustomMobileSelect: function displayCustomMobileSelect(select) {
			var selectComponent = select.parents('.select-component');
			var em = selectComponent.find("em");
			var optionSelected;

			selectComponent.addClass("device");

			if (!em.length) {
				em = $("<em></em>");
				selectComponent.prepend(em);
			}

			optionSelected = select.find('option:selected').text();
			em.text(optionSelected);
		},
		destroyMobileCustomSelect: function destroyMobileCustomSelect() {
			var selects = $(".select-component");
			if (selects.length) {
				selects.each(function (i, el) {
					var em, selectComponent;
					selectComponent = $(el);
					em = selectComponent.find("em");
					if (em.length) {
						em.remove();
						selects.removeClass("device");
						wiley.displayCustomSelect(selectComponent);
					}
				});
			}
		},
		destroyLaptopCustomSelect: function destroyLaptopCustomSelect() {
			var selects = $(".select-component");
			if (selects.length) {
				selects.each(function (i, el) {
					var select, selectComponent, uiSelectMenuButton;
					selectComponent = $(el);
					select = selectComponent.find("select");
					uiSelectMenuButton = selectComponent.find(".ui-selectmenu-button");

					if (uiSelectMenuButton.length) {
						selectComponent.find("select").selectmenu("destroy");
						wiley.helper.displayCustomMobileSelect(select);
					}
				});
			}
		},
		setSelectComponentPlaceholder: function (element) {
			var elementParent = $(element).parent();
			var defaultValue = elementParent.find('.ui-selectmenu-text');
			var defaultOption = $(element).find('option:selected');
			if (defaultOption.length) {
				var value = defaultOption.attr('value');
				if (!value && defaultOption.text() === defaultValue.text()) {
					defaultValue.addClass('placeholder');
				}
			}
		},
		KEY_CODE_MAP: {
			space: 32,
			enter: 13,
			escape: 27,
			tab: 9,
			keydown: 40
		}
	},
	/* Viewport fix for iPad */
	iPadViewport: function iPadViewport() {
		var viewport = $("meta[name='viewport']");
		if (wiley.checkDevice.isDeviceIPad()) {
			viewport.attr("content", "width=1024");
		}
	},
	owlCarousel: function owlCarousel() {
		var defaultOptions = {
			nav: true,
			items: 2,
			navText: '',
			navElement: 'a tabindex="0"',
			responsive: {
				0: {
					items: 1,
					margin: 5,
					nav: false,
					dots: true
				},
				641: {
					items: 2,
					nav: true,
					dots: false,
					margin: 147
				}
			}
		};
		var carouselWrapper = $('.recommended-items');
		var recommendedItemsComponent = $('.recommended-items-carousel-wrapper');
		var recommendedItems = $('.recommended-items.owl-carousel').find('.recommended-item');

		recommendedItemsComponent.append('<div class="wiley-loader"></div>');

		for (var count = 0; count < recommendedItems.length; count += 2) {
			recommendedItems.slice(count, count + 2).wrapAll("<div class='carouselItem'></div>");
		}

		$(".owl-carousel").each(function () {
			var configuration = $(this).data('component-model') || defaultOptions;

			$(this).on('initialized.owl.carousel', function () {
				if ($(this).hasClass('recommended-items')) {
					carouselWrapper.addClass('visible');
					recommendedItemsComponent.find('.wiley-loader').remove();
				}
			});

			$(this).owlCarousel(configuration);

			//Accessibility start
			var navigation = $(this).find('.owl-nav:not(.disabled)');
			if (navigation.length) {
				navigation.find('a').keydown(function (e) {
					if (e.keyCode === 13) {
						$(this).trigger('click');
					}
				});
			}
			//Accessibility end
		});
	},
	setCharactersCounter: function setCharactersCounter() {
		$('.characters-counter').each(function () {
			var inputContainer = $(this);
			var input = inputContainer.find('.form-control');
			var result = inputContainer.find('.characters-counter-value');
			var maxLength = input.attr("maxlength");
			var inputValue = input.val();
			var inputLength = inputValue.length;
			var leftCharacters = inputLength ? maxLength - inputLength : maxLength;
			result.text(leftCharacters);

			input.on('keyup keypress', function () {
				var value = $(this).val();
				var length = value.length;
				leftCharacters = maxLength - length;
				if (leftCharacters >= 0) {
					result.text(leftCharacters);
				} else {
					leftCharacters = 0;
				}

				/*
				 * On android, the maxlength of an input allows you to write more than the maxlength
				 * in this input. It's only when you quit the input, that the text above the
				 * maxlength gets deleted from the input. I wanted to force the forbidding of
				 * writing above the maxlength.
				 */
				if (length >= maxLength) {
					$(this).val(value.substring(0, maxLength));
				}
			});
		});
	},
	setMainHeaderNavigation: function setMainHeaderNavigation() {
		$(".navbar-toggle").on("click", function () {
			$('body, html').toggleClass("has-drawer");
			$('.menu-item.user-name').toggleClass("visible");

		});

		$('.dropdown-menu').click(function (e) {
			e.stopPropagation();
		});
	},
	initPaymentOptionsTabs: function initPaymentOptionsTabs() {
		$('.tab-links').on('click', 'a', function () {
			var radioBtn = $(this).find('input');
			if (radioBtn.attr('disabled')) {
				return false;
			} else {
				radioBtn.attr('checked', 'true');
			}
		});
	},
	displayCustomSelect: function displayCustomSelect() {
		var selects = $(".select-component");
		selects.each(function (i, el) {
			var selectComponent = $(el);
			var select = selectComponent.find("select");

			if (wiley.helper.isMobileDevice() && !wiley.checkDevice.isDeviceIPad()) {

				wiley.helper.displayCustomMobileSelect(select);

				select.change(function () {
					wiley.helper.displayCustomMobileSelect($(this));
				});

				selectComponent.on("touchstart", "select", function () {
					selectComponent.addClass("select-open");
				});

				selectComponent.on("blur", "select", function () {
					selectComponent.removeClass("select-open");
				});
			} else {
				select.selectmenu({
					width: 'inherit',
					appendTo: $(this).closest('.modal').length ? $(this).closest('.select-component') : 'inherit',
					create: function (event, ui) {
						wiley.helper.setSelectComponentPlaceholder(event.target);
						$(this).on('change', function () {
							$(this).selectmenu("refresh");
						});
					},
					open: function (event, ui) {
						selectComponent.addClass("select-open");
						$('.ps-scrollbar-container').perfectScrollbar("update"); // visible perfectScrollbar by click on menu button
					},

					close: function () {
						selectComponent.removeClass("select-open");
					}
				}).selectmenu("menuWidget").addClass("ps-scrollbar-container always-visible").perfectScrollbar();
			}

		});


	},
	initPopover: function initPopover() {
		var popoverElements = $('[data-toggle="popover"]');
		var arrow;

		$('.popover-help-icon').click(function (e) {
			e.stopPropagation();
			e.preventDefault();
			return false;
		});

		if (popoverElements.length) {
			popoverElements.each(function (index, icon) {
				var container = $(this).data('container') || 'body';
				var containerClass = $(this).data('container-class') || '';
				$(icon).popover({
					placement: 'bottom',
					html: "true",
					container: container,
					animation: false,
					template: '<div class="popover ' + containerClass + '"><div class="arrow"></div><div class="popover-content"></div></div>',
					trigger: "click"
				}).on("keydown", function () {
					var _this = this;
					if (e.keyCode === wiley.KEY_CODE_MAP.enter) {
						if ($('.popover').hasClass('in')) {
							$(this).popover("hide");
						} else {
							$(this).popover("show");
						}
					} else if (e.keyCode === wiley.KEY_CODE_MAP.escape || e.keyCode === wiley.KEY_CODE_MAP.tab) {
						$(this).popover("hide");
					} else if (e.keyCode === wiley.KEY_CODE_MAP.keydown) {
						var focusableElements = $('.popover.in').find('select, input, textarea, button, a[href]').filter(':visible').not(':disabled');
						if (focusableElements.length) {
							focusableElements.first().focus();
						}
					}
					$(".popover.in").on("keydown", function (e) {
						if (e.keyCode === wiley.KEY_CODE_MAP.tab) {
							$(_this).popover('hide');
							$(_this).focus();
						}
					});
				});

			});
		}

		if (wiley.checkDevice.isDeviceIOS()) {
			$('body').css({'cursor': 'pointer'});
		}

		$('html').on('click', function (e) {
			if ($(e.target).attr('data-toggle') !== 'popover' && !$(e.target).parents().is('.popover.in')) {
				popoverElements.popover('hide');
			}
		});


	},
	showDatePicker: function showDatePicker() {
		var datepicker = $(".datepicker");
		var datepickerIcon;

		function isDateExpired(dateToday, datePickerValue) {
			var parsedDate = new Date(datePickerValue).setHours(0, 0, 0, 0);
			var currentDate = dateToday.setHours(0, 0, 0, 0);
			return parsedDate < currentDate;
		}

		if (wiley.helper.isMobileDevice()) {
			if (datepicker.length) {
				datepicker.each(function () {
					var originalInput = $(this);
					var originalInputDate = $.datepicker.parseDate("mm/dd/yy", $(originalInput).val());
					var clonedInput = $(originalInput).clone()
						.attr("id", $(originalInput).attr("id") + "Cloned")
						.attr("type", "date")
						.addClass("ignore")
						.removeAttr("name")
						.val($.datepicker.formatDate("yy-mm-dd", originalInputDate));
					$(clonedInput).appendTo($(originalInput).parent());

					datepickerIcon = $(this).parent().find("label");
					datepickerIcon.unbind("click.datepicker");
					$(originalInput).unbind("change.datepicker");
					$(originalInput).datepicker("destroy");

					$(clonedInput).change(function () {
						var dateValue = $(this).val();
						var dateToday = new Date();

						if (dateValue.length === 0) {
							$(originalInput).val(dateValue);
						}
						else if (isDateExpired(dateToday, dateValue)) {
							var curDate = $.datepicker.formatDate("mm/dd/yy", dateToday);
							$(originalInput).val(curDate);
							$(this).val(curDate);
						}
						else {
							var newDate = new Date(dateValue);
							var formattedDate = $.datepicker.formatDate("mm/dd/yy", newDate);
							$(originalInput).val(formattedDate);
						}
						$(originalInput).change();
					});
				});
			}

		} else {
			if (datepicker.length) {
				var dateToday = new Date();

				datepicker.each(function () {
					$(this).attr("type", "text");
					$(this).unbind("blur.inputDate");

					datepickerIcon = $(this).parent().find("label");

					$(this).datepicker({
						beforeShow: function (input, inst) {
							var calendar = inst.dpDiv;
							setTimeout(function () {
								calendar.position({
									my: 'right top',
									at: 'right bottom',
									collision: 'none',
									of: input
								});
							}, 1);
						},
						onSelect: function () {
							$(this).change();
						},

						minDate: dateToday,
						showOtherMonths: true,
						selectOtherMonths: true,
						autoSize: true,
						hideIfNoPrevNext: true,
						dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
						nextText: "",
						prevText: "",
						monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"]
					});

					// toggle show/hide date picker by click icon
					datepickerIcon.bind("click.datepicker", function (event) {
						event.preventDefault();

						return $(this).datepicker("widget").is(":visible") ? $(this).off("focus").datepicker("hide") : $(this).on("focus").datepicker("show");
					});

					/* Add mask on keyboard changes */
					var maskBlock = $("<span></span>").text("MM/dd/yyyy").addClass("date-mask-string");
					var maskParentBlock = $(this).parent();
					var maskString = maskBlock.text();

					var maskChars = maskString.split("");
					maskBlock.empty();

					$.each(maskChars, function (i) {
						var span = document.createElement("span");
						span.innerText = maskChars[i];
						maskBlock.append(span);
					});

					function displayMask() {
						var countValues = datepicker.val().length;
						maskBlock.find("span").css("opacity", 1);
						maskBlock.find("span:lt(" + countValues + ")").css("opacity", 0);
					}

					function removeMask() {
						maskParentBlock.find(".date-mask-string").remove();
					}

					$(this).mask('00/00/0000');

					$(this).on("keyup", function () {
						if (maskParentBlock.children(".date-mask-string").length > 0) {
							displayMask();
						} else {
							maskParentBlock.append(maskBlock);
							displayMask();
						}
						if ($(this).val() === "") {
							removeMask();
						}

					});

					$(this).on("change.datepicker", function (event) {
						removeMask();
						var datePickerValue = $(this).val();

						if (isDateExpired(dateToday, datePickerValue)) {
							var newValue = $.datepicker.formatDate("mm/dd/yy", dateToday);
							$(this).val(newValue);
						}

					});
					/* Add mask on keyboard changes */
				});
			}
		}
	},
	stickyHeader: function stickyHeader() {
		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var header = $('.header-container');
		var navbarHeight = header.outerHeight();
		header.addClass('nav-down');

		$(window).scroll(function (event) {
			didScroll = true;
		});

		setInterval(function () {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(this).scrollTop();

			// Make sure they scroll more than delta
			if (Math.abs(lastScrollTop - st) <= delta)
				return;

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight) {
				// Scroll Down
				if (header.hasClass('nav-down')) {
					header.removeClass('nav-down').addClass('nav-up');
					wiley.helper.headerIsVisible = false;
				}

			} else {
				// Scroll Up
				if (st + $(window).height() < $(document).height()) {
					if (header.hasClass('nav-up')) {
						header.removeClass('nav-up').addClass('nav-down');
						wiley.helper.headerIsVisible = true;
					}

				}
			}

			lastScrollTop = st;
		}
	},
	setFlashMessage: function setFlashMessage() {
		var flashMsgContainer = $('.flash-message-wrapper');
		var flashMsg = flashMsgContainer.find('.flash-message');
		var header = $('.header-container');
		var startPosition = wiley.isMobileView ? 60 : 48; // height of the header

		function calculatePositions(startPosition) {
			$.each(flashMsgContainer, function (index, item) {
				var topPos = startPosition;
				if (index !== 0) {
					for (var i = index - 1; i >= 0; i--) {
						topPos += flashMsgContainer.eq(i).height();
					}
				}
				$(item).css('top', topPos);
			});
		}

		function getStartPosition() {
			return wiley.helper.headerIsVisible ? startPosition : 0;
		}

		function init() {

			Object.defineProperty(wiley.helper, 'headerIsVisible', {
				configurable: true,
				get: function () {
					return header.hasClass('nav-down');
				},
				set: function () {
					calculatePositions(getStartPosition());
				}
			});

			calculatePositions(getStartPosition());

			flashMsg.each(function (i, el) {
				$(el).on('click', '.close', function () {
					$(el).hide();
					setTimeout(function () {
						calculatePositions(getStartPosition());
					}, 0);

				});
			});
		}

		init();
	},
	checkSurveyFormRequiredFields: function checkSurveyFormRequiredFields() {
		var surveyForm = $('#shopping-cart-survey-form');
		var submitBtn = surveyForm.find('button[type="submit"]');
		var input = surveyForm.find('.form-control');
		var radioButtons = surveyForm.find('input[type="radio"]');

		function checkInputs() {
			var ifEmptyControls = !input.val().length || !radioButtons.filter(':checked').length;
			submitBtn.attr('disabled', ifEmptyControls);
			// prevent form submit with empty inputs
			if (ifEmptyControls) {
				return false;
			}
		}

		// disable submit button by default
		submitBtn.attr('disabled', 'disabled');

		surveyForm.on('submit', checkInputs)
			.on('keyup', input, checkInputs)
			.on('change', radioButtons, checkInputs);
	},
	sendConfirmationMessage: function sendConfirmationMessage() {
		var confirmationPage = document.getElementById('wileyas-confirmation-message-page');
		if ($(confirmationPage).length > 0) {
			var notificationUrl = confirmationPage.getAttribute('notificationUrl');
			$.ajax({
				type: "POST",
				url: notificationUrl
			});
		}
	},
	bindOrientationChange: function bindOrientationChange() {
		$(window).on("orientationchange", function () {
			setTimeout(function () {
				wiley.isMobileView = ($(window).width() <= 640);
				if (!wiley.checkDevice.isDeviceIPad()) {
					if (wiley.helper.isMobileDevice()) {
						wiley.helper.destroyLaptopCustomSelect();
					} else {
						wiley.helper.destroyMobileCustomSelect();
					}
				}
				wiley.setMultilineEllipsis();
			}, 0);
		});
	},
	setMultilineEllipsis: function setMultilineEllipsis() {

		if ($('.recommended-items-carousel-wrapper').length && !wiley.isMobileView) {
			$('.recommended-item-title').truncate({
				width: '437'
			});
		}

	},
	preventMultipleFormSubmit: function preventMultipleFormSubmit() {

		$(document).on("submit", "form", function () {
			var submitBtn = $(this).find("button[type='submit']");
			submitBtn.prop('disabled', true);
		});


	},

	disableAutoZoomIOS: function disableAutoAoomIOS(){
		var $objHead = $( 'head' );

		var zoomDisable = function() {
			$objHead.find( 'meta[name=viewport]' )
				.attr("content", "width=device-width, initial-scale=1.0, user-scalable=0");
		};

		var zoomEnable = function() {
			$objHead.find( 'meta[name=viewport]' )
				.attr("content", "width=device-width, initial-scale=1.0, user-scalable=1");
		};

		if( wiley.checkDevice.isDeviceIOS() ) {
			$(".input-group input, .input-group textarea, .select-component.device")
				.on( { 'touchstart' : function() { zoomDisable(); } } )
				.on( { 'touchend' : function() { setTimeout( zoomEnable , 500 ); } } );
		}
	},

	init: function init() {
		Object.keys(this).forEach(function (key) {
			if (typeof wiley[key] === 'function' && wiley[key].name !== 'init') {
				try {
					wiley[key].call(this);
				} catch (e) {
					console.error(e);
				}

			}
		});
	}
};

$(document).ready(function () {

	wiley.init();

});




