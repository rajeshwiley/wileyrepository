<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>


<spring:url value="/my-account/order/" var="orderDetailsUrl"/>
<c:set var="searchUrl" value="/my-account/orders?sort=${searchPageData.pagination.sort}"/>

<c:set var="covered">
    <spring:theme code="text.account.orderHistory.covered"/>
</c:set>

<c:if test="${empty searchPageData.results}">
	<div class="container shopping-cart-page shopping-cart-order-history-empty">
		<div class="row">
			<div class="col-xs-12">
				<div class="shopping-cart-top">
					<h1 class="main-page-title icon-confirmation"><spring:theme code="text.account.orderHistory" /></h1>
				</div>
				<div class="no-orders-found">
					<p><spring:theme code="text.account.orderHistory.noOrders" /></p>
				</div>
			</div>
		</div>
	</div>
</c:if>
<c:if test="${not empty searchPageData.results}">
<div class="container shopping-cart-page shopping-cart-order-history">
		<div class="row">
			<div class="col-xs-12">
				<div class="shopping-cart-top">
					<h1 class="main-page-title icon-confirmation"><spring:theme code="text.account.orderHistory" /></h1>
				</div>
				<div class="sort-panel">
					<span class="sort-panel-title"></span>
					<div class="select-component sort-panel-component"></div>
				</div>
				<div class="products-table order-history-table">
					<div class="table-head">
						<div class="table-head-item item-product-detail"><spring:theme code="text.account.orderHistory.productdetail" /></div>
						<div class="table-head-item item-order-number"><spring:theme code="text.account.orderHistory.ordernumber" /></div>
						<div class="table-head-item item-data-placed"><spring:theme code="text.account.orderHistory.dateplaced" /></div>
						<div class="table-head-item item-total"><spring:theme code="text.account.orderHistory.amountcharged" /></div>
						<div class="table-head-item item-action-button"></div>
					</div>
					<div class="table-body">
						<c:forEach items="${searchPageData.results}" var="order">
							<div class="table-body-row">
								<div class="table-body-item item-product-detail">
									<p class="item-title-mobile"><spring:theme code="text.account.orderHistory.productdetail" /></p>
									<c:forEach items="${order.entries}" var="orderentry">
										<div class="item-value">
											<span class="product-label">${orderentry.product.name}</span>${orderentry.article.journalName}</div>
									</c:forEach>
								</div>
								<div class="table-body-item item-order-number">
									<p class="item-title-mobile"><spring:theme code="text.account.orderHistory.ordernumber" /></p>
									<p class="item-value">${order.code}</p>
								</div>
								<div class="table-body-item item-data-placed">
									<p class="item-title-mobile"><spring:theme code="text.account.orderHistory.dateplaced" /></p>
									<p class="item-value">
										<fmt:formatDate pattern="dd MMMM YYYY" value="${order.placed}" />
									</p>
								</div>
								<div class="table-body-item item-total">
									<p class="item-title-mobile"><spring:theme code="text.account.orderHistory.amountcharged" /></p>
									<c:set var="orderTotalValue" value="${order.total.formattedValue}" />
									<p class="item-value"><c:out value="${order.orderType == 'AS_FUNDED' ?  covered : orderTotalValue}" /></p>
								</div>
								<div class="table-body-item item-action-button">
									<a href="${orderDetailsUrl}${order.guid}" class="button button-main small"><spring:theme code="text.account.orderHistory.manageorder" /></a>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
				<div class="account-orderhistory-pagination">
					<nav:pagination top="false" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}"
						supportShowAll="${isShowAllAllowed}"
						searchPageData="${searchPageData}" searchUrl="${searchUrl}"
						numberPagesShown="${numberPagesShown}" />
				</div>
			</div>
		</div>
	</div>
</c:if>
