<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<spring:htmlEscape defaultHtmlEscape="true"/>
<spring:url value="/cart/voucher/apply" var="applyVoucherAction"/>
<spring:url value="/cart/voucher/remove" var="removeVoucherAction"/>

<c:set var="containerClass">
    <c:choose>
        <c:when test="${not empty errorMsg}">has-error</c:when>
        <c:when test="${not empty successMsg}">has-success</c:when>
        <c:otherwise></c:otherwise>
    </c:choose>
</c:set>

<c:set var="errorClass">
    <c:if test="${not empty errorMsg}">
        error
    </c:if>
</c:set>

    <form:form id="applyVoucherForm" action="${applyVoucherAction}" method="post" commandName="voucherForm" class="discount-code-form" _lpchecked="1">

        <spring:theme code="text.voucher.apply.input.placeholder" var="voucherInputPlaceholder"/>
        <div class="discount-code-form-title"><label for="js-voucher-code-text"><spring:theme code="text.voucher.form.title"/></label></div>
        <div class="input-group">
            <div class="form-field">
                <c:set var="couponCodeInputPlaceholder"><spring:theme code='text.voucher.apply.input.placeholder'/></c:set>
                <form:input id="js-voucher-code-text" path="voucherCode" name="voucher-code" placeholder="${couponCodeInputPlaceholder}" required="" class="discount-code-input ${errorClass} form-control" type="text"/>
                <input type="hidden" name="cartId" value="${cartData.guid }" />
            </div>
            <span class="input-group-btn">
            <button type ="button" id="js-voucher-apply-btn" class="button form-button small"><spring:theme code="text.voucher.apply.button.label"/></button></span>
        </div>
        <c:if test="${not empty successMsg}">
            <div class="form-field-message"><img src="${commonResourcePath}/images/applied-discount-icon.png" srcset="${commonResourcePath}/images/applied-discount-icon@2x.png 2x, ${commonResourcePath}/images/applied-discount-icon@3x.png 3x"><span>${successMsg}</span></div>
        </c:if>
        <c:if test="${not empty errorMsg}">
            <div class="form-field-message error-message"><img src="${commonResourcePath}/images/error-discount-icon.png" srcset="${commonResourcePath}/images/error-discount-icon@2x.png 2x, ${commonResourcePath}/images/error-discount-icon@3x.png 3x"><span>${errorMsg}</span></div>
        </c:if>
    </form:form>
