<%@ tag body-content="empty" trimDirectiveWhitespaces="true" import="java.math.BigDecimal"%>
<%@ attribute name="priceData" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="displayFreeForZero" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayCancelled" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="isZero" value="${priceData.value.compareTo(BigDecimal.ZERO) eq 0}"/>
<c:set var="displayFreeForZero" value="${displayFreeForZero != null ? displayFreeForZero : true}"/>
<c:set var="displayCancelled" value="${displayCancelled != null ? displayCancelled : false}"/>
<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
--%>
<c:choose>
	<c:when test="${isZero and displayCancelled}">
	 <spring:theme code="text.cancelled" text="CANCELLED"/>
	</c:when>
	<c:when test="${isZero and displayFreeForZero}">
	 <spring:theme code="text.free" text="FREE"/>
	</c:when>
	<c:otherwise>
		${priceData.formattedValue}
	</c:otherwise>
</c:choose>
