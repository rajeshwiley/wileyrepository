<%@ page trimDirectiveWhitespaces="true" contentType="text/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

{
 "globalMessages": "<spring:escapeBody javaScriptEscape="true"><common:globalMessages /></spring:escapeBody>"
 <spring:hasBindErrors name="addressForm">,
 "billingInformationStep": "<spring:escapeBody javaScriptEscape="true"><order:accountOrderEditingBillingInformationStep/></spring:escapeBody>"
 </spring:hasBindErrors>
}