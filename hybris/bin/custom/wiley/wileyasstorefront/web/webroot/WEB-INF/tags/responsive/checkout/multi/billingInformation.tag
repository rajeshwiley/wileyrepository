<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="panel">
	<a data-toggle="collapse" data-parent="#checkout-accordion"
		href="#cart-billing-info" class="panel-title"> 
		<spring:theme code="text.multi.checkout.billing.information" /></a>
	<div id="cart-billing-info" class="panel-collapse collapse"></div>
</div>