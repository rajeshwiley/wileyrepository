<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<div class="tab-content order-review-payment">
    <c:set value="${order.paymentMode}" var="selectedPaymentMode" />
    <h3 class="order-review-section-title"><spring:theme code="text.multi.checkout.orderreview.payment" /></h3>
    <div class="order-review-items-list">
        <div class="order-review-item">
            <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.paymentmethod" /></div>
            <div class="order-review-value"><spring:theme code="text.multi.checkout.cartdata.paymentmode.${order.paymentMode}"/></div>
        </div>
        <c:choose>
            <c:when test="${selectedPaymentMode eq 'card'}">
                <div class="order-review-item">
                    <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.paymenttype" /></div>
                    <div class="order-review-value">${order.paymentInfo.cardTypeData.name}&nbsp;<spring:theme code="text.multi.checkout.paymentmethod.card.pattern"/>&nbsp;${order.wileyPaymentInfo.creditCardNumber}</div>
                </div>
            </c:when>
            <c:when test="${selectedPaymentMode eq 'invoice'}">
                <c:if test="${not empty order.purchaseOrderNumber}">
                    <div class="order-review-item">
                        <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.purchase.order.number" /></div>
                        <div class="order-review-value">${order.purchaseOrderNumber}</div>
                    </div>
                </c:if>
                <c:if test="${not empty order.userNotes}">
                    <div class="order-review-item">
                        <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.notes.on.invoice" /></div>
                        <div class="order-review-value">${order.userNotes}</div>
                    </div>
                </c:if>
            </c:when>
            <c:when test="${selectedPaymentMode eq 'proforma'}">
                <c:if test="${not empty order.purchaseOrderNumber}">
                    <div class="order-review-item">
                        <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.purchase.order.number" /></div>
                        <div class="order-review-value">${order.purchaseOrderNumber}</div>
                    </div>
                </c:if>
                <c:if test="${not empty order.userNotes}">
                    <div class="order-review-item">
                        <div class="order-review-label"><spring:theme code="text.multi.checkout.orderreview.notes.on.proforma" /></div>
                        <div class="order-review-value">${order.userNotes}</div>
                    </div>
                </c:if>
            </c:when>
        </c:choose>
    </div>
</div>