<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty product.includeFeatures}">
	<c:forEach items="${product.includeFeatures}" var="feature">
		<div class="extra-item">
			<p class="item-title">${feature.name}</p>
			<p class="item-value">
				<c:forEach items="${feature.featureValues}" var="featureValue" varStatus="status">
					${featureValue.value}
					<c:choose>
						<c:when test="${feature.range}">
							${not status.last ? '-' : feature.featureUnit.symbol}
						</c:when>
						<c:otherwise>
							${feature.featureUnit.symbol}
							${not status.last ? '<br/>' : ''}
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</p>
		</div>
	</c:forEach>
</c:if>
