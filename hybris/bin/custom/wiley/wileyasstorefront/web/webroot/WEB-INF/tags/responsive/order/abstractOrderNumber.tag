<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="abstractOrderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="messageCode" required="true" type="java.lang.String" %>

<div class="product-cart-id">
    <c:if test="${not empty abstractOrderData.code}">
        <spring:theme code="${messageCode}"/>
        <b> ${abstractOrderData.code}</b>
    </c:if>
</div>