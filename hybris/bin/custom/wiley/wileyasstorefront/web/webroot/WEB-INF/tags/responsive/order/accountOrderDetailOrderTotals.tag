<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty order}">
    <div class="product-summ-total">
        <div class="total-row">
            <div class="item-total-label"><spring:message code="text.account.orderHistory.entry.subTotal"/></div>
            <div class="item-total-value">${order.subTotalWithoutDiscount.formattedValue}</div>
        </div>
        <c:if test="${order.totalDiscounts.value gt 0}">
            <div class="total-row">
                <div class="item-total-label"><spring:message code="text.account.order.discount"/></div>
                <div class="item-total-value">-&nbsp;${order.totalDiscounts.formattedValue}</div>
            </div>
        </c:if>
        <c:if test="${order.taxAvailable}">
             <div class="total-row">
              	 <div class="item-total-label"><spring:message code="text.account.order.netTax"/></div>
                 <div class="item-total-value">${order.totalTax.formattedValue}</div>
             </div>
        </c:if>
        <div class="total-row">
            <div class="item-total-label"><spring:message code="text.account.order.total"/></div>
            <div class="item-total-value">${order.totalPriceWithTax.formattedValue}</div>
        </div>
    </div>
</c:if>