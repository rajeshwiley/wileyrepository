<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="containerCSS" required="false" type="java.lang.String" %>

<div class="product-summ-total">
    <div class="total-row">
        <div class="item-total-label">
            <spring:theme code="text.multi.checkout.orderreview.subtotal" />
        </div>
        <div class="item-total-value">
            <format:price priceData="${order.subTotalWithoutDiscount}" displayFreeForZero="FALSE"/>
        </div>
    </div>
    <c:if test="${order.totalDiscounts.value > 0}">
    <div class="total-row">
        <div class="item-total-label"><spring:theme code="text.account.order.discount"/></div>
        <div class="item-total-value">
            - 
            <format:price priceData="${order.totalDiscounts}" displayFreeForZero="false"/>
        </div>
    </div>
    </c:if>
    <c:if test="${order.taxAvailable}">
    	<div class="total-row">
    		<div class="item-total-label"><spring:theme code="text.account.order.netTax"/></div>
    		<div class="item-total-value">
    			<format:price priceData="${order.totalTax}" displayFreeForZero="FALSE"/>
    		</div>
    	</div>
    </c:if>
    <div class="total-row">
        <div class="item-total-label">
            <spring:theme code="text.multi.checkout.orderreview.total" />
        </div>
        <div class="item-total-value">
            <c:choose>
                <c:when test="${order.net}">
                    <format:price priceData="${order.totalPriceWithTax}" displayFreeForZero="FALSE"/>
                </c:when>
                <c:otherwise>
                    <format:price priceData="${order.totalPrice}" displayFreeForZero="FALSE"/>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>