<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<div class="form-wrapper order-review-block">
    <div class="form-top">
        <div class="form-title">
            <spring:theme code="text.multi.checkout.orderreview.productdetails"/>
            <a data-toggle="collapse"
               href="#orderReviewProductTable" role="button" aria-expanded="false"
               aria-controls="orderReviewProductTable"
               class="button-view-details">
                <spring:theme code="text.multi.checkout.orderreview.button.view"/>
                <span
                        class="word-more"><spring:theme code="text.multi.checkout.orderreview.button.more"/></span>
                <span class="word-less"><spring:theme code="text.multi.checkout.orderreview.button.less"/></span>
            </a>
        </div>
        <div id="orderReviewProductTable" class="order-review-table collapse show in">
            <div class="products-table">
                <div class="table-head">
                    <div class="table-head-item item-product">
                        <spring:theme code="text.multi.checkout.orderreview.product"/>
                    </div>
                    <div class="table-head-item item-quantity">
                        <spring:theme code="text.multi.checkout.orderreview.quantity"/>
                    </div>
                    <div class="table-head-item item-total">
                        <spring:theme code="text.multi.checkout.orderreview.total"/>
                    </div>
                </div>
                <div class="table-body">
                    <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
                        <div class="table-body-row">
                            <div class="table-body-item item-image">
                                <c:choose>
                                    <c:when test="${not empty entry.article.pictureUrl}">
                                        <img src="${entry.article.pictureUrl}"/>
                                    </c:when>
                                    <c:otherwise>
                                        <product:productPrimaryImage product="${entry.product}"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="table-body-mobile-wrap">
                                <div class="table-body-item item-description">
                                    <span class="product-label">${entry.product.name}</span>
                                    <c:if test="${not empty entry.article.journalName}">
                                    <h4 class="item-description-title">${entry.article.journalName}</h4>
                                    </c:if>
                                    <p class="item-description-text">
                                        ${entry.article.summary}</p>
                                    <div class="description-extra-info">
                                        <c:if test="${not empty entry.article.doi}">
                                            <div class="extra-item">
                                                <p class="item-title">
                                                    <spring:theme code="product.article.doi"/>
                                                </p>
                                                <p class="item-value">${entry.article.doi}</p>
                                            </div>
                                        </c:if>
                                        <c:if test="${not empty entry.namedDeliveryDate}">
                                            <div class="extra-item">
                                                <p class="item-title">
                                                    <spring:theme code="basket.page.delivery.date"/>
                                                </p>
                                                <p class="item-value">
                                                    <fmt:formatDate pattern="dd MMMM YYYY"
                                                                    value="${entry.namedDeliveryDate}"/>
                                                </p>
                                            </div>
                                        </c:if>
                                        <product:productListerClassifications product="${entry.product}"/>
                                    </div>
                                </div>
                                <div class="table-body-item item-quantity">
                                    <c:if test="${entry.product.countable }">
                                        <p class="item-title-mobile">
                                            <spring:theme code="text.multi.checkout.orderreview.quantity"/>
                                        </p>
                                        <p class="item-value">${entry.quantity }</p>
                                    </c:if>
                                </div>
                                <div class="table-body-item item-total">
                                    <p class="item-title-mobile">
                                        <spring:theme code="basket.page.total"/>
                                    </p>
                                    <p class="item-value">
                                        <format:price priceData="${entry.totalPrice}"
                                                      displayFreeForZero="true"/>
                                    </p>

                                </div>
                            </div>
                            <checkout:checkoutPromotionItems cartData="${cartData}" entry="${entry}"/>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <div class="product-summ-total">
                <div class="total-row">
                    <div class="item-total-label">
                        <spring:theme code="text.multi.checkout.orderreview.subtotal"/>
                    </div>
                    <div class="item-total-value">
                        <format:price priceData="${cartData.subTotalWithoutDiscount}" displayFreeForZero="FALSE"/>
                    </div>
                </div>
                <c:if test="${cartData.net && showTax}">
                    <c:if test="${cartData.totalDiscounts.value > 0}">
                        <div class="total-row">
                            <div class="item-total-label">
                                <spring:theme code="text.multi.checkout.orderreview.discount"/>
                            </div>
                            <div class="item-total-value">-
                                <format:price priceData="${cartData.totalDiscounts}" displayFreeForZero="FALSE"/>
                            </div>
                        </div>
                    </c:if>
                    <div class="total-row">
                        <div class="item-total-label">
                            <spring:theme code="basket.page.totals.netTax"/>
                        </div>
                        <div class="item-total-value">
                            <format:price priceData="${cartData.totalTax}" displayFreeForZero="FALSE"/>
                        </div>
                    </div>
                </c:if>
                <div class="total-row">
                    <div class="item-total-label">
                        <spring:theme code="text.multi.checkout.orderreview.total"/>
                    </div>
                    <div class="item-total-value">
                        <c:choose>
                            <c:when test="${showTax}">
                                <format:price priceData="${cartData.totalPriceWithTax}" displayFreeForZero="FALSE"/>
                            </c:when>
                            <c:otherwise>
                                <format:price priceData="${cartData.totalPrice}" displayFreeForZero="FALSE"/>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content order-review-billing">
        <h3 class="order-review-section-title">
            <spring:theme code="text.multi.checkout.orderreview.billinginfo"/>
        </h3>
        <div class="order-review-items-list">
            <div class="order-review-item">
                <div class="order-review-label">
                    <spring:theme code="text.multi.checkout.orderreview.billinginfo.name"/>
                </div>
                <div class="order-review-value">
                    <c:if test="${not empty cartData.paymentAddress.firstName}">${cartData.paymentAddress.firstName}</c:if>
                    <c:if test="${not empty cartData.paymentAddress.lastName}">${cartData.paymentAddress.lastName}</c:if>
                </div>
            </div>
            <c:if test="${not empty cartData.paymentAddress.companyName}">
                <div class="order-review-item">
                    <div class="order-review-label">
                        <spring:theme code="text.multi.checkout.orderreview.billinginfo.university"/>
                    </div>
                    <div class="order-review-value">${cartData.paymentAddress.companyName}</div>
                </div>
            </c:if>
            <c:if test="${not empty cartData.paymentAddress.department}">
                <div class="order-review-item">
                    <div class="order-review-label">
                        <spring:theme code="text.multi.checkout.orderreview.billinginfo.department"/>
                    </div>
                    <div class="order-review-value">${cartData.paymentAddress.department}</div>
                </div>
            </c:if>
            <div class="order-review-item">
                <div class="order-review-label">
                    <spring:theme code="text.multi.checkout.orderreview.billinginfo.address"/>
                </div>
                <div class="order-review-value">
                    ${cartData.paymentAddress.line1}<c:if test="${not empty cartData.paymentAddress.line2}">,
                        ${cartData.paymentAddress.line2}</c:if>,
                    ${cartData.paymentAddress.town},
                    <c:if test="${cartData.paymentAddress.region != null && not empty cartData.paymentAddress.region.name}">
                        ${cartData.paymentAddress.region.name}</c:if>
                    ${cartData.paymentAddress.postalCode},
                    ${cartData.paymentAddress.country.name}
                </div>
            </div>
            <c:if test="${not empty cartData.paymentAddress.phone}">
                <div class="order-review-item">
                    <div class="order-review-label">
                        <spring:theme code="text.multi.checkout.orderreview.billinginfo.phone"/>
                    </div>
                    <div class="order-review-value">${cartData.paymentAddress.phone}</div>
                </div>
            </c:if>
            <c:if test="${not empty cartData.paymentAddress.email}">
                <div class="order-review-item">
                    <div class="order-review-label">
                        <spring:theme code="text.multi.checkout.orderreview.billinginfo.email"/>
                    </div>
                    <div class="order-review-value">${cartData.paymentAddress.email}</div>
                </div>
            </c:if>
        </div>
        <c:if test="${not empty cartData.taxNumber}">
            <c:if test="${cartData.country.applicableTaxExemption == 'TAX'}">
                <h3 class="order-review-section-title">
                    <spring:theme code="text.multi.checkout.orderreview.billinginfo.taxdetails"/>
                </h3>
                <div class="order-review-items-list">
                    <div class="order-review-item">
                        <div class="order-review-label">
                            <spring:theme code="text.multi.checkout.orderreview.billinginfo.taxnumber"/>
                        </div>
                        <div class="order-review-value">${cartData.taxNumber}</div>
                    </div>
                    <c:if test="${not empty cartData.taxNumberExpirationDate}">
                        <div class="order-review-item">
                            <div class="order-review-label">
                                <spring:theme code="text.multi.checkout.orderreview.billinginfo.taxexpirationdate"/>
                            </div>
                            <div class="order-review-value">
                                <fmt:formatDate pattern="dd MMMM YYYY" value="${cartData.taxNumberExpirationDate}"/>
                            </div>
                        </div>
                    </c:if>
                </div>
            </c:if>
            <c:if test="${cartData.country.applicableTaxExemption == 'VAT'}">
                <h3 class="order-review-section-title">
                    <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.title"/>
                </h3>
                <div class="order-review-items-list">
                    <div class="order-review-item">
                        <div class="order-review-label">
                            <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.label"/>
                        </div>
                        <div class="order-review-value">${cartData.taxNumber}</div>
                    </div>
                </div>
            </c:if>
        </c:if>
        <form
                action="${request.contextPath}/checkout/multi/payment-address/add"
                method="GET">
            <button class="button order-review-edit-button">
                <spring:theme code="text.multi.checkout.orderreview.edit.button"/>
            </button>
        </form>
    </div>
    <div class="tab-content order-review-payment">
        <c:set value="${cartData.paymentMode}" var="selectedPaymentMode"/>
        <h3 class="order-review-section-title">
            <spring:theme code="text.multi.checkout.orderreview.payment"/>
        </h3>
        <div class="order-review-items-list">
            <div class="order-review-item">
                <div class="order-review-label">
                    <spring:theme code="text.multi.checkout.orderreview.paymentmethod"/>
                </div>
                <%-- ${cartData.paymentMode} --%>
                <div class="order-review-value">
                    <spring:theme code="text.multi.checkout.cartdata.paymentmode.${cartData.paymentMode}"/>
                </div>
            </div>
            <c:choose>
                <c:when test="${selectedPaymentMode eq 'card'}">
                    <div class="order-review-item">
                        <div class="order-review-label">
                            <spring:theme code="text.multi.checkout.orderreview.paymenttype"/>
                        </div>
                        <div class="order-review-value">${cartData.paymentInfo.cardTypeData.name}&nbsp;<spring:theme
                                code="text.multi.checkout.paymentmethod.card.pattern"/>&nbsp;${fn:escapeXml(cartData.paymentInfo.cardNumber)}
                        </div>
                    </div>
                </c:when>
                <c:when test="${selectedPaymentMode eq 'invoice'}">
                    <c:if test="${not empty cartData.purchaseOrderNumber}">
                        <div class="order-review-item">
                            <div class="order-review-label">
                                <spring:theme code="text.multi.checkout.orderreview.purchase.order.number"/>
                            </div>
                            <div class="order-review-value">${cartData.purchaseOrderNumber}</div>
                        </div>
                    </c:if>
                    <c:if test="${not empty cartData.userNotes}">
                        <div class="order-review-item">
                            <div class="order-review-label">
                                <spring:theme code="text.multi.checkout.orderreview.notes.on.invoice"/>
                            </div>
                            <div class="order-review-value">${cartData.userNotes}</div>
                        </div>
                    </c:if>
                </c:when>
                <c:when test="${selectedPaymentMode eq 'proforma'}">
                    <c:if test="${not empty cartData.purchaseOrderNumber}">
                        <div class="order-review-item">
                            <div class="order-review-label">
                                <spring:theme code="text.multi.checkout.orderreview.purchase.order.number"/>
                            </div>
                            <div class="order-review-value">${cartData.purchaseOrderNumber}</div>
                        </div>
                    </c:if>
                    <c:if test="${not empty cartData.userNotes}">
                        <div class="order-review-item">
                            <div class="order-review-label">
                                <spring:theme code="text.multi.checkout.orderreview.notes.on.proforma"/>
                            </div>
                            <div class="order-review-value">${cartData.userNotes}</div>
                        </div>
                    </c:if>
                </c:when>
            </c:choose>

        </div>
        <form
                action="${request.contextPath}/checkout/multi/payment-method/add"
                method="GET">
            <button class="button order-review-edit-button">
                <spring:theme code="text.multi.checkout.orderreview.edit.button"/>
            </button>
        </form>
    </div>
    <div class="form-footer form">
        <label class="custom-checkbox">
            <input type="checkbox" id="Terms1" tabindex="10"/>
            <spring:theme code="text.multi.checkout.orderreview.terms.check"/>
        </label>
    </div>
</div>
