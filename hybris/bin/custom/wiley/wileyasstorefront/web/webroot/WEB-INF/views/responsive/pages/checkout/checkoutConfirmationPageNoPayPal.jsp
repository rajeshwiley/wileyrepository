<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>


<template:page pageTitle="${pageTitle}" hideHeaderLinks="true"
              renderAntiClickJacking="true">
   <multi-checkout:orderConfirmationBreadcrumb />
   <div class="container shopping-cart-page shopping-cart-confirmation">
       <div class="row">
         <div class="col-xs-12">
            <cms:pageSlot position="TopContent" var="feature" element="div" class="span-24 top-content-slot cms_disp-img_slot">
		    <cms:component component="${feature}"/>
	     </cms:pageSlot>
	     
	     <div class="shopping-cart-top">
             <h1 class="main-page-title icon-shopping-cart"><spring:theme code="text.order.confirmation.title" /></h1>
            </div>
            <cms:pageSlot position="SideContent" var="feature" element="div" class="confirmation-message-block">
	       <cms:component component="${feature}"/>
         </cms:pageSlot>
           <div class="confirmation-order-review">
             <order:confirmationOrderSummary order="${orderData}"/>
             <div class="form-wrapper order-review-block">
              <order:orderDetailsItem order="${orderData}"  />
              <order:paymentAddressItem order="${orderData}"/>
              <c:if test="${isNonZeroOrder}">
                <order:paymentMethodItem order="${orderData}"/>
              </c:if>
             </div>
           </div>
           
           <order:giftCoupon giftCoupon="${giftCoupon}"/>
           
           <a href="${backToDashboardUrl}" title="<spring:theme code='text.order.confirmation.backToDashboard' />" class="button button-main back-button"><spring:theme code="text.order.confirmation.backToDashboard" /></a>
         </div>
       </div>
     </div>
</template:page>