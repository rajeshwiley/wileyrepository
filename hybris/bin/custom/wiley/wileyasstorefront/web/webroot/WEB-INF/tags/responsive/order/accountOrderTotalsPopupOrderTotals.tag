<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="message" required="false" type="java.lang.String" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>

<spring:htmlEscape defaultHtmlEscape="true"/>

<c:if test="${not empty order}">
	<div class="modal-order-details">
		<c:if test="${not empty message}">
			<div class="order-details-info-message">
					${message}
			</div>
		</c:if>
		<div class="order-details-title">
				${title}
		</div>
		<div class="order-details-text">
			<spring:theme code="text.account.orderTotalsPopup.order.details.subtotal"/>
			<span class="order-details-text-right">${order.subTotalWithoutDiscount.formattedValue}</span>
		</div>
		<c:if test="${order.totalDiscounts.value > 0}">
			<div class="order-details-text">
				<spring:theme code="text.account.orderTotalsPopup.order.details.discounts"/>
				<span class="order-details-text-right">-&nbsp;${order.totalDiscounts.formattedValue}</span>
			</div>
		</c:if>
		<c:if test="${order.taxAvailable}">
        	 <div class="order-details-text">
        		 <spring:theme code="text.account.orderTotalsPopup.order.details.estimatedTaxes"/>
                 <span class="order-details-text-right">${order.totalTax.formattedValue}</span>
        	</div>
        </c:if>
		<div class="order-details-text bold">
			<spring:theme code="text.account.orderTotalsPopup.order.details.total"/>
			<span class="order-details-text-right">${order.totalPriceWithTax.formattedValue}</span>
		</div>
	</div>
</c:if>
