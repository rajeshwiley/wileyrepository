<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="wt" uri="http://hybris.com/tld/wro4j_tag" %>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" />
<c:choose>
	<c:when test="${wro4jEnabled}">
	 <link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${commonResourcePath}/min' resource='all_responsive.css' />" />
	 <c:if test="${not empty addOnThemeCssPaths}">
	  <link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${themeResourcePath}/min' resource='${themeName}_responsive.css' />" />
	 </c:if>
	 <link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${commonResourcePath}/min' resource='addons_responsive.css' />" />
	</c:when>
	<c:otherwise>
		<%-- make sure any custom CSS are also added to wro.xml --%>
		<%-- Theme CSS files --%>
		<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css"/>

		<%-- make sure any AddOn Common CSS are also added to wro_addons.xml --%>
		<%--  AddOn Common CSS files --%>
		<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
			<link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}"/>
		</c:forEach>

		<%-- make sure any AddOn Theme CSS are also added to wro_addons.xml --%>
		<%--  AddOn Theme CSS files --%>
		<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
			<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
		</c:forEach>
	</c:otherwise>
</c:choose>

<%-- <link rel="stylesheet" href="${commonResourcePath}/blueprint/print.css" type="text/css" media="print" />
<style type="text/css" media="print">
	@IMPORT url("${commonResourcePath}/blueprint/print.css");
</style>
 --%>

<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />
