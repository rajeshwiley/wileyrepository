<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<spring:htmlEscape defaultHtmlEscape="true"/>
<c:set var="order" value="${orderData}"/>
<spring:url value="/my-account/orders" var="orderHistoryUrl"/>
<spring:url value="/my-account/order/${orderData.guid}/edit" var="orderEditingUrl"/>

<div class="container shopping-cart-page shopping-cart-history-details">
    <div class="row">
        <div class="col-xs-12 order-history-details-wrapper">
            <div class="shopping-cart-top">
                <spring:message code="text.account.orderHistory.back.to.orderHistory" var="back_to_order_history"/>
                <a href="${orderHistoryUrl}" title="${back_to_order_history}" class="button button-secondary button-back-to-order">${back_to_order_history}</a>
                <h1 class="main-page-title icon-shopping-cart"><spring:message code="text.account.order.title.details"/></h1>
            </div>
            <div class="order-history-details">
                <div class="form-top">
                    <div class="payment-details">
                        <div class="payment-number-date">
                            <p><spring:message code="text.account.orderHistory.orderNumber"/>:<span>&nbsp;${order.code}</span></p>
                            <p><spring:message code="text.account.orderHistory.dateplaced"/>:<span>&nbsp;<fmt:formatDate pattern="dd MMMM YYYY hh:mm a" value="${order.created}"/></span></p>
                        </div>
                    <div class="payment-group-buttons">
                      <c:if test="${canEditOrder}">
                        <a href="${orderEditingUrl}" title="Edit Payment Details" class="button button-main"><spring:message code="text.account.orderHistory.edit.paymentMode"/></a>
                      </c:if>
                    </div>
                </div>
                <div class="order-history-details-table">
                    <div class="products-table">
                        <div class="table-head">
                            <div class="table-head-item item-product"><spring:message code="text.account.orderHistory.entry"/></div>
                            <div class="table-head-item item-product-status"><spring:message code="text.account.orderHistory.entry.status"/></div>
                            <div class="table-head-item item-quantity"><spring:message code="text.account.orderHistory.entry.quantity"/></div>
                            <div class="table-head-item item-total"><spring:message code="text.account.orderHistory.entry.total"/></div>
                        </div>
                        <order:accountOrderDetailsItem order="${order}"/>
                    </div>
                    <c:choose>
                        <c:when test="${order.orderConfiguration.hidePricing}">
                           <div class="product-summ-total-funder">*&nbsp;${order.externalText}</div>
                        </c:when>
                        <c:otherwise>
                           <order:accountOrderDetailOrderTotals order="${order}"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <c:if test="${not order.orderConfiguration.hidePaymentDetails}">
                  <order:paymentAddressItem order="${order}"/>
                  <c:if test="${isNonZeroOrder}">
                      <order:paymentMethodItem order="${order}"/>
                  </c:if>
            </c:if>
            <div id="removeOrderLineModal" tabindex="-1" role="dialog"
                 aria-hidden="true" class="modal fade"></div>
        </div>
            <c:if test="${canCancelOrder}">
                <a href="" title="Cancel Order" data-toggle="modal" data-target="#cancelOrderConfirm" class="button">Cancel Order</a>
                <!-- Modal-->
                <div id="cancelOrderConfirm" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                <div class="modal-dialog modal-confirmation">
                <div class="modal-content">
                    <spring:url value="{orderGuid}/cancel/submit" var="cancelOrderActionUrl">
                        <spring:param name="orderGuid" value="${order.guid}"/>
                    </spring:url>
                    <form:form action="${cancelOrderActionUrl}" method="post"
                               commandName="orderCancelReasonForm">
                        <div class="modal-body">
                            <div class="modal-info"><spring:message code="text.account.orderHistory.cancelOrder.confirmText"/></div>
                        </div>
                        <c:if test="${orderCancelReasons ne null}">
                            <div class="order-details-cancellation-reason">
                                <div class="order-details-cancellation-reason-title">
                                    <spring:message code="text.account.orderHistory.cancelOrder.cancellation.reason"/>
                                </div>
                                <div class="input-group">
                                    <div class="form-field required">
                                        <div class="form-group">
                                            <div id="cancel-reason" class="select-component">
                                                <form:select path="cancelReason">
                                                    <form:options items="${orderCancelReasons}" itemLabel="localizedName" itemValue="cancelReason"/>
                                                </form:select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="button button-red" onClick="this.form.reset();"><spring:message code="text.account.orderHistory.cancelOrder.cancel"/></button>
                            <button type="submit" class="button button-main"><spring:message code="text.account.orderHistory.cancelOrder.confirm"/></button>
                        </div>
                    </form:form>
                </div>
                </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
