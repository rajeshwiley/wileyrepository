<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
<c:url value="/cart/paypal-checkout" var="payPalCheckoutUrl" scope="session"/>
<c:url value="/checkout/multi/summary/placeZeroOrder" var="zeroOrderUrl"/>
<c:set var="payPalButtonAlt">
	<spring:theme code="text.paypal.button.alt"/>
</c:set>
<c:set var="checkoutButtonTitle">
	<spring:theme code="checkout.checkout"/>
</c:set>
<c:set var="isPayPalDisabled" value="${!cartData.taxAvailable && (cartData.totalPrice.value gt 0)}"/>
<spring:message code="tax.not.calculated.warning.title" var="taxNotCalculatedWarningTitle"/>

<div class="shopping-cart-action-buttons">
   
	<c:if test="${not empty cartData.entries}">
		<a class="button button-secondary button-add-services btn--continue-shopping js-continue-shopping-button" href="${continueShoppingUrl}">
			<spring:theme code="cart.page.continue"/>
		</a>
	</c:if>
    <div class="action-buttons-mobile-wrapper">
		<c:choose>
			<c:when test="${empty cartData.entries}">
				<a href="#" title="${checkoutButtonTitle}" class="button button-main disabled">
					${checkoutButtonTitle}
				</a>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${isPayPalAvailable && isNonZeroOrder}">
						<form:form id="proceedToCheckout" action="${payPalCheckoutUrl}" method="post">
							<input type="hidden" name="cartId" value="${cartData.guid }"/>
							<span title="${isPayPalDisabled ? taxNotCalculatedWarningTitle : checkoutButtonTitle}" class="button-paypal-wrapper">
								<button class="button button-paypal ${isPayPalDisabled ? 'disabled' : ''}">
									${checkoutButtonTitle}
								</button>
							</span>
							<a href="${checkoutUrl}" title="${checkoutButtonTitle}" class="button button-main button-checkout">
								${checkoutButtonTitle}
							</a>
						</form:form>
					</c:when>
					<c:when test="${not isNonZeroOrder}">
						<form:form id="placeZeroOrder" action="${zeroOrderUrl}" method="post">
							<input type="hidden" name="cartId" value="${cartData.guid }"/>
							<button  type="submit" class="button button-main right">
								<spring:theme code="checkout.summary.placeOrder"/>
							</button>
						</form:form>
					</c:when>
					<c:otherwise>
						<a href="${checkoutUrl}" title="${checkoutButtonTitle}" class="button button-main">
							${checkoutButtonTitle}
						</a>
					</c:otherwise>
				</c:choose>            
			</c:otherwise>
		</c:choose>
    </div>
</div>

<c:if test="${showCheckoutStrategies && not empty cartData.entries}" >
    <div class="cart-actions">
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-3 col-lg-2 pull-right">
                <input type="hidden" name="flow" id="flow"/>
                <input type="hidden" name="pci" id="pci"/>
                <select id="selectAltCheckoutFlow" class="doFlowSelectedChange form-control">
                    <option value="multistep"><spring:theme code="checkout.checkout.flow.select"/></option>
                    <option value="multistep"><spring:theme code="checkout.checkout.multi"/></option>
                    <option value="multistep-pci"><spring:theme code="checkout.checkout.multi.pci"/></option>
                </select>
                <select id="selectPciOption" class="display-none">
                    <option value=""><spring:theme code="checkout.checkout.multi.pci.select"/></option>
                    <c:if test="${!isOmsEnabled}">
                        <option value="default"><spring:theme code="checkout.checkout.multi.pci-ws"/></option>
                        <option value="hop"><spring:theme code="checkout.checkout.multi.pci-hop"/></option>
                    </c:if>
                    <option value="sop"><spring:theme code="checkout.checkout.multi.pci-sop" text="PCI-SOP" /></option>
                </select>
            </div>
        </div>
    </div>
</c:if>