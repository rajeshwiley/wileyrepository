<%@ attribute name="paymentMode" required="true" type="java.lang.String" %>
<%@ attribute name="permittedPaymentModes" required="false" type="java.util.Set" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="renderPaymentMode" value="false" />
<c:forEach var="permittedPaymentMode" items="${permittedPaymentModes}">
	<c:if test="${paymentMode eq permittedPaymentMode}">
		<c:set var="renderPaymentMode" value="true" />
	</c:if>
</c:forEach>

<c:if test="${renderPaymentMode}">
	<jsp:doBody/>
</c:if>