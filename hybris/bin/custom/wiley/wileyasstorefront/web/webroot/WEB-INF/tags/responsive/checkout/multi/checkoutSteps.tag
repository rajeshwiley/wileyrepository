<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ attribute name="checkoutSteps" required="true" type="java.util.List" %>
<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<div class="container shopping-cart-page shopping-cart-checkout">
    <div class="row">
        <div class="col-xs-12">
            <div class="shopping-cart-top">
                <cart:cartNumber cartData="${cartData}"/>
                <h1 class="main-page-title icon-shopping-cart">
                    <spring:theme code="text.checkout"/>
                </h1>
            </div>
            <multiCheckout:checkoutOrderDetails cartData="${cartData}"
                                                showDeliveryAddress="false" showPaymentInfo="false"
                                                showTaxEstimate="false" showTax="${cartData.taxAvailable}"/>
            <div id="checkout-accordion" class="panel-group">
                <c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
                    <div class="panel ${cssClass}">
                        <c:url value="${checkoutStep.url}" var="stepUrl"/>
                        <c:choose>
                            <c:when test="${progressBarId eq checkoutStep.progressBarId}">
                                <c:set scope="page" var="activeCheckoutStepNumber" value="${checkoutStep.stepNumber}"/>
                                <a href="${stepUrl}" class="panel-title">
                                    <spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
                                </a>
                                <div class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <jsp:doBody/>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${checkoutStep.stepNumber > activeCheckoutStepNumber}">
                                <a href="${stepUrl}" class="panel-title collapsed">
                                    <spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="${stepUrl}" class="panel-title completed collapsed">
                                    <spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </c:forEach>
            </div>
            <c:choose>
                <c:when test="${placeOrderForm ne null}">
                    <form:form id="placeOrderForm" commandName="placeOrderForm" action="${placeOrderUrl}" method="post">
                        <input type="hidden" name="cartId" value="${cartData.guid}"/>
                        <form:input type="hidden" path="termsCheck" id="termsCheck" name="termsCheck"/>
                        <button id="placeOrderBtn" type="submit" class="button button-main right place-order-btn" disabled>
                            <spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
                        </button>
                    </form:form>
                </c:when>
                <c:otherwise>
                    <button id="placeOrderBtn" type="submit" class="button button-main right place-order-btn" disabled>
                        <spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
                    </button>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>