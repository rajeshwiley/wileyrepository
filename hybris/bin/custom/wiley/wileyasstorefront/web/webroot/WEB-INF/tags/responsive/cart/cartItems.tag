<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

 <c:set var="errorStatus" value="<%= de.hybris.platform.catalog.enums.ProductInfoStatus.valueOf(\"ERROR\") %>" />

<div class="products-table">
    <div class="table-head">
        <div class="table-head-item item-product"><spring:theme code="basket.page.item"/></div>
        <div class="table-head-item item-quantity"><spring:theme code="basket.page.quantity"/></div>
        <div class="table-head-item item-total"><spring:theme code="basket.page.total"/></div>
    </div>
    <div class="table-body">
        <c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
            <div class="table-body-row">

                <c:if test="${not empty entry.statusSummaryMap}" >
                    <c:set var="errorCount" value="${entry.statusSummaryMap.get(errorStatus)}"/>
                    <c:if test="${not empty errorCount && errorCount > 0}" >
                        <div class="notification has-error">
                            <spring:theme code="basket.error.invalid.configuration" arguments="${errorCount}"/>
                            <a href="<c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" />" >
                                <spring:theme code="basket.error.invalid.configuration.edit"/>
                            </a>
                        </div>
                    </c:if>
                </c:if>

                <c:set var="showEditableGridClass" value=""/>
                <c:url value="${entry.product.url}" var="productUrl"/>

                <%-- chevron for multi-d products --%>
                <div class="hidden-xs hidden-sm item__toggle">
                    <c:if test="${entry.product.multidimensional}" >
                        <div class="js-show-editable-grid" data-index="${loop.index}" data-read-only-multid-grid="${not entry.updateable}">
                            <ycommerce:testId code="cart_product_updateQuantity">
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </ycommerce:testId>
                        </div>
                    </c:if>
                </div>

                <%-- product image --%>
                <div class="table-body-item item-image">
                <c:choose>
                    <c:when test="${not empty entry.article.pictureUrl}">
                        <img src="${entry.article.pictureUrl}"/>
                    </c:when>
                    <c:otherwise>
                        <product:productPrimaryImage product="${entry.product}"/>
                    </c:otherwise>
                </c:choose>
                </div>

                <div class="table-body-mobile-wrap">
                    <%-- product name, code, promotions --%>
                    <div class="table-body-item item-description"><span class="product-label">${entry.product.name}</span>
                    <c:if test="${not empty entry.article.journalName}">
                    <h4 class="item-description-title">${entry.article.journalName}</h4>
                    </c:if>
                    <p class="item-description-text">${entry.article.summary}</p>
                        <div class="description-extra-info">
                            <c:if test="${not empty entry.article.doi}">
                                <div class="extra-item">
                                    <p class="item-title"><spring:theme code="product.article.doi"/></p>
                                    <p class="item-value">${entry.article.doi}</p>
                                </div>
                            </c:if>
                            <c:if test="${not empty entry.namedDeliveryDate}">
                                <div class="extra-item">
                                    <p class="item-title"><spring:theme code="basket.page.delivery.date"/></p>
                                    <p class="item-value"><fmt:formatDate pattern="dd MMMM YYYY" value="${entry.namedDeliveryDate}"/></p>
                                </div>
                            </c:if>
                            <product:productListerClassifications product="${entry.product}"/>
                        </div>

                        <c:if test="${entry.product.configurable}">
                            <div class="hidden-xs hidden-sm">
                                <c:url value="/cart/${entry.entryNumber}/configuration/${entry.configurationInfos[0].configuratorType}" var="entryConfigUrl"/>
                                <div class="item__configurations">
                                    <c:forEach var="config" items="${entry.configurationInfos}">
                                        <c:set var="style" value=""/>
                                        <c:if test="${config.status eq errorStatus}">
                                            <c:set var="style" value="color:red"/>
                                        </c:if>
                                        <div class="item__configuration--entry" style="${style}">
                                            <div class="row">
                                                <div class="item__configuration--name col-sm-4">
                                                        ${config.configurationLabel}
                                                        <c:if test="${not empty config.configurationLabel}">:</c:if>

                                                </div>
                                                <div class="item__configuration--value col-sm-8">
                                                        ${config.configurationValue}
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                                <c:if test="${not empty entry.configurationInfos}">
                                    <div class="item__configurations--edit">
                                        <a class="btn" href="${entryConfigUrl}"><spring:theme code="basket.page.change.configuration"/></a>
                                    </div>
                                </c:if>
                            </div>
                        </c:if>
                    </div>
                    <div class="table-body-item item-quantity">
                        <c:if test="${entry.product.countable }">
                            <p class="item-title-mobile">Quantity</p>
                            <p class="item-value"> ${entry.quantity }</p>
                        </c:if>
                    </div>
                    <div class="table-body-item item-total">
                        <p class="item-title-mobile"><spring:theme code="basket.page.total"/></p>
                        <p class="item-value"><format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/></p>
                    </div>
                    <c:if test="${isRemoveButtonAvailable}">
                        <div class="table-body-item item-remove"><a href="" title="Remove" data-toggle="modal" data-target="#removeProductConfirm${entry.entryNumber}" class="button-remove">Remove</a></div>
                        <!-- Modal-->
                        <div id="removeProductConfirm${entry.entryNumber}" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade">
                            <div class="modal-dialog modal-confirmation">
                                <div class="modal-content">
                                    <c:url value="/cart/update" var="cartUpdateFormAction"/>
                                    <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post"
                                               commandName="updateQuantityForm${entry.entryNumber}">
                                        <input type="hidden" name="cartId" value="${cartData.guid }" />
                                        <input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
                                        <input type="hidden" name="productCode" value="${entry.product.code}"/>
                                        <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                        <input type="hidden" name="quantity" value="${entry.quantity}">

                                        <div class="modal-body">
                                            <div class="modal-info">
                                                <c:choose>
                                                    <c:when test="${entry.product.mandatory}">
                                                        <spring:theme code="basket.page.remove.product.confirmation.popup.text.mandatory"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <spring:theme code="basket.page.remove.product.confirmation.popup.text.optional"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="button button-red">
                                                <spring:theme code="basket.page.remove.product.confirmation.popup.actoin.cancel"/>
                                            </button>
                                            <button id="removeProduct_${entry.entryNumber}" type="button" class="button button-main js-remove-entry-button">
                                                <spring:theme code="basket.page.remove.product.confirmation.popup.actoin.confirm"/>
                                            </button>
                                        </div>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
                <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                    <div class="item-price-discount">
                        <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>
                            <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
                                    <c:set var="displayed" value="true"/>
                                    <p>
                                    <ycommerce:testId code="cart_potentialPromotion_label">
                                        ${promotion.description}
                                    </ycommerce:testId>
                                    </p>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </div>
                </c:if>
                <c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                    <div class="item-price-discount">
                        <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>
                            <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
                                    <c:set var="displayed" value="true"/>
                                    <p>
                                    <ycommerce:testId code="cart_appliedPromotion_label">
                                        ${promotion.description}
                                    </ycommerce:testId>
                                    </p>
                                </c:if>
                            </c:forEach>
                        </c:forEach>
                    </div>
                </c:if>
            </div>
        </c:forEach>
    </div>
</div>
<product:productOrderFormJQueryTemplates />
<storepickup:pickupStorePopup />
