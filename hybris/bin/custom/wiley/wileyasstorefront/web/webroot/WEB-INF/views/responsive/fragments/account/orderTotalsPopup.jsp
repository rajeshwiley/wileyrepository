<%@ page trimDirectiveWhitespaces="true" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="isCancel" value="${action == 'cancel'}"/>
<c:set var="isEdit" value="${action == 'edit'}"/>

<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <spring:url value="/my-account/order/${orderData.guid}/update-order-entry/${entryNumber}"
                    var="proceedWithEntryCancelUrl"/>
        <form:form method="get" action="${proceedWithEntryCancelUrl}" commandName="orderCancelReasonForm">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">
                    <c:if test="${isCancel}">
                        <spring:theme code="text.account.orderTotalsPopup.lineEntry.cancel.title"/>
                    </c:if>
                    <c:if test="${isEdit}">
                        <spring:theme code="text.account.orderTotalsPopup.title"/>
                    </c:if>
                </h4>
            </div>
            <div class="modal-body">
                <c:if test="${isCancel}">
                    <spring:theme code="text.account.orderTotalsPopup.lineEntry.cancel.confirmation"
                                  arguments="${productName}" var="orderDetailsMessage"/>
                </c:if>
                <c:if test="${isEdit}">
                    <spring:theme code="text.account.orderTotalsPopup.order.details.message" var="orderDetailsMessage"/>
                </c:if>
                <spring:theme code="text.account.orderTotalsPopup.order.details.title" var="orderDetailsTitle"/>
                <order:accountOrderTotalsPopupOrderTotals order="${orderData}" message="${orderDetailsMessage}"
                                                          title="${orderDetailsTitle}"/>
                <spring:theme code="text.account.orderTotalsPopup.order.details.title.updated"
                              var="orderDetailsTitleUpdated"/>
                <order:accountOrderTotalsPopupOrderTotals order="${updatedOrderData}"
                                                          title="${orderDetailsTitleUpdated}"/>
                <c:if test="${orderCancelReasons ne null}">
                    <div class="order-details-cancellation-reason">
                        <div class="order-details-cancellation-reason-title">
                            <spring:message code="text.account.orderHistory.cancelOrder.cancellation.reason"/>
                        </div>
                        <div class="input-group">
                            <div class="form-field required">
                                <div class="form-group">
                                    <div id="cancel-reason" class="select-component">
                                        <form:select path="cancelReason">
                                            <form:options items="${orderCancelReasons}"
                                                          itemLabel="localizedName"
                                                          itemValue="cancelReason"/>
                                        </form:select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="button button-red">
                    <spring:message code="text.account.orderHistory.cancelOrder.cancel"/>
                </button>
                <c:if test="${isCancel}">
                    <button type="submit" class="button button-main">
                        <spring:message code="text.account.orderHistory.cancelOrder.confirm"/>
                    </button>
                </c:if>
                <c:if test="${isEdit}">
                    <button id="confirmSubmitWileyOrderForm" type="submit" class="button button-main">
                        <spring:message code="text.account.orderHistory.cancelOrder.confirm"/>
                    </button>
                </c:if>
            </div>
        </form:form>
    </div>
</div>
