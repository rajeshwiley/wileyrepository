<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="messageKey" required="true" type="java.lang.String" %>
<%@ attribute name="message" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div id="global-alert" class="flash-message-wrapper hidden">
    <div class="flash-message error">
        <div class="container">
            <span id="${messageKey}" class="flash-message-text">${message}</span>
                <button class="close"><span>&times;</span></button>
        </div>
    </div>
</div>