<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="format" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${empty format}">
    <c:set value="thumbnail" var="format"/>
</c:if>
<c:set value="${ycommerce:productImage(product, format)}" var="primaryImage"/>
<img src="${primaryImage.url}" alt=""/>
