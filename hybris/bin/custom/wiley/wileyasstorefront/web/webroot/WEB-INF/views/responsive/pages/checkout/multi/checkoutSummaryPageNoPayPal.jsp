<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true"
               renderAntiClickJacking="true">
	<multi-checkout:checkoutStepsBreadcrumb />
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}"
			progressBarId="${progressBarId}">
			<jsp:body>
         <ycommerce:testId code="checkoutStepThree">
         <multi-checkout:checkoutOrderReview cartData="${cartData}" showTax="${cartData.taxAvailable}"/>
                        <input type="hidden" name="cartId" id="carId"
						value="${cartData.guid }" />
         </ycommerce:testId>
        </jsp:body>
		</multi-checkout:checkoutSteps>
</template:page>