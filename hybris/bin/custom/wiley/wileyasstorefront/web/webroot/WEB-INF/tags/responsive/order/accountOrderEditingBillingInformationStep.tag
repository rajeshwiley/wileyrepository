<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<%@ attribute name="cssClass" required="false" type="java.lang.String" %>

<div id="billingInformationStep" class="panel ${cssClass}">
				<a class="panel-title completed" data-toggle="collapse" href="#billingInformationPanel">
								<spring:theme code="checkout.multi.paymentAddress"/>
				</a>
				<div id="billingInformationPanel" class="panel-collapse in">
								<div class="panel-body">
												<address:addressFormSelector supportedCountries="${countries}"
																																									regions="${regions}" country="${country}" showNextStepButton="false" showSaveAddressButton="false"/>
								</div>
				</div>
</div>