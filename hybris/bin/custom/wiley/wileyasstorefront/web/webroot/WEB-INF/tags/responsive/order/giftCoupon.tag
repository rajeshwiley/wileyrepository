<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="giftCoupon" required="true" type="de.hybris.platform.commercefacades.coupon.data.CouponData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:htmlEscape defaultHtmlEscape="true" />
<c:if test="${not empty giftCoupon}">
 <div class="discount-coupon-block">
   <p><spring:theme code="text.order.confirmation.coupon.thankYouMessage" /></p>
   <p class="blue"><spring:theme code="text.order.confirmation.coupon.earned" /></p>
   <div class="discount-coupon">
     <div class="discount-coupon-amount">${giftCoupon.name}</div>
     <p><spring:theme code="text.order.confirmation.coupon.afterNameText" /></p>
     <span class="button button-main small">${giftCoupon.couponCode}</span>
   </div>
 </div>
</c:if>