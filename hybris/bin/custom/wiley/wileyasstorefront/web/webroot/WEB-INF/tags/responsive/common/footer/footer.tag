<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer class="main-footer-container">
	<div class="container">
		<nav class="main-footer-nav">
			<cms:pageSlot position="FooterNavigationSlot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</nav>
		<div class="footer-copyright">
			<div class="footer-logo">
				<cms:pageSlot position="CompanyLogoSlot" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
			<div class="footer-copyright-text">
				<cms:pageSlot position="FooterCopyrightSlot" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>
	</div>
</footer>