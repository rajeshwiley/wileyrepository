<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${not empty cartData.potentialOrderPromotions}">
    <c:forEach items="${cartData.potentialOrderPromotions}" var="promotion">
        <div class="discount-code-message">${promotion.description}</div>
    </c:forEach>
</c:if>
