<%@ tag body-content="empty" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<c:if test="${not empty validationData}">
<div class="flash-message-wrapper">
	<div class="flash-message warning">
		<c:forEach items="${validationData}" var="modification">
			<div class="container">
				<span class="flash-message-text">
					<c:choose>
						<c:when test="${not empty modification.entry}">
							<spring:theme code="basket.validation.${modification.statusCode}"
										  arguments="${modification.messageParameters}" /> &nbsp;
						</c:when>
						<c:otherwise>
							<spring:theme code="basket.validation.${modification.statusCode}" /> &nbsp;
						</c:otherwise>
					</c:choose>

				</span>
				<button class="close">
					<span>×</span>
				</button>
			</div>
		</c:forEach>
	</div>
</div>
</c:if>