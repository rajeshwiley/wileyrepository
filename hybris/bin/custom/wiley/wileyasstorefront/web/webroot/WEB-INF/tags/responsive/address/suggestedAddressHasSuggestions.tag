<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<%@ attribute name="wileyBillingAddressForm" required="true" type="com.wiley.storefrontcommons.forms.WileyAddressForm"%>
<%@ attribute name="suggestionSubmitBtnId" required="true" type="java.lang.String"%>

<spring:htmlEscape defaultHtmlEscape="true"/>

<div class="saved-card-info suggested-address">
    <div class="order-details-info-message">
        <spring:message code="address.suggestion.has.suggestions"/>
    </div>
</div>
<div class="saved-card-info">
    <div class="saved-info-title">
        <spring:message code="address.suggestion.title.default"/>
    </div>
    <address:suggestedAddressLayout line1="${wileyBillingAddressForm.line1}"
                                    line2="${wileyBillingAddressForm.line2}"
                                    town="${wileyBillingAddressForm.townCity}"
                                    postCode="${wileyBillingAddressForm.postcode}"
                                    countryData="${addressFormCountry}"
                                    regionData="${addressFormRegion}"/>
    <button id="${suggestionSubmitBtnId}" type="button" class="button"
            data-address-line1="${fn:escapeXml(wileyBillingAddressForm.line1)}"
            data-address-line2="${fn:escapeXml(wileyBillingAddressForm.line2)}"
            data-address-town="${fn:escapeXml(wileyBillingAddressForm.townCity)}"
            data-address-postcode="${fn:escapeXml(wileyBillingAddressForm.postcode)}"
            data-address-countrydata="${addressFormCountry.isocode}"
            data-address-regiondata="${addressFormRegion.isocode}">
        <spring:message code="address.suggestion.button.use"/>
    </button>
</div>
<div class="saved-card-info recommended-address">
    <div class="saved-info-title">
        <spring:message code="address.suggestion.title.recommended"/>
    </div>
</div>
<c:forEach var="suggestedAddress" items="${suggestions.suggestedAddresses}">
    <div class="saved-card-info">
        <address:suggestedAddressLayout line1="${suggestedAddress.line1}"
                                        line2="${suggestedAddress.line2}"
                                        town="${suggestedAddress.town}"
                                        postCode="${suggestedAddress.postalCode}"
                                        countryData="${suggestedAddress.country}"
                                        regionData="${suggestedAddress.region}"/>
        <button id="${suggestionSubmitBtnId}" type="button" class="button button-main"
                data-address-line1="${fn:escapeXml(suggestedAddress.line1)}"
                data-address-line2="${fn:escapeXml(suggestedAddress.line2)}"
                data-address-town="${fn:escapeXml(suggestedAddress.town)}"
                data-address-postcode="${fn:escapeXml(suggestedAddress.postalCode)}"
                data-address-countrydata="${suggestedAddress.country.isocode}"
                data-address-regiondata="${suggestedAddress.region.isocode}">
            <spring:message code="address.suggestion.button.use"/>
        </button>
    </div>
</c:forEach>