<%@ tag body-content="empty" trimDirectiveWhitespaces="true" pageEncoding="UTF-8"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/cart/voucher/remove" var="removeVoucherAction"/>

<div class="total-details-row">
    <div><spring:theme code="basket.page.totals.subtotal"/></div>
    <div><format:price priceData="${cartData.subTotalWithoutDiscount}" displayFreeForZero="FALSE"/></div>
</div>

<c:if test="${not empty cartData.deliveryCost}">
    <div class="total-details-row">
        <div><spring:theme code="basket.page.totals.delivery"/></div>
        <div><format:price priceData="${cartData.deliveryCost}" displayFreeForZero="FALSE"/></div>
    </div>
</c:if>

<c:if test="${not empty cartData.appliedVouchers}">
    <div class="total-details-row" id="js-applied-vouchers">
        <div><spring:theme code="basket.page.totals.discountCodes"/></div>
        <div>
            <c:forEach items="${cartData.appliedVouchers}" var="voucher" varStatus="loop">

                <form:form id="removeVoucherForm${loop.index}" action="${removeVoucherAction}" method="post"
                           commandName="voucherForm">
                    <form:input id="voucherCode${loop.index}" hidden="hidden"  value="${voucher}" path="voucherCode"/>
																				<!-- Here's simple input because we use it in WileyAsCartValidationFilter and don't have field for it in VoucherForm -->
                    <input type="hidden" name="cartId" value="${cartData.guid }"/>
                    <a href="javascript:void(0)" class="discount-code-value js-release-voucher-remove-btn" title="${voucher}">${voucher}<i class="remove-discount">✕</i></a>
                </form:form>
            </c:forEach>
        </div>
    </div>
</c:if>

<c:if test="${cartData.totalDiscounts.value > 0}">
    <div class="total-details-row">
        <div><spring:theme code="basket.page.totals.discounts"/></div>
        <div>
            -<format:price priceData="${cartData.totalDiscounts}" displayFreeForZero="FALSE"/>
        </div>
    </div>
</c:if>

<c:if test="${cartData.net && showTax}">
    <div class="total-details-row">
        <div><spring:theme code="basket.page.totals.netTax"/></div>
        <div><format:price priceData="${cartData.totalTax}" displayFreeForZero="FALSE"/></div>
    </div>
</c:if>

<div class="total-details-row">
    <div><b><spring:theme code="order.total"/></b></div>
    <div>
        <b>
            <c:choose>
                <c:when test="${showTax}">
                    <format:price priceData="${cartData.totalPriceWithTax}" displayFreeForZero="FALSE"/>
                </c:when>
                <c:otherwise>
                    <format:price priceData="${cartData.totalPrice}" displayFreeForZero="FALSE"/>
                </c:otherwise>
            </c:choose>
        </b>
    </div>
</div>

<c:if test="${not cartData.net}">
    <div class="cart-totals-taxes text-right">
        <ycommerce:testId code="cart_taxes_label"><spring:theme code="basket.page.totals.grossTax" arguments="${cartData.totalTax.formattedValue}" argumentSeparator="!!!!"/></ycommerce:testId>
     </div>
</c:if>
