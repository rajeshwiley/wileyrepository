<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="panel">
	<a data-toggle="collapse" data-parent="#checkout-accordion"
		href="#cart-order-review" class="panel-title"> 
    <spring:theme code="text.multi.checkout.order.review" /></a>
</div>