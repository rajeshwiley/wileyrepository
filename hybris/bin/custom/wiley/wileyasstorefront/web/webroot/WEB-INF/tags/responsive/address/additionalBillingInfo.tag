<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>


<c:set var="addressCompanyLength">
    <spring:theme code="checkout.multi.paymentAddress.company.length"/>
</c:set>
<c:set var="addressCompanyPlaceholder">
    <spring:theme code="checkout.multi.paymentAddress.company.label.placeholder"/>
</c:set>
<c:set var="addressDepartmentLength">
    <spring:theme code="checkout.multi.paymentAddress.company.length"/>
</c:set>
<c:set var="addressDepartmentPlaceholder">
    <spring:theme code="checkout.multi.paymentAddress.department.placeholder"/>
</c:set>

<div class="additionalBillingInfoForm">
    <div class="form-title">
        <spring:theme code="checkout.multi.paymentAddress.additionalInfo"/>
    </div>
    <div class="form-controls-wrapper">
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox
                    idKey="address.company"
                    labelKey="checkout.multi.paymentAddress.company.label"
                    path="company"
                    placeholder="${addressCompanyPlaceholder}"
                    maxlength="${addressCompanyLength}"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox
                    idKey="address.department"
                    labelKey="checkout.multi.paymentAddress.department.label"
                    path="department"
                    placeholder="${addressDepartmentPlaceholder}"
                    maxlength="${addressDepartmentLength }"/>
            </div>
        </div>
    </div>
</div>