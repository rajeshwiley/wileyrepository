<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<ycommerce:testId code="checkoutSteps">
	<div class="checkout-breadcrumb">
		<div class="breadcrumb-item done">
			<span class="status">
			<spring:theme code="text.checkout.multi.breadcrumb.step1"/>
			</span>
			<spring:theme code="text.cart" />
		</div>
		<div class="breadcrumb-item done">
			<span class="status">
			<spring:theme code="text.checkout.multi.breadcrumb.step2"/>
			</span>
			<spring:theme code="text.checkout" />
		</div>
		<div class="breadcrumb-item in-progress">
			<span class="status">
			<spring:theme code="text.checkout.multi.breadcrumb.step3" />
			</span>
			<spring:theme code="text.confirmation" />
		</div>
	</div>
</ycommerce:testId>
