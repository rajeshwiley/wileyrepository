<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="active" required="true" type="java.lang.Boolean" %>


<div id="paypal-tab" class="tab-pane fade ${active ? 'active in' : ''}">
    <div class="form-footer">

        <c:url value="/checkout/multi/payment-method/add/paypal" var="payWithPaypalUrl" />
        <form:form action="${payWithPaypalUrl}" method="post">
            <input type="hidden" name="cartId" value="${cartData.guid}"/>
            <button type="submit" class="button button-main small" href="${payWithPaypalUrl}">
                <spring:theme code="text.checkout.multi.paypal.continue"/>
            </button>
        </form:form>
    </div>
</div>