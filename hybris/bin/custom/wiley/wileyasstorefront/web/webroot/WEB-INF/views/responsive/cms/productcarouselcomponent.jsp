<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="recommended-items-wrapper">
    <div class="recommended-items-title">${title}</div>
    <div class="recommended-items-carousel-wrapper">
        <div class="recommended-items owl-carousel">
            <c:forEach items="${productData}" var="product">
                <product:productSuggestion product="${product}"/>
            </c:forEach>
        </div>
    </div>
</div>

