<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url var="orderDetailsUrl" value="/my-account/order/${orderData.guid}"/>
<c:url var="notificationUrl" value="${notificationBaseUrl}/api/commonOrder/orderPlaced/${orderData.guid}"/>

<div id="wileyas-confirmation-message-page" notificationUrl="${notificationUrl}">
  <div class="confirmation-message-title">
    <spring:theme code="text.order.confirmation.thankYouMessage"/>
  </div>
  <spring:theme code="text.order.confirmation.confirmationMessage" arguments="${orderData.code},${orderData.paymentAddress.email},${orderDetailsUrl}"/>
</div>