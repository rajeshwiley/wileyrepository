<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="visible" value="${addressFormCountry.applicableTaxExemption == 'TAX'}"/>
<c:set var="required" value="${not empty addressForm.taxExemptionDetailsForm.taxNumber || not empty addressForm.taxExemptionDetailsForm.taxNumberExpirationDate}"/>
<c:set var="taxNumberMaxLength">
    <spring:theme code="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumber.length"/>
</c:set>
<jsp:useBean id="now" class="java.util.Date" scope="request"/>
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" var="minTaxNumberExpirationDate"/>

<div class="taxExemptionDetailsForm" style="display: ${visible ? 'block' : 'none'}">
    <div class="form-title">
        <spring:theme code="checkout.multi.paymentAddress.taxExemptionDetailsForm.title"/>
    </div>
    <div class="form-controls-wrapper">
        <div class="tax-details">
            <p class="tax-details-country-name">
                <spring:theme code="checkout.multi.paymentAddress.taxExemptionDetailsForm.disclaimer.countries"/>
            </p>
            <p>
                <spring:theme code="checkout.multi.paymentAddress.taxExemptionDetailsForm.disclaimer.message"/>
            </p>
        </div>
        <div class="input-group">
            <div class="form-field ${required ? 'required' : ''}">
                <formElement:formInputBox idKey="taxExemptionDetailsForm.taxNumber" path="taxExemptionDetailsForm.taxNumber"
                                          inputCSS="text" mandatory="true"
                                          tooltip="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumber.tooltip"
                                          labelKey="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumber.label"
                                          maxlength="${taxNumberMaxLength}"
                                          placeholder="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumber.placeholder"/>
            </div>
        </div>
        <div class="input-group">
            <div class="form-field  wiley-datepicker  ${required ? 'required' : ''}">
                <formElement:formInputBox idKey="taxExemptionDetailsForm.taxNumberExpirationDate"
                                          path="taxExemptionDetailsForm.taxNumberExpirationDate"
                                          inputCSS="form-control datepicker" mandatory="true"
                                          tooltip="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumberExpirationDate.tooltip"
                                          labelKey="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumberExpirationDate.label"
                                          min="${minTaxNumberExpirationDate}"
                                          placeholder="checkout.multi.paymentAddress.taxExemptionDetailsForm.taxNumberExpirationDate.placeholder"/>
            </div>
        </div>
    </div>
</div>