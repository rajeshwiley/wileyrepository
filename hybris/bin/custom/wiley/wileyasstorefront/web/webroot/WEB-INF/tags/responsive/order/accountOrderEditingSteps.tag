<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order"%>

<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<div id="checkout-accordion" class="panel-group">
    <order:accountOrderEditingBillingInformationStep cssClass="${cssClass}"/>
    <order:accountOrderEditingPaymentInformationStep cssClass="${cssClass}" orderData="${orderData}"/>
</div>