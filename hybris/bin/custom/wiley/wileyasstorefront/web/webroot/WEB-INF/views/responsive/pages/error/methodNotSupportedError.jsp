<%@ page trimDirectiveWhitespaces="true" isErrorPage="true" %>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Wiley</title>
    <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css">
</head>
<body>
<main>
    <div class="container-fluid">
        <div class="row">
            <header role="banner" class="header-container nav-down">
                <nav role="navigation" class="header-container-navigation">
                    <div class="navbar-header">
                        <a href="${wileyasBaseUrl}" title="Wiley" class="navigation-logo">
                         <img src="${commonResourcePath}/images/logo-mobile.png" alt="Wiley">
                        </a>
                    </div>
                    <div id="willey-navbar-collapse" class="collapse navbar-collapse">
                        <div class="container-navigation">
                            <div class="container-navigation-item-1">
                                <a href="${wileyasBaseUrl}" title="Wiley" class="navigation-logo">
                                    <img src="${commonResourcePath}/images/logo-desktop.png" alt="Wiley">
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
        </div>
    </div>
    <div class="container error-page">
        <div class="row">
            <div class="col-xs-12">
                <div class="error-block"><img src="${commonResourcePath}/images/405error_icon@2x.png" class="error-image">
                    <h1 class="error-title">CLIENT ERROR (405)</h1>
                    <p class="error-text">
                    Please try again later or <a href='https://hub.wiley.com/community/support/authorservices/pages/public-form'>
                    contact us</a>&nbsp; if this issue persists.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <footer role="contentinfo" class="main-footer-container">
        <div class="container">
            <div class="footer-copyright">
                <a href="${wileyasBaseUrl}" title="Wiley" class="footer-logo">
                    <img src="${commonResourcePath}/images/wiley-logo-footer@3x.png" alt="Wiley logo">
                </a>
                <div class="footer-copyright-text">
                 <p>
																	 Copyright &copy; 2000-${currentYear} by John Wiley & Sons, Inc., or related companies.	All rights reserved.
																 </p>
                </div>
            </div>
        </div>
    </footer>
</main>

<!-- scripts-->
<script src="${commonResourcePath}/js/jquery-2.1.1.min.js"></script>
<script src="${commonResourcePath}/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${commonResourcePath}/js/owl.carousel-2.min.js"></script>
<script src="${commonResourcePath}/js/jquery-ui-1.12.1.min.js"></script>
<script src="${commonResourcePath}/js/perfect-scrollbar.jquery.min.js"></script>
<script src="${commonResourcePath}/js/jquery.validate.min.js"></script>
<script src="${commonResourcePath}/js/jsvat.custom.js"></script>
<script src="${commonResourcePath}/js/jquery.mask.min.js"></script>
<script src="${commonResourcePath}/js/wiley.js"></script>
</body>
</html>