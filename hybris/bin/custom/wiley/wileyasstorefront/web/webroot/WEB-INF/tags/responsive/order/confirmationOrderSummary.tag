<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<div class="confirmation-order-summary">
    <h3 class="order-review-section-title"><spring:theme code="text.order.confirmation.summary.title"/></h3>
    <div class="order-review-items-list">
        <div class="order-review-item">
            <div class="order-review-label">
                <spring:theme code="text.account.order.orderNumber" text="Order Number"/>
            </div>
            <div class="order-review-value"><b>${order.code}</b></div>
        </div>
        <div class="order-review-item">
            <div class="order-review-label">
                <spring:theme code="text.account.order.orderPlaced" text="Date Placed"/>
            </div>
            <div class="order-review-value"><fmt:formatDate pattern="dd MMMM YYYY hh:mm a" value="${order.created}"/></div>
        </div>
        <div class="order-review-item">
            <div class="order-review-label">
                <spring:theme code="text.account.order.orderTotals"/>
            </div>
            <div class="order-review-value">
                <c:choose>
                    <c:when test="${order.net}">
                        <format:price priceData="${order.totalPriceWithTax}" displayFreeForZero="FALSE"/>
                    </c:when>
                    <c:otherwise>
                        <format:price priceData="${order.totalPrice}" displayFreeForZero="FALSE"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="order-review-item"></div>
    </div>
</div>