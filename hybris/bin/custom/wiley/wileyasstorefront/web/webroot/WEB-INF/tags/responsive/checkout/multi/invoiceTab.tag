<%@ attribute name="paymentMode" required="true" type="java.lang.String" %>
<%@ attribute name="formName" required="true" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="active" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showSubmitButton" required="false" type="java.lang.Boolean" %>
<c:set var="showSubmitButton" value="${showSubmitButton != null ? showSubmitButton : true}"/>

<c:set var="purchaseOrderNumberLength">
    <spring:theme code="checkout.multi.payment.${paymentMode}.purchaseOrderNumber.length"/>
</c:set>
<c:set var="descriptionLength">
    <spring:theme code="checkout.multi.payment.${paymentMode}.description.length"/>
</c:set>
<c:set var="purchaseOrderNumberPlaceholder">
    <spring:theme code="checkout.multi.payment.${paymentMode}.purchaseOrderNumber.placeholder"/>
</c:set>
<c:set var="descriptionPlaceholder">
    <spring:theme code="checkout.multi.payment.${paymentMode}.description.placeholder"/>
</c:set>


<div id="${paymentMode}-tab" class="tab-pane fade ${active ? 'active in' : ''}">
    <form:form method="post" action="/authorservices/checkout/multi/payment-method/add/${paymentMode}"
               commandName="${formName}" id="${formName}">
        <input type="hidden" name="cartId" value="${cartData.guid }"/>
        <div class="form-content">
            <div class="form-title"><spring:theme code="checkout.multi.payment.name"/>
                <spring:theme code="checkout.multi.payment.${paymentMode}.name"/>
            </div>
            <div class="form-controls-wrapper">
                <div class="input-group characters-counter">
                    <div class="form-field">
                        <label for="${paymentMode}Name">
                            <spring:theme code="checkout.multi.payment.${paymentMode}.purchaseOrderNumber.name"/>
                            <span class="popover-help-icon">&nbsp;
                                <a role="menuitem"
                                   tabindex="0"
                                   data-toggle="popover"
                                   aria-label="information icon"
                                   data-content='<spring:theme code="checkout.multi.payment.${paymentMode}.purchaseOrderNumber.tooltip"/>'
                                   class="popover-help"></a>
                                </span>
                        </label>
                        <form:input id="${paymentMode}Name" path="purchaseOrderNumber"
                                    maxlength="${purchaseOrderNumberLength}" name="${paymentMode}Name" type="text"
                                    placeholder="${purchaseOrderNumberPlaceholder}" class="form-control"/>
                    </div>
                    <i class="characters-counter-result"><spring:theme code="checkout.multi.payment.charactersLeft.name"/>
                        <span class="characters-counter-value">${purchaseOrderNumberLength}</span>
                    </i>
                </div>
                <div class="input-group characters-counter">
                    <div class="form-field">
                        <label for="${paymentMode}Description"><spring:theme code="checkout.multi.payment.${paymentMode}.description.name"/>
                            <span class="popover-help-icon">&nbsp;
                                <a role="menuitem"
                                   tabindex="0"
                                   data-toggle="popover"
                                   aria-label="information icon"
                                   data-content='<spring:theme code="checkout.multi.payment.${paymentMode}.description.tooltip"/>'
                                   class="popover-help"></a>
                                </span>
                        </label>
                        <form:textarea id="${paymentMode}Description" path="userNotes"
                                       maxlength="${descriptionLength}" name="${paymentMode}Description"
                                       placeholder="${descriptionPlaceholder}" class="form-control"/>
                    </div>
                    <i class="characters-counter-result"><spring:theme code="checkout.multi.payment.charactersLeft.name"/>
                        <span class="characters-counter-value">${descriptionLength}</span>
                    </i>
                </div>
            </div>
        </div>
        <c:if test="${showSubmitButton}">
             <div class="form-footer">
                <button type="submit" class="button button-main small">
                    <spring:theme code="checkout.multi.payment.continue"/>
                </button>
             </div>
        </c:if>
    </form:form>
</div>