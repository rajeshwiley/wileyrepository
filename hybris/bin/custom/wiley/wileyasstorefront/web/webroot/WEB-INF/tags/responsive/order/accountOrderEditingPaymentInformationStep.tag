<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>

<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>

<div id="paymentInformationStep" class="panel ${cssClass}">
    <a class="panel-title completed" data-toggle="collapse" href="#paymentInformationPanel">
        <spring:theme code="checkout.multi.paymentMethod"/>
    </a>
    <div id="paymentInformationPanel" class="panel-collapse in">
        <div class="panel-body">
            <multiCheckout:addPaymentMethodStep abstractOrderData="${orderData}" showCurrentPaymentMode="true"
                                                showNextStepButton="false"/>
        </div>
    </div>
</div>