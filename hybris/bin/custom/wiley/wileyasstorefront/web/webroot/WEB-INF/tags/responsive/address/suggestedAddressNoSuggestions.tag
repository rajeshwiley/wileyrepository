<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>

<%@ attribute name="wileyBillingAddressForm" required="true" type="com.wiley.storefrontcommons.forms.WileyAddressForm"%>
<%@ attribute name="suggestionSubmitBtnId" required="true" type="java.lang.String"%>

<spring:htmlEscape defaultHtmlEscape="true"/>


<div class="saved-card-info suggested-address">
    <div class="order-details-info-message">
        <spring:message code="address.suggestion.no.sugestions"/>
    </div>
    <div class="saved-info-title">
        <spring:message code="address.suggestion.title.default"/>
    </div>
    <address:suggestedAddressLayout line1="${wileyBillingAddressForm.line1}"
                                    line2="${wileyBillingAddressForm.line2}"
                                    town="${wileyBillingAddressForm.townCity}"
                                    postCode="${wileyBillingAddressForm.postcode}"
                                    countryData="${addressFormCountry}"
                                    regionData="${addressFormRegion}"/>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="button ">
            <spring:message code="address.suggestion.button.edit"/>
        </button>
        <button id="${suggestionSubmitBtnId}" type="button" class="button button-main"
                data-address-line1="${fn:escapeXml(wileyBillingAddressForm.line1)}"
                data-address-line2="${fn:escapeXml(wileyBillingAddressForm.line2)}"
                data-address-town="${fn:escapeXml(wileyBillingAddressForm.townCity)}"
                data-address-postcode="${fn:escapeXml(wileyBillingAddressForm.postcode)}"
                data-address-countrydata="${addressFormCountry.isocode}"
                data-address-regiondata="${addressFormRegion.isocode}">
            <spring:message code="address.suggestion.button.use"/>
        </button>
    </div>
</div>