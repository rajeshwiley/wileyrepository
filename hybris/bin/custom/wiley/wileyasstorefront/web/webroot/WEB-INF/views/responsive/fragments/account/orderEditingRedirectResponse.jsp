<%@ page trimDirectiveWhitespaces="true" contentType="text/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<spring:url value="/my-account/order/${orderData.guid}" var="redirectUrl"/>

{
 "redirectUrl": "${redirectUrl}"
}
