<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="wileyAddonError" tagdir="/WEB-INF/tags/addons/wileystorefrontcommons/responsive/error" %>

<template:page pageTitle="${pageTitle}">
    <cart:cartValidation/>
    <cart:cartPickupValidation/>

    <div class="container shopping-cart-page">
        <div class="row">
            <div class="col-xs-12">

                <cms:pageSlot position="TopContent" var="feature">
                    <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
                </cms:pageSlot>

                <c:if test="${not empty cartData.entries}">
                    <cms:pageSlot position="CenterLeftContentSlot" var="feature">
                        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
                    </cms:pageSlot>
                </c:if>

                <c:if test="${not empty cartData.entries}">
                    <cms:pageSlot position="CenterRightContentSlot" var="feature">
                        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
                    </cms:pageSlot>
                    <cms:pageSlot position="BottomContentSlot" var="feature">
                        <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
                    </cms:pageSlot>
                </c:if>
                <c:if test="${empty cartData.entries}">
                    <cms:pageSlot position="EmptyCartMiddleContentSlot" var="feature">
                        <cms:component component="${feature}" element="div" class="yComponentWrapper shopping-cart-empty"/>
                    </cms:pageSlot>
                </c:if>

                <c:if test="${empty cartData.entries}">
                    <cms:pageSlot position="EmptyCartBottomContentSlot" var="feature">
                        <c:choose>
                            <c:when test="${fn:containsIgnoreCase(feature['class'], 'ProductCarouselComponentModel') && empty feature.products}"></c:when>
                            <c:otherwise>
                                <cms:component component="${feature}" element="div" class="yComponentWrapper"/>
                            </c:otherwise>
                        </c:choose>
                    </cms:pageSlot>
                </c:if>
            </div>
        </div>

    </div>
</template:page>