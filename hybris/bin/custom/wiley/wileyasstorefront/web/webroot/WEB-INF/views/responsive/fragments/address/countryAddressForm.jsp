<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="tax" tagdir="/WEB-INF/tags/responsive/tax"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${not empty country}">
    <form:form commandName="addressForm" data-applicable-tax-exemption="${not empty addressFormCountry.applicableTaxExemption ? addressFormCountry.applicableTaxExemption : ''}">
        <address:addressFormElements regions="${regions}" country="${country}"/>
    </form:form>
</c:if>