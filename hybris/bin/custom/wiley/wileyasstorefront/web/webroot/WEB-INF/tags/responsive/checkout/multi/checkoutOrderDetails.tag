<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>

<div class="order-summary">
	<h2 class="order-summary-title"><spring:theme code="checkout.multi.order.summary" /></h2>
	<div class="order-summary-product-summ">
		 <div class="product-summ-wr">
			<div class="product-summ-image">
			 <c:set var="firstEntry" value="${cartData.entries[0]}" />
				<c:choose>
					<c:when test="${not empty firstEntry.article.pictureUrl}">
						<img src="${firstEntry.article.pictureUrl}" />
					</c:when>
					<c:otherwise>
						<product:productPrimaryImage product="${firstEntry.product}"/>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="product-summ-description">
				<c:forEach items="${cartData.entries}" var="entry">
					<div class="description-items-list">
						<span class="product-label">${entry.product.name}</span>
						<c:if test="${not empty entry.article.journalName}">
						<h4 class="item-description-title">${entry.article.journalName}</h4>
						</c:if>
					</div>
				</c:forEach>
			</div>
			<a data-toggle="collapse" href="#productTable" role="button"
				aria-expanded="false" aria-controls="productTable"
				class="button-view-details collapsed"><spring:theme code="text.button.view.detail" /></a> 
		</div>
		<div class="product-summ-total">
			<div class="total-row">
				<div class="item-total-label"><spring:theme code="text.multi.checkout.ordersummary.subtotal" /></div>
				<div class="item-total-value">
					<format:price priceData="${cartData.subTotalWithoutDiscount}" displayFreeForZero="FALSE"/>
				</div>
			</div>
			<c:if test="${cartData.totalDiscounts.value > 0}">
			<div class="total-row">
				<div class="item-total-label"><spring:theme code="text.multi.checkout.ordersummary.discount" /></div>
				<div class="item-total-value">-<format:price priceData="${cartData.totalDiscounts}" displayFreeForZero="FALSE"/></div>
			</div>
			</c:if>
			<c:if test="${cartData.net && showTax}">
            	<div class="total-row">
            		<div class="item-total-label"><spring:theme code="basket.page.totals.netTax"/></div>
            		<div class="item-total-value"><format:price priceData="${cartData.totalTax}" displayFreeForZero="FALSE"/></div>
            	</div>
            </c:if>
			<div class="total-row">
				<div class="item-total-label"><spring:theme code="text.multi.checkout.ordersummary.total" /></div>
				<div class="item-total-value">
				<c:choose>
					<c:when test="${showTax}">
						<format:price priceData="${cartData.totalPriceWithTax}" displayFreeForZero="FALSE"/>
					</c:when>
					<c:otherwise>
						<format:price priceData="${cartData.totalPrice}" displayFreeForZero="FALSE"/>
					</c:otherwise>
				</c:choose>
				</div>
			</div>
		</div>
		<div id="productTable"
			class="product-summ-table products-table collapse">
			<div class="table-body">
				<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
					<div class="table-body-row">
						<div class="table-body-item item-image">
							<c:choose>
								<c:when test="${not empty entry.article.pictureUrl}">
									<img src="${entry.article.pictureUrl}" />
								</c:when>
								<c:otherwise>
									<product:productPrimaryImage product="${entry.product}"/>
								</c:otherwise>
							</c:choose>
						</div>
						<div class="table-body-mobile-wrap">
							<div class="table-body-item item-description">
								<span class="product-label">${entry.product.name}</span>
								<c:if test="${not empty entry.article.journalName}">
								<h4 class="item-description-title">${entry.article.journalName}</h4>
								</c:if>
								<p class="item-description-text">
									${entry.article.summary}</p>
								<div class="description-extra-info">
									<c:if test="${not empty entry.article.doi}">
										<div class="extra-item">
											<p class="item-title">
												<spring:theme code="product.article.doi" />
											</p>
											<p class="item-value">${entry.article.doi}</p>
										</div>
									</c:if>
									<c:if test="${not empty entry.namedDeliveryDate}">
										<div class="extra-item">
											<p class="item-title">
												<spring:theme code="basket.page.delivery.date" />
											</p>
											<p class="item-value">
												<fmt:formatDate pattern="dd MMMM YYYY"
													value="${entry.namedDeliveryDate}" />
											</p>
										</div>
									</c:if>
									<product:productListerClassifications product="${entry.product}"/>
								</div>
							</div>
							<div class="table-body-item item-quantity">
								<c:if test="${entry.product.countable }">
									<p class="item-title-mobile"><spring:theme code="basket.page.quantity" /></p>
									<p class="item-value">${entry.quantity }</p>
								</c:if>
							</div>
							<div class="table-body-item item-total">
								<p class="item-title-mobile">
									<spring:theme code="basket.page.total" />
								</p>
								<p class="item-value">
									<format:price priceData="${entry.totalPrice}"
										displayFreeForZero="true"
										displayCancelled="${entry.status.code == 'CANCELLED'}"/>
								</p>
							</div>
						</div>
						<checkout:checkoutPromotionItems cartData="${cartData}" entry="${entry}"/>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>