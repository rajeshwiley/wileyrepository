<%@ page trimDirectiveWhitespaces="true" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multiCheckout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags/responsive/error" %>

<spring:url value="/my-account/order/${orderData.guid}" var="orderUrl"/>
<spring:url value="/my-account/order/${orderData.guid}/update" var="orderUpdateUrl"/>
<spring:url value="/my-account/order/${orderData.guid}/updateConfirm" var="orderUpdateConfirmUrl"/>
<spring:url value="/my-account/order/${orderData.guid}/suggestion" var="orderSuggestionAddresses"/>

<template:page pageTitle="${pageTitle}">
    <jsp:attribute name="pageScripts">
        <script src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/payment/wileyPaymentForm.js"></script>
        <script src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/wileyOrderForm.js"></script>
    </jsp:attribute>
    <jsp:body>
        <error:paymentGlobalAlert messageKey="global-error-message-list"/>
        <cms:pageSlot position="SideContent" var="feature" class="accountPageSideContent">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
        <div class="container shopping-cart-page shopping-cart-history-details order-edit-page">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shopping-cart-top">
                        <order:orderNumber orderData="${orderData}"/>
                        <spring:message code="text.account.orderEditing.back.to.orderDetails" var="back_to_order_details"/>
                        <a href="${orderUrl}" title="${back_to_order_details}"
                                    class="button button-secondary button-back-to-order">${back_to_order_details}</a>
                        <h1 class="main-page-title icon-shopping-cart">
                            <spring:message code="text.account.orderEditing.title.editing"/>
                        </h1>
                    </div>
                    <multiCheckout:checkoutOrderDetails cartData="${orderData}" showDeliveryAddress="false" showPaymentInfo="false"
                                showTaxEstimate="false" showTax="${orderData.taxAvailable}"/>
                    <order:accountOrderEditingSteps orderData="${orderData}"/>
                    <form:form id="wileyOrderForm" commandName="wileyOrderForm" data-order-update-url="${orderUpdateUrl}"
                                                                                data-order-update-confirm-url="${orderUpdateConfirmUrl}"
                                                                                data-order-suggestion-addresses-url="${orderSuggestionAddresses}">
                        <button id="submitWileyOrderForm" type="submit" class="button button-main right place-order-btn" disabled="disabled">
                            <spring:theme code="text.account.orderEditing.saveChanges" text="Save Changes"/>
                        </button>
                    </form:form>
                    <!-- Modal asdasd-->
                    <div id="confirmOrderUpdateModal" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade"/>
                </div>
            </div>
        </div>
    </jsp:body>
</template:page>
