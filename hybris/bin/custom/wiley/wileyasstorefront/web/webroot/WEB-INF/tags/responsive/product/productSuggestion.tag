<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="displayProductTitles" type="java.lang.Boolean" %>
<%@ attribute name="displayProductPrices" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>


<div class="recommended-item">
    <div class="recommended-item-image">
        <product:productPrimaryImage product="${product}"/>
    </div>
    <div class="recommended-item-info">
        <c:if test="${empty displayProductTitles || displayProductTitles}">
            <div class="recommended-item-title" title="${product.name}">${product.name}</div>
        </c:if>
        <c:if test="${product.baseProduct ne null && product.price ne null 
                    && (empty displayProductPrices || displayProductPrices)}">
            <div class="recommended-item-price-title">
                <spring:theme
                    code="text.productcarouselcomponent.price.title" />
            </div>
            <div class="recommended-item-price">
                <format:fromPrice priceData="${product.price}" />
            </div>
        </c:if>
        <div class="recommended-item-button">
            <a href="${product.productPageUrl}" class="button">
                <spring:theme code="text.productcarouselcomponent.button"/>
            </a>
        </div>
    </div>
</div>