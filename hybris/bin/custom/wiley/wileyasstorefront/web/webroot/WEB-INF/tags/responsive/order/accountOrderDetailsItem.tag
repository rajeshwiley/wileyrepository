<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<spring:url value="/my-account/pre-order-entry-cancel/${order.guid}" var="orderEntryCancelUrl"/>
<spring:url value="/my-account/error-pre-order-entry-cancel/${order.guid}/" var="errorOrderEntryCancelUrl"/>

<div class="table-body">
	<c:forEach items="${order.entries}" var="entry">
		<div class="table-body-row">
			<div class="table-body-item item-image">
				<c:choose>
					<c:when test="${not empty entry.article.pictureUrl}">
						<img src="${entry.article.pictureUrl}"/>
					</c:when>
					<c:otherwise>
						<product:productPrimaryImage product="${entry.product}"/>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="table-body-mobile-wrap">
				<div class="table-body-item item-description">
					<span class="product-label">${entry.product.name}</span>
					<c:if test="${not empty entry.article.journalName}">
					<h4 class="item-description-title">${entry.article.journalName}</h4>
					</c:if>
					<p class="item-description-text">${entry.article.summary}</p>
					<div class="description-extra-info">
						<div class="extra-item mobile">
							<p class="item-title"><spring:message code="text.account.orderHistory.entry.status"/>
							</p>
							<p class="item-value ${entry.status.code == 'CANCELLED' ? 'item-cancelled' : ''}"><spring:message
									code="text.account.orderHistory.status.${entry.status.code}" text=""/></p>
						</div>
						<c:if test="${not empty entry.article.doi}">
							<div class="extra-item">
								<p class="item-title"><spring:message code="text.account.orderHistory.entry.doi"/></p>
								<p class="item-value">${entry.article.doi}</p>
							</div>
						</c:if>
						<c:if test="${not empty entry.namedDeliveryDate}">
							<div class="extra-item">
								<p class="item-title"><spring:message code="text.account.orderHistory.entry.delivery.date"/></p>
								<p class="item-value"><fmt:formatDate pattern="dd MMMM YYYY"
																	  value="${entry.namedDeliveryDate}"/></p>
							</div>
						</c:if>

						<product:productListerClassifications product="${entry.product}"/>
					</div>
				</div>
				<div class="table-body-item item-status desktop">
					<p class="item-title-mobile"><spring:message code="text.account.orderHistory.entry.status"/></p>
					<p class="item-value ${entry.status.code == 'CANCELLED' ? 'item-cancelled' : ''}"><spring:message
							code="text.account.orderHistory.status.${entry.status.code}" text=""/></p>
				</div>
				<div class="table-body-item item-quantity">
					<c:if test="${entry.product.countable and entry.status.code != 'CANCELLED'}">
						<p class="item-title-mobile"><spring:message code="text.account.orderHistory.entry.quantity"/></p>
						<p class="item-value">${entry.quantity}</p>
					</c:if>
				</div>
				<div class="table-body-item item-total">
					<p class="item-title-mobile"><spring:message code="text.account.orderHistory.entry.total"/></p>
					<c:if test="${not order.orderConfiguration.hidePricing and entry.status.code != 'CANCELLED'}">
						<p class="item-value">
							<format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
						</p>
					</c:if>
				</div>
				<c:if test="${cancellableEntry ne entry.entryNumber and entry.cancelable}">
					<div class="table-body-item item-remove">
						<a title="Remove" class="button-remove" onclick="cancelLineItem(${entry.entryNumber})">
							<spring:message code="text.account.orderHistory.entry.action.remove"/>
						</a>
					</div>
				</c:if>
				<c:if test="${entry.hasInvoice}">
					<spring:message code="text.account.orderHistory.view.invoice" var="viewInvoiceText"/>
					<spring:url value="/my-account/invoice/${entry.guid}" var="invoiceUrl"/>
					<a title="${viewInvoiceText}" href="${invoiceUrl}" class="button button-main item-invoice-button">
							${viewInvoiceText}
					</a>
				</c:if>
			</div>
			<c:if test="${entry.status.code != 'CANCELLED'}">
				<div class="item-price-discount">
					<c:forEach items="${order.appliedProductPromotions}" var="promotion">
						<c:set var="displayed" value="false"/>
						<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
							<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
								<c:set var="displayed" value="true"/>
								<ycommerce:testId code="cart_potentialPromotion_label">
									<p>${promotion.description}</p>
								</ycommerce:testId>
							</c:if>
						</c:forEach>
					</c:forEach>
				</div>
			</c:if>
		</div>
	</c:forEach>
</div>

<script type="text/javascript">
	function cancelLineItem(entryNumber) {
		$.ajax({
			type: 'GET',
			url: "${orderEntryCancelUrl}",
			data: {entryNumber: entryNumber},
			success: function (response) {
				$('#removeOrderLineModal').html(response).modal('show');
				wiley.displayCustomSelect();
			},
			error: function (response) {
				location.replace("${errorOrderEntryCancelUrl}" + String(entryNumber));
			}
		})
	}
</script>
