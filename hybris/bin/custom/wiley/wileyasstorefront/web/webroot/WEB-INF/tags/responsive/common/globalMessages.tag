<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${(not empty accSuccessMsgs) || (not empty accConfMsgs) || (not empty accInfoMsgs) || (not empty accErrorMsgs) || (not empty staticErrorMsgs)}">

	<div id="global-messages" class="flash-message-wrapper">
			<%-- Information (confirmation) messages --%>
		<c:if test="${not empty accConfMsgs}">
			<c:forEach items="${accConfMsgs}" var="msg">
				<div class="flash-message informational">
					<div class="container">
						<span class="flash-message-text"><spring:theme code="${msg.code}" arguments="${msg.attributes}"/></span>
						<button class="close"><span>&times;</span></button>
					</div>
				</div>
			</c:forEach>
		</c:if>

			<%-- Success messages --%>
		<c:if test="${not empty accSuccessMsgs}">
			<c:forEach items="${accSuccessMsgs}" var="msg">
				<div class="flash-message success">
					<div class="container">
						<span class="flash-message-text"><spring:theme code="${msg.code}" arguments="${msg.attributes}"/></span>
						<button class="close"><span>&times;</span></button>
					</div>
				</div>
			</c:forEach>
		</c:if>

			<%-- Warning messages --%>
		<c:if test="${not empty accInfoMsgs}">
			<c:forEach items="${accInfoMsgs}" var="msg">
				<div class="flash-message warning">
					<div class="container">
						<span class="flash-message-text"><spring:theme code="${msg.code}" arguments="${msg.attributes}"/></span>
						<button class="close"><span>&times;</span></button>
					</div>
				</div>
			</c:forEach>
		</c:if>

			<%-- Error messages (includes spring validation messages)--%>
		<c:if test="${not empty accErrorMsgs}">
			<c:forEach items="${accErrorMsgs}" var="msg">
				<div class="flash-message error">
					<div class="container">
						<span class="flash-message-text"><spring:theme code="${msg.code}" arguments="${msg.attributes}"/></span>
						<button class="close"><span>&times;</span></button>
					</div>
				</div>
			</c:forEach>
		</c:if>

		<c:if test="${not empty staticErrorMsgs}">
			<c:forEach items="${staticErrorMsgs}" var="msg">
				<div class="flash-message error">
					<div class="container">
						<span class="flash-message-text"><spring:theme code="${msg.code}" arguments="${msg.attributes}"/></span>
						<button class="close"><span>&times;</span></button>
					</div>
				</div>
			</c:forEach>
		</c:if>
	</div>
</c:if>
