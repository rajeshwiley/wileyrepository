<%@ page trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="asm-panel">
    <div class="container">
        <p class="asm-title"><spring:message code="text.asm.mode.panel.mode"/></p>
        <div class="asm-user-info">
            <span class="asm-info-label"><spring:message code="text.asm.mode.panel.customer"/></span>
            <span class="asm-info-value">${wiley_as_emulatedUser.firstName}&nbsp;${wiley_as_emulatedUser.lastName}</span>
        </div>
        <div class="asm-agent-info">
            <span class="asm-info-label"><spring:message code="text.asm.mode.panel.agent"/></span>
            <span class="asm-info-value">${wiley_as_agent.name}</span>
        </div>
    </div>
</div>
