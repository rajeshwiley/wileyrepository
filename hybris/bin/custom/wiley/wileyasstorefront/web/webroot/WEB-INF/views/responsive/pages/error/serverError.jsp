<%@ page trimDirectiveWhitespaces="true" isErrorPage="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css">
    <title>
        <spring:theme code="error.page.server.error.title" text="500"/>
    </title>
</head>
<body>
<spring:theme code="error.page.server.error.alt.wiley" text="Wiley" var="altText"/>
<spring:theme code="error.page.server.error.alt.logo.wiley" text="Wiley logo" var="altWileyLogo"/>
<spring:theme code="error.page.server.error.header.title" text="AUTHOR SERVICES" var="headerTitle"/>
<main>
    <div class="container-fluid">
        <div class="row">
            <header role="banner" class="header-container nav-down">
                <nav role="navigation" class="header-container-navigation">
                    <div class="navbar-header">
                        <a href="https://authorservices.wiley.com/" title="${altText}"
                           class="navigation-logo">
                            <img src="${commonResourcePath}/images/logo-mobile.png"
                                 alt="${altText}">
                        </a>
                    </div>
                    <div id="willey-navbar-collapse" class="collapse navbar-collapse">
                        <div class="container-navigation">
                            <div class="container-navigation-item-1">
                                <a href="${wileyasBaseUrl}" title="${altText}"
                                   class="navigation-logo">
                                    <img src="${commonResourcePath}/images/logo-desktop.png"
                                         alt="${altText}">
                                </a>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
        </div>
    </div>
    <div class="container error-page">
        <div class="row">
            <div class="col-xs-12">
                <div class="error-block"><img src="${commonResourcePath}/images/500error_icon@2x.png" class="error-image">
                    <h1 class="error-title">
                        <spring:theme code="error.page.server.error.main.title" text="SERVER ERROR (500)"/>
                    </h1>
                    <p class="error-text">
                        <spring:theme code="error.page.server.error.main.text"
                            text="Apologies, we are currently experiencing a server error.<br>Please try again later or &nbsp;<a href='https://hub.wiley.com/community/support/authorservices/pages/public-form'>contact us</a>&nbsp; if this issue persists."/>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <footer role="contentinfo" class="main-footer-container">
        <div class="container">
            <div class="footer-copyright">
                <a href="${wileyasBaseUrl}" title="${altText}"
                   class="footer-logo">
                    <img src="${commonResourcePath}/images/wiley-logo-footer@3x.png"
                         alt="${altWileyLogo}">
                </a>
                <div class="footer-copyright-text">
                    <p>
                        <spring:theme code="error.page.server.error.copyright"
                                      text="Copyright &#169; 2000-{0} by John Wiley & Sons, Inc., or related companies. All rights reserved."
                                      arguments="<script>document.write(new Date().getFullYear())</script>"/>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</main>
<!-- scripts-->
<script src="${commonResourcePath}/js/jquery-2.1.1.min.js"></script>
<script src="${commonResourcePath}/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${commonResourcePath}/js/owl.carousel-2.min.js"></script>
<script src="${commonResourcePath}/js/jquery-ui-1.12.1.min.js"></script>
<script src="${commonResourcePath}/js/perfect-scrollbar.jquery.min.js"></script>
<script src="${commonResourcePath}/js/jquery.validate.min.js"></script>
<script src="${commonResourcePath}/js/jsvat.custom.js"></script>
<script src="${commonResourcePath}/js/jquery.mask.min.js"></script>
<script src="${commonResourcePath}/js/wiley.js"></script>
</body>
</html>