<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="visible" value="${addressFormCountry.applicableTaxExemption == 'VAT'}"/>
<c:set var="taxNumberMaxLength">
    <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.length"/>
</c:set>
<div class="vatRegistrationDetailsForm" style="display: ${visible ? 'block' : 'none'}">
    <div class="form-title">
        <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.title"/>
    </div>
    <div class="form-controls-wrapper">
        <div class="tax-details">
            <p class="tax-details-country-name">
                <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.disclaimer.countries"/>
            </p>
            <p>
                <spring:theme code="checkout.multi.paymentAddress.vatRegistrationDetailsForm.disclaimer.message"/>
            </p>
        </div>
        <div class="input-group">
            <div class="form-field">
                <formElement:formInputBox idKey="vatRegistrationDetailsForm.taxNumber" path="vatRegistrationDetailsForm.taxNumber"
                                          inputCSS="text" mandatory="true"
                                          tooltip="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.tooltip"
                                          labelKey="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.label"
                                          maxlength="${taxNumberMaxLength}"
                                          placeholder="checkout.multi.paymentAddress.vatRegistrationDetailsForm.taxNumber.placeholder"/>
            </div>
        </div>
    </div>
</div>