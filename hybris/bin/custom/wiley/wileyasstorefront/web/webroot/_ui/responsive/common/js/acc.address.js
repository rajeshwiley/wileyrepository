/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
ACC.address = {

    _autoload: [
        "bindToChangeAddressButton",
        "bindCreateUpdateAddressForm",
        "bindSuggestedDeliveryAddresses",
        "bindCountrySpecificAddressForms",
        "showAddressFormButtonPanel",
        "bindViewAddressBook",
        "bindToColorboxClose",
        "showRemoveAddressFromBookConfirmation",
        "backToListAddresses",
        "bindAddressFormToggleSubmitBtnOnChange",
        "bindInteractivePatternValidators",
        "toggleSubmitBtnForCheckout"
    ],

    spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
    addressID: '',

    handleChangeAddressButtonClick: function() {


        ACC.address.addressID = ($(this).data("address")) ? $(this).data("address") : '';
        $('#summaryDeliveryAddressFormContainer').show();
        $('#summaryOverlayViewAddressBook').show();
        $('#summaryDeliveryAddressBook').hide();


        $.getJSON(getDeliveryAddressesUrl, ACC.address.handleAddressDataLoad);
        return false;
    },

    handleAddressDataLoad: function(data) {
        ACC.address.setupDeliveryAddressPopupForm(data);

        // Show the delivery address popup
        ACC.colorbox.open("", {
            inline: true,
            href: "#summaryDeliveryAddressOverlay",
            overlayClose: false,
            onOpen: function() {
                // empty address form fields
                ACC.address.emptyAddressForm();
                $(document).on('change', '#saveAddress', function() {
                    var saveAddressChecked = $(this).prop('checked');
                    $('#defaultAddress').prop('disabled', !saveAddressChecked);
                    if (!saveAddressChecked) {
                        $('#defaultAddress').prop('checked', false);
                    }
                });
            }
        });

    },

    setupDeliveryAddressPopupForm: function(data) {
        // Fill the available delivery addresses
        $('#summaryDeliveryAddressBook').html($('#deliveryAddressesTemplate').tmpl({
            addresses: data
        }));
        // Handle selection of address
        $('#summaryDeliveryAddressBook button.use_address').click(ACC.address.handleSelectExistingAddressClick);
        // Handle edit address
        $('#summaryDeliveryAddressBook button.edit').click(ACC.address.handleEditAddressClick);
        // Handle set default address
        $('#summaryDeliveryAddressBook button.default').click(ACC.address.handleDefaultAddressClick);
    },

    emptyAddressForm: function() {
        var options = {
            url: getDeliveryAddressFormUrl,
            data: {
                addressId: ACC.address.addressID,
                createUpdateStatus: ''
            },
            type: 'GET',
            success: function(data) {
                $('#summaryDeliveryAddressFormContainer').html(data);
                ACC.address.bindCreateUpdateAddressForm();
            }
        };

        $.ajax(options);
    },

    handleSelectExistingAddressClick: function() {
        var addressId = $(this).attr('data-address');
        $.postJSON(setDeliveryAddressUrl, {
            addressId: addressId
        }, ACC.address.handleSelectExitingAddressSuccess);
        return false;
    },

    handleEditAddressClick: function() {

        $('#summaryDeliveryAddressFormContainer').show();
        $('#summaryOverlayViewAddressBook').show();
        $('#summaryDeliveryAddressBook').hide();

        var addressId = $(this).attr('data-address');
        var options = {
            url: getDeliveryAddressFormUrl,
            data: {
                addressId: addressId,
                createUpdateStatus: ''
            },
            target: '#summaryDeliveryAddressFormContainer',
            type: 'GET',
            success: function(data) {
                ACC.address.bindCreateUpdateAddressForm();
                ACC.colorbox.resize();
            },
            error: function(xht, textStatus, ex) {
                alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
            }
        };

        $(this).ajaxSubmit(options);
        return false;
    },

    handleDefaultAddressClick: function() {
        var addressId = $(this).attr('data-address');
        var options = {
            url: setDefaultAddressUrl,
            data: {
                addressId: addressId
            },
            type: 'GET',
            success: function(data) {
                ACC.address.setupDeliveryAddressPopupForm(data);
            },
            error: function(xht, textStatus, ex) {
                alert("Failed to update address book. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
            }
        };

        $(this).ajaxSubmit(options);
        return false;
    },

    handleSelectExitingAddressSuccess: function(data) {
        if (data != null) {
            ACC.refresh.refreshPage(data);
            ACC.colorbox.close();
        } else {
            alert("Failed to set delivery address");
        }
    },

    bindCreateUpdateAddressForm: function() {
        $('.create_update_address_form').each(function() {
            var options = {
                type: 'POST',
                beforeSubmit: function() {
                    $('#checkout_delivery_address').block({
                        message: ACC.address.spinner
                    });
                },
                success: function(data) {
                    $('#summaryDeliveryAddressFormContainer').html(data);
                    var status = $('.create_update_address_id').attr('status');
                    if (status != null && "success" === status.toLowerCase()) {
                        ACC.refresh.getCheckoutCartDataAndRefreshPage();
                        ACC.colorbox.close();
                    } else {
                        ACC.address.bindCreateUpdateAddressForm();
                        ACC.colorbox.resize();
                    }
                },
                error: function(xht, textStatus, ex) {
                    alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
                },
                complete: function() {
                    $('#checkout_delivery_address').unblock();
                }
            };

            $(this).ajaxForm(options);
        });
    },

    refreshDeliveryAddressSection: function(data) {
        $('.summaryDeliveryAddress').replaceWith($('#deliveryAddressSummaryTemplate').tmpl(data));

    },

    bindSuggestedDeliveryAddresses: function() {
        var status = $('.add_edit_delivery_address_id').attr('status');
        if (status != null && "hasSuggestedAddresses" == status) {
            ACC.address.showSuggestedAddressesPopup();
        }
    },

    showSuggestedAddressesPopup: function() {

        ACC.colorbox.open("", {
            href: "#popup_suggested_delivery_addresses",
            inline: true,
            overlayClose: false,
            width: 525,
        });
    },

    bindCountrySpecificAddressForms: function() {
        $(document).on("change selectmenuchange", '#countrySelector select', function() {
            var options = {
                'addressCode': '',
                'countryIsoCode': $(this).val()
            };
            ACC.address.displayCountrySpecificAddressForm(options, ACC.address.showAddressFormButtonPanel);
        })

    },

    showAddressFormButtonPanel: function()

    {
        wiley.displayCustomSelect();
        ACC.address.validateAddressForm();
        if ($('#countrySelector :input').val() !== '') {
            $('#addressform_button_panel').show();
        }
    },

    showTaxDetailsPanel: function(applicableTaxExemption) {
        if (applicableTaxExemption == 'VAT') {
            $('.vatRegistrationDetailsForm').show();
            $('.vatRegistrationDetailsForm').removeAttr('disabled');

            $('.taxExemptionDetailsForm').hide();
            $('.taxExemptionDetailsForm').attr('disabled', 'disabled');
        } else if (applicableTaxExemption == 'TAX') {
            $('.vatRegistrationDetailsForm').hide();
            $('.vatRegistrationDetailsForm').attr('disabled', 'disabled');

            $('.taxExemptionDetailsForm').show();
            $('.taxExemptionDetailsForm').removeAttr('disabled');
        }
    },

    hideTaxDetailsPanel: function() {
        $('.vatRegistrationDetailsForm').hide();
        $('.taxExemptionDetailsForm').hide();
    },

    resetTaxDetailsPanelFormElements: function() {
        ACC.address.resetFormElements('.vatRegistrationDetailsForm');
        ACC.address.resetFormElements('.taxExemptionDetailsForm');
    },

    resetFormElements: function(element) {
        $(element).find('input:text, input:password, input:file, select, textarea').val('');
        $(element).find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        // clean error messages if any
        $(element).find('.form-group').removeClass('has-error');
        $(element).find('.form-field').removeClass('required');
        $(element).find(".help-block").remove();
    },

    validateAddressForm: function() {
        wiley.helper.validateTaxes();
    },
    toggleSubmitBtnForCheckout: function() {
        var isOrderEditPage = $("#wileyOrderForm").length;
        if (!isOrderEditPage) {
            ACC.address.toggleSubmitBtnBasedOnRequiredFields('#addressForm', '#addressSubmit');
        }
    },
    bindAddressFormToggleSubmitBtnOnChange: function() {
        var addressFormSelector = '#addressForm';
        var btnSubmitSelector = '#addressSubmit, #submitWileyOrderForm';

        $(document).on("keyup change", "#addressForm .form-field input", function() {
            ACC.address.toggleSubmitBtnBasedOnRequiredFields(addressFormSelector, btnSubmitSelector)
        });
        $(document).on("click", "#paymentMethods li", function() {
            ACC.address.toggleSubmitBtnBasedOnRequiredFields(addressFormSelector, btnSubmitSelector)
        });
        $(document).on("selectmenuchange change", "#addressForm .form-field select", function() {
            ACC.address.toggleSubmitBtnBasedOnRequiredFields(addressFormSelector, btnSubmitSelector)
        });
    },
    toggleSubmitBtnBasedOnRequiredFields: function(fieldsParentSelector, btnSubmitSelector) {
        var requiredFields = $(fieldsParentSelector).find('.form-field.required');
        // Do not enable 'Next' button if only CountrySelector is available.
        if (requiredFields.length == 1 && requiredFields[0].id === 'countrySelector') {
            $(btnSubmitSelector).prop("disabled", true);
            return;
        }
        var textInputs = requiredFields.find('input');
        var selectInputs = requiredFields.find('select');
        var textNodesArray = [].slice.call(textInputs);
        var selectNodesArray = [].slice.call(selectInputs);
        var isDisabled = ifEmptyFields(textNodesArray) || ifEmptyFields(selectNodesArray);
        $(btnSubmitSelector).prop("disabled", isDisabled)

        function ifEmptyFields(fields) {
            return fields.some(function(field) {
                return !field.value && !field.disabled;
            })
        }
    },

    bindToColorboxClose: function() {
        $(document).on("click", ".closeColorBox", function() {
            ACC.colorbox.close();
        })
    },

	interactivePatternValidator: function ($element) {
		var previousValue = $element.val();
		var pattern = $element.attr("id") === "address.phone" ? "^([0-9]*)$" : "^([\u0020-\u007E\u00A0-\u00FF]*)$";
		var regex = new RegExp(pattern);

		$element.on('input', function (e) {
			var value = $element.val();
			if (regex.test(value)) {
				previousValue = value;
			}
			else {
				$element.val(previousValue);
			}
		});
	},

	bindInteractivePatternValidators: function () {
		var inputsWithPattern = $('#addressForm').find('input[type="text"]');
		$.each(inputsWithPattern, function (index, element) {
			ACC.address.interactivePatternValidator($(element));
		});
	},

    displayCountrySpecificAddressForm: function(options, callback) {
        $.ajax({
            url: ACC.config.encodedContextPath + '/my-account/addressform',
            async: true,
            data: options,
            dataType: "html",
            beforeSend: function() {
                $("#i18nAddressForm").html(ACC.address.spinner);
                ACC.address.hideTaxDetailsPanel();
                ACC.address.resetTaxDetailsPanelFormElements();
                ACC.address.resetAdditionalBillingInfoFormElements();
            }
        }).done(function(data) {
            $("#i18nAddressForm").html($(data).html());
            var applicableTaxExemption = $(data).data("applicable-tax-exemption");
            $("#i18nAddressForm").data("applicable-tax-exemption", applicableTaxExemption);
            ACC.address.showTaxDetailsPanel(applicableTaxExemption)

            if (typeof callback == 'function') {
                callback.call();
            }

            ACC.address.toggleSubmitBtnBasedOnRequiredFields('#addressForm', '#addressSubmit, #submitWileyOrderForm');
            ACC.address.bindInteractivePatternValidators();
            wiley.disableAutoZoomIOS();
        });
    },

    bindToChangeAddressButton: function() {
        $(document).on("click", '.summaryDeliveryAddress .editButton', ACC.address.handleChangeAddressButtonClick);
    },

    bindViewAddressBook: function() {

        $(document).on("click", ".js-address-book", function(e) {
            e.preventDefault();

            ACC.colorbox.open("Saved Addresses", {
                href: "#addressbook",
                inline: true,
                width: "380px"
            });

        })


        $(document).on("click", '#summaryOverlayViewAddressBook', function() {
            $('#summaryDeliveryAddressFormContainer').hide();
            $('#summaryOverlayViewAddressBook').hide();
            $('#summaryDeliveryAddressBook').show();
            ACC.colorbox.resize();
        });
    },

    showRemoveAddressFromBookConfirmation: function() {
        $(document).on("click", ".removeAddressFromBookButton", function() {
            var addressId = $(this).data("addressId");
            var popupTitle = $(this).data("popupTitle");

            ACC.colorbox.open(popupTitle, {
                inline: true,
                height: false,
                href: "#popup_confirm_address_removal_" + addressId,
                onComplete: function() {

                    $(this).colorbox.resize();
                }
            });

        })
    },

    backToListAddresses: function() {
        $(".addressBackBtn").on("click", function() {
            var sUrl = $(this).data("backToAddresses");
            window.location = sUrl;
        });
    },
    resetAdditionalBillingInfoFormElements: function() {
        ACC.address.resetFormElements('.additionalBillingInfoForm');
    }
};