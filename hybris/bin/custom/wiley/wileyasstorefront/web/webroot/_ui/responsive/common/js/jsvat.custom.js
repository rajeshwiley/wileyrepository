var jsvat = (function () {

	'use strict'

	/** Customization of https://github.com/se-panfilov/jsvat
	 * https://confluence.wiley.com/pages/viewpage.action?spaceKey=ECSC&title=%5BAS%5D+Ability+for+end+user+to+provide+VAT+ID
	 *
	 */


	function Result(vat, isValid, country) {
		this.value = vat || null
		this.isValid = !!isValid

		if (country) {
			this.country = {
				name: country.name,
				isoCode: {
					short: country.codes[0],
					long: country.codes[1],
					numeric: country.codes[2]
				}
			}
		}
	}

	function removeExtraChars(vat) {
		vat = vat || ''
		return vat.toString().toUpperCase().replace(/(\s|-|\.)+/g, '')
	}

	function isValEqToCode(val, codes) {
		return (val === codes[0] || val === codes[1] || val === codes[2])
	}

	function isInList(list, country) {
		if (!list) return false

		for (var i = 0; i < list.length; i++) {
			var val = list[i].toUpperCase()
			if (val === country.name.toUpperCase()) return true
			if (isValEqToCode(val, country.codes)) return true
		}

		return false
	}

	function isBlocked(country, blocked, allowed) {
		var isBlocked = isInList(blocked, country)
		if (isBlocked) return true
		var isAllowed = isInList(allowed, country)
		return allowed.length > 0 && !isAllowed
	}

	function getCountryByIso(isoCode, countries) {
		for (var k in countries) {
			if (countries[k].codes[0] === isoCode) {
				return countries[k]
			}
		}

		return null
	}

	function isVatValidToRegexp(vat, regexArr) {
		for (var i = 0; i < regexArr.length; i++) {
			var regex = regexArr[i]
			var isValid = regex.test(vat)
			if (isValid) return {
				isValid: true,
				regex: regex
			}
		}

		return {
			isValid: false
		}
	}

	function isVatValid(vat, country) {
		var regexpValidRes = isVatValidToRegexp(vat, country.rules.regex)
		return regexpValidRes.isValid
	}

	var exports = {
		blocked: [],
		allowed: [],
		countries: {},
		checkVAT: function (vat, isoCode) {
			if (!vat) throw new Error('VAT should be specified')
			var cleanVAT = removeExtraChars(vat)

			var country = getCountryByIso(isoCode, this.countries)
			if (!country) return new Result(cleanVAT, true)

			if (isBlocked(country, this.blocked, this.allowed)) return new Result(cleanVAT, false, country)

			var isValid = isVatValid(cleanVAT, country)
			return new Result(cleanVAT, isValid, country)
		}
	}


	// eslint-disable-next-line no-undef
	exports.countries.austria = {
		name: 'Austria',
		codes: ['AT', 'AUT', '040'],
		rules: {
			regex: [/^U(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.belgium = {
		name: 'Belgium',
		codes: ['BE', 'BEL', '056'],
		rules: {
			regex: [/^(\w{9})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.bulgaria = {
		name: 'Bulgaria',
		codes: ['BG', 'BGR', '100'],
		rules: {
			regex: [/^(\w{9,10})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.cyprus = {
		name: 'Cyprus',
		codes: ['CY', 'CYP', '196'],
		rules: {
			regex: [/^(\w{8}[A-Z])$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.czech_republic = {
		name: 'Czech Republic',
		codes: ['CZ', 'CZE', '203'],
		rules: {
			regex: [/^(\d{8,10})$/],
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.denmark = {
		name: 'Denmark',
		codes: ['DK', 'DNK', '208'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.estonia = {
		name: 'Estonia',
		codes: ['EE', 'EST', '233'],
		rules: {
			regex: [/^(\w{9})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.finland = {
		name: 'Finland',
		codes: ['FI', 'FIN', '246'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.france = {
		name: 'France',
		codes: ['FR', 'FRA', '250'],
		rules: {
			regex: [
				/^(\d{11})$/,
				/^([A-HJ-NP-Z]\d{10})$/,
				/^(\d[A-HJ-NP-Z]\d{9})$/,
				/^([A-HJ-NP-Z]{2}\d{9})$/
			]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.germany = {
		name: 'Germany',
		codes: ['DE', 'DEU', '276'],
		rules: {
			regex: [/^(\d{9})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.greece = {
		name: 'Greece',
		codes: ['EL', 'GR', 'GRC', '300'],
		rules: {
			regex: [/^(0)(\d{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.hungary = {
		name: 'Hungary',
		codes: ['HU', 'HUN', '348'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.ireland = {
		name: 'Ireland',
		codes: ['IE', 'IRL', '372'],
		rules: {
			regex: [
				/^(\d{7}[A-Z])$/,
				/^(\d{1}[A-Z)]\d{5}[A-Z])$/
			]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.italy = {
		name: 'Italy',
		codes: ['IT', 'ITA', '380'],
		rules: {
			regex: [/^(\w{11})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.latvia = {
		name: 'Latvia',
		codes: ['LV', 'LVA', '428'],
		rules: {
			regex: [/^(\w{11})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.lithuania = {
		name: 'Lithuania',
		codes: ['LT', 'LTU', '440'],
		rules: {
			regex: [/^(\w{9}|\w{12})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.luxembourg = {
		name: 'Luxembourg',
		codes: ['LU', 'LUX', '442'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.malta = {
		name: 'Malta',
		codes: ['MT', 'MLT', '470'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.netherlands = {
		name: 'Netherlands',
		codes: ['NL', 'NLD', '528'],
		rules: {
			regex: [/^(\w{9})B\w{2}$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.poland = {
		name: 'Poland',
		codes: ['PL', 'POL', '616'],
		rules: {
			regex: [/^(\w{10})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.portugal = {
		name: 'Portugal',
		codes: ['PT', 'PRT', '620'],
		rules: {
			regex: [/^(\w{9})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.romania = {
		name: 'Romania',
		codes: ['RO', 'ROU', '642'],
		rules: {
			regex: [/^(\d{2,10})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.spain = {
		name: 'Spain',
		codes: ['ES', 'ESP', '724'],
		rules: {
			regex: [
				/^([A-Z]\d{8})$/,
				/^(\d{8}[A-Z])$/,
				/^([A-Z]\d{7}[A-Z])$/,
			]
		}
	}

	exports.countries.slovakia_republic = {
		name: 'Slovakia',
		codes: ['SK', 'SVK', '703'],
		rules: {
			regex: [/^(\w{10})$/]
		}
	}

	exports.countries.slovenia = {
		name: 'Slovenia',
		codes: ['SI', 'SVN', '705'],
		rules: {
			regex: [/^(\w{8})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.sweden = {
		name: 'Sweden',
		codes: ['SE', 'SWE', '752'],
		rules: {
			regex: [/^(\w{12})$/]
		}
	}

	// eslint-disable-next-line no-undef
	exports.countries.united_kingdom = {
		name: 'United Kingdom',
		codes: ['GB', 'GBR', '826'],
		rules: {
			regex: [
				/^(\w{9})$/,
				/^(\w{12})$/,
				/^(\w{5})$/,
			]
		}
	}

	exports.countries.norway = {
		name: 'Norway',
		codes: ['NO', 'NOR', '578'],
		rules: {
			regex: [/^(\w{9})$/]
		}
	}


	exports.countries.south_africa = {
		name: 'South Africa',
		codes: ['ZA'],
		rules: {
			regex: [/^4(\w{9})$/]
		}
	}


	//Support of node.js

	if (typeof module === 'object' && module.exports) module.exports = exports

	return exports

})()