package com.wiley.wiremock.transromers.tax;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.http.HttpStatus;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wiley.wiremock.transromers.common.dto.ErrorWmDto;
import com.wiley.wiremock.transromers.tax.dto.TaxCalculationRequestWmDto;
import com.wiley.wiremock.transromers.tax.dto.TaxCalculationRequestEntryWmDto;
import com.wiley.wiremock.transromers.tax.dto.TaxCalculationResponseWmDto;
import com.wiley.wiremock.transromers.tax.dto.TaxCalculationResponseEntryWmDto;
import com.wiley.wiremock.transromers.tax.dto.TaxValueWmDto;

import static java.util.Arrays.asList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


public class TaxResponseTransformer extends ResponseTransformer
{
	private static final BigDecimal TAX_PERCENTAGE = BigDecimal.valueOf(0.18);
	private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

	private static final String PARSING_ERROR_CODE = "err001";

	private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		final Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
		final String bodyAsString = request.getBodyAsString();

		final TaxCalculationRequestWmDto taxCalculationRequest = gson.fromJson(bodyAsString, TaxCalculationRequestWmDto.class);
		final Set<ConstraintViolation<TaxCalculationRequestWmDto>> violations = validator.validate(taxCalculationRequest);
		if (isNotEmpty(violations))
		{

			return Response.Builder
					.like(response)
					.body(gson.toJson(createErrorResponse(violations)))
					.status(HttpStatus.BAD_REQUEST.value())
					.build();
		}
		final TaxCalculationResponseWmDto taxCalculationResponse = createTaxCalculationResponse(taxCalculationRequest);
		return Response.Builder
				.like(response)
				.but()
				.body(gson.toJson(taxCalculationResponse))
				.build();
	}

	private ErrorWmDto createErrorResponse(final Set<ConstraintViolation<TaxCalculationRequestWmDto>> violations)
	{
		final ErrorWmDto errorResponse = new ErrorWmDto();
		final String errorMessage = violations
				.stream()
				.map(violation -> String.format("%s:%s", violation.getPropertyPath(), violation.getMessage()))
				.collect(Collectors.joining(", "));
		
		errorResponse.setCode(PARSING_ERROR_CODE);
		errorResponse.setMessage(errorMessage);
		return errorResponse;
	}

	private TaxCalculationResponseWmDto createTaxCalculationResponse(final TaxCalculationRequestWmDto taxCalculationRequest)
	{
		final TaxCalculationResponseWmDto taxCalculationResponse = new TaxCalculationResponseWmDto();
		taxCalculationResponse.setHandlingTaxes(asList(createTaxValue("handlingTax", taxCalculationRequest.getHandlingCost())));
		taxCalculationResponse.setShippingTaxes(asList(createTaxValue("shippingTax", taxCalculationRequest.getShippingCost())));
		taxCalculationResponse.setEntries(createTaxCalculationResponseEntries(taxCalculationRequest.getEntries()));
		return taxCalculationResponse;
	}

	private List<TaxCalculationResponseEntryWmDto> createTaxCalculationResponseEntries(final List<TaxCalculationRequestEntryWmDto> entries)
	{
		return entries
				.stream()
				.map(this::createTaxCalculationResponseEntry)
				.collect(Collectors.toList());
	}

	private TaxCalculationResponseEntryWmDto createTaxCalculationResponseEntry(
			final TaxCalculationRequestEntryWmDto taxCalculationRequestEntry)
	{
		final TaxCalculationResponseEntryWmDto taxCalculationResponseEntry = new TaxCalculationResponseEntryWmDto();
		taxCalculationResponseEntry.setId(taxCalculationRequestEntry.getId());
		taxCalculationResponseEntry.setTaxes(asList(createTaxValue("entryTax", taxCalculationRequestEntry.getAmount())));
		return taxCalculationResponseEntry;
	}

	private TaxValueWmDto createTaxValue(final String code, final BigDecimal baseValue)
	{
		final TaxValueWmDto taxValue = new TaxValueWmDto();
		taxValue.setCode(code);
		taxValue.setValue(calculateTaxValue(baseValue));
		return taxValue;
	}

	private BigDecimal calculateTaxValue(final BigDecimal baseValue)
	{
		return baseValue.multiply(TAX_PERCENTAGE)
				.setScale(2, RoundingMode.HALF_UP);
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "tax-response-transformer";
	}
}
