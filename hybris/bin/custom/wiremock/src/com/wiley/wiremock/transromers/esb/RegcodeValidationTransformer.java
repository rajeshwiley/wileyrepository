package com.wiley.wiremock.transromers.esb;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;


/**
 * In success case the following rule is applied:
 * 1) The syntax od regcode is rc-[isbn]
 * 2) When rc- is missed or misspelled then it returns errorCode and message
 * 3) When isbn ends with '0' the regCode is burnt
 * 4) [isbn] returned as is except some cases see next rule
 * 5) rc-400, rc-401, rc-404, rc-500, rc-503 return cooresponsing http-status instead
 */
public class RegcodeValidationTransformer extends ResponseTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(RegcodeValidationTransformer.class);

	private static final String REGCODES = "regcodes";
	private static final String VALID_REG_CODE_PREFIX = "rc-";

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		LOG.debug("Starting transforming response for request [{}].", request);

		// response which will be sent back
		final JSONObject responseBody = createResponseBodyForRegCode(parseRegcodeFromRequest(request));

		return Response.Builder.like(response)
				.but().body(responseBody.toString()).build();
	}

	private JSONObject createResponseBodyForRegCode(final String regCode)
	{
		try
		{
			final JSONObject responseBody = new JSONObject();

			if (!regCode.startsWith(VALID_REG_CODE_PREFIX))
			{
				responseBody.put("code", regCode);
				responseBody.put("message", "Something went wrong");
			}
			else
			{
				responseBody.put("code", regCode);
				responseBody.put("isbn", regCode.substring(VALID_REG_CODE_PREFIX.length()));
				responseBody.put("isBurned", regCode.endsWith("0"));
			}
			return responseBody;
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying to build response.", e);
		}
	}

	private String parseRegcodeFromRequest(final Request request)
	{
		final String url = request.getUrl();
		if (StringUtils.isNotEmpty(url) && url.indexOf(REGCODES) >= 0)
		{
			return url.substring(url.indexOf(REGCODES) + REGCODES.length() + 1);
		}
		else
		{
			throw new RuntimeException("Empty URL for regCode validation");
		}
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "regcode-validation-transformer";
	}
}
