package com.wiley.wiremock.transromers.tax.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class TaxCalculationRequestEntryWmDto
{
	@NotNull
	@Size(max = 255)
	private String id;

	@NotNull
	@DecimalMin("0.0")
	private BigDecimal amount;

	@NotNull
	@Size(max = 255)
	private String codeType;

	@NotNull
	@Size(max = 255)
	private String code;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(final BigDecimal amount)
	{
		this.amount = amount;
	}

	public String getCodeType()
	{
		return codeType;
	}

	public void setCodeType(final String codeType)
	{
		this.codeType = codeType;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}
}

