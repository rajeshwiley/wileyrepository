package com.wiley.wiremock.constants;

@SuppressWarnings("PMD")
public class WiremockConstants extends GeneratedWiremockConstants
{
	public static final String EXTENSIONNAME = "wiremock";

	public static final String REFERRER_PATH_PARAM = "referrer-path";
	public static final String URL_PATH_PARAM = "url-path";
	public static final String REFERRER_HEADER_NAME = "Referer";
	public static final String ORIGIN_HEADER_NAME = "Origin";
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME = "Access-Control-Allow-Origin";
	public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER_NAME = "Access-Control-Allow-Credentials";
	public static final String P3P_HEADER_NAME = "P3P";
	public static final String P3P_HEADER_VALUE = "Wiley does not have a P3P policy";

	private WiremockConstants()
	{
		//empty
	}
	
	
}
