package com.wiley.wiremock.transromers;

import org.apache.commons.lang.StringUtils;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;

import static com.wiley.wiremock.constants.WiremockConstants.ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.ORIGIN_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.P3P_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.P3P_HEADER_VALUE;


/**
 * Created by Maksim_Kozich on 04.05.17.
 */
public class CorsHeaderTransfomer extends ResponseTransformer
{

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		HttpHeader accessControlAllowOriginHeader = new HttpHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME,
				StringUtils.defaultIfEmpty(request.getHeader(ORIGIN_HEADER_NAME), ""));
		HttpHeader accessControlAllowCredentialsHeader = new HttpHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS_HEADER_NAME,
				Boolean.TRUE.toString());
		HttpHeader p3pHeader = new HttpHeader(P3P_HEADER_NAME, P3P_HEADER_VALUE);
		HttpHeaders updatedHeaders = response.getHeaders().plus(
				accessControlAllowOriginHeader,
				accessControlAllowCredentialsHeader,
				p3pHeader);
		return Response.Builder.like(response)
				.but().headers(updatedHeaders).build();
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "cors-header-transfomer";
	}
}
