package com.wiley.wiremock.transromers.esb.verifyandcalculatecart;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Generic transformer for common cases.<br/>
 * The main reason of this transformer to unblock other test cases related to cart calculation.
 */
public class CartCalculationTransformer extends ResponseTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(CartCalculationTransformer.class);

	private static final double baseProductPrice = 200.55;

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		LOG.debug("Starting transforming response for request [{}].", request);

		// parsing request to generate response
		JSONObject requestBody = getJsonObject(request.getBodyAsString());

		// response which will be sent back
		final JSONObject responseBody = getResponseBody(requestBody);

		final Response newResponse = Response.Builder
				.like(response).but()
				.body(responseBody.toString())
				.build();

		LOG.debug("Ended transforming response for request [{}]. Sending response [{}]", request, newResponse);
		return newResponse;
	}

	private JSONObject getResponseBody(final JSONObject requestBody)
	{
		try
		{
			final JSONObject responseBody = new JSONObject();

			final JSONArray entriesFromRequest = requestBody.optJSONArray("entries");
			final JSONArray entriesForResponse = getEntriesForResponse(entriesFromRequest);

			// Calculating total prices
			int quantityOfProducts = getQuantityOfExistingProducts(entriesForResponse);
			final double subTotal = baseProductPrice * quantityOfProducts;
			final double totalPrice = subTotal;

			responseBody.put("entries", entriesForResponse);
			responseBody.put("subtotal", subTotal);
			responseBody.put("totalDiscount", 0);
			responseBody.put("deliveryCost", 0);
			responseBody.put("totalTax", 0);
			responseBody.put("totalPrice", totalPrice);

			return responseBody;
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying to build response.", e);
		}
	}

	private JSONArray getEntriesForResponse(final JSONArray entriesFromRequest) throws JSONException
	{
		final JSONArray entriesForResponse = new JSONArray();

		if (entriesFromRequest != null)
		{
			for (int i = 0; i < entriesFromRequest.length(); i++)
			{
				final JSONObject entryFromRequest = entriesFromRequest.getJSONObject(i);
				final String isbn = entryFromRequest.getString("isbn");

				final JSONObject entryForResponse = new JSONObject();

				entryForResponse.put("isbn", isbn);

				LOG.debug("Updating or just keep unmodified product [isbn:{}].", isbn);
				entryForResponse.put("quantity", entryFromRequest.get("quantity"));

				final double baseEntryPrice = baseProductPrice;
				final int quantity = entryForResponse.getInt("quantity");
				final double totalEntryPrice = baseEntryPrice * quantity;

				entryForResponse.put("basePrice", baseEntryPrice);
				entryForResponse.put("totalPrice", totalEntryPrice);
				LOG.debug("Added base price [{}] and total price [{}] for product [isbn:{}].", baseEntryPrice, totalEntryPrice,
						isbn);

				JSONArray inventoryStatuses = createDefaultInventoryStatuses(quantity);
				entryForResponse.put("inventoryStatuses", inventoryStatuses);

				entriesForResponse.put(entryForResponse);
			}
		}
		return entriesForResponse;
	}

	private JSONArray createDefaultInventoryStatuses(final int quantity) throws JSONException
	{
		JSONArray inventoryStatuses = new JSONArray();
		JSONObject inventoryStatus = createDefaultInventoryStatus(quantity);
		inventoryStatuses.put(inventoryStatus);
		return inventoryStatuses;
	}

	private JSONObject createDefaultInventoryStatus(final int quantity) throws JSONException
	{
		JSONObject inventoryStatus = new JSONObject();
		inventoryStatus.put("quantity", quantity);
		inventoryStatus.put("status", "IN_STOCK");
		inventoryStatus.put("estimatedDeliveryDate", "2017-07-04T12:08:56.235-07:00");
		return inventoryStatus;
	}

	private int getQuantityOfExistingProducts(final JSONArray entries) throws JSONException
	{
		int quantity = 0;
		for (int i = 0; i < entries.length(); i++)
		{
			final JSONObject entry = entries.getJSONObject(i);
			quantity += entry.getInt("quantity");
		}
		return quantity;
	}

	private JSONObject getJsonObject(final String body)
	{
		try
		{
			return new JSONObject(body);
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying parse response definition body.", e);
		}
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "common-cart-calculation-transformer";
	}
}
