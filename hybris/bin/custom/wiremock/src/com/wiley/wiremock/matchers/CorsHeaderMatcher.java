package com.wiley.wiremock.matchers;

import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.RequestMatcherExtension;
import com.github.tomakehurst.wiremock.matching.UrlPattern;

import static com.wiley.wiremock.constants.WiremockConstants.ORIGIN_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.REFERRER_HEADER_NAME;
import static com.wiley.wiremock.constants.WiremockConstants.REFERRER_PATH_PARAM;
import static com.wiley.wiremock.constants.WiremockConstants.URL_PATH_PARAM;


/**
 * Created by Maksim_Kozich on 04.05.17.
 */
public class CorsHeaderMatcher extends RequestMatcherExtension
{
	private final String referrerUrl;

	public CorsHeaderMatcher(final String referrerUrl)
	{
		this.referrerUrl = referrerUrl;
	}

	@Override
	public MatchResult match(final Request request, final Parameters parameters)
	{
		boolean result;


		String urlPathParam = parameters.getString(URL_PATH_PARAM);
		UrlPattern pattern = UrlPattern.fromOneOf(null, null, urlPathParam, null);

		result = pattern.match(request.getUrl()).isExactMatch();

		if (result)
		{
			String originHeader = request.getHeader(ORIGIN_HEADER_NAME);
			result = referrerUrl.equals(originHeader);

			String referrerPathParam = parameters.getString(REFERRER_PATH_PARAM);
			if (result && referrerPathParam != null)
			{
				String expectedReferrerHeader = referrerUrl.concat(referrerPathParam);
				String referrerHeader = request.getHeader(REFERRER_HEADER_NAME);
				result = expectedReferrerHeader.equals(referrerHeader);
			}
		}


		return MatchResult.of(result);
	}

	@Override
	public String getName()
	{
		return "cors-header-matcher";
	}
}
