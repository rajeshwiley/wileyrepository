package com.wiley.wiremock.transromers.address;

import org.apache.commons.lang3.StringUtils;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


public class AddressResponseSuccessTransformer extends ResponseTransformer
{
	private static final String ADDRESS_LINE_1 = "ADDRESS_LINE_1";
	private static final String ADDRESS_LINE_2 = "ADDRESS_LINE_2";
	private static final String CITY = "CITY";
	private static final String REGION = "REGION";
	private static final String POSTAL_CODE = "POSTAL_CODE";
	private static final String COUNTRY = "COUNTRY";
	private static final String Q_3 = "Q3";

	private static final String REGION_CD = "REGION_CD";
	private static final String COUNTRY_ISO2 = "COUNTRY_ISO2";
	private static final String AD_MATCH_CODE = "AD_MATCH_CODE";

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		JsonArray jsonData = getJsonData(request, response);

		Gson gson = new Gson();
		String responseBody = gson.toJson(jsonData);
		return Response.Builder
				.like(response)
				.but()
				.body(responseBody)
				.build();
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "address-response-suggest";
	}

	private JsonArray getJsonData(final Request request, final Response response)
	{
		JsonArray result = new JsonArray();

		JsonObject jsonFromRequest = getJsonDataFromRequest(request);
		result.add(jsonFromRequest);
		JsonArray additionalDataFromResponse = getAdditionalDataFromResponse(response);
		result.addAll(additionalDataFromResponse);

		return result;
	}

	/*
	Method to extract data from previous response transformer
	 */
	private JsonArray getAdditionalDataFromResponse(Response response)
	{
		JsonArray result = new JsonArray();

		String responseBodyAsString = response.getBodyAsString();
		if (StringUtils.isNotEmpty(responseBodyAsString))
		{
			JsonArray jsonFromResponse = getJson(responseBodyAsString).getAsJsonArray();
			return jsonFromResponse;
		}
		return result;
	}

	private JsonObject getJsonDataFromRequest(Request request)
	{
		String requestBodyAsString = request.getBodyAsString();
		JsonObject jsonFromRequest = getJson(requestBodyAsString).getAsJsonObject();

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty(ADDRESS_LINE_1, getAddressLine1(jsonFromRequest));
		jsonObject.addProperty(ADDRESS_LINE_2, getAddressLine2(jsonFromRequest));
		jsonObject.addProperty(CITY, getCity(jsonFromRequest));
		jsonObject.addProperty(POSTAL_CODE, getPostalCode(jsonFromRequest));
		jsonObject.addProperty(COUNTRY_ISO2, getCountry(jsonFromRequest));
		jsonObject.addProperty(AD_MATCH_CODE, Q_3);

		String region = getRegion(jsonFromRequest);
		if (StringUtils.isNotEmpty(region))
		{
			jsonObject.addProperty(REGION_CD, region);
		}

		return jsonObject;
	}

	private JsonElement getJson(final String jsonString)
	{
		JsonParser parser = new JsonParser();
		JsonElement json = parser.parse(jsonString);
		return json;
	}

	private String getAddressLine1(final JsonObject json)
	{
		return getNotNullValue(json.get(ADDRESS_LINE_1));
	}

	private String getAddressLine2(final JsonObject json)
	{
		return getNotNullValue(json.get(ADDRESS_LINE_2));
	}

	private String getCity(final JsonObject json)
	{
		return getNotNullValue(json.get(CITY));
	}

	private String getRegion(final JsonObject json)
	{
		return getNotNullValue(json.get(REGION));
	}

	private String getPostalCode(final JsonObject json)
	{
		return getNotNullValue(json.get(POSTAL_CODE));
	}

	private String getCountry(final JsonObject json)
	{
		return getNotNullValue(json.get(COUNTRY));
	}

	private String getNotNullValue(final JsonElement value)
	{
		return value != null ? value.getAsString() : StringUtils.EMPTY;
	}
}
