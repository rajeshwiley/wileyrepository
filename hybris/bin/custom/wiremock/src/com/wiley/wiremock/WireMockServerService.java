package com.wiley.wiremock;

import de.hybris.platform.util.Utilities;

import java.io.File;

import org.apache.log4j.Logger;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.wiley.wiremock.common.WileySlf4jNotifier;
import com.wiley.wiremock.matchers.CorsHeaderMatcher;
import com.wiley.wiremock.transromers.CorsHeaderTransfomer;
import com.wiley.wiremock.transromers.address.AddressResponseSuccessTransformer;
import com.wiley.wiremock.transromers.esb.AddressIDResponseTransformer;
import com.wiley.wiremock.transromers.esb.RegcodeValidationTransformer;
import com.wiley.wiremock.transromers.esb.inventorycheck.InventoryCheckTransformer;
import com.wiley.wiremock.transromers.esb.verifyandcalculatecart.CartCalculationTransformer;
import com.wiley.wiremock.transromers.tax.TaxResponseTransformer;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;


/**
 * Service that start up WireMock(server for mocking external systems) on init Spring content and shutdown on destroy
 *
 * @author Dzmitryi_Halahayeu
 */
public class WireMockServerService
{
	private static final String EXTENSION_NAME = "wiremock";
	private static final String RESOURCES_FOLDER = "resources";
	private static final String FILES_FOLDER = RESOURCES_FOLDER + "/__files";
	private static final String MAPPINGS_FOLDER = RESOURCES_FOLDER + "/mappings";
	private static final boolean NOTIFIER_VERBOSE_MODE_TRUE = true;
	private int port;
	private int httpsPort;
	private String httpsKeystorePath;
	private String httpsKeystorePassword;
	private String referrerUrl;
	private boolean startInEmbeddedMode;
	private static final Logger LOG = Logger.getLogger(WireMockServerService.class);
	private WireMockServer wireMockServer;


	public void startServer()
	{
		if (startInEmbeddedMode)
		{
			LOG.info("Starting WireMock server...");
			wireMockServer = configureWireMock();
			wireMockServer.start();
		}
	}

	public void stopServer()
	{
		if (startInEmbeddedMode && wireMockServer != null)
		{
			LOG.info("Stopping WireMock server...");
			wireMockServer.shutdown();
			awaitTermination();
			LOG.info("WireMock server stopped");
		}
	}

	private void awaitTermination()
	{
		long timeout = System.currentTimeMillis() + 3000L;

		while (wireMockServer.isRunning())
		{
			try
			{
				Thread.currentThread();
				Thread.sleep(100L);
			}
			catch (InterruptedException var4)
			{
				Thread.currentThread().interrupt();
			}

			if (System.currentTimeMillis() > timeout)
			{
				throw new RuntimeException("Server took too long to stop.");
			}
		}
	}

	private WireMockServer configureWireMock()
	{
		return new WireMockServer(
				wireMockConfig()
						.notifier(new WileySlf4jNotifier(NOTIFIER_VERBOSE_MODE_TRUE))
						.port(port)
						.httpsPort(httpsPort)
						.keystorePath(
								new File(Utilities.getPlatformConfig().getExtensionInfo(EXTENSION_NAME).getExtensionDirectory(),
										httpsKeystorePath).getAbsolutePath())
						.keystorePassword(httpsKeystorePassword)
						.withRootDirectory(
								new File(Utilities.getPlatformConfig().getExtensionInfo(EXTENSION_NAME).getExtensionDirectory(),
										RESOURCES_FOLDER).getAbsolutePath())
						.extensions(new CartCalculationTransformer(), new AddressIDResponseTransformer(),
								new InventoryCheckTransformer(), new RegcodeValidationTransformer(), new CorsHeaderTransfomer(),
								new CorsHeaderMatcher(referrerUrl), new ResponseTemplateTransformer(false),
								new TaxResponseTransformer(), new AddressResponseSuccessTransformer())
						.containerThreads(80) //for PERF needs
						.jettyAcceptQueueSize(300) //for PERF needs
		);
	}

	public int getPort()
	{
		return port;
	}

	public void setPort(final int port)
	{
		this.port = port;
	}

	public int getHttpsPort()
	{
		return httpsPort;
	}

	public void setHttpsPort(final int httpsPort)
	{
		this.httpsPort = httpsPort;
	}

	public String getHttpsKeystorePath()
	{
		return httpsKeystorePath;
	}

	public void setHttpsKeystorePath(final String httpsKeystorePath)
	{
		this.httpsKeystorePath = httpsKeystorePath;
	}

	public String getHttpsKeystorePassword()
	{
		return httpsKeystorePassword;
	}

	public void setHttpsKeystorePassword(final String httpsKeystorePassword)
	{
		this.httpsKeystorePassword = httpsKeystorePassword;
	}

	public String getReferrerUrl()
	{
		return referrerUrl;
	}

	public void setReferrerUrl(final String referrerUrl)
	{
		this.referrerUrl = referrerUrl;
	}

	public boolean isStartInEmbeddedMode()
	{
		return startInEmbeddedMode;
	}

	public void setStartInEmbeddedMode(final boolean startInEmbeddedMode)
	{
		this.startInEmbeddedMode = startInEmbeddedMode;
	}

	public static void main(final String[] args)
	{
		WireMockServerService wireMockServerService = new WireMockServerService();
		wireMockServerService.setStartInEmbeddedMode(true);
		wireMockServerService.port = 3700;
		wireMockServerService.httpsPort = 3701;
		wireMockServerService.httpsKeystorePath = "resources/wileywiremock.jks";
		wireMockServerService.httpsKeystorePassword = "wileywiremock";
		wireMockServerService.referrerUrl = "https://localhost:9002";
		wireMockServerService.startServer();
	}
}
