package com.wiley.wiremock.transromers.tax.dto;

import java.util.List;


public class TaxCalculationResponseEntryWmDto
{
	private String id;
	private List<TaxValueWmDto> taxes;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public List<TaxValueWmDto> getTaxes()
	{
		return taxes;
	}

	public void setTaxes(final List<TaxValueWmDto> taxes)
	{
		this.taxes = taxes;
	}
}

