package com.wiley.wiremock.transromers.common.dto;

public class ErrorWmDto
{
	private String code;
	private String message;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}
}
