package com.wiley.wiremock.common;

import com.github.tomakehurst.wiremock.common.Slf4jNotifier;


/**
 * Created by Maksim_Kozich on 26.05.17.
 */
public class WileySlf4jNotifier extends Slf4jNotifier
{

	public static final String REQUEST_WAS_NOT_MATCHED = "Request was not matched:";

	public WileySlf4jNotifier(final boolean verbose)
	{
		super(verbose);
	}

	@Override
	public void error(final String message)
	{
		if (isRequestMatchMessage(message))
		{
			superInfo(message);
		}
		else
		{
			superError(message);
		}
	}

	@Override
	public void error(final String message, final Throwable t)
	{
		if (isRequestMatchMessage(message))
		{
			superInfo(message);
		}
		else
		{
			superError(message, t);
		}
	}

	protected void superInfo(final String message)
	{
		super.info(message);
	}

	protected void superError(final String message)
	{
		super.error(message);
	}

	protected void superError(final String message, final Throwable t)
	{
		super.error(message, t);
	}

	private boolean isRequestMatchMessage(final String message)
	{
		return message.startsWith(REQUEST_WAS_NOT_MATCHED);
	}
}
