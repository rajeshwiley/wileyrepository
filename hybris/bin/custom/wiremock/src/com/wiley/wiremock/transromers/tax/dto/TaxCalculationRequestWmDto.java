package com.wiley.wiremock.transromers.tax.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class TaxCalculationRequestWmDto
{
	@NotNull
	@Size(max = 255)
	private String siteId;

	@NotNull
	private Date date;

	@NotNull
	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;

	@NotNull
	@Valid
	private TaxAddressWmDto paymentAddress;

	@Valid
	private TaxAddressWmDto deliveryAddress;

	@NotNull
	@Valid
	private List<TaxCalculationRequestEntryWmDto> entries;

	@NotNull
	@DecimalMin("0.0")
	private BigDecimal handlingCost;

	@NotNull
	@DecimalMin("0.0")
	private BigDecimal shippingCost;

	public String getSiteId()
	{
		return siteId;
	}

	public void setSiteId(final String siteId)
	{
		this.siteId = siteId;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public TaxAddressWmDto getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final TaxAddressWmDto paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	public TaxAddressWmDto getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final TaxAddressWmDto deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	public List<TaxCalculationRequestEntryWmDto> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationRequestEntryWmDto> entries)
	{
		this.entries = entries;
	}

	public BigDecimal getHandlingCost()
	{
		return handlingCost;
	}

	public void setHandlingCost(final BigDecimal handlingCost)
	{
		this.handlingCost = handlingCost;
	}

	public BigDecimal getShippingCost()
	{
		return shippingCost;
	}

	public void setShippingCost(final BigDecimal shippingCost)
	{
		this.shippingCost = shippingCost;
	}
}

