package com.wiley.wiremock.transromers.esb.inventorycheck;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;
import com.wiley.wiremock.transromers.utils.InventoryTransformerUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by Mikhail_Asadchy on 8/1/2016.
 */
public class InventoryCheckTransformer extends ResponseTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(InventoryCheckTransformer.class);

	@Override
	public Response transform(final Request request, final Response responseDefinition, final FileSource fileSource,
			final Parameters parameters)
	{
		LOG.debug("Starting transforming response for request [{}].", request);

		// parsing request to generate response
		JSONObject requestBody = InventoryTransformerUtil.getJsonObject(request.getBodyAsString());

		// response which will be sent back
		final JSONObject responseBody = getResponseBody(requestBody);

		return Response.Builder.like(responseDefinition)
				.but().body(responseBody.toString()).build();
	}

	private JSONObject getResponseBody(final JSONObject requestBody)
	{
		try
		{
			final JSONObject responseBody = new JSONObject();

			final JSONArray entriesFromRequest = requestBody.optJSONArray("entries");
			final String deliveryOptionCode = requestBody.getString("deliveryOptionCode");
			final JSONArray entriesForResponse = convertRequestToResponse(entriesFromRequest, deliveryOptionCode);

			responseBody.put("entries", entriesForResponse);

			return responseBody;
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying to build response.", e);
		}
	}

	private JSONArray convertRequestToResponse(final JSONArray entriesFromRequest, String deliveryOptionCode) throws JSONException
	{
		final JSONArray entriesForResponse = new JSONArray();

		if (entriesFromRequest != null)
		{
			for (int i = 0; i < entriesFromRequest.length(); i++)
			{
				final JSONObject entryFromRequest = entriesFromRequest.getJSONObject(i);
				final String sapProductCode = entryFromRequest.getString("sapProductCode");
				final String isbn = entryFromRequest.getString("isbn");
				final int quantity = entryFromRequest.getInt("quantity");

				final JSONObject entryForResponse = new JSONObject();

				JSONArray inventoryStatuses = InventoryTransformerUtil.createInventoryStatuses(quantity, deliveryOptionCode);
				entryForResponse.put("sapProductCode", sapProductCode);
				entryForResponse.put("isbn", isbn);
				entryForResponse.put("statuses", inventoryStatuses);

				entriesForResponse.put(entryForResponse);
			}
		}
		return entriesForResponse;
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "inventory-check-transformer";
	}
}
