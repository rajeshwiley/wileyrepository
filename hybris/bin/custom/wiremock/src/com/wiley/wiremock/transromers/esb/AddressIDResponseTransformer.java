package com.wiley.wiremock.transromers.esb;

import java.util.UUID;

import com.github.tomakehurst.wiremock.common.FileSource;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.extension.ResponseTransformer;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.http.Response;


public class AddressIDResponseTransformer extends ResponseTransformer
{

	private static final String ADDDRESS_ID_FORMAT = "{\"addressId\":\"%s\"}";

	@Override
	public Response transform(final Request request, final Response response, final FileSource fileSource,
			final Parameters parameters)
	{
		return Response.Builder.like(response)
				.but().body(generateAddressIdBody(response.getBodyAsString())).build();
	}

	private String generateAddressIdBody(final String body)
	{
		return (body != null && body.contains("addressId")) ? body : String.format(ADDDRESS_ID_FORMAT,
				UUID.randomUUID().toString());
	}

	@Override
	public boolean applyGlobally()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "address-id-response-transformer";
	}
}
