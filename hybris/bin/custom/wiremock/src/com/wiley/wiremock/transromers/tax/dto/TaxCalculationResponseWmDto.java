package com.wiley.wiremock.transromers.tax.dto;

import java.util.List;


public class TaxCalculationResponseWmDto
{
	private List<TaxCalculationResponseEntryWmDto> entries;
	private List<TaxValueWmDto> handlingTaxes;
	private List<TaxValueWmDto> shippingTaxes;

	public List<TaxCalculationResponseEntryWmDto> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationResponseEntryWmDto> entries)
	{
		this.entries = entries;
	}

	public List<TaxValueWmDto> getHandlingTaxes()
	{
		return handlingTaxes;
	}

	public void setHandlingTaxes(final List<TaxValueWmDto> handlingTaxes)
	{
		this.handlingTaxes = handlingTaxes;
	}

	public List<TaxValueWmDto> getShippingTaxes()
	{
		return shippingTaxes;
	}

	public void setShippingTaxes(final List<TaxValueWmDto> shippingTaxes)
	{
		this.shippingTaxes = shippingTaxes;
	}
}

