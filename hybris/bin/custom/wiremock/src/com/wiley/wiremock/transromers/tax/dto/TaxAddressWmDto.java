package com.wiley.wiremock.transromers.tax.dto;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class TaxAddressWmDto
{
	@Size(max = 255)
	private String postcode;

	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;

	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String state;

	@Size(max = 255)
	private String city;

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}
}

