package com.wiley.wiremock.transromers.tax.dto;

import java.math.BigDecimal;


public class TaxValueWmDto
{
	private String code;
	private BigDecimal value;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public BigDecimal getValue()
	{
		return value;
	}

	public void setValue(final BigDecimal value)
	{
		this.value = value;
	}
}

