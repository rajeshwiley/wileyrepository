package com.wiley.wiremock.jalo;

import com.wiley.wiremock.constants.WiremockConstants;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import org.apache.log4j.Logger;

@SuppressWarnings("PMD")
public class WiremockManager extends GeneratedWiremockManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger( WiremockManager.class.getName() );
	
	public static final WiremockManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WiremockManager) em.getExtension(WiremockConstants.EXTENSIONNAME);
	}
	
}
