package com.wiley.wiremock.transromers.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Mikhail_Asadchy on 8/1/2016.
 */
public class InventoryTransformerUtil
{

	private static Map<String, String> deliveryOptionCodeToDeliveryDateMap;

	static {
		Map<String, String> map = new HashMap<>();

		map.put("A3", "2017-02-04T12:08:56.235-07:00");
		map.put("A4", "2017-02-05T12:08:56.235-07:00");
		map.put("S",  "2017-02-06T12:08:56.235-07:00");
		map.put("A5", "2017-02-07T12:08:56.235-07:00");
		map.put("A1", "2017-02-08T12:08:56.235-07:00");
		map.put("IN", "2017-02-09T12:08:56.235-07:00");
		map.put("IX", "2017-02-10T12:08:56.235-07:00");
		map.put("IS", "2017-02-11T12:08:56.235-07:00");
		map.put("SD", "2017-02-12T12:08:56.235-07:00");
		map.put("FF", "2017-02-13T12:08:56.235-07:00");
		map.put("1E", "2017-02-14T12:08:56.235-07:00");
		map.put("GS", "2017-02-15T12:08:56.235-07:00");
		map.put("G1", "2017-02-16T12:08:56.235-07:00");
		map.put("G2", "2017-02-17T12:08:56.235-07:00");

		deliveryOptionCodeToDeliveryDateMap = Collections.unmodifiableMap(map);
	}

	public static JSONArray createInventoryStatuses(final int quantity, String deliveryOptionCode) throws JSONException
	{
		JSONArray inventoryStatuses = new JSONArray();

		final int perStatusQuantity = quantity / 2;
		if (quantity == 10) {

			JSONObject backOrder = createInventoryStatus(perStatusQuantity, "BACK_ORDER", "2017-08-04T12:08:56.235-07:00");
			inventoryStatuses.put(backOrder);

			JSONObject inStock = createInventoryStatus(perStatusQuantity, "IN_STOCK", "2017-06-04T12:08:56.235-07:00");
			inventoryStatuses.put(inStock);
		} else if (quantity == 30) {
			JSONObject preOrder = createInventoryStatus(perStatusQuantity, "PRE_ORDER", "2017-09-04T12:08:56.235-07:00");
			inventoryStatuses.put(preOrder);

			JSONObject inStock = createInventoryStatus(perStatusQuantity, "IN_STOCK", "2017-07-14T12:08:56.235-07:00");
			inventoryStatuses.put(inStock);
		} else if (quantity == 50) {
			JSONObject printOnDemand = createInventoryStatus(perStatusQuantity, "PRINT_ON_DEMAND",
					"2017-10-04T12:08:56.235-07:00");
			inventoryStatuses.put(printOnDemand);

			JSONObject inStock = createInventoryStatus(perStatusQuantity, "IN_STOCK", "2017-03-14T12:08:56.235-07:00");
			inventoryStatuses.put(inStock);
		} else {
			JSONObject inStock = createInventoryStatus(quantity, "IN_STOCK", deliveryOptionCodeToDeliveryDateMap.get(deliveryOptionCode));
			inventoryStatuses.put(inStock);
		}

		return inventoryStatuses;
	}

	private static JSONObject createInventoryStatus(final int quantity, final String status, final String deliveryDate)
	{
		JSONObject inventoryStatus = new JSONObject();
		inventoryStatus.put("quantity", quantity);
		inventoryStatus.put("status", status);
		inventoryStatus.put("estimatedDeliveryDate", deliveryDate);
		return inventoryStatus;
	}

	public static JSONObject getJsonObject(final String body)
	{
		try
		{
			return new JSONObject(body);
		}
		catch (JSONException e)
		{
			throw new RuntimeException("Error occurred when trying parse response definition body.", e);
		}
	}

}
