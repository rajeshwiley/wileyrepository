package com.wiley.wiremock.transromers.utils;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import org.json.JSONArray;
import org.junit.Test;


/**
 * Created by Mikhail_Asadchy on 8/2/2016.
 */
@UnitTest
public class InventoryTransformerUtilTest
{
	@Test
	public void shouldReturnInStockStatusOnly() throws Exception
	{
		// given

		// preparations

		// when
		final JSONArray quantityDependentInventoryStatuses = InventoryTransformerUtil.createInventoryStatuses(2, "");

		// then
		assertEquals(1, quantityDependentInventoryStatuses.length());
		assertEquals("IN_STOCK", quantityDependentInventoryStatuses.getJSONObject(0).get("status"));
	}

	@Test
	public void shouldReturnBackOrderStatus() throws Exception
	{
		// given

		// preparations

		// when
		final JSONArray quantityDependentInventoryStatuses = InventoryTransformerUtil.createInventoryStatuses(10, "");

		// then
		assertEquals(2, quantityDependentInventoryStatuses.length());
		assertEquals("BACK_ORDER", quantityDependentInventoryStatuses.getJSONObject(0).get("status"));
	}

	@Test
	public void shouldReturnPreOrderStatus() throws Exception
	{
		// given

		// preparations

		// when
		final JSONArray quantityDependentInventoryStatuses = InventoryTransformerUtil.createInventoryStatuses(30, "");

		// then
		assertEquals(2, quantityDependentInventoryStatuses.length());
		assertEquals("PRE_ORDER", quantityDependentInventoryStatuses.getJSONObject(0).get("status"));
	}

	@Test
	public void shouldReturnPrintOnDemandStatus() throws Exception
	{
		// given

		// preparations

		// when
		final JSONArray quantityDependentInventoryStatuses = InventoryTransformerUtil.createInventoryStatuses(50, "");

		// then
		assertEquals(2, quantityDependentInventoryStatuses.length());
		assertEquals("PRINT_ON_DEMAND", quantityDependentInventoryStatuses.getJSONObject(0).get("status"));
	}
}
