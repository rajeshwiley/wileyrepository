package com.wiley.wiremock.common;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 26.05.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySlf4jNotifierUnitTest
{
	public static final String SOME_OTHER_TEXT = "some other text";
	public static final String REQUEST_WAS_NOT_MATCHED_MESSAGE = WileySlf4jNotifier.REQUEST_WAS_NOT_MATCHED + SOME_OTHER_TEXT;
	public static final String OTHER_MESSAGE = SOME_OTHER_TEXT + WileySlf4jNotifier.REQUEST_WAS_NOT_MATCHED;

	@Mock
	Throwable throwable;

	@Spy
	private WileySlf4jNotifier wileySlf4jNotifier = new WileySlf4jNotifier(true);

	@Before
	public void setUp() throws Exception
	{
		doNothing().when(wileySlf4jNotifier).superInfo(any());
		doNothing().when(wileySlf4jNotifier).superError(any());
		doNothing().when(wileySlf4jNotifier).superError(any(), any());
	}

	@Test
	public void testSuperInfoIsCalledForRequestNotMatchedMessages() throws Exception
	{
		//given

		//when
		wileySlf4jNotifier.error(REQUEST_WAS_NOT_MATCHED_MESSAGE);

		//then
		verify(wileySlf4jNotifier, never()).superError(any());
		verify(wileySlf4jNotifier).superInfo(REQUEST_WAS_NOT_MATCHED_MESSAGE);
	}

	@Test
	public void testSuperErrorIsCalledForOtherMessages() throws Exception
	{
		//given

		//when
		wileySlf4jNotifier.error(OTHER_MESSAGE);

		//then
		verify(wileySlf4jNotifier, never()).superInfo(any());
		verify(wileySlf4jNotifier, never()).superError(any(), any());
		verify(wileySlf4jNotifier).superError(OTHER_MESSAGE);
	}

	@Test
	public void testSuperErrorWithThrowableIsCalledForOtherMessages() throws Exception
	{
		//given

		//when
		wileySlf4jNotifier.error(OTHER_MESSAGE, throwable);

		//then
		verify(wileySlf4jNotifier, never()).superInfo(any());
		verify(wileySlf4jNotifier, never()).superError(any());
		verify(wileySlf4jNotifier).superError(OTHER_MESSAGE, throwable);
	}
}
