package com.epam.hybris.migration.executor;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.tx.Transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.service.MigrationReleaseService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


@IntegrationTest
public class TransactionalSqlExecutorTest extends ServicelayerTest
{
	private static final Logger LOG = Logger.getLogger(MigrationExecutorTest.class);

	@Resource
	private MigrationExecutor migrationExecutor;

	@Resource
	private MigrationReleaseService migrationReleaseService;

	private static final String RELEASE_VERSION = "p0.0-v0.4";
	private static final String RELEASE_PATH = "test/" + RELEASE_VERSION;


	@Before
	public void setUp()
	{
		migrationExecutor.execute(RELEASE_PATH, "TEST_DATA_CREATE");
		Transaction.current().flushDelayedStore();
	}

	@Test
	public void testNoRollbackCounterpartBlockSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQLBLOCK_NOROLLBACK_COUNTERPART");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}
		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK_COUNTERPART"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK_COUNTERPART",
				"updateProductsWithoutError.sql"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK_COUNTERPART",
				"updateProductsWithError.sql"));

		// partial error-script's result
		assertEquals("444", selectSingleStringJdbc("SELECT p_ean FROM junit_products"));
		// counterpart result
		assertEquals("8", selectSingleStringJdbc("SELECT p_order FROM junit_categories"));
	}

	@Test
	public void testRollbackCounterpartBlockSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQLBLOCK_ROLLBACK_COUNTERPART");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}
		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK_COUNTERPART"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK_COUNTERPART",
				"updateProductsWithoutError.sql"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK_COUNTERPART",
				"updateProductsWithError.sql"));

		// noerror-script's result
		assertEquals("111", selectSingleStringJdbc("SELECT p_ean FROM junit_products"));
		// counterpart result
		assertEquals("8", selectSingleStringJdbc("SELECT p_order FROM junit_categories"));
	}

	@Test
	public void testRollbackBlockSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQLBLOCK_ROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}
		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK"));
		assertTrue(migrationReleaseService
				.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK", "updateProductsWithoutError.sql"));
		assertFalse(migrationReleaseService
				.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_ROLLBACK", "updateProductsWithError.sql"));

		// noerror-script's result
		assertEquals("111", selectSingleStringJdbc("SELECT p_ean FROM junit_products"));
		assertEquals("3", selectSingleStringJdbc("SELECT p_order FROM junit_categories"));
	}

	@Test
	public void testNoRollbackBlockSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQLBLOCK_NOROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}
		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK"));
		assertTrue(migrationReleaseService
				.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK", "updateProductsWithoutError.sql"));
		assertFalse(migrationReleaseService
				.isScriptExecuted(RELEASE_VERSION, "TEST_SQLBLOCK_NOROLLBACK", "updateProductsWithError.sql"));

		// partial error-script's result
		assertEquals("444", selectSingleStringJdbc("SELECT p_ean FROM junit_products"));
		// noerror-script's result
		assertEquals("3", selectSingleStringJdbc("SELECT p_order FROM junit_categories"));
	}

	@Test
	public void testRollbackSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQL_ROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_SQL_ROLLBACK"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQL_ROLLBACK", "scriptWithoutError.sql"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_SQL_ROLLBACK", "scriptWithError.sql"));

		// check if product1.ean has value '111' from the first script
		assertEquals("111", selectSingleStringJdbc("SELECT p_ean FROM junit_products WHERE p_code='product1'"));

	}

	@Test
	public void testCounterpartSql()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_SQL_COUNTERPART");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		// check if column has been added (altered)
		executeJdbc("SELECT test_column FROM new_junit_products");

		// tears down
		executeJdbc("DROP table new_junit_products");
	}

	private void executeJdbc(final String statementStr)
	{
		try (Connection connection = Registry.getCurrentTenant().getDataSource().getConnection())
		{
			LOG.info("Statement to execute: " + statementStr);
			try (PreparedStatement statement = connection.prepareStatement(statementStr))
			{
				statement.execute();
				if (!connection.getAutoCommit())
				{
					connection.commit();
				}
			}
			catch (SQLException e)
			{
				if (!connection.getAutoCommit())
				{
					connection.rollback();
				}
				throw new RuntimeException("Errors occurred while executing jdbs", e);
			}
		}
		catch (SQLException e)
		{
			throw new RuntimeException("Errors occurred while getting connection", e);
		}
	}

	private String selectSingleStringJdbc(final String statementStr)
	{
		try (Connection connection = Registry.getCurrentTenant().getDataSource().getConnection())
		{
			try (PreparedStatement statement = connection.prepareStatement(statementStr);
					ResultSet result = statement.executeQuery())
			{
				if (result.next())
				{
					String rowValue = result.getString(1);
					return rowValue;
				}
				else
				{
					fail("No rows found after migration executed");
				}
			}
			catch (SQLException e)
			{
				throw new RuntimeException("Errors occurred while executing jdbs", e);
			}
		}
		catch (SQLException e)
		{
			throw new RuntimeException("Errors occurred while getting connection", e);
		}
		return null;
	}


}
