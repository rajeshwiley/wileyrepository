package com.epam.hybris.migration.service;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.service.impl.ReleaseVersionTreatmentStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReleaseVersionTreatmentStrategyTest
{
	private ReleaseVersionTreatmentStrategy releaseVersionTreatmentStrategy = new ReleaseVersionTreatmentStrategy();

	@Test
	public void testNormalization()
	{
		assertEquals("p0.0-v0.0", releaseVersionTreatmentStrategy.getNormalizedRelease("p0.0-v0.0"));
		assertEquals("p0.0-v0.123", releaseVersionTreatmentStrategy.getNormalizedRelease("p0.0-v0.123"));
		assertEquals("p0.20-v300.400", releaseVersionTreatmentStrategy.getNormalizedRelease("p0.20-v300.400"));
		assertEquals("p0.0-v0.0", releaseVersionTreatmentStrategy.getNormalizedRelease("test/p0.0-v0.0"));
		assertEquals("p0.0-v0.0", releaseVersionTreatmentStrategy.getNormalizedRelease("test\\p0.0-v0.0"));

	}

	@Test(expected = MigrationException.class)
	public void testNormalizationNoMatching()
	{
		releaseVersionTreatmentStrategy.getNormalizedRelease("v0.0");
	}

	@Test(expected = MigrationException.class)
	public void testNormalizationWrongVersionPart()
	{
		releaseVersionTreatmentStrategy.getNormalizedRelease("p0.0-v1.2.3");
	}

	@Test(expected = MigrationException.class)
	public void testNormalizationWrongPhasePart()
	{
		releaseVersionTreatmentStrategy.getNormalizedRelease("p1.2.3-v0.0");
	}

}
