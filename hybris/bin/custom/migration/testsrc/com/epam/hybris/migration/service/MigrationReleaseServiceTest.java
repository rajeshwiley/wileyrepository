package com.epam.hybris.migration.service;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.model.MigrationReleaseModel;


@IntegrationTest
public class MigrationReleaseServiceTest extends ServicelayerTransactionalTest
{

	@Resource
	private MigrationReleaseService migrationReleaseService;
	@Resource
	private ModelService modelService;



	@Before
	public void setUp() throws Exception
	{
		insertTestData();
	}


	@Test
	public void testAllReleases()
	{
		List<MigrationReleaseModel> result = migrationReleaseService.getAllMigrationRelease();
		assertEquals(13, result.size());
		assertEquals("2.1", result.get(0).getRelease());
		assertEquals("1.0", result.get(12).getRelease());

	}

	@Test
	public void testAlreadyMigrated()
	{
		assertTrue(migrationReleaseService.isReleaseMigrated("2.1", null));
		assertTrue(migrationReleaseService.isReleaseMigrated("2.1", "Z"));
		assertTrue(migrationReleaseService.isReleaseMigrated("2.1", "X"));
		assertFalse(migrationReleaseService.isReleaseMigrated("2.1", "Q"));

		assertTrue(migrationReleaseService.isReleaseMigrated("1.0", "A"));
		assertFalse(migrationReleaseService.isReleaseMigrated("1.0.1", "A"));

		assertTrue(migrationReleaseService.isReleaseMigrated("2.0", "A"));
		assertFalse(migrationReleaseService.isReleaseMigrated("2.0", "Q"));

		assertFalse(migrationReleaseService.isReleaseMigrated("3.0", ""));
		assertFalse(migrationReleaseService.isReleaseMigrated("3.0", "A"));

		migrationReleaseService.insertUpdateMigrationRelease("3.0", "A", true);
		assertTrue(migrationReleaseService.isReleaseMigrated("3.0", "A"));
		assertFalse(migrationReleaseService.isReleaseMigrated("3.0", "B"));
		assertTrue(migrationReleaseService.isReleaseMigrated("3.0", ""));

		assertFalse(migrationReleaseService.isReleaseMigrated("3.1", "A"));

		// check if changing to FALSE ignored
		migrationReleaseService.insertUpdateMigrationRelease("3.0", "A", false);
		assertTrue(migrationReleaseService.isReleaseMigrated("3.0", "A"));
		assertFalse(migrationReleaseService.isReleaseMigrated("3.0", "B"));
		assertTrue(migrationReleaseService.isReleaseMigrated("3.0", ""));

		// check if ignored incompleted
		migrationReleaseService.insertUpdateMigrationRelease("3.1", "A", false);
		assertTrue(migrationReleaseService.isReleaseMigrated("3.1", "A"));

	}

	@Test
	public void testAlreadyExecuted()
	{
		assertFalse(migrationReleaseService.isReleaseExecuted("2.1", null));
		assertTrue(migrationReleaseService.isReleaseExecuted("2.1", "Z"));
		assertTrue(migrationReleaseService.isReleaseExecuted("2.0", "A"));
		assertFalse(migrationReleaseService.isReleaseExecuted("2.0", "Z"));

		migrationReleaseService.insertUpdateMigrationRelease("3.1", "A", false);
		assertFalse(migrationReleaseService.isReleaseExecuted("3.1", "A"));
	}

	@Test(expected = MigrationException.class)
	public void testAddingScriptsWithMissedRelease()
	{
		migrationReleaseService.insertUpdateMigrationScript("1.3", "", "name");
	}

	@Test(expected = MigrationException.class)
	public void testAddingScriptsWithMissedScope()
	{
		migrationReleaseService.insertUpdateMigrationScript("1.0", "", "name");
	}

	@Test
	public void testAddingScripts()
	{
		migrationReleaseService.insertUpdateMigrationScript("1.0", "A", "script1");
		migrationReleaseService.insertUpdateMigrationScript("1.0", "A", "script2");
		assertTrue(migrationReleaseService.isScriptExecuted("1.0", "A", "script1"));
		assertTrue(migrationReleaseService.isScriptExecuted("1.0", "A", "script2"));
		assertFalse(migrationReleaseService.isScriptExecuted("1.0", "A", "script3"));

		// check update operation
		migrationReleaseService.insertUpdateMigrationScript("1.0", "A", "script2");
		assertTrue(migrationReleaseService.isScriptExecuted("1.0", "A", "script2"));

		// check script with incompleted release
		migrationReleaseService.insertUpdateMigrationRelease("3.1", "A", false);
		migrationReleaseService.insertUpdateMigrationScript("3.1", "A", "script2");
		assertTrue(migrationReleaseService.isScriptExecuted("3.1", "A", "script2"));
	}

	private String[][] testData =
			{ { "1.0", "A" }, { "1.0", "B" }, { "1.0", "C" }, { "1.1", "A" }, { "1.2", "C" }, { "2.0", "A" }, { "2.0", "B" },
					{ "2.0", "C" }, { "2.0", "D" }, { "2.0", "E" }, { "2.1", "X" }, { "2.1", "Y" }, { "2.1", "Z" } };

	private void insertTestData()
	{
		DateTime firstReleaseDate = new DateTime().minusHours(15);
		DateTime currentCase = firstReleaseDate;
		for (String[] migration : testData)
		{
			MigrationReleaseModel migrationModel = modelService.create(MigrationReleaseModel.class);
			migrationModel.setRelease(migration[0]);
			migrationModel.setScope(migration[1]);
			migrationModel.setDate(currentCase.toDate());
			migrationModel.setCompleted(Boolean.TRUE);
			modelService.save(migrationModel);
			currentCase = currentCase.plusHours(1);
		}

	}
}
