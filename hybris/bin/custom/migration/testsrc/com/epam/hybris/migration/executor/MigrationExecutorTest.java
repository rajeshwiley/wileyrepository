package com.epam.hybris.migration.executor;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.epam.hybris.migration.dao.MigrationReleaseDao;
import com.epam.hybris.migration.service.MigrationReleaseService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


@IntegrationTest
public class MigrationExecutorTest extends ServicelayerTransactionalTest
{
	private static final Logger LOG = Logger.getLogger(MigrationExecutorTest.class);

	private static final String USER1 = "rerunable";
	private static final String TEST_STRING = "nextrun";
	private static final String USER2 = "single_runnable";

	@Resource
	private MigrationExecutor migrationExecutor;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private MigrationReleaseService migrationReleaseService;

	@Resource
	private MigrationReleaseDao migrationReleaseDao;

	private static final String RERUN_ALLOWED_PROP = "migration.rerun.allowed";

	private static final String RELEASE_VERSION_0_1 = "p0.0-v0.1";
	private static final String RELEASE_PATH_0_1 = "test/" + RELEASE_VERSION_0_1;

	private static final String RELEASE_VERSION_1_1 = "p0.0-v1.1";
	private static final String RELEASE_PATH_1_1 = "test/" + RELEASE_VERSION_1_1;

	private static final String RELEASE_VERSION_2_1 = "p0.0-v2.1";
	private static final String RELEASE_PATH_2_1 = "test/" + RELEASE_VERSION_2_1;

	@Before
	public void setUp()
	{

	}

	@Test
	public void testSingleRunableScriptFailedOnSecondPass()
	{
		// first pass: both scripts shouild be ok
		migrationExecutor.execute(RELEASE_PATH_1_1, "TEST");
		assertNotNull(userService.getUserForUID(USER2));
		UserModel user1 = userService.getUserForUID(USER1);
		assertEquals(0, user1.getDomain().indexOf(TEST_STRING));

		assertTrue(migrationReleaseService.isReleaseMigrated(RELEASE_VERSION_1_1, "TEST"));
		assertTrue(migrationReleaseService.isReleaseMigrated(RELEASE_VERSION_1_1, ""));

		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION_1_1, "TEST", "rerunable.groovy"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION_1_1, "TEST", "single_runnable.impex"));

		// second pass: impex script must fail, but groovy should be ok again
		try
		{
			migrationExecutor.execute(RELEASE_PATH_0_1, "TEST");
			user1 = userService.getUserForUID(USER1);
			LOG.info("Domain of " + USER1 + " is " + user1.getDomain());
			fail("We expected exception in impex");
		}
		catch (Exception e)
		{
			user1 = userService.getUserForUID(USER1);
			assertEquals(TEST_STRING.length() + 1, user1.getDomain().lastIndexOf(TEST_STRING));
			LOG.info(e.getMessage());
		}
	}

	@Test
	public void testSingleRunableScriptSkippedOnSecondPass()
	{
		// first pass: both scripts shouild be ok
		migrationExecutor.execute(RELEASE_PATH_1_1, "TEST");
		UserModel user1 = userService.getUserForUID(USER1);
		assertEquals(0, user1.getDomain().indexOf(TEST_STRING));
		assertNotNull(userService.getUserForUID(USER2));

		// second pass: impex script must fail, but groovy should be ok again
		migrationExecutor.execute(RELEASE_PATH_1_1, "TEST");
		user1 = userService.getUserForUID(USER1);
		LOG.info("Domain of " + USER1 + " is " + user1.getDomain());

		assertEquals(TEST_STRING.length() + 1, user1.getDomain().lastIndexOf(TEST_STRING));
		assertNotNull(userService.getUserForUID(USER2));
	}



}
