package com.epam.hybris.migration.executor;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSPageTemplateDao;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static de.hybris.platform.catalog.jalo.CatalogManager.OFFLINE_VERSION;
import static de.hybris.platform.catalog.jalo.CatalogManager.ONLINE_VERSION;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


@IntegrationTest
public class SyncExecutorTest extends ServicelayerTest   //ServicelayerTransactionalTest
{
	private static final String PRODUCT_CATALOG = "testSyncProductCatalog";
	private static final String CONTENT_CATALOG = "testSyncContentCatalog";

	private static final String PRODUCT_CODE1 = "product1";

	private static final String TEMPLATE_ID = "ProductDetailsPageTemplate";

	private static final String RELEASE_VERSION =  "p0.0-v0.3";
	private static final String RELEASE_PATH =  "test/" + RELEASE_VERSION;
	@Resource
	private MigrationExecutor migrationExecutor;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ProductService productService;

	@Resource
	private CMSPageTemplateDao cmsPageTemplateDao;

	@Before
	public void setUp()
	{
		migrationExecutor.execute(RELEASE_PATH, "TEST_BEFORE");
	}

	@Test
	public void testSyncBothCatalogs()
	{
		// check initial data
		CatalogVersionModel prodStaged = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, OFFLINE_VERSION);
		CatalogVersionModel contStaged = catalogVersionService.getCatalogVersion(CONTENT_CATALOG, OFFLINE_VERSION);

		assertNotNull(productService.getProductForCode(prodStaged, PRODUCT_CODE1));

		List<PageTemplateModel> templates = cmsPageTemplateDao
				.findPageTemplatesByIdAndCatalogVersion(TEMPLATE_ID, contStaged);
		assertNotNull(templates);
		assertEquals(1, templates.size());

		migrationExecutor.execute(RELEASE_PATH, "TEST_AFTER");

		// check the same in Online version
		CatalogVersionModel prodOnline = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, ONLINE_VERSION);
		CatalogVersionModel contOnline = catalogVersionService.getCatalogVersion(CONTENT_CATALOG, ONLINE_VERSION);

		assertNotNull(productService.getProductForCode(prodOnline, PRODUCT_CODE1));

		templates = cmsPageTemplateDao
				.findPageTemplatesByIdAndCatalogVersion(TEMPLATE_ID, contOnline);
		assertNotNull(templates);
		assertEquals(1, templates.size());
	}
}