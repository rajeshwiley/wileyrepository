package com.epam.hybris.migration.executor;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.service.MigrationReleaseService;

import static de.hybris.platform.catalog.jalo.CatalogManager.OFFLINE_VERSION;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


@IntegrationTest
public class TransactionalExecutorTest extends ServicelayerTest
{
	private static final String PRODUCT_CATALOG = "testProductCatalog";

	@Resource
	private MigrationExecutor migrationExecutor;

	@Resource
	private MigrationReleaseService migrationReleaseService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ProductService productService;

	@Resource
	private CategoryService categoryService;

	private static final String RELEASE_VERSION = "p0.0-v0.4";
	private static final String RELEASE_PATH = "test/" + RELEASE_VERSION;

	@Before
	public void setUp()
	{
		migrationExecutor.execute(RELEASE_PATH, "TEST_DATA_CREATE");
	}

	@Test
	public void testRollbackGroovies()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_GROOVY_ROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_GROOVY_ROLLBACK"));
		assertTrue(
				migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_ROLLBACK", "scriptWithoutError.groovy"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_ROLLBACK", "scriptWithError.groovy"));

		checkCategoriesAndProducts();
	}

	@Test
	public void testCounterpartWithRollbackGroovy()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_GROOVY_COUNTERPART_ROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_ROLLBACK"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_ROLLBACK",
				"scriptWithoutError.groovy"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_ROLLBACK",
				"scriptWithError.groovy"));

		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, OFFLINE_VERSION);
		// check script_without_error outcome
		ProductModel product1 = productService.getProductForCode(catalogVersion, "product1");
		assertEquals(2, product1.getSupercategories().size());
		// check counterpart outcome
		assertEquals("category2", productService.getProductForCode(catalogVersion, "product2").getSupercategories().iterator()
				.next().getCode());

	}

	@Test
	public void testCounterpartWithoutRollbackGroovy()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_GROOVY_COUNTERPART_NOROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_NOROLLBACK"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_NOROLLBACK",
				"scriptWithoutError.groovy"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_GROOVY_COUNTERPART_NOROLLBACK",
				"scriptWithError.groovy"));

		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, OFFLINE_VERSION);
		// check norollbacked script_with_error outcome
		ProductModel product1 = productService.getProductForCode(catalogVersion, "product1");
		assertEquals(1, product1.getSupercategories().size());
		assertEquals("category2", product1.getSupercategories().iterator().next().getCode());

		// check counterpart outcome
		assertEquals("category2", productService.getProductForCode(catalogVersion, "product2").getSupercategories().iterator()
				.next().getCode());

	}

	@Test
	public void testBadCounterpart()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_GROOVY_BADCOUNTERPART");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour (check if this exception from counterpart
			assertTrue(ex.getMessage().indexOf("badCounterpart.groovy") > 0);

		}

	}

	@Test
	public void testRollbackImpexes()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_IMPEX_ROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_IMPEX_ROLLBACK"));
		assertTrue(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_IMPEX_ROLLBACK", "scriptWithoutError.impex"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_IMPEX_ROLLBACK", "scriptWithError.impex"));

		checkCategoriesAndProducts();
	}

	@Test
	public void testNoRollbackImpexes()
	{
		try
		{
			migrationExecutor.execute(RELEASE_PATH, "TEST_IMPEX_NOROLLBACK");
			fail("A MigrationException expected");
		}
		catch (MigrationException ex)
		{
			// correct behaviour
		}

		assertFalse(migrationReleaseService.isReleaseExecuted(RELEASE_VERSION, "TEST_IMPEX_NOROLLBACK"));
		assertTrue(
				migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_IMPEX_NOROLLBACK", "scriptWithoutError.impex"));
		assertFalse(migrationReleaseService.isScriptExecuted(RELEASE_VERSION, "TEST_IMPEX_NOROLLBACK", "scriptWithError.impex"));

		// check partial result of failed script
		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, OFFLINE_VERSION);
		assertNotNull(categoryService.getCategoryForCode(catalogVersion, "category3"));
	}

	private void checkCategoriesAndProducts()
	{
		CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, OFFLINE_VERSION);
		// check that error-free script has been committed
		assertNotNull(categoryService.getCategoryForCode(catalogVersion, "category2"));
		ProductModel product1 = productService.getProductForCode(catalogVersion, "product1");
		assertNotNull(product1);
		assertEquals(2, product1.getSupercategories().size());
		ProductModel product2 = productService.getProductForCode(catalogVersion, "product2");
		assertNotNull(product2);
		assertEquals(1, product2.getSupercategories().size());
		assertEquals("category1", product2.getSupercategories().iterator().next().getCode());

		try
		{
			assertNull(categoryService.getCategoryForCode(catalogVersion, "category3"));
			fail("Category3 must not exist");
		}
		catch (UnknownIdentifierException | ModelNotFoundException ex)
		{
			// correct behaviour
		}

	}


}
