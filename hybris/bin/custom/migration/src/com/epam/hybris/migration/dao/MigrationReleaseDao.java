package com.epam.hybris.migration.dao;

import java.util.List;

import com.epam.hybris.migration.model.MigrationReleaseModel;
import com.epam.hybris.migration.model.MigrationScriptModel;


/**
 * <p>
 * interface of DAO for working with database releases
 * </p>
 *
 * Created by Pavel_Maximov on 9/17/2015.
 */
public interface MigrationReleaseDao
{

	/**
	 * <p>
	 * return all release of database migration
	 * </p>
	 *
	 * @return
	 */
	List<MigrationReleaseModel> findAllCompletedMigrationReleases();

	/**
	 * <p>
	 * returns release items with given releaseVersion and scope
	 * </p>
	 */
	List<MigrationReleaseModel> findByVersionAndScope(String releaseVersion, String scope);

	/**
	 * <p>
	 * returns release items with given releaseVersion
	 * </p>
	 */
	List<MigrationReleaseModel> findCompletedByVersion(String releaseVersion);

	/**
	 * <p>
	 * return last release of database migration
	 * </p>
	 *
	 * @return
	 */
	List<MigrationReleaseModel> findLastMigrationRelease();

	List<MigrationScriptModel> findScriptByNameAndRelease(String scriptName, MigrationReleaseModel migrationRelease);
}
