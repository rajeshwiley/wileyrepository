package com.epam.hybris.migration.executor.impl;

import java.nio.file.Paths;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.epam.hybris.migration.dto.File;
import com.epam.hybris.migration.dto.ScriptStep;
import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationReleaseService;
import com.epam.hybris.migration.service.MigrationService;
import com.epam.hybris.migration.service.impl.ReleaseVersionTreatmentStrategy;


/**
 * Base MigrationStepExecutor implementation for script steps.
 */
public abstract class AbstractScriptStepExecutor implements MigrationStepExecutor
{
	@Autowired
	private ReleaseVersionTreatmentStrategy releaseVersionTreatmentStrategy;
	@Autowired
	private MigrationReleaseService migrationReleaseService;

	@Autowired
	protected MigrationService migrationService;

	private TransactionTemplate transactionTemplate;

	/**
	 * Executes all files for a step.
	 *
	 * @param jaxbStep
	 * 		step definition
	 * @param releaseDir
	 * 		release directory name
	 */
	@Override
	public void execute(final JAXBElement jaxbStep, final String releaseDir, final String scope)
	{
		ScriptStep step = (ScriptStep) jaxbStep.getValue();
		String releaseVersion = releaseVersionTreatmentStrategy.getNormalizedRelease(releaseDir);

		for (File file : step.getFile())
		{
			if (isExecuteAllowed(file, releaseVersion, scope))
			{
				try
				{
					if (file.isTransactional())
					{
						executeScriptFileInTransaction(file, jaxbStep, releaseDir);
					}
					else
					{
						executeScriptFileNoTransaction(file, jaxbStep, releaseDir);
					}
					migrationReleaseService.insertUpdateMigrationScript(releaseVersion, scope, file.getName());
				}
				catch (MigrationException ex)
				{
					executeCounterpartScript(file, jaxbStep, releaseDir);
					throw ex;
				}
				catch (Throwable ex)
				{
					// TODO: perhabs we have to check UndeclaredThrowableException here
					executeCounterpartScript(file, jaxbStep, releaseDir);
					throw new MigrationException("Exception during execute single script: " + file.getName(), ex);
				}
			}
		}
	}

	private boolean isExecuteAllowed(final File file, final String releaseVersion, final String scope)
	{
		return file.isRerunAllowed() || !migrationReleaseService.isScriptExecuted(releaseVersion, scope, file.getName());
	}

	private void executeScriptFileInTransaction(final File file, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir)
	{
		this.transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			protected void doInTransactionWithoutResult(final TransactionStatus status)
			{
				executeFile(file.getName(), jaxbStep, releaseDir, file.isRelaxedValidationMode());
			}
		});
	}

	private void executeScriptFileNoTransaction(final File file, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir)
	{
		executeFile(file.getName(), jaxbStep, releaseDir, file.isRelaxedValidationMode());
	}

	/** Current implementation executes script of the same nature as the main script */
	private void executeCounterpartScript(final File file, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir)
	{
		if (StringUtils.isNotEmpty(file.getCounterpartName()))
		{
			try
			{
				executeFile(file.getCounterpartName(), jaxbStep, releaseDir, file.isRelaxedValidationMode());
			}
			catch (Throwable ex)
			{
				throw new MigrationException("Error execution script counterpart: " + file.getCounterpartName(), ex);
			}
		}
	}

	/**
	 * Constructs absolute file path.
	 * Files are scripts placed in release directory sub-directories named by step name in migration config.
	 *
	 * @param filename
	 * @param releaseDir
	 * 		release directory name
	 * @param stepName
	 * 		step name in migration config
	 * @return absolute file path
	 */
	protected String getFilePath(final String filename, final String releaseDir, final String stepName)
	{
		return migrationService.getMigrationReleasePath(releaseDir).resolve(Paths.get(stepName, filename)).toString();
	}

	/**
	 * Constructs absolute step directory path.
	 *
	 * @param releaseDir
	 * 		release directory name
	 * @param stepName
	 * 		step name in migration config
	 * @return absolute directory path
	 */
	protected String getStepDirPath(final String releaseDir, final String stepName)
	{
		return migrationService.getMigrationReleasePath(releaseDir).resolve(Paths.get(stepName)).toString();
	}

	/** By default script can be transactional */
	protected boolean isTransactionAware()
	{
		return true;
	}

	/**
	 * Executes file given.
	 *
	 * @param fileName
	 * 		script file name without path
	 * @param jaxbStep
	 * 		step definition
	 * @param filePath
	 * 		absolute path
	 * @param isRelaxedValidationMode
	 * 		relaxed validation mode flag
	 */
	protected abstract void executeFile(String fileName, JAXBElement<ScriptStep> jaxbStep, String filePath,
			boolean isRelaxedValidationMode);

	/////////////////////////////////////////////////////////////////////////////////////////

	@Required
	public void setTransactionTemplate(final TransactionTemplate transactionTemplate)
	{
		this.transactionTemplate = transactionTemplate;
	}



}
