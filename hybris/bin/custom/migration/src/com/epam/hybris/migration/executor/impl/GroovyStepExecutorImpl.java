package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import com.epam.hybris.migration.dto.ScriptStep;


public class GroovyStepExecutorImpl extends AbstractScriptStepExecutor
{

	@Override
	protected void executeFile(final String fileName, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir,
			final boolean isRelaxedValidationMode)
	{
		String filePath = getFilePath(fileName, releaseDir, jaxbStep.getName().toString());

		migrationService.executeGroovyScript(filePath);
	}

}
