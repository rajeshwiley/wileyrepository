package com.epam.hybris.migration.service.impl;

import de.hybris.platform.core.Registry;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.service.SqlScriptProcessor;


public class SqlScriptProcessorImpl implements SqlScriptProcessor
{
	private static final Logger LOG = Logger.getLogger(SqlScriptProcessorImpl.class);


	@Override
	public void executeSingleSql(final File sqlFile)
	{

		String query = null;
		try
		{
			query = FileUtils.readFileToString(sqlFile);
		}
		catch (IOException e2)
		{
			throw new MigrationException("Errors occurred while executing SQL. See logs for more details.", e2);
		}

		LOG.info("Query: " + query);

		try (Connection connection = Registry.getCurrentTenant().getDataSource().getConnection())
		{
			// Auto-commit should be performed on Statement.close()
			try (PreparedStatement statement = connection.prepareStatement(query))
			{
				final int affectedRowsNum = statement.executeUpdate();
				LOG.info("Execution finished. Have been affected " + affectedRowsNum + " rows");
			}
			catch (SQLException e)
			{
				throw new MigrationException("Errors occurred while executing sql. See logs for more details.", e);
			}
		}
		catch (SQLException e)
		{
			throw new MigrationException("Errors occurred while executing sql. See logs for more details.", e);
		}

	}

	@Override
	public void executeBlockSql(final File sqlFile)
	{
		try (Connection connection = Registry.getCurrentTenant().getDataSource().getConnection())
		{
			Resource scriptResource = new FileSystemResource(sqlFile);

			ScriptUtils.executeSqlScript(connection, scriptResource);
		}
		catch (ScriptException ex)
		{
			throw new MigrationException("Errors occurred while executing sql. See logs for more details.", ex);
		}
		catch (SQLException ex)
		{
			throw new MigrationException("Errors occurred while getting data source connection. See logs for more details.", ex);
		}
	}

}
