package com.epam.hybris.migration.exception;

public class MigrationException extends RuntimeException
{
	public MigrationException()
	{
	}

	public MigrationException(final String message)
	{
		super(message);
	}

	public MigrationException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public MigrationException(final Throwable cause)
	{
		super(cause);
	}

	public MigrationException(final String message, final Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
