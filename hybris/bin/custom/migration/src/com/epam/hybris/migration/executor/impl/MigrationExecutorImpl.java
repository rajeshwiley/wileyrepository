package com.epam.hybris.migration.executor.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dto.CleanupStep;
import com.epam.hybris.migration.dto.RemoveCatalogStep;
import com.epam.hybris.migration.dto.RunIndexStep;
import com.epam.hybris.migration.dto.ScriptStep;
import com.epam.hybris.migration.dto.Steps;
import com.epam.hybris.migration.dto.SyncStep;
import com.epam.hybris.migration.dto.UpdateStep;
import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.executor.MigrationExecutor;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationReleaseService;
import com.epam.hybris.migration.service.MigrationService;
import com.epam.hybris.migration.service.impl.ReleaseVersionTreatmentStrategy;


/**
 * Automatic hybris migration executer implementation.
 *
 * Migration config file name convention is migrationConfig.xml.
 * This file is used as instruction describing migration steps sequence.
 * Config file is parsed against migrationConfig.xsd schema, resulting in Steps instance.
 * For each step, name is used to resolve executor for it with a stepName -> MigrationStepExecutors map.
 */
public class MigrationExecutorImpl implements MigrationExecutor
{
	private static final String MIGRATION_LOG_PATTERN =
			"%d{dd MMM yyyy HH:mm:ss,SSS} %-5p [%t] %X{RemoteAddr}%X{Tenant}%X{CronJob}[%c{1}] %m%n";
	private static final String MIGRATION_CONFIG_FILENAME = "migrationConfig.xml";
	private static final String RERUN_ALLOWED_PROP = "migration.rerun.allowed";

	private Map<String, MigrationStepExecutor> stepExecutorMap;

	@Autowired
	private MigrationService migrationService;
	@Autowired
	private MigrationReleaseService migrationReleaseService;
	@Autowired
	private ReleaseVersionTreatmentStrategy releaseVersionTreatmentStrategy;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private UserService userService;
	@Autowired
	private ConfigurationService configService;

	/**
	 * Performs migration according to migrationConfig.xml file.
	 * Logging format is modified to add timestamp.
	 * All steps are performed on behalf of the Administrator user.
	 *
	 * @param releaseDirectory
	 * 		name of a directory under MIGRATION_DIR containing migrationConfig.xml
	 */
	@Override
	public void execute(final String releaseDirectory, final String scope)
	{
		setLoggingPattern();

		migrationService.updateRunningSystemIfNecessary();

		final Logger logger = Logger.getRootLogger();
		String normalizedRelease = releaseVersionTreatmentStrategy.getNormalizedRelease(releaseDirectory);

		final Steps migrationSteps = parseConfig(releaseDirectory);
		if (isCurrentMigrationAllowed(normalizedRelease, scope))
		{
			logger.info("Migration of release " + normalizedRelease + " started. Scope is " + scope);

			logReleaseMigrationStarted(normalizedRelease, scope);

			final UserModel adminUser = userService.getAdminUser();
			for (final JAXBElement stepJaxb : migrationSteps.getUpdateOrGroovyOrExport())
			{
				sessionService.executeInLocalView(new SessionExecutionBody()
				{
					@Override
					public void executeWithoutResult()
					{
						if (isScopeAllowed(scope, stepJaxb))
						{
							getStepExecutorMap().get(stepJaxb.getName().toString()).execute(stepJaxb, releaseDirectory, scope);
						}
						else
						{
							logger.info("Step " + stepJaxb.getName() + " is out of scope");
						}
					}
				}, adminUser);
			}

			logReleaseMigrationSucceeded(normalizedRelease, scope);
			logger.info("Finished successfully");
		}
		else
		{
			logger.warn("Cannot perform release migration " + normalizedRelease + " scope=" + scope);
			return;
		}
	}

	protected boolean isCurrentMigrationAllowed(final String releaseVersion, final String scope)
	{
		if (configService.getConfiguration().getBoolean(RERUN_ALLOWED_PROP))
		{
			return true;
		}
		return !migrationReleaseService.isReleaseMigrated(releaseVersion, scope);
	}

	protected boolean isScopeAllowed(final String scope, final JAXBElement stepJaxb)
	{
		if (StringUtils.isEmpty(scope))
		{
			return true;
		}
		if (stepJaxb.getValue() instanceof ScriptStep)
		{
			return scope.equals(((ScriptStep) stepJaxb.getValue()).getScope());
		}
		else if (stepJaxb.getValue() instanceof CleanupStep)
		{
			return scope.equals(((CleanupStep) stepJaxb.getValue()).getScope());
		}
		else if (stepJaxb.getValue() instanceof UpdateStep)
		{
			return scope.equals(((UpdateStep) stepJaxb.getValue()).getScope());
		}
		else if (stepJaxb.getValue() instanceof SyncStep)
		{
			return scope.equals(((SyncStep) stepJaxb.getValue()).getScope());
		}
		else if (stepJaxb.getValue() instanceof RemoveCatalogStep)
		{
			return scope.equals(((RemoveCatalogStep) stepJaxb.getValue()).getScope());
		}
		else if (stepJaxb.getValue() instanceof RunIndexStep)
		{
			return scope.equals(((RunIndexStep) stepJaxb.getValue()).getScope());
		}
		else
		{
			return true;
		}
	}

	private Steps parseConfig(final String releaseDirectory)
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(Steps.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Source source = new StreamSource(new FileInputStream(getMigrationConfigPath(releaseDirectory).toFile()));

			JAXBElement<Steps> migrationStepsJaxb = unmarshaller.unmarshal(source, Steps.class);

			return migrationStepsJaxb.getValue();
		}
		catch (JAXBException | FileNotFoundException e)
		{
			throw new MigrationException(e);
		}
	}

	private Path getMigrationConfigPath(final String releaseDirectory)
	{
		return migrationService.getMigrationReleasePath(releaseDirectory).resolve(MIGRATION_CONFIG_FILENAME);
	}

	public Map<String, MigrationStepExecutor> getStepExecutorMap()
	{
		return stepExecutorMap;
	}

	public void setStepExecutorMap(final Map<String, MigrationStepExecutor> stepExecutorMap)
	{
		this.stepExecutorMap = stepExecutorMap;
	}


	private void logReleaseMigrationSucceeded(final String currentRelease, final String scope)
	{
		migrationReleaseService.insertUpdateMigrationRelease(currentRelease, scope, true);
	}

	private void logReleaseMigrationStarted(final String currentRelease, final String scope)
	{
		migrationReleaseService.insertUpdateMigrationRelease(currentRelease, scope, false);
	}

	private void setLoggingPattern()
	{
		Logger rootLogger = Logger.getRootLogger();
		Enumeration apps = rootLogger.getAllAppenders();
		while (apps.hasMoreElements())
		{
			Object app = apps.nextElement();
			if (app instanceof Appender)
			{
				((Appender) app).setLayout(new PatternLayout(MIGRATION_LOG_PATTERN));
			}
		}
	}

}
