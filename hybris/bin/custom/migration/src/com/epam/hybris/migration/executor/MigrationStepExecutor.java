package com.epam.hybris.migration.executor;

import javax.xml.bind.JAXBElement;


/**
 * Migration step executor.
 *
 * @author Sergey_Voronov
 */
public interface MigrationStepExecutor
{
	/**
	 * Executes migration step.
	 *
	 * @param migrationStep
	 * 		definition of step to execute
	 * @param releaseDir
	 * 		directory to execute step in
	 * @param scope
	 * 		migration scope. used for logging only
	 */
	void execute(JAXBElement migrationStep, String releaseDir, String scope);
}
