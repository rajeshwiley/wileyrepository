package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationService;


public class CleanupStepExecutorImpl implements MigrationStepExecutor
{

	@Autowired
	protected MigrationService migrationService;

	@Override
	public void execute(final JAXBElement jaxbStep, final String releaseDir, final String scope)
	{
		migrationService.cleanUpTypes();
	}

}
