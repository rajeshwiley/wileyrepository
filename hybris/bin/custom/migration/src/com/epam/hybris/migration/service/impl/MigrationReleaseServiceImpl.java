package com.epam.hybris.migration.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dao.MigrationReleaseDao;
import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.model.MigrationReleaseModel;
import com.epam.hybris.migration.model.MigrationScriptModel;
import com.epam.hybris.migration.service.MigrationReleaseService;

import junit.framework.Assert;


/**
 * <p>
 * Implemetation of MigrationReleaseService
 * </p>
 *
 * Created by Pavel_Maximov on 9/18/2015.
 */
public class MigrationReleaseServiceImpl implements MigrationReleaseService
{
	@Autowired
	private ReleaseVersionTreatmentStrategy releaseVersionTreatmentStrategy;

	@Autowired
	private ModelService modelService;

	@Autowired
	private MigrationReleaseDao releaseDao;

	@Override
	public List<MigrationReleaseModel> getAllMigrationRelease()
	{
		return releaseDao.findAllCompletedMigrationReleases();
	}


	@Override
	public void insertUpdateMigrationRelease(final String releaseVersion, final String scope, boolean completed)
	{
		List<MigrationReleaseModel> existingRelease = releaseDao.findByVersionAndScope(releaseVersion, scope);
		MigrationReleaseModel newRelease = existingRelease.isEmpty() ?
				modelService.create(MigrationReleaseModel.class) :
				existingRelease.get(0);
		newRelease.setRelease(releaseVersion);
		newRelease.setScope(scope);
		newRelease.setDate(new Date());
		Boolean oldCompleted = newRelease.getCompleted();
		if (oldCompleted == null || !oldCompleted)
		{
			newRelease.setCompleted(completed);
		}

		modelService.save(newRelease);
	}

	@Override
	public void insertUpdateMigrationScript(final String releaseVersion, final String scope, final String scriptName)
	{
		List<MigrationReleaseModel> existingRelease = releaseDao.findByVersionAndScope(releaseVersion, scope);
		if (existingRelease.isEmpty())
		{
			throw new MigrationException(String.format("Release %s with scope %s is not persisted", releaseVersion, scope));
		}
		else if (existingRelease.size() > 1)
		{
			throw new MigrationException(String.format("Release %s with scope %s is ambiguous", releaseVersion, scope));
		}

		List<MigrationScriptModel> existingScript = releaseDao.findScriptByNameAndRelease(scriptName, existingRelease.get(0));
		MigrationScriptModel newScript = existingScript.isEmpty() ?
				modelService.create(MigrationScriptModel.class) :
				existingScript.get(0);

		newScript.setScriptName(scriptName);
		newScript.setDate(new Date());
		newScript.setMirationRelease(existingRelease.get(0));
		modelService.save(newScript);
	}

	@Override
	public boolean isReleaseMigrated(final String currentRelease, final String scope)
	{
		Assert.assertNotNull(currentRelease);
		List<MigrationReleaseModel> foundReleases = new ArrayList<>();

		if (StringUtils.isEmpty(scope))
		{
			foundReleases.addAll(releaseDao.findCompletedByVersion(currentRelease));
		}
		else
		{
			foundReleases.addAll(releaseDao.findByVersionAndScope(currentRelease, scope));
		}

		return !foundReleases.isEmpty();
	}

	@Override
	public boolean isReleaseExecuted(final String currentRelease, final String scope)
	{
		String scopeLocal = scope != null ? scope : "";
		List<MigrationReleaseModel> found = releaseDao.findByVersionAndScope(currentRelease, scopeLocal);
		return !found.isEmpty() && found.get(0).getCompleted();
	}

	@Override
	public boolean isScriptExecuted(final String releaseVersion, final String scope, final String scriptName)
	{
		List<MigrationReleaseModel> existingRelease = releaseDao.findByVersionAndScope(releaseVersion, scope);
		if (existingRelease.isEmpty())
		{
			throw new MigrationException(String.format("Release %s with scope %s is not persisted", releaseVersion, scope));
		}
		List<MigrationScriptModel> existingScript = releaseDao.findScriptByNameAndRelease(scriptName, existingRelease.get(0));
		return !existingScript.isEmpty();
	}

}
