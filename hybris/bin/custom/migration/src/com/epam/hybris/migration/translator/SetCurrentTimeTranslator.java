package com.epam.hybris.migration.translator;

import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;

import java.util.Date;

import org.apache.log4j.Logger;


/**
 * Perform attribute import during impex import process by setting current time in it.
 * Require input parameter - "attributeName"
 * Example for impex usage: @lastsynctime[translator=SetCurrentTimeTranslator,attributeName='lastSyncTime'];
 *
 * @author Sergey_Andreev on 9/9/2015.
 */
public class SetCurrentTimeTranslator extends AbstractSpecialValueTranslator
{
	private static final Logger LOG = Logger.getLogger(SetCurrentTimeTranslator.class.getName());
	private String attributeName;

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		super.init(columnDescriptor);

		attributeName = columnDescriptor.getDescriptorData().getModifier("attributeName");
		if (attributeName == null)
		{
			throw new HeaderValidationException(columnDescriptor.getHeader(),
					"missing expression for velocity column " + columnDescriptor.getValuePosition() + ":" + columnDescriptor
							.getQualifier(), 0);
		}
	}

	@Override
	public String performExport(final Item item) throws ImpExException
	{
		return new Date().toString();
	}

	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		try
		{
			processedItem.setAttribute(attributeName, new Date());
		}
		catch (JaloBusinessException e)
		{
			LOG.error(e);
		}
	}
}
