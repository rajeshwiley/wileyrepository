package com.epam.hybris.migration.service;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;


public interface MigrationService
{
	/**
	 * Imports data using impex from given path. Supports files with {@code .impex} extension and {@code zip} archives with
	 * {@code importscript.impex} file and additional csv data files.
	 * @param impexFilePath
	 * 		the import impex file path
	 * @param executeInLegacyMode
	 * 		the legacy mode flag
	 * @param executeInRelaxedValidationMode
	 * 		the relaxed validation mode flag
	 */
	void importImpex(String impexFilePath, boolean executeInLegacyMode, boolean executeInRelaxedValidationMode);

	/**
	 * Exports data to the {@code exportDestFilePath} folder, using impex script from {@code impexFilePath}.
	 * Result file will have suffix {@code "_result"} and extension {@code ".zip"}
	 * if {@code exportInSingleFile == false}, otherwise - {@code ".impex"}.
	 * For example if impex file name is {@code "export_venues.impex"} and {@code exportInSingleFile == false}
	 * then result file name is
	 * {@code "export_venues_result.zip"}.
	 *
	 * @param impexFilePath
	 * 		the export script file path
	 * @param exportDestDirPath
	 * 		the exported data destination directory path
	 * @param exportInSingleFile
	 * 		if {@code false }then export result will be a zip archive with {@code importscript.impex}
	 * 		file and additional csv data files. If {@code true} then export result will be a single impex file.
	 */
	void exportImpex(String impexFilePath, String exportDestDirPath, boolean exportInSingleFile);

	/**
	 * Updates system with given parameters.
	 *
	 * @param extensionNames
	 * 		the names of extensions to load project data. {@code null} accepted
	 * @param updateRunningSystem
	 * 		the update type system flag
	 * @param clearHmc
	 * 		the clear hmc flag
	 * @param loadEssentialData
	 * 		the load essential data flag
	 * @param localizeTypes
	 * 		the localize types flag
	 * @param logWriter
	 * 		the {@code StringWriter} that will be used by OOB Initialization class to log all update steps
	 * 		(like in hac -> Platform -> Update)
	 */
	void updateSystem(Collection<String> extensionNames, boolean updateRunningSystem, boolean clearHmc,
			boolean loadEssentialData, boolean localizeTypes, StringWriter logWriter);

	/**
	 * Update system with parameters taken from JSON-file
	 * @param configFileName name of JSON file with parameters. Format see in hybris-wiki.
	 * @param logWriter
	 * 		the {@code StringWriter} that will be used by OOB Initialization class to log all update steps
	 * 		(like in hac -> Platform -> Update)
	 */
	void updateSystem(String configFileName, String releaseDir, StringWriter logWriter);

	/**
	 * Perfrom syncronization between catalog versions of given catalog
	 * Default syncropnization job is used, i.e. it syncs all catalogAware items between Staged and Online versions
	 * Method defines whether catalog is ContentCatalog and creates proper job
	 *
	 * @param catalogName
	 */
	void syncronizeCatalog(String catalogName);

	/**
	 * Drops specified catalog
	 *
	 * @param catalogName
	 */
	void removeCatalog(String catalogName);

	/**
	 * Cleans up orphaned types.
	 *
	 * <p><b>NOTE: </b>tries to remove types until all types will be removed without errors.
	 * If after {@code MAX_NUMBER_OF_CLEANUP_ATTEMPTS}
	 * attempts there will remain undeleted types, {@code MigrationExcepton} will be thrown.</p>
	 */
	void cleanUpTypes();

	/**
	 * Executes SQL from file with given path.
	 *
	 * <p><b>NOTE: </b>supports only INSERT, UPDATE and DELETE queries.
	 * Throws {@code IllegalArgumentException} for other query types.</p>
	 *
	 * @param sqlFilePath
	 * 		the sql query file path
	 * @throws IOException
	 */
	void executeSql(String sqlFilePath);

	/**
	 * Executes groovy script from file with given path.
	 *
	 * @param scriptFilePath
	 * 		the groovy script file path
	 */
	void executeGroovyScript(String scriptFilePath);

	/**
	 * Constructs migration release directory path.
	 *
	 * @param releaseDirectory
	 * 		release directory name
	 * @return Path to migration release directory.
	 */
	Path getMigrationReleasePath(String releaseDirectory);

	/**
	 * Perform  update running system if it's necesary for migration work
	 */
	void updateRunningSystemIfNecessary();
	
	void runSolrIndex(List<String> stores);
}

