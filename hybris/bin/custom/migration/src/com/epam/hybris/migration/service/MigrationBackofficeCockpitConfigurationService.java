package com.epam.hybris.migration.service;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Backoffice cockpit configuration service which should reset cockpitng configuration.
 * Most part of this class is a copy-paste of com.hybris.backoffice.config.impl.BackofficeCockpitConfigurationService
 *
 * @author Anna Kochneva, Sergey_Andreev
 */
public class MigrationBackofficeCockpitConfigurationService
{

	public static final String CHARSET_NAME = Charset.defaultCharset().toString();
	private static final Logger LOG = LoggerFactory.getLogger(MigrationBackofficeCockpitConfigurationService.class);

	@Autowired
	private MediaService mediaService;
	@Autowired
	private ModelService modelService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private UserService userService;

	private ByteArrayOutputStream configBuffer;

	public void resetToDefaults()
	{
		final UserModel adminUser = userService.getAdminUser();
		sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				LOG.info("Reset to default backoffice configuration is started.");
				reset();
				LOG.info("Reset to default backoffice configuration is ended.");
			}
		}, adminUser);
	}

	protected void reset()
	{
		InputStream resourceAsStream = null;
		try
		{
			resourceAsStream = getDefaultCockpitConfigAsStream();
			if (resourceAsStream != null)
			{
				String defaultCockpitConfig = convertConfigToString(resourceAsStream);
				setConfigAsString(defaultCockpitConfig);
			}
		}
		finally
		{
			IOUtils.closeQuietly(resourceAsStream);
		}
	}

	private InputStream getDefaultCockpitConfigAsStream()
	{
		return getCurrentClassLoader(this.getClass()).getResourceAsStream(
				"impex/backoffice-cockpit-config.xml");
	}

	private ClassLoader getCurrentClassLoader(final Class<?> clazz)
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		return classLoader == null ? clazz.getClassLoader() : classLoader;
	}

	protected String convertConfigToString(final InputStream inputStream)
	{
		StringBuilder builder = new StringBuilder();
		try
		{
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, CHARSET_NAME));
			try
			{
				String line;
				while ((line = reader.readLine()) != null)
				{
					builder.append(line).append("\n");
				}
			}
			finally
			{
				reader.close();
			}
		}
		catch (Exception e)
		{
			if (LOG.isErrorEnabled())
			{
				LOG.error("could not load cockpit configuration", e);
			}
		}
		return builder.toString();
	}

	public void setConfigAsString(final String content)
	{
		setConfigAsStr(content);
		mediaService.setDataForMedia(getCockpitNGConfig(), configBuffer.toByteArray());
		modelService.save(getCockpitNGConfig());
		configBuffer = null;
	}

	public void setConfigAsStr(final String content)
	{
		try
		{
			BufferedOutputStream ostream = new BufferedOutputStream(getConfigFileOutputStream());
			try
			{
				ostream.write(content.getBytes(CHARSET_NAME));
			}
			finally
			{
				ostream.close();
			}
		}
		catch (Exception e)
		{
			if (LOG.isErrorEnabled())
			{
				LOG.error("could not store cockpit configuration", e);
			}
		}
	}

	protected ByteArrayOutputStream getConfigFileOutputStream()
	{
		if (configBuffer == null)
		{
			configBuffer = new ByteArrayOutputStream();
		}
		return configBuffer;
	}

	protected MediaModel getCockpitNGConfig()
	{
		MediaModel media = null;
		try
		{
			media = mediaService.getMedia("cockpitng-config");
		}
		catch (UnknownIdentifierException e)
		{
			LOG.info("Create new config file");
		}
		if (media == null)
		{
			media = createConfigFile();
		}
		return media;
	}

	protected MediaModel createConfigFile()
	{
		CatalogUnawareMediaModel media = modelService.create(CatalogUnawareMediaModel.class);
		media.setCode("cockpitng-config");
		media.setMime("text/xml");
		modelService.save(media);
		return media;
	}
}