package com.epam.hybris.migration.executor;

/**
 * Executes automatic hybris migration.
 *
 * @author Sergey_Voronov
 */
public interface MigrationExecutor
{
	/**
	 * Performs migration according to migrationConfig.xml file.
	 *
	 * @param releaseDirectory
	 * 		name of a directory under MIGRATION_DIR containing migrationConfig.xml
	 */
	void execute(String releaseDirectory, String scope);

}
