/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package com.epam.hybris.migration.constants;

/**
 * Global class for all Migration constants. You can add global constants for your extension into this class.
 */
public final class MigrationConstants extends GeneratedMigrationConstants
{
	public static final String EXTENSIONNAME = "migration";
	public static final String RELEASE_PATTERN = "(.*?)(p\\d+\\.\\d+\\-v\\d+\\.\\d+)";

	private MigrationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
