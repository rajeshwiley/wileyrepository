package com.epam.hybris.migration.service.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.RemoveCatalogVersionCronJob;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.Initialization;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.cronjob.util.TypeRemovalUtil;
import de.hybris.platform.impex.jalo.exp.Export;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloAbstractTypeException;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.scripting.engine.ScriptExecutable;
import de.hybris.platform.scripting.engine.ScriptingLanguagesService;
import de.hybris.platform.scripting.engine.content.ScriptContent;
import de.hybris.platform.scripting.engine.content.impl.ResourceScriptContent;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.impex.ExportConfig;
import de.hybris.platform.servicelayer.impex.ExportResult;
import de.hybris.platform.servicelayer.impex.ExportService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.impex.impl.FileBasedImpExResource;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.JspContext;
import de.hybris.platform.util.Utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockJspWriter;
import org.springframework.util.CollectionUtils;
import org.springframework.util.FileCopyUtils;

import com.epam.hybris.migration.exception.MigrationException;
import com.epam.hybris.migration.model.MigrationReleaseModel;
import com.epam.hybris.migration.model.MigrationScriptModel;
import com.epam.hybris.migration.service.MigrationService;
import com.epam.hybris.migration.service.SqlScriptProcessor;
import com.google.common.collect.ImmutableMap;

import static de.hybris.platform.servicelayer.impex.ImportConfig.ValidationMode.RELAXED;
import static de.hybris.platform.servicelayer.impex.ImportConfig.ValidationMode.STRICT;


public class MigrationServiceImpl implements MigrationService
{
	public static final String MIGRATION_EXTENSION_NAME = "migration";
	private static final Logger LOG = Logger.getLogger(MigrationService.class);

	private static final int MAX_NUMBER_OF_CLEANUP_ATTEMPTS = 5;
	public static final String HYBRIS_BIN_DIR = "HYBRIS_BIN_DIR";
	public static final String MIGRATION_DIR = "MIGRATION_DIR";

	@Autowired
	private TypeService typeService;
	@Autowired
	private ImportService importService;
	@Autowired
	private ExportService exportService;
	@Autowired
	private ModelService modelService;
	@Autowired
	private ScriptingLanguagesService scriptingLanguagesService;
	@Autowired
	private CatalogService catalogService;
	@Autowired
	private SqlScriptProcessor sqlScriptProcessor;
	@javax.annotation.Resource(name = "migrationSetupSyncJobService")
	private SetupSyncJobService setupSyncJobService;
	@Autowired
	private CoreDataImportService coreDataImportService;

	private boolean supportBlockSql;

	@Required
	public void setSupportBlockSql(final boolean supportBlockSql)
	{
		this.supportBlockSql = supportBlockSql;
	}

	@Override
	public void importImpex(final String impexFilePath, boolean executeInLegacyMode, final boolean executeInRelaxedValidationMode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("impexFilePath", impexFilePath);

		LOG.info("Starting import. Impex file path: " + impexFilePath);

		final File impexFile = new File(impexFilePath);
		final FileBasedImpExResource resource = new FileBasedImpExResource(impexFile, CharEncoding.UTF_8);

		final ImportConfig importConfig = new ImportConfig();
		importConfig.setEnableCodeExecution(true);
		importConfig.setLegacyMode(executeInLegacyMode);
		importConfig.setValidationMode(executeInRelaxedValidationMode ? RELAXED : STRICT);
		importConfig.setScript(resource);

		final ImportResult importResult = importService.importData(importConfig);
		if (importResult.isError())
		{
			throw new MigrationException("Errors occurred while importing. See logs for more details.");
		}

		LOG.info("Impex import finished.");
	}

	@Override
	public void exportImpex(final String impexFilePath, final String exportDestDirPath, boolean exportInSingleFile)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("impexFilePath", impexFilePath);
		ServicesUtil.validateParameterNotNullStandardMessage("exportDestDirPath", exportDestDirPath);

		LOG.info("Starting export. Impex file path: " + impexFilePath);

		final File impexFile = new File(impexFilePath);
		final FileBasedImpExResource resource = new FileBasedImpExResource(impexFile, CharEncoding.UTF_8);
		final ExportConfig exportConfig = new ExportConfig();
		exportConfig.setScript(resource);
		exportConfig.setSingleFile(exportInSingleFile);

		final ExportResult exportResult = exportService.exportData(exportConfig);
		if (exportResult.isError())
		{
			throw new MigrationException("Errors occurred while exporting. See logs for more details.");
		}

		final File resultFile = ((Export) modelService.getSource(exportResult.getExport())).getExportedData().getFile();
		final String exportResultFileName;
		if (exportInSingleFile)
		{
			exportResultFileName = impexFile.getName().replace(".impex", "_result.impex");
		}
		else
		{
			exportResultFileName = impexFile.getName().replace(".impex", "_result.zip");
		}
		final File exportFile = new File(exportDestDirPath, exportResultFileName);
		try
		{
			FileCopyUtils.copy(resultFile, exportFile);
		}
		catch (IOException e)
		{
			throw new MigrationException("Errors occurred while exporting. See logs for more details.", e);
		}

		LOG.info("Impex export finished. Exported data path: " + exportDestDirPath);
	}

	@Override
	public void updateSystem(final Collection<String> extensionNames, boolean updateRunningSystem, boolean clearHmc,
			boolean loadEssentialData, boolean localizeTypes, final StringWriter logWriter)
	{
		LOG.info(buildUpdateSystemStartLogMessage(extensionNames, updateRunningSystem, clearHmc, loadEssentialData,
				localizeTypes));

		final MockJspWriter mockWriter = new MockJspWriter(logWriter);
		final MockHttpServletResponse response = new MockHttpServletResponse();
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.setParameters(buildRequestParams(extensionNames, updateRunningSystem, clearHmc, loadEssentialData,
				localizeTypes));

		updateSystemWithJspContext(new JspContext(mockWriter, request, response));

		LOG.info("Update finished successfully.");
	}

	@Override
	public void updateSystem(final String configFileName, final String releaseDir, final StringWriter logWriter)
	{
		LOG.info("Update system based on config-file: " + configFileName);

		final MockJspWriter mockWriter = new MockJspWriter(logWriter);
		final MockHttpServletResponse response = new MockHttpServletResponse();
		final MockHttpServletRequest request = new MockHttpServletRequest();
		for (Map.Entry<String, Object> configEntry : getConfigurationFromFile(configFileName, releaseDir).entrySet())
		{
			if (configEntry.getValue() instanceof List)
			{
				List paramList = (List) configEntry.getValue();
				request.addParameter((String) configEntry.getKey(), (String[]) paramList.toArray(new String[0]));
			}
			else
			{
				request.addParameter((String) configEntry.getKey(), (String) configEntry.getValue());
			}
		}

		updateSystemWithJspContext(new JspContext(mockWriter, request, response));

		LOG.info("Update finished successfully.");
	}

	private void updateSystemWithJspContext(final JspContext jspContext)
	{
		try
		{
			Initialization.doInitialize(jspContext);
		}
		catch (Exception e)
		{
			throw new MigrationException("Errors occurred while updating. See logs for more details.", e);
		}
	}

	@Override
	public void cleanUpTypes()
	{
		LOG.info("Starting to cleanup orphaned types.");

		boolean isAllTypesRemoved = false;
		for (int i = 0; !isAllTypesRemoved && i < MAX_NUMBER_OF_CLEANUP_ATTEMPTS; i++)
		{
			LOG.debug("Trying to remove orphaned types. Attempt number " + (i + 1) + " of " + MAX_NUMBER_OF_CLEANUP_ATTEMPTS);
			Map<String, String> types = TypeRemovalUtil.removeOrphanedTypes(true, true);

			isAllTypesRemoved = true;
			for (Map.Entry<String, String> entry : types.entrySet())
			{
				final boolean isRemoved = (entry.getValue() == null) ? Boolean.TRUE : Boolean.FALSE;
				if (!isRemoved)
				{
					isAllTypesRemoved = false;
					LOG.warn("Can't remove type: " + entry.getKey() + ". Error: " + entry.getValue());
				}
			}
		}

		if (!isAllTypesRemoved)
		{
			throw new MigrationException("Errors occurred while cleaning types. See logs for more details.");
		}

		LOG.info("Clean up finished.");
	}

	@Override
	public void syncronizeCatalog(final String catalogName)
	{
		LOG.info("Begin synchronizing catalog: " + catalogName);
		if (isCatalogValid(catalogName))
		{
			if (isContentCatalog(catalogName))
			{
				setupSyncJobService.createContentCatalogSyncJob(catalogName);
			}
			else
			{
				setupSyncJobService.createProductCatalogSyncJob(catalogName);
			}

			final PerformResult syncJobResult = setupSyncJobService.executeCatalogSyncJob(catalogName);
			if (CronJobStatus.FINISHED.equals(syncJobResult.getStatus()) && !CronJobResult.SUCCESS.equals(
					syncJobResult.getResult()))
			{
				throw new MigrationException("Fatal error during syncronization");
			}
			LOG.info("Synchronization completed. Catalog: " + catalogName);
		}
		else
		{
			LOG.error("Catalog does not exist: " + catalogName);
		}
	}

	@Override
	public void removeCatalog(final String catalogName)
	{
		LOG.info("Start removing catalog: " + catalogName);
		if (isCatalogValid(catalogName))
		{
			final RemoveCatalogVersionCronJob cronjob;
			try
			{
				cronjob = getCronJob();
				cronjob.setCatalog(CatalogManager.getInstance().getCatalog(catalogName));
				cronjob.getJob().perform(cronjob, true);
			}
			catch (JaloGenericCreationException | JaloAbstractTypeException e)
			{
				throw new MigrationException(
						String.format("Error during creation RemoveCatalogVersionJob for catalog %s", catalogName));
			}
			LOG.info(String.format("Catalog %s successfully removed", catalogName));
		}
		else
		{
			LOG.warn(String.format("Catalog does not exist: %s", catalogName));
		}
	}

	private boolean isContentCatalog(final String catalogName)
	{
		return catalogService.getCatalogForId(catalogName) instanceof ContentCatalogModel;
	}

	private boolean isCatalogValid(final String catalogName)
	{
		try
		{
			catalogService.getCatalogForId(catalogName);
			return true;
		}
		catch (SystemException ex)
		{
			return false;
		}
	}

	@Override
	public void executeSql(final String sqlFilePath)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("sqlFilePath", sqlFilePath);
		LOG.info("Starting SQL query execution. SQL file path: " + sqlFilePath);

		if (supportBlockSql)
		{
			sqlScriptProcessor.executeBlockSql(new File(sqlFilePath));
		}
		else
		{
			sqlScriptProcessor.executeSingleSql(new File(sqlFilePath));
		}
	}

	@Override
	public void executeGroovyScript(final String scriptFilePath)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("scriptFilePath", scriptFilePath);

		LOG.info("Starting Groovy script execution. Script file path: " + scriptFilePath);
		LOG.info(
				"Session: " + JaloSession.getCurrentSession().getClass() + " User: " + JaloSession.getCurrentSession().getUser());

		final Resource resource = new FileSystemResource(scriptFilePath);
		final ScriptContent scriptContent = new ResourceScriptContent(resource);
		final ScriptExecutable executable = scriptingLanguagesService.getExecutableByContent(scriptContent);

		executable.execute();

		LOG.info("Execution finished.");
	}

	@Override
	public Path getMigrationReleasePath(final String releaseDirectory)
	{
		final Path extensionPath = Utilities.getPlatformConfig().getExtensionInfo(MIGRATION_EXTENSION_NAME)
				.getExtensionDirectory().toPath();
		return extensionPath.resolve(Paths.get(Config.getString(MIGRATION_DIR, "/"), releaseDirectory));
	}

	private String buildUpdateSystemStartLogMessage(final Collection<String> extensionNames, boolean updateRunningSystem,
			boolean clearHmc, boolean loadEssentialData, boolean localizeTypes)
	{
		final StringBuilder logMsgBuilder = new StringBuilder();
		logMsgBuilder.append("Starting system update.").append('\n');
		if (!CollectionUtils.isEmpty(extensionNames))
		{
			logMsgBuilder.append('\t').append("Extensions: ").append(extensionNames).append('\n');
		}
		logMsgBuilder.append('\t').append("Update running system: ").append(updateRunningSystem).append('\n')
				.append('\t').append("Clear hmc: ").append(clearHmc).append('\n')
				.append('\t').append("Load essential data: ").append(loadEssentialData).append('\n')
				.append('\t').append("Localize types: ").append(localizeTypes).append('\n');

		return logMsgBuilder.toString();
	}

	private Map<String, String> buildRequestParams(final Collection<String> extensionNames, boolean updateRunningSystem,
			boolean clearHmc, boolean loadEssentialData, boolean localizeTypes)
	{
		final Map<String, String> requestParams = new HashMap();
		requestParams.put("init", "Go");
		if (updateRunningSystem)
		{
			requestParams.put("initmethod", "update");
		}
		if (clearHmc)
		{
			requestParams.put("clearhmc", Boolean.toString(clearHmc));
		}
		if (loadEssentialData)
		{
			requestParams.put("essential", Boolean.toString(loadEssentialData));
		}
		if (localizeTypes)
		{
			requestParams.put("localizetypes", Boolean.toString(localizeTypes));
		}

		if (extensionNames != null)
		{
			for (String extension : extensionNames)
			{
				requestParams.put(extension + "_sample", Boolean.toString(true));
			}
		}
		LOG.debug("Builded request params: " + requestParams);

		return requestParams;
	}

	private Map<String, Object> getConfigurationFromFile(final String configurationFileName, final String releaseDir)
	{
		if (StringUtils.isEmpty(configurationFileName))
		{
			throw new MigrationException("Configuration file passed for update has empty name");
		}

		Path pathToConfigFile = getMigrationReleasePath(releaseDir).resolve(Paths.get(configurationFileName)).toAbsolutePath();
		//Paths.get(configurationFileName, new String[0]).toAbsolutePath();
		try (InputStream configStream = Files.newInputStream(pathToConfigFile, new OpenOption[0]))
		{
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, Object> configMap = objectMapper.readValue(configStream, new TypeReference<Map<String, Object>>()
			{
			});
			return ImmutableMap.copyOf(configMap);
		}
		catch (IOException e)
		{
			LOG.error(e.getMessage(), e);
			throw new MigrationException(e);
		}
	}

	private Collection<String> getItemTypes()
	{
		Collection<String> itemTypes = new ArrayList<>();
		itemTypes.add(MigrationReleaseModel._TYPECODE);
		itemTypes.add(MigrationScriptModel._TYPECODE);
		return itemTypes;
	}

	private void checkTypesForCode(final Collection<String> itemTypes)
	{
		for (String type : itemTypes)
		{
			typeService.getTypeForCode(type);
		}
	}

	private RemoveCatalogVersionCronJob getCronJob() throws JaloGenericCreationException, JaloAbstractTypeException
	{
		final ComposedType comptype = JaloSession.getCurrentSession().getTypeManager()
				.getComposedType(RemoveCatalogVersionCronJob.class);
		RemoveCatalogVersionCronJob cronjob = null;
		final HashMap params = new HashMap();
		params.put(CronJob.JOB, CatalogManager.getInstance().getOrCreateDefaultRemoveCatalogVersionJob());
		cronjob = (RemoveCatalogVersionCronJob) comptype.newInstance(params);
		return cronjob;
	}

	public void updateRunningSystemIfNecessary()
	{
		Collection<String> itemTypes = getItemTypes();
		try
		{
			checkTypesForCode(itemTypes);
		}
		catch (UnknownIdentifierException e)
		{
			updateSystem(new ArrayList<String>(), true, false, false, false, null);
			checkTypesForCode(itemTypes);
		}
	}
	
	@Override
	public void runSolrIndex(final List<String> stores)
	{
		LOG.info("Run solr indexing for stores " + stores);
		stores.forEach(s -> coreDataImportService.runSolrIndex(null, s));
	}
}
