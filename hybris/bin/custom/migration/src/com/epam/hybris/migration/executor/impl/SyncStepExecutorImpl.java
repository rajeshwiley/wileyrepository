package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dto.SyncStep;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationService;


public class SyncStepExecutorImpl implements MigrationStepExecutor
{

	@Autowired
	protected MigrationService migrationService;

	@Override
	public void execute(final JAXBElement migrationStep, final String releaseDir, final String scope)
	{
		SyncStep syncStep = (SyncStep) migrationStep.getValue();
		if (syncStep.getCatalog() != null)
		{
			migrationService.syncronizeCatalog(syncStep.getCatalog());
		}
	}
}

