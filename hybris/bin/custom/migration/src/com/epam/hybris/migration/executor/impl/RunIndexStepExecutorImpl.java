package com.epam.hybris.migration.executor.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dto.RunIndexStep;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationService;

import de.hybris.platform.servicelayer.util.ServicesUtil;


public class RunIndexStepExecutorImpl implements MigrationStepExecutor
{

	private static final String SEPARATOR = ",";
	@Autowired
	private MigrationService migrationService;

	@Override
	public void execute(final JAXBElement migrationStep, final String releaseDir, final String scope)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("migrationStep", migrationStep);
		RunIndexStep runIndexStep = (RunIndexStep) migrationStep.getValue();
		ServicesUtil.validateParameterNotNullStandardMessage("runIndexStep.stores", runIndexStep.getStores());
		migrationService.runSolrIndex(parseStores(runIndexStep.getStores()));
	}

	private List<String> parseStores(final String storesValue)
	{
		String[] stores = StringUtils.split(storesValue, SEPARATOR);
		Stream<String> storesStream = Arrays.asList(stores).stream();
		return storesStream.map(String::trim).collect(Collectors.toList());
	}

}
