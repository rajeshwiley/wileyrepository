package com.epam.hybris.migration.service;

import java.util.List;

import com.epam.hybris.migration.model.MigrationReleaseModel;


/**
 * <p>
 * interface of service for working with database migration releases
 * </p>
 *
 * Created by Pavel_Maximov on 9/18/2015.
 */
public interface MigrationReleaseService
{
	/**
	 * <p>
	 * return all release of database migration
	 * </p>
	 *
	 * @return list of all database migration releases
	 */

	List<MigrationReleaseModel> getAllMigrationRelease();
	
	/**
	 * <p>
	 * save new release of database migration
	 * </p>
	 *
	 * @param releaseVersion
	 * 		new/old database migration release
	 * @param scope
	 * 		scope of release
	 */
	void insertUpdateMigrationRelease(String releaseVersion, String scope, boolean completed);

	/**
	 * Checks if it is possible to migrate given current release
	 * Logic is based on the rule, that we cannot migrate version lower, than the highest already migrated release.
	 * If you need to know if concrete version/scope has been physically migrated, call isReleaseExecuted()
	 *
	 * @param currentRelease
	 * 		Release version to check
	 * @param scope
	 * 		Scope to check
	 * @return True, if release cannot be migrated.
	 */
	boolean isReleaseMigrated(String currentRelease, String scope);

	/**
	 * Checks if the release with given version/scope has been physically excuted and has completed status
	 *
	 * @param currentRelease
	 * 		Release version to check
	 * @param scope
	 * 		Scope to check
	 * @return True, if there is certain MigrationRelease item in the database
	 */
	boolean isReleaseExecuted(String currentRelease, String scope);

	void insertUpdateMigrationScript(String releaseVersion, String scope, String scriptName);

	boolean isScriptExecuted(String releaseVersion, String scope, String scriptName);

}
