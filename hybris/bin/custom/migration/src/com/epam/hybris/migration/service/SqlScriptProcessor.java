package com.epam.hybris.migration.service;

import java.io.File;


public interface SqlScriptProcessor
{
	/**
	 * Executes script with single SQL-statement via JDBC PrepareStatement.
	 * Can execute DDL, DML. Cannot execute SELECT queries
	 *
	 * @param sqlFile
	 * 		File on disk containing script-text
	 */
	void executeSingleSql(File sqlFile);

	/**
	 * Executes script with multiple SQL-statements, separated by ";" or new line
	 * Supports comments like --
	 *
	 * @param sqlFile
	 * 		File on disk containing script-text
	 */
	void executeBlockSql(File sqlFile);
}
