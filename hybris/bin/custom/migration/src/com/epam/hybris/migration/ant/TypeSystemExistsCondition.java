package com.epam.hybris.migration.ant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.condition.Condition;


public class TypeSystemExistsCondition implements Condition
{

	private String typeSystemName;
	private String dbUrl;
	private String dbUsername;
	private String dbPassword;
	private String dbDriver;

	public void setDbPassword(final String dbPassword)
	{
		this.dbPassword = dbPassword;
	}

	public void setDbUrl(final String dbUrl)
	{
		this.dbUrl = dbUrl;
	}

	public void setDbUsername(final String dbUsername)
	{
		this.dbUsername = dbUsername;
	}

	public void setDbDriver(final String dbDriver)
	{
		this.dbDriver = dbDriver;
	}

	public void setTypeSystemName(final String typeSystemName)
	{
		this.typeSystemName = typeSystemName;
	}

	@Override
	public boolean eval() throws BuildException
	{
		boolean typeSystemExists;
		try
		{
			Class.forName(dbDriver);
		}
		catch (ClassNotFoundException e)
		{
			throw new BuildException("Unable to load DB driver", e);
		}

		try (Connection con = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
				PreparedStatement ps = con.prepareStatement(createSqlQuery());
				ResultSet rs = ps.executeQuery())
		{
			//check if at least one result is returned
			typeSystemExists = rs.next();
		}
		catch (SQLException e)
		{
			throw new BuildException("Unable to check typesystem presence", e);
		}

		return typeSystemExists;
	}

	private String createSqlQuery() {
		return String.format("SELECT DISTINCT TypeSystemName FROM ydeployments WHERE TypeSystemName = '%s'", typeSystemName);
	}
}
