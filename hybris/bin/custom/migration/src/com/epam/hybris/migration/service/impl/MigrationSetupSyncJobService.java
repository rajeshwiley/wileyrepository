package com.epam.hybris.migration.service.impl;

import de.hybris.platform.catalog.jalo.SyncItemCronJob;
import de.hybris.platform.catalog.jalo.SyncItemJob;
import de.hybris.platform.commerceservices.setup.impl.DefaultSetupSyncJobService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


public class MigrationSetupSyncJobService extends DefaultSetupSyncJobService
{
	private static final Logger LOG = Logger.getLogger(DefaultSetupSyncJobService.class);

	@Autowired
	private UserService userService;
	@Autowired
	private ModelService modelService;

	@Override
	protected SyncItemCronJob getLastFailedSyncCronJob(final SyncItemJob syncItemJob)
	{
		SyncItemCronJob result = super.getLastFailedSyncCronJob(syncItemJob);
		if (result.getSessionUser() == null)
		{
			result.setSessionUser(modelService.toPersistenceLayer(userService.getAdminUser()));
		}
		return result;
	}

	/**
	 * OOTB_1808
	 * @param catalogId
	 * @return
	 */
	@Override
	public PerformResult executeCatalogSyncJob(final String catalogId)
	{
		final SyncItemJob catalogSyncJob = getCatalogSyncJob(catalogId);
		if (catalogSyncJob == null)
		{
			LOG.error("Couldn't find 'SyncItemJob' for catalog [" + catalogId + "]", null);
			return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.UNKNOWN);
		}
		else
		{
			final SyncItemCronJob syncJob = getLastFailedSyncCronJob(catalogSyncJob);
			syncJob.setLogToDatabase(false);
			// temporary change to debug synchronization issues
			syncJob.setLogToFile(true);
			syncJob.setForceUpdate(false);

			LOG.info("Created cronjob [" + syncJob.getCode() + "] to synchronize catalog [" + catalogId
					+ "] staged to online version.");

			syncJob.setConfigurator(new FullSyncConfigurator(catalogSyncJob));

			LOG.info("Starting synchronization, this may take a while ...");

			catalogSyncJob.perform(syncJob, true);

			LOG.info("Synchronization complete for catalog [" + catalogId + "]");

			final CronJobResult result = modelService.get(syncJob.getResult());
			final CronJobStatus status = modelService.get(syncJob.getStatus());
			return new PerformResult(result, status);
		}
	}
}
