package com.epam.hybris.migration.service.impl;

import com.epam.hybris.migration.constants.MigrationConstants;
import com.epam.hybris.migration.exception.MigrationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ReleaseVersionTreatmentStrategy
{
	private static final int RELEASE_VERSION_GROUP_NUMBER = 2;
	private static final String COMMON_RELEASE_NAME = "common";

	public String getNormalizedRelease(final String path)
	{
		if (COMMON_RELEASE_NAME.equals(path)) {
			return path;
		}

		Pattern pattern = Pattern.compile(MigrationConstants.RELEASE_PATTERN);
		Matcher matcher = pattern.matcher(path);
		if (!matcher.matches())
		{
			throw new MigrationException("Migration release name does not fit the pattern");
		}

		return matcher.group(RELEASE_VERSION_GROUP_NUMBER);
	}

}
