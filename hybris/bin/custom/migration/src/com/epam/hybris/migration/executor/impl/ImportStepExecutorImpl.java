package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import com.epam.hybris.migration.dto.ImportStep;
import com.epam.hybris.migration.dto.ScriptStep;


public class ImportStepExecutorImpl extends AbstractScriptStepExecutor
{

	@Override
	protected void executeFile(final String fileName, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir,
			final boolean isRelaxedValidationMode)
	{
		String filePath = getFilePath(fileName, releaseDir, jaxbStep.getName().toString());
		ImportStep importStep = (ImportStep) jaxbStep.getValue();

		migrationService.importImpex(filePath, importStep.isLegacyMode(), isRelaxedValidationMode);
	}
}
