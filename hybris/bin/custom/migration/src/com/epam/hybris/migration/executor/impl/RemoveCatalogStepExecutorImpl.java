package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dto.RemoveCatalogStep;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationService;


/**
 * Created by Raman_Hancharou on 3/21/2017.
 */
public class RemoveCatalogStepExecutorImpl implements MigrationStepExecutor
{
	@Autowired
	protected MigrationService migrationService;

	@Override
	public void execute(final JAXBElement migrationStep, final String releaseDir, final String scope)
	{
		RemoveCatalogStep removeCatalogStep = (RemoveCatalogStep) migrationStep.getValue();
		if (removeCatalogStep.getCatalog() != null)
		{
			migrationService.removeCatalog(removeCatalogStep.getCatalog());
		}
	}
}
