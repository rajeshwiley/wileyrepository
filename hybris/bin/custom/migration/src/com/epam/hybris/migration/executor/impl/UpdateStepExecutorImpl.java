package com.epam.hybris.migration.executor.impl;

import java.util.Collection;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dto.ProjectData;
import com.epam.hybris.migration.dto.UpdateStep;
import com.epam.hybris.migration.executor.MigrationStepExecutor;
import com.epam.hybris.migration.service.MigrationService;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;


public class UpdateStepExecutorImpl implements MigrationStepExecutor
{

	@Autowired
	private MigrationService migrationService;

	@Override
	public void execute(final JAXBElement jaxbStep, final String releaseDir, final String scope)
	{
		UpdateStep updateStep = (UpdateStep) jaxbStep.getValue();

		if (StringUtils.isNotEmpty(updateStep.getConfigFile()))
		{
			updateFromFile(updateStep, releaseDir);
		}
		else
		{
			updateFromParameters(updateStep);
		}
	}

	private void updateFromFile(final UpdateStep updateStep, final String releaseDir)
	{
		migrationService.updateSystem(updateStep.getConfigFile(), releaseDir, null);
	}

	private void updateFromParameters(final UpdateStep updateStep)
	{
		Collection<String> extensionNames = Collections2.transform(updateStep.getProjectData(),
				new Function<ProjectData, String>()
				{
					@Override
					public String apply(final ProjectData projectData)
					{
						return projectData.getExtension();
					}
				});
		migrationService.updateSystem(extensionNames, updateStep.isRunningSystem(), updateStep.isHmcConfig(),
				updateStep.isEssentialData(), updateStep.isLocalizeTypes(), null);
	}

}
