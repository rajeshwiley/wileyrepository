package com.epam.hybris.migration.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.hybris.migration.dao.MigrationReleaseDao;
import com.epam.hybris.migration.model.MigrationReleaseModel;
import com.epam.hybris.migration.model.MigrationScriptModel;


/**
 * <p>
 * Implementation of MigrationReleaseDao
 * </p>
 *
 */
public class MigrationReleaseDaoImpl implements MigrationReleaseDao
{

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<MigrationReleaseModel> findAllCompletedMigrationReleases()
	{
		String query = "SELECT {" + MigrationReleaseModel.PK + "} "
				+ "FROM {" + MigrationReleaseModel._TYPECODE + "} "
				+ "WHERE {" + MigrationReleaseModel.COMPLETED + "}= ?" + MigrationReleaseModel.COMPLETED
				+ " ORDER BY {" + MigrationReleaseModel.DATE + "} DESC";

		FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter(MigrationReleaseModel.COMPLETED, Boolean.TRUE);
		SearchResult<MigrationReleaseModel> searchResult = flexibleSearchService.search(searchQuery);
		List<MigrationReleaseModel> allMigrationRelease = searchResult.getResult();

		return allMigrationRelease;
	}

	@Override
	public List<MigrationReleaseModel> findByVersionAndScope(final String releaseVersion, final String scope)
	{
		String query = "SELECT {" + MigrationReleaseModel.PK + "} "
				+ "FROM {" + MigrationReleaseModel._TYPECODE + "} "
				+ "WHERE {" + MigrationReleaseModel.RELEASE + "}= ?" + MigrationReleaseModel.RELEASE
				+ " AND {" + MigrationReleaseModel.SCOPE + "}= ?" + MigrationReleaseModel.SCOPE;

		FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter(MigrationReleaseModel.RELEASE, releaseVersion);
		searchQuery.addQueryParameter(MigrationReleaseModel.SCOPE, scope);
		SearchResult<MigrationReleaseModel> searchResult = flexibleSearchService.search(searchQuery);
		List<MigrationReleaseModel> foundMigrationRelease = searchResult.getResult();

		return foundMigrationRelease;
	}

	@Override
	public List<MigrationReleaseModel> findCompletedByVersion(final String releaseVersion)
	{
		String query = "SELECT {" + MigrationReleaseModel.PK + "} "
				+ "FROM {" + MigrationReleaseModel._TYPECODE + "} "
				+ "WHERE {" + MigrationReleaseModel.RELEASE + "}= ?" + MigrationReleaseModel.RELEASE
				+ " AND {" + MigrationReleaseModel.COMPLETED + "}= ?" + MigrationReleaseModel.COMPLETED;

		FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter(MigrationReleaseModel.RELEASE, releaseVersion);
		searchQuery.addQueryParameter(MigrationReleaseModel.COMPLETED, Boolean.TRUE);
		SearchResult<MigrationReleaseModel> searchResult = flexibleSearchService.search(searchQuery);
		List<MigrationReleaseModel> foundMigrationRelease = searchResult.getResult();

		return foundMigrationRelease;
	}

	@Override
	public List<MigrationReleaseModel> findLastMigrationRelease()
	{
		List<MigrationReleaseModel> lastMigrationRelease = new ArrayList<>();
		List<MigrationReleaseModel> list = findAllCompletedMigrationReleases();

		if (CollectionUtils.isNotEmpty(list))
		{
			lastMigrationRelease.add(list.get(0));
		}

		return lastMigrationRelease;
	}

	@Override
	public List<MigrationScriptModel> findScriptByNameAndRelease(final String scriptName,
			final MigrationReleaseModel migrationRelease)
	{
		String query = "SELECT {" + MigrationScriptModel.PK + "} "
				+ "FROM {" + MigrationScriptModel._TYPECODE + "} "
				+ "WHERE {" + MigrationScriptModel.MIRATIONRELEASE + "}= ?" + MigrationScriptModel.MIRATIONRELEASE
				+ " AND {" + MigrationScriptModel.SCRIPTNAME + "}= ?" + MigrationScriptModel.SCRIPTNAME;

		FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		searchQuery.addQueryParameter(MigrationScriptModel.SCRIPTNAME, scriptName);
		searchQuery.addQueryParameter(MigrationScriptModel.MIRATIONRELEASE, migrationRelease);
		SearchResult<MigrationScriptModel> searchResult = flexibleSearchService.search(searchQuery);
		List<MigrationScriptModel> foundMigrationRelease = searchResult.getResult();

		return foundMigrationRelease;
	}

}
