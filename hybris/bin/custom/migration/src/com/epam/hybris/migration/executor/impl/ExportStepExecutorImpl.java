package com.epam.hybris.migration.executor.impl;

import javax.xml.bind.JAXBElement;

import com.epam.hybris.migration.dto.ExportStep;
import com.epam.hybris.migration.dto.ScriptStep;


public class ExportStepExecutorImpl extends AbstractScriptStepExecutor
{
	private static final String IMPORT_TYPE = "import";

	@Override
	protected void executeFile(final String fileName, final JAXBElement<ScriptStep> jaxbStep, final String releaseDir,
			final boolean isRelaxedValidationMode)
	{
		String sourcePath = getFilePath(fileName, releaseDir, jaxbStep.getName().toString());
		String destinationPath = getStepDirPath(releaseDir, IMPORT_TYPE);
		ExportStep exportStep = (ExportStep) jaxbStep.getValue();

		migrationService.exportImpex(sourcePath, destinationPath, exportStep.isExportInSingleFile());
	}

	@Override
	/** Export implementation ignores any rollback-related attributes */
	protected boolean isTransactionAware()
	{
		return false;
	}
}
