package de.hybris.platform.couponservices.validation.constraints;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern.Flag;

import org.hibernate.validator.internal.util.logging.Log;
import org.hibernate.validator.internal.util.logging.LoggerFactory;


public class ObjectPatternValidator implements ConstraintValidator<ObjectPattern, Object>
{
	private static final Log LOG = LoggerFactory.make();
	private Pattern pattern;

	public ObjectPatternValidator()
	{
	}

	public void initialize(final ObjectPattern parameters)
	{
		Flag[] flags = parameters.flags();
		int intFlag = 0;
		Flag[] var7 = flags;
		int var6 = flags.length;

		for (int var5 = 0; var5 < var6; ++var5)
		{
			Flag flag = var7[var5];
			intFlag |= flag.getValue();
		}

		try
		{
			this.pattern = Pattern.compile(parameters.regexp(), intFlag);
		}
		catch (PatternSyntaxException var8)
		{
			throw LOG.getInvalidRegularExpressionException(var8);
		}
	}

	public boolean isValid(final Object value, final ConstraintValidatorContext constraintValidatorContext)
	{
		if (value == null)
		{
			return true;
		}
		else
		{
			Matcher m = this.pattern.matcher(value.toString());
			return m.matches();
		}
	}
}