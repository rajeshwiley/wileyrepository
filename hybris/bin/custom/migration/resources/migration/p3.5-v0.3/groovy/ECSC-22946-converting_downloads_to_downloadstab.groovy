import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import groovy.transform.Field
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.apache.log4j.Logger;

@Field
Logger LOG = Logger.getLogger(this.getClass().getName());
@Field
int numberOfProductsForRequest = 100;
@Field
String htmlTableTemplate = "<table>downloadsTrToReplace</table>";
@Field
String htmlTrTemplate = "<tr><td>downloadsTitleToReplace</td><td><a href=\"downloadsLinkToReplace\" target=\"_blank\">Download</a></td></tr>";
@Field
final Locale enLocale = commonI18NService.getLocaleForIsoCode("en");

final FlexibleSearchQuery flexibleSearchQueryForProducts = getFlexibleSearchQueryForProducts();

List<ProductModel> result = flexibleSearchService.<ProductModel> search(flexibleSearchQueryForProducts).getResult();
while(CollectionUtils.isNotEmpty(result))
{
    updateDownloadsTabForProducts(result);
	result = flexibleSearchService.<ProductModel> search(flexibleSearchQueryForProducts).getResult();
}
	
def FlexibleSearchQuery getFlexibleSearchQueryForProducts()
{
	String productsSearchQuery = "SELECT {p:" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + " as p} WHERE {p:" + ProductModel.DOWNLOADS + "} IS NOT NULL AND {p:" + ProductModel.DOWNLOADSTAB + "} IS NULL AND {p:" + ProductModel.CATALOGVERSION + "}=?catalogVersion";
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(productsSearchQuery);

		flexibleSearchQuery.addQueryParameter("catalogVersion", catalogVersionService.getCatalogVersion("wileyProductCatalog", "Staged"));
		flexibleSearchQuery.setDisableCaching(true);
		flexibleSearchQuery.setResultClassList(Arrays.asList(ProductModel.class));
		flexibleSearchQuery.setCount(numberOfProductsForRequest);
		return flexibleSearchQuery;
}
		
def void updateDownloadsTabForProducts(final List<ProductModel> result)
	{
		for (ProductModel product : result)
		{
			LOG.info("\"Article number\" of current product is: " + product.getCode())
			final List<MediaContainerModel> downloads = product.getDownloads();
			String html = getConvertedDownloadsToHtml(downloads);
			if (StringUtils.isNotBlank(html))
			{
				product.setDownloadsTab(html);
			}
		}
		modelService.saveAll();
	}

def String getConvertedDownloadsToHtml(final List<MediaContainerModel> downloads)
	{
		String html = "";
		for (MediaContainerModel container : downloads)
		{
			String downloadTitle = container.getName(enLocale);
			String downloadLink = getDownloadLink(container);
			if(StringUtils.isEmpty(downloadTitle) || StringUtils.isEmpty(downloadLink)) {
                continue;
            }
			html = html + htmlTrTemplate.replace("downloadsTitleToReplace", downloadTitle).replace("downloadsLinkToReplace", downloadLink);
		}
		if (StringUtils.isNotBlank(html))
		{
		    html = htmlTableTemplate.replace("downloadsTrToReplace", html);
		}
		return html;
	}

def String getDownloadLink(final MediaContainerModel container)
	{
		List<MediaModel> medias = container.getMedias();
		if(CollectionUtils.isEmpty(medias)) {
			return null;
		}
		//One MediaContainer must contains only one Media. It has been checked for prod.
		final MediaModel media = medias.iterator().next();
		String downloadLink = media.getURL();
		return downloadLink;
	}