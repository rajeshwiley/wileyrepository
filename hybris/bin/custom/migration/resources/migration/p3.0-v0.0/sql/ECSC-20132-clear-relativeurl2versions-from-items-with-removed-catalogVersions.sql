--- This script removes items from relativeurl2versions which have reference to not existing catalogVersions ---

--- Such situation is possible if simply remove catalogVersion. Reference in the relation table would be still existed. 
--- During incremental update process we import wileycomRelativeUrlCronJob (wileycominitialdata/import/coredata/stores/wileyb2c/jobs.impex).
--- If after this impex we stop process, remove any contentCatalog and rerun incremental update it would fail
 
SET SQL_SAFE_UPDATES = 0;
DELETE FROM relativeurl2versions WHERE TargetPK NOT IN (SELECT PK FROM catalogversions);
