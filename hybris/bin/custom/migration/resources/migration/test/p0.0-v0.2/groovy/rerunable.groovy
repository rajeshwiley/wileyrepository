import de.hybris.platform.core.model.user.UserModel
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.user.UserService

def userUid = "rerunable";
def testString = "nextrun";

ModelService modelService = spring.getBean("modelService");
UserService userService = spring.getBean("userService");

try {
    UserModel user = userService.getUserForUID(userUid);
    logExecution(user, testString, modelService);
} catch (UnknownIdentifierException ex) {
    UserModel user = modelService.create(UserModel.class);
    user.setUid(userUid);
    user.setDomain("");
    logExecution(user, testString, modelService);
}

private void logExecution(UserModel user, String testString, ModelService modelService) {
    user.setDomain(user.getDomain() + testString + "_");
    modelService.save(user);
}

