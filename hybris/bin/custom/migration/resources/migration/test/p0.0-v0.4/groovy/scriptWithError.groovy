import com.google.common.collect.Sets
import de.hybris.platform.catalog.CatalogVersionService
import de.hybris.platform.catalog.jalo.CatalogManager
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.category.CategoryService
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.product.ProductService
import de.hybris.platform.servicelayer.model.ModelService

ModelService modelService = (ModelService) spring.getBean("modelService");
ProductService productService = (ProductService) spring.getBean("productService");
CategoryService categoryService = (CategoryService) spring.getBean("categoryService");
CatalogVersionService cvService = (CatalogVersionService) spring.getBean("catalogVersionService");

CatalogVersionModel catVer = cvService.getCatalogVersion("testProductCatalog", CatalogManager.OFFLINE_VERSION);
CategoryModel category2 = categoryService.getCategory(catVer, "category2");

CategoryModel category3 = modelService.create(CategoryModel.class);
category3.setCode("category3");
category3.setCatalogVersion(catVer);
modelService.save(category3);

ProductModel product1 = productService.getProductForCode(catVer, "product1");
product1.setSupercategories(Sets.newHashSet(category2));

ProductModel product2 = productService.getProductForCode(catVer, "product2");
product2.setSupercategories(Sets.newHashSet(category3));

modelService.saveAll(product1, product2);

throw new RuntimeException("Something went wrong");