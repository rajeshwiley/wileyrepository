import com.google.common.collect.Sets
import de.hybris.platform.catalog.CatalogVersionService
import de.hybris.platform.catalog.jalo.CatalogManager
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.category.CategoryService
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.product.ProductService
import de.hybris.platform.servicelayer.model.ModelService

ModelService modelService = (ModelService) spring.getBean("modelService");
ProductService productService = (ProductService) spring.getBean("productService");
CategoryService categoryService = (CategoryService) spring.getBean("categoryService");
CatalogVersionService cvService = (CatalogVersionService) spring.getBean("catalogVersionService");

CatalogVersionModel catVer = cvService.getCatalogVersion("testProductCatalog", CatalogManager.OFFLINE_VERSION);
CategoryModel category1 = categoryService.getCategory(catVer, "category1");

CategoryModel category2 = modelService.create(CategoryModel.class);
category2.setCode("category2");
category2.setCatalogVersion(catVer);
modelService.save(category2);

ProductModel product1 = productService.getProductForCode(catVer, "product1");
product1.setSupercategories(Sets.newHashSet(category1, category2));

ProductModel product2 = modelService.create(ProductModel.class);
product2.setCode("product2");
product2.setCatalogVersion(catVer);
product2.setSupercategories(Sets.newHashSet(category1));

modelService.saveAll(product1, product2);