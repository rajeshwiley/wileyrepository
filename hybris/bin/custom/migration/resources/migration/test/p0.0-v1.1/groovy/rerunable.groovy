import de.hybris.platform.core.model.user.UserModel
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.user.UserService
import org.slf4j.Logger
import org.slf4j.LoggerFactory

def userUid = "rerunable";
def testString = "nextrun";
def logger = LoggerFactory.getLogger("TestRerunnableGroovy");

ModelService modelService = spring.getBean("modelService");
UserService userService = spring.getBean("userService");

try {
    UserModel user = userService.getUserForUID(userUid);
    logExecution(user, testString, modelService);
} catch (UnknownIdentifierException ex) {
    UserModel user = modelService.create(UserModel.class);
    user.setUid(userUid);
    user.setDomain("");
    logExecution(user, testString, modelService);
}

private void logExecution(UserModel user, String testString, ModelService modelService) {
    String newDomain = user.getDomain() + testString + "_";
    user.setDomain(newDomain);
    modelService.save(user);
    modelService.refresh(user);
    Logger logger = LoggerFactory.getLogger("TestRerunnableGroovy");
    logger.info("UserId=" + user.getUid() + "  intented domain=" + newDomain + " updated domain=" + user.getDomain());
}

