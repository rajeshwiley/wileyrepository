#
# Import the CMS content for the WEL site
#
$contentCatalog = welContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]
$contentCVOnline = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Online])[default = $contentCatalog:Online]
$addonExtensionName = assistedservicestorefront

$productCatalog = welProductCatalog
$productCatalogName = WEL Product Catalog
$productCV = catalogVersion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]

# Import modulegen config properties into impex macros
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$jarResourceCms = $config-jarResourceCmsValue

#page template : Stage

INSERT_UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; SimpleProductListPageTemplate        ; Simple Product List Page Template         ; category/simpleProductListPage    ; CategoryPage

UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; SimpleProductListPageTemplate        ; $jarResourceCms/structure-view/wel/structure_simpleProductListPageTemplate.vm

# Product List Simple Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'SimpleProductListPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo                ; ; SimpleResponsiveBannerComponent
; BrandLogo               ; ; SimpleResponsiveBannerComponent
; NavigationBar           ; ;                            ; navigation
; LiveChat                ; ;                            ; navigation
; CategoryNavigation      ; ;                            ; wide
; SocialLinksSlot         ; ; SocialMediaFollowComponent ;
; SimpleProductsTitle     ; ; CMSParagraphComponent      ; wide
; SimpleProductsList      ; ; JspIncludeComponent        ; wide
; Footer                  ; ;                            ; footer
; CompanyLogo             ; ; SimpleResponsiveBannerComponent
; CurrencySelector        ; ;                            ;
; TopHeaderSlot           ; ;                            ; wide

# Create Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; SimpleProductsTitleSlot         ; Simple Products Title Slot          ; true
; ; SimpleProductsListSlot          ; Simple Product List Slot            ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'SimpleProductListPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-SimpleProductListPage           ; SiteLogo           ; ; SiteLogoSlot            ; true
; ; BrandLogo-SimpleProductListPage          ; BrandLogo          ; ; BrandLogoSlot           ; true
; ; NavigationBar-SimpleProductListPage      ; NavigationBar      ; ; NavigationBarSlot       ; true
; ; LiveChat-SimpleProductListPage           ; LiveChat           ; ; LiveChatNavigationSlot  ; true
; ; CategoryNavigation-SimpleProductListPage ; CategoryNavigation ; ; CategoryNavigationSlot  ; true
; ; ProductsTitle-SimpleProductListPage      ; ProductsTitle      ; ; SimpleProductsTitleSlot ; true
; ; ProductsList-SimpleProductListPage       ; ProductsList       ; ; SimpleProductsListSlot  ; true
; ; Footer-SimpleProductListPage             ; Footer             ; ; FooterSlot              ; true
; ; CompanyLogo-SimpleProductListPage        ; CompanyLogo        ; ; CompanyLogoSlot         ; true
; ; CurrencySelector-SimpleProductListPage   ; CurrencySelector   ; ; CurrencySelectorSlot    ; true
; ; TopHeaderSlot-SimpleProductListPage      ; TopHeaderSlot      ; ; TopHeaderSlot           ; true

#page template : Online

INSERT_UPDATE PageTemplate; $contentCVOnline[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; SimpleProductListPageTemplate        ; Simple Product List Page Template         ; category/simpleProductListPage    ; CategoryPage

UPDATE PageTemplate; $contentCVOnline[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; SimpleProductListPageTemplate        ; $jarResourceCms/structure-view/wel/structure_simpleProductListPageTemplate.vm

# Product List Simple Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCVOnline)[unique = true][default = 'SimpleProductListPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo                ; ; SimpleResponsiveBannerComponent
; BrandLogo               ; ; SimpleResponsiveBannerComponent
; NavigationBar           ; ;                            ; navigation
; LiveChat                ; ;                            ; navigation
; CategoryNavigation      ; ;                            ; wide
; SocialLinksSlot         ; ; SocialMediaFollowComponent ;
; SimpleProductsTitle     ; ; CMSParagraphComponent      ; wide
; SimpleProductsList      ; ; JspIncludeComponent        ; wide
; Footer                  ; ;                            ; footer
; CompanyLogo             ; ; SimpleResponsiveBannerComponent
; CurrencySelector        ; ;                            ;
; TopHeaderSlot           ; ;                            ; wide

# Create Content Slots
INSERT_UPDATE ContentSlot; $contentCVOnline[unique = true]; uid[unique = true]; name; active
; ; SimpleProductsTitleSlot         ; Simple Products Title Slot          ; true
; ; SimpleProductsListSlot          ; Simple Product List Slot            ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCVOnline[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCVOnline)[unique = true][default = 'SimpleProductListPageTemplate']; contentSlot(uid, $contentCVOnline)[unique = true]; allowOverwrite
; ; SiteLogo-SimpleProductListPage           ; SiteLogo           ; ; SiteLogoSlot            ; true
; ; BrandLogo-SimpleProductListPage          ; BrandLogo          ; ; BrandLogoSlot           ; true
; ; NavigationBar-SimpleProductListPage      ; NavigationBar      ; ; NavigationBarSlot       ; true
; ; LiveChat-SimpleProductListPage           ; LiveChat           ; ; LiveChatNavigationSlot  ; true
; ; CategoryNavigation-SimpleProductListPage ; CategoryNavigation ; ; CategoryNavigationSlot  ; true
; ; ProductsTitle-SimpleProductListPage      ; ProductsTitle      ; ; SimpleProductsTitleSlot ; true
; ; ProductsList-SimpleProductListPage       ; ProductsList       ; ; SimpleProductsListSlot  ; true
; ; Footer-SimpleProductListPage             ; Footer             ; ; FooterSlot              ; true
; ; CompanyLogo-SimpleProductListPage        ; CompanyLogo        ; ; CompanyLogoSlot         ; true
; ; CurrencySelector-SimpleProductListPage   ; CurrencySelector   ; ; CurrencySelectorSlot    ; true
; ; TopHeaderSlot-SimpleProductListPage      ; TopHeaderSlot      ; ; TopHeaderSlot           ; true

#Page
INSERT_UPDATE CategoryPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); defaultPage; approvalStatus(code)[default = 'approved']
; ; simpleProductList ; Simple Product List   ; SimpleProductListPageTemplate ; false ;

INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; SimpleProductsListComponent ; Simple Prosucts List Component ; simpleGridProductListPage.jsp ; ; SimpleProductsListComponent

INSERT_UPDATE CMSCategoryRestriction; $contentCV[unique = true]; uid[unique = true]; name; categories(code, $productCV); recursive; pages(uid,$contentCV )
; ; WEL-SimpleProductsListPageRestriction ; Restriction for Simple products list page ; WEL_FINRA_CATEGORY ; true; simpleProductList

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef; content[lang = en]
; ; ProductsTitleComponentFINRA ; Products Title for FINRA ; ProductsTitleComponentFINRA ; <h2 class='comparison-table-title'>FINRA Products</h2>

INSERT_UPDATE CMSCategoryRestriction; $contentCV[unique = true]; uid[unique = true]; name; categories(code, $productCV); components(uid, $contentCV); recursive
; ; FINRA-SimpleProductListTitleRestriction ; Restriction for FINRA simple products list title ; WEL_FINRA_CATEGORY ; ProductsTitleComponentFINRA ; true

# Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; cmsComponents(&componentRef)
; ; SimpleProductsListSlot  ; SimpleProductsListComponent
; ; SimpleProductsTitleSlot ; ProductsTitleComponentFINRA

### End Simple product list page

