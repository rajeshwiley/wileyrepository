import com.wiley.core.model.AbandonCartEmailHistoryEntryModel
import de.hybris.platform.core.model.order.CartModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.SearchResult

import java.time.Instant

FlexibleSearchService fss = (FlexibleSearchService) spring.getBean("flexibleSearchService");
ModelService modelService = (ModelService) spring.getBean("modelService");

int start = 0;
final int range = 50;
int total;

String query = "SELECT {pk} FROM {Cart}";
FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
fsq.setCount(range);
fsq.setNeedTotal(true);

final Instant instant = Instant.now();
Date dateNow = Date.from(instant);

fsq.setStart(start);
SearchResult<CartModel> searchResultInitial = search(fss, fsq, modelService, dateNow)
total = searchResultInitial.getTotalCount();
start += range;

while (start < total) {
    fsq.setStart(start);
    SearchResult<CartModel> searchResult = search(fss, fsq, modelService, dateNow)
    total = searchResult.getTotalCount();
    start += range;
}

private SearchResult<CartModel> search(FlexibleSearchService fss, FlexibleSearchQuery fsq, modelService, Date date) {
    final SearchResult<CartModel> searchResult = fss.search(fsq);
    List<CartModel> result = searchResult.getResult();
    if (!result.isEmpty()) {
        result.each() {
            CartModel cart = it;
            AbandonCartEmailHistoryEntryModel entry = modelService.create(AbandonCartEmailHistoryEntryModel.class)
            entry.setEmailSentDate(date);
            entry.setOrder(cart);
            modelService.save(entry);
        }
    }
    return searchResult;
}
                                   