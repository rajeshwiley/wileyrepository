import de.hybris.platform.catalog.CatalogVersionService
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.category.CategoryService
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.product.ProductService
import de.hybris.platform.servicelayer.model.ModelService


CatalogVersionService catVerService = (CatalogVersionService) spring.getBean("catalogVersionService");
CategoryService categoryService = (CategoryService) spring.getBean("categoryService");
ProductService productService = (ProductService) spring.getBean("productService");
ModelService modelService = (ModelService) spring.getBean("modelService");

CatalogVersionModel welStaged = catVerService.getCatalogVersion("welProductCatalog", "Staged");
Collection<CategoryModel> rootCats = categoryService.getRootCategoriesForCatalogVersion(welStaged);
rootCats.each {
    CategoryModel categ = it;
    List<ProductModel> products = productService.getProductsForCategory(categ);
    products.each() {
        ProductModel product = it;
        if ("111".equals(product.getEan())) {
            product.setEan("222");
        } else {
            product.setEan("333");
        }
    }
    modelService.saveAll(products);
}

