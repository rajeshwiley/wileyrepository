import de.hybris.platform.catalog.CatalogVersionService
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService

FlexibleSearchService fss = (FlexibleSearchService) spring.getBean("flexibleSearchService");
CatalogVersionService catVerService = (CatalogVersionService) spring.getBean("catalogVersionService");

CatalogVersionModel welStaged = catVerService.getCatalogVersion("welProductCatalog", "Staged");
String query = "SELECT {pk} FROM {WileyProduct AS wp} WHERE {wp.catalogVersion} = ?catVer AND {wp.ean} <> '444'"
FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
fsq.addQueryParameter("catVer", welStaged);
List<ProductModel> result = fss.search(fsq).getResult();
if (!result.isEmpty()) {
    throw new RuntimeException("Something went wrong");
}
