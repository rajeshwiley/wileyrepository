import de.hybris.platform.catalog.jalo.Catalog
import de.hybris.platform.catalog.jalo.CatalogManager
import de.hybris.platform.catalog.jalo.RemoveCatalogVersionCronJob
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants
import de.hybris.platform.jalo.JaloSession
import de.hybris.platform.jalo.JaloSystemException
import de.hybris.platform.jalo.SearchResult
import de.hybris.platform.jalo.SessionContext
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch
import de.hybris.platform.jalo.type.ComposedType
import de.hybris.platform.jalo.type.JaloAbstractTypeException
import de.hybris.platform.jalo.type.JaloGenericCreationException
import de.hybris.platform.jalo.user.User
import org.slf4j.LoggerFactory

def logger = LoggerFactory.getLogger("RemoveCatalogScript");
def catQueryPattern = "SELECT {pk} FROM {Catalog} where {id} like '%s'"

// Assume script starts from admin user
User adminUser = JaloSession.getCurrentSession().getUser();

Catalog catalog = findCatalog(String.format(catQueryPattern, "welProductCatalog"))
SessionContext sessCtx = JaloSession.getCurrentSession().getSessionContext();
if (catalog != null) {
    RemoveCatalogVersionCronJob finishedJob = removeCatalog(catalog, adminUser);
    logger.info("RemoveCatalogJob has been performed for catalog: 'welProductCatalog' with result: {}", finishedJob.getResult());
    println "RemoveCatalogJob has been performed for catalog: 'welProductCatalog' with result: " + finishedJob.getResult();
} else {
    logger.info("Catalog 'welProductCatalog' not found");
    println "Catalog 'welProductCatalog' not found";
}
catalog = findCatalog(String.format(catQueryPattern, "welContentCatalog"))
if (catalog != null) {
    RemoveCatalogVersionCronJob finishedJob = removeCatalog(catalog, adminUser);
    logger.info("RemoveCatalogJob has been performed for catalog: 'welContentCatalog' with result: {}", finishedJob.getResult());
    println "RemoveCatalogJob has been performed for catalog: 'welContentCatalog' with result: " + finishedJob.getResult()
} else {
    logger.info("Catalog 'welContentCatalog' not found");
    println "Catalog 'welContentCatalog' not found";
}
catalog = findCatalog(String.format(catQueryPattern, "WELClassification"))
if (catalog != null) {
    RemoveCatalogVersionCronJob finishedJob = removeCatalog(catalog, adminUser);
    logger.info("RemoveCatalogJob has been performed for catalog: 'WELClassification' with result: {}", finishedJob.getResult());
    println "RemoveCatalogJob has been performed for catalog: 'WELClassification' with result: " + finishedJob.getResult()
} else {
    logger.info("Catalog 'WELClassification' not found");
    println "Catalog 'WELClassification' not found";
}

public Catalog findCatalog(String catalogQuery) {
    SearchResult<Catalog> sresult = FlexibleSearch.getInstance().search(JaloSession.getCurrentSession().getSessionContext(),
            catalogQuery, Collections.emptyMap(), Catalog.class);
    if (sresult.count <= 0) {
        return null;
    } else {
        return sresult.result.get(0);
    }

}

public RemoveCatalogVersionCronJob removeCatalog(Catalog catalog, User adminUser) {

    ComposedType comptyp = JaloSession.getCurrentSession().getTypeManager().getComposedType(RemoveCatalogVersionCronJob.class);
    Map<String, Object> cronjobmap = new HashMap();
    cronjobmap.put("job", CatalogManager.getInstance().getOrCreateDefaultRemoveCatalogVersionJob());
    cronjobmap.put("catalogVersion", null);
    cronjobmap.put("catalog", catalog);
    cronjobmap.put("sessionUser", adminUser);

    try {
        RemoveCatalogVersionCronJob cronJob = (RemoveCatalogVersionCronJob) comptyp.newInstance(cronjobmap);
        cronJob.getJob().perform(cronJob, true);

        if (GeneratedCronJobConstants.Enumerations.CronJobStatus.FINISHED == cronJob.getStatus().getCode()) {
            return cronJob;
        } else {
            throw new RuntimeException("CronJob is not FINISHED. " + cronJob.getStatus());
        }
    } catch (JaloGenericCreationException var4) {
        throw new JaloSystemException(var4);
    } catch (JaloAbstractTypeException var5) {
        throw new JaloSystemException(var5);
    }
}

