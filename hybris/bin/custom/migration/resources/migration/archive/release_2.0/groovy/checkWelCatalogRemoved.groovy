import de.hybris.platform.catalog.jalo.Catalog
import de.hybris.platform.jalo.JaloSession
import de.hybris.platform.jalo.SearchResult
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch

def catQueryPattern = "SELECT {pk} FROM {Catalog} where {id} like '%s'"

checkCatalogRemoved(String.format(catQueryPattern, "welProductCatalog"));
checkCatalogRemoved(String.format(catQueryPattern, "welContentCatalog"));
checkCatalogRemoved(String.format(catQueryPattern, "WELClassification"));

public checkCatalogRemoved(String catalogQuery) {
    SearchResult<Catalog> sresult = FlexibleSearch.getInstance().search(JaloSession.getCurrentSession().getSessionContext(),
            catalogQuery, Collections.emptyMap(), Catalog.class);
    if (sresult.count > 0) {
        throw new RuntimeException("Catalog has not been removed");
    }
}
