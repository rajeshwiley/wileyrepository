import de.hybris.platform.europe1.model.DiscountRowModel
import de.hybris.platform.europe1.model.PriceRowModel
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import org.slf4j.LoggerFactory

def logger = LoggerFactory.getLogger("RemoveCatalogScript");
def discountQuerySoft = "SELECT {pk} FROM {DiscountRow AS drow JOIN Product AS pr ON {pr.code}={drow.productId} " +
        "JOIN CatalogVersion AS cv ON{cv.pk}={pr.catalogVersion} JOIN Catalog AS cat ON {cat.pk}={cv.catalog} } " +
        "WHERE {cat.id} like '%welProductCatalog%' AND {cv.version} = 'Staged'";

def priceQuerySoft = "SELECT {pk} FROM {PriceRow AS prow JOIN Product AS pr ON {pr.code}={prow.productId} " +
        "JOIN CatalogVersion AS cv ON{cv.pk}={pr.catalogVersion} JOIN Catalog AS cat ON {cat.pk}={cv.catalog} } " +
        "WHERE {cat.id} like '%welProductCatalog%' AND {cv.version} = 'Staged'";

ModelService modelService = (ModelService) spring.getBean("modelService");
FlexibleSearchService flexService = (FlexibleSearchService) spring.getBean("flexibleSearchService");

List<DiscountRowModel> softDiscountList = flexService.search(discountQuerySoft).getResult();
String logStr = String.format("We are going to remove %s soft  discountRows for products from welProductCatalog:Staged", softDiscountList.size());
logger.info(logStr);
println logStr;
modelService.removeAll(softDiscountList);

List<PriceRowModel> softPriceList = flexService.search(priceQuerySoft).getResult();
logStr = String.format("We are going to remove %s soft  priceRows for products from welProductCatalog:Staged", softPriceList.size());
logger.info(logStr);
println logStr;
modelService.removeAll(softPriceList);
