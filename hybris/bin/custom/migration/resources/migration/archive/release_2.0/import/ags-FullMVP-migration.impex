#
# Import the CMS content for the AGS site
#
$contentCatalog = agsContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]
$addonExtensionName = assistedservicestorefront

$contentCatalogName = AGS Content Catalog
$productCatalog = agsProductCatalog
$productCatalogName = AGS Product Catalog
$productCV = catalogVersion(catalog(id[default = $productCatalog]), version[default = 'Staged'])[unique = true, default = $productCatalog:Staged]
$picture = media(code, $contentCV);
$image = image(code, $contentCV);
$media = media(code, $contentCV);
$page = page(uid, $contentCV);
$contentPage = contentPage(uid, $contentCV);
$product = product(code, $productCV)
$category = category(code, $productCV)
$siteResource = jar:com.wiley.initialdata.constants.WileyInitialDataConstants&/wileyinitialdata/import/sampledata/contentCatalogs/$contentCatalog
$productResource = jar:com.wiley.initialdata.constants.WileyInitialDataConstants&/wileyinitialdata/import/sampledata/productCatalogs/$productCatalog
$jarResourceCms = jar:com.wiley.initialdata.constants.WileyInitialDataConstants&/wileyinitialdata/import/sampledata/cockpits/cmscockpit
# Language
$lang = en

#### Core Data

# Import modulegen config properties into impex macros
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$jarResourceCms = $config-jarResourceCmsValue

# Create PageTemplates
# These define the layout for pages
# "FrontendTemplateName" is used to define the JSP that should be used to render the page for pages with multiple possible layouts.
# "RestrictedPageTypes" is used to restrict templates to page types
INSERT_UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; PinActivationPageTemplate            ; Pin Activation Page Template              ; pinActivation/pinActivationPage   ; ContentPage ; true  ;
; ; PinLoginPageTemplate                 ; Pin Login Page Template                   ; pinActivation/pinLoginPage        ; ContentPage ; true  ;
; ; PinConfirmationPageTemplate          ; PIN Confirmation Page Template            ; pinActivation/pinConfirmationPage ; ContentPage ; true  ;

# Add Velocity templates that are in the CMS Cockpit. These give a better layout for editing pages
# The FileLoaderValueTranslator loads a File into a String property. The templates could also be inserted in-line in this file.
UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; PinActivationPageTemplate            ; $jarResourceCms/structure-view/structure_pinActivationPageTemplate.vm
; ; PinConfirmationPageTemplate          ; $jarResourceCms/structure-view/structure_pinConfirmationPageTemplate.vm
; ; PinLoginPageTemplate                 ; $jarResourceCms/structure-view/structure_pinLoginPageTemplate.vm

INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'ErrorPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; TopHeaderSlot ; ; ; wide

# Checkout Login Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'CheckoutLoginPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo                ; ; SimpleResponsiveBannerComponent
; HeaderLinks             ; ; ; headerlinks
; SearchBox               ; ; ; searchbox
; MiniCart                ; ; ; minicart
; NavigationBar           ; ; ; navigation
; CheckoutBreadcrumbsSlot ; ; JspIncludeComponent
; TopContent              ; ; ; wide
; BottomContent           ; ; ; wide
; Footer                  ; ; ; footer
; LeftContentSlot         ; ; ; wide
; RightContentSlot        ; ; ; wide
; CenterContentSlot       ; ; ; wide
; PlaceholderContentSlot  ; ; ;
; CompanyLogo             ; ; SimpleResponsiveBannerComponent
; TopHeaderSlot           ; ; ; wide

# Multi Step Checkout Summary Page Templates
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'MultiStepCheckoutSummaryPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; CheckoutBreadcrumbsSlot ; ; JspIncludeComponent

# Order Confirmation Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'OrderConfirmationPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; CheckoutBreadcrumbsSlot ; ; JspIncludeComponent

# Pin Activation Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'PinActivationPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo        ; ; SimpleResponsiveBannerComponent
; NavigationBar   ; ; ; navigation
; Footer          ; ; ; footer
; CompanyLogoSlot ; ; SimpleResponsiveBannerComponent
; TopHeaderSlot   ; ; ; wide

# PIN Confirmation Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'PinConfirmationPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo      ; ; SimpleResponsiveBannerComponent
; NavigationBar ; ; ; navigation
; SideContent   ; ; ; narrow
; TopContent    ; ; ; wide
; Footer        ; ; ; footer
; CompanyLogo   ; ; SimpleResponsiveBannerComponent
; TopHeaderSlot ; ; ; wide

# Terms and Conditions Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'TermsAndConditionsPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; TopHeaderSlot   ; ; ; wide

# Create Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; CheckoutBreadcrumbsSlot ; Checkout Breadcrumbs Slot       ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'CheckoutLoginPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; CheckoutBreadcrumbs-CheckoutLoginPage    ; CheckoutBreadcrumbs    ; ; CheckoutBreadcrumbsSlot ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'PinLoginPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-PinLoginPage               ; SiteLogo               ; ; SiteLogoSlot            ; true
; ; HomepageLink-PinLoginPage           ; HomepageNavLink        ; ; HomepageNavLinkSlot     ; true
; ; NavigationBar-PinLoginPage          ; NavigationBar          ; ; NavigationBarSlot       ; true
; ; MiniCart-PinLoginPage               ; MiniCart               ; ; MiniCartSlot            ; true
; ; CheckoutBreadcrumbs-PinLoginPage    ; CheckoutBreadcrumbs    ; ; CheckoutBreadcrumbsSlot ; true
; ; Footer-PinLoginPage                 ; Footer                 ; ; FooterSlot              ; true
; ; HeaderLinks-PinLoginPage            ; HeaderLinks            ; ; HeaderLinksSlot         ; true
; ; SearchBox-PinLoginPage              ; SearchBox              ; ; SearchBoxSlot           ; true
; ; TopHeaderSlot-PinLoginPage          ; TopHeaderSlot          ; ; TopHeaderSlot           ; true
; ; BottomHeaderSlot-PinLoginPage       ; BottomHeaderSlot       ; ; BottomHeaderSlot        ; true
; ; LeftContentSlot-PinLoginPage        ; LeftContentSlot        ; ; LeftContentSlot         ; true
; ; RightContentSlot-PinLoginPage       ; RightContentSlot       ; ; RightContentSlot        ; true
; ; CenterContentSlot-PinLoginPage      ; CenterContentSlot      ; ; CenterContentSlot       ; true
; ; PlaceholderContentSlot-PinLoginPage ; PlaceholderContentSlot ; ; PlaceholderContentSlot  ; true
; ; CompanyLogo-PinLoginPage            ; CompanyLogo            ; ; CompanyLogoSlot         ; true

# Bind Content Slots to Multi Step Checkout Summary Page Templates
INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'MultiStepCheckoutSummaryPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; CheckoutBreadcrumbs-MultiStepCheckoutSummaryPage    ; CheckoutBreadcrumbs    ; ; CheckoutBreadcrumbsSlot ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'OrderConfirmationPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; CheckoutBreadcrumbs-OrderConfirmationPage    ; CheckoutBreadcrumbs    ; ; CheckoutBreadcrumbsSlot ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'PinActivationPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-PinActivationPage      ; SiteLogo      ; ; SiteLogoSlot      ; true
; ; NavigationBar-PinActivationPage ; NavigationBar ; ; NavigationBarSlot ; true
; ; Footer-PinActivationPage        ; Footer        ; ; FooterSlot        ; true
; ; CompanyLogo-PinActivationPage   ; CompanyLogo   ; ; CompanyLogoSlot   ; true
; ; TopHeaderSlot-PinActivationPage ; TopHeaderSlot ; ; TopHeaderSlot     ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'PinConfirmationPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-PinConfirmationPage      ; SiteLogo      ; ; SiteLogoSlot      ; true
; ; NavigationBar-PinConfirmationPage ; NavigationBar ; ; NavigationBarSlot ; true
; ; Footer-PinConfirmationPage        ; Footer        ; ; FooterSlot        ; true
; ; CompanyLogo-PinConfirmationPage   ; CompanyLogo   ; ; CompanyLogoSlot   ; true
; ; TopHeaderSlot-PinConfirmationPage ; TopHeaderSlot ; ; TopHeaderSlot     ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'ErrorPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; TopHeaderSlot-ErrorPage ; TopHeaderSlot ; ; TopHeaderSlot     ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'TermsAndConditionsPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; TopHeaderSlot-TermsAndConditionsPage          ; TopHeaderSlot ; ; TopHeaderSlot     ; true

# Preview Image for use in the CMS Cockpit for special ContentPages
INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; mime; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]
; ; ContentPageModel__function_preview ; text/gif ; ContentPageModel__function_preview.gif ; $jarResourceCms/preview-images/ContentPageModel__function_preview.gif

# Functional Content Pages
INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); label; robotsmetatag(code)[default = 'UNDEFINED']; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']; previewImage(code, $contentCV)[default = 'ContentPageModel__function_preview']
; ; pinActivationPage            ; PIN Activation Page       ; PinActivationPageTemplate            ; pinActivation             ; INDEX_NOFOLLOW
; ; pinConfirmationPage          ; PIN Confirmation Page     ; PinConfirmationPageTemplate          ; pinConfirmation           ; NOINDEX_NOFOLLOW
; ; pinLoginPage                 ; Pin Login Page            ; PinLoginPageTemplate                 ; pinLogin                  ; INDEX_NOFOLLOW

INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); label; robotsmetatag(code)[default = 'UNDEFINED']; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']
; ; termsAndConditions ; Terms and Conditions Page ; TermsAndConditionsPageTemplate ; /termsAndConditions; INDEX_FOLLOW


############################################################ Core Data EN ########################################################
UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; title[lang = $lang]
; ; pinLoginPage                 ; "PIN Activation Login"


########################################################## Sample data ###########################################################
$jarResourceCms = jar:com.wiley.initialdata.constants.WileyInitialDataConstants&/wileyinitialdata/import/sampledata/cockpits/cmscockpit

#Checkout Breadcrumb Component
INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; CheckoutBreadcrumbComponent ; Checkout Breadcrumb Component ; checkoutBreadcrumb.jsp ; ; CheckoutBreadcrumbComponent

# Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; cmsComponents(&componentRef)
; ; CheckoutBreadcrumbsSlot; CheckoutBreadcrumbComponent

# Preview Image for use in the CMS Cockpit for special ContentPages
INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; mime; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]; folder(qualifier)[default = 'images']
; ; pinLoginPagePreview                 ; text/png ; PinLoginPage.png                 ; $jarResourceCms/preview-images/wiley/PinLoginPage.png             ; ; ;

# Functional Content Pages
UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; previewImage(code, $contentCV)
; ; pinLoginPage                 ; pinLoginPagePreview

INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; NewCustomerPinLoginComponent            ; New Customer Pin Login Component              ; pinNewCustomerLogin.jsp            ;                 ; NewCustomerPinLoginComponent
; ; ReturningCustomerPinLoginComponent      ; Returning Customer Pin Login Component        ; pinReturningCustomerLogin.jsp      ;                 ; ReturningCustomerPinLoginComponent

### Pin Login
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(uid, $contentCV);;;
; ; TopContentSlot-pinLoginPage   ; Top Content Slot for Customer Login   ; true ; ContactInfoContentParagraph        ; ; ;
; ; LeftContentSlot-pinLoginPage  ; Left Content Slot for Customer Login  ; true ; NewCustomerPinLoginComponent       ; ; ;
; ; RightContentSlot-pinLoginPage ; Right Content Slot for Customer Login ; true ; ReturningCustomerPinLoginComponent ; ; ;

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'pinLoginPage']; contentSlot(uid, $contentCV)[unique = true];;;
; ; TopContentSlot-pinLoginPage   ; TopContentSlot   ; ; TopContentSlot-pinLoginPage   ; ; ;
; ; LeftContentSlot-pinLoginPage  ; LeftContentSlot  ; ; LeftContentSlot-pinLoginPage  ; ; ;
; ; RightContentSlot-pinLoginPage ; RightContentSlot ; ; RightContentSlot-pinLoginPage ; ; ;

### Pin Activation Page
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(uid, $contentCV);;;
; ; TopContentSlot-pinActivationPage    ; Top Content Slot for Pin Activation    ; true ; ContactInfoContentParagraph  ; ; ;

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'pinActivationPage']; contentSlot(uid, $contentCV)[unique = true];;;
; ; TopContentSlot-pinActivationPage    ; TopContent    ; ; TopContentSlot-pinActivationPage   ; ; ;

### PIN Confirmation Page

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef
; ; PinConfirmationTextParagraph ; PIN confirmation text paragraph ; PinConfirmationTextParagraph

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(uid, $contentCV);;;
; ; CustomerSupportContactSlot-PinConfirmationPage    ; Customer support contact slot for PIN Confirmation    ; true ; ContactInfoContentParagraph; ; ;

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef);;;
; ; ConfirmationTextSlot-PinConfirmationPage          ; Confirmation text slot for order confirmation           ; true ; PinConfirmationTextParagraph  ; ; ;

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'pinConfirmationPage']; contentSlot(uid, $contentCV)[unique = true];;;
; ; TopContent-PinConfirmationPage         ; TopContent         ; ; CustomerSupportContactSlot-PinConfirmationPage    ; ; ;
; ; PinConfirmation-PinConfirmationPage    ; PinConfirmation    ; ; ConfirmationTextSlot-PinConfirmationPage          ; ; ;

######################################################### Sample Data EN #########################################################
# PIN Confirmation Page
UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; content[lang = $lang]
; ; PinConfirmationTextParagraph   ; "<div class=""confirmation-text""><h2><strong>Thank you for activating your PIN!</strong></h2>
 <p>You may now access your product.</p></div>"