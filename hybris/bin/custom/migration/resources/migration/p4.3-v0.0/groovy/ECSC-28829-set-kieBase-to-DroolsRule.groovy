import de.hybris.platform.ruleengine.model.DroolsKIEBaseModel
import de.hybris.platform.ruleengine.model.DroolsRuleModel
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.transform.Field
import org.apache.log4j.Logger

@Field FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean("flexibleSearchService")
@Field ModelService modelService = (ModelService) spring.getBean("modelService")
@Field Logger LOG = Logger.getLogger(this.getClass().getName());

LOG.info("Searching DroolsRules with empty kieBase...");
List<DroolsRuleModel> droolsRules = findDroolsRules();

List<DroolsKIEBaseModel> kieBases = findKieBase();
if (!kieBases.isEmpty() && !droolsRules.isEmpty()) {
    DroolsKIEBaseModel droolsKIEBaseModel = kieBases.get(0);
    LOG.info("Setting " + droolsRules.size() + " DroolsRules to '" + droolsKIEBaseModel.getName() + "'...");
    Set<DroolsRuleModel> droolsRuleWithoutKieBase = new HashSet<>(droolsRules);
    Set<DroolsRuleModel> droolsRuleWithKieBase = new HashSet<>(droolsKIEBaseModel.getRules());
    droolsRuleWithKieBase.addAll(droolsRuleWithoutKieBase);
    droolsKIEBaseModel.setRules(droolsRuleWithKieBase);
    modelService.save(droolsKIEBaseModel);

    LOG.info("Done");
}

List<DroolsRuleModel> findDroolsRules() {
    String query = "SELECT {d:" + DroolsRuleModel.PK + "} FROM {" + DroolsRuleModel._TYPECODE + " as d} " +
            "WHERE {d:" + DroolsRuleModel.KIEBASE + "} IS NULL";
    final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
    return flexibleSearchService.search(flexibleSearchQuery).getResult()
}

List<DroolsKIEBaseModel> findKieBase() {
    String query = "SELECT {k:" + DroolsKIEBaseModel.PK + "} FROM {" + DroolsKIEBaseModel._TYPECODE + " as k} " +
            "WHERE {k:" + DroolsKIEBaseModel.NAME + "} = 'promotions-base'";
    final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
    return flexibleSearchService.search(flexibleSearchQuery).getResult();
}