import de.hybris.platform.cronjob.model.CronJobModel
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService

FlexibleSearchService flexibleSearchService = spring.getBean("flexibleSearchService", FlexibleSearchService.class)
ModelService modelService = spring.getBean("modelService", ModelService.class)

String selectLuceneCronjobs =
'''SELECT {Pk} FROM {CronJob} WHERE {code} in(
'Cluster 0: Lucenesearch-RebuildIndex-Job', 'Cluster 1: Lucenesearch-RebuildIndex-Job',
'Cluster 2: Lucenesearch-RebuildIndex-Job', 'Cluster 0: Lucenesearch-UpdateIndex-Job',
'Cluster 1: Lucenesearch-UpdateIndex-Job', 'Cluster 2: Lucenesearch-UpdateIndex-Job')'''

final FlexibleSearchQuery query = new FlexibleSearchQuery(selectLuceneCronjobs)
List<CronJobModel> cronJobModelList = flexibleSearchService.search(query).getResult() as List<CronJobModel>
modelService.removeAll(cronJobModelList)
