import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel
import de.hybris.platform.ruleengine.RuleEngineActionResult;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerResult;
import de.hybris.platform.ruleengineservices.maintenance.RuleCompilerPublisherResult;
import de.hybris.platform.ruleengineservices.maintenance.RuleMaintenanceService;
import de.hybris.platform.ruleengineservices.model.SourceRuleModel
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.transform.Field;
import org.apache.log4j.Logger

FlexibleSearchService flexibleSearchService = spring.getBean("flexibleSearchService", FlexibleSearchService.class);
RuleMaintenanceService ruleMaintenanceService = (RuleMaintenanceService) spring.getBean("ruleMaintenanceService");
@Field Logger LOG = Logger.getLogger(this.getClass().getName());
ConfigurationService configurationService = (ConfigurationService) spring.getBean("configurationService");

configurationService.getConfiguration().setProperty("ruleengine.engine.active", "true");

LOG.info("Searching promotion rules to recompile and publish...");

String selectSourceRulesForActiveDroolsRules = """select distinct {sr:pk} 
    from {SourceRule as sr JOIN DroolsRule as dr ON {sr:PK}={dr:sourceRule}} 
    where {dr:active} = ?active and {dr:currentVersion} = ?currentVersion""";
Map<String, Object> params = new HashMap<>();
params.put("active", true)
params.put("currentVersion", true)

final FlexibleSearchQuery query = new FlexibleSearchQuery(selectSourceRulesForActiveDroolsRules, params);
List<SourceRuleModel> sourceRuleModelList = flexibleSearchService.search(query).getResult();
LOG.info("Found " + sourceRuleModelList.size() + " rules to recompile and publish");

LOG.info("Compiling...");
RuleCompilerPublisherResult result = ruleMaintenanceService.compileAndPublishRulesWithBlocking(sourceRuleModelList, "promotions-module", true)
LOG.info("Completed with " + result.getResult());

result.getCompilerResults().forEach({cr -> logCompileResult(cr)});
result.getPublisherResults().forEach({pr -> logPublishResult(pr)});

LOG.info("Done");

void logCompileResult(RuleCompilerResult result) {
    LOG.info(result.getRuleCode()+"."+ result.getRuleVersion() + " compiled with " + result.getResult() + " result problems: " + result.getProblems());
}

void logPublishResult(RuleEngineActionResult result) {
    LOG.info(result.getMessagesAsString(null));
}