import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel
import de.hybris.platform.ruleengineservices.model.RuleGroupModel
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.transform.Field
import org.apache.log4j.Logger

@Field FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean("flexibleSearchService")
@Field ModelService modelService = (ModelService) spring.getBean("modelService")
@Field Logger LOG = Logger.getLogger(this.getClass().getName());

LOG.info("Searching non-stackable promotion rules...");
List<PromotionSourceRuleModel> promotionRules = findPromotionRules();

List<RuleGroupModel> ruleGroup = findDefaultExclusiveRuleGroup();
if (!ruleGroup.isEmpty()) {
    RuleGroupModel ruleGroupModel = ruleGroup.get(0);
    LOG.info("Setting " + promotionRules.size() + " promotion rules to 'defaultExclusivePromotionRuleGroup'...");
    ruleGroupModel.setRules(new HashSet<>(promotionRules));
    modelService.save(ruleGroupModel);

    LOG.info("Done");
}

List<PromotionSourceRuleModel> findPromotionRules() {
    String query = "SELECT {p:" + PromotionSourceRuleModel.PK + "} FROM {" + PromotionSourceRuleModel._TYPECODE + " as p} " +
            "WHERE {p:" + PromotionSourceRuleModel.STACKABLE + "} = false";
    final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
    return flexibleSearchService.search(flexibleSearchQuery).getResult()
}

List<RuleGroupModel> findDefaultExclusiveRuleGroup() {
    String query = "SELECT {r:" + RuleGroupModel.PK + "} FROM {" + RuleGroupModel._TYPECODE + " as r} " +
            "WHERE {r:" + RuleGroupModel.CODE + "} = 'defaultExclusivePromotionRuleGroup'";
    final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
    return flexibleSearchService.search(flexibleSearchQuery).getResult();
}