import de.hybris.platform.ruleengineservices.model.SourceRuleModel
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService

FlexibleSearchService flexibleSearchService = spring.getBean("flexibleSearchService", FlexibleSearchService.class)
ModelService modelService = spring.getBean("modelService", ModelService.class)

String selectSourceRulesForActiveDroolsRules = "SELECT {Pk} FROM {AbstractRule} WHERE {version} is null"
final FlexibleSearchQuery query = new FlexibleSearchQuery(selectSourceRulesForActiveDroolsRules)
List<SourceRuleModel> sourceRuleModelList = flexibleSearchService.search(query).getResult() as List<SourceRuleModel>
sourceRuleModelList.forEach({ rule -> rule.setVersion(0L) })
modelService.saveAll(sourceRuleModelList)