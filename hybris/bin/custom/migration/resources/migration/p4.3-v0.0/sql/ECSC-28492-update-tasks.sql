update tasks
set p_expirationtimemillis = 7258161600000
where p_expirationtimemillis is null;
update taskconditions
set p_expirationtimemillis = 7258161600000
where p_expirationtimemillis is null;
update tasks
set p_executionhourmillis = p_executiontimemillis - MOD(p_executiontimemillis, 1000 * 60 * 60)
where p_executionhourmillis is null;