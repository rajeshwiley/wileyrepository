SELECT TableName INTO @enum_table FROM ydeployments WHERE Name = 'EnumerationValue' and TypeSystemName = 'p4.3-v0.0';
SET @query_update = concat(
                          'UPDATE cmspage SET p_pagestatus = (SELECT PK FROM ',
                          @enum_table,
                          ' WHERE code = "active" AND p_extensionname = "cms2") WHERE p_pagestatus IS NULL');
PREPARE stmt FROM @query_update;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;