import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import groovy.transform.Field
import de.hybris.platform.catalog.model.CatalogVersionModel;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Field final Logger LOG = LoggerFactory.getLogger("product_lifecycleStatuses_converter");
@Field int numberOfProductsForRequest = 100;
@Field String catalog = "welProductCatalog";
@Field String stagedCatalogVersion = "Staged";
@Field String onlineCatalogVersion = "Online";

@Field final String productsSearchQuery = "SELECT DISTINCT({vp.pk}) " +
        "FROM {" +
        "VariantProduct as vp " +
        "JOIN Product AS bp ON {vp.baseProduct} = {bp.pk} " +
        "JOIN CategoryProductRelation AS cpr ON {cpr:target}={bp:pk} " +
        "JOIN Category AS c ON {cpr:source}={c:pk}" +
        "}" +
        "WHERE {vp.catalogVersion}=?catalogVersion " +
        "AND {c:code} LIKE '%CFA%'" +
        " AND {vp.lifecycleStatus} IS NULL";


LOG.info("Converting Stage products");
convertProductsForCatalogVersion(stagedCatalogVersion);
LOG.info("Converting Online products");
convertProductsForCatalogVersion(onlineCatalogVersion);
LOG.info("Converting finished");

def void convertProductsForCatalogVersion(String catalogVersionForConvert)
{
    final FlexibleSearchQuery flexibleSearchQueryForProducts = getFlexibleSearchQueryForProducts(catalogVersionForConvert);
    List<VariantProductModel> result = flexibleSearchService.<VariantProductModel> search(flexibleSearchQueryForProducts).getResult();
    while(CollectionUtils.isNotEmpty(result))
    {
        LOG.info("Converting statuses for {} products", result.size());
        setLifecycleStatusForProducts(result);
        result = flexibleSearchService.<VariantProductModel> search(flexibleSearchQueryForProducts).getResult();
    }
}

def FlexibleSearchQuery getFlexibleSearchQueryForProducts(String catalogVersionForConvert)
{
    final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(productsSearchQuery);

    flexibleSearchQuery.addQueryParameter("catalogVersion", getWELProductCatalogVersion(catalogVersionForConvert));
    flexibleSearchQuery.setDisableCaching(true);
    flexibleSearchQuery.setResultClassList(Arrays.asList(VariantProductModel.class));
    flexibleSearchQuery.setCount(numberOfProductsForRequest);
    return flexibleSearchQuery;
}
def CatalogVersionModel getWELProductCatalogVersion(String catalogVersionForConvert) {
    final Set<CatalogVersionModel> welProductCatalogVersions = catalogService.getCatalogForId(catalog).getCatalogVersions();
    CatalogVersionModel welProductCatalogVersion = null;
    for(CatalogVersionModel catalogVersion : welProductCatalogVersions) {
        if(Objects.equals(catalogVersion.getVersion(), catalogVersionForConvert)) {
            welProductCatalogVersion = catalogVersion;
        }
    }
    return welProductCatalogVersion;
}
def void setLifecycleStatusForProducts(final List<VariantProductModel> result)
{
    for (VariantProductModel product : result)
    {
        product.setLifecycleStatus(WileyProductLifecycleEnum.ACTIVE);
    }
    modelService.saveAll();
}