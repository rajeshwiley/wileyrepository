import com.google.common.collect.Maps
import com.paypal.hybris.model.PaypalPaymentInfoModel
import com.wiley.core.model.EdiExportCronJobModel
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel
import de.hybris.platform.servicelayer.cronjob.CronJobService
import de.hybris.platform.servicelayer.cronjob.impl.DefaultCronJobService
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.transform.Field

@Field CronJobService cronJobService = (DefaultCronJobService) spring.getBean("defaultCronJobService")
@Field FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean("flexibleSearchService")
@Field ModelService modelService = (ModelService) spring.getBean("modelService")

fillDailyReportSent("wel", PaypalPaymentInfoModel._TYPECODE)
fillDailyReportSent("wel", CreditCardPaymentInfoModel._TYPECODE)
fillDailyReportSent("ags", CreditCardPaymentInfoModel._TYPECODE)
fillDailyReportSent("ags", PaypalPaymentInfoModel._TYPECODE)
activateAllEdiTriggers()

void fillDailyReportSent(String baseStoreUid, String paymentInfo) {
    Map<String, Object> params = Maps.newHashMap();
    params.put("store", baseStoreUid)

    String selectOrdersReadyForExportQuery = String.format("""SELECT {wep:PK} " + "FROM {WileyExportProcess as wep 
    LEFT JOIN Order AS o ON {wep:order} = {o:PK} 
    LEFT JOIN BaseStore AS bs ON {o:store} = {bs:PK} 
    LEFT JOIN %s! AS pi ON {o:paymentInfo} = {pi:PK}} 
    WHERE {wep:systemCode}  = 'edi' 
    AND {wep:done} = true AND {wep:dailyReportSent} != true
    AND {bs:uid} = ?store AND  {pi:code} is NOT NULL""", paymentInfo)

    final FlexibleSearchQuery query = new FlexibleSearchQuery(selectOrdersReadyForExportQuery, params)
    List<WileyExportProcessModel> exportProcessModelList = flexibleSearchService.search(query).getResult()
    exportProcessModelList.forEach({ process ->
        process.setDailyReportSent(true)
    })
    modelService.saveAll(exportProcessModelList)
}

void activateAllEdiTriggers() {
    EdiExportCronJobModel ediCCCronJob = cronJobService.getCronJob("ediCCExportJob")
    EdiExportCronJobModel ediPPCronJob = cronJobService.getCronJob("ediPPExportJob")
    ediCCCronJob.getTriggers().forEach({
        trigger ->
            trigger.setActive(true)
            modelService.save(trigger)
    })
    ediPPCronJob.getTriggers().forEach({
        trigger ->
            trigger.setActive(true)
            modelService.save(trigger)
    })
}


