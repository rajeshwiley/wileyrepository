--- This script removes records that does not have valid TypePkString. Also removed relations with this broken records ---
--- !!! DO NOT RUN IT UNLESS YOU KNOW WHAT YOU ARE DOING !!! --

SET SQL_SAFE_UPDATES = 0;

-- Table cmscomponent : contains wrong TypePks: [8796135915602, 8796135555154, 8796135587922, 8796135850066, 8796135620690, 8796135751762, 8796135882834] --
DELETE FROM cmscomponent WHERE TypePkString = 8796135915602;-- Wileyb2cSiteLogoComponent --
DELETE FROM cmscomponent WHERE TypePkString = 8796135555154;-- CMSLinkComponent --
DELETE FROM cmscomponent WHERE TypePkString = 8796135587922;-- Wileyb2cContinueShoppingLinkComponent --
DELETE FROM cmscomponent WHERE TypePkString = 8796135850066 ;-- WileycomFooterComponent --
DELETE FROM cmscomponent WHERE TypePkString = 8796135620690 ;-- PayPalExpressCheckoutShortcut --
DELETE FROM cmscomponent WHERE TypePkString = 8796135751762 ;-- PayPalMiniCartComponent --
DELETE FROM cmscomponent WHERE TypePkString = 8796135882834 ;-- PayPalJspIncludeComponent --

-- Table cronjobs : contains wrong TypePks: [8796133949522, 8796133982290]--
DELETE FROM cronjobs  WHERE TypePkString = 8796133949522 ;-- SessionEventsRemovalCronJob --
DELETE FROM cronjobs  WHERE TypePkString = 8796133982290 ;-- EdiExportCronJob --

-- Table deliverymodes : contains wrong TypePks: [8796133294162] --
DELETE FROM deliverymodes  WHERE TypePkString = 8796133294162 ;-- ExternalDeliveryMode --

-- Table externalcompany : contains wrong TypePks: [8796133687378] --
DELETE FROM externalcompany WHERE TypePkString = 8796133687378 ;-- ExternalCompany --

-- Table jobs : contains wrong TypePks: [8796133851218] --
DELETE FROM jobs WHERE TypePkString = 8796133851218 ;-- WileyHotFolderImpExImportJob --

-- Table namesuffixes : contains wrong TypePks: [8796132474962] --
DELETE FROM namesuffixes WHERE TypePkString = 8796132474962 ;-- NameSuffix --

-- Table oauthclientdetails : contains wrong TypePks: [8796135096402] --
DELETE FROM oauthclientdetails WHERE TypePkString = 8796135096402 ;-- OAuthClientDetails --

-- Table schools : contains wrong TypePks: [8796132507730] --
DELETE FROM schools  WHERE TypePkString = 8796132507730 ; -- School --

-- Table solrindex : contains wrong TypePks: [8796135358546] --
DELETE FROM solrindex WHERE TypePkString = 8796135358546 ; -- SolrIndex --

-- Table solrindexoperation : contains wrong TypePks: [8796135424082] --
DELETE FROM solrindexoperation  WHERE TypePkString = 8796135424082 ; -- SolrIndexOperation --

-- Table worldregion : contains wrong TypePks: [8796133326930] --
DELETE FROM worldregion  WHERE TypePkString = 8796133326930 ; -- WorldRegion --

-- Table usergroups : contains wrong TypePks: [8796132245586] --
DELETE FROM usergroups  WHERE TypePkString = 8796132245586 ; -- BackofficeRole --

-- Table validationconstraints : contains wrong TypePks: [8796134768722, 8796134735954] --
DELETE FROM validationconstraints  WHERE TypePkString = 8796134768722 ; -- B2BCustomerEmailConstraint --
DELETE FROM validationconstraints  WHERE TypePkString = 8796134735954 ; -- B2BUnitActiveConstraint --

-- Table wproduct2subscrtermrel : contains wrong TypePks: [8796133752914] --
DELETE FROM wproduct2subscrtermrel WHERE TypePkString = 8796133752914 ; -- WileyProduct2SubscriptionTermRelation --

-- Table enumerationvalues : contains wrong TypePks: [8796130771026, 8796131262546] --
DELETE FROM enumerationvalues WHERE TypePkString = 8796130771026 ; -- robotsMetaTag --
DELETE FROM enumerationvalues WHERE TypePkString = 8796131262546 ; -- KpmgFunction --

-- Table useraudit : contains wrong TypePks: [8796132769874] --
-- ??? --
-- Table processes : contains wrong TypePks: [8796130508882, 8796130574418] --
-- ??? --
-- Table migrationrelease : contains wrong TypePks: [8796130836562] --
-- ??? --


-- Table searchrestrictions : contains wrong TypePks: [8796131786834, 8796132311122, 8796132573266, 8796133195858, 8796137193554, 8796137685074]--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796133195858; -- Fix for RestrictedType B2BCostCenter !!!!!--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796131786834; -- Fix for RestrictedType B2BUnit--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796132311122; -- Fix for RestrictedType B2BUserGroup--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796132573266; -- Fix for RestrictedType B2BCustomer--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796137685074; -- Fix for RestrictedType B2BPermission--
DELETE FROM searchrestrictions WHERE RestrictedType = 8796137193554; -- Fix for RestrictedType B2BBudget--



-- Missing link from ContentSlot to component --
-- 8797502080060	B2cSiteLogoComponent --
-- 8797503685692	ContinueShoppingLink
-- 8797503816764	HupCheckOutFooterComponent
-- 8797503849532	HupFooterComponent
-- 8797503882300	Wileyb2cFooterComponent
-- 8797503915068	Wileyb2cCheckOutFooterComponent
-- 8797505946684	PayPalMiniCart
-- 8797505979452	PayPalCheckoutComponent
-- 8797505979452	PayPalCheckoutComponent
-- 8797509223484	WileycomCheckOutFooterComponent
-- 8797509256252	WileycomFooterComponent
-- 8797509551164	B2cSiteLogoComponent
-- 8797509583932	PayPalMiniCart
-- 8797509682236	Wileyb2cFooterComponent
-- 8797510337596	HupFooterComponent
-- 8797510534204	Wileyb2cCheckOutFooterComponent
-- 8797510566972	HupCheckOutFooterComponent
-- 8797511484476	ContinueShoppingLink
-- 8797512369212	PayPalCheckoutComponent
-- 8797512369212	PayPalCheckoutComponent
-- 8797513876540	WileycomCheckOutFooterComponent
-- 8797513909308	WileycomFooterComponent
DELETE FROM elements4slots WHERE TargetPK = 8797502080060;
DELETE FROM elements4slots WHERE TargetPK = 8797503685692;
DELETE FROM elements4slots WHERE TargetPK = 8797503816764;
DELETE FROM elements4slots WHERE TargetPK = 8797503849532;
DELETE FROM elements4slots WHERE TargetPK = 8797503882300;
DELETE FROM elements4slots WHERE TargetPK = 8797503915068;
DELETE FROM elements4slots WHERE TargetPK = 8797505946684;
DELETE FROM elements4slots WHERE TargetPK = 8797505979452;
DELETE FROM elements4slots WHERE TargetPK = 8797509223484;
DELETE FROM elements4slots WHERE TargetPK = 8797509256252;
DELETE FROM elements4slots WHERE TargetPK = 8797509551164;
DELETE FROM elements4slots WHERE TargetPK = 8797509583932;
DELETE FROM elements4slots WHERE TargetPK = 8797509682236;
DELETE FROM elements4slots WHERE TargetPK = 8797510337596;
DELETE FROM elements4slots WHERE TargetPK = 8797510534204;
DELETE FROM elements4slots WHERE TargetPK = 8797510566972;
DELETE FROM elements4slots WHERE TargetPK = 8797511484476;
DELETE FROM elements4slots WHERE TargetPK = 8797512369212;
DELETE FROM elements4slots WHERE TargetPK = 8797513876540;
DELETE FROM elements4slots WHERE TargetPK = 8797513909308;

-- Missing link from CMSNavNode to component --

DELETE FROM cmslinksfornavnodes WHERE SourcePK = 8797503652924;
DELETE FROM cmslinksfornavnodes WHERE SourcePK = 8797503685692;
DELETE FROM cmslinksfornavnodes WHERE SourcePK = 8797511451708;
DELETE FROM cmslinksfornavnodes WHERE SourcePK = 8797511484476;

-- Delete Trigger without assigned Tasks''

DELETE FROM triggerscj where PK = 8796158886390; -- for update-wileyb2cIndex-cronJob --
DELETE FROM triggerscj where PK = 8796158984694; -- for update-wileyb2bIndex-cronJob --
DELETE FROM triggerscj where PK = 8796256895478; -- for ediPPExportJob --
DELETE FROM triggerscj where PK = 8796256928246; -- for ediCCExportJob --
















