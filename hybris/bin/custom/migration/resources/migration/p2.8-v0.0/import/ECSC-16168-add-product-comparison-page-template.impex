$contentCatalog = welContentCatalog
$contentCVStaged = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]
$contentCVOnline = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Online])[default = $contentCatalog:Online]
$classificationCatalog = WELClassification

# Import modulegen config properties into impex macros
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$jarResourceCms = $config-jarResourceCmsValue

INSERT_UPDATE ComponentTypeGroup; code[unique = true]
; welproductcomparison
; welproductlist

INSERT_UPDATE ComponentTypeGroups2ComponentType; source(code)[unique = true]; target(code)[unique = true]
; welproductcomparison ; CMSParagraphComponent
; welproductcomparison ; VariantProductComparisonTableComponent
; welproductlist       ; CMSParagraphComponent
; welproductlist       ; VariantProductListComponent

INSERT_UPDATE PageTemplate; $contentCVStaged[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; ProductComparisonPageTemplate        ; Product Comparison Page Template          ; layout/contentProductComparisonPage ; ContentPage      ; true  ;
INSERT_UPDATE PageTemplate; $contentCVOnline[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; ProductComparisonPageTemplate        ; Product Comparison Page Template          ; layout/contentProductComparisonPage ; ContentPage      ; true  ;

UPDATE PageTemplate; $contentCVStaged[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; ProductComparisonPageTemplate        ; $jarResourceCms/structure-view/wel/structure_productComparisonPageTemplate.vm
UPDATE PageTemplate; $contentCVOnline[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; ProductComparisonPageTemplate        ; $jarResourceCms/structure-view/wel/structure_productComparisonPageTemplate.vm

# Product Comparison Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCVStaged)[unique = true][default = 'ProductComparisonPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo              ; ; SimpleResponsiveBannerComponent ;
; NavigationBar         ; ;                                 ; navigation
; LiveChat              ; ;                                 ; navigation
; BrandLogo             ; ; SimpleResponsiveBannerComponent ;
; CategoryNavigation    ; ;                                 ; navigation
; SocialLinksSlot       ; ; SocialMediaFollowComponent      ;
; ProductComparisonSlot ; ;                                 ; welproductcomparison
; ProductListSlot       ; ;                                 ; welproductlist
; Footer                ; ;                                 ; footer
; CompanyLogoSlot       ; ; SimpleResponsiveBannerComponent ;
; TopHeaderSlot         ; ;                                 ; wide
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCVOnline)[unique = true][default = 'ProductComparisonPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo              ; ; SimpleResponsiveBannerComponent ;
; NavigationBar         ; ;                                 ; navigation
; LiveChat              ; ;                                 ; navigation
; BrandLogo             ; ; SimpleResponsiveBannerComponent ;
; CategoryNavigation    ; ;                                 ; navigation
; SocialLinksSlot       ; ; SocialMediaFollowComponent      ;
; ProductComparisonSlot ; ;                                 ; welproductcomparison
; ProductListSlot       ; ;                                 ; welproductlist
; Footer                ; ;                                 ; footer
; CompanyLogoSlot       ; ; SimpleResponsiveBannerComponent ;
; TopHeaderSlot         ; ;                                 ; wide

INSERT_UPDATE ContentSlotForTemplate; $contentCVStaged[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCVStaged)[unique = true][default = 'ProductComparisonPageTemplate']; contentSlot(uid, $contentCVStaged)[unique = true]; allowOverwrite
; ; SiteLogo-ProductComparisonPage           ; SiteLogo                    ; ; SiteLogoSlot                    ; true
; ; NavigationBar-ProductComparisonPage      ; NavigationBar               ; ; NavigationBarSlot               ; true
; ; CurrencySelector-ProductComparisonPage   ; CurrencySelector            ; ; CurrencySelectorSlot            ; true
; ; LiveChat-ProductComparisonPage           ; LiveChat                    ; ; LiveChatNavigationSlot          ; true
; ; BrandLogo-ProductComparisonPage          ; BrandLogo                   ; ; BrandLogoSlot                   ; true
; ; CategoryNavigation-ProductComparisonPage ; CategoryNavigation          ; ; CategoryNavigationSlot          ; true
; ; Footer-ProductComparisonPage             ; Footer                      ; ; FooterSlot                      ; true
; ; CompanyLogo-ProductComparisonPage        ; CompanyLogo                 ; ; CompanyLogoSlot                 ; true
; ; TopHeaderSlot-ProductComparisonPage      ; TopHeaderSlot               ; ; TopHeaderSlot                   ; true
INSERT_UPDATE ContentSlotForTemplate; $contentCVOnline[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCVOnline)[unique = true][default = 'ProductComparisonPageTemplate']; contentSlot(uid, $contentCVOnline)[unique = true]; allowOverwrite
; ; SiteLogo-ProductComparisonPage           ; SiteLogo                    ; ; SiteLogoSlot                    ; true
; ; NavigationBar-ProductComparisonPage      ; NavigationBar               ; ; NavigationBarSlot               ; true
; ; CurrencySelector-ProductComparisonPage   ; CurrencySelector            ; ; CurrencySelectorSlot            ; true
; ; LiveChat-ProductComparisonPage           ; LiveChat                    ; ; LiveChatNavigationSlot          ; true
; ; BrandLogo-ProductComparisonPage          ; BrandLogo                   ; ; BrandLogoSlot                   ; true
; ; CategoryNavigation-ProductComparisonPage ; CategoryNavigation          ; ; CategoryNavigationSlot          ; true
; ; Footer-ProductComparisonPage             ; Footer                      ; ; FooterSlot                      ; true
; ; CompanyLogo-ProductComparisonPage        ; CompanyLogo                 ; ; CompanyLogoSlot                 ; true
; ; TopHeaderSlot-ProductComparisonPage      ; TopHeaderSlot               ; ; TopHeaderSlot                   ; true

INSERT_UPDATE ClassificationSystemVersion; catalog(id)[unique = true]; version[unique = true]; writePrincipals(uid)[mode = append]
; $classificationCatalog ; 1.0 ; cmsmanagergroup