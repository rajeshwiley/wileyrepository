# Has been increased p_name column size due of the containing both first (varchar255) and last (varchar255) names into this columns
ALTER TABLE users MODIFY COLUMN p_name varchar(512)