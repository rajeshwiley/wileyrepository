import com.wiley.core.model.WileyPurchaseOptionProductModel
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel
import de.hybris.platform.catalog.model.classification.ClassificationClassModel
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel
import de.hybris.platform.classification.ClassificationService
import de.hybris.platform.classification.ClassificationSystemService
import de.hybris.platform.classification.features.*
import de.hybris.platform.commerceservices.setup.SetupImpexService
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.i18n.I18NService
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import groovy.transform.Field
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wiremock.org.apache.commons.collections4.CollectionUtils

import java.util.stream.Collectors

import static org.apache.commons.collections.CollectionUtils.intersection
import static org.apache.commons.collections.CollectionUtils.isNotEmpty

@Field final Logger LOG = LoggerFactory.getLogger("transform_classification");
@Field final String SYSTEM_ID = "WileyMediumClassificationCatalog";
@Field final String SYSTEM_VERSION = "1.0";
@Field final String FIND_ALL_PRODUCTS_BY_CATALOG_QUERY = "SELECT {p.PK} FROM " +
        "{WileyPurchaseOptionProduct as p " +
        "JOIN CatalogVersion as cv ON {p.catalogVersion}={cv.PK} " +
        "JOIN Catalog as c on {cv.catalog} = {c.PK}}" +
        "WHERE {c.id}='wileyProductCatalog'"
@Field final String FIND_CLASS_ATTRIBUTE_ASSIGNMENT_BY_CLASS_AND_ATTR_QUERY = "SELECT {a.PK} FROM " +
        "{ClassAttributeAssignment as a " +
        "JOIN ClassificationClass as c on {a.classificationClass} = {c.PK} " +
        "JOIN ClassificationAttribute as attr on {a.classificationAttribute} = {attr.PK}} " +
        "WHERE {c.code}=?classCode AND {attr.code} = ?attributeCode"
@Field final String FIND_PRODUCT_FEATURES_BY_ASSIGNMENT_QUERY = "SELECT {PK} FROM {ProductFeature} " +
        "WHERE {classificationAttributeAssignment} = ?assignment"

@Field final ClassificationService classificationService = (ClassificationService) spring.getBean("classificationService");
@Field final ClassificationSystemService classificationSystemService = (ClassificationSystemService) spring.getBean("classificationSystemService");
@Field final ModelService modelService = (ModelService) spring.getBean("modelService");
@Field final I18NService i18NService = (I18NService) spring.getBean("i18NService");
@Field final FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean("flexibleSearchService");
@Field final SetupImpexService setupImpexService = (SetupImpexService) spring.getBean("setupImpexService");

@Field ClassificationSystemVersionModel systemVersion = this.classificationSystemService.getSystemVersion(this.SYSTEM_ID, this.SYSTEM_VERSION);
@Field Map<ClassAttributeAssignmentModel, ClassAttributeAssignmentModel> classAttributeAssignmentMap = new HashMap<>();

this.createNewClassificationHierarchy();
this.transformClassificationHierarhy();
this.updateClassificationSolrIndexedProperties();

void createNewClassificationHierarchy() {
    this.setupImpexService.importImpexFile("/migration/p3.5-v0.1/import/ECSC-22463-create-new-classification-hierarchy.impex", true);
}

void transformClassificationHierarhy() {
    LOG.info("Start classifications transformation process");
    this.initClassAttributeAssignmentMap();
    
    this.updateAllClassAttributeAssignmentsWithNewClass("journal", "journalAttributes");
    this.updateProducts(this.FIND_ALL_PRODUCTS_BY_CATALOG_QUERY);
    this.removeRedundantClassAttributeAssignments();
    LOG.info("Finished classifications transformation process");
}

void updateClassificationSolrIndexedProperties() {
    this.setupImpexService.importImpexFile("/migration/p3.5-v0.1/import/ECSC-22463-update-classification-solr-idexed-properties.impex", true);
}

void initClassAttributeAssignmentMap() {
    LOG.info("Start classAttributeAssignmentMapping initialization")
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "book", "bookAttributes", "isbn13");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "book", "bookAttributes", "publicationDate");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "book", "bookAttributes", "numberOfPages");

    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "course", "bookAttributes", "isbn13");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "course", "bookAttributes", "publicationDate");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "course", "bookAttributes", "numberOfPages");

    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "labProtocols", "bookAttributes", "isbn13");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "labProtocols", "bookAttributes", "publicationDate");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "labProtocols", "bookAttributes", "numberOfPages");

    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "media", "bookAttributes", "isbn13");
    this.addClassAttributeAssignmentMapping(this.classAttributeAssignmentMap, "media", "bookAttributes", "publicationDate");
    LOG.info("Finished classAttributeAssignmentMapping initialization")
}

void addClassAttributeAssignmentMapping(
        final Map map, final String oldClassCode, final String newClassCode, final String attrCode) {
    ClassAttributeAssignmentModel key = this.findClassAttributeAssignmentByClassAndAttr(oldClassCode, attrCode);
    ClassAttributeAssignmentModel value = this.findClassAttributeAssignmentByClassAndAttr(newClassCode, attrCode)
    if (key != null & value != null) {
        map.put(key, value);
        LOG.info("Added mapping {}:{} -> {}:{}", oldClassCode, attrCode, newClassCode, attrCode);
    } else {
        LOG.info("ClassAttributeAssignment is null for old or new class code:  {}:{}, {}:{}", oldClassCode, attrCode, newClassCode, attrCode);
    }
}

ClassAttributeAssignmentModel findClassAttributeAssignmentByClassAndAttr(final String classCode, final String attributeCode) {
    Map<String, String> params = new HashMap<>();
    params.put("classCode", classCode);
    params.put("attributeCode", attributeCode);
    List<ClassAttributeAssignmentModel> result = this.flexibleSearchService.search(this.FIND_CLASS_ATTRIBUTE_ASSIGNMENT_BY_CLASS_AND_ATTR_QUERY, params).getResult();
    if (CollectionUtils.isNotEmpty(result)) {
        return this.flexibleSearchService.search(this.FIND_CLASS_ATTRIBUTE_ASSIGNMENT_BY_CLASS_AND_ATTR_QUERY, params).getResult().get(0);
    } else {
        return null;
    }
}

boolean isProductFeaturesListForAssignmentEmpty(final ClassAttributeAssignmentModel assignment) {
    this.flexibleSearchService.search(this.FIND_PRODUCT_FEATURES_BY_ASSIGNMENT_QUERY, Collections.singletonMap("assignment", assignment)).getTotalCount() == 0;
}

void updateAllClassAttributeAssignmentsWithNewClass(
        final String currentClassificationClassCode, final String newClassificationClassCode) {
    LOG.info("Start updating classAttributeAssignment for class [{}] with new one [{}]", currentClassificationClassCode, newClassificationClassCode);
    ClassificationClassModel currentClassificationClass = this.classificationSystemService.getClassForCode(this.systemVersion, currentClassificationClassCode);
    ClassificationClassModel newClassificationClass = this.classificationSystemService.getClassForCode(this.systemVersion, newClassificationClassCode);
    List<ClassAttributeAssignmentModel> classAttributeAssignments = currentClassificationClass.getAllClassificationAttributeAssignments();
    for (ClassAttributeAssignmentModel classAttributeAssignment : classAttributeAssignments) {
        LOG.info("Updating classificationClass for classAttributeAssignment with attribute [{}]", classAttributeAssignment.getClassificationAttribute().getCode());
        classAttributeAssignment.setClassificationClass(newClassificationClass);
    }
    LOG.info("Finished updating classAttributeAssignment");
    this.modelService.saveAll(classAttributeAssignments);
    LOG.info("Saved changed classAttributeAssignments");
}


void updateProducts(final String query) {
    LOG.info("Start products updating");
    List<WileyPurchaseOptionProductModel> products = this.flexibleSearchService.search(query).getResult();
    LOG.info("Found [{}] products to be processed", products.size());
    products.forEach({ product -> this.updateProductFeatures(product) })
    LOG.info("Finished products updating");
}

void updateProductFeatures(final ProductModel product) {
    if (this.isProductFeaturesUpdateRequired(product)) {
        LOG.info("Start updating attributes for product [{}]", product.getCode());
        List<Feature> features = this.classificationService.getFeatures(product).getFeatures()
                .stream()
                .map({ feature -> this.updateFeatureClassAttributeAssignment(feature) })
                .collect(Collectors.toList())

        this.classificationService.replaceFeatures(product, new FeatureList(features));
        LOG.info("Updated attributes for product [{}]", product.getCode());
    } else {
        LOG.info("Attributes update isn't required for product [{}]", product.getCode());
    }
}

boolean isProductFeaturesUpdateRequired(final ProductModel product) {
    final Set<ClassAttributeAssignmentModel> productClassAttributeAssignments = this.classificationService.getFeatures(product).getClassAttributeAssignments();
    return isNotEmpty(intersection(productClassAttributeAssignments, this.classAttributeAssignmentMap.keySet()));
}

Feature updateFeatureClassAttributeAssignment(final Feature feature) {
    final ClassAttributeAssignmentModel classAttributeAssignment = feature.getClassAttributeAssignment();
    final Locale currentLocale = this.i18NService.getCurrentLocale();
    Feature newFeature = null;
    if (this.classAttributeAssignmentMap.containsKey(classAttributeAssignment)) {
        final ClassAttributeAssignmentModel newAssignment = this.classAttributeAssignmentMap.get(classAttributeAssignment);
        LOG.info("Copy attribute [{}]: {}:{}->{}:{}", feature.getCode(),
                classAttributeAssignment.getClassificationClass().getCode(), classAttributeAssignment.getClassificationAttribute().getCode(),
                newAssignment.getClassificationClass().getCode(), newAssignment.getClassificationAttribute().getCode());
        if (feature instanceof LocalizedFeature) {
            final Map<Locale, List<FeatureValue>> values = ((LocalizedFeature) feature).getValuesForAllLocales();
            newFeature = new LocalizedFeature(newAssignment, this.createNewLocalizedFeatureValues(values), currentLocale);
        } else if (feature instanceof UnlocalizedFeature) {
            final List<FeatureValue> values = feature.getValues();
            newFeature = new UnlocalizedFeature(newAssignment, this.createNewUnlocalizedFeatureValues(values));
        }
    } else {
        LOG.info("No update is required for attribute [{}], classAttributeAssignment [{}:{}]", feature.getCode(),
                classAttributeAssignment.getClassificationClass().getCode(), classAttributeAssignment.getClassificationAttribute().getCode())
        newFeature = feature;
    }
    return newFeature;
}

Map<Locale, List<FeatureValue>> createNewLocalizedFeatureValues(Map<Locale, List<FeatureValue>> values) {
    final Map<Locale, List<FeatureValue>> newValues = new HashMap<>();
    values.entrySet().forEach({ entry ->
        final List<FeatureValue> newValuesForLocale = new ArrayList<>();
        entry.getValue().forEach({ featureValue ->
            newValuesForLocale.add(new FeatureValue(featureValue.getValue(), featureValue.getDescription(), featureValue.getUnit()));
        })
        newValues.put(entry.getKey(), newValuesForLocale);
    });
    return newValues;
}

List<FeatureValue> createNewUnlocalizedFeatureValues(List<FeatureValue> values) {
    final List<FeatureValue> newValues = new ArrayList<>();
    values.forEach({ featureValue ->
        newValues.add(new FeatureValue(featureValue.getValue(), featureValue.getDescription(), featureValue.getUnit()));
    })
    return newValues;
}

void removeRedundantClassAttributeAssignments() {
    LOG.info("Start removing redundant classAttributeAssignments");
    this.classAttributeAssignmentMap.keySet().forEach({ assignment ->
        String classCode = assignment.getClassificationClass().getCode();
        String attributeCode = assignment.getClassificationAttribute().getCode();
        if (this.isProductFeaturesListForAssignmentEmpty(assignment)) {
            this.modelService.remove(assignment);
            LOG.info("Removed assignment {}:{}", classCode, attributeCode);
        } else {
            LOG.error("There are productFeatures for assignment {}:{}. Skip removing", classCode, attributeCode);
        }
    });
    LOG.info("Finished removing");
}
