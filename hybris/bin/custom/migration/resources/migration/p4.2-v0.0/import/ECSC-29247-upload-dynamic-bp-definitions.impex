INSERT_UPDATE DynamicProcessDefinition; code[unique = true]; active; content;
                                      ; ags-order-process  ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='checkOrder' name='ags-order-process'
         processClass='com.wiley.core.model.WileyOrderProcessModel'>

    <action id='checkOrder' bean='wileyCheckOrderAction'>
        <transition name='OK' to='createSubscription'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='createSubscription' bean='createWileySubscriptionAction'>
        <transition name='OK' to='sendOrderPlacedNotification'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='orderFailedNotification' bean='wileyOrderFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <action id='sendOrderPlacedNotification' bean='sendOrderPlacedNotificationAction'>
        <transition name='TAKE_PAYMENT' to='waitForTakePayment'/>
        <transition name='SKIP_PAYMENT' to='exportOrder'/>
    </action>

    <wait id='waitForTakePayment' then='takePayment' prependProcessCode='false'>
        <event>${process.code}_WaitForTakePayment</event>
        <timeout delay='PT5M' then='takePayment'/>
    </wait>

    <action id='takePayment' bean='takePaymentAction'>
        <transition name='OK' to='exportOrder'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='exportOrder' bean='startOrderExportAction'>
        <transition name='OK' to='waitForOrderExport'/>
    </action>

    <wait id='waitForOrderExport' then='isOrderExportCompleted' prependProcessCode='false'>
        <event>${process.code}_WileyExportSubprocessEnd</event>
    </wait>

    <action id='isOrderExportCompleted' bean='orderExportCompletedAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='waitForOrderExport'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]              ; active; content;
                                      ; ags-subscription-renewal-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='cloneCart' name='ags-subscription-renewal-process'
         processClass='com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel'>

    <action id='cloneCart' bean='cloneCartAction'>
        <transition name='OK' to='calculateCart'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='calculateCart' bean='calculateCartAction'>
        <transition name='OK' to='authorizePayment'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='authorizePayment' bean='authorizePaymentAction'>
        <transition name='OK' to='placeOrder'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='placeOrder' bean='placeOrderAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='orderFailedNotification' bean='orderFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]; active; content;
                                      ; as-order-process   ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='checkOrder' name='as-order-process'
         processClass='com.wiley.core.model.WileyOrderProcessModel'>

    <action id='checkOrder' bean='wileyasCheckOrderAction'>
        <transition name='OK' to='takePayment'/>
        <transition name='NOK' to='orderFailedNotification'/>
        <transition name='SKIP_PAYMENT' to='exportOrder'/>
    </action>

    <action id='takePayment' bean='wileyasTakePaymentAction'>
        <transition name='OK' to='refundPayment'/>
        <transition name='NOK' to='orderFailedNotification'/>
        <transition name='SKIP_REFUND' to='exportOrder'/>
    </action>

    <action id='refundPayment' bean='wileyasRefundPaymentAction'>
        <transition name='OK' to='exportOrder'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='exportOrder' bean='wileyPublishOrderAction'>
        <transition name='OK' to='deactivateProcess'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='deactivateProcess' bean='deactivateProcessAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='orderFailedNotification' bean='wileyOrderFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true] ; active; content;
                                      ; consignment-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='waitBeforeTransmission' name='consignment-process'
         processClass='de.hybris.platform.ordersplitting.model.ConsignmentProcessModel'>

    <action id='waitBeforeTransmission' bean='waitBeforeTransmissionAction'>
        <transition name='NOK' to='waitBeforeTransmission'/>
        <transition name='OK' to='sendConsignmentToWarehouse'/>
    </action>

    <action id='sendConsignmentToWarehouse' bean='sendConsignmentToWarehouseAction'>
        <transition name='OK' to='waitForWarehouse'/>
    </action>

    <wait id='waitForWarehouse' then='receiveConsignmentStatus' prependProcessCode='false'>
        <event>${process.code}_WaitForWarehouse</event>
    </wait>

    <action id='receiveConsignmentStatus' bean='receiveConsignmentStatusAction'>
        <transition name='OK' to='allowShipment'/>
        <transition name='CANCEL' to='cancelConsignment'/>
        <transition name='ERROR' to='error'/>
    </action>

    <action id='allowShipment' bean='allowShipmentAction'>
        <transition name='DELIVERY' to='sendDeliveryMessage'/>
        <transition name='PICKUP' to='sendReadyForPickupMessage'/>
        <transition name='ERROR' to='error'/>
        <transition name='CANCEL' to='cancelConsignment'/>
    </action>

    <action id='sendDeliveryMessage' bean='sendDeliveryMessageAction'>
        <transition name='OK' to='subprocessEnd'/>
    </action>

    <action id='sendReadyForPickupMessage' bean='sendReadyForPickupMessageAction'>
        <transition name='OK' to='waitForPickup'/>
    </action>

    <action id='cancelConsignment' bean='cancelConsignmentAction'>
        <transition name='OK' to='sendCancelMessage'/>
    </action>

    <action id='sendCancelMessage' bean='sendCancelMessageAction'>
        <transition name='OK' to='subprocessEnd'/>
    </action>

    <wait id='waitForPickup' then='confirmConsignmentPickup' prependProcessCode='false'>
        <event>${process.code}_ConsignmentPickup</event>
    </wait>

    <action id='confirmConsignmentPickup' bean='confirmConsignmentPickupAction'>
        <transition name='OK' to='sendPickedUpMessage'/>
        <transition name='CANCEL' to='cancelConsignment'/>
        <transition name='ERROR' to='error'/>
    </action>

    <action id='sendPickedUpMessage' bean='sendPickedUpMessageAction'>
        <transition name='OK' to='subprocessEnd'/>
    </action>

    <action id='subprocessEnd' bean='subprocessEndAction'>
        <transition name='OK' to='success'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>
</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]            ; active; content;
                                      ; export-customer-to-cdm-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='cdmCreateCustomer' name='export-customer-to-cdm-process'
         processClass='com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel'>

    <action id='cdmCreateCustomer' bean='cdmCreateCustomerAction'>
        <transition name='OK' to='cdmProcessEnd'/>
        <transition name='NOK' to='cdmFailedNotification'/>
    </action>

    <action id='cdmProcessEnd' bean='cdmProcessEndAction'>
        <transition name='OK' to='success'/>
    </action>

    <action id='cdmFailedNotification' bean='cdmFailedNotificationAction'>
        <transition name='OK' to='error'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='success' state='SUCCEEDED'>Export has finished successfully.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]            ; active; content;
                                      ; export-order-to-eloqua-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='eloquaProcessStart' name='export-order-to-eloqua-process'
         processClass='com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel'>

    <action id='eloquaProcessStart' bean='eloquaProcessStartAction'>
        <transition name='ELOQUA_ID_NOT_EXIST' to='searchForEloquaContact'/>
        <transition name='ELOQUA_ID_EXISTS' to='updateEloquaContact'/>
    </action>

    <action id='searchForEloquaContact' bean='searchForEloquaContactAction'>
        <transition name='FOUND' to='updateEloquaContact'/>
        <transition name='NOT_FOUND' to='createEloquaContact'/>
        <transition name='FAILURE' to='eloquaFailedNotification'/>
    </action>

    <action id='createEloquaContact' bean='createEloquaContactAction'>
        <transition name='OK' to='createEloquaTransactionalRecord'/>
        <transition name='NOK' to='eloquaFailedNotification'/>
    </action>

    <action id='updateEloquaContact' bean='updateEloquaContactAction'>
        <transition name='OK' to='createEloquaTransactionalRecord'/>
        <transition name='NOK' to='eloquaFailedNotification'/>
    </action>

    <action id='createEloquaTransactionalRecord' bean='createEloquaTransactionalRecordAction'>
        <transition name='OK' to='eloquaProcessEnd'/>
        <transition name='NOK' to='eloquaFailedNotification'/>
    </action>

    <action id='eloquaProcessEnd' bean='eloquaProcessEndAction'>
        <transition name='OK' to='success'/>
    </action>

    <action id='eloquaFailedNotification' bean='eloquaFailedNotificationAction'>
        <transition name='OK' to='error'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='success' state='SUCCEEDED'>Export has finished successfully.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]               ; active; content;
                                      ; export-order-to-wileycore-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='wileyCoreProcessStart' name='export-order-to-wileycore-process'
         processClass='com.wiley.fulfilmentprocess.model.WileyCoreOrderExportProcessModel'>

    <action id='wileyCoreProcessStart' bean='wileyCoreProcessStartAction'>
        <transition name='OK' to='wileyCoreProcessPinActivation'/>
        <transition name='NOK' to='wileyCoreProcessPinActivationEnd'/>
    </action>

    <action id='wileyCoreProcessPinActivation' bean='wileyCoreProcessPinActivationAction'>
        <transition name='OK' to='wileyCoreProcessPinActivationEnd'/>
        <transition name='NOK' to='error'/>
    </action>

    <action id='wileyCoreProcessPinActivationEnd' bean='wileyCoreProcessPinActivationEnd'>
        <transition name='OK' to='success'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='success' state='SUCCEEDED'>Export has finished successfully.</end>
</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]; active; content;
                                      ; export-subprocess  ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='exportProcessStart' name='export-subprocess'
         processClass='com.wiley.fulfilmentprocess.model.WileyExportProcessModel'>

    <action id='exportProcessStart' bean='exportProcessStartAction'>
        <transition name='OK' to='waitForExport'/>
    </action>

    <wait id='waitForExport' then='exportProcessEnd' prependProcessCode='false'>
        <event>${process.code}_WileyOrderExported</event>
    </wait>

    <action id='exportProcessEnd' bean='exportProcessEndAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='error'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='success' state='SUCCEEDED'>Order exported.</end>

</process>"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]; active; content;
                                      ; order-process      ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='checkOrder' name='order-process'
         processClass='com.wiley.core.model.WileyOrderProcessModel'>

    <action id='checkOrder' bean='checkOrderAction'>
        <transition name='OK' to='checkAuthorizeOrderPayment'/>
        <transition name='NOK' to='error'/>
    </action>

    <action id='checkAuthorizeOrderPayment' bean='checkAuthorizeOrderPaymentAction'>
        <transition name='OK' to='reserveAmount'/>
        <transition name='NOK' to='authorizationFailedNotification'/>
    </action>

    <action id='reserveAmount' bean='reserveOrderAmountAction'>
        <transition name='OK' to='checkTransactionReviewStatus'/>
        <transition name='NOK' to='sendPaymentFailedNotification'/>
    </action>

    <action id='checkTransactionReviewStatus' bean='checkTransactionReviewStatusAction'>
        <transition name='OK' to='fraudCheck'/>
        <transition name='NOK' to='notifyCustomer'/>
        <transition name='WAIT' to='waitForReviewDecision'/>
    </action>

    <wait id='waitForReviewDecision' then='checkTransactionReviewStatus' prependProcessCode='false'>
        <event>${process.order.code}_ReviewDecision</event>
    </wait>

    <action id='fraudCheck' bean='fraudCheckOrderInternalAction'>
        <transition name='OK' to='sendOrderPlacedNotification'/>
        <transition name='POTENTIAL' to='manualOrderCheckCSA'/>
        <transition name='FRAUD' to='notifyCustomer'/>
    </action>

    <action id='manualOrderCheckCSA' bean='prepareOrderForManualCheckAction'>
        <transition name='OK' to='waitForManualOrderCheckCSA'/>
    </action>

    <wait id='waitForManualOrderCheckCSA' then='orderManualChecked' prependProcessCode='false'>
        <event>${process.code}_CSAOrderVerified</event>
    </wait>

    <action id='orderManualChecked' bean='orderManualCheckedAction'>
        <transition name='OK' to='sendOrderPlacedNotification'/>
        <transition name='NOK' to='waitForCleanUp'/>
        <transition name='UNDEFINED' to='waitForManualOrderCheckCSA'/>
    </action>

    <action id='notifyCustomer' bean='notifyCustomerAboutFraudAction'>
        <transition name='OK' to='waitForCleanUp'/>
    </action>

    <wait id='waitForCleanUp' then='scheduleForCleanUp' prependProcessCode='false'>
        <event>${process.code}_CleanUpEvent</event>
    </wait>

    <action id='scheduleForCleanUp' bean='scheduleForCleanUpAction'>
        <transition name='OK' to='cancelOrder'/>
        <transition name='NOK' to='orderManualChecked'/>
    </action>

    <action id='sendOrderPlacedNotification' bean='sendOrderPlacedNotificationAction'>
        <transition name='TAKE_PAYMENT' to='splitOrder'/>
        <transition name='SKIP_PAYMENT' to='splitOrder'/>
    </action>

    <action id='cancelOrder' bean='cancelWholeOrderAuthorizationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <action id='authorizationFailedNotification' bean='sendAuthorizationFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <action id='sendPaymentFailedNotification' bean='sendPaymentFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <action id='splitOrder' bean='splitOrderAction'>
        <transition name='OK' to='waitForWarehouseSubprocessEnd'/>
    </action>

    <wait id='waitForWarehouseSubprocessEnd' then='isProcessCompleted' prependProcessCode='false'>
        <event>${process.code}_ConsignmentSubprocessEnd</event>
    </wait>

    <action id='isProcessCompleted' bean='subprocessesCompletedAction'>
        <transition name='OK' to='sendOrderCompletedNotification'/>
        <transition name='NOK' to='waitForWarehouseSubprocessEnd'/>
    </action>

    <action id='sendOrderCompletedNotification' bean='sendOrderCompletedNotificationAction'>
        <transition name='OK' to='success'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>

</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]     ; active; content;
                                      ; create-ebp-user-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='createEBPUserAction' name='create-ebp-user-process'
         processClass='com.wiley.fulfilmentprocess.model.CreateEbpUserProcessModel'>

    <action id='createEBPUserAction' bean='createEBPUserAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='failed'/>
    </action>

    <end id='success' state='SUCCEEDED'>User details was sent to ebp.</end>
    <end id='failed' state='FAILED'>EBP create User fail.</end>
</process>"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]; active; content;
                                      ; wel-order-process  ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='isPreOrder' name='wel-order-process'
         processClass='com.wiley.core.model.WileyOrderProcessModel'>

    <action id='isPreOrder' bean='isPreOrderAction'>
        <transition name='OK' to='preOrderNotification'/>
        <transition name='NOK' to='checkOrder'/>
    </action>

    <action id='preOrderNotification' bean='preOrderNotificationAction'>
        <transition name='OK' to='waitForPreOrder'/>
    </action>

    <wait id='waitForPreOrder' then='authorizePreOrder'>
        <case event='waitForProductActivationOrCancel'>
            <choice id='CANCELLED' then='failed'/>
        </case>
    </wait>

    <action id='authorizePreOrder' bean='authorizePreOrderAction'>
        <transition name='OK' to='checkOrder'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='checkOrder' bean='welCheckOrderAction'>
        <transition name='OK' to='notifyEBP'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='notifyEBP' bean='notifyEBPAction'>
        <transition name='OK' to='sendOrderPlacedNotification'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='sendOrderPlacedNotification' bean='sendOrderPlacedNotificationAction'>
        <transition name='TAKE_PAYMENT' to='waitForTakePayment'/>
        <transition name='SKIP_PAYMENT' to='exportOrder'/>
    </action>

    <wait id='waitForTakePayment' then='takePayment' prependProcessCode='false'>
        <event>${process.code}_WaitForTakePayment</event>
        <timeout delay='PT5M' then='takePayment'/>
    </wait>

    <action id='takePayment' bean='takePaymentAction'>
        <transition name='OK' to='exportOrder'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='exportOrder' bean='startOrderExportAction'>
        <transition name='OK' to='waitForOrderExport'/>
    </action>

    <wait id='waitForOrderExport' then='isOrderExportCompleted' prependProcessCode='false'>
        <event>${process.code}_WileyExportSubprocessEnd</event>
    </wait>

    <action id='isOrderExportCompleted' bean='orderExportCompletedAction'>
        <transition name='OK' to='success'/>
        <transition name='NOK' to='waitForOrderExport'/>
    </action>

    <action id='orderFailedNotification' bean='welOrderFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <end id='error' state='ERROR'>All went wrong.</end>
    <end id='failed' state='FAILED'>Order not placed.</end>
    <end id='success' state='SUCCEEDED'>Order placed.</end>


</process>
"

INSERT_UPDATE DynamicProcessDefinition; code[unique = true]    ; active; content;
                                      ; wileycom-order-process ; true  ; "<?xml version='1.0' encoding='utf-8'?>
<process xmlns='http://www.hybris.de/xsd/processdefinition' start='checkOrder' name='wileycom-order-process'
         processClass='com.wiley.core.model.WileyOrderProcessModel'>

    <action id='checkOrder' bean='wileyCheckOrderAction'>
        <transition name='OK' to='wileycomExportToERP'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

    <action id='wileycomExportToERP' bean='wileycomExportToERPAction'>
        <transition name='OK' to='wileycomUpdateUserProfile'/>
        <transition name='NOK' to='orderFailedNotification'/>
    </action>

	<action id='wileycomUpdateUserProfile' bean='wileycomUpdateUserProfileAction'>
        <transition name='OK' to='sendOrderPlacedNotification'/>
    </action>

    <action id='sendOrderPlacedNotification' bean='wileycomSendOrderPlacedNotificationAction'>
        <transition name='OK' to='sendOrderCompletedNotification'/>
    </action>

    <action id='sendOrderCompletedNotification' bean='sendOrderCompletedNotificationAction'>
        <transition name='OK' to='success'/>
    </action>

    <action id='orderFailedNotification' bean='wileyOrderFailedNotificationAction'>
        <transition name='OK' to='failed'/>
    </action>

    <end id='failed' state='FAILED'>Order upload to fulfilment system failed.</end>
    <end id='success' state='SUCCEEDED'>Order uploaded to fulfilment system.</end>

</process>
"