/*
 *   DO NOT FORGET SWITCH GROOVY-CONSOLE INTO COMMIT-MODE
 *
 *   Please don't expect that <sync> step will work properly.
 *   OOTB syncronization is performed in separated thread
 */
import com.epam.hybris.migration.executor.MigrationExecutor

MigrationExecutor migExecutor = (MigrationExecutor) spring.getBean("migrationExecutor");

// 1. release directory from /migration folder
// 2. any scope you defined in migrationConfig.xml
migExecutor.execute("release_0.5", "my_scope");