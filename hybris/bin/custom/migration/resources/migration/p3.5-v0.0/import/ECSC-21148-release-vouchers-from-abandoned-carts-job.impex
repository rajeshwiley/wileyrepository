"$releaseAllLegacyVouchersAndDiscountGroupFromCarts =  
import de.hybris.platform.basecommerce.model.site.BaseSiteModel
import de.hybris.platform.core.model.order.CartModel
import de.hybris.platform.europe1.enums.UserDiscountGroup
import de.hybris.platform.order.CalculationService
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import de.hybris.platform.site.BaseSiteService
import de.hybris.platform.voucher.VoucherService
import groovy.transform.Field
import org.apache.commons.collections.CollectionUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Field final Logger LOG = LoggerFactory.getLogger('release_legacy_vouchers');

@Field final BaseSiteService baseSiteService = (BaseSiteService) spring.getBean('baseSiteService');
@Field final VoucherService defaultVoucherService = (VoucherService) spring.getBean('defaultVoucherService');
@Field final FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean('flexibleSearchService');
@Field final CalculationService calculationService = (CalculationService) spring.getBean('calculationService');

@Field String FIND_ALL_CARTS_FOR_SITE_QUERY = 'SELECT {c.PK} FROM ' +
        '{Cart AS c JOIN BaseSite AS s ON {c.site} = {s.PK}} ' +
        'WHERE {s.uid} = ?siteUid';

@Field BaseSiteModel agsSite = baseSiteService.getBaseSiteForUID('ags');
@Field BaseSiteModel welSite = baseSiteService.getBaseSiteForUID('wel');

releaseAllLegacyVouchersAndDiscountGroupFromCarts();

void releaseAllLegacyVouchersAndDiscountGroupFromCarts() {
    releaseAllLegacyVouchersAndDiscountGroupForSite(agsSite);
    releaseAllLegacyVouchersAndDiscountGroupForSite(welSite);
}

private void releaseAllLegacyVouchersAndDiscountGroupForSite(BaseSiteModel site) {
    LOG.info('Starting releasing legacy vouchers and discountGroup for [{}] site', site.getUid());
    List<CartModel> carts = getAllCartsForSite(site);
    LOG.info('Found [{}] carts', carts.size());
    carts.forEach({ cart ->
    	try {
			releaseAllLegacyVouchersForCart(cart);
			resetDiscountGroupForCart(cart);
		} catch (Exception e){
			LOG.error('Error occured during releasing vouchers/discount codes for cart: ' + cart.getCode(), e);
		}
    })
    LOG.info('End releasing legacy vouchers and discountGroup for [{}] site', site.getUid());
}

private void releaseAllLegacyVouchersForCart(CartModel cart) {
    Collection<String> voucherCodes = defaultVoucherService.getAppliedVoucherCodes(cart);
    if (CollectionUtils.isNotEmpty(voucherCodes)) {
    	LOG.info('Found [{}] vouchers for cart [{}]', voucherCodes.size(), cart.getCode());
        voucherCodes.forEach({ voucherCode ->
            defaultVoucherService.releaseVoucher(voucherCode, cart);
        });
        Collection<String> voucherCodesAfterReleasing = defaultVoucherService.getAppliedVoucherCodes(cart);

        int releasedVoucherCodes = voucherCodes.size() - voucherCodesAfterReleasing.size();
        if (releasedVoucherCodes < voucherCodes.size()) {
            LOG.warn('Released [{}/{}] legacy vouchers for cart [{}]', releasedVoucherCodes, voucherCodes.size(), cart.getCode());
        } else {
            LOG.info('Released [{}/{}] legacy vouchers for cart [{}]', releasedVoucherCodes, voucherCodes.size(), cart.getCode());
        }
    }
}

private void resetDiscountGroupForCart(CartModel cart) {
    UserDiscountGroup discountGroup = cart.getDiscountGroup();
    if (UserDiscountGroup.STUDENT.equals(discountGroup)) {
        cart.setDiscountGroup(null);
        calculationService.recalculate(cart);
        LOG.info('Reseted [{}] discount group for cart [{}]', discountGroup, cart.getCode());
    } else if (discountGroup != null && cart.getWileyPartner() != null) {
        calculationService.recalculate(cart);
        LOG.info('Recalculated cart [{}] with discountGroup [{}] ', cart.getCode(), discountGroup);
    }
}

private List<CartModel> getAllCartsForSite(BaseSiteModel site) {
    return flexibleSearchService.<CartModel> search(FIND_ALL_CARTS_FOR_SITE_QUERY, Collections.singletonMap('siteUid', site.getUid())).getResult()
}"

INSERT_UPDATE Script; code[unique = true]; active[unique = true, default = true]; content
; releaseAllLegacyVouchersAndDiscountGroupFromCartsScript ; ; $releaseAllLegacyVouchersAndDiscountGroupFromCarts

INSERT_UPDATE ScriptingJob; code[unique = true]; scriptURI
; releaseAllLegacyVouchersAndDiscountGroupFromCartsJob ; model://releaseAllLegacyVouchersAndDiscountGroupFromCartsScript

INSERT_UPDATE CronJob; code[unique = true]; job(code); sessionLanguage(isoCode)
; releaseAllLegacyVouchersAndDiscountGroupFromCartsCronJob ; releaseAllLegacyVouchersAndDiscountGroupFromCartsJob ; en