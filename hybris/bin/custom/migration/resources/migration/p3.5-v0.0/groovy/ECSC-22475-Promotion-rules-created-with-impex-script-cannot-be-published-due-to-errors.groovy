
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
import org.slf4j.LoggerFactory;

LOG = LoggerFactory.getLogger("create_mobile_promo_images")

modelService = spring.getBean("defaultModelService")
mediaService = spring.getBean("defaultMediaService")
catalogVersionService = spring.getBean("defaultCatalogVersionService")

welContentCatalogStaged = catalogVersionService.getCatalogVersion("welContentCatalog","Staged")
welContentCatalogOnline = catalogVersionService.getCatalogVersion("welContentCatalog","Online")
welProductCatalogStaged = catalogVersionService.getCatalogVersion("welProductCatalog","Staged")
welProductCatalogOnline = catalogVersionService.getCatalogVersion("welProductCatalog","Online")
mobileMediaFormatModel = mediaService.getFormat("mobile")

createMobileImage("pmp offer image", welContentCatalogStaged, welContentCatalogOnline)
createMobileImage("CIA Promo Banner", welContentCatalogStaged, welContentCatalogOnline)
createMobileImage("cpa-offer", welContentCatalogOnline, welContentCatalogStaged)
createMobileImage("CMA Promo Aug 2016", welProductCatalogStaged, welProductCatalogOnline)
createMobileImage("FRM Promo Banner", welProductCatalogStaged, welProductCatalogOnline)

def createMobileImage(imageCode, stagedCatalogVersion, onlineCatalogVersion) {
    mobileImageCode = "${imageCode} Mobile"
    try {
        mediaService.getMedia(stagedCatalogVersion, mobileImageCode)
        LOG.warn("Image {} is present. No need to create. Skipping.", mobileImageCode)
        //image is present
    } catch (UnknownIdentifierException exc) {
        // no image - it needs to be created
        LOG.info("Image {} is absent. Will create mobile version", mobileImageCode)

        imageStaged = mediaService.getMedia(stagedCatalogVersion, imageCode)

        imageMobileStaged = modelService.clone(imageStaged)
        imageMobileStaged.setCode(mobileImageCode)
        imageMobileStaged.setMediaFormat(mobileMediaFormatModel)

        modelService.save(imageMobileStaged)

        imageMobileOnline = modelService.clone(imageMobileStaged)
        imageMobileOnline.setCatalogVersion(onlineCatalogVersion)

        modelService.save(imageMobileOnline)
    }
}