import de.hybris.platform.catalog.model.CatalogModel
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.core.model.security.PrincipalGroupModel
import de.hybris.platform.couponservices.model.*
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel
import de.hybris.platform.promotions.model.PromotionGroupModel
import de.hybris.platform.ruleengineservices.maintenance.RuleCompilerPublisherResult
import de.hybris.platform.ruleengineservices.maintenance.RuleMaintenanceService
import de.hybris.platform.ruleengineservices.rule.data.*
import de.hybris.platform.ruleengineservices.rule.services.RuleActionsRegistry
import de.hybris.platform.ruleengineservices.rule.services.RuleActionsService
import de.hybris.platform.ruleengineservices.rule.services.RuleConditionsRegistry
import de.hybris.platform.ruleengineservices.rule.services.RuleConditionsService
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException
import de.hybris.platform.servicelayer.exceptions.ModelSavingException
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.search.FlexibleSearchService
import de.hybris.platform.voucher.model.*
import groovy.transform.Field
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.LocaleUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.stream.Collectors

@Field final Locale EN = LocaleUtils.toLocale("en")
@Field final Logger LOG = LoggerFactory.getLogger("voucher_to_coupon")
@Field final ModelService modelService = (ModelService) spring.getBean("modelService")
@Field final FlexibleSearchService flexibleSearchService = (FlexibleSearchService) spring.getBean("flexibleSearchService")
@Field final RuleConditionsService ruleConditionsService = (RuleConditionsService) spring.getBean("ruleConditionsService");
@Field final RuleConditionsRegistry ruleConditionsRegistry = (RuleConditionsRegistry) spring.getBean("ruleConditionsRegistry");
@Field final RuleActionsService ruleActionsService = (RuleActionsService) spring.getBean("ruleActionsService");
@Field final RuleActionsRegistry ruleActionsRegistry = (RuleActionsRegistry) spring.getBean("ruleActionsRegistry");
@Field final RuleMaintenanceService ruleMaintenanceService = (RuleMaintenanceService) spring.getBean("ruleMaintenanceService");

@Field final String PROMOTIONAL_VOUCHER_QUERY =
        "SELECT {pv.pk} " +
                "FROM {PromotionVoucher AS pv " +
                "LEFT JOIN SingleCodeCoupon AS scc ON {pv.voucherCode} = {scc.couponId} } " +
                "WHERE {scc.couponId} IS NULL";

@Field final String SERIAL_VOUCHER_QUERY =
        "SELECT {sv.pk} " +
                "FROM {SerialVoucher AS sv " +
                "LEFT JOIN MultiCodeCoupon AS mcc ON {sv.code} = {mcc.couponId} } " +
                "WHERE {mcc.couponId} IS NULL";

@Field final String DEFAULT_PROMO_GROUP_ID = "welPromoGrp";

@Field Map<String, RuleConditionDefinitionData> conditionDefinitionMap = null;
@Field Map<String, RuleActionDefinitionData> actionDefinitionMap = null;
@Field Map<String, PromotionGroupModel> promotionGroupMap = null;
@Field PromotionGroupModel defaultPromotionGroup = null;
@Field CodeGenerationConfigurationModel codeGenerationConfiguration = null;

migrateVouchersToCoupons();

void migrateVouchersToCoupons() {

    conditionDefinitionMap = ruleConditionsRegistry.getAllConditionDefinitionsAsMap();
    actionDefinitionMap = ruleActionsRegistry.getAllActionDefinitionsAsMap();
    promotionGroupMap = getAllPromotionGroupAsMap();
    defaultPromotionGroup = getPromotionGroupForId(DEFAULT_PROMO_GROUP_ID);

    try {
        CodeGenerationConfigurationModel codeGenerationConfigurationTemplate = new CodeGenerationConfigurationModel();
        codeGenerationConfigurationTemplate.setName("default-configuration");
        codeGenerationConfiguration = flexibleSearchService.getModelByExample(codeGenerationConfigurationTemplate);
    } catch (ModelNotFoundException exc) {
        LOG.error("Cannot continue voucher migration... Reason: '{}'", exc.getMessage());
        return;
    }

    LOG.info("Starting PromotionVoucher to SingleCodeCoupon migration...");
    migratePromotionVoucherToSingleCodeCoupon();
    LOG.info("Starting SerialVoucher to MultiCodeCoupon migration...");
    migrateSerialVoucherToMultiCodeCoupon();

}

void migratePromotionVoucherToSingleCodeCoupon() {
    List<PromotionVoucherModel> vouchers = flexibleSearchService.search(PROMOTIONAL_VOUCHER_QUERY).getResult();
    LOG.info("Found {} PromotionVoucher instance(s) to migrate", vouchers.size());
    vouchers.forEach({ voucher ->
        if (isVoucherWithValidDateRestriction(voucher) && voucher.getVoucherCode() != null) {
            LOG.info("Migrating PromotionVoucher '{}'", voucher.getCode());

            SingleCodeCouponModel coupon = modelService.create(SingleCodeCouponModel.class);
            PromotionSourceRuleModel rule = modelService.create(PromotionSourceRuleModel.class);

            coupon.setCouponId(voucher.getVoucherCode());
            coupon.setMaxTotalRedemptions(voucher.getRedemptionQuantityLimit());
            coupon.setMaxRedemptionsPerCustomer(voucher.getRedemptionQuantityLimitPerUser());

            migrateVoucherToCoupon(coupon, voucher, rule);
        } else {
            LOG.warn("PromotionVoucher '{}' has invalid DateRestriction or empty voucherCode. Skipping migration", voucher.getCode());
        }
    });
}

void migrateSerialVoucherToMultiCodeCoupon() {
    List<SerialVoucherModel> vouchers = flexibleSearchService.search(SERIAL_VOUCHER_QUERY).getResult();
    LOG.info("Found {} SerialVoucher instance(s) to migrate", vouchers.size());
    vouchers.forEach({ voucher ->
        if (isVoucherWithValidDateRestriction(voucher)) {
            LOG.info("Migrating SerialVoucher '{}'", voucher.getCode());

            MultiCodeCouponModel coupon = modelService.create(MultiCodeCouponModel.class);
            PromotionSourceRuleModel rule = modelService.create(PromotionSourceRuleModel.class);

            coupon.setCouponId(voucher.getCode());
            coupon.setGeneratedCodes(voucher.getCodes());
            coupon.setCodeGenerationConfiguration(codeGenerationConfiguration);

            migrateVoucherToCoupon(coupon, voucher, rule);
        } else {
            LOG.warn("SerialVoucher '{}' has invalid DateRestriction. Skipping migration", voucher.getCode());
        }
    });
}

private void migrateVoucherToCoupon(AbstractCouponModel coupon, VoucherModel voucher, PromotionSourceRuleModel rule) {
    migrateBasicData(coupon, voucher, rule);
    migrateRestrictionToCondition(coupon, voucher, rule);
    migrateValueToAction(voucher, rule);

    List<CouponRedemptionModel> redemptions = new ArrayList<>();
    migrateInvalidationToRedemption(voucher, coupon, redemptions)

    try {
        modelService.saveAll(coupon, rule);
        if (!redemptions.isEmpty()) {
            modelService.saveAll(redemptions);
        }
        LOG.info("Coupon '{}' and rule '{}' have been saved.", coupon.getCouponId(), rule.getCode());
        RuleCompilerPublisherResult publishing = ruleMaintenanceService.compileAndPublishRules(Arrays.asList(rule));
        if (!RuleCompilerPublisherResult.Result.SUCCESS.equals(publishing.getResult())) {
            LOG.error("Cannot publish the rule '{}'", rule.getCode());
        }
    } catch (ModelSavingException exc) {
        LOG.error("Cannot save coupon '{}' and rule '{}'. Reason: '{}'", coupon.getCouponId(), rule.getCode(), exc.getMessage());
    }
}

private void migrateBasicData(AbstractCouponModel coupon, VoucherModel voucher, PromotionSourceRuleModel rule) {
    coupon.setActive(true);
    coupon.setName(voucher.getName(EN), EN);

    rule.setCode("voucher_coupon_promotion_" + voucher.getCode());
    rule.setName(voucher.getName(EN), EN);
    rule.setMessageFired(voucher.getDescription(EN), EN);
    rule.setDescription(null);
    rule.setPriority(voucher.getPriority() + 15000);
    rule.setStackable(false);
    rule.setRuleGroup(null);
    rule.setExcludeFromStorefrontDisplay(true);
}

private void migrateRestrictionToCondition(AbstractCouponModel coupon, VoucherModel voucher, PromotionSourceRuleModel rule) {
    List<RuleConditionData> conditions = new ArrayList<>();

    RuleConditionDefinitionData qualifyingCouponsDefinition = conditionDefinitionMap.get("y_qualifying_coupons");
    RuleConditionData qualifyingCouponsCondition = ruleConditionsService.createConditionFromDefinition(qualifyingCouponsDefinition);
    Map<String, RuleParameterData> params = qualifyingCouponsCondition.getParameters();
    RuleParameterData couponsParam = params.get("coupons");
    couponsParam.setValue(Arrays.asList(coupon.getCouponId()));
    conditions.add(qualifyingCouponsCondition);

    for (RestrictionModel restriction : voucher.getRestrictions()) {
        if (restriction.getClass().equals(DateRestrictionModel.class)) {
            coupon.setStartDate(((DateRestrictionModel) restriction).getStartDate());
            coupon.setEndDate(((DateRestrictionModel) restriction).getEndDate());
            rule.setStartDate(((DateRestrictionModel) restriction).getStartDate());
            rule.setEndDate(((DateRestrictionModel) restriction).getEndDate());
        } else if (restriction.getClass().equals(ProductRestrictionModel.class)) {
            RuleConditionDefinitionData qualifyingProductsDefinition = conditionDefinitionMap.get("y_qualifying_products");
            RuleConditionData qualifyingProductsCondition = ruleConditionsService.createConditionFromDefinition(qualifyingProductsDefinition);
            params = qualifyingProductsCondition.getParameters();
            RuleParameterData productsParam = params.get("products");
            productsParam.setValue(
                    ((ProductRestrictionModel) restriction).getProducts().stream().map({ product -> product.getCode() }).collect(Collectors.toList())
            );
            RuleParameterData productsOperatorParam = params.get("products_operator");
            if (((ProductRestrictionModel) restriction).getPositive()) {
                productsOperatorParam.setValue("CONTAINS_ANY");
            } else {
                productsOperatorParam.setValue("NOT_CONTAINS");
            }
            conditions.add(qualifyingProductsCondition);
            List<ProductModel> products = ((ProductRestrictionModel) restriction).getProducts();
            if (CollectionUtils.isNotEmpty(products)) {
                rule.setWebsite(getPromoGroupForCatalogVersion(products.get(0).getCatalogVersion()));
            }
        } else if (restriction instanceof ProductCategoryRestrictionModel) {
            RuleConditionDefinitionData qualifyingCategoriesDefinition = conditionDefinitionMap.get("y_qualifying_categories");
            RuleConditionData qualifyingCategoriesCondition = ruleConditionsService.createConditionFromDefinition(qualifyingCategoriesDefinition);
            params = qualifyingCategoriesCondition.getParameters();
            RuleParameterData categoriesParam = params.get("categories");
            categoriesParam.setValue(
                    ((ProductCategoryRestrictionModel) restriction).getCategories().stream().map({ category -> category.getCode() }).collect(Collectors.toList())
            );
            RuleParameterData categoriesOperatorParam = params.get("categories_operator");
            if (((ProductCategoryRestrictionModel) restriction).getPositive()) {
                categoriesOperatorParam.setValue("CONTAINS_ANY");
            } else {
                categoriesOperatorParam.setValue("NOT_CONTAINS");
            }
            conditions.add(qualifyingCategoriesCondition);
            List<CategoryModel> categories = ((ProductCategoryRestrictionModel) restriction).getCategories();
            if (CollectionUtils.isNotEmpty(categories)) {
                rule.setWebsite(getPromoGroupForCatalogVersion(categories.get(0).getCatalogVersion()));
            }

        } else if (restriction.getClass().equals(OrderRestrictionModel)) {
            RuleConditionDefinitionData cartTotalDefinition = conditionDefinitionMap.get("y_cart_total");
            RuleConditionData cartTotalCondition = ruleConditionsService.createConditionFromDefinition(cartTotalDefinition);
            params = cartTotalCondition.getParameters();
            RuleParameterData valueParam = params.get("value");
            Map<String, BigDecimal> map = new HashMap<>();
            map.put(((OrderRestrictionModel) restriction).getCurrency().getIsocode(), BigDecimal.valueOf(((OrderRestrictionModel) restriction).getTotal()));
            valueParam.setValue(map);
            RuleParameterData operatorParam = params.get("operator");
            if (((OrderRestrictionModel) restriction).getPositive()) {
                operatorParam.setValue("GREATER_THAN");
            } else {
                operatorParam.setValue("LESS_THAN_OR_EQUAL");
            }
            conditions.add(cartTotalCondition);
        } else if (restriction.getClass().equals(UserRestrictionModel)) {
            RuleConditionDefinitionData targetCustomersDefinition = conditionDefinitionMap.get("y_target_customers");
            RuleConditionData targetCustomersCondition = ruleConditionsService.createConditionFromDefinition(targetCustomersDefinition);
            params = targetCustomersCondition.getParameters();
            RuleParameterData customerGroupsParam = params.get("customer_groups");
            RuleParameterData customersParam = params.get("customers");
            List<String> groupList = new ArrayList<>();
            List<String> userList = new ArrayList<>();
            ((UserRestrictionModel) restriction).getUsers().forEach({ principal ->
                if (principal instanceof PrincipalGroupModel) {
                    groupList.add(principal.getUid());
                } else {
                    userList.add(principal.getUid());
                }
            });
            if (!groupList.isEmpty()) {
                customerGroupsParam.setValue(groupList);
            }
            if (!userList.isEmpty()) {
                customersParam.setValue(userList);
            }
            RuleParameterData operatorParam = params.get("customer_groups_operator");
            if (((UserRestrictionModel) restriction).getPositive()) {
                operatorParam.setValue("CONTAINS_ANY");
            } else {
                operatorParam.setValue("NOT_CONTAINS");
            }
            conditions.add(targetCustomersCondition);
        } else {
            LOG.warn("Restriction of '{}' type is skipped for voucher '{}'", restriction.getClass(), voucher.getCode());
        }
    }

    if (rule.getWebsite() == null) {
        rule.setWebsite(defaultPromotionGroup);
    }

    String conditionsString = ruleConditionsService.convertConditionsToString(conditions);
    LOG.info("Condition: '{}'", conditionsString);
    rule.setConditions(conditionsString);
}

private void migrateValueToAction(VoucherModel voucher, PromotionSourceRuleModel rule) {
    String actionString = "";
    if (voucher.getAbsolute()) {
        RuleActionDefinitionData fixedDiscountDefinition = null;
        if (isVoucherWithProductRestriction(voucher)) {
            fixedDiscountDefinition = actionDefinitionMap.get("y_order_entry_fixed_discount");
        } else {
            fixedDiscountDefinition = actionDefinitionMap.get("y_order_fixed_discount");
        }
        RuleActionData fixedDiscountAction = ruleActionsService.createActionFromDefinition(fixedDiscountDefinition);
        Map<String, RuleParameterData> params = fixedDiscountAction.getParameters();
        RuleParameterData valueParam = params.get("value");
        Map<String, BigDecimal> map = new HashMap<>();
        map.put(voucher.getCurrency().getIsocode(), BigDecimal.valueOf(voucher.getValue()));
        valueParam.setValue(map);
        actionString = ruleActionsService.convertActionsToString(Arrays.asList(fixedDiscountAction));
    } else {
        RuleActionDefinitionData percentageDiscountDefinition = null;
        if (isVoucherWithProductRestriction(voucher)) {
            percentageDiscountDefinition = actionDefinitionMap.get("y_order_entry_percentage_discount");
        } else {
            percentageDiscountDefinition = actionDefinitionMap.get("y_order_percentage_discount");
        }
        RuleActionData percentageDiscountAction = ruleActionsService.createActionFromDefinition(percentageDiscountDefinition);
        Map<String, RuleParameterData> params = percentageDiscountAction.getParameters();
        RuleParameterData valueParam = params.get("value");
        valueParam.setValue(BigDecimal.valueOf(voucher.getValue()));
        actionString = ruleActionsService.convertActionsToString(Arrays.asList(percentageDiscountAction));
    }
    LOG.info("Action: '{}'", actionString);
    rule.setActions(actionString);
}

private void migrateInvalidationToRedemption(VoucherModel voucher, AbstractCouponModel coupon, ArrayList<CouponRedemptionModel> redemptions) {
    voucher.getInvalidations().forEach({ invalidation ->
        CouponRedemptionModel redemption = modelService.create(CouponRedemptionModel.class);
        redemption.setCoupon(coupon);
        redemption.setCouponCode(invalidation.getCode());
        redemption.setOrder(invalidation.getOrder());
        redemption.setUser(invalidation.getUser());
        redemptions.add(redemption);
    });
    LOG.info("'{}' redemptions were created for coupon '{}'", redemptions.size(), coupon.getCouponId());
}

private Map<String, PromotionGroupModel> getAllPromotionGroupAsMap() {
    Map<String, PromotionGroupModel> promotionGroupMap = new HashMap<>();
    promotionGroupMap.put("agsProductCatalog", getPromotionGroupForId("agsPromoGrp"));
    promotionGroupMap.put("welProductCatalog", getPromotionGroupForId("welPromoGrp"));
    return promotionGroupMap;
}

private PromotionGroupModel getPromotionGroupForId(String id) {
    PromotionGroupModel promotionGroupTemplate = new PromotionGroupModel();
    promotionGroupTemplate.setIdentifier(id);
    return flexibleSearchService.getModelByExample(promotionGroupTemplate);
}

private PromotionGroupModel getPromoGroupForCatalogVersion(CatalogVersionModel catalogVersionModel) {
    CatalogModel catalog = catalogVersionModel.getCatalog()
    return promotionGroupMap.get(catalog.getId())
}

private boolean isVoucherWithProductRestriction(VoucherModel voucherModel) {
    voucherModel.getRestrictions()
            .stream()
            .filter({ r -> r instanceof ProductRestrictionModel || r instanceof ProductCategoryRestrictionModel })
            .findAny().isPresent();
}

private boolean isVoucherWithValidDateRestriction(VoucherModel voucherModel) {
    for (RestrictionModel restriction : voucherModel.getRestrictions()) {
        if (restriction instanceof DateRestrictionModel && ((DateRestrictionModel) restriction).getEndDate() < new Date()) {
            LOG.warn("DateRestriction '{}' has endDate in the past", restriction.getPk());
            return false;
        }
    }
    return true;
}