ruleNames = ["legacy_promotion_PMP_Promotion_Text", "legacy_promotion_WEL_CFA_Offer","legacy_promotion_WEL_CFA_Offer_-_11th_hour","legacy_promotion_WEL_CIA_Offer","legacy_promotion_WEL_CMA_11th_Hour","legacy_promotion_WEL_CMA_Offer","legacy_promotion_WEL_CPA_Offer_-_Platinum_Only","legacy_promotion_WEL_FRM_Messaging"]
//find rule models for codes
rules = ruleNames.collect {ruleService.getRuleForCode(it)}
//publish 'em all
ruleMaintenanceService.compileAndPublishRules(rules)
