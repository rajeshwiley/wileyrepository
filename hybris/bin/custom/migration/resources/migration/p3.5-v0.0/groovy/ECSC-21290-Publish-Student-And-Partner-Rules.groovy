ruleNames = ["studentDiscountPromotion", "partnerDiscountPromotion","partnerDiscountPotentialPromotion"]
//find rule models for codes
rules = ruleNames.collect {ruleService.getRuleForCode(it)}
//publish 'em all
ruleMaintenanceService.compileAndPublishRules(rules)
