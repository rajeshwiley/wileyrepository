https://confluence.wiley.com/display/ECSC/QA
#To build docker:

cd hybwiley/configs/common/nutch-image/
docker build -f Dockerfile -t wiley-nutch .
docker run -e SOLR_URL=http://ecommerce-qa-solr-master.ecomm-internal.wiley.com:8983/solr -e SOLR_URL_SLAVE=http://ecommerce-qa-solr-slave.ecomm-internal.wiley.com:8983/solr -e SITE_URL=https://qa.store.wiley.com -e SOLR_USER=solrindexingclient -e SOLR_PASSWORD=indexingclient123 wiley-nutch