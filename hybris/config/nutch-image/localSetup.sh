#!/bin/bash

#script for setup on local docker on windows
#run after local hybris is up and running
RUN yum -y install openssl
RUN yum -y install less
rm -rf apache-nutch-1.13/conf
mv conf apache-nutch-1.13
mv plugins/* apache-nutch-1.13/plugins
rm -f apache-nutch-1.13/lib/apache-nutch-1.13.jar
mv apache-nutch-1.13.jar apache-nutch-1.13/lib/apache-nutch-1.13.jar
mkdir apache-nutch-1.13/urls
mv urls apache-nutch-1.13/urls
#add certificate
#change to your ip
echo '10.30.0.100     wiley.local' >> /etc/hosts
echo -n | openssl s_client -connect wiley.local:9002 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/examplecert.crt
/usr/java/latest/bin/keytool -import -trustcacerts -keystore /usr/java/latest/jre/lib/security/cacerts -storepass changeit -noprompt -alias mycert -file /tmp/examplecert.crt
#clean solr core
wget "http://wiley.local:8983/solr/master_wileyb2cContent_Item_default/update?stream.body=<delete><query>*:*</query></delete>" --no-check-certificate
wget "http://wiley.local:8983/solr/admin/cores?action=UNLOAD&core=master_wileyb2cContent_Item_default" --no-check-certificate
wget "http://wiley.local:8983/solr/admin/cores?action=CREATE&name=master_wileyb2cContent_Item_default&configSet=content" --no-check-certificate

#usefull commands
#removes crawled sites from nutch db and allows to rerun test
#rm -rf apache-nutch-1.13/crawl

#creates dump from nutch db
#apache-nutch-1.13/bin/nutch readdb apache-nutch-1.13/crawl/crawldb/crawldb -dump apache-nutch-1.13/crawldb_dump