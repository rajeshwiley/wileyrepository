#!/bin/bash
#Load variables from properties
# SOLR_URL=`eval grep 'solr.url.master' local.properties|cut -d'=' -f2| tr -d '\r'`
echo $SOLR_URL
# SITE_URL=`eval grep 'website.wileyb2c.https' local.properties|cut -d'=' -f2| tr -d '\r'`
echo $SITE_URL
# NUM_ROUNDS=`eval grep 'num.rounds' nutch.properties|cut -d'=' -f2| tr -d '\r'`

echo $NUM_ROUNDS
echo $ALLOWED_DOCS_COUNT_DELTA
echo $NUM_CLEAN_ATTEMPTS
echo $CORE_CLEAN_DELAY

#Configure Nutch
echo -e "\n\n"
echo "Preparing Apache Nutch..."
echo -e "\n\n"

rm -r apache-nutch-$NUTCH_VERSION/conf &&
cp -r conf apache-nutch-$NUTCH_VERSION &&
cp -r plugins/* apache-nutch-$NUTCH_VERSION/plugins
rm apache-nutch-$NUTCH_VERSION/plugins/indexer-solr/indexer-solr.jar
cp indexer-solr-patched.jar apache-nutch-$NUTCH_VERSION/plugins/indexer-solr/indexer-solr.jar

# Update solr parameters in index-writers.xml file to ENV variables passed to docker run
sed -i -e "s!SOLR_CONTENT_INDEX_URL!$SOLR_URL/$SOLR_CORE_NAME!" -e "s!SOLR_USER!$SOLR_USER!" -e "s!SOLR_PASSWORD!$SOLR_PASSWORD!" apache-nutch-$NUTCH_VERSION/conf/index-writers.xml

# local env configuration
if [[ ($SITE_URL == *wiley.local*) ]]
then
    DOCKER_HOST_IP=`getent hosts host.docker.internal | awk '{ print $1 }'`
    echo $DOCKER_HOST_IP wiley.local >> /etc/hosts
    # get certificate for wiley.local site and store it in java cacerts
    echo -n | openssl s_client -connect wiley.local:9002 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/wiley.crt
    /usr/java/latest/bin/keytool -import -trustcacerts -keystore /usr/java/latest/jre/lib/security/cacerts -storepass changeit -noprompt -alias wileycert -file /tmp/wiley.crt
fi

if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo "ERROR: Unable to prepare Apache Nutch..."
    echo -e "\n\n"
    exit 1
fi

mkdir apache-nutch-$NUTCH_VERSION/urls

echo "Downloading URLs from $SITE_URL..."
wget $SITE_URL/contentsearch/urls --no-check-certificate
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo "ERROR: Unable to download URLs list"
    echo -e "\n\n"
    exit 1
fi
mv urls apache-nutch-$NUTCH_VERSION/urls/urls
#head -2 apache-nutch-$NUTCH_VERSION/urls/urls > apache-nutch-$NUTCH_VERSION/urls/urls2
#rm apache-nutch-$NUTCH_VERSION/urls/urls

#hybris warmup on initial urls
while read -r line
do
        if [ ! -z "$line" ]
        then
                echo "$line"
                curl -s -S "$line" > /dev/null
        fi
done < apache-nutch-$NUTCH_VERSION/urls/urls

#create Solr core for content pages
echo -e "\n\n"
echo "Preparing Solr master at $SOLR_URL..."
echo -e "\n\n"

curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/admin/cores?action=CREATE&name=$SOLR_CORE_NAME&configSet=$SOLR_CONFIG_NAME"

for solr_slaves in $SOLR_URL_SLAVE; do
echo -e "\n\n"
echo "Preparing Solr slave at $solr_slaves..."
echo -e "\n\n"
curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$solr_slaves/admin/cores?action=CREATE&name=$SOLR_CORE_NAME&configSet=$SOLR_CONFIG_NAME"
done

#save amount of documents to compare later
CRAWLED_DOCS_BEFORE=`curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/admin/cores?action=STATUS&core=$SOLR_CORE_NAME" | jq -r ".status.${SOLR_CORE_NAME}.index.numDocs"`
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo "ERROR: Unable to check solr core $SOLR_CORE_NAME"
    echo -e "\n\n"
    exit 1
fi
echo -e "\n\n"
echo "Num documents before crawling $CRAWLED_DOCS_BEFORE"
echo -e "\n\n"

#disable core replication
echo -e "\n\n"
echo "Disabling Solr replication..."
echo -e "\n\n"
curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/$SOLR_CORE_NAME/replication?command=disablereplication"
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo "ERROR: Unable to disable core replication"
    echo -e "\n\n"
    exit 1
fi
#remove documents from solr core



CLEAN_ATTEMPT=0

while [ $CLEAN_ATTEMPT -le $NUM_CLEAN_ATTEMPTS ]
do
    echo -e "\n\n"
    echo "Cleaning up solr documents in core $SOLR_CORE_NAME... Attempt #$CLEAN_ATTEMPT"
    echo -e "\n\n"
    curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/$SOLR_CORE_NAME/update?commit=true" --data "<delete><query>*:*</query></delete>" -H "Content-type:text/xml; charset=utf-8"
    if [ $? -ne 0 ]
    then
        echo -e "\n\n"
        echo "ERROR: Unable to clean documents from core. REPLICATION DISABLED"
        echo -e "\n\n"
        exit 1
    fi
    
    sleep $CORE_CLEAN_DELAY
    
    NUM_DOCS=`curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/admin/cores?action=STATUS&core=$SOLR_CORE_NAME" | jq -r ".status.${SOLR_CORE_NAME}.index.numDocs"`
    if [ $? -ne 0 ]
    then
        echo -e "\n\n"
        echo "ERROR: Unable to check solr core $SOLR_CORE_NAME"
        echo -e "\n\n"
        exit 1
    fi
    if [ $NUM_DOCS -eq 0 ]
    then
        echo -e "\n\n"
        echo "Core cleaned"
        echo -e "\n\n"
        break
    fi
    
    CLEAN_ATTEMPT=$(($CLEAN_ATTEMPT+1))
    
    if [ $CLEAN_ATTEMPT -eq $NUM_CLEAN_ATTEMPTS ]
    then
        echo -e "\n\n"
        echo "ERROR: Unable to check clean core $SOLR_CORE_NAME. Still $NUM_DOCS docs left in core"
        echo -e "\n\n"
        exit 1
    fi
done

#Start indexing on container start up
echo -e ""
echo "Starting crawling $SOLR_CORE_NAME..."
echo -e ""
apache-nutch-$NUTCH_VERSION/bin/crawl -i -s apache-nutch-$NUTCH_VERSION/urls/ apache-nutch-$NUTCH_VERSION/crawl/crawldb $NUM_ROUNDS
if [ $? -ne 0 ]
then
    echo -e "\n\n"
    echo "ERROR: There were errors during crawling process. Stopping. REPLICATION DISABLED"
    echo -e "\n\n"
    exit 1
fi

#check amount of crawled documents and compare it
CRAWLED_DOCS_AFTER=`curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/admin/cores?action=STATUS&core=$SOLR_CORE_NAME" | jq -r ".status.${SOLR_CORE_NAME}.index.numDocs"`
if [ $CRAWLED_DOCS_BEFORE -eq 0 ]
then
    ACCEPTABLE_NEW_DOCS_AMOUNT=1
else
    echo -e "\n\n"
    echo "Num documents after crawling $CRAWLED_DOCS_AFTER. Comparing new amount with previous($CRAWLED_DOCS_BEFORE) using allowed delta $ALLOWED_DOCS_COUNT_DELTA"
    echo -e "\n\n"
    ACCEPTABLE_NEW_DOCS_AMOUNT=`bc -l <<< "$CRAWLED_DOCS_AFTER/$CRAWLED_DOCS_BEFORE >= $ALLOWED_DOCS_COUNT_DELTA"`
fi

if [ $ACCEPTABLE_NEW_DOCS_AMOUNT -eq 1 ]
then
    #enable core replication
    echo -e "\n\n"
    echo "Newly crawled document amount is sufficient - enabling master->slave replication for core $SOLR_CORE_NAME..."
    echo -e "\n\n"
    curl -s -S --user $SOLR_USER:$SOLR_PASSWORD "$SOLR_URL/$SOLR_CORE_NAME/replication?command=enablereplication"
    if [ $? -ne 0 ]
    then
        echo -e "\n\n"
        echo "ERROR: There were errors during enabling replication. Stopping. REPLICATION DISABLED"
        echo -e "\n\n"
        exit 1
    fi
else
    echo -e "\n\n"
    echo "ERROR: Not enough crawled document. Amount before crawling: $CRAWLED_DOCS_BEFORE, amount after $CRAWLED_DOCS_AFTER. REPLICATION DISABLED"
    echo -e "\n\n"
    exit 1
fi

#cat apache-nutch-$NUTCH_VERSION/logs/hadoop.log
