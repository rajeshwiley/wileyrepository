echo creating git hooks...
if exist ..\.git\hooks (
	echo symlink from .git\hooks to git-hooks already exists
) else (
	echo create symlink from .git\hooks to git-hooks
	mklink /J ..\.git\hooks ..\git-hooks
)

echo configuring git filters for properties encryption...
@echo off
set /p PASSWORD="Please enter DEV password: "
git config filter.openssl-DEV.clean "openssl enc -aes-256-cfb -iter 300000 -nosalt -k %PASSWORD%"
git config filter.openssl-DEV.smudge "openssl enc -d -aes-256-cfb -iter 300000 -nosalt -k %PASSWORD% 2> /dev/null || cat"
git config filter.openssl-DEV.required true
git config diff.openssl-DEV.textconv "openssl enc -d -aes-256-cfb -iter 300000 -nosalt -k %PASSWORD% -in 2> /dev/null || cat"
